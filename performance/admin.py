# encoding: utf-8
from django import forms
from django.contrib import admin, messages
from django.contrib.admin.filters import SimpleListFilter
from django.utils.dateformat import format
from performance.db_models.competency import *
from performance.db_models.kpi_library import *
from performance.db_models.log import AlignLog
from performance.db_models.strategy_map import StrategyMap, GroupKPI

from performance.forms import CompetencyForm, KPIForm
from performance.models import *
from utils import ExcelResponse
# from simple_history.admin import SimpleHistoryAdmin
from utils.export_as_csv import export_as_csv_action

# import reversion


class JobFilter(SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = 'Job title'

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'job__id'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        jobs = JobTitle.objects.all()
        filters = []
        for j in jobs:
            competency = Competency.objects.filter(job=j).count()
            if competency > 0 and competency == Competency.objects.filter(job=j, name_vi__isnull=False).exclude(
                    name_vi='').count():
                filters.append((j.id, u"✔    " + j.name))
            else:
                filters.append((j.id, j.name))
        return tuple(filters)

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        if self.value():
            return queryset.filter(job_id=self.value())
        else:
            return queryset


class CompetencyInline(admin.TabularInline):
    model = Competency
    fk_name = 'parent'
    extra = 1
    fields = ('name', 'name_en', 'name_vi', 'description', 'description_en',
              'description_vi', 'in_library')


class KPIInline(admin.TabularInline):
    model = KPI
    fk_name = 'parent'
    extra = 1
    # 'refer_to_id', 'cascaded_from_id',
    fields = ('id', 'name', 'parent',
              # 'name_en', 'name_vi',
              'description',

              # 'description_en', 'description_vi',
              'weight', 'in_library')


class CompetencyLibInline(admin.TabularInline):
    model = CompetencyLib
    fk_name = 'parent'
    extra = 1
    fields = (
    'name', 'name_vi', 'name_en', 'description', 'description_vi', 'description_en', 'level', 'parent', 'category',
    'code')


class CompetencyLibAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'level', 'parent', 'category', 'code')
    search_fields = ('name', 'job__name',)
    inlines = [
        CompetencyLibInline,
    ]
    list_per_page = 30

    def get_queryset(self, request):
        qs = super(CompetencyLibAdmin, self).get_queryset(request)
        return qs.filter(parent=None)


class CompetencyAdmin(admin.ModelAdmin):  # (SimpleHistoryAdmin):
    list_display = ('name', 'user',)
    search_fields = ('name', 'job__name',)
    list_filter = ('in_library', JobFilter, 'belong_to_jobs__category__industry', 'belong_to_jobs',)
    filter_horizontal = ('belong_to_jobs',)
    inlines = [
        CompetencyInline,
    ]
    exclude = ('parent', 'copy_from', 'job',)
    form = CompetencyForm
    list_per_page = 200

    def get_queryset(self, request):
        qs = super(CompetencyAdmin, self).get_queryset(request)
        return qs.filter(parent=None, user=None)


# class KPIAdmin( SimpleHistoryAdmin):
@admin.register(KPI)
class KPIAdmin(admin.ModelAdmin):  # ( SimpleHistoryAdmin):
    list_display = ('name', 'user', 'year', 'quarter_period')
    search_fields = ('name', 'user__username', 'unique_key')
    #  list_filter = ('in_library', 'belong_to_jobs', 'belong_to_jobs__category__industry', 'year')
    list_filter = ('in_library', 'year')
    # filter_horizontal = ('belong_to_jobs',)

    # Duan disable the following lines, because theses will raise errors when access admin page
    # inlines = [
    #     KPIInline,
    # ]
    list_per_page = 200
    # exclude = ('parent', 'copy_from', 'job')
    #  exclude = ('job',)
    form = KPIForm
    raw_id_fields = ('parent', 'user', 'quarter_period', 'copy_from', 'cascaded_from', 'assigned_to', 'refer_to')

# admin.site.register(YourModel, YourModelAdmin)


class JobCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'industry', 'ordering',)
    list_editable = ('ordering',)
    list_filter = ('industry',)


class JobCompetencyInline(admin.TabularInline):
    model = Competency.belong_to_jobs.through
    raw_id_fields = ("competency",)
    # inlines = [CompetencyInline,]


class JobKPIInline(admin.StackedInline):
    #    model = KPI.belong_to_jobs.through
    raw_id_fields = ("kpi",)
    # inlines = [KPIInline,]
    # from django import forms


class JobtitleModelForm(forms.ModelForm):
    description = forms.CharField(widget=forms.Textarea)

    class Meta:
        model = JobTitle
        fields = '__all__'


class JobTitleAdmin(admin.ModelAdmin):
    list_display = ('name', 'category')
    search_fields = ('name', 'category__name')
    list_filter = ('category__industry',)
    inlines = [JobCompetencyInline]
    form = JobtitleModelForm


class QuotationAdmin(admin.ModelAdmin):
    list_display = (
    'client_name', 'created_date', 'package_training_total', 'package_standard_total', 'package_premium_total')


class ScoreLogAdmin(admin.ModelAdmin):
    list_filter = ('quarter_period',)


class AlignLogAdmin(admin.ModelAdmin):
    search_fields = ('user__email', 'user__username')
    list_display = ('user', 'quarter_period', 'is_done', 'log')

#
# class ReviewAdmin(admin.ModelAdmin):
#     list_display = ('reviewee', 'organization', 'reviewer', 'review_type', 'quarter_period', 'completion')
#     search_fields = ('reviewer__username', 'reviewee__username', 'year')
#     list_filter = ('review_type', 'organization', 'quarter_period')
#     actions = ['share_review_to_manager', 'share_review_to_all']
#
#     def share_review_to_manager(self, request, queryset):
#         queryset.filter(review_type='self').update(share_to_manager=True)
#
#     share_review_to_manager.short_description = "Share selected reviews to manager"
#
#     def share_review_to_all(self, request, queryset):
#         queryset.filter(review_type='manager').update(share_to_all=True)
#
#     share_review_to_all.short_description = "Share selected reviews to all"


class DescriptionCellAdmin(admin.ModelAdmin):
    list_display = ('id', 'slug', 'title')


class QuarterPeriodAdmin(admin.ModelAdmin):
    list_display = ('organization', 'due_date', 'quarter_name', 'quarter', 'year',
                    'status', 'completion',)
    search_fields = ('organization__name', 'year',)
    list_filter = ('organization', 'year', 'quarter', 'status')
    actions = ['export_current_quarter']

    def export_current_quarter(self, request, querset):
        orgs = Organization.objects.filter(enable_performance=True).values_list('id', flat=True)
        quarters = QuarterPeriod.objects.select_related('organization') \
            .filter(status='IN', organization__in=orgs)
        data = [['Organization', 'Due date']]
        for q in quarters:
            item = [
                q.organization.name,
                format(q.due_date, 'd-m-Y')
            ]
            data.append(item)

        return ExcelResponse(data, 'Current quarters')


class EbookAdmin(admin.ModelAdmin):
    list_display = ('name', 'ebook', 'product')


class BscKPIAdmin(admin.ModelAdmin):
    list_filter = ('checked', 'translated', 'department')
    list_display = ('name', 'measurement',)
    search_fields = ('name',)
    actions = ['export_excel']

    def export_excel(self, request, queryset):
        data = [['name',
                 'nam_vi',
                 'measurement', 'department']]
        for obj in queryset:
            item = [
                obj.name_en,
                obj.name_vi,
                obj.measurement,
                obj.department.name,
                obj.job_title.name
            ]
            data.append(item)

        return ExcelResponse(data)


class KpiLogAdmin(admin.ModelAdmin):
    list_display = ('editor', 'role_edit', 'user', 'status', 'log_type', 'created_at')
    list_filter = ('edited_date',)

    def created_at(self, obj):
        return format(obj.edited_date, 'd-m-Y')

    created_at.short_description = 'edited date'


class KpiCommentAdmin(admin.ModelAdmin):
    list_display = ('user', 'content', 'kpi_user', 'kpi', 'created_at', 'send_from_ui')
    search_fields = ('kpi__name', 'user__username', 'kpi__user__username', 'kpi_unique_key', 'content')

    list_filter = ('kpi__quarter_period', 'send_from_ui')

    actions = ['delete_change_unique_key_kpi',
               export_as_csv_action("Export only selected to excel",
                                    fields=['user', 'user__email', 'content', 'created_at'])]

    #   actions = ['delete_change_unique_key_kpi']

    def kpi_user(self, obj):
        if obj.kpi_id and obj.kpi.user_id:
            return obj.kpi.user.username
        return ''

    kpi_user.short_description = 'Kpi user'

    def delete_change_unique_key_kpi(self, request, queryset):
        for i in queryset:
            kpi = i.kpi
            kpi.unique_key = str(uuid.uuid1())
            kpi.save(actor=request.user)
            i.delete()

        messages.info(request, 'OK')


@admin.register(CascadeKPIQueue)
class CascadeKPIQueueAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'is_run', 'created_at',)  # 'kpi_id',)
    list_filter = ('is_run',)
    search_fields = ('kpi__name', 'kpi__id',)
    raw_id_fields = ("kpi",)


@admin.register(StrategyMap)
class StrategyMapAdmin(admin.ModelAdmin):
    list_display = ('organization', 'name', 'year', 'quarter_period', 'hash')
    #   list_filter = ('organization', 'year', 'hash')
    search_fields = ('organization', 'year', 'hash')


class TagItemInline(admin.TabularInline):
    model = TagItem


@admin.register(TagItem)
class TagItemAdmin(admin.ModelAdmin):
    list_display = ('name', 'parent',)
    search_fields = ('name',)
    inlines = [TagItemInline]
    exclude = ('parent',)

    def get_queryset(self, request):
        return admin.ModelAdmin.get_queryset(self, request).filter(parent=None)

#
# @admin.register(CategoryKPIFeedback)
# class CategoryKPIFeedbackAdmin(admin.ModelAdmin):
#     list_display = ('name', 'color', 'cat_order',)
#     list_filter = ()
#
#     form = CategoryKPIFeedbackAdminForm

#
# @admin.register(KPIFeedback)
# class KPIFeedbackAdmin(admin.ModelAdmin):
#     list_display = ('name', 'category')
#     list_filter = ()
#
#
# @admin.register(KPIFieldErrorMapping)
# class KPIFieldErrorMappingAdmin(admin.ModelAdmin):
#     list_display = ('field', 'error')
#     list_filter = ()
#
#     form = KPIFieldErrorMappingForm
#


admin.site.register(Industry)
admin.site.register(JobCategory, JobCategoryAdmin)
# admin.site.register(Review, ReviewAdmin)
admin.site.register(DescriptionCell, DescriptionCellAdmin)
admin.site.register(QuarterPeriod, QuarterPeriodAdmin)
# admin.site.register(Ebook, EbookAdmin)
# admin.site.register(Weeklygoal)
# admin.site.register(KPIApproval)

# admin.site.register(KPIApprovelNotification)
# admin.site.register(KPIApprovalNotificationMessage)
# admin.site.register(KpiLog, KpiLogAdmin)
admin.site.register(KpiComment, KpiCommentAdmin)
admin.site.register(BscDepartment)
admin.site.register(BscJobTitle)

admin.site.register(JobTitle, JobTitleAdmin)
admin.site.register(Competency, CompetencyAdmin)
admin.site.register(CompetencyLib, CompetencyLibAdmin)
admin.site.register(BscKPI, BscKPIAdmin)
admin.site.register(GroupKPI)
# admin.site.register(KPIFeedback)
# admin.site.register(Quotation, QuotationAdmin)
# admin.site.register(KPIFieldErrorMapping)
# admin.site.register(KPIErrorLog)
admin.site.register(ScoreLog, ScoreLogAdmin)
admin.site.register(AlignLog, AlignLogAdmin)
