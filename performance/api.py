#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.http.response import HttpResponse
from performance.documents import ReportScoreFinal
import json
from performance.base.views import PerformanceBaseView
from django.utils.dateformat import format
from performance.models import QuarterPeriod
from django.shortcuts import get_object_or_404
from mongoengine import Q as MQ
import datetime
from utils import ReportTemplate, unsigned_vi


# class PerfromanceAPI:
#     def get_kpi(self):
#         pass


# class PerformRewardData(PerformanceBaseView):
#     def get(self, request, *args, **kwargs):
#         self.organization = self.get_organization(request.user)
#         if 'get_quarters_by_year' in request.GET:
#             year = int(request.GET['get_quarters_by_year'])
#             response = self.get_quarters_by_year(year)
#             return HttpResponse(json.dumps(response), content_type="application/json")
#
#         if 'quarter_id' in request.GET:
#             self.quarter_period = get_object_or_404(QuarterPeriod,
#                                                organization=self.organization,
#                                                id=request.GET['quarter_id'])
#
#         else:
#             self.quarter_period = self.organization.get_current_quarter()
#
#         self.allowed_edit = self.is_allowed_edit(self.quarter_period)
#
#
#         if self.allowed_edit and self.quarter_period.update_perform_reward:  # update employees est-reward when performance soce change
#             self.calculate_emps_est_reward()
#
#             self.quarter_period.unsaved = True
#             self.quarter_period.update_perform_reward = False
#             self.quarter_period.save()
#
#
#         # archived_quarters = self.organization.get_passed_quarter().order_by('-id')[:5]
#         quarters = self.get_quarters_by_year(self.quarter_period.year)
#
#         rewarding_rules = {
#             'start_rewarding': self.quarter_period.start_rewarding,
#             'end_rewarding': self.quarter_period.end_rewarding,
#             'max_rewarding': self.quarter_period.max_rewarding,
#             'reward_cap': self.quarter_period.reward_cap,
#             'quarter_period': self.quarter_period.id,
#             'quarters': quarters,
#             'quarters_year':self.quarter_period.year,
#             'allowed_edit':self.allowed_edit,
#             'unsaved':self.quarter_period.unsaved,
#
#         }
#         scores = ReportScoreFinal.objects.filter(organization=self.organization.id,
#                                                  quarter_period=self.quarter_period.id).to_json()
#
#         response = [rewarding_rules, json.loads(scores)]
#         return HttpResponse(json.dumps(response), content_type="application/json")
#
#     def get_quarters_by_year(self, year):
#         archived_quarters = self.organization.get_passed_quarter_by_year(year).order_by('-id')
#
#         quarters = []
#         for q in archived_quarters:
#             quarters.insert(0, {
#                 'month': format(q.due_date, "M"),
#                 'quarter_id': q.id,
#                 'quarter_name': q.quarter_name
#             })
#         return quarters
#
#     def post(self, request, *args, **kwargs):
#         self.organization = self.get_organization(request.user)
#         self.quarter_period = self.organization.get_current_quarter()
#
#         self.allowed_edit = self.is_allowed_edit(self.quarter_period)
#
#         if 'export_perform_reward' in request.POST:
#             quarter_id = int(request.POST.get('quarter_id', 0))
#             if quarter_id:
#                 try:
#                     to_export_quarter = QuarterPeriod.objects.get(organization=self.organization, id=quarter_id)
#
#
#                     company_info = {
#                         'name': self.organization.name,
#                         'logo': self.organization.get_logo(),
#                         'date': format(datetime.date.today(), 'd-m-Y'),
#                         'quarter_period': self.quarter_period.quarter_name
#                     }
#
#                     results = []
#
#                     emps = ReportScoreFinal.objects.filter(organization=self.organization.id,
#                                                  quarter_period=to_export_quarter.id)
#                     count = 0
#                     for emp in emps:
#                         emp_data = [
#                                   emp.employee_code,
#                                   emp.employee_name,
#                                   emp.email,
#                                   emp.kpi_percent,
#                                   emp.net_salary,
#                                   emp.start_rewarding,
#                                   emp.end_rewarding,
#                                   emp.max_rewarding,
#                                   emp.final_reward,
#                                   ]
#                         results.append(emp_data)
#                         count += 1
#                     file_name = u'Performance Reward [%s]' % (unsigned_vi(to_export_quarter.quarter_name),)
#                     headers = [u'Employee Id', u'Name', 'Email', 'Performance Score', 'NET Salary', 'From', 'To', 'Personal bonus (%/salary)', 'Final Bonus']
#                     widths = [15, 30, 30, 15, 15, 15, 15, 15, 15]
#                     return ReportTemplate(results, company_info, widths=widths, output_name=file_name, headers=headers)
#                 except Exception as e:
#                     raise  Exception(str(e))
#                     # raise QuarterPeriod.DoesNotExist
#             else:
#                 # response error to client
#                 raise QuarterPeriod.DoesNotExist
#
#
#
#         if self.allowed_edit:
#             if request.POST.get('action', False) == 'update_employee_field':
#                 return self.update_employee_field(request)
#             elif request.POST.get('action', False) == 'update_global_field':
#                 return self.update_global_field(request)
#             elif request.POST.get('action', False) == 'save_final_rewards':
#                 return self.save_employee_final_rewards(request)
#         else:
#             response = {'status': "error", 'msg': "Server error!"}
#             return HttpResponse(json.dumps(response), content_type="application/json")
#
#         response = {'status': "error", 'msg': "Server error!"}
#         return HttpResponse(json.dumps(response), content_type="application/json")
#     def is_allowed_edit(self, quarter_period):
#         curr_quarter_period = self.organization.get_current_quarter()
#         if curr_quarter_period.id != quarter_period.id:
#             return False
#         else:
#             return True
#
#     def calcalate_est_reward(self, emp):
#         try:
#             est_reward = 0
#             if emp.kpi_percent >= emp.end_rewarding:
#                 est_reward = (emp.max_rewarding / 100.0) * emp.net_salary
#             else:
#                 if emp.kpi_percent - emp.start_rewarding < 0:
#                     est_reward = 0
#                 else:
#                     est_reward = float((emp.kpi_percent - emp.start_rewarding) / (emp.end_rewarding - emp.start_rewarding)) * (emp.max_rewarding / 100.0) * emp.net_salary
#
#             emp.est_reward = round(est_reward)
#         except Exception as e:
#             return 0
#
#         return emp.est_reward
#     def calculate_emps_est_reward(self):
#         emps = ReportScoreFinal.objects.filter(organization=self.organization.id,
#                                              quarter_period=self.quarter_period.id)
#         for emp in emps:
#             self.calcalate_est_reward(emp)
#             emp.save()
#
#     def calculate_final_reward(self, emp):
#         final_reward = 0
#         return final_reward
#     def calculate_total_estimated_reward(self):
#         try:
#             total_estimated_reward = ReportScoreFinal.objects.filter(organization=self.organization.id,
#                                                  quarter_period=self.quarter_period.id).sum('est_reward')
#             return round(total_estimated_reward)
#         except Exception as e:
#             return 0
#
#     def calculate_reward_cap_amount(self, total_monthly_salary):
#         try:
#             reward_cap_amount = round((self.quarter_period.reward_cap / 100.00) * total_monthly_salary)
#             return reward_cap_amount
#         except Exception as e:
#             return 0
#     def calculate_reward_cap(self, reward_cap_amount, total_monthly_salary):
#         try:
#             reward_cap = round((reward_cap_amount / total_monthly_salary) * 100, 2);
#             return reward_cap
#         except Exception as e:
#             return 0
#     def calculate_total_monthly_salary(self):
#         try:
#             total_monthly_salary = ReportScoreFinal.objects.filter(organization=self.organization.id,
#                                                  quarter_period=self.quarter_period.id).sum('net_salary')
#             return round(total_monthly_salary)
#         except Exception as e:
#             return 0
#
#     def update_employee_field(self, request):
#         if not request.POST.get('employee_id'):
#             return HttpResponse("ID update is missed.")
#
#         try:
#             employee = ReportScoreFinal.objects.get(id=request.POST['employee_id'],
#                                                     organization=self.organization.id)
#         except:
#             return HttpResponse("Record is not exists")
#
#         update = False
#
#         # employee settings
#         if request.POST.get('field', False) == 'net_salary':
#             employee.net_salary = round(float(request.POST.get('value', 0)))
#             update = True
#         elif request.POST.get('field', False) == 'start_rewarding':
#             employee.start_rewarding = round(float(request.POST.get('value', 0)), 2)
#             if employee.end_rewarding != None and employee.start_rewarding > employee.end_rewarding:
#                 response = {'status': "error", 'msg': "start performance-score must less than end performance-score!"}
#                 update = False
#             elif employee.start_rewarding < 0:
#                 response = {'status': "error", 'msg': "start performance-score must greater or equal 0"}
#                 update = False
#             else:
#                 update = True
#         elif  request.POST.get('field', False) == 'end_rewarding':
#             employee.end_rewarding = round(float(request.POST.get('value', 0)), 2)
#
#             if employee.start_rewarding != None and employee.start_rewarding > employee.end_rewarding:
#                 response = {'status': "error", 'msg': "end performance-score must greater than start performance-score!"}
#                 update = False
#             elif employee.start_rewarding < 0:
#                 response = {'status': "error", 'msg': "end performance-score must greater or equal 0"}
#                 update = False
#             else:
#                 update = True
#         elif  request.POST.get('field', False) == 'max_rewarding':
#             employee.max_rewarding = round(float(request.POST.get('value', 0)), 2)
#             # employee.bind_global=0
#             update = True
#         elif  request.POST.get('field', False) == 'bind_global':
#             employee.bind_global = int(request.POST.get('value', 0))
#             if employee.bind_global:
#                 employee.bind_global = 1
#                 employee.start_rewarding = self.quarter_period.start_rewarding
#                 employee.end_rewarding = self.quarter_period.end_rewarding
#                 employee.max_rewarding = self.quarter_period.max_rewarding
#
#             else:
#                 employee.bind_global = 0
#             update = True
#
#
#         if update:
#             try:
#                 self.quarter_period.unsaved = True
#                 self.quarter_period.save()
#
#                 self.calcalate_est_reward(employee)
#                 employee.save()
#                 emp_json = employee.to_json()
#                 # return HttpResponse('ok')
#                 response = {'status':'ok', 'emp':json.loads(emp_json)}
#                 return HttpResponse(json.dumps(response), content_type="application/json")
#             except Exception as e:
#                 print 'error'
#         else:
#             return HttpResponse(json.dumps(response), content_type="application/json")
#     def update_global_field(self, request):
#         update = False
#         response = {'status': "error", 'msg':'Something error!'}
#         # global settings
#         if request.POST.get('field', False) == 'reward_cap':
#             self.quarter_period.reward_cap = round(float(request.POST.get('value')), 2)
#             update = True
#         elif request.POST.get('field', False) == 'reward_cap_amount':
#             # quarter_period.reward_cap = float(request.POST.get('value'))
#             # calculate total salary
#             # --> reward_cap
#             total_monthly_salary = self.calculate_total_monthly_salary()
#             reward_cap_amount = round(float(request.POST.get('value')))
#             self.quarter_period.reward_cap = self.calculate_reward_cap(reward_cap_amount, total_monthly_salary)
#             update = True
#
#
#         elif request.POST.get('field', False) == 'start_rewarding':
#             old_start_rewarding = self.quarter_period.start_rewarding
#             end_end_rewarding = self.quarter_period.end_rewarding
#
#             self.quarter_period.start_rewarding = round(float(request.POST.get('value')))
#             if self.quarter_period.end_rewarding != None and self.quarter_period.start_rewarding > self.quarter_period.end_rewarding:
#                 response = {'status': "error", 'msg': "start performance-score must less than end performance-score!"}
#                 update = False
#             elif self.quarter_period.start_rewarding < 0:
#                 response = {'status': "error", 'msg': "start performance-score must greater or equal 0"}
#                 update = False
#             else:
#                 emps = ReportScoreFinal.objects.filter(MQ(organization=self.organization.id) and
#                                             MQ(quarter_period=self.quarter_period.id) and
#                                              MQ(bind_global=1) | MQ(bind_global=None)
#                                              ).update(set__start_rewarding=self.quarter_period.start_rewarding)
#                 update = True
#
#         elif request.POST.get('field', False) == 'end_rewarding':
#             self.quarter_period.end_rewarding = round(float(request.POST.get('value')))
#             if self.quarter_period.start_rewarding != None and self.quarter_period.start_rewarding > self.quarter_period.end_rewarding:
#                 response = {'status': "error", 'msg': "end performance-score must greater than start performance-score!"}
#                 update = False
#             elif self.quarter_period.start_rewarding < 0:
#                 response = {'status': "error", 'msg': "end performance-score must greater or equal 0"}
#                 update = False
#             else:
#                 emps = ReportScoreFinal.objects.filter(MQ(organization=self.organization.id) and
#                                             MQ(quarter_period=self.quarter_period.id) and
#                                              MQ(bind_global=1) | MQ(bind_global=None)
#                                              ).update(set__end_rewarding=self.quarter_period.end_rewarding)
#                 update = True
#
#
#         elif request.POST.get('field', False) == 'max_rewarding':
#             self.quarter_period.max_rewarding = round(float(request.POST.get('value')))
#             # update all employees for new max_rewarding
#             emps = ReportScoreFinal.objects.filter(MQ(organization=self.organization.id) and
#                                             MQ(quarter_period=self.quarter_period.id) and
#                                              MQ(bind_global=1) | MQ(bind_global=None)
#                                              ).update(set__max_rewarding=self.quarter_period.max_rewarding)
#             update = True
#
#         if update:
#             try:
#                 self.quarter_period.unsaved = True
#                 self.quarter_period.save()
#                 self.calculate_emps_est_reward()
#                 emps = ReportScoreFinal.objects.filter(organization=self.organization.id,
#                                              quarter_period=self.quarter_period.id).to_json()
#
#                 quarter_period_data = {
#                     'start_rewarding': self.quarter_period.start_rewarding,
#                     'end_rewarding': self.quarter_period.end_rewarding,
#                     'max_rewarding': self.quarter_period.max_rewarding,
#                     'reward_cap': self.quarter_period.reward_cap,
#                     'quarter_period': self.quarter_period.id,
#                     'allowed_edit':self.allowed_edit,
#                     'unsaved':self.quarter_period.unsaved,
#                     'quarters_year':self.quarter_period.year,
#                 }
#                 response = {'status':'ok', 'quarter_period':quarter_period_data, 'employees':json.loads(emps)}
#                 return HttpResponse(json.dumps(response), content_type="application/json")
#             except Exception as e:
#                 print 'error'
#         else:
#             return HttpResponse(json.dumps(response), content_type="application/json")
#     def save_employee_final_rewards(self, request):
#         employees = json.loads(request.POST.get('employees'))
#         employees = ReportScoreFinal.objects.filter(organization=self.organization.id,
#                                              quarter_period=self.quarter_period.id)
#         total_monthly_salary = self.calculate_total_monthly_salary()
#         total_estimated_reward = self.calculate_total_estimated_reward()
#         reward_cap_amount = self.calculate_reward_cap_amount(total_monthly_salary)
#         reward_over_ratio = (total_estimated_reward / float(reward_cap_amount)) if reward_cap_amount else 0
#         # update final-reward for every employee
#         try:
#             for emp in employees:
#                 if reward_over_ratio <= 1:
#                     emp.final_reward = emp.est_reward
#                 else:
#                     emp.final_reward = round(emp.est_reward / reward_over_ratio)
#
#                 emp.save()
#
#             self.quarter_period.unsaved = False
#             self.quarter_period.save()
#
#             emps = ReportScoreFinal.objects.filter(organization=self.organization.id,
#                                              quarter_period=self.quarter_period.id).to_json()
#             quarter_period_data = {
#                     'start_rewarding': self.quarter_period.start_rewarding,
#                     'end_rewarding': self.quarter_period.end_rewarding,
#                     'max_rewarding': self.quarter_period.max_rewarding,
#                     'reward_cap': self.quarter_period.reward_cap,
#                     'quarter_period': self.quarter_period.id,
#                     'allowed_edit':self.allowed_edit,
#                     'unsaved':self.quarter_period.unsaved,
#                     'quarters_year':self.quarter_period.year,
#                 }
#             response = {'status':'ok', 'quarter_period':quarter_period_data, 'employees':json.loads(emps)}
#
#
#
#         except Exception as e:
#             print 'save employee final rewards error'
#             response = {'status': "error", 'msg': "Server error! Try reload page!"}
#
#         return HttpResponse(json.dumps(response), content_type="application/json")

