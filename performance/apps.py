# !/usr/bin/python
# -*- coding: utf-8 -*-
from django.apps.config import AppConfig
from modeltranslation.translator import translator
from performance import translation
from django.db.models.signals import post_save


class PerformanceConfig(AppConfig):
    name = 'performance'

    def ready(self):
        JobTitle = self.get_model('JobTitle')
        KPI = self.get_model('KPI')
        Competency = self.get_model('Competency')
        DescriptionCell = self.get_model('DescriptionCell')
        BscDepartment = self.get_model('BscDepartment')
        BscJobTitle = self.get_model('BscJobTitle')
        BscKPI = self.get_model('BscKPI')
        CompetencyLib = self.get_model('CompetencyLib')
        QuarterPeriod = self.get_model('QuarterPeriod')
        
        from .signals import set_current_quarter_cache
        
        # Don't remove this
        post_save.connect(set_current_quarter_cache, QuarterPeriod)

        translator.register(JobTitle, translation.JobTranslationOptions)
        translator.register(Competency, translation.CompetencyTranslationOptions)
        translator.register(DescriptionCell, translation.DescriptionCellTranslationOptions)
        translator.register(BscDepartment, translation.BscDepartmentTranslationOptions)
        translator.register(BscJobTitle, translation.BscJobTitleTranslationOptions)
        translator.register(BscKPI, translation.BscKpiTranslationOptions)
        translator.register(CompetencyLib, translation.CompetencyLibTranslationOptions)

        from actstream import registry
        registry.register(self.get_model('KPI'))
        # registry.register(self.get_model('User'))
        # registry.register(self.get_model('Organization'))
