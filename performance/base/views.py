#!/usr/bin/python
# -*- coding: utf-8 -*-
import json

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.db import transaction
from django.http.response import HttpResponse
from django.utils.decorators import method_decorator
from django.utils.translation import gettext
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import View

from performance.logger import KPILogger
from performance.models import KPI
from performance.services.common import PerformanceApi
# from silk.profiling.profiler import silk_profile
from user_profile.services.profile import send_new_password_to_email


class PerformanceBaseView(View):
    @method_decorator(login_required)
    @method_decorator(csrf_exempt)
    # @method_decorator(silk_profile(name='View Blog Post'))
    def dispatch(self, *args, **kwargs):
        return super(PerformanceBaseView, self).dispatch(*args, **kwargs)

    def get_organization(self, user):
        org = PerformanceApi.get_organization(user)
        # org = user.profile.get_organization()
        if not org:
            raise PermissionDenied
        return org

    # FUCK
    def get_current_quarter(self, user):
        # type: (object) -> object

        organization = self.get_organization(user)
        return organization.get_current_quarter()

    # ENDFUCK

    # Wrongly named method. Need to check. as method returns profile even if organization is none.
    def get_user_organization(self, user):
        user_org = PerformanceApi.get_user_organization(user)
        if not user_org:
            raise PermissionDenied
        return user_org

    # not used
    def send_new_password(self, email, name, password):#pragma: no cover

        return send_new_password_to_email(email, name, password)

        # THIS IS NOT THE PLACE FOR THIS FUNCTION
        # message = render_to_string('mail/send_new_password.html', {
        #     'full_name': name,
        #     'new_password': password,
        #     'email': email
        # })
        # msg = EmailMessage(u'Cloudjet password information',
        #                    message,
        #                    'Cloudjet <hello@cloudjetsolutions.com>', [email],  ['hello@cloudjetsolutions.com'])
        # msg.content_subtype = "html"
        # msg.send(fail_silently=True)


def ajax_request(function):
    def wrapper(request, *args, **kwargs):
        return function(request, *args, **kwargs)

    return wrapper

""" unused, Toan comment """
# class CopyPerformanceBaseView(View):
#     model = KPI
#
#     @method_decorator(csrf_exempt)
#     def dispatch(self, *args, **kwargs):
#         return super(CopyPerformanceBaseView, self).dispatch(*args, **kwargs)
#
#     def update_kpi(self, kpi):
#         if isinstance(kpi, KPI):
#             if kpi.parent_id:
#                 kpi.refer_to_id = kpi.parent_id
#                 kpi.owner_email = kpi.user.email
#                 kpi.save()
#             else:
#                 kpi.owner_email = kpi.user.email
#                 kpi.save()
#                 children = kpi.get_children()
#                 for child in children:
#                     self.update_kpi(child)
#
#     @method_decorator(transaction.atomic)
#     def post(self, request, *args, **kwargs):
#         organization = request.user.get_profile().get_organization()
#         quarter_period = organization.get_current_quarter()
#
#         user_id = request.POST.get('user_id')
#         obj_id = request.POST.get('obj_id')
#         parent_id = request.POST.get('parent_id', None)
#         copy_all = request.POST['copy_all']
#
#         user = User.objects.get(pk=user_id)
#         try:
#             obj = self.model.objects.get(pk=obj_id)
#         except:
#             return HttpResponse("%s doesn't exist." % (gettext(self.model.__name__)))
#         if not parent_id and copy_all == 'true':
#             objs = self.model.objects.filter(user=obj.user, quarter_period=quarter_period, parent=None)
#             for obj in objs:
#                 temp = obj.clone_to_user(user, quarter_period)
#                 self.update_kpi(temp)
#                 name = ''
#                 try:
#                     name = obj.get_name()
#                 except:
#                     pass
#
#                 KPILogger(request.user, temp, 'new', name).start()
#         else:
#             new_obj = obj.clone_to_user(user, quarter_period)
#             name = ''
#             try:
#                 name = new_obj.get_name()
#             except:
#                 pass
#             KPILogger(request.user, new_obj, 'new', name).start()
#
#             if parent_id:
#                 parent = self.model.objects.get(pk=parent_id)
#                 new_obj.parent = parent
#
#             new_obj.save()
#
#             self.update_kpi(new_obj)
#         return HttpResponse('ok')


# class CurrentPerformanceBaseView(View):
#     model = KPI
#
#     @method_decorator(csrf_exempt)
#     def dispatch(self, *args, **kwargs):
#         return super(CurrentPerformanceBaseView, self).dispatch(*args, **kwargs)
#
#     def post(self, request, *args, **kwargs):
#         organization = request.user.get_profile().get_organization()
#         quarter_period = organization.get_current_quarter()
#
#         user_id = request.POST.get('user_id')
#         objs = self.model.objects.filter(user_id=user_id,
#                                          quarter_period=quarter_period,
#                                          parent=None)
#         obj_list = []
#         obj_list.append({'id': '', 'name': ''})
#         for obj in objs:
#             item = {
#                 'id': obj.id,
#                 'name': obj.name
#             }
#             obj_list.append(item)
#
#         return HttpResponse(json.dumps(obj_list, indent=4),
#                             content_type='application/json')


class EmailResetPassword(View):
    @method_decorator(ajax_request)
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(EmailResetPassword, self).dispatch(*args, **kwargs)
    #
    # def send_email(self, email, name, password):
    #     # MEO BIET TAI SAO LAI DUPLICATE CODE O DAY @@
    #     user = User.objects.get(email=email)
    #
    #     login_link_no_pass = user.profile.get_login_link()
    #     login_url = settings.DOMAIN + "/login/"
    #     message = render_to_string('mail/send_new_password.html',
    #                                {'full_name': name,
    #                                 'new_password': password,
    #                                 'login_link_no_pass': login_link_no_pass,
    #                                 'login_url': login_url,
    #
    #                                 'email': email})
    #
    #     msg = EmailMessage(u'Cloudjet KPI password', message,
    #                        from_email, [email, ])
    #     msg.content_subtype = "html"
    #     msg.send()
