# !/usr/bin/python
# -*- coding: utf-8 -*-
import datetime
import time
import threading
import traceback

from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.contrib.auth.models import User
from django.core.cache import cache
from django.core.mail import mail_admins
from django.core.mail.message import EmailMessage
from django.db import transaction
from django.template.loader import render_to_string

from company.models import Organization
from elms.settings import DOMAIN
from performance.db_models.competency import Competency
from performance.db_models.log import KpiLog, AlignLog
from performance.db_models.strategy_map import GroupKPI, StrategyMap
from performance.models import KPI


from user_profile.services.profile import send_mail_with_perm
from user_profile.services.user import set_new_password
from utils import unsigned_vi, notify_slack, reload_model_obj
from utils.caching_name import CREATING_ASSESSMENT, CREATING_ASSESSMENT_ORG, CREATING_ASSESSMENT_QUARTER, \
    CREATING_ASSESSMENT_ORG_CLONEKPI_PROGRESS, CREATING_ASSESSMENT_ORG_TOTAL_USER, CREATING_ASSESSMENT_ORG_QUARTER, \
    CREATING_ASSESSMENT_ORG_CLONEMAP_PROGRESS, CREATING_ASSESSMENT_ORG_ALIGNKPI_PROGRESS
from utils.common import set_redis, remove_redis
from user_profile.models import Profile
import logging
logger = logging.getLogger('django')

class Cloner(object):
    def __init__(self, organization, prev_quarter_period, quarter_period, *args, **kwargs):

        self.organization = organization
        self.prev_quarter_period = prev_quarter_period
        self.quarter_period = quarter_period

        self.message = None
        self.successful_cloning = False
        self.successful_cloning_kpi = False

        self.total_user=0 # use for clone-kpi & align-kpi progress
        self.total_map=0
        self.index_clone_kpi=0
        self.index_align_kpi = 0
        self.index_clone_map = 0

    def run(self):
        logger.info('Cloner running')
        self.pre_clone()
        self.clone()
        self.post_clone()
        logger.info('Cloner finish')

    def pre_clone(self):
        # we integrated this to update_progress_status method
        # self.mark_system_as_in_cloning_process()
        logger.info('Pre_clone')
        self.update_progress_status()



    def post_clone(self):
        logger.info('Post_clone')
        if self.successful_cloning is True:
            self.organization.set_default_deadlines(self.quarter_period)
            self.quarter_period.mark_quarter_ready()

            self.mark_system_as_finish_cloning_process()
        else:
            # do other jobs here when failing to clone
            pass


        self.notify_admin_successful_cloning_status()




    def clone(self):
        '''
                    Note: we move all non-business code out of the transaction
                    So, we can avoid so much side effects from non-business code.
                    For ex: we call send_mail function inside transaction and then the function is errored,
                    then the transaction may be rollback all regardless of we already successfully cloned.
                '''
        try:
            sid = transaction.savepoint()

            ##### 3 PHASES CLONING####################################
            self._clone_kpis_phase()
            self._align_kpis_phase()
            # we commit savepoint here because clone-phase and align-phase should go together
            # if the follow phase is failed, we can re-run without breaking kpi data
            transaction.savepoint_commit(sid)
            self.successful_cloning_kpi = True

            self._clone_maps_phase()
            #########################################################



            self.successful_cloning = True
            logger.info("****************** Create new performance review succeed ******************")

        except:
            self.successful_cloning = False
            self.message = traceback.format_exc()
            logger.info('Clone error:{}'.format(self.message,))
            transaction.savepoint_rollback(sid)

            logger.info("****************** Create new performance review failed ******************")

    def _clone_kpis_phase(self):
        ps = Profile.objects.select_related('user').filter(organization=self.organization)
        total_user = ps.count()
        self.total_user=total_user
        self.index_clone_kpi=0

        for p in ps:
            self.index_clone_kpi += 1
            self.update_progress_status()

            # we currently not maintain and distribute Competency, so temporary disable this.
            # if False and self.organization.enable_competency:
            #     Competency.objects.filter(user=p.user, quarter_period=self.quarter_period).delete()
            #     p.copy_competency_of_previous_quarter(self.prev_quarter_period,
            #                                                 self.quarter_period)
            KPI.objects.filter(user=p.user, quarter_period=self.quarter_period).delete()
            logger.info("---------> Copying KPI {}".format(p.user.username, ))
            p.copy_kpi_of_previous_quarter(self.prev_quarter_period,
                                                 self.quarter_period, True,
                                                 update_prefix_id=False)

    def _align_kpis_phase(self):
        logger.info('_align_kpis_phase')
        # align all  kpi
        # ensure alignlog of new quater in DB is clean
        AlignLog.objects.filter(quarter_period=self.quarter_period).delete()
        self.align_kpis()


    def _clone_maps_phase(self):
        self.clone_strategy_map()
        # clone_strategy_map(prev_quarter_period=self.prev_quarter_period,
        #                    quarter_period=self.quarter_period,
        #                    organization=self.organization)



    def update_progress_status(self):
        # use this function for avoiding duplicate code when set the same cache key for different cache engines
        def set_cache(key, value, expire):
            cache.set(key, value, expire)
            set_redis(key, value, expire)

        '''
                Note: we do not want any side-exceptions interfer to/beak the main business
                So, we add try/catch here

        '''
        try:
            # this marking may be redundant, because we prefer to make sure that the system is in cloning process
            self.mark_system_as_in_cloning_process(self.organization)

            # general caching for creating new assessment
            set_cache(CREATING_ASSESSMENT_ORG_TOTAL_USER.format(self.organization.id, ), self.total_user,
                      120 * 1)
            set_cache(CREATING_ASSESSMENT_ORG.format(self.organization.id), self.organization.id, 60 * 2)
            set_cache(CREATING_ASSESSMENT_QUARTER.format(self.quarter_period.id), True, 60 * 2)
            set_cache(CREATING_ASSESSMENT_ORG_QUARTER.format(self.organization.id), self.quarter_period.id, 60 * 10)


            # cache for cloning progress
            set_cache(CREATING_ASSESSMENT_ORG_CLONEKPI_PROGRESS.format(self.organization.id, ),
                      '{}/{}'.format(self.index_clone_kpi, self.total_user), 120 * 1)

            set_cache(CREATING_ASSESSMENT_ORG_ALIGNKPI_PROGRESS.format(self.organization.id, ),
                      '{}/{}'.format(self.index_align_kpi, self.total_user), 120 * 1)

            set_cache(CREATING_ASSESSMENT_ORG_CLONEMAP_PROGRESS.format(self.organization.id, ),
                      '{}/{}'.format(self.index_clone_map, self.total_map), 60 * 2)


        except Exception as ex:
            print traceback.print_exc()
            logger.info('Cannot set cache: {}'.format(str(ex)))

    def align_kpis(self):
        def align_kpi_recusive(profile):

            self.update_progress_status()
            self.index_align_kpi += 1

            align_kpis_single_user(profile.user, self.prev_quarter_period, self.quarter_period, self.organization)

            subs_profile = profile.get_subordinate()
            for p in subs_profile:
                align_kpi_recusive(p)

        # self.organization=reload_model_obj(self.organization)
        # ceo=reload_model_obj(self.organization.ceo)
        logger.info('ceo: {}'.format(self.organization.ceo.id,))
        # time.sleep(20)
        ceo_profile = self.organization.ceo.get_profile()
        align_kpi_recusive(ceo_profile)


    def clone_strategy_map(self):
        logger.info("---------> Copying strategy map")

        StrategyMap.objects.filter(organization=self.organization, quarter_period=self.quarter_period).delete()
        maps = StrategyMap.objects.filter(organization=self.organization,
                                          quarter_period=self.prev_quarter_period).order_by('director')

        self.total_map = maps.count()
        for map_obj in maps:
            self.index_clone_map += 1
            try:
                # this marking may be redundant, be we prefer to make sure that the system is in cloning process
                self.update_progress_status()

                start_clone_strategy_map(s_map=map_obj, organization=self.organization, quarter_period=self.quarter_period)
            except Exception as ex:
                mail_admins('Cannot clone map when creating new assessment', str(ex))


    @classmethod
    def mark_system_as_in_cloning_process(cls, organization):
        '''
        We set this function as classmethod,
        because we want external call for marking system is in creating assessment before the job actual firing
        :param organization:
        :return:
        '''
        update_last_copy_time(organization)

    def mark_system_as_finish_cloning_process(self):
        # clear las_copy_time
        clear_last_copy_time(self.organization)


    def notify_admin_successful_cloning_status(self):
        extra_message=''
        if self.successful_cloning is True:
            subject = u"[{}] Successfully created new assessment.".format(self.organization.name, )
        else:
            if self.successful_cloning_kpi is True:
                extra_message='Successful cloned and aligned kpis'

            subject = u'[{}] Failed to create new assessment.'.format(self.organization.name, )

        if self.message is None:
            self.message=subject

        adjusted_message=u'Organization: {} \nDomain: {}\n{} \n{}'.format(self.organization.name, DOMAIN, self.message, extra_message)
        mail_admins(subject=subject, message=adjusted_message)

        channel_message=u'@tech {}'.format(adjusted_message,)
        notify_slack(settings.SLACK_DEV_CHANNEL, channel_message)


# we do not need this Thread-based class,
# because we can use `utils.threading_decorator.threading_or_worker` decorator to handle thread cases and other worker cases
# class PerformanceCloner(threading.Thread):
#
#     def __init__(self, organization,
#                  # prev_quarter_period,
#                  quarter_period, *args, **kwargs):
#         super(PerformanceCloner, self).__init__(*args, **kwargs)
#         self.organization = organization
#         self.prev_quarter_period = quarter_period.clone_from_quarter_period
#         self.quarter_period = quarter_period
#
#     def run(self):
#         start_cloner(organization=self.organization, quarter_period=self.quarter_period,
#                      prev_quarter_period=self.prev_quarter_period)


def start_cloner(organization, prev_quarter_period, quarter_period):
    thecloner = Cloner(organization, prev_quarter_period, quarter_period)
    thecloner.run()

def update_last_copy_time(organization):
    organization.last_copy_time = datetime.datetime.now()
    organization.save(update_fields=['last_copy_time',])
    # organization.save()
    print "Last updated: {0}".format(organization.last_copy_time, )

def clear_last_copy_time(organization):
    organization.last_copy_time = None
    organization.save(update_fields=['last_copy_time',])


def clone_group(groups, s_map_new, quarter_period):
    for group in groups:
        new_group = GroupKPI(map=s_map_new,
                             name=group.name,
                             category=group.category)
        new_group.save()
        for k in group.get_kpis():
            new_k = KPI.objects.filter(copy_from=k,
                                       quarter_period=quarter_period).first()
            if new_k:
                new_k.group_kpi = new_group
                new_k.save()
            # new_k.groupkpi_set.add(new_group)



# this function should be seperated as it is. It's presented as and unit code, and we can write test for this
def start_clone_strategy_map(s_map, organization, quarter_period):

    if s_map:

        new_map = StrategyMap.objects.filter(organization=organization,
                                                             director=s_map.director,
                                                             quarter_period=quarter_period).first()

        if not new_map:
            new_map = StrategyMap(organization=organization,
                                                 director=s_map.director,
                                                 quarter_period=quarter_period)

        new_map.year=quarter_period.year
        new_map.name=s_map.name,
        new_map.description=s_map.description

        new_map.save()

        group_financial = GroupKPI.objects.filter(map=s_map,
                                                  category='financial').order_by('id')
        clone_group(groups=group_financial, s_map_new=new_map, quarter_period=quarter_period)

        group_customer = GroupKPI.objects.filter(map=s_map,
                                                 category='customer').order_by('id')
        clone_group(groups=group_customer, s_map_new=new_map, quarter_period=quarter_period)

        group_internal = GroupKPI.objects.filter(map=s_map,
                                                 category='internal').order_by('id')
        clone_group(groups=group_internal, s_map_new=new_map, quarter_period=quarter_period)

        group_learning = GroupKPI.objects.filter(map=s_map,
                                                 category='learninggrowth').order_by('id')
        clone_group(groups=group_learning, s_map_new=new_map, quarter_period=quarter_period)


# def align_kpi(user, old_quarter, new_quarter, organization=None):
#     align_kpi_to_new_quarter(user, old_quarter, new_quarter, organization)


def set_caching(organization, quarter_period):
    """
    Purpose: to notify using that we can creating new quarter for them.
    """
    try:
        cache.set(CREATING_ASSESSMENT, organization.id, 60 * 2)  # DON't DELETE THIS
        cache.set(CREATING_ASSESSMENT_ORG.format(organization.id), organization.id, 60 * 2)  # DON't DELETE THIS
        cache.set(CREATING_ASSESSMENT_ORG_QUARTER.format(organization.id), quarter_period.id, 60 * 10)  # DON't DELETE THIS

        # set_redis(CREATING_ASSESSMENT_ORG.format(organization.id), organization.id, 60 * 2)  # DON't DELETE THIS
        # set_redis(CREATING_ASSESSMENT_QUARTER.format(quarter_period.id), True, 60 * 2)  # DON't DELETE THIS
        # set_redis(CREATING_ASSESSMENT_ORG_QUARTER.format(organization.id), quarter_period.id, 60 * 10)  # DON't DELETE THIS
    except Exception as ex:
        logger.info('Cannot set cache: {}'.format(str(ex)))

# since we do use cache for detecting system's creating state --> this function is useless
# def delete_caching(organization, quarter_period):
#     remove_redis(CREATING_ASSESSMENT_ORG.format(organization.id))
#     cache.delete(CREATING_ASSESSMENT_ORG.format(organization.id))
#

def align_kpi_to_new_quarter(user, old_quarter, new_quarter, organization):
    '''
    Log progress:
    create AlignLog if needed
    AlignLog.is_done = False
    '''

    print "*" * 60
    print user.email

    # todo: remove it
    set_caching(organization, new_quarter)

    align_kpis_single_user(user, old_quarter, new_quarter, organization)

    profile = user.get_profile()
    subs = profile.get_subordinate()
    update_last_copy_time(organization)

    for sub in subs:
        align_kpi_to_new_quarter(sub.user, old_quarter, new_quarter, organization)


def align_kpis_single_user(user, old_quarter, new_quarter, organization):
    align_log, is_created = AlignLog.objects.get_or_create(user=user, quarter_period=new_quarter)


    # count_alignlog = AlignLog.objects.filter(quarter_period = new_quarter).count()
    if align_log.is_done is not True:
    # return
        kpis = KPI.objects.filter(user=user, quarter_period=old_quarter,
                                  cascaded_from__isnull=False,
                                  parent=None).select_related('cascaded_from')

    # print "Align kpis user " + user.username
    for old in kpis:

        try:

            cascade_old = old.cascaded_from

            new_kpi = KPI.objects.filter(copy_from=old, quarter_period=new_quarter).order_by('-id').first()
            cascade_new = KPI.objects.filter(copy_from=cascade_old).first()

            if new_kpi and cascade_new:
                new_kpi.cascaded_from = cascade_new
                new_kpi.refer_to = cascade_new.parent
                new_kpi.owner_email = new_kpi.user.email

                cascade_new.refer_to = None
                cascade_new.assigned_to_id = cascade_old.assigned_to_id
                # cascade_new.save(add_queue = False )
                cascade_new.save(update_fields=['assigned_to_id', 'refer_to'], add_queue=False)
                # new_kpi.save(get_system_user()) # ERROR HERE: TypeError: save() takes exactly 0 arguments (1 given)
                new_kpi.save(update_fields=['cascaded_from_id', 'refer_to_id', 'owner_email'], add_queue=False)

            #####
            # update children_data
            #####

            from performance.services.children_data_services import get_children_data

            children_data = get_children_data(new_kpi.user, new_kpi)


            def update_kpi_id_children_data(elm):

                # 1. get _new_kpi, which is copy_from old KPI with ID: elm['kpi_id']
                _new_kpi = KPI.objects.filter(copy_from_id=elm['kpi_id'], quarter_period=new_quarter).order_by(
                    '-id').first()

                # 2. Update  elm['kpi_id'] with new kpi_id
                if _new_kpi:
                    elm['kpi_id'] = _new_kpi.id

                return elm


            new_kpi.children_data["children_weights"] = map(update_kpi_id_children_data, children_data)
            new_kpi.save()



        except Exception as e:

            # print str(e), traceback.print_exc()

            align_log.log += "*" * 60 + "\n"
            align_log.log += unicode(datetime.datetime.now()) + "\n"
            align_log.log += unicode(e)

            # notify_slack(settings.SLACK_CRONJOB_CHANNEL, unicode(e))
            # notify_slack(settings.SLACK_CRONJOB_CHANNEL, traceback.format_exc())

        '''
        Set log
        '''
        align_log.is_done = True
        align_log.save()


def clone_user(org, manager, domain_email, quarter_period,
               clone_manager, clone_org, clone_quarter_period):
    from user_profile.models import Profile
    # profiles = Profile.objects.filter(report_to=clone_manager)
    profiles = Profile.objects.filter(parent=clone_manager.profile)
    import re
    for p in profiles:

        _username = unsigned_vi(p.display_name).lower().replace(' ', '')
        username = re.sub(r'[^A-Za-z0-9\.\+_-]+', "", _username)

        if len(username) > 30:  # django limit username, firstname, lastname to 30 characters
            # random username
            username = username[:30]

        i = 0
        if len(username) <= 30:
            i = 30 - len(username)

        baseusername = username
        while User.objects.filter(username=username).exists():
            ulen = 30 - i
            username = baseusername[:ulen]
            if User.objects.filter(username=username).exists():
                uname = username
                for j in range(0, i * 10):
                    username = uname + str(j)
                    if not User.objects.filter(username=username).exists():
                        break
            i = i + 1

        print "duan", username
        user = User.objects.create(username=username,
                                   email=username + "@" + domain_email)

        profile = user.get_profile()
        # profile.report_to_id = manager.id
        profile.parent = manager.profile
        profile.display_name = p.display_name
        profile.title = p.title
        profile.current_job_title = p.current_job_title
        profile.short_term_career = p.short_term_career
        profile.short_term_development = p.short_term_development
        profile.long_term_career = p.long_term_career
        profile.long_term_development = p.long_term_development
        profile.is_deputy_manager = p.is_deputy_manager
        profile.user = user
        profile.organization = org
        profile.is_superuser = p.is_superuser
        profile.is_admin = p.is_admin
        profile.start_date = p.start_date
        profile.role_start = p.role_start
        profile.position = p.position
        profile.employee_code = p.employee_code
        profile.save()
        #         clone_uo = UserOrganization.objects.get(organization=clone_org,
        #                                                 user_id=p.user_id)
        #         uo = UserOrganization.objects.create(user=user,
        #                                              organization=org,
        #                                              is_superuser=clone_uo.is_superuser,
        #                                              is_admin=clone_uo.is_admin,
        #                                              start_date=clone_uo.start_date,
        #                                              role_start=clone_uo.role_start,
        #                                              position=clone_uo.position,
        #                                              employee_code=clone_uo.employee_code)
        #         uo.save()

        kpis = KPI.objects.filter(parent=None, user_id=p.user_id,
                                  quarter_period=clone_quarter_period)
        for kpi in kpis:
            kpi.clone_to_user(user, quarter_period)

        competencies = Competency.objects.filter(parent=None, user_id=p.user_id,
                                                 quarter_period=clone_quarter_period)
        for comp in competencies:
            comp.clone_to_user(user, quarter_period)

        clone_user(org, user, domain_email, quarter_period,
                   p.user, clone_org, clone_quarter_period)


class CompanyCloner(threading.Thread):

    def __init__(self, clone_ceo, clone_org, company_name, ceo_email, password, *args, **kwargs):
        self.clone_ceo = clone_ceo
        self.clone_org = clone_org
        self.company_name = company_name
        self.ceo_email = ceo_email
        self.domain_email = ceo_email.split('@')[1]
        self.password = password
        threading.Thread.__init__(self, *args, **kwargs)

    def run(self):
        try:
            sid = transaction.savepoint()
            ceo_id = self.clone_ceo.id
            clone_ceo = User.objects.get(pk=ceo_id)
            ceo_profile = clone_ceo.get_profile()
            username = unsigned_vi(self.clone_ceo.get_profile().display_name).lower().replace(' ', '')

            try:
                user = User.objects.get(email=self.ceo_email)
            except:
                user = None

            if not user:
                if User.objects.filter(username=username).exists():
                    count = 1
                    while (True):
                        temp = username + str(count)
                        if not User.objects.filter(username=temp).exists():
                            username = temp
                            break
                        count += 1

                user = User.objects.create(username=username,
                                           email=self.ceo_email)
                # user.set_password(self.password)
                # user.profile.set_new_password(self.password)

                user, p = set_new_password(user.id, self.password)
                user.save()

            slug = unsigned_vi(self.company_name).lower().replace(' ', '')
            domain = slug + ".cloudjetsolutions.com"

            organization = Organization.objects.create(user=user,
                                                       slug=slug,
                                                       domain=domain,
                                                       name=self.company_name,
                                                       enable_performance=True,
                                                       ceo=user,
                                                       industry_id=self.clone_org.industry_id,
                                                       trial=True)

            profile = user.get_profile()
            profile.display_name = ceo_profile.display_name
            profile.title = ceo_profile.title
            profile.current_job_title = ceo_profile.current_job_title
            profile.short_term_career = ceo_profile.short_term_career
            profile.short_term_development = ceo_profile.short_term_development
            profile.long_term_career = ceo_profile.long_term_career
            profile.long_term_development = ceo_profile.long_term_development
            profile.is_deputy_manager = ceo_profile.is_deputy_manager
            #             clone_uo = UserOrganization.objects.get(organization=self.clone_org,
            #                                                     user_id=ceo_id)

            profile.is_superuser = True
            profile.is_admin = True
            profile.start_date = ceo_profile.start_date
            profile.role_start = ceo_profile.role_start
            profile.position = ceo_profile.position
            profile.employee_code = ceo_profile.employee_code  # no need
            profile.save()

            quarter_period = organization.get_current_quarter()
            quarter_period.is_ready = True
            quarter_period.save()
            clone_quarter_period = self.clone_org.get_current_quarter()

            kpis = KPI.objects.filter(parent=None, user=clone_ceo,
                                      quarter_period=clone_quarter_period)
            for kpi in kpis:
                kpi.clone_to_user(user, quarter_period)

            competencies = Competency.objects.filter(parent=None, user=clone_ceo,
                                                     quarter_period=clone_quarter_period)
            for comp in competencies:
                comp.clone_to_user(user, quarter_period)

            clone_user(organization, user, self.domain_email, quarter_period,
                       clone_ceo, self.clone_org, clone_quarter_period)
            transaction.savepoint_commit(sid)
            print "****************** Clone succeed ******************"
        except:  # pragma: no cover
            transaction.savepoint_rollback(sid)
            traceback.print_exc()
            # print "****************** Clone failed ******************"
