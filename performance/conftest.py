# !/usr/bin/python
# -*- coding: utf-8 -*-
import json
import time

from datetime import datetime

from datetime import timedelta

from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

from authorization.models import RuleTable, add_roles
from authorization.rules import SETTINGS__VIEW_ORG_CHART, KPI__EDITING

from memoize import delete_memoized_verhash
from performance.models import KPI
from user_profile.models import Profile
import pytest
import logging
from performance.services import KPIServices, create_child_kpi
import random
from company.models import Position
from elms.settings import CELERY_RESULT_BACKEND, BROKER_URL
from elms import celery_app as _celery_app
from utils import random_string
from utils.threading_decorator import threading_or_worker

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger()
from celery import shared_task
# from faker import Factory

# faker = Factory.create()
from user_profile.models import get_organization_by_user
#
# @pytest.fixture(autouse=True)
# def enable_db_access(db):
#     pass
#
# @pytest.fixture(autouse=True)
# def system_user(db):
#     logger.info("here#" + "#"*100)
#     from user_profile.services.user import get_system_user
#
#     system_user = get_system_user()


# system_user = get_system_user()
@pytest.mark.django_db(transaction=False)
@pytest.fixture()
def parent_kpi(userA, organization):
    kpi = KPI()
    kpi.user = userA

    kpi.name = "Parent KPI"

    current_quarter = organization.get_current_quarter()
    kpi.quarter_period = current_quarter

    kpi.save()

    return kpi


@pytest.mark.django_db(transaction=False)
@pytest.fixture()
def kpi_userA_manager(userA_manager, organization):
    # kpi for userA_manager
    kpi = KPI()
    kpi.user = userA_manager

    kpi.name = "Parent KPI from userA_manager"

    current_quarter = organization.get_current_quarter()
    kpi.quarter_period = current_quarter

    kpi.save()
    return kpi


@pytest.mark.django_db(transaction=False)
@pytest.fixture()
def parent_kpi2(userA):
    kpi = KPI()
    kpi.user = userA

    kpi.name = "Parent KPI 2"

    kpi.save()

    return kpi


@pytest.mark.django_db(transaction=False)
@pytest.fixture()
def kpi(userA, parent_kpi, organization):
    kpi = KPI()
    kpi.name = "Child KPI"
    kpi.quarter_period = organization.get_current_quarter()
    kpi.user = userA

    kpi.parent = parent_kpi
    kpi.refer_to = parent_kpi

    kpi.target = 100
    kpi.operator = ">="

    kpi.save()

    return kpi


_index = 1


class ConfigReview:
    MAX_LEVEL = 3
    AMOUNT_KPI_PER_PERSON = 8
    MAX_SUBORDINATE = 4

    class KPI_1:
        QUARTER_ONE_TARGET = 10
        QUARTER_TWO_TARGET = 10
        QUARTER_THREE_TARGET = 10
        QUARTER_FOUR_TARGET = 10

    class KPI_2:
        QUARTER_ONE_TARGET = 15
        QUARTER_TWO_TARGET = 15
        QUARTER_THREE_TARGET = 15
        QUARTER_FOUR_TARGET = 15


# Note: Hong disable fixture this to improve speed.
#       We should only use this for certain benchmarking tests, not all tests
#
# @pytest.mark.django_db(transaction=False)
# @pytest.fixture()
def organization_chart(ceo, organization_standard, client_ceo):
    max_level = ConfigReview.MAX_LEVEL
    services = KPIServices()
    kpis = []
    for i in range(0, ConfigReview.AMOUNT_KPI_PER_PERSON):
        kpi = services.create_kpi(creator=ceo, name="KPI | user: {} | user index: {}".format(ceo.id, _index),
                                  user=ceo, quarter_period=organization_standard.get_current_quarter(),
                                  quarter_one_target=ConfigReview.KPI_1.QUARTER_ONE_TARGET,
                                  quarter_two_target=ConfigReview.KPI_1.QUARTER_TWO_TARGET,
                                  quarter_three_target=ConfigReview.KPI_1.QUARTER_THREE_TARGET,
                                  quarter_four_target=ConfigReview.KPI_1.QUARTER_FOUR_TARGET)
        kpis.append(kpi)

    def create_new_subordinate(organization, manager_profile_id, level):
        global _index
        data = {'name': 'user test level {}'.format(level),
                'email': 'user_{}_level_{}@gmail.com'.format(_index, level),
                'manager': 'u{}'.format(manager_profile_id),
                'send_pass': 'true'
                }
        url = reverse('add_people')
        response = client_ceo.post(url, data)

        data = json.loads(response.content)
        assert response.status_code == 200
        assert data['id'] > 0
        _index = _index + 1
        return data['data']

    def create_new_sub_recursive(organization, manager_profile_id, max_level, kpis=[], level=1):
        if level > max_level:
            return

        logger.info("*"*30)
        logger.info("processing | level: {} | index: {} ".format(level, _index))
        logger.info("*"*30)

        for i in range(0, random.randint(1, ConfigReview.MAX_SUBORDINATE)):
            # ## Step 1: create new user
            create_new_subordinate(organization=organization,
                                   manager_profile_id=manager_profile_id,
                                   level=level)

        manager_profile = Profile.objects.get(id=manager_profile_id)
        subs = manager_profile.get_subordinate()
        for sub in subs:
            new_kpis = []
            count = 1
            for kpi in kpis:
                # ## Step 2: assign KPI from manager to new user
                name = 'New KPI Child {}'.format(count)
                new_kpi_refer_to = create_child_kpi(kpi, organization.get_current_quarter(),
                                                    name, manager_profile.user)
                new_kpi_refer_to = new_kpi_refer_to.assign_to(sub.user)

                assert new_kpi_refer_to.owner_email == sub.user.email
                assert kpi.get_children_refer().first() == new_kpi_refer_to
                assert new_kpi_refer_to.id in sub.get_kpis_parent().values_list('id', flat=True)
                new_kpis.append(new_kpi_refer_to)
                count += 1

            # ## Step 3: assert new user have n kpi
            assert sub.get_kpis_parent().count() == ConfigReview.AMOUNT_KPI_PER_PERSON

            # ## Step 4: create new user's subordinate
            create_new_sub_recursive(organization=organization,
                                     manager_profile_id=sub.id,
                                     max_level=max_level, kpis=new_kpis,
                                     level=level + 1)

    create_new_sub_recursive(organization=organization_standard,
                             manager_profile_id=ceo.profile.id,
                             max_level=max_level,
                             kpis=kpis)

    # assert _index == 2 ** (max_level + 1) - 1
    assert ceo.profile.get_all_subordinate().count() == _index - 1


# Note: Hong disable fixture this to improve speed.
#       We should only use this for certain benchmarking tests, not all tests
#
@pytest.mark.django_db(transaction=False)
@pytest.fixture()
def organization_standard(ceo):
    organization = ceo.profile.get_organization()
    if not organization:
        from company.models import Organization

        organization = Organization()
        organization.discord_server = random_string()
        organization.user = ceo
        organization.ceo = ceo
        organization.name = "ten cong ty 2 "
        organization.save()

        ou = ceo.profile
        ou.organization = organization
        ou.save()

    current_quarter = organization.get_current_quarter()
    pC = ceo.profile
    pC.organization = organization
    pC.is_superuser = True
    pC.is_admin = True
    pC.save()

    assert ceo.profile.get_organization() == organization

    #  print user.username
    return organization


@pytest.mark.django_db(transaction=False)
@pytest.fixture()
def organization(userA, userB, ceo, userA_manager, userA_manager_manager,
                 userAdmin, userA1, userB1):
    '''
    caches must be deleted otherwise the test not work
    '''
    delete_memoized_verhash(get_organization_by_user)

    organization = userA.profile.get_organization()
    if not organization:
        from company.models import Organization

        organization = Organization()
        organization.discord_server = random_string()
        organization.user = userA

        organization.name = "[py.test] Tên cong ty "
        organization.enable_edit = True
        organization.edit_to_date = (datetime.now() + timedelta(days=30)).date()
        organization.default_auto_score_parent = True
        organization.save()

        ceo_p = ceo.profile
        ceo_p.is_superuser = True
        ceo_p.organization = organization
        ceo_p.save()

        organization.ceo = ceo
        organization.save()

        # ## Update profile
        pA = userA.profile
        pA.organization = organization
        pA.save()

        pA = userA1.profile
        pA.organization = organization
        pA.save()

        pA = userB.profile
        pA.organization = organization
        pA.save()

        pA = userB1.profile
        pA.organization = organization
        pA.save()

        pA = userAdmin.profile
        pA.is_superuser = True
        pA.organization = organization
        pA.save()

        pA = userA_manager.profile
        pA.organization = organization
        pA.save()

        pA = userA_manager_manager.profile
        pA.organization = organization
        pA.save()

    # userA1 has rule: unit admin
    rule = RuleTable(actor=userA1, target=userA_manager_manager, roles='admin')
    rule.save()
    # userB1 has rule: KPI_EDITING
    rule = RuleTable(actor=userB1, target=userA_manager_manager, roles=KPI__EDITING)
    rule.save()

    current_quarter = organization.get_current_quarter()

    # set the self_review_date to the due_date
    organization.self_review_date = current_quarter.due_date
    organization.save(update_fields=['self_review_date', ])
    delete_memoized_verhash(get_organization_by_user)

    assert userB.profile.get_organization() == organization

    #  print user.username
    return organization


@pytest.mark.django_db(transaction=False)
@pytest.fixture()
def organization2(userC):
    organization = userC.profile.get_organization()
    if not organization:
        from company.models import Organization

        organization = Organization()
        organization.discord_server = random_string()
        organization.user = userC
        organization.ceo = userC
        organization.name = "ten cong ty 2 "
        organization.save()

        ou = userC.profile
        ou.organization = organization
        ou.save()

    current_quarter = organization.get_current_quarter()
    pC = userC.profile
    pC.organization = organization
    pC.save()

    assert userC.profile.get_organization() == organization

    #  print user.username
    return organization

#
# def ts_javascript_error(selenium_userA_client, live_server, link):
#
#     #Hong temporary disable
#     return True
#
#     ignore_links = [
#         '/login-by-sessionid/',
#         '/performance/report/'
#     ]
#     if link and link in ignore_links:
#         return
#
#     selenium_userA_client.get(live_server + link)
#     try:
#         time.sleep(10)  # sleep 5 seconds for JS to be all runnning
#         body = selenium_userA_client.find_elements_by_tag_name('body')[0]
#
#     except:
#         body = None
#
#     if body:
#         JSError = body.get_attribute("JSError")
#         print link
#         #   print body
#         assert JSError == ''
#
#     # check csrf in javascript
#     assert "var CSRF_COOKIE_NAME = '{}'".format(settings.CSRF_COOKIE_NAME) in selenium_userA_client.page_source
#     assert "const csrftoken =" in selenium_userA_client.page_source


@pytest.mark.django_db(transaction=False)
@pytest.fixture()
def organization3(userD):
    from company.models import Organization

    organization = Organization()
    organization.discord_server = random_string()
    organization.user = userD
    organization.ceo = userD
    organization.name = "Company 3"
    organization.save()

    ou = userD.profile
    ou.organization = organization
    ou.save()

    return organization

@pytest.mark.django_db(transaction=False)
@pytest.fixture()
def position_parent(organization):
    p = Position(organization=organization,
                 name="VTCD Parent",
                 parent=None)
    p.save()
    return p


@pytest.mark.django_db(transaction=False)
@pytest.fixture()
def position(organization, position_parent):
    p = Position(organization=organization,
                 name="VTCD",
                 parent=position_parent)
    p.save()
    return p

@pytest.fixture(scope='session')
def celery_app():
    return _celery_app
#
# /home/homoske/.virtualenvs/cloudjetkpi-celery-new/lib/python2.7/site-packages/celery/contrib/testing/app.py
# # Put this in your conftest.py
@pytest.fixture(scope='session')
def celery_config():
    return {
        'broker_url': BROKER_URL,
        'result_backend': CELERY_RESULT_BACKEND,
        # 'broker_url': 'redis://127.0.0.1:6379/15',
        # 'result_backend': 'redis://127.0.0.1:6379/15',
        'accept_content': {'json', 'pickle'},
        # 'worker_hijack_root_logger': True,
    }

@pytest.fixture(scope='session')
def celery_worker_parameters():
    return {
        'loglevel':'debug',
        'queues': ('cloner',
                   'default',
                   'celery',
                   ),
        'exclude_queues': (
            # 'celery',
        ),
    }

@pytest.fixture(scope='session')
def celery_enable_logging():
    return True

# this not worked because the task not registered yet
# @threading_or_worker
# @_celery_app.task
# def run_celery():
#     print 'run celery'

@threading_or_worker
@shared_task
def my_celery_shared_task():
    print 'run celery'

