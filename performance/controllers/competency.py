#!/usr/bin/python
# -*- coding: utf-8 -*-
import json

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.db import transaction
from django.db.models import Q
from django.http import HttpResponse, Http404
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from performance.base.views import PerformanceBaseView  # , CopyPerformanceBaseView, CurrentPerformanceBaseView
from performance.logger import KPILogger
from performance.db_models.competency import CompetencyLib, Competency
from performance.forms import PerformanceForm
from performance.models import KPI
from performance.services import remove_kpi
from utils.common import HttpResponseJson

import threading
from django.core.cache import cache

#
# class CompetencyComplete(PerformanceBaseView):
#     def get(self, request, *args, **kwargs):
#         query = request.GET.get('query', '')
#         organization = self.get_organization(request.user)
#         comps = Competency.objects.filter(Q(name__icontains=query,
#                                             in_library=True,
#                                             belong_to_jobs__category__industry_id=organization.industry_id) |
#                                           Q(name__icontains=query,
#                                             in_library=True,
#                                             parent__belong_to_jobs__category__industry_id=organization.industry_id)) \
#             .select_related('parent').values('name', "parent__name")
#         response = []
#         list_names = []
#         for comp in comps:
#             if comp['parent__name']:
#                 value = comp['parent__name'] + u' → ' + comp['name']
#             else:
#                 value = comp['name']
#
#             if value in list_names:
#                 continue
#             else:
#                 list_names.append(value)
#
#             item = {
#                 'data': comp['name'],
#                 'value': value
#             }
#             response.append(item)
#
#         data = {
#             "query": "Unit",
#             "suggestions": response
#         }
#         return HttpResponse(json.dumps(data), content_type="application/json")
#

@login_required
@csrf_exempt
def update_competency(request, quarter_period):
    result = "ok"
    form = PerformanceForm(request.POST)
    if form.is_valid():
        comp_id = request.POST.get('obj_id', '')
        if comp_id.isdigit():
            try:
                updated = False
                comp = Competency.objects.get(pk=comp_id, quarter_period=quarter_period)
                if comp.name != form.cleaned_data['name']:
                    updated = True
                    old_name = comp.name
                    comp.name = form.cleaned_data['name']
                    comp.name_vi = form.cleaned_data['name']
                    comp.name_en = form.cleaned_data['name']

                comp.description = form.cleaned_data['description']
                person_id = request.POST.get('person_id', None)
                parent_id = request.POST.get('parent', None)
                if parent_id.isdigit() and person_id.isdigit() and Competency.objects.filter(user_id=person_id,
                                                                                             id=parent_id).exists():
                    if comp.parent_id != int(parent_id):
                        result = 'changed'
                    comp.parent_id = parent_id
                else:
                    comp.parent_id = None
                comp.save()

                if updated:
                    description = u"%s → %s" % (old_name, comp.name)
                    KPILogger(request.user, comp, 'update', description).start()
                return HttpResponse(result)
            except:
                return HttpResponse("Competency doesn't exist.")
        else:
            comp = Competency()
            comp.name = form.cleaned_data['name']
            comp.name_vi = form.cleaned_data['name']
            comp.name_en = form.cleaned_data['name']
            comp.description = form.cleaned_data['description']
            person_id = request.POST.get('person_id', None)
            parent_id = request.POST.get('parent', None)
            comp.user_id = person_id
            comp.is_owner = True
            comp.quarter_period = quarter_period
            if parent_id.isdigit() and person_id.isdigit() and Competency.objects.filter(user_id=person_id,
                                                                                         id=parent_id).exists():
                comp.parent_id = parent_id
            else:
                comp.parent_id = None
            comp.save()

            result = {
                'type': 'new',
                "parent": comp.parent_id,
                'obj': comp.newbie_to_json()
            }

            KPILogger(request.user, comp, 'new', comp.get_name()).start()
            return HttpResponse(json.dumps(result), content_type="application/json")

    return HttpResponse('failed')
#
#
# @login_required
# @csrf_exempt
# def delete_competency(request):
#     if request.method == "POST" and request.is_ajax():
#         user_id = request.POST.get('user_id', None)
#         competency_id = request.POST.get('competency_id', None)
#         try:
#             competency = Competency.objects.get(id=competency_id, user_id=user_id)
#             KPILogger(request.user, competency, 'delete', competency.get_name()).run()
#             children = competency.get_children()
#             for child in children:
#                 KPILogger(request.user, child, 'delete', child.get_name()).run()
#             competency.delete()
#             return HttpResponse('ok')
#         except Exception as e:
#             return HttpResponse(str(e))
#     else:
#         # return HttpResponse("Bad Request")
#         return HttpResponseJson({'status': 'Bad Request'})
#
#
# @login_required()
# def competency_explain_request(request, id):
#     competency = Competency.objects.get(id=id)
#     user = request.user
#     from_email = user.email
#
#     url = 'http://www.cjs.vn/admin/performance/competency/?q=' + competency.get_name()
#
#     send_mail(u'Yêu cầu giải thích Năng Lực: ' + competency.get_name(),
#               u'Yêu cầu giải thích Năng Lực: ' + competency.get_name() + u"\r\n link: " + url, from_email,
#               ['tickets@cloudjetsolutions.uservoice.com'], fail_silently=False)
#
#     return HttpResponse("ok")
#
#
# @login_required()
# def competency_lib_copy(request):
#     id = request.GET.get('id', 0)
#     comps = CompetencyLib.objects.filter(parent=None).order_by('id')
#
#     allow = False
#
#     if id == 0:
#         user = request.user
#         allow = True
#     else:
#
#         user = User.objects.get(id=id)
#         # if request.user.profile.is_superuser() or request.user == user.profile.report_to:
#         if request.user.profile.is_superuser_org() or request.user.profile == user.profile.parent:
#             allow = True
#
#     if allow:
#         for comp in comps:
#             comp.copy_to_user(user)
#
#         return HttpResponse("ok")
#     else:
#         return HttpResponse("not allow")


#
#
# class CurrentCompetencyView(CurrentPerformanceBaseView):
#     model = Competency
#
#
# class CopyCompetencyView(CopyPerformanceBaseView):
#     model = Competency

#
# class DuplicateKPICompetency(PerformanceBaseView):
#     @method_decorator(login_required)
#     @method_decorator(csrf_exempt)
#     def dispatch(self, *args, **kwargs):
#         return super(DuplicateKPICompetency, self).dispatch(*args, **kwargs)
#
#     def get(self, request, *args, **kwargs):
#         cache_key = request.GET.get('cache_key', None)
#         if cache_key is None:
#             raise Http404
#         return self.process_get(cache_key)
#
#     def process_get(self, cache_key):
#         state = cache.get(cache_key, -1)
#         if state == 0:
#             return HttpResponse('ok')
#         elif state == -1:
#             return HttpResponse('other')  # not start or failed
#         else:  # 1 - mean running
#             return HttpResponse(cache_key)
#
#     # @transaction.atomic
#     def post(self, request, *args, **kwargs):
#         user_id = request.POST['user_id']
#         if not user_id:
#             raise Http404
#
#         list_user = request.POST.getlist('list_user[]')
#
#         delete_all_kpis = True if request.POST.get('delete_all_kpis') == 'true' else False
#         current_quarter = self.get_current_quarter(request.user)
#         #  NEED SET CACHE HERE
#         # need re-order list
#         list_user_tmp = [int(x) for x in list_user]
#         list_user_tmp.sort()
#         list_user_tmp = [str(x) for x in list_user]
#         list_user_tmp = '_'.join(list_user_tmp)
#
#         cache_key = 'duplicate_from_{}_to_{}_{}'.format(user_id, list_user_tmp, 'true' if delete_all_kpis else 'false')
#
#         cache.set(cache_key, 1, 60 * 1)
#         DuplicateKPICompetencyWorker(request.user, current_quarter, user_id, list_user, delete_all_kpis,
#                                      cache_key).start()
#
#         return self.process_get(cache_key)

#
# class DuplicateKPICompetencyWorker(threading.Thread):
#     def __init__(self, request_user, current_quarter, user_id, list_user, delete_all_kpis, cache_key, *args, **kwargs):
#         super(DuplicateKPICompetencyWorker, self).__init__(*args, **kwargs)
#         self.request_user = request_user
#         self.current_quarter = current_quarter
#         self.user_id = user_id
#         self.list_user = list_user
#         self.delete_all_kpis = delete_all_kpis
#         self.cache_key = cache_key
#
#     def run(self):
#         self.set_cache(1)
#         self.process_duplicate()
#
#     def update_kpi(self, kpi):
#         if isinstance(kpi, KPI):
#             if kpi.parent_id:
#                 kpi.refer_to_id = kpi.parent_id
#                 kpi.owner_email = kpi.user.email
#                 kpi.save()
#             else:
#                 kpi.owner_email = kpi.user.email
#                 kpi.save()
#                 children = kpi.get_children()
#                 for child in children:
#                     self.update_kpi(child)
#
#     def set_cache(self, value):
#         cache.set(self.cache_key, value, 60 * 1)
#
#     @transaction.atomic
#     def process_duplicate(self):
#         user_id = self.user_id
#         if not user_id:
#             raise Http404
#
#         list_user = self.list_user
#         request_user = self.request_user
#
#         current_quarter = self.current_quarter
#         users = User.objects.filter(id__in=list_user).exclude(id=user_id)
#
#         kpis = KPI.objects.filter(quarter_period=current_quarter,
#                                   user_id=user_id,
#                                   parent=None)
#         if self.delete_all_kpis:
#             tobe_deleted = KPI.objects.filter(quarter_period=current_quarter,
#                                               user_id__in=list_user).exclude(user_id=user_id)  # .delete()
#             for k in tobe_deleted:
#                 self.set_cache(1)
#                 remove_kpi(k)
#         for kpi in kpis:
#             for user in users:
#                 self.set_cache(1)
#                 temp = kpi.clone_to_user(user, current_quarter)
#                 KPILogger(request_user, temp, 'new', kpi.get_name_current_goal()).start()
#                 self.update_kpi(temp)
#
#         competencies = Competency.objects.filter(quarter_period=current_quarter,
#                                                  user_id=user_id,
#                                                  parent=None)
#         Competency.objects.filter(quarter_period=current_quarter,
#                                   user_id__in=list_user).delete()
#         for competency in competencies:
#             for user in users:
#                 self.set_cache(1)
#                 comp = competency.clone_to_user(user, current_quarter)
#                 KPILogger(request_user, comp, 'new', comp.get_name()).start()
#
#         self.set_cache(0)
#
#         # return HttpResponse('ok')
