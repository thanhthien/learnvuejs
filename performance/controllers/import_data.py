from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth.models import User
from company.services.organization import get_reseller
from performance.db_models.log import KpiLog
from performance.models import KPI
from user_profile.services.profile import get_user_by_id, get_organization_from_user
from user_profile.services.user import superuser_required, get_user_by_email

from django.http import Http404

from utils.api.response import APIJsonResponseNotFound


@superuser_required
def import_users(request):
    return render(request, 'imports/import_users.html', {})


@login_required
def import_kpis(request):
    # user_id = request.GET.get('user_id', 0)
    # user = get_user_by_id( user_id)
    #
    # if not user or not user_id:
    #     raise Http404
    #
    # if user.profile.get_organization() != request.user.profile.get_organization():
    #     raise PermissionDenied
    organization = get_organization_from_user(request.user)
    return render(request, 'imports/import_kpis.html', {
        # 'user_organizations': user_organizations,
        'organization': organization
    })


@login_required
def import_general_kpis(request):
    return render(request, 'imports/import_general_kpis.html', {
        # 'user_organizations': user_organizations,

    })


@login_required
def import_kpis_copy(request):
    return render(request, 'imports/import_kpis_copy.html', {
        # 'user_organizations': user_organizations,
        'actor':request.user
    })

@login_required
def history_deleted_kpi(request):
    user_id = request.GET.get('user_id', None)
    user = None
    if user_id:
        try:
            user = User.objects.get(pk=user_id)
        except:
            return APIJsonResponseNotFound(message="User Not Found")

    logs = []

    if user:
        #kpis = KPI.deleted_objects.filter(user=user)
        logs = KpiLog.objects.filter(user=user, status='delete', log_type='kpi').order_by('-edited_date')

    return render(request, 'performance/history_deleted_kpi.html', {'logs': logs})


@login_required
def integration(request):
    return render(request, 'integration/dashboard.html', {})


# @superuser_required
def email_redirect(request):
    type = request.GET.get('type', 'editor')
    email = request.GET.get('email', '')

    user = get_user_by_email(email)

    if not email or not user:
        raise Http404

    url = ''

    if type == 'editor':
        url = reverse('kpi_editor_emp', kwargs={'user_id': user.id})

    if type == 'team':
        url = reverse('team_view') + "?user_id={}".format(user.id)

    if type == 'people':
        url = reverse('people') + "?user_id={}".format(user.id)

    return HttpResponseRedirect(url)
