#!/usr/bin/python
# -*- coding: utf-8 -*-
import datetime
import hashlib
import json
import traceback

import xlrd
from authorization.rules import DELETE_KPI, KPI__EDITING
from authorization.rules import has_perm
from dateutil.parser import parse
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.cache import cache
from django.core.exceptions import ValidationError, PermissionDenied
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.validators import validate_email
from django.db import transaction, models
from django.db.models import Q
from django.db.models.expressions import Value
from django.db.models.functions import Concat
from django.http.response import HttpResponse, HttpResponseRedirect, Http404, HttpResponseForbidden
from django.shortcuts import render, render_to_response, get_object_or_404
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import FormView, ListView
from performance.base.views import PerformanceBaseView  # , CopyPerformanceBaseView
from performance.logger import KPILogger
from performance.db_models.log import KpiLog
from performance.documents import KPIRviewByStage
from performance.forms import ImportKPIUserForm, ImportKPIForm
from performance.models import KPI, KpiComment, MarkAsReadComment, \
    are_other_children_delayed
from performance.services import add_new_cascadeKPIQueue, get_ecal_link, get_kpi_from_unique_key, \
    KPIServices, add_user_comment, add_auto_comment, remove_kpi_thread, get_kpi, \
    update_score, get_input_update_score, get_kpi_parent_related
from user_profile.models import Profile
from  user_profile.services.permissions import can_view
from user_profile.services.profile import filter_profile_by_report_to
from user_profile.templatetags.user_tags import display_name
from utils import postpone
from utils.api.response import APIJsonResponseNotFound, APIJsonResponseForbidden, APIJsonResponseOK, \
    APIJsonResponseBadRequest, APIJsonResponse404
from utils.common import HttpResponseJsonForbidden
from utils.random_str import random_string
import logging
logger=logging.getLogger('django')

#
# @login_required
# @csrf_exempt
# def kpi_filling_stats(request):
#     organization = request.user.get_profile().get_organization()
#     quarter_period = organization.get_current_quarter()
#     users = organization.get_users()
#     users_not_kpi = []
#     for u in users:
#         if KpiLog.objects.filter(user=u, quarter=quarter_period).count() == 0:
#             users_not_kpi.append(u)
#     noti_messages = KPIApprovalNotificationMessage.objects.all()
#
#     variables = RequestContext(request, {
#         'organization': organization,
#         # 'quarter_periods': organization.get_all_quater_period(),
#         'current_quarter_period': quarter_period,
#         'employees': users_not_kpi,
#         'noti_messages': noti_messages,
#         'employees_count': len(users_not_kpi),
#     })
#
#     template = 'performance/kpi_filling_stats.html'
#     return render_to_response(template, variables)
class KPIDeleted(ListView):
    model = KPI
    template_name = 'admin/deleted_kpis_view.html'
    context_object_name = 'kpis'
    paginate_by = 50

    @method_decorator(staff_member_required)
    def dispatch(self, request, *args, **kwargs):
        return super(KPIDeleted, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        kpis = None
        kpi_name = self.request.GET.get('kpi_name', '')
        kpi_org = self.request.GET.get('kpi_org', '')
        kpi_user_email = self.request.GET.get('kpi_user_email', '')
        kpi_user_name = self.request.GET.get('kpi_user_name', '')

        search = self.request.GET.get('search', '')
        operator = self.request.GET.get('operator', '').upper()

        from_date_str = self.request.GET.get('from', '')
        to_date_str = self.request.GET.get('to', '')
        self.from_date_str = from_date_str
        self.to_date_str = to_date_str
        # parse to datetime
        from_date = None
        to_date = None

        from_date = datetime.datetime.strptime(from_date_str, '%Y-%m-%d') if from_date_str else None
        to_date = datetime.datetime.strptime(to_date_str, '%Y-%m-%d') if to_date_str else None

        self.kpi_name = kpi_name
        self.kpi_org = kpi_org
        self.kpi_user_email = kpi_user_email
        self.kpi_user_name = kpi_user_name
        self.operator = operator

        if search == 'advance' and operator == 'NONE':
            self.kpi_name = ''
            self.kpi_org = ''
            self.kpi_user_email = ''
            self.kpi_user_name = ''
            self.operator = 'NONE'
            self.from_date_str = ''
            self.to_date_str = ''
            pass

        # http://www.nomadjourney.com/2009/04/dynamic-django-queries-with-kwargs/
        kwargs = {}
        args = []

        args.append(Q(removed__isnull=False))
        # args.append(Q(owner_email__isnull=False))
        # args.append(~Q(owner_email=''))

        p1 = {}
        p2 = {}
        p3 = {}
        p4 = {}
        p5 = {}

        if kpi_name != '':
            p1['name__icontains'] = kpi_name
        if kpi_org != '':
            p2['quarter_period__organization__name__icontains'] = kpi_org
        if kpi_user_email != '':
            p3['user__email__icontains'] = kpi_user_email
        if kpi_user_name != '':
            p4['user__username__icontains'] = kpi_user_name
        if from_date or to_date:
            if from_date:
                args.append(Q(removed__gte=from_date))
            if to_date:
                args.append(Q(removed__lte=to_date))

        if search == 'advance':
            if operator == 'AND':
                kwargs = dict(p1.items() + p2.items() + p3.items() + p4.items())
                kpis = KPI.all_objects.filter(*args, **kwargs).order_by('-id')
            elif operator == 'OR':
                kwargs = dict(p1.items() + p2.items() + p3.items() + p4.items())
                query = Q()
                for k, v in kwargs.items():
                    query |= Q(**{k: v})
                args.append(query)
                kpis = KPI.all_objects.filter(*args).order_by('-id')
            else:  # NONE
                kpis = KPI.deleted_objects.all()

            return kpis

        else:
            if search == 'kpi_name':
                kpis = KPI.all_objects.filter(Q(removed__isnull=False), **p1).order_by('-id')

            elif search == 'kpi_org':
                kpis = KPI.all_objects.filter(Q(removed__isnull=False), **p2).order_by('-id')

            elif search == 'kpi_user_email':
                kpis = KPI.all_objects.filter(Q(removed__isnull=False), **p3).order_by('-id')

            elif search == 'kpi_user_name':
                kpis = KPI.all_objects.filter(Q(removed__isnull=False), **p4).order_by('-id')
            elif search == 'kpi_date_range':
                kpis = KPI.all_objects.filter(*args, **p5).order_by('-id')
            else:
                kpis = KPI.deleted_objects.all()

        return kpis
        # return KPI.deleted_objects.filter(**search_params).order_by('-id')

    def get_context_data(self, **kwargs):
        context = super(KPIDeleted, self).get_context_data(**kwargs)
        context['request'] = self.request
        context['kpi_name'] = self.kpi_name
        context['kpi_org'] = self.kpi_org
        context['kpi_user_email'] = self.kpi_user_email
        context['kpi_user_name'] = self.kpi_user_name
        context['operator'] = self.operator
        context['from_date_str'] = self.from_date_str
        context['to_date_str'] = self.to_date_str
        return context

        #  _changed = kpi.get_changed_history()
        # KPILogger(actor, kpi, 'update', _changed).start()


def set_review_type(kpi, month_1, month_2, month_3):
    # Khang update condition to update month result
    # Must check lock month before update score
    try:

        # Khang note
        organization = kpi.user.profile.get_organization()
        if organization.enable_month_1_review():
            kpi.month_1 = month_1
        if organization.enable_month_2_review():
            kpi.month_2 = month_2
        if organization.enable_month_3_review():
            kpi.month_3 = month_3

        # Legacy code here
        if month_1 != '' and month_1 is not None:
            month = 1
            kpi.review_type = "monthly"
        if month_2 != '' and month_2 is not None:
            month = 2
            kpi.review_type = "monthly"
        if month_3 != '' and month_3 is not None:
            month = 3
            kpi.review_type = "monthly"
    except:
        return None
    return kpi


class UpdateScoreKPI(PerformanceBaseView):

    # Khang note: Tính điểm là tính điểm, không update liên quan target
    def post(self, request, *args, **kwargs):
        try:
            kpi_id = request.POST.get('kpi_id', '')

            if not kpi_id.isdigit():
                kpi_id = request.POST.get('id', None)

            # get kpi to kpi_id
            kpi = get_kpi_parent_related(kpi_id)
            if kpi:

                # if there is enough fields in kpi and kpi is not None

                # get input to kpi services
                input = get_input_update_score(request.user, kpi, request.POST)
                # update score from input
                try:
                    kpi = update_score(input['kpi'], request.user, input['month_1'], input['month_2'], input['month_3'],
                                           input['real'])
                except PermissionDenied as e:
                    logger.info(str(e))
                    logger.info(traceback.print_exc())

                    return APIJsonResponseBadRequest(e.message)

                if kpi:
                    # return KPI is Json and message
                    return HttpResponse(json.dumps({'status': 'ok',
                                                        'score': kpi.get_score(),
                                                        'parent_score': 0,  # remove parent_score,
                                                        'parent_id': kpi.parent_id if kpi.parent_id else kpi.id,
                                                        'final_score': 0,  # final_score,
                                                        'min_score': 0,  # min_score,
                                                        'max_score': 0,  # max_score,
                                                        'strength_kpi': 0,  # strength_kpi,
                                                        'weak_kpi': 0,  # weak_kpi,
                                                        'real': kpi.real,
                                                        'kpi': kpi.to_json()
                                                        }),
                                            content_type="application/json")

        # if fields not exist return status 400 and message
        except Exception as ex:  # if Key error return BadRequest
            logger.info(str(ex))
            logger.info(traceback.print_exc())
            return APIJsonResponseBadRequest()

        # return status 404 and message
        return APIJsonResponseNotFound(message=u'Không tìm thấy KPI nào')


class KpiDelete(PerformanceBaseView):

    def remove_kpi_worksheet(self, request, *args, **kwargs):
        kpi_id = request.POST.get('kpi_id', None)
        #     try:
        kpi = get_kpi(kpi_id, request.user)
        if not kpi:
            return APIJsonResponseNotFound(ugettext("Can not search any KPI"))
        if not has_perm(DELETE_KPI, request.user, kpi.user, kpi):
            return APIJsonResponseForbidden(ugettext("You do not have permission to delete this KPI"))
        is_removed = remove_kpi_thread(kpi_id, request.user)
        parent = kpi.get_parent()
        if parent:
            # clear cache to avoid rendering error
            key = "has_child{}".format(parent.id);
            if cache.get(key, None) is not None:
                cache.delete(key)
        print "is_removed ", is_removed

        return APIJsonResponseOK({}, ugettext("Delete successful"))

        # else:
        #
        #     return HttpResponse("error")
        #     # except:
        #     print traceback.format_exc()
        #     return HttpResponse("Unable delete this KPI")

    def post(self, request, *args, **kwargs):
        # if 'kpi_id' in request.POST and 'unique_key' in request.POST:
        return self.remove_kpi_worksheet(request, *args, **kwargs)
        #
        # user_id = request.POST.get('user_id', None)
        # kpi_id = request.POST.get('kpi_id', None)
        # try:
        #     kpi = KPI.objects.get(id=kpi_id, user_id=user_id)
        #     if kpi.cascaded_from_id:
        #         if kpi.user_id == request.user.id:
        #             return HttpResponse("KPI này đang liên kết với KPI khác. Bạn không thể xoá KPI này.")
        #         try:
        #             cascaded_from = kpi.cascaded_from
        #             cascaded_from.assigned_to = None
        #             cascaded_from.save()
        #         except:
        #             pass
        #
        #     KPILogger(request.user, kpi, 'delete', kpi.get_name_current_goal()).run()
        #     children = kpi.get_children()
        #     for child in children:
        #         if child.assigned_to_id:
        #             cascaded_kpi = KPI.objects.filter(user_id=child.assigned_to_id,
        #                                               cascaded_from=child).first()
        #             if cascaded_kpi and cascaded_kpi.get_children().filter(assigned_to_id__isnull=True):
        #                 for c in cascaded_kpi.get_children():
        #                     KPILogger(request.user, c, 'delete', c.get_name_current_goal()).run()
        #
        #                 KPILogger(request.user, cascaded_kpi, 'delete',
        #                           cascaded_kpi.get_name_current_goal()).run()
        #                 cascaded_kpi.delete()
        #
        #         KPILogger(request.user, child, 'delete', child.get_name_current_goal()).run()
        #
        #     if kpi.assigned_to_id:
        #         cascaded_kpi = KPI.objects.filter(user_id=kpi.assigned_to_id,
        #                                           cascaded_from=kpi).first()
        #         if cascaded_kpi and cascaded_kpi.get_children().filter(assigned_to_id__isnull=True):
        #             for c in cascaded_kpi.get_children():
        #                 KPILogger(request.user, c, 'delete', c.get_name_current_goal()).run()
        #
        #         if cascaded_kpi:
        #             KPILogger(request.user, cascaded_kpi, 'delete',
        #                       cascaded_kpi.get_name_current_goal()).run()
        #             cascaded_kpi.delete()
        #
        #     kpi.delete()
        #     parent = None
        #     try:
        #         parent = kpi.parent
        #     except KPI.DoesNotExist:
        #         parent = None
        #
        #     if parent:
        #         final_score = parent.calculate_avg_score(parent.user_id)
        #         if final_score:
        #             parent.set_score(final_score, parent.user_id)
        #             parent.set_score(final_score, parent.user.profile.report_to_id)
        #             add_new_cascadeKPIQueue(parent)
        #             # CascadeKPIQueue.objects.create(kpi=parent)
        #     return HttpResponse('ok')
        # except Exception as e:
        #     print traceback.format_exc()
        #     return HttpResponse(str(e))
#
#
# class ImportKPI(PerformanceBaseView, FormView):
#     template_name = 'performance/import_kpi.html'
#     form_class = ImportKPIForm
#
#     def get_form_kwargs(self):
#         kwargs = FormView.get_form_kwargs(self)
#         self.organization = self.get_organization(self.request.user)
#         user_org = self.get_user_organization(self.request.user)
#         kwargs['organization'] = self.organization
#         extra_args = {}
#         if not (user_org.is_admin or user_org.is_superuser):
#             extra_args = {'user': self.request.user}
#
#         kwargs['users'] = Profile.objects.filter(organization=self.organization,
#                                                  active=True, **extra_args) \
#             .annotate(full_name=Concat("display_name",
#                                        Value(" ("),
#                                        "user__email", Value(")"),
#                                        output_field=models.CharField())) \
#             .values_list('user_id', 'full_name')
#         return kwargs
#
#     def get_user_id(self, form):
#         if 'employee' in form.cleaned_data:
#             return form.cleaned_data['employee']
#         return self.kwargs['user_id']
#
#     def is_email(self, email):
#         try:
#             validate_email(email)
#             return True
#         except ValidationError as e:
#             return False
#
#     def form_valid(self, form):
#         user_id = self.get_user_id(form)
#         file_data = form.cleaned_data['file_data']
#         clear_old_data = False  # form.cleaned_data['clear_old_data']
#         book = xlrd.open_workbook(file_contents=file_data.read(),
#                                   encoding_override='utf-8').sheets()[0]
#
#         current_quarter = self.organization.get_current_quarter()
#         sid = transaction.savepoint()
#         try:
#             if clear_old_data:
#                 KPI.objects.filter(quarter_period=current_quarter,
#                                    user_id=user_id).delete()
#
#             parent = None
#             #             for line in range(1, book.nrows):
#             #                 row = book.row(line)
#             #                 name = row[2].value
#             #
#             #                 if name:
#             #                     parent = self.create_kpi(row, current_quarter, user_id)
#             #                     temp_kpi = parent
#             #                 else:
#             #
#             #                     kpi = self.create_kpi(row, current_quarter, user_id, parent=parent, refer_to=parent)
#             #                     temp_kpi = kpi
#
#             for line in range(1, book.nrows):
#                 row = book.row(line)
#                 name = row[2].value
#                 email = str(row[12].value).strip()
#                 if name:
#                     if email and self.is_email(email):
#                         if Profile.objects.filter(user_id=user_id,
#                                                   user__email=email).exists():  # user assigned is the imported user
#                             parent = self.create_kpi(row, current_quarter, user_id, None, None, email)
#                         elif self.is_email(email) and Profile.objects.filter(
#                                 user__email=email).exists():  # assign to other user
#                             # first create a median kpi --->kpi-1
#                             kpi_1 = self.create_kpi(row, current_quarter, user_id, None, None, None)
#                             # then, create the kpi that assign to the specified user
#                             # use assign to
#                             # kpi_1 = self.create_kpi(row, current_quarter, user_id, None, None, None)
#                             uo = Profile.objects.select_related('user').filter(user__email=email).first()
#                             kpi_1.assign_to(uo.user)
#                             parent = kpi_1
#
#                         else:
#                             raise Exception("Email: " + email + "not valid OR not existed!")
#                     else:
#                         # create .single kpi
#                         kpi = self.create_kpi(row, current_quarter, user_id)
#                         parent = kpi
#                 else:
#                     # create child kpi
#                     kpi = self.create_kpi(row, current_quarter, user_id, parent=parent)
#                     temp_kpi = kpi
#
#                     # TODO: why we write logic here?? no way to understand
#                     # Truong hop nay la phan cong kpi con ma kpi con do user khac dam nhan
#                     if email and parent.user.email != email:
#                         email = row[12].value
#                         if self.is_email(email) and Profile.objects.filter(user__email=email).exists():
#                             uo = Profile.objects.select_related('user').filter(user__email=email).first()
#                             temp_kpi.assign_to(uo.user)
#
#         except Exception as e:  # pragma: no cover
#             print traceback.format_exc()
#             transaction.savepoint_rollback(sid)
#             messages.info(self.request, "Import KPI failed: " + str(e))
#             return HttpResponseRedirect(self.get_success_url())
#         transaction.savepoint_commit(sid)
#         messages.info(self.request, "Import KPI sucessed.")
#         return HttpResponseRedirect(self.get_success_url())
#
#     def get_success_url(self):
#         return self.request.path
#
#     def get_bsc_category(self, value):
#         if value == "F":
#             return 'financial'
#         elif value == "C":
#             return "customer"
#         elif value == "P":
#             return "internal"
#         elif value == "L":
#             return "learninggrowth"
#         return 'other'
#
#     def get_target(self, kpi, quarter_period):
#         if quarter_period.quarter == 1:
#             return float(kpi.quarter_one_target) if kpi.quarter_one_target else None
#         elif quarter_period.quarter == 2:
#             return float(kpi.quarter_two_target) if kpi.quarter_two_target else None
#         elif quarter_period.quarter == 3:
#             return float(kpi.quarter_three_target) if kpi.quarter_three_target else None
#         elif quarter_period.quarter >= 4:
#             return float(kpi.quarter_four_target) if kpi.quarter_four_target else None
#         return None
#
#     def create_kpi(self, row, quarter_period, user_id, parent=None, refer_to=None, owner_email=None):
#         name = row[2].value.encode('utf-8').strip()
#         print name
#         if not name:
#             name = row[3].value.encode('utf-8').strip()
#         year_target = float(row[7].value) if row[7].value != '' else None
#         quarter_one_target = float(row[8].value) if row[8].value != '' else None
#         quarter_two_target = float(row[9].value) if row[9].value != '' else None
#         quarter_three_target = float(row[10].value) if row[10].value != '' else None
#         quarter_four_target = float(row[11].value) if row[11].value != '' else None
#
#         ordering = None
#         if row[1].value:
#             ordering = float(row[1].value)
#
#         obj = KPI()
#         obj.quarter_one_target = quarter_one_target
#         obj.quarter_two_target = quarter_two_target
#         obj.quarter_three_target = quarter_three_target
#         obj.quarter_four_target = quarter_four_target
#
#         kpi = KPIServices().create_kpi(name=name,
#                                        bsc_category=self.get_bsc_category(row[0].value),
#                                        quarter_period=quarter_period,
#                                        user_id=user_id,
#                                        ordering=ordering,
#                                        unit=row[4].value.encode('utf-8').strip(),
#                                        current_goal=row[5].value.encode('utf-8').strip(),
#                                        operator=row[6].value,
#                                        year_target=year_target,
#                                        quarter_one_target=quarter_one_target,
#                                        quarter_two_target=quarter_two_target,
#                                        quarter_three_target=quarter_three_target,
#                                        quarter_four_target=quarter_four_target,
#                                        target=self.get_target(obj, quarter_period),
#                                        parent=parent,
#                                        refer_to=refer_to,
#                                        owner_email=owner_email)
#
#         #         year_target=row[7].value if row[7].value != '' else None
#         #         quarter_one_target=row[8].value if row[8].value != '' else None
#         #         quarter_two_target=row[9].value if row[9].value != '' else None
#         #         quarter_three_target=row[10].value if row[10].value != '' else None
#         #         quarter_four_target=row[11].value if row[11].value != '' else None
#         #
#         #         kpi = KPIServices().create_kpi(name=name,
#         #                                        bsc_category=self.get_bsc_category(row[0].value),
#         #                                        ordering=row[1].value,
#         #                                        quarter_period=quarter_period,
#         #                                        user_id=user_id,
#         #                                        unit=row[4].value,
#         #                                        current_goal=row[5].value,
#         #                                        operator=row[6].value,
#         #                                        year_target=year_target,
#         #                                        quarter_one_target=quarter_one_target,
#         #                                        quarter_two_target=quarter_two_target,
#         #                                        quarter_three_target=quarter_three_target,
#         #                                        quarter_four_target=quarter_four_target,
#         #
#         #                                        parent=parent,
#         #                                        refer_to=refer_to,
#         #                                        owner_email=owner_email,
#         #         )
#         return kpi
#
#
# class ImportKPIByUser(ImportKPI):
#     template_name = "performance/import_kpi_user.html"
#     form_class = ImportKPIUserForm
#
#     def get_form_kwargs(self):
#         kwargs = FormView.get_form_kwargs(self)
#         self.organization = self.get_organization(self.request.user)
#         return kwargs

#
# class CopyKPIView(CopyPerformanceBaseView):
#     model = KPI


def kpi_logs(request, user_id):
    # user = User.objects.get(id=user_id)
    try:
        user = User.objects.get(id=user_id)
    except Exception as e:
        return APIJsonResponse404("User not found")

    if not can_view(user, request.user):
        return HttpResponseForbidden()

    page = request.GET.get('page', None)

    kpi_logs_ = KpiLog.objects.filter(user=user).order_by('-id')  # [:100]

    paginator = Paginator(kpi_logs_, 75)

    try:
        kpi_logs = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        kpi_logs = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        kpi_logs = paginator.page(paginator.num_pages)

    return render(request, 'notification_modules/kpi_log.html', {
        'kpi_logs': kpi_logs,
        'page': page,
        'user_id': user_id
        # 'u':user,
    })


def logs(request):
    return render(request, 'notification_modules/log.html', {

    })


def system_logs(request):
    return render(request, 'notification_modules/system_logs.html', {

    })

class Ecal(PerformanceBaseView):

    def get(self, request, *args, **kwargs):
        kpi_unique_key = kwargs['kpi_unique_key']

        variables = {
            'ecal_link': get_ecal_link(kpi_unique_key),
            'kpi': get_kpi_from_unique_key(kpi_unique_key)
        }

        return render(request, 'performance/ecal/iframe.html', variables)


@login_required
@csrf_exempt
def kpicomment(request, unique_key):
    user = request.user
    organization = user.get_profile().get_organization()
    quarter_period = organization.get_current_quarter()

    kpi_id = request.GET.get('id', None)
    if kpi_id and kpi_id.isdigit():
        kpi = KPI.objects.filter(id=kpi_id).first()
    else:
        kpi = KPI.objects.filter(unique_key=unique_key,
                                 quarter_period=quarter_period).order_by('id').first()

    content = request.POST.get('comment', '')
    comment_id = request.POST.get('obj_id', None)
    exist = False
    if comment_id:
        try:
            kc = KpiComment.objects.get(pk=comment_id)
            kc.created_at = datetime.datetime.now()
            exist = True
        except: # pragma: no cover
            pass
    else:
        if kpi:
            # kc = KpiComment(user=request.user, kpi_unique_key=unique_key, kpi=kpi[0], send_from_ui='web')
            # Khang
            # Convert this into service
            kc = add_user_comment(request.user, kpi, content, unique_key)
            exist = False

    if (kpi and content and not exist) or exist:  # THE FUCK

        kc.content = content
        kc.save()

        if 'editor' in request.GET: #pragma: no cover - not use, can't reach
            return HttpResponse(render_to_string('kpi_modules/comment.html', {"note": kc}))

        return HttpResponse(json.dumps(kc.to_dict()), content_type="application/json")
    else:

        kcs = KpiComment.objects.filter(kpi_unique_key=unique_key)
        kc_list = []
        for kc in kcs:
            kc_list.append(kc.to_dict())
        return HttpResponse(json.dumps(kc_list), content_type="application/json")


@staff_member_required
@csrf_exempt
def kpi_deleted(request):# pragma: no cover
    kpis = KPI.deleted_objects.all().order_by('-id')
    variables = RequestContext(request, {
        'kpis': kpis
    })

    template = 'admin/deleted_kpis.html'
    return render_to_response(template, variables)


class AssignKPI(PerformanceBaseView):

    def post(self, request, *args, **kwargs):
        if request.POST.get('parent') == "true":
            return self.assign_kpi_parent(request)

        kpi_id = request.POST.get('kpi_id')
        kpi = get_object_or_404(KPI, id=kpi_id)
        user = get_object_or_404(User, id=request.POST.get('user_id'))
        kpi.assign_to(user)

        content = ugettext("assigned KPI \"%(name)s\" for %(username)s") % {
            "name": kpi.name,
            'username': user.profile.display_name
        }
        # kc = KpiComment(user=request.user,
        #                 kpi_unique_key=kpi.unique_key,
        #                 kpi=kpi, send_from_ui='auto',
        #                 content=content,
        #                 is_update=True)
        # kc.save()

        # Khang
        # Convert this into service
        add_auto_comment(request.user, kpi, content)

        return HttpResponse(json.dumps({'user_id': user.id,
                                        'full_name': user.profile.display_name}))

    def assign_kpi_parent(self, request):
        kpi_id = request.POST.get('kpi_id')
        kpi = get_object_or_404(KPI, id=kpi_id)
        user = get_object_or_404(User, id=request.POST.get('user_id'))

        attrs = {}
        for f in kpi._meta.fields:
            if f.name not in ['id', 'hash']:
                attrs[f.name] = getattr(kpi, f.name)

        new_kpi = KPI(**attrs)
        new_kpi.assigned_to = user
        new_kpi.user = kpi.user
        new_kpi.parent = kpi
        new_kpi.copy_from = None
        new_kpi.unique_key = None
        new_kpi.cascaded_from = None
        new_kpi.bsc_category = None
        new_kpi.owner_email = None
        new_kpi.save()

        new_kpi.assign_to(user)

        content = ugettext("assigned KPI \"%(name)s\" for %(username)s") % {
            "name": kpi.name,
            'username': user.profile.display_name
        }
        # kc = KpiComment(user=request.user,
        #                 kpi_unique_key=new_kpi.unique_key,
        #                 kpi=new_kpi, send_from_ui='auto',
        #                 content=content,
        #                 is_update=True)
        # kc.save()

        # Khang
        # Change into service
        add_auto_comment(request.user, new_kpi, content)

        # cascade_kpi = KPI(**attrs)
        # cascade_kpi.assigned_to = None
        # cascade_kpi.user = user
        # cascade_kpi.parent = None
        # cascade_kpi.copy_from = None
        # cascade_kpi.unique_key = new_kpi.unique_key
        # cascade_kpi.cascaded_from = new_kpi
        # cascade_kpi.bsc_category = kpi.bsc_category
        # cascade_kpi.self_confirmed = False
        # cascade_kpi.manager_confirmed = False
        #         cascade_kpi.owner_email = user.email
        #         if cascade_kpi.bsc_category:
        #             cascade_kpi.ordering = kpi.ordering
        #         else:
        #             cascade_kpi.ordering = kpi.get_next_ordering()
        #         if Profile.objects.filter(report_to_id=kpi.user_id, user=user).exists():
        #             cascade_kpi.manager_confirmed = True
        #         cascade_kpi.save()

        return HttpResponse(json.dumps(new_kpi.newbie_to_json()), content_type="application/json")


class AlignUpKPI(PerformanceBaseView):

    def get(self, request, *args, **kwargs):
        organization = self.get_organization(request.user)
        current_quarter = organization.get_current_quarter()
        user_id = request.GET.get('user_id')
        bsc_category = request.GET.get('bsc_category')
        p = Profile.objects.filter(user_id=user_id).first()
        if p:
            # user_id = p.report_to_id
            user_id = p.parent.user_id if p.parent else None
        else:
            user_id = None

        if user_id:
            extra_select = '''
                CASE WHEN performance_kpi.bsc_category = 'financial' THEN 1
                WHEN performance_kpi.bsc_category = 'customer' THEN 2
                WHEN performance_kpi.bsc_category = 'internal' THEN 3
                WHEN performance_kpi.bsc_category = 'learninggrowth' THEN 4 ELSE 5 END '''
            kpis = KPI.objects.filter(quarter_period=current_quarter,
                                      user_id=user_id,
                                      parent=None,
                                      bsc_category=bsc_category,
                                      weight__gt=0) \
                .extra(select={'order_category': extra_select}) \
                .order_by('order_category', 'ordering', 'id')
            response = [{'id': None, 'name': '-------'}]
            for kpi in kpis:
                category = kpi.get_prefix_category()
                if category != '':
                    category = "[{0}{1}]".format(category, (kpi.ordering))
                response.append({
                    'name': u"{0} {1}".format(category, kpi.name),
                    'id': kpi.id
                })
                children = kpi.get_children().filter(assigned_to__isnull=True)

                # NO MORE CHILDREN --> IT MAKES THINGS TOO COMPlicated.
                #  for child in children:
                #     if child.assigned_to_id:
                #         continue
                #
                #     response.append({
                #         'name': u" - - - - - " + child.name,
                #         'id': child.id
                #     })

        else:
            response = []

        return HttpResponse(json.dumps(response), content_type="application/json")

    def post(self, request, *args, **kwargs):

        def toggle_parent(kpi, _to_state):
            refer_to = None
            if kpi.refer_to_id:
                try:
                    refer_to = kpi.refer_to
                except KPI.ObjectDoesNotExist: #pragma: no cover
                    pass

            if not refer_to and kpi.parent_id: #pragma: no cover - can't reach this because kpi.refer to always exist when run to this function
                try:
                    refer_to = kpi.parent
                except KPI.ObjectDoesNotExist:
                    pass

            if refer_to: refer_to.delay_toggle(force_to_state=_to_state, only_up_recursive=True,
                                               ignore_perm_check=False)

        with transaction.atomic():
            aligned_kpi = get_object_or_404(KPI, id=request.POST.get('aligned_kpi_id'))
            organization = request.user.profile.get_organization()

            # allow superadmin align kpi when Time to edit KPI expired.
            if not organization.enable_to_edit() and ( not request.user.profile.is_admin and not request.user.profile.is_superuser ):
                return HttpResponseJsonForbidden(ugettext("Time to edit KPI has expired."))

            if not has_perm(KPI__EDITING, request.user, target=aligned_kpi.user, kpi=aligned_kpi):
                return HttpResponseJsonForbidden(ugettext("You don't have permission to perform this action."))

            if not request.POST.get('kpi_id'):
                if aligned_kpi.cascaded_from_id:

                    try:
                        user_id = aligned_kpi.cascaded_from.user_id
                        aligned_kpi.cascaded_from.delete()
                    except: #pragma: no cover - can't reach this
                        user_id = None
                        pass

                    if are_other_children_delayed(aligned_kpi) and aligned_kpi.refer_to_id and aligned_kpi.refer_to.get_children_refer().count() > 1:  # and recursive_from_parent == False :
                        toggle_parent(aligned_kpi, 'delay')

                    aligned_kpi.cascaded_from = None
                    aligned_kpi.refer_to = None
                    aligned_kpi.save()

                    content = ugettext("unlinked KPI \"%(name)s\" of %(user_kpi)s to %(target_user)s") % {
                        'name': aligned_kpi.name,
                        'user_kpi': display_name(aligned_kpi.user_id),
                        'target_user': display_name(user_id)
                    }

                    # kc = KpiComment(user=request.user,
                    #                 kpi_unique_key=aligned_kpi.unique_key,
                    #                 kpi=aligned_kpi, send_from_ui='auto',
                    #                 content=content,
                    #                 is_update=True)
                    # kc.save()

                    add_auto_comment(request.user, aligned_kpi, content)

                response = {
                    'cascaded_from': None,
                    'category': aligned_kpi.bsc_category,
                    'ordering': aligned_kpi.ordering
                }
                return HttpResponse(json.dumps(response), content_type="application/json")

            if aligned_kpi.cascaded_from_id:
                try:
                    parent = aligned_kpi.cascaded_from.parent
                    aligned_kpi.cascaded_from.delete()
                    # CascadeKPIQueue.objects.create(parent)
                    add_new_cascadeKPIQueue(parent)
                except: #pragma: no cover
                    pass

            # aligned_kpi.cascaded_from = None
            # aligned_kpi.save()

            kpi_new = KPI()

            kpi = KPI.objects.filter(id=request.POST.get('kpi_id')).first()
            if aligned_kpi.weight == 0:
                return HttpResponseJsonForbidden(ugettext("KPI was delayed."))

            if kpi:
                try:
                    kpi_new = aligned_kpi.assign_up(kpi, force_run=False) # not force_run, because of we only allow align-up kpi to active parent kpi
                except PermissionDenied as e:
                    return HttpResponseJsonForbidden(unicode(e))

                content = ugettext("linked KPI \"%(name)s\" of %(user_kpi)s to %(target_user)s") % {
                    'name': aligned_kpi.name,
                    'user_kpi': display_name(aligned_kpi.user_id),
                    'target_user': display_name(kpi_new.user_id)
                }

                add_auto_comment(request.user, aligned_kpi, content)

            response = {
                'cascaded_from': kpi_new.id,
                'category': aligned_kpi.bsc_category,
                'ordering': aligned_kpi.ordering
            }

            return HttpResponse(json.dumps(response), content_type="application/json")


class CascadeKPIName(PerformanceBaseView):

    def get(self, request, *args, **kwargs):
        try:
            kpi = KPI.objects.get(id=kwargs['kpi_id'])
            name = ""
            if kpi.parent_id:
                category = kpi.parent.get_kpi_id_with_cat()  # kpi.parent.get_prefix_category()
                # if category != '':
                #     category = u"[{0}{1}] ".format(category, kpi.parent.ordering)
                name = u"[{0}] {1} - <i>{2}</i>".format(category, kpi.parent.name, display_name(kpi.user_id))

                #  if Profile.objects.filter(user=request.user, is_consultant=True).exists():
                #    name += u"<br><span style='margin-left: 10px'>↳</span> [{2}] {0} [{1}]".format(kpi.name, str(kpi.id), category = kpi.get_kpi_id_with_cat() )
                # else:#
            name += u"<br><span style='margin-left: 10px'>↳</span> [{0}] {1} <span style='color:#5c5c5c'>{2}</span>".format(
                kpi.get_kpi_id_with_cat(), kpi.name, kpi.id)

            assigned_kpi = kpi.get_assigned_kpi()
            if assigned_kpi and assigned_kpi.name != kpi.name: #pragma: no cover - caching in get_assigned_kpi()
                kpi.set_assigned_kpi_attrs()
            return HttpResponse(name)
        except KPI.DoesNotExist:
            return HttpResponse("KPI không tồn tại")

            #  return HttpResponse('')


class CategoryUpdateView(PerformanceBaseView):

    def get(self, request, *args, **kwargs):
        response = [
            {'value': '', 'text': '..'},
            {'value': 'financial', 'text': ugettext('Financial')},
            {'value': 'customer', 'text': ugettext('Customer')},
            {'value': 'internal', 'text': ugettext('Internal Process')},
            {'value': 'learninggrowth', 'text': ugettext('Learning & Growth')},
            {'value': 'other', 'text': ugettext('Other')},
        ]
        return HttpResponse(json.dumps(response), content_type="application/json")

    def post(self, request, *args, **kwargs):
        organization = request.user.get_profile().get_organization()
        quarter_period = organization.get_current_quarter()

        value = request.POST.get('value', 0)
        obj_id = request.POST.get('pk')
        try:
            kpi = KPI.objects.get(pk=obj_id, quarter_period=quarter_period)
        except:
            return HttpResponse("Kpi doesn't exist.")
        kpi.bsc_category = value
        kpi.save()

        return HttpResponse(json.dumps({
            'id': kpi.id,
            'parent_id': kpi.parent_id,
            'category': value,
            'category_order': kpi.get_category_order()
        }), content_type='application/json')

#
# class RemoveAssignedTo(PerformanceBaseView):
#
#     def post(self, request, *args, **kwargs):
#         kpi_id = request.POST.get('kpi_id')
#         user_id = request.POST.get('user_id')
#         assigned_to = request.POST.get('assigned_to')
#
#         kpi = get_object_or_404(KPI, id=kpi_id, assigned_to_id=assigned_to)
#         cascade_kpi = KPI.objects.filter(user_id=assigned_to,
#                                          cascaded_from_id=kpi.id)
#         if cascade_kpi.exists():
#             temp = cascade_kpi.first()
#             if cascade_kpi.exists():
#                 KPILogger(request.user, temp, 'delete', temp.get_name()).run()
#                 children = temp.get_children()
#                 for child in children:
#                     KPILogger(request.user, child, 'delete', child.get_name_current_goal()).run()
#
#                 temp.delete()
#
#         assigned_to = kpi.assigned_to_id
#         kpi.assigned_to = None
#         kpi.refer_to_id = kpi.parent_id
#         kpi.user = kpi.parent.user
#         kpi.owner_email = kpi.parent.user.email
#         kpi.save()
#
#         content = ugettext("Removed assignment for ") + display_name(assigned_to)
#         add_auto_comment(request.user, kpi, content)
#
#         return HttpResponse('ok')


class LoadComments(PerformanceBaseView):

    def post(self, request, *args, **kwargs):
        kpi = get_object_or_404(KPI, pk=request.POST.get('kpi_id'))
        return HttpResponse(json.dumps(kpi.get_kpi_notes()), content_type="application/json")

#
# class AssignKPITeamMember(PerformanceBaseView):
#     def post(self, request, *args, **kwargs):
#         current_quarter = self.get_current_quarter(request.user)
#         kpi_id = request.POST.get('kpi_id')
#         user_id = request.POST.get('user_id')
#         try:
#             kpi = KPI.objects.get(pk=kpi_id)
#             profile = Profile.objects.get(user_id=user_id)
#
#             team = profile.get_subordinate(type='user')
#
#             if not kpi.parent_id and kpi.owner_email != kpi.user.email:
#                 kpi.owner_email = kpi.user.email
#                 kpi.save()
#
#             # assgin to own manager
#             manager_kpi = kpi.clone_kpi_to_user(profile.user, current_quarter)
#             manager_kpi.parent = kpi
#             manager_kpi.refer_to = kpi
#             manager_kpi.owner_email = profile.user.email
#             manager_kpi.assigned_to = None
#             manager_kpi.save()
#
#             for user in team:
#                 user_kpi = kpi.clone_kpi_to_user(user, current_quarter)
#                 manager_kpi = kpi.clone_kpi_to_user(profile.user, current_quarter)
#                 manager_kpi.parent = kpi
#                 manager_kpi.refer_to = None
#                 manager_kpi.owner_email = None
#                 manager_kpi.assigned_to = user
#                 manager_kpi.save()
#
#                 user_kpi.cascaded_from = manager_kpi
#                 user_kpi.parent = None
#                 user_kpi.refer_to = kpi
#                 user_kpi.assigned_to = None
#                 user_kpi.owner_email = user.email
#                 user_kpi.save()
#
#             # children = kpi.get_children_dict('manager', True, profile.report_to_id)
#             children = kpi.get_children_dict('manager', True, profile.parent.user_id if profile.parent else None)
#         except:
#             raise Http404
#
#         return HttpResponse(json.dumps(children), content_type="application/json")

#
# class MoveKPI(PerformanceBaseView):
#
#     def get(self, request, *args, **kwargs):
#         user_id = request.GET.get('user_id', request.user.id)
#         data = []
#         try:
#             profile = Profile.objects.get(user_id=user_id)
#             # if profile.report_to_id:
#             if profile.parent:
#                 # Profile.objects.filter(report_to_id=profile.report_to_id)
#                 # data = list(filter_profile_by_report_to(profile.report_to_id).exclude(user_id=user_id).values('user_id',  'display_name'))
#                 data = list(filter_profile_by_report_to(profile.parent.user_id if profile.parent else None).exclude(
#                     user_id=user_id).values('user_id', 'display_name'))
#             else:
#                 # data = list(Profile.objects.filter(report_to_id=profile.user_id).values('user_id', 'display_name'))
#                 data = list(Profile.objects.filter(parent=profile).values('user_id', 'display_name'))
#         except:
#             print traceback.format_exc()
#             pass
#
#         return HttpResponse(json.dumps(data), content_type="application/json")
#
#     def post(self, request, *args, **kwargs):
#         organization = self.get_organization(request.user)
#         kpi_id = request.POST.get('kpi_id')
#         from_user_id = request.POST.get('from_user_id')
#         to_user_id = request.POST.get('to_user_id')
#
#         try:
#             kpi = KPI.objects.get(id=kpi_id, user_id=from_user_id)
#             user_org = Profile.objects.get(organization=organization,
#                                            user_id=to_user_id)
#
#             kpi.user = user_org.user
#             if kpi.cascaded_from_id:
#                 try:
#                     cascaded_from = kpi.cascaded_from
#                     cascaded_from.assigned_to = user_org.user
#                     cascaded_from.save()
#                 except:
#                     kpi.cascaded_from_id = None
#             kpi.owner_email = user_org.user.email
#             kpi.save()
#             children = kpi.get_children()
#             children.update(user=user_org.user)
#             children.filter(assigned_to__isnull=True).update(owner_email=user_org.user.email, refer_to=kpi)
#             KPILogger(request.user, kpi, 'transfer',
#                       kpi.get_name_current_goal()).start()
#             return HttpResponse('ok')
#         except:
#             return HttpResponse("failed")

#
# class InputDataKPI(PerformanceBaseView):
#     def get(self, request, *args, **kwargs):
#         # TODO: check authentication & authorization
#
#         kpi = KPI.objects.get(hash=kwargs['hash'])
#
#         return render(request, 'performance/frequency/kpi_input_mobile.html', {  # focus on mobile view
#             'kpi': kpi,
#
#         })
#
#     def post(self, request, *args, **kwargs):
#         pass


class EnableEditKPI(PerformanceBaseView):

    def post(self, request, *args, **kwargs):
        organization = self.get_organization(request.user)
        organization.enable_edit = True
        organization.edit_to_date = datetime.date.today()
        organization.save()
        cache.set('org_user' + str(request.user.id), organization, 60 * 2)
        return HttpResponse('ok')

#
# class KPIStageReview(PerformanceBaseView):
#
#     def get(self, request, *args, **kwargs):
#         try:
#             org = self.get_organization(request.user)
#             kpis = KPI.objects.filter(hash=kwargs['hash'])
#             kpi = None
#             if kpis.count() > 1:
#                 i = 1
#                 for k in kpis:
#                     if i == 1:  # not update the first one
#                         i += 1
#                     else:
#
#                         # TODO: chỗ này lại duplicate code, viết duplciate code hoài sau này sửa rất mệt
#                         k.hash = hashlib.sha1(str(k.id) + random_string(3)).hexdigest()
#                         k.save()
#                         if k.quarter_period.organization.id == org.id and not kpi:
#                             kpi = k
#
#             kpi = KPI.objects.get(hash=kwargs['hash'])
#             # kpi = KPI.objects.get(hash=kwargs['hash'])
#         except Exception as e:  # pragma: no cover
#             p = get_object_or_404(Profile, hash=kwargs['hash'])
#             u = p.user
#             kpi = KPI.objects.filter(user=u)
#             if kpi.count() == 0:
#                 raise Http404("No KPI")
#
#             else:
#                 kpi = kpi[0]
#
#         # kpi = get_object_or_404(KPI, hash=kwargs['hash'])
#
#         if request.GET.get('filter', None) == 'all':
#             kpis = KPI.objects.exclude(review_type='').filter(user=kpi.user)
#         else:
#             kpis = [kpi]
#
#         if 'json' in request.GET:
#             sub_kpis = json.loads(KPIRviewByStage.objects.filter(kpi_id=kpi.id).order_by('review_date').to_json())
#             parent_name = kpi.parent.name if kpi.parent else None
#
#             data = {
#                 # 'sum': sum,
#                 # 'average': average,
#                 # 'latest_result': latest_result,
#                 'kpi_parent_name': parent_name,
#                 'user_id': kpi.user_id,
#                 'display_name': display_name(kpi.user_id),
#                 'kpi_id': kpi.id,
#                 'assigned_to': display_name(kpi.assigned_to),
#                 'review_type': kpi.get_review_type_display(),
#                 'sub_kpis': sub_kpis or [],
#                 'score': kpi.get_score()  # kpi.user_id)
#             }
#             return HttpResponse(json.dumps(data), content_type="application/json")
#
#         return render(request, 'performance/kpi_stage_review.html', {
#             'kpi': kpi,
#             'kpis': kpis,
#             'hash': kpi.get_hash()
#         })
#
#     def post(self, request, *args, **kwargs):
#         pass
#
#
# class KPIStageAdd(PerformanceBaseView):
#
#     def post(self, request, *args, **kwargs):
#         kpi = get_object_or_404(KPI, hash=kwargs['hash'])
#
#         s = KPIRviewByStage()
#         s.kpi_id = kpi.id
#         if request.POST['review_date']:
#             review_date = parse(request.POST['review_date'], dayfirst=True)
#             s.review_date = review_date
#
#         if 'note' in request.POST:
#             s.note = request.POST['note']
#
#         if request.POST.get('real'):
#             s.real = float(request.POST['real'])
#
#         s.save()
#
#         return HttpResponse(s.to_json(), content_type="application/json")

#
# class KPIStageDelete(PerformanceBaseView):
#     def post(self, request, *args, **kwargs):
#         kpi = KPIRviewByStage.objects.filter(id=request.POST['id'])
#         if kpi.count() > 0:
#             kpi.delete()
#
#             kpi = get_object_or_404(KPI, hash=kwargs['hash'])
#             sub_kpis = KPIRviewByStage.objects.filter(kpi_id=kpi.id)
#             total = sub_kpis.count()
#             scores = []
#             for k in sub_kpis:
#                 score = k.score_to_percent()
#                 if score is not None:
#                     scores.append(score)
#
#             if scores:
#                 final_score = sum(scores) / float(total)
#                 final_score = final_score
#
#                 kpi.set_score(final_score, kpi.user_id)
#                 profile = Profile.objects.get(user_id=kpi.user_id)
#                 # if profile.report_to_id:
#                 if profile.parent:
#                     # kpi.set_score(final_score, profile.report_to_id)
#                     kpi.set_score(final_score, profile.parent.user_id if profile.parent else None)
#
#                 if kpi.parent_id:
#                     parent_score_self = kpi.parent.calculate_avg_score(kpi.user_id)
#                     parent_score = parent_score_self
#                     kpi.parent.set_score(parent_score_self, kpi.user_id)
#                     # if profile.report_to_id:
#                     if profile.parent:
#                         # parent_score = kpi.parent.calculate_avg_score(profile.report_to_id)
#                         parent_score = kpi.parent.calculate_avg_score(profile.parent.user_id)
#                         kpi.parent.set_score(parent_score, profile.parent.user_id)
#
#                     # CascadeKPIQueue.objects.create(kpi=kpi.parent)
#                     add_new_cascadeKPIQueue(kpi.parent)
#                 else:
#                     parent_score = final_score
#                     if final_score is not None and kpi.cascaded_from_id:
#                         # CascadeKPIQueue.objects.create(kpi=kpi)
#                         add_new_cascadeKPIQueue(kpi)
#
#         return HttpResponse('ok')
#


class UpdateReviewTypeKPI(PerformanceBaseView):

    def get(self, request, *args, **kwargs):
        response = [
            {'value': '', 'text': ugettext('Final Review')},
            {'value': 'daily', 'text': ugettext('Daily')},
            {'value': 'weekly', 'text': ugettext('Weekly')},
            {'value': 'monthly', 'text': ugettext('Monthly')},
        ]
        return HttpResponse(json.dumps(response), content_type="application/json")

    def post(self, request, *args, **kwargs):
        organization = request.user.get_profile().get_organization()
        quarter_period = organization.get_current_quarter()

        value = request.POST.get('value', None)
        obj_id = request.POST.get('pk')
        try:
            kpi = KPI.objects.get(pk=obj_id, quarter_period=quarter_period)
        except:
            return HttpResponse("Kpi doesn't exist.")
        if value in ['daily', 'weekly', 'monthly', '']:
            kpi.review_type = value
        else: #pragma: no cover - when run to this path, It will cause exception when save kpi. #TODO:
            kpi.review_type = None
        kpi.save_calculate_score()

        return HttpResponse(json.dumps({
            'id': kpi.id,
            'parent_id': kpi.parent_id,
            'review_type': kpi.review_type or ''
        }), content_type='application/json')


class KPIComment(PerformanceBaseView):

    def get(self, request, *args, **kwargs):
        kpi_id = request.GET.get("id")
        extra = {}
        if kpi_id:
            extra = {'id': kpi_id}

        kpi = KPI.objects.filter(unique_key=kwargs['unique_key'], **extra).order_by('-id').first()
        if not kpi:
            kpi = KPI.deleted_objects.filter(unique_key=kwargs['unique_key']).order_by('-id').first()

        # MarkAsReadComment.objects.filter(kpi=kpi, read_user=request.user).update(read_date=datetime.datetime.now())
        MarkAsReadComment.objects.filter(kpi__unique_key=kpi.unique_key, read_user=request.user).update(
            read_date=datetime.datetime.now())
        if not kpi: #pragma: no cover - can't reach this
            raise Http404
        if 'b3' in request.GET:
            return HttpResponse(kpi.get_notes_intemplate3())
        return HttpResponse(kpi.get_notes_intemplate())


class KPITarget(PerformanceBaseView):

    def get(self, request, *args, **kwargs):
        organization = self.get_organization(request.user)
        return render(request, 'performance/target/index.html', {'organization': organization})