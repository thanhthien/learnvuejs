#!/usr/bin/python
# -*- coding: utf-8 -*-
import json
import logging
import random
from datetime import date

import os

from django.db.backends.postgresql.base import DatabaseError

from authorization.rules import *
from company.models import Organization
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.cache import cache
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.http.response import HttpResponse, Http404
from django.shortcuts import render, redirect
from django.template import RequestContext
from django.template.loader import render_to_string
from django.views.generic import View
from elms import settings
from elms.settings import SLACK_DEV_CHANNEL
from landing.views import page403, page404
from performance.base.views import PerformanceBaseView
from performance.db_models.strategy_map import GroupKPI, StrategyMap
from performance.models import KPI, KpiComment, QuarterPeriod, MarkAsReadComment, \
    BSC_CATEGORY
from performance.services.common import PerformanceApi
from performance.services.kpi import KPIServices, create_child_kpi, get_kpi_cache
from performance.services.kpilib import get_function_tags
from performance.services.organization import *
from performance.services.quarter import QuarterServices, get_all_quarter_by_org, get_last_quarter, get_quarter
# from performance.templatetags.kpi import is_consultant
# from silk.profiling.profiler import silk_profile
from user_profile.models import Profile
from utils import ExcelResponse, \
    notify_slack, postpone

from utils.api.response import APIJsonResponseForbidden, APIJsonResponse500
from performance.db_models.kpi_library import TagItem, KPILib
from utils.common import set_redis, HttpResponseJsonForbidden
from django.utils.translation import ugettext, ugettext_lazy
from collections import OrderedDict

logger = logging.getLogger('django')

# THIS CLASS NOT USE ANYMORE
# class KPIWorkSheetMap(PerformanceBaseView):#pragma: no cover
#
#     def post(self, request, *args, **kwargs):
#         add_other_kpi = request.POST.get('add_other_kpi', None)
#         year = kwargs['year']
#
#         try:
#             organization = self.get_organization(request.user)
#             current_quarter = organization.get_current_quarter()
#         except:
#             raise Http404
#
#         if add_other_kpi:
#             s_map = StrategyMap.objects.filter(organization=organization,
#                                                year=year,
#                                                quarter_period=current_quarter).first()
#
#             group_other, created = GroupKPI.objects.get_or_create(map=s_map,
#                                                                   category='other')
#
#             # k = KPI()
#             # k.name = "New KPI..."
#             # k.quarter_period = current_quarter
#             # k.year = year
#             # k.save()
#             #
#
#             k = KPIServices().create_kpi(creator=request.user, name="New KPI...", year=year,
#                                          quarter_period=current_quarter)
#
#             group_other.kpis.add(k)
#             group_other.save()
#             return HttpResponseRedirect(request.get_full_path())
#
#         parent_kpi = request.POST.get('parent_kpi', None)
#         if parent_kpi:
#             # k = KPI()
#             # k.name = "New KPI..."
#             # k.year = year
#             # k.refer_to_id = int(parent_kpi)
#             # k.quarter_period = current_quarter
#             # k.bsc_category = k.refer_to.bsc_category
#             # k.save()
#             refer_to = KPI.objects.get(id=parent_kpi)
#
#             k = KPIServices().create_kpi(creator=request.user, name="New KPI...", year=year, refer_to=refer_to,
#                                          quarter_period=current_quarter,
#                                          bsc_category=refer_to.bsc_category)
#
#         level = request.POST.get('level')
#         new_row = ''
#         if level in ['1', '2', '3']:
#             new_row = render_to_string('kpi_modules/row_level' + level + '.html', {
#                 'kpi': k, 'parent': k.refer_to
#             }, context_instance=RequestContext(request))
#
#         res = {
#             'kpi_id': k.id,
#             'new_row': new_row,
#             'parent_id': k.refer_to_id,
#             'level': level
#         }
#         return HttpResponse(json.dumps(res), content_type="application/json")
#         # return HttpResponseRedirect(request.path)
#
#     def get(self, request, *args, **kwargs):
#         category = request.GET.get('category')
#         if category not in ['financial', 'customer', 'process', 'learn-growth', 'other']:
#             category = 'financial'
#
#         try:
#             organization = self.get_organization(request.user)
#             current_quarter = organization.get_current_quarter()
#         except:
#             raise Http404
#
#         if not (request.user.is_staff or Profile.objects.filter(Q(is_superuser=True) | Q(is_admin=True),
#                                                                 user=request.user).exists()):
#             if Profile.objects.filter(user=request.user):
#                 return HttpResponseRedirect(reverse('kpi_worksheet_employee', kwargs={'year': kwargs['year'],
#                                                                                       'hash': kwargs['hash'],
#                                                                                       'user_id': request.user.id}))
#
#         year = kwargs['year']
#         s_map = StrategyMap.objects.filter(organization=organization,
#                                            quarter_period=current_quarter).first()
#
#         s_maps = StrategyMap.objects.filter(organization=organization).order_by('-quarter_period_id')
#
#         years = []
#         for y in range(2014, date.today().year + 2):
#             years.append(y)
#
#         if s_map:
#             group_financial = GroupKPI.objects.filter(map=s_map,
#                                                       category='financial').order_by('id')
#             group_customer = GroupKPI.objects.filter(map=s_map,
#                                                      category='customer').order_by('id')
#             group_internal = GroupKPI.objects.filter(map=s_map,
#                                                      category='internal').order_by('id')
#             group_learning = GroupKPI.objects.filter(map=s_map,
#                                                      category='learninggrowth').order_by('id')
#         else:
#             group_financial = []
#             group_customer = []
#             group_internal = []
#             group_learning = []
#
#         group_other, created = GroupKPI.objects.get_or_create(map=s_map,
#                                                               category='other')
#
#         lang = 'en'
#         if 'lang' in request.GET and request.GET.get('lang') in ['en', 'vi']:
#             lang = request.GET.get('lang')
#
#         users = Profile.objects.filter(organization=organization,
#                                        active=True).values('user_id', 'user__email', 'display_name')
#
#         can_edit = organization.enable_to_edit()
#         return render(request, 'performance/bsc/map/kpi_worksheet.html', {
#             'map': s_map,
#             's_maps': s_maps,
#             'group_financial': group_financial,
#             'group_customer': group_customer,
#             'group_internal': group_internal,
#             'group_learning': group_learning,
#             'group_other': [group_other],
#             'year': year,
#             'ceo_id': organization.ceo_id,
#             # 'kpis': kpis,
#             'years': years,
#             'org': organization,
#             'lang': lang,
#             'category': category,
#             'users': users,
#             'can_edit': can_edit
#         })

# THIS CLASS NOT USE ANYMORE
# class KPIWorkSheetEmployee(PerformanceBaseView):
#
#     def post(self, request, *args, **kwargs):
#         add_other_kpi = request.POST.get('add_other_kpi', None)
#         year = kwargs['year']
#
#         try:
#             organization = self.get_organization(request.user)
#             current_quarter = organization.get_current_quarter()
#             uo = Profile.objects.get(user_id=kwargs['user_id'],
#                                      organization=organization)
#         except:
#             raise Http404
#
#         if 'kpi_report' in request.POST:
#             return self.export_missed_data_kpi(organization, current_quarter)
#         elif 'comment_report' in request.POST:
#             return self.comment_report(organization, current_quarter)
#         elif 'count_comments' in request.POST:
#             return self.count_comments(request, organization, current_quarter)
#         elif 'user_comment_report' in request.POST:
#             return self.comment_report(organization, current_quarter, kwargs['user_id'])
#
#         if add_other_kpi:
#             # k = KPI()
#             # k.name = "New KPI..."
#             # k.bsc_category = None
#             # k.quarter_period = current_quarter
#             # k.owner_email = uo.user.email
#             # k.user = uo.user
#             # k.year = year
#
#             k = KPIServices().create_kpi(creator=request.user, name="New KPI...", year=year,
#                                          quarter_period=current_quarter,
#                                          email=uo.user.email, user=uo.user)
#             # k.save()
#             return HttpResponseRedirect(request.get_full_path())
#
#         parent_kpi = request.POST.get('parent_kpi', None)
#         if parent_kpi:
#             # k = KPI()
#             # k.name = "New KPI..."
#             # k.year = year
#             # k.quarter_period = current_quarter
#             # k.refer_to_id = int(parent_kpi)
#             # k.bsc_category = k.refer_to.bsc_category
#             # k.save()
#
#             refer_to = KPI.objects.get(id=parent_kpi)
#
#             k = KPIServices().create_kpi(name="New KPI...", year=year, refer_to=refer_to,
#                                          quarter_period=current_quarter,
#                                          bsc_category=refer_to.bsc_category
#                                          )
#
#         level = request.POST.get('level')
#         new_row = ''
#         if level in ['1', '2', '3']:
#             new_row = render_to_string('kpi_modules/row_level' + level + '.html', {
#                 'kpi': k, 'parent': k.refer_to
#             }, context_instance=RequestContext(request))
#
#         res = {
#             'kpi_id': k.id,
#             'new_row': new_row,
#             'parent_id': k.refer_to_id,
#             'level': level
#         }
#         return HttpResponse(json.dumps(res), content_type="application/json")
#
#     def count_comments(self, request, organization, quarter_period):
#         kpi_ids = request.POST.getlist('kpi_id')
#         kpis = KPI.objects.filter(id__in=kpi_ids)
#         as_read = MarkAsReadComment.objects.filter(read_user=request.user,
#                                                    kpi__quarter_period=quarter_period,
#                                                    kpi_id__in=kpi_ids).values_list('kpi_id', flat=True)
#         kpis = kpis.exclude(id__in=as_read)
#
#         kpi_as_read = []
#         for kpi in kpis:
#             kpi_as_read.append(MarkAsReadComment(kpi_id=kpi.id,
#                                                  read_user=request.user,
#                                                  read_date=kpi.created_at))
#
#         MarkAsReadComment.objects.bulk_create(kpi_as_read)
#
#         date_filter = '''select performance_markasreadcomment.read_date from performance_markasreadcomment
#                          where performance_markasreadcomment.kpi_id=performance_kpi.id and
#                          performance_markasreadcomment.read_user_id=%s limit 1''' % (request.user.id,)
#
#         select_clause = '''select count(1) from performance_kpicomment
#                            where performance_kpicomment.kpi_id in (select id from performance_kpi as t0
#                                                                    where t0.unique_key = performance_kpi.unique_key) and
#                                  performance_kpicomment.user_id <> %s and
#                                  performance_kpicomment.created_at >= (%s)''' % (request.user.id, date_filter,)
#
#         kpis = KPI.objects.filter(id__in=kpi_ids).extra(select={'count_comments': select_clause}).values('id',
#                                                                                                          'count_comments')
#         return HttpResponse(json.dumps(list(kpis)), content_type="application/json")
#
#     def comment_report(self, organization, quarter_period, user_id=None):
#         if not user_id:
#             uo = Profile.objects.filter(
#                 Q(is_consultant=True) | Q(user__email__endswith="@cjs.vn"),
#                 organization=organization).first()
#         else:
#             uo = Profile.objects.filter(user_id=user_id,
#                                                  organization=organization).first()
#
#         if uo:
#             # comments = KpiComment.objects.filter(user=uo.user,
#             #                                      kpi__quarter_period=quarter_period) \
#             #     .values_list('content', 'kpi__name', 'kpi__user__email',
#             #                  'kpi__user__profile__report_to__email',
#             #                  'kpi__user_id').order_by('kpi__user_id')
#             comments = KpiComment.objects.filter(user=uo.user,
#                                                  kpi__quarter_period=quarter_period) \
#                 .values_list('content', 'kpi__name', 'kpi__user__email',
#                              'kpi__user__profile__parent__user__email',
#                              'kpi__user_id').order_by('kpi__user_id')
#
#             response = []
#             url = settings.DOMAIN  # 'http://%s' % (Site.objects.get_current(),)
#             for obj in comments:
#                 item = list(obj)
#                 link = url + reverse('all_kpi') + "?team_id=" + str(item[-1])
#                 item[-1] = link
#                 response.append(item)
#
#             headers = ['Comment', 'KPI', 'Employee Email', 'Manager Email', 'Link']
#             return ExcelResponse(response, 'Comments report', headers=headers)
#         else:
#             return HttpResponse("Consultant is not exist.")
#
#     def export_missed_data_kpi(self, organization, quarter_period):
#
#         # uos = UserOrganization.objects.filter(organization=organization,
#         #                                       active=True).exclude(user_id=organization.ceo_id).order_by(
#         #     'user__profile__report_to_id')
#         uos = Profile.objects.filter(organization=organization,
#                                      active=True).exclude(user_id=organization.ceo_id).order_by('parent_id')
#
#         current_quarter = organization.get_current_quarter()
#         response = []
#         url = settings.DOMAIN  # 'http://%s' % (Site.objects.get_current(),)
#         i = 1
#         for uo in uos:
#             # if obj[2] > 0 or obj[3] > 0 or obj[4] > 0 or obj[5] > 0:
#             item = list()  # list(obj)
#             item.append(i)
#             item.append(uo.display_name)
#             item.append(uo.user.username)
#             item.append(uo.user.email)
#             # if uo.user.profile.report_to:
#             if uo.parent and uo.parent.user:
#                 item.append(uo.parent.user.email)
#             else:
#                 item.append("")
#
#             item.append(uo.title)  # position
#             # count parent kpis
#             no_parent_kpi = KPI.objects.filter(user=uo.user,
#                                                owner_email=uo.user.email,
#                                                parent=None,
#                                                quarter_period=current_quarter).count()
#             item.append(no_parent_kpi)
#
#             no_child_kpi = KPI.objects.filter(user=uo.user,
#                                               owner_email=uo.user.email,
#                                               quarter_period=current_quarter) \
#                 .exclude(parent=None).count()
#             item.append(no_child_kpi)
#
#             item.append("")
#             link = url + reverse('kpi_editor_emp', kwargs={
#                 # 'year': quarter_period.year,
#                 'user_id': uo.user.id}) + "?category=all"
#             item.append(link)
#             response.append(item)
#
#             i += 1
#
#         headers = ['No.', 'Employee Name', 'Username',
#                    'Employee Email', 'Manager Email',
#                    'Position', 'No. Parent KPI', 'No. Child KPI',
#                    'Level of Completion (A: Perfect, B: < 5 Erros, C: >5 erros )', 'Link']
#         return ExcelResponse(response, 'KPI report', headers=headers)
#
#     def get(self, request, *args, **kwargs):
#         category = request.GET.get('category')
#         if category not in ['financial', 'customer', 'process', 'learn-growth', 'other']:
#             category = 'financial'
#
#         try:
#             organization = Organization.objects.get(hash=kwargs['hash'])
#             current_quarter = organization.get_current_quarter()
#             uo = Profile.objects.get(user=kwargs['user_id'], organization=organization)
#         except:
#             raise Http404
#
#         print "view kpi worksheet: " + str(uo.user)
#
#         team = uo.get_my_team(me=True)
#         # if not request.user.is_staff and \
#         #         not UserOrganization.objects.filter(Q(is_superuser=True) | Q(is_admin=True),
#         #                                             user=request.user).exists() and \
#         #                 request.user.id != uo.user.profile.report_to_id and \
#         #                 request.user.get_profile() not in team:
#         parent_user_id = uo.parent.user_id if uo.parent_id else None
#         if not request.user.is_staff and \
#                 not Profile.objects.filter(Q(is_superuser=True) | Q(is_admin=True),
#                                            user=request.user).exists() and \
#                         request.user.id != parent_user_id and \
#                         request.user.profile not in team:
#
#             if Profile.objects.filter(user=request.user):
#                 return HttpResponseRedirect(reverse('kpi_worksheet_employee', kwargs={'year': kwargs['year'],
#                                                                                       'hash': kwargs['hash'],
#                                                                                       'user_id': request.user.id}))
#
#             users = request.user.profile.get_all_subordinate()
#         else:
#             users = Profile.objects.filter(organization=organization,
#                                                     active=True).values('user_id', 'user__email',
#                                                                         'display_name')
#
#         year = kwargs['year']
#         s_map = StrategyMap.objects.filter(organization=organization,
#                                            # year=year,
#                                            quarter_period=current_quarter).first()
#
#         years = []
#         for y in range(2014, date.today().year + 2):
#             years.append(y)
#
#         email = uo.user.email
#
#        # KPI.objects.filter(owner_email=email,
#          #                  quarter_period__isnull=True).update(quarter_period=current_quarter)
#         financial_kpis = KPI.objects.filter(owner_email=email,
#                                             user=uo.user,
#                                             parent=None,
#                                             quarter_period=current_quarter,
#                                             bsc_category='financial').exclude(parent__owner_email=email).order_by(
#             'ordering')
#         customer_kpis = KPI.objects.filter(owner_email=email,
#                                            user=uo.user,
#                                            parent=None,
#                                            quarter_period=current_quarter,
#                                            bsc_category='customer').exclude(parent__owner_email=email).order_by(
#             'ordering')
#         internal_kpis = KPI.objects.filter(owner_email=email,
#                                            user=uo.user,
#                                            parent=None,
#                                            quarter_period=current_quarter,
#                                            bsc_category='internal').exclude(parent__owner_email=email).order_by(
#             'ordering')
#         learning_kpis = KPI.objects.filter(owner_email=email,
#                                            user=uo.user,
#                                            parent=None,
#                                            quarter_period=current_quarter,
#                                            bsc_category='learninggrowth').exclude(parent__owner_email=email).order_by(
#             'ordering')
#
#         other_kpis = KPI.objects.filter(Q(bsc_category__isnull=True) | Q(bsc_category='') | Q(bsc_category='other'),
#                                         owner_email=email,
#                                         user=uo.user,
#                                         parent=None,
#                                         quarter_period=current_quarter).exclude(parent__owner_email=email).order_by(
#             'ordering')
#
#         lang = 'en'
#         if 'lang' in request.GET and request.GET.get('lang') in ['en', 'vi']:
#             lang = request.GET.get('lang')
#
#         can_edit = organization.enable_to_edit()
#         return render(request, 'performance/bsc/map/kpi_worksheet_employee.html', {
#             'map': s_map,
#             'ceo_id': organization.ceo_id,
#             'financial_kpis': financial_kpis,
#             'customer_kpis': customer_kpis,
#             'internal_kpis': internal_kpis,
#             'learning_kpis': learning_kpis,
#             'other_kpis': other_kpis,
#             'year': year,
#             'hash': organization.hash,
#             'org_user': uo.user,
#             # 'kpis': kpis,
#             'years': years,
#             'org': organization,
#             'lang': lang,
#             'category': category,
#             'users': users,
#             'can_edit': can_edit
#         })


# @method_decorator(silk_profile, name='dispatch')
def fix_kpi_expanding(kpi):

    if kpi:
        if not os.getenv('fix_self_threading'):
            kpi.fix_self_threading()
        else:
            kpi.fix_self()
        # from performance.services import add_new_cascadeKPIQueue
        # add_new_cascadeKPIQueue(kpi)

        cache.set('fix_kpi_expanding{}'.format(kpi.id), kpi.id)
        set_redis('fix_kpi_expanding{}'.format(kpi.id), kpi.id, 30)


# to test only 200 response
class KPIWorkSheetDetail(PerformanceBaseView):

    def get(self, request, *args, **kwargs):
        #  follow_page=request.GET.get('follow_page', False)

      #  return StreamingHttpResponse(self._get(request, *args, **kwargs))
        return self._get(request, *args, **kwargs)

    def _get(self, request, *args, **kwargs):
       # yield ''  # this is to trigger connection to avoid heroku timeout
        # https://devcenter.heroku.com/articles/request-timeout#long-polling-and-streaming-responses
        kpi_id = int(kwargs['kpi_id'])
        kpi_unique_key = request.GET.get('kpi_unique_key', '')
        follow_page = request.GET.get('follow_page', False)
        load_child = request.GET.get('load_child', '0')
        reload = request.GET.get('reload', '0')
        level = int(request.GET.get('level', 1))
        has_child_loaded = request.GET.get('has_child_loaded', '0')
        kpi_search = request.GET.get('kpi_search', None)
        kpi_wrapper = request.GET.get('kpi_wrapper', None)
        public_view = request.GET.get('public_view', None)

        specified_quarter = request.GET.get('quarter', None)

        response = None
        if kpi_id == 0 and not kpi_unique_key:  # pragma: no cover
            raise Http404

        remote_organization = request.user.profile.get_organization()
        current_quarter = remote_organization.get_current_quarter()

        super_permission = False

        if public_view is not None:
            public_view = True
        else:
            public_view = False

        target_org_id = None
        if 'target_org' in request.GET:
            try:
                target_org_id = int(float(request.GET.get('target_org')))
            except Exception as e:  # pragma: no cover
                target_org_id = -1

        # Code refactored to cover case of target_organization being None after `get_organization(target_org_id)`
        target_organization = None

        if target_org_id == None:
            target_organization = remote_organization

        elif target_org_id < 0:  # pragma: no cover
                raise Http404

        else:
            if target_org_id:
                target_organization = get_organization(target_org_id)

            if not target_organization:
                target_organization = remote_organization

        # if target_org_id == None:not
        #     target_organization = remote_organization
        # else:
        #     if target_org_id < 0:  # pragma: no cover
        #         raise Http404
        #     else:
        #         target_organization = get_organization(target_org_id)
        #         # target_organization = Organization.objects.get(id=target_org_id)

        # TODO: authorization here to avoid hacking

        the_quarter = current_quarter
        if specified_quarter:
            the_quarter = QuarterPeriod.objects.get(id=specified_quarter)
        if kpi_id == 0:
            kpi = KPI.objects.filter(unique_key=kpi_unique_key, quarter_period=the_quarter,
                                     assigned_to__isnull=True).first()  # not kpi trung gian

            if not kpi:  # pragma: no cover
                response = HttpResponse("KPI not found!")

        else:

            kpi = get_kpi_cache(kpi_id, request.user)

        kpi_user = None
        if kpi:
            kpi_user = kpi.user
        if has_perm(SETTINGS__VIEW_ORG_CHART, request.user, kpi_user):
            super_permission = True

        if not response:
            '''
            QUICK HACK - SHOULD use Better way
            '''
            if kpi and kpi.score_calculation_type == 'most_recent':
                save_kpi = False
                if kpi.month_1_target is None:
                    kpi.month_1_target = kpi.get_month_1_target()
                    save_kpi = True
                if kpi.month_2_target is None:
                    kpi.month_2_target = kpi.get_month_2_target()
                    save_kpi = True
                if kpi.month_3_target is None:
                    kpi.month_3_target = kpi.get_month_3_target()
                    save_kpi = True

                if save_kpi:
                    try:
                        kpi.save()
                    except DatabaseError:  # pragma: no cover
                        pass

            '''
            END QUICK HACK
            '''
            if kpi:
                enable_edit = has_perm(KPI__EDITING, request.user, kpi.user)
                enable_review = has_perm(KPI__REVIEWING, request.user, kpi.user, kpi)
                enable_delete = has_perm(DELETE_KPI, request.user, kpi.user, kpi)
            else:
                enable_delete = enable_edit = enable_review = False
            # fix kpi bug if there click detail
            # recalculate score
            if follow_page == False or follow_page == '0' or reload == '1':
                fix_kpi_expanding(kpi)
                # pass
            random_num = int(random.randint(0, 1000000))  # for dynamic id of kpi-archivable-score
            last_quarter = get_last_quarter(actor=request.user, organization=target_organization)

            response = render(request, 'performance/bsc/worksheet/modules/worksheet.html', {
                # 'quarter': current_quarter,
                'quarter': the_quarter,
                'kpi': kpi,
                'remote_organization': remote_organization,
                'target_organization': target_organization,
                'cross_review': target_organization.cross_review,
                'ceo_id': target_organization.ceo_id,
                'super_permission': super_permission,
                'public_view': public_view,
                'random_num': random_num,
                'last_quarter':last_quarter,
                'kpi_search': kpi_search,
                'follow_page': follow_page,
                'load_child': load_child,
                'reload': reload,
                'level': level,
                'has_child_loaded': has_child_loaded,
                'kpi_wrapper': kpi_wrapper,

                'enable_edit': 'true' if enable_edit else 'false',
                'enable_review': 'true' if enable_review else 'false',
                'enable_delete': 'true' if enable_delete else 'false',

            })
            cache.set('kpi_editor_{}'.format(kpi_id), response.content, 120)
            # caching will be expired after 120 seconds and delete in KPI.save() cache.delete( 'kpi_editor_{}'.format(self.id))

        # yield response.content

        return response


class KPIMindMap(PerformanceBaseView):  # pragma: no cover

    def get(self, request, *args, **kwargs):
        user_id = kwargs['user_id']
        kpi_user = User.objects.get(id=user_id)

        response = render(request, 'performance/visualization/mindmap.html', {'kpi_user': kpi_user})

        return response


class KPIMindMapPub(View):

    def get(self, request, *args, **kwargs):
        user_id = kwargs['user_id']
        kpi_user = User.objects.get(id=user_id)
        is_admin = request.user.profile.is_admin
        if not is_admin:
            return page403(request)
        response = render(request, 'performance/visualization/mindmap_pub.html', {'kpi_user': kpi_user})

        return response


@postpone
def fix_user_organzation(user_viewed, target_organization):  # pragma: no cover
    try:

        # user-viewed must instance of UserOrganization

        uo = Profile.objects.filter(user_id=user_viewed.id,
                                    organization=target_organization)

        uo_count = uo.count()
        if uo_count > 1 or uo_count == 0:

            uo, created = Profile.objects.get_or_create(user_id=user_viewed.id,
                                                        organization=target_organization)
        else:
            uo = uo.first()
    except Exception as e:
        notify_slack(SLACK_DEV_CHANNEL, "Error: performance/views.py: line 6813: {0}".format(str(e)))
        raise Http404


# @postpone
def fix_quarter(user, current_quarter):

    key = "fix_quarter{}".format(user.id)
    if cache.get(key, None):
        return
    cache.set(key, True, 60 * 60 * 24)
    # print 'fix_quarter not cached'

    KPI.objects.filter(user_id=user.id,
                       quarter_period__isnull=True).update(quarter_period=current_quarter)


@login_required()
def kpi_editor_year(request):
    return render(request, 'v5/kpi-editor/year.html', {})


class KPIWorkSheetEmployeeNew(PerformanceBaseView):

    def get(self, request, *args, **kwargs):
        user_id = kwargs['user_id']
        request_public = False
        try:
            user_id = int(user_id)
            profile = Profile.objects.select_related('user').get(user_id=user_id)
            user_viewed = profile.user
        except:
            # try with request for public kpi-editor
            # now, asumpt user_id is hash code
            request_public = True
            profile = Profile.objects.filter(hash=user_id).first()
            if profile and profile.allow_public_view:
                user_viewed = profile.user
            else:
                raise Http404
        if profile.is_deleted:
            return redirect(reverse('kpi_dashboard'))

        profile.follow_team()

        if has_perm(USER__BELONG_TO_ORG, request.user, user_viewed):
            return self.process_get(user_viewed, True, request_public=request_public)

        raise Http404(ugettext("You don't have permission to access this page"))

    def get_kpi_editor_permission(self, user_viewed, for_report_user=False, request_public=False):
        '''
        for_report_user: use when request public kpi-editor
            if for_report_user==True: we don't need check anything more.
            Because request-public-kpi-editor would never have any permissions to Report user kpis
        '''
        request = self.request
        user_can_edit = False  # unuse
        user_can_view = False  # unuse
        permission = False  # user can view or edit
        super_permission = False
        public_view = False

        if user_viewed:
            remote_organization = self.get_organization(request.user)
            target_organization = self.get_organization(user_viewed)

            if request_public \
                    and not for_report_user \
                    and user_viewed.get_profile().allow_public_view:
                public_view = True
                user_can_view = True
                user_can_edit = False
                permission = True

            # #
            # # request user & user-viewed may not the same organization
            # #
            if remote_organization == target_organization:
                # public_view = False
                # super permission
                if  has_perm(SETTINGS__VIEW_ORG_CHART, request.user, user_viewed):  # request.user.profile.org_super_permission():
                    user_can_edit = True
                    user_can_view = True
                    permission = True
                    super_permission = True

                # team permission
                # elif request.user.id == user_viewed.id \
                #         or request.user.get_profile().report_to_id == user_viewed.id \
                #         or user_viewed.get_profile().is_subordinate_of(request.user.get_profile()):
                # all follwing cases are covered in above permission if block, so excluded from cover.
                elif request.user.id == user_viewed.id \
                        or request.user.profile.parent == user_viewed.profile \
                        or user_viewed.profile.is_subordinate_of(request.user.profile):  # pragma: no cover
                    user_can_view = True
                    permission = True
                    # if request.user.get_profile().report_to_id == user_viewed.id:
                    if request.user.profile.parent == user_viewed.profile:
                        user_can_edit = False
                        permission = True
                    else:
                        user_can_edit = True
                        permission = False

        return {'permission': permission,
                'super_permission': super_permission,
                'user_can_edit': user_can_edit,
                'user_can_view': user_can_view,
                'public_view': public_view,
                }

    def process_get(self, user_viewed, kpi_dashboard=False, editing_vision=False, request_public=False):
        request = self.request
        quarter_id = request.GET.get('quarter_id', '')
        remote_organization = self.get_organization(request.user)
        target_organization = self.get_organization(user_viewed)
        # report_to_user = user_viewed.get_profile().report_to
        parent_user = user_viewed.profile.parent.user if user_viewed.profile.parent else None

        user_viewed_email = user_viewed.email
        user_viewed_display_name = user_viewed.get_profile().display_name

        # perm__viewed_user_kpi=False
        # perm__report_to_user_kpi=False
        # super_permission=False
        # public_view=True
     #   logger.info('kpi_editor 739')

        parents_financial = KPI.objects.none()
        parents_customer = KPI.objects.none()
        parents_internal = KPI.objects.none()
        parents_learning = KPI.objects.none()
        parents_other = KPI.objects.none()

        # financial_count = 0
        # customer_count = 0
        # internal_count = 0
        # learning_count = 0
        # other_count = 0

        parents_financial_report_to = KPI.objects.none()
        parents_customer_report_to = KPI.objects.none()
        parents_internal_report_to = KPI.objects.none()
        parents_learning_report_to = KPI.objects.none()
        parents_other_report_to = KPI.objects.none()

        # check if user_view is my_kpi or team_kpi, or not
        my_kpi = None
        my_team_kpi = None
        # user_report_to_id = None
        # consultant = False
        ceo_kpi = False
        # my_report_to_id = None
        parent_user_email = ''
        parent_user_display_name = ''

     #   logger.info('kpi_editor 761')

        is_search_page = False
        search_terms = ''
        page = 'kpi-editor'
        if request.resolver_match.view_name == u'kpi_editor_search':
            # search page
            is_search_page = True
            page = 'kpi-search'
        if quarter_id:
            print 'quarter_id:' + quarter_id
            current_quarter = get_quarter(actor=request.user, quarter_id=quarter_id)
        else:
            current_quarter = target_organization.get_current_quarter()
        if current_quarter:
            year = current_quarter.year
        else:
            raise Http404("Quarter Not exist")

        fix_user_organzation(user_viewed, target_organization)

        permission__viewed_user_kpi = self.get_kpi_editor_permission(user_viewed, request_public=request_public)

        # QuocDuan removed this because of the current editor donot need to check permission of view manager kpi when enter to (self) personal kpi
        # permission__parent_user_kpi = self.get_kpi_editor_permission(parent_user, True,
        #                                                                 request_public=request_public)

        perm__viewed_user_kpi = permission__viewed_user_kpi['permission']
        # perm__parent_user_kpi = permission__parent_user_kpi['permission']
        perm__parent_user_kpi = True
        super_permission = permission__viewed_user_kpi['super_permission']  # or permission__parent_user_kpi['super_permission']
        public_view = permission__viewed_user_kpi['public_view']

    #   logger.info('kpi_editor 804')

        s_map = StrategyMap.get(target_organization.id, current_quarter.id)

        fix_quarter(user_viewed, current_quarter)

        # ONLY FOR KPI-EDITOR
        if not is_search_page:
            if perm__viewed_user_kpi:
                parents_financial = KPI.get_parent_kpis_by_category(
                    user_viewed.id, current_quarter.id, 'financial')
                parents_customer = KPI.get_parent_kpis_by_category(user_viewed.id, current_quarter.id, 'customer')
                parents_internal = KPI.get_parent_kpis_by_category(user_viewed.id, current_quarter.id, 'internal')
                parents_learning = KPI.get_parent_kpis_by_category(user_viewed.id, current_quarter.id, 'learninggrowth')
                parents_other = KPI.get_parent_kpis_by_category(user_viewed.id, current_quarter.id, 'other')
            if parent_user:
                parent_user_display_name = parent_user.get_profile().display_name
            # if perm__viewed_user_kpi and perm__parent_user_kpi and parent_user:
            #     parent_user_display_name = parent_user.get_profile().display_name

                # Note: Hong remove 2017 to increase speed

                # report_to_user_email = report_to_user.email
                #
                #
                # parents_financial_report_to = KPI.get_parent_kpis_by_category(report_to_user.id,
                #                                                               current_quarter.id,
                #                                                               'financial')
                # parents_customer_report_to = KPI.get_parent_kpis_by_category(report_to_user.id, current_quarter.id,
                #                                                              'customer')
                # parents_internal_report_to = KPI.get_parent_kpis_by_category(report_to_user.id, current_quarter.id,
                #                                                              'internal')
                # parents_learning_report_to = KPI.get_parent_kpis_by_category(report_to_user.id, current_quarter.id,
                #                                                              'learninggrowth')
                # parents_other_report_to = KPI.get_parent_kpis_by_category(report_to_user.id, current_quarter.id,
                #                                                           'other')

        else:
            # KPI SEARCH PAGE
            search_terms = self.request.GET.get('q', '')
            cross_review = target_organization.cross_review
            if not cross_review and search_terms == 'reviewer_email':
                return page404(request)
            if search_terms:
                parents_financial = KPI.get_root_kpi_by_search_terms(search_terms, request.user.id,
                                                                     current_quarter.id,
                                                                     'financial')
                parents_customer = KPI.get_root_kpi_by_search_terms(search_terms, request.user.id, current_quarter.id,
                                                                    'customer')
                parents_internal = KPI.get_root_kpi_by_search_terms(search_terms, request.user.id, current_quarter.id,
                                                                    'internal')
                parents_learning = KPI.get_root_kpi_by_search_terms(search_terms, request.user.id,
                                                                    current_quarter.id, 'learninggrowth')
                parents_other = KPI.get_root_kpi_by_search_terms(search_terms, request.user.id, current_quarter.id,
                                                                 'other')

            pass

        lang = 'en'
        if 'lang' in request.GET and request.GET.get('lang') in ['en', 'vi']:
            lang = request.GET.get('lang')

        # quarters = QuarterPeriod.objects.filter(organization=target_organization, status__in=['IN', 'AR']).order_by(
        #     'due_date')
        quarters = get_all_quarter_by_org(request.user, target_organization)
        quarter_start = QuarterServices.get_quarter_start(target_organization)
    #    logger.info('kpi_editor 881')
        # https://artemrudenko.wordpress.com/2013/04/12/python-finding-a-key-of-dictionary-element-with-the-highestmin-value/

        # from utils.common import get_seachable_user_list
        # seachable_user_list=get_seachable_user_list(target_organization.id)

        # seachable_user_list=cache.get('searchable_user_list_{}'.format(target_organization.id))
        seachable_user_list = None

        if not seachable_user_list:
            # user_id_list=cache.get('user_id_list_{}'.format(target_organization.id))
            user_id_list = None
            if not user_id_list:
                user_id_list = Profile.objects.filter(organization=target_organization).values_list('user_id', flat=True)
                cache.set('user_id_list_{}'.format(target_organization.id), user_id_list, 60 * 10)
            # test_kpi=KPI.objects.get(id=338193)
            from django.db.models import Value, CharField
            from django.db.models.functions import Concat
            # Author.objects.using('default').all()

            list_name_1 = Profile.objects.filter(user_id__in=user_id_list, display_name__isnull=False).exclude(is_deleted=True).values('display_name', 'user__email', 'user_id') \
               .annotate(nameofuser=Concat('display_name', Value(' ('), 'user__email', Value(') |'), 'user_id', output_field=CharField())).values_list('nameofuser', flat=True)
            # list_name_1 = Profile.objects.using('alias_default').filter(user_id__in=user_id_list, display_name__isnull=False).values('display_name', 'user__email', 'user_id') \


            # list_name_2 = Profile.objects.using('alias_default').filter(user_id__in=user_id_list, display_name__isnull=True) \
            list_name_2 = Profile.objects.filter(user_id__in=user_id_list, display_name__isnull=True).exclude(is_deleted=True) \
                .values('user_id', 'user__email').annotate(nameofuser=Concat('user__email', Value('|'), 'user_id', output_field=CharField())).values_list('nameofuser', flat=True)

            list_name = list(list_name_1) + list(list_name_2)
            seachable_user_list = list_name
            cache.set('searchable_user_list_{}'.format(target_organization.id), list_name, 60 * 10)

    #    logger.info('kpi_editor 905')
        DEPARTMENTS = get_function_tags()
        EXTRA_FIELDS = OrderedDict(KPILib.EXTRA_FIELDS)

        for (key, value) in EXTRA_FIELDS.iteritems():
            EXTRA_FIELDS[key] = unicode(value)

        bsc_category = dict(BSC_CATEGORY)
        for (key, val) in bsc_category.iteritems():
            bsc_category[key] = unicode(val)

        response = render(request, 'performance/bsc/worksheet/kpi_worksheet_employee.html', {
            'map': s_map,
            'ceo_id': target_organization.ceo_id,
            'parents_financial': parents_financial,
            'parents_customer': parents_customer,
            'parents_internal': parents_internal,
            'parents_learning': parents_learning,
            'parents_other': parents_other,

            # 'parents_financial_report_to': parents_financial_report_to,
            # 'parents_customer_report_to': parents_customer_report_to,
            # 'parents_internal_report_to': parents_internal_report_to,
            # 'parents_learning_report_to': parents_learning_report_to,
            # 'parents_other_report_to': parents_other_report_to,

            #  'financial_count': financial_count,
            #  'customer_count': customer_count,
            #  'internal_count': internal_count,
            #  'learning_count': learning_count,
            #  'other_count': other_count,
            'year': year,
            'hash': target_organization.hash,
            'org_user': user_viewed,
            'user_viewed': user_viewed,
            # 'kpis': kpis,
            # 'years': years,
            'remote_organization': remote_organization,
            'target_organization': target_organization,
            'lang': lang,
            #  'users': searchable_list,
            #  'userlist': userlist_data_list,
            'quarters': quarters,

            # 'my_report_to_id': my_report_to_id,

            'super_permission': super_permission,
            # 'perm_can_edit__viewed_user_kpi': permission__viewed_user_kpi['user_can_edit'],
            # 'perm_can_view__viewed_user_kpi': permission__viewed_user_kpi['user_can_view'],
            'perm__viewed_user_kpi': perm__viewed_user_kpi,
            # 'perm__report_to_user_kpi': perm__parent_user_kpi, # the variable is not use anymore --> removed

            'public_view': public_view,

            'editing_vision': editing_vision,
            # 'editing_vision':False,
            'my_kpi': my_kpi,
            'my_team_kpi': my_team_kpi,
            'ceo_kpi': ceo_kpi,
            'user_viewed_display_name': user_viewed_display_name,
            'report_to_user_display_name': parent_user_display_name,
            # 'user_report_to_id': user_report_to_id,
           # 'consultant': consultant,  # TODO: remove this

            'quarter': current_quarter,
            'quarter_start': quarter_start,
            # collapse states
            'kpi_team_section_collapse': 'collapse',  # kpi_team_section_collapse,
            'kpi_personal_section_collapse': 'expanse',  # kpi_personal_section_collapse,
            # 'test_kpi':test_kpi,
            'page': page,
            'is_search_page': is_search_page,
            'search_terms': search_terms,
            'seachable_user_list':json.dumps(list(seachable_user_list)),  # TODO: fix this with ajax
            'is_superuser': request.user.profile.is_superuser_org(),
            'is_admin': request.user.profile.is_admin,
            'cross_review': target_organization.cross_review,
            'user_request': request.user,
            'DEPARTMENTS': json.dumps(DEPARTMENTS),
            'BSC_CATEGORY': json.dumps(bsc_category),
            'EXTRA_FIELDS': json.dumps(EXTRA_FIELDS),
            'is_manager': user_viewed.profile.is_subordinate_of(request.user.profile)
        })

        # if not is_search_page:
        #     response.set_cookie('kpi-team-section-collapse', kpi_team_section_collapse, 7 * 24 * 60 * 60)
        #     response.set_cookie('kpi-personal-section-collapse', kpi_personal_section_collapse, 7 * 24 * 60 * 60)

        return response

    def post(self, request, *args, **kwargs):

        # year = kwargs['year']

        try:
            organization = self.get_organization(request.user)
            current_quarter = organization.get_current_quarter()
            uo = Profile.objects.get(user_id=kwargs['user_id'],
                                              organization=organization)
        except:
            raise Http404

        return self.process_post(request, organization, current_quarter, uo)

    def process_post(self, request, organization, current_quarter, uo):
        add_other_kpi = request.POST.get('add_other_kpi', None)
        last_quarter = get_last_quarter(actor=request.user, organization=organization)
        """
        Ankita commented kpi_report code after confirming with Mr. Hong
        """
        # if 'kpi_report' in request.POST:
        #     return self.export_missed_data_kpi(organization, current_quarter)
        # elif 'comment_report' in request.POST:
        #     return self.comment_report(organization, current_quarter)
        if 'count_comments' in request.POST:
            return self.count_comments(request, organization, current_quarter)
        elif 'user_comment_report' in request.POST:
            return self.comment_report(organization, current_quarter, uo.user_id)

        parent_kpi = request.POST.get('parent_kpi', None)

        year = current_quarter.year
        if parent_kpi:
            parent = KPI.objects.filter(id=parent_kpi).first()
        else:
            parent = None

        if parent_kpi and parent:
            # clear cache when add KPI
            key = "has_child{}".format(parent.id);
            if cache.get(key, None) is not None:
                cache.delete(key)
            k = create_child_kpi(parent, current_quarter, actor=request.user)

            # k = KPIServices().create_kpi(creator=request.user, year=year,
            #                              quarter_period=current_quarter,
            #                              refer_to_id=parent_kpi,
            #                              name=parent.name,
            #                              unit=parent.unit,
            #                              operator=parent.operator,
            #                              current_goal=parent.current_goal,
            #                              future_goal=parent.future_goal,
            #                              quarter_one_target=parent.quarter_one_target,
            #                              quarter_two_target=parent.quarter_two_target,
            #                              quarter_three_target=parent.quarter_three_target,
            #                              quarter_four_target=parent.quarter_four_target,
            #                              year_target=parent.year_target,
            #                              archivable_score=parent.archivable_score,
            #                              score_calculation_type=parent.score_calculation_type,
            #
            #                              )  # add_other_kpi=add_other_kpi,
            #  bsc_category=request.POST['category']
            # )
            # k.refer_to_id = int(parent_kpi)
            # k.bsc_category = k.refer_to.bsc_category
        else:
            print 'line 1083'
            if not uo.active:
                return APIJsonResponseForbidden(ugettext("You can't add KPI to the disabled user"))

            if 'from_lib' in request.POST:
                kpi_lib = json.loads(request.POST.get('kpilib'))
                k = KPIServices().create_kpi(name=kpi_lib['name'], year=year,
                                             quarter_period=current_quarter,
                                             bsc_category=kpi_lib['category'],
                                             user_id=uo.user_id,
                                             owner_email=uo.user.email,
                                             creator=request.user,
                                             unit=kpi_lib['extra_data'].get('unit'),
                                             description=kpi_lib['description'],
                                             current_goal=kpi_lib['measurement_method'],
                                             operator=kpi_lib['operator'])
            else:
                k = KPIServices().create_kpi(name="", year=year,
                                         quarter_period=current_quarter,
                                         #   parent_id=parent_kpi,# add_other_kpi=add_other_kpi,
                                         bsc_category=request.POST.get('category', ''),
                                         user_id=uo.user_id,
                                         owner_email=uo.user.email,
                                         creator=request.user
                                         )
            print 'line 1091'
            # k.user_id = uo.user_id
            # k.owner_email = uo.user.email
            # if add_other_kpi:
            # k.bsc_category = None
            # elif 'category' in request.POST:
            #     k.bsc_category = request.POST['category']
            # k.ordering = k.get_next_ordering()
            # k.save()
        print 'line 1100'
        super_permission = False
        # if  request.user.is_staff or is_consultant(request.user)\
        #     or UserOrganization.objects.filter(Q(is_superuser=True) | Q(is_admin=True),
        #                                             user=request.user).exists():
        if request.user.profile.org_super_permission():
            super_permission = True

        level = request.POST.get('level')
        new_row = ''
        if level in ['1', '2', '3']:
            enable_edit = has_perm(KPI__EDITING, request.user, k.user)
            enable_review = has_perm(KPI__REVIEWING, request.user, k.user)

            # new_row = render_to_string('performance/bsc/worksheet/modules/row_level' + level + '.html',
            # new_row = render_to_string('performance/bsc/worksheet/modules/row.html',
            new_row = render_to_string('performance/bsc/worksheet/modules/row_container.html',
                                       {
                                           'kpi': k,
                                           'parent': k.refer_to,
                                           'quarter': current_quarter,
                                           'organization': organization,
                                           'target_organization': organization,
                                           'ceo_id': organization.ceo_id,
                                           'super_permission': super_permission,
                                            'last_quarter':last_quarter,
                                           'include_row': 1,
                                           'follow_page': 0,
                                           'reload': '1',
                                           'is_child': True if parent and level == '1' else False,
                                           'loading': 0,
                                           'level': int(level),
                                           'enable_edit': 'true' if enable_edit else 'false',
                                           'enable_review': 'true' if enable_review else 'false',
                                       }, context_instance=RequestContext(request))

        res = {
            'kpi_id': k.id,
            'new_row': new_row,
            'parent_id': k.refer_to_id,
            'level': level,
            'status': 'ok'
        }
        return HttpResponse(json.dumps(res), content_type="application/json")

    def count_comments(self, request, organization, quarter_period):  # pragma: no cover
        '''
        SELECT (
                -- count comments of the kpi of other users that the user not read.
                select count(1)

                from performance_kpicomment kpicomment

                where kpicomment.kpi_id = performance_kpi.id
                   and kpicomment.user_id <> 1   -- khong phai user do create

                    -- kpicomment.created_at >= kpi.created_at
                    -- default: marc.read_date=kpi.created_at,
                    -- IF read: marc.read_date = date_read ==> marc.read_date > kpicomment.created_at
                    -- ELSE: marc.read_date=kpi.created_at ==> marc.read_date < kpicomment.created_at
                   and kpicomment.created_at >=
                        ---- SELECT read_date of marc related to user_id & kpi_id
                        -- explain: lan cuoi cung user read la khi nao?
                       (
                            select MAX(marc.read_date)
                            from performance_markasreadcomment marc
                            where marc.kpi_id=performance_kpi.id
                                and marc.read_user_id=1
                        )

               )

        AS "count_comments",
        performance_kpi.id

        FROM "performance_kpi"

        WHERE (
            performance_kpi.removed IS NULL
            AND performance_kpi.id IN (260, 273, 274, 238)
        )
        '''

        kpi_ids = request.POST.getlist('kpi_id')
        kpi_ids = list(set(kpi_ids))
        kpis = KPI.objects.filter(id__in=kpi_ids)

        # select những kpi_id mà đã tạo object MarkAsReadComment
        kpi_ids_has_marc = MarkAsReadComment.objects.filter(read_user=request.user,
                                                            # kpi__quarter_period=quarter_period,
                                                            kpi_id__in=kpi_ids).values_list('kpi_id', flat=True)
        kpis = kpis.exclude(id__in=kpi_ids_has_marc)
        kpi_as_read = []
        for kpi in kpis:
            kpi_as_read.append(MarkAsReadComment(kpi_id=kpi.id,
                                                 read_user=request.user,
                                                 read_date=kpi.created_at))

        MarkAsReadComment.objects.bulk_create(kpi_as_read)

        # ---- SELECT read_date of performance_markasreadcomment related to user_id & kpi_id
        # -- explain: lan cuoi cung user read la khi nao?
        last_read_date = '''select MAX(performance_markasreadcomment.read_date) from performance_markasreadcomment
                         where performance_markasreadcomment.kpi_id=performance_kpi.id and
                         performance_markasreadcomment.read_user_id=%s''' % (request.user.id,)

        # -- count comments of the kpi of other users that the user not read.
        count_comments = '''select count(1) from performance_kpicomment
                           where performance_kpicomment.kpi_id = performance_kpi.id
                                and performance_kpicomment.user_id <> %s
                                and performance_kpicomment.created_at >= (%s)''' % (request.user.id, last_read_date,)
        kpis = KPI.objects.filter(id__in=kpi_ids).extra(select={'count_comments': count_comments}).values('id',
                                                                                                          'count_comments')
        return HttpResponse(json.dumps(list(kpis)), content_type="application/json")

    # def comment_report(self, organization, quarter_period, user_id=None):
    #     if not user_id:
    #         uo = Profile.objects.filter(
    #             Q(is_consultant=True) | Q(user__email__endswith="@cjs.vn"),
    #             organization=organization).first()
    #     else:
    #         uo = Profile.objects.filter(user_id=user_id,
    #                                     organization=organization).first()
    #
    #     if uo:
    #         # comments = KpiComment.objects.filter(user=uo.user,
    #         #                                      kpi__quarter_period=quarter_period) \
    #         #     .values_list('content', 'kpi__name', 'kpi__user__email',
    #         #                  'kpi__user__profile__report_to__email',
    #         #                  'kpi__user_id').order_by('kpi__user_id')
    #         comments = KpiComment.objects.filter(user=uo.user,
    #                                              kpi__quarter_period=quarter_period) \
    #             .values_list('content', 'kpi__name', 'kpi__user__email',
    #                          'kpi__user__profile__parent__user__email',
    #                          'kpi__user_id').order_by('kpi__user_id')
    #
    #         response = []
    #         url = settings.DOMAIN  # 'http://%s' % (Site.objects.get_current(),)
    #         for obj in comments:
    #             item = list(obj)
    #             link = url + reverse('all_kpi') + "?team_id=" + str(item[-1])
    #             item[-1] = link
    #             response.append(item)
    #
    #         headers = ['Comment', 'KPI', 'Employee Email', 'Manager Email', 'Link']
    #         return ExcelResponse(response, 'Comments report', headers=headers)
    #     else:
    #         return HttpResponse("Consultant is not exist.")

    """
    Ankita commented following code for export_missed_data after confirming with Mr. Hong
    """
    # def export_missed_data_kpi(self, organization, quarter_period):
    #
    #     # why the fuck we duplicate this code here?
    #     # uos = UserOrganization.objects.filter(organization=organization,
    #     #                                       active=True).exclude(user_id=organization.ceo_id).order_by(
    #     #     'user__profile__report_to_id')
    #     uos = Profile.objects.filter(organization=organization,
    #                                  active=True) \
    #         .exclude(user_id=organization.ceo_id).order_by('parent_id')
    #     #
    #     # extra_clause = '''select count(1) from performance_kpi as a
    #     #                   where a.user_id=company_userorganization.user_id and
    #     #                   (a.parent_id is null or (a.parent_id is not null and a.assigned_to_id is null))
    #     #                   and a.quarter_period_id = %d''' % (quarter_period.id,)
    #     # extra_select = {
    #     #     'unit_blank': extra_clause + " and (a.unit is null or a.unit='')",
    #     #     'current_goal_blank': extra_clause + " and (a.current_goal is null or a.current_goal='')",
    #     #     'operator_blank': extra_clause + " and (a.operator is null or a.operator='')",
    #     #     'target_blank': extra_clause + " and a.target is null",
    #     # }
    #     # data = uos.extra(select=extra_select).values_list('user__email', 'user__profile__report_to__email',
    #     #                                                   'unit_blank', 'current_goal_blank', 'operator_blank',
    #     #                                                   'target_blank', 'user_id')
    #
    #     response = []
    #     url = settings.DOMAIN  # 'http://%s' % (Site.objects.get_current(),)
    #     for uo in uos:
    #         # if obj[2] > 0 or obj[3] > 0 or obj[4] > 0 or obj[5] > 0:
    #         item = []  # list(obj)
    #         item.append(uo.user.email)
    #         # item.append(uo.user.profile.report_to.email)
    #         item.append(uo.parent.user.email if uo.parent else '')
    #         item.append("")
    #         link = url + reverse('kpi_editor_emp', kwargs={
    #             # 'year': quarter_period.year,
    #             'user_id': uo.user.id}) + "?category=all"
    #         item.append(link)
    #         response.append(item)
    #
    #     headers = ['Employee Email', 'Manager Email', 'Level of Completion (A: Perfect, B: < 5 Erros, C: >5 erros )',
    #                'Link']
    #     return ExcelResponse(response, 'KPI report', headers=headers)


@login_required
def public_kpi_editor(request, user_id):  # pragma: no cover
    user = User.objects.get(id=user_id)
    profile = user.get_profile()
    organization = PerformanceApi.get_organization(profile.user)

    if request.method == 'POST':
        allow_public = request.POST.get('public_link', '')
        if allow_public:

            profile.allow_public_view = True
            profile.save()
        else:
            profile.allow_public_view = False
            profile.save()

    variables = {
        'profile': profile,
        'year': organization.get_current_quarter().year,

    }
    return render(request, 'modals/change_public_view.html', variables)


class KPIComposer(KPIWorkSheetEmployeeNew):

    def get(self, request, *args, **kwargs):
        response = render(request, 'performance/composer/kpi-composer.html', {})

        return response


class KPIDaskboard(KPIWorkSheetEmployeeNew):

    def get(self, request, *args, **kwargs):
        # print 'I\'m in KPIDaskboard.get()'
        # request.META['HTTP_HOST']
        # localhost:8000, 127.0.0.1:8000

        # Hong disable Jan 29
        # if Profile.objects.get(user=request.user).should_onboarding() == True and request.GET.get('noonboard',
        # if request.user.profile.should_onboarding() == True and request.GET.get('noonboard', None) != 'true':
        #     return HttpResponseRedirect('/performance/onboarding/')

        # try:
        organization = self.get_organization(request.user)
        # except Exception as e:

            # return APIJsonResponse500( unicode(e))
            #

            #  if is_consultant(request.user): NO consultant any more
            #     user_viewed = organization.ceo
            # else:
        user_viewed = request.user

        if organization.load_manager_first == False:
         #   year = organization.get_current_quarter().year
            # return HttpResponseRedirect(reverse("kpi_dashboard_emp", kwargs={'year': year, 'user_id': request.user.id}))
            return HttpResponseRedirect(reverse("kpi_editor_emp", kwargs={'user_id': request.user.id}))

        return self.process_get(user_viewed, kpi_dashboard=True, editing_vision=True, request_public=False)

    def post(self, request, *args, **kwargs):
        try:
            organization = self.get_organization(request.user)
            current_quarter = organization.get_current_quarter()
            # year = current_quarter.year
            # if is_consultant(request.user):
            #     user_id = organization.ceo_id
            # else:
            user_id = request.user.id
            uo = Profile.objects.get(user_id=user_id, organization=organization)
        except:
            raise Http404

        return self.process_post(request, organization, current_quarter, uo)
