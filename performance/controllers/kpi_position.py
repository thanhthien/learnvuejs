from performance.base.views import PerformanceBaseView
from django.shortcuts import render, get_object_or_404
from company.models import Position
from user_profile.services.profile import get_organization_from_user


class KPIPostition(PerformanceBaseView):

    def get(self, request, *args, **kwargs):
        position_id = kwargs['position_id']
        position = get_object_or_404(Position, pk=position_id, organization=get_organization_from_user(request.user))
        res = {
            'position': position.id
        }
        return render(request, 'performance/bsc/kpi_position/kpi_position.html', res)
