# !/usr/bin/python
# -*- coding: utf-8 -*-
from performance.base.views import PerformanceBaseView
from django.shortcuts import render
from performance.db_models.kpi_library import KPILib
import json
from django.core.serializers.json import DjangoJSONEncoder
from performance.models import KPI, BSC_CATEGORY, LANGUAGES
from collections import OrderedDict

from performance.services.kpilib import get_function_tags


class KPILibManagement(PerformanceBaseView):

    def get(self, request, *args, **kwargs):
        EXTRA_FIELDS = OrderedDict(KPILib.EXTRA_FIELDS)
        CATEGORY = OrderedDict(BSC_CATEGORY)
        LANGUAGES_FIELD = OrderedDict(LANGUAGES)
        FUNCTIONS = get_function_tags()
        for (key, value) in LANGUAGES_FIELD.iteritems():
            LANGUAGES_FIELD[key] = unicode(value)
        for (key, value) in CATEGORY.iteritems():
            CATEGORY[key] = unicode(value)
        for (key, value) in EXTRA_FIELDS.iteritems():
            EXTRA_FIELDS[key] = unicode(value)

        return render(request, 'performance/kpilib/kpilib.html', {
            'EXTRA_FIELDS': json.dumps(EXTRA_FIELDS),
            'CATEGORY': json.dumps(CATEGORY),
            'LANGUAGES_FIELD': json.dumps(LANGUAGES_FIELD),
            'FUNCTIONS': json.dumps(FUNCTIONS)
        })
