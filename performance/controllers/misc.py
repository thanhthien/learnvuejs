#!/usr/bin/python
# -*- coding: utf-8 -*-
import datetime

from company.models import  Organization
from django.contrib.auth import login as login_as
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.mail import mail_admins
from django.db.models import Count
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.shortcuts import render_to_response, render, get_object_or_404
from django.template import RequestContext
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from elms import settings
from performance.base.views import PerformanceBaseView
from performance.decorators import validate_license_apiview
from performance.documents import ExcelFile
from performance.models import   KPI,    KpiComment
from user_profile.models import Profile
from user_profile.services.profile import send_mail_with_perm
from user_profile.templatetags.user_tags import display_name
from utils import ExcelResponse


#
# @login_required
# @csrf_exempt
# def kpi_approval_get(request):
#     uid = request.GET.get('uid', 0)
#
#     result = {
#         'manager_status': 'PE',
#         'employee_status': 'PE',
#     }
#     try:
#         uid = int(uid)
#     except:
#         uid = 0
#
#     if uid == 0:
#         employee = request.user
#     else:
#         employee = User.objects.get(id=uid)
#     quarter = employee.get_profile().get_organization().get_current_quarter()
#     kpi_approval, created = KPIApproval.objects.get_or_create(user=employee, quarter=quarter)
#
#     result = {
#         'manager_status': kpi_approval.manager_status,
#         'employee_status': kpi_approval.employee_status,
#     }
#
#     return HttpResponse(json.dumps(result), content_type="application/json")
#

#
# @login_required
# def kpi_approval_reminder_redirect(request):
#     # TODO: check superuser
#     return HttpResponseRedirect('/performance/kpi_approval/reminder/all/')

#
# @login_required
# @csrf_exempt
# def kpi_approval_reminder(request, status):
#     # TODO: check superuser
#     user = request.user
#     organization = user.get_profile().get_organization()
#     if status == 'ok':
#         _employees = organization.get_employees_kpi_approval_ok()
#     else:
#         _employees = organization.get_employees()
#
#     q = request.GET.get('q', '')
#     if 'q' in request.GET:
#         profiles = Profile.objects.filter(user__in=_employees,
#                                           display_name__contains=request.GET['q']).values_list('user')
#         if profiles:
#             list_user = zip(*profiles)[0]
#         else:
#             list_user = []
#         _employees = _employees.filter(Q(email__contains=request.GET['q']) | Q(id__in=list_user))
#
#     if request.method == "POST":
#         data = []
#         company_info = {
#             'name': organization.name,
#             'logo': organization.logo.url if organization.logo else '',
#             'date': format(datetime.date.today(), 'd-m-Y')
#         }
#         header = ['STT', 'Employee', 'Email', 'Last login', 'Manager status', 'Employee status']
#         data.append(header)
#         count = 1
#         for e in _employees:
#             profile = e.get_profile()
#             last_login = profile.get_last_visit()
#             if last_login:
#                 last_login = naturaltime(last_login.start_time)
#             else:
#                 last_login = "Not login"
#
#             row = [
#                 "=row()-6",
#                 profile.display_name,
#                 e.email,
#                 last_login,
#                 profile.current_kpi_approval().get_manager_status_display(),
#                 profile.current_kpi_approval().get_employee_status_display()
#             ]
#             data.append(row)
#             count += 1
#
#         return ExcelKPIApprovalResponse(data, company_info, "KPI Approval")
#
#     paginator = Paginator(_employees, 30)
#     page = request.GET.get('page')
#     try:
#         employees = paginator.page(page)
#     except PageNotAnInteger:
#         # If page is not an integer, deliver first page.
#         employees = paginator.page(1)
#     except EmptyPage:
#         # If page is out of range (e.g. 9999), deliver last page of results.
#         employees = paginator.page(paginator.num_pages)
#
#     noti_messages = KPIApprovalNotificationMessage.objects.all()
#
#     variables = RequestContext(request, {
#         # 'organization': organization,
#         'employees': employees,
#         'noti_messages': noti_messages,
#         'query': q
#
#     })
#     template = 'performance/kpi_approval_reminder.html'
#
#     return render_to_response(template, variables)
#
#
# def kpi_mail_action(request, kpi_id, unique_key, value, reviewer_id):
#     reviewer_id = int(reviewer_id)
#     try:
#         kpi = KPI.objects.get(id=kpi_id)
#     except:
#         return HttpResponse(u"<h1>KPI không tồn tại</h1>")
#
#     try:
#         reviewer = User.objects.get(id=reviewer_id)
#     except:
#         pass
#
#     if kpi.unique_key != unique_key or reviewer is None:
#         em = mail_admins("wrong key/reviewer alert",
#                           str(kpi.id) + " - key " + unique_key + " - reviewer_id " + str(reviewer_id))
#
#
#         return HttpResponse('Fail - Wrong Key')
#     value = int(value)
#
#     if value < 0 or value > 130:
#         em = mail_admins("wrong value alert", str(kpi.id) + " - key " + unique_key + ' - value: ' + str(value))
#
#         return HttpResponse('Fail - Wrong Key')
#
#     # TODO check whether this reviewer can set score
#     kpi.set_score(value, reviewer_id)
#
#     return HttpResponseRedirect("/performance/home/?utm_source=e-action&utm_medium=em&utm_campaign=engagement")

#
# class ImportReviewReport(PerformanceBaseView):
#
#     def get(self, request, *args, **kwargs):
#         organization = self.get_organization(request.user)
#         if 'quarter_id' in kwargs:
#             quarter_period = get_object_or_404(QuarterPeriod, id=kwargs['quarter_id'],
#                                                organization=organization)
#         else:
#             quarter_period = organization.get_current_quarter()
#
#         quarters = organization.get_all_quarter_period()
#
#         log = KPIImportLog.objects.filter(organization=organization,
#                                           quarter_period=quarter_period).order_by('-import_date').first()
#         if log:
#             import_date = log.import_date
#         else:
#             import_date = datetime.date.today()
#
#         logs = KPIImportLog.objects.filter(organization=organization,
#                                            quarter_period=quarter_period).order_by('id')
#
#         imported_file = ExcelFile.objects.filter(organization=organization.id,
#                                                  quarter_period=quarter_period.id).first()
#         if 'download' in request.GET and imported_file:
#             output_name = imported_file.file_name
#             response = HttpResponse(content_type='application/vnd.ms-excel',
#                                     content=imported_file.file.read())
#             response['Content-Disposition'] = 'attachment;filename="%s"' % (output_name,)
#             return response
#
#         return render(request, 'performance/customers/vinhphat/import_log.html', {
#             'import_date': import_date,
#             'quarter_period': quarter_period,
#             'quarters': quarters,
#             'imported_file': imported_file,
#             'logs': logs
#         })
#
#
#
# class InvitationRemove(View):
#     @method_decorator(login_required)
#     @method_decorator(csrf_exempt)
#     def dispatch(self, *args, **kwargs):
#         return super(InvitationRemove, self).dispatch(*args, **kwargs)
#
#     def post(self, request, *args, **kwargs):
#         review_id = request.POST['review_id']
#         user_id = request.POST['user_id']
#         try:
#             review = Review.objects.get(pk=review_id, reviewee_id=user_id)
#             if review.review_type in ['peer', 'senior_manager', 'direct_report']:
#                 reviewer_name = review.reviewer.profile.display_name
#                 reviewee_name = review.reviewee.profile.display_name
#                 remover = request.user.profile.display_name
#                 message = render_to_string('mail/invitation_review_removed.html',
#                                            {'reviewer': reviewer_name,
#                                             'reviewee': reviewee_name,
#                                             'remover': remover})
#                 if not settings.LOCAL:
#                     msg = EmailMessage(u'360 review invitation was removed.', message,
#                                        'Cloudjet <hello@cloudjetsolutions.com>', [review.reviewer.email, ])
#                     msg.content_subtype = "html"
#                     msg.send()
#                 review.delete()
#                 return HttpResponse('ok')
#         except:
#             pass
#         return HttpResponse('error')


# https://github.com/github/email_reply_parser/blob/master/lib/email_reply_parser.rb
def remove_email_signature(content):
    content = content.split('-- \n')[0]
    content = content.split('--\n')[0]
    content = content.split('-----Original Message-----')[0]
    content = content.split('________________________________')[0]
    content = content.split('From: ')[0]
    content = content.split('Sent from ')[0]
    content = content.split('<div class="gmail_extra">')[0]
    content = content.split('<div class="yahoo_quoted">')[0]
    content = content.split('Best regards,')[0]
    content = content.split('best regards,')[0]
    content = content.split('regards,')[0]
    content = content.split('Regards,')[0]
    content = content.split(u'Trân trọng,')[0]

    return content


# http://stackoverflow.com/questions/1650941/django-csrf-framework-cannot-be-disabled-and-is-breaking-my-site
@csrf_exempt
def parse_email(request):
    subject = request.POST.get('subject', 'no subject')
    from_email = request.POST.get('from', '')
    to_email = request.POST.get('to', '')
    text = request.POST.get('text', '')
    if text == '':
        text = request.POST.get('html', '')

    text = remove_email_signature(text)
    kpi_id = None
    kpi = None
    user = None

    try:
        kpi_id = from_email.split('@')[0]
        kpi_id = int(kpi_id)

        kpi = KPI.objects.get(id=kpi_id)
        user = User.objects.get(email=from_email)
    except:
        pass

    if kpi_id is None or kpi is None or user is None:
        # reporting
        em = mail_admins(subject + " error from: " + from_email, text)

        return HttpResponse('bug')

    km = KpiComment(user=user, content=text, kpi_unique_key=kpi.get_unique_key(), kpi=kpi, send_from_ui='email')
    km.save()

    # testing / notifying
    em = mail_admins(subject, text)

    return HttpResponse("ok")
#
# @login_required
# @csrf_exempt
# def kpi_approval_send_reminder(request):
#     uid = request.POST.get('employee_id', 0)
#
#     try:
#         uid = int(uid)
#     except:
#         uid = 0
#
#     if uid != 0:
#         employee = User.objects.get(id=uid)
#
#         kpi_approval = employee.get_profile().current_kpi_approval()
#         content = request.POST.get('content', '')
#         phase = request.POST.get('phase', 'P1')
#
#     if content != "":
#         kan = KPIApprovelNotification(kpi_approval=kpi_approval, send_by=request.user, content=content, phase=phase)
#         kan.save()
#
#         body = content
#
#         send_to = [employee.email, ]
#         if employee.get_profile().report_to:
#             send_to.append(employee.get_profile().report_to.email)
#
#         email = EmailMessage(u'[Quan Trọng] Nhắc nhở về việc điều chỉnh KPIs',
#                              body, settings.FROM_EMAIL,
#                              send_to, [settings.customer_happiness_email], )
#         email.send(fail_silently=True)
#
#         # yammer = yampy.Yammer(access_token='rySg5dVtKG37WfKNjyzGGQ')
#         # yammer.messages.create(body + " send to:  " + employee.email + " - " + employee.get_profile().report_to.email,
#         #                        group_id=settings.yammer_kpi_approval_group)  #
#
#     return HttpResponse("Ok")
#


def test_email_template(request):
    # user = request.user
    companies = Organization.objects.all()

    return render(request, 'mail/reports/clients_usage.html', {
        'companies': companies,
        # 'one_week': one_week,
        # 'u':user,
    })


def test_email_template_kpi(request):
    # user = request.user
    kpi = KPI.objects.filter(parent_id__gt=0).order_by("?").first()

    if kpi:
        # report_to_id = kpi.user.get_profile().report_to_id
        parent = kpi.user.profile.parent
        parent_user_id = parent.user_id if parent else None

        kpi.get_kpi_mail_action_url0 = kpi.get_kpi_mail_action_url(0, parent_user_id)
        kpi.get_kpi_mail_action_url25 = kpi.get_kpi_mail_action_url(25, parent_user_id)
        kpi.get_kpi_mail_action_url50 = kpi.get_kpi_mail_action_url(50, parent_user_id)
        kpi.get_kpi_mail_action_url80 = kpi.get_kpi_mail_action_url(80, parent_user_id)
        kpi.get_kpi_mail_action_url100 = kpi.get_kpi_mail_action_url(100, parent_user_id)

    return render(request, 'mail/performance/kpi_email_review.html', {
        'kpi': kpi,
        'STATIC_URL':settings.STATIC_URL,
        # 'companies': companies,
        # 'one_week': one_week,
        # 'u':user,
    })


def test_email_template_kpicomment(request):
    # notes = KpiComment.objects.all().order_by("?")[:8]
    return render(request, 'mail/performance/notification__complete_review.html', {
     #   'notes': notes,
        'profile': request.user.profile,
        'kpis': request.user.profile.get_kpis_parent(),
        'quarter_period': request.user.profile.get_organization().get_current_quarter(),

    })


class LoginCEO(PerformanceBaseView):
    def post(self, request, *args, **kwargs):
        uo = self.get_user_organization(request.user)
        organization = uo.organization
        if uo.is_admin and request.is_ajax():
            ceo = organization.ceo
            ceo.backend = 'django.contrib.auth.backends.ModelBackend'
            login_as(request, ceo)
            request.session.set_expiry(60 * 60 * 2)
            return HttpResponse('ok')

        return HttpResponse('failed')


class LoginEmployee(PerformanceBaseView):
    def post(self, request, *args, **kwargs):
        user_id = request.POST.get('user_id')
        if user_id:
            user = User.objects.filter(id=user_id).first()
            uo = self.get_user_organization(user)
            organization = uo.organization

            request_user = self.get_user_organization(request.user)
            if (request_user.is_admin or request_user.is_superuser) and request.is_ajax() and organization.id in [1,
                                                                                                                  47]:
                ceo = uo.user
                ceo.backend = 'django.contrib.auth.backends.ModelBackend'
                login_as(request, ceo)
                request.session.set_expiry(60 * 60 * 2)
                return HttpResponse('ok')

        return HttpResponse('failed')

#
# class KPIQualityControl(View):
#
#     def get(self, request, *args, **kwargs):
#         try:
#             organization = Organization.objects.get(hash=kwargs['hash'])
#         except:
#             raise Http404
#
#         # user_organizations = UserOrganization.objects.select_related('user').filter(organization=organization).order_by(
#         #     "user__profile__report_to_id")
#         user_organizations = Profile.objects.select_related('user') \
#             .filter(organization=organization).order_by("parent_id")
#         quarter = organization.get_current_quarter()
#
#         variables = {
#             'org': organization,
#             'user_organizations': user_organizations,
#             'quarter': quarter
#         }
#         return render(request, 'performance/kpi_qc.html', variables)
#
#     def post(self, request, *args, **kwargs):
#         try:
#             organization = Organization.objects.get(hash=kwargs['hash'])
#         except:
#             raise Http404
#
#         # user_organizations = UserOrganization.objects.select_related('user').filter(organization=organization).order_by(
#         #     "user__profile__report_to_id")
#         user_organizations = Profile.objects.select_related('user').filter(organization=organization).order_by("parent_id")
#         quarter = organization.get_current_quarter()
#
#         data = [['STT', 'Employee', 'Manager', 'KPI parent', 'KPI child', 'First Check', 'Second Check', "CJS Note",
#                  "Client Note"], ]
#         count = 1
#         for uo in user_organizations:
#             item = []
#             name = u"%s \n%s" % (display_name(uo.user), uo.user.email)
#             item.append("= ROW()-2 ")
#             # manager = uo.user.get_profile().report_to
#             manager = uo.parent.user if uo.parent else None
#             item.append(name)
#             item.append(display_name(manager))
#             kpi_parent = KPI.objects.filter(user=uo.user,
#                                             quarter_period=quarter,
#                                             parent=None).aggregate(kpi_parent=Count('id'))
#             kpi_parent = kpi_parent['kpi_parent'] or 0
#             kpi_child = KPI.objects.filter(user=uo.user,
#                                            quarter_period=quarter,
#                                            parent__isnull=False).aggregate(kpi_child=Count('id'))
#             kpi_child = kpi_child['kpi_child'] or 0
#             item.append(kpi_parent)
#             item.append(kpi_child)
#             item.append('Yes' if uo.cjs_check_kpi_firsttime else "No")
#             item.append('Yes' if uo.cjs_check_kpi_secondtime else "No")
#             item.append(uo.kpi_qc_note)
#             item.append(uo.client_note)
#             data.append(item)
#             count += 1
#
#         return ExcelResponse(data, "KPI QC")
#
