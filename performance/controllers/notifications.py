# !/usr/bin/python
# -*- coding: utf-8 -*-
from performance.base.views import PerformanceBaseView
from django.shortcuts import render
from performance.db_models.kpi_library import KPILib
import json
from django.core.serializers.json import DjangoJSONEncoder


class Notifications(PerformanceBaseView):

    def get(self, request, *args, **kwargs):
        ACTIVE_MENU = 'active'
        return render(request, 'performance/notifications/index.html', {'ACTIVE_MENU': ACTIVE_MENU})
