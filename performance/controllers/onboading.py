#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from performance.base.views import PerformanceBaseView
from user_profile.models import Profile


class ONBOARDING(PerformanceBaseView):
    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(reverse('onboarding-step-1'))


class ONBOARDING1(PerformanceBaseView):
    def get(self, request, *args, **kwargs):
        variables = {}

        return render(request, 'performance/onboarding/onboarding-step-1.html', variables)


class ONBOARDING2(PerformanceBaseView):
    def get(self, request, *args, **kwargs):
        variables = {}

        return render(request, 'performance/onboarding/onboarding-step-2.html', variables)


class ONBOARDING3(PerformanceBaseView):
    def get(self, request, *args, **kwargs):
        variables = {}

        return render(request, 'performance/onboarding/onboarding-step-3.html', variables)


class ONBOARDING32(PerformanceBaseView):
    def get(self, request, *args, **kwargs):
        variables = {}

        return render(request, 'performance/onboarding/onboarding-step-3-2.html', variables)


class ONBOARDING4(PerformanceBaseView):
    def get(self, request, *args, **kwargs):
        variables = {}

        return render(request, 'performance/onboarding/onboarding-step-4.html', variables)


class ONBOARDING5(PerformanceBaseView):
    def get(self, request, *args, **kwargs):
        variables = {}

        return render(request, 'performance/onboarding/onboarding-step-5.html', variables)


class ONBOARDING6(PerformanceBaseView):
    def get(self, request, *args, **kwargs):
        variables = {}

        return render(request, 'performance/onboarding/onboarding-step-6.html', variables)


class ONBOARDING7(PerformanceBaseView):
    def get(self, request, *args, **kwargs):
        variables = {}

        profile = Profile.objects.get(user=request.user)
        profile.is_onboarded = True
        profile.save()

        return render(request, 'performance/onboarding/onboarding-step-7.html', variables)

