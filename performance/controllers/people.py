# -*- coding: utf-8 -*-

import json

from rest_framework import serializers

from authorization.models import get_rules_by_actor
from authorization.rules import SETTINGS__VIEW_ORG_CHART, DELETE_USER, MANAGER_REVIEW, \
    USER__BELONG_TO_ORG
from authorization.rules import has_perm
from company.models import UnitCode, Position
from dateutil.parser import parse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.cache import cache
from django.core.exceptions import PermissionDenied
from django.http.response import HttpResponse, Http404
from django.middleware.csrf import get_token
from django.shortcuts import render_to_response, render, redirect, get_object_or_404
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _, ugettext
from django.views.decorators.csrf import csrf_exempt
from elms import settings
from performance.base.views import PerformanceBaseView, EmailResetPassword
from performance.db_models.kpi_library import JobTitle
from performance.db_models.log import AlignLog
from performance.forms import MemberForm
from performance.models import KPI, KPIBackup
from performance.services import get_user_by_id, get_user_by_email
from performance.services.user_change_log import log_change_user_email
from performance.services.user_organization import toggle_active, \
    migrate_employee_to_exist_position, get_archived_employee
from user_profile.models import Profile, get_full_scores_v2
from user_profile.services.profile import create_new_user, send_new_password_to_email, get_employee_code, get_profile, get_current_user_viewed
from user_profile.services.user import set_new_password
# from user_profile.signals import set_profile_cache
from utils import short_name, unsigned_vi, brief_name
from utils.api.response import APIJsonResponse403, APIJsonResponseBadRequest, \
    APIJsonResponse404
from utils.common import HttpResponseJson, HttpResponseJsonForbidden, HttpResponseJson500, \
    to_json
from utils.random_str import random_string
from utils.caching_name import CREATING_ASSESSMENT, CREATING_ASSESSMENT_ORG, CREATING_ASSESSMENT_QUARTER, \
    CREATING_ASSESSMENT_ORG_CLONEKPI_PROGRESS, CREATING_ASSESSMENT_ORG_TOTAL_USER, CREATING_ASSESSMENT_ORG_QUARTER, \
    CREATING_ASSESSMENT_ORG_CLONEMAP_PROGRESS, CREATING_ASSESSMENT_ORG_ALIGNKPI_PROGRESS
from performance.services.kpi_backup import create_backup_kpis
from django.db import transaction
from django.core.serializers.json import DjangoJSONEncoder
from company.services.position_chart import generate_position_chart
from performance.tasks import start_generate_position_chart, migrate_employee_to_exist_position_use_celery_or_thread
from company.services.organization import get_position


@login_required
@csrf_exempt
# #@consultant_permission
def creating_assessment(request):
    view_progress = request.GET.get('progress', None)

    organization = request.user.get_profile().get_organization()

    clonekpi_progress = cache.get(CREATING_ASSESSMENT_ORG_CLONEKPI_PROGRESS.format(organization.id,))
    alignkpi_progress = cache.get(CREATING_ASSESSMENT_ORG_ALIGNKPI_PROGRESS.format(organization.id,))
    clonemap_progress = cache.get(CREATING_ASSESSMENT_ORG_CLONEMAP_PROGRESS.format(organization.id,))

    variables = RequestContext(request, {

        'organization': organization,
        'view_progress': view_progress,
        # 'progress': progress,
        # 'count_alignlog': count_alignlog,
        # 'total_user': total_user,
        # 'quarter_id': quarter_id,
        'clonekpi_progress':clonekpi_progress,
        'alignkpi_progress':alignkpi_progress,
        'clonemap_progress':clonemap_progress,

    })

    return render_to_response('performance/notifications/creating_new_quarter.html', variables)


@login_required
@csrf_exempt
# #@consultant_permission
def people_view(request):
    # x.d =10
    # print "xxxxx{}".format(request.GET)

    organization = request.user.get_profile().get_organization()
    user_organization = request.user.get_profile()

    if not organization or not user_organization:
        return render(request, '403.html', {"message": "Don't have permission to access this page."})

    user_id = request.GET.get('user_id', None)
    if user_id is None:
        ceo_id = organization.ceo_id
    else:
        ceo_id = int(request.GET.get('user_id'))

    try:
        profile = Profile.objects.get(user_id=user_id)
        if profile.is_deleted:
            from django.core.urlresolvers import reverse
            return redirect(reverse('kpi_dashboard'))
    except Profile.DoesNotExist:
        pass

    if request.method == "POST":
        if not request.POST.get('user_id', None):
            ceo_id = organization.ceo_id
        else:
            ceo_id = int(request.POST.get('user_id'))

    ceo = User.objects.filter(id=ceo_id).first()  # dat ten bien tao lao qua
    can_delete = has_perm(DELETE_USER, actor=request.user, target=ceo)

    target_user_id_rule_by_actor = get_rules_by_actor(request.user)
    if target_user_id_rule_by_actor:
        target_user_id_rule_by_actor = target_user_id_rule_by_actor.first().target.id

    if not has_perm(SETTINGS__VIEW_ORG_CHART, request.user, ceo):
        # return render(request,'404.html',{"message":"Don't have permission to access this page."})
        return render(request, '403.html')

        # return HttpResponseJsonForbidden()
        # ceo = User.objects.filter(id=request.GET.get('user_id', None)).first()

    ceo_profile_json = ceo.profile.get_json(break_level=1)

    if request.method == "POST":
        if 'is_checked' in request.POST:
            user_id = request.POST.get('user_id', None)
            username = request.POST.get('username', None)
            is_checked = True if request.POST.get('is_checked') == 'true' else False
            org = Profile.objects.filter(user_id=user_id).first()

            if org and request.user.profile.is_superuser_org():
                org.is_admin = is_checked
                org.save()
            return HttpResponse('ok')
        elif request.POST.get('active') == 'active':
            user_id = request.POST.get('user_id', None)
            user = get_user_by_id(user_id)
            is_checked = True
            if not has_perm(MANAGER_REVIEW, actor=request.user, target=user):
                return APIJsonResponse403("You do not have permission to active this user")
            toggle_active(is_checked, user_id, request.user, ignore_perm_check=True)

            profile = get_user_by_id(user_id).profile
            profile.reason = ''
            profile.save(update_fields=['reason'])

            data = {
                'name': render_to_string('performance/chart/node.html',
                                         {'username': profile.user.username,
                                          'short_name': short_name(profile.display_name),
                                          'brief_name': brief_name(profile.display_name),
                                          'STATIC_URL': settings.STATIC_URL,
                                          'active': is_checked,
                                          'avatar': profile.get_avatar(),
                                          'color': None}),
                'active': is_checked
            }
            return HttpResponse(json.dumps(data), content_type="application/json")

        elif request.FILES:
            print request.user
            user_id = request.POST.get('user_id', None)
            user = get_user_by_id(user_id)
            profile = get_profile(user_id, request.user)
            if not profile:
                return APIJsonResponse403("You not permission change avatar")
            if request.FILES.get('file') and user is not None:
                try:
                    avatar = request.FILES.get('file')
                    profile.avatar.save(avatar.name, avatar)
                    profile.save()
                    cache.set('avatar_user' + str(user.id), profile.get_avatar(), 60 * 20)
                    cache.set('avatar_user' + user.email, profile.get_avatar(), 60 * 60 * 2)
                except Exception as e:  # pragma: no cover
                    error = str(e)
                    print error

                    # duan temporary disable EVN
                    # print 'disable EVN '
                    # FUCK FUCK Duan
                    #     if organization.id == 1333:
                    #         return None

        profile_id = request.POST.get('id', None)
        if profile_id:
            try:
                p = Profile.objects.get(pk=profile_id[1:])
                organization = p.get_organization()
                current_quarter = organization.get_current_quarter()
                user = p.user
                email = request.POST.get('email').lower().strip()
                if user.email != email:
                    if get_user_by_email(email):  # User.objects.filter(email=email).exists():
                        return HttpResponse(ugettext('Email is existed') + ' ' + email)
                    old_email = user.email
                    user.email = email
                    KPI.objects.filter(user_id=user.id, owner_email=old_email).update(owner_email=email)

                    log_change_user_email(request.user, user, old_email, user.email,
                                          user.profile.get_current_quarter())
                if p.display_name != request.POST.get('name') and request.POST.get('name'):
                    username = unsigned_vi(request.POST.get('name')).lower().replace(' ', '')
                    if User.objects.filter(username=username).exists():
                        count = 1
                        while (True):
                            temp = username + str(count)
                            if not User.objects.filter(username=temp).exists():
                                username = temp
                                break
                            count += 1
                    user.username = username
                user.save()

                p.display_name = request.POST.get('name')
                p.title = request.POST.get('position')
                p.phone = request.POST.get('phone')
                p.skype = request.POST.get('skype')
                manager = request.POST.get('manager')
                p.unit_code = request.POST.get('unit_code')
                p.department = request.POST.get('department')
                p.active = True if request.POST.get('active') == '1' else False
                changed_manager = False
                # if p.report_to and manager and p.report_to.get_profile().id != int(manager[1:]):
                if p.parent and manager and p.parent.id != int(manager[1:]):
                    #
                    # r = Review.objects.filter(organization=organization,
                    #                           reviewee_id=p.user_id,
                    #                           review_type='manager',
                    #                           quarter_period=current_quarter).first()
                    manager = Profile.objects.get(pk=manager[1:])
                    # p.report_to = manager.user
                    p.parent = manager
                    # if r:
                    #     r.reviewer = manager.user
                    #     r.save()
                    changed_manager = True
                try:
                    role_changed = False
                    job_title_id = request.POST.get('job_title_id', '')
                    if job_title_id and JobTitle.objects.filter(pk=int(request.POST.get('job_title_id', ''))).exists():
                        if p.current_job_title_id != int(job_title_id):
                            p.current_job_title_id = int(job_title_id)
                            role_changed = True

                except ValueError:  # pragma: no cover
                    pass
                p.save()
                # set_profile_cache(Profile, p)

                # if role_changed:
                # p.copy_competency(quarter_period, remove=False)
                # p.copy_kpi(quarter_period, remove=False)

                usr_org = p
                if not usr_org:  # pragma: no cover - can't reach this
                    usr_org = Profile.objects.create(organization=organization,
                                                     user=user)
                usr_org.position = request.POST.get('position')
                #   usr_org.employee_code = request.POST.get('employee_code')
                p.employee_code = request.POST.get('employee_code')
                p.unit_code = request.POST.get('unit_code')
                p.save()
                try:
                    usr_org.start_date = parse(request.POST.get('start_date'), dayfirst=True)
                except:
                    pass
                try:
                    usr_org.role_start = parse(request.POST.get('role_start'), dayfirst=True)
                except:
                    pass
                usr_org.save()

                job_title = ""
                if p.current_job_title_id:
                    job_title = p.current_job_title.name
                return HttpResponse(json.dumps({'short_name': short_name(p.display_name, "<br>"),
                                                'changed_manager': changed_manager,
                                                'active': p.active,
                                                'job_title': job_title}),
                                    content_type="application/json")
            except Exception as e:
                return HttpResponse(str(e))

    if not has_perm(USER__BELONG_TO_ORG, request.user, ceo):
        raise Http404(ugettext("You don't have permission to access this page"))

    from django.db.models import Value, CharField
    from django.db.models.functions import Concat

    # user_id_list = UserOrganization.objects.filter(organization=organization).values_list('user_id', flat=True)
    # we must check None for dispplay_name
    list_name_1 = Profile.objects.filter(organization=organization,
                                         display_name__isnull=False) \
        .exclude(is_deleted=True) \
        .values('display_name', 'user__email', 'user_id') \
        .annotate(nameofuser=Concat('display_name', Value(' ('), 'user__email',
                                    Value(') |'), 'user_id',
                                    output_field=CharField())) \
        .values_list('nameofuser', flat=True)

    list_name_2 = Profile.objects.filter(organization=organization,
                                         display_name__isnull=True).exclude(is_deleted=True) \
        .values('user_id', 'user__email') \
        .annotate(nameofuser=Concat('user__email', Value('|'), 'user_id', output_field=CharField())) \
        .values_list('nameofuser', flat=True)

    list_name = list(list_name_1) + list(list_name_2)
    seachable_user_list = list_name
    # cache.set('searchable_user_list_{}'.format(target_organization.id), list_name, 60 * 10)

    # job_categories = JobCategory.objects.filter(industry_id=organization.industry_id)

    user_viewed = get_current_user_viewed(request, request.GET.get('user_id', None))

    list_uc = UnitCode.objects.filter(organization=organization).values('code', 'name')
    variables = RequestContext(request, {
        'json_data': ceo_profile_json,
        'csrf_token': get_token(request),
        # 'job_categories': job_categories,
        'user_organization': user_organization,
        'can_delete': can_delete,
        'list_name': json.dumps(list(seachable_user_list)),
        'organization': organization,
        'list_uc': list(list_uc),
        'user_request': request.user,
        'target_user_id_rule_by_actor': target_user_id_rule_by_actor,
        'user_viewed': user_viewed
    })

    return render_to_response('performance/people.html', variables)


# didn't find any usage hence excluded from test coverage
class UserResetPassword(EmailResetPassword):  # pragma: no cover

    def post(self, request, *args, **kwargs):
        user_id = request.POST.get('user_id', None)
        email = request.POST.get('email', None)
        username = request.POST.get('username', None)
        if user_id.isdigit() and email and username:
            try:
                u = User.objects.get(id=user_id, email=email)

                u, new_password = set_new_password(u.id)
                # _user.save()

                return HttpResponse('ok')
            except Exception as e:
                return HttpResponseJson500(e.message)
                # return HttpResponse('error')
        return HttpResponse('error')


# didn't find any usage hence excluded from test coverage
class GroupUserResetPassword(EmailResetPassword):

    def post(self, request, *args, **kwargs):
        user_id = request.POST.get('user_id', None)
        email = request.POST.get('email', None)
        username = request.POST.get('username', None)
        if user_id.isdigit() and email and username:
            try:
                u = User.objects.get(username=username, id=user_id, email=email)
                subordinate = u.profile.get_subordinate()
                for p in subordinate:
                    user = p.user
                    # new_password = cache.get(user.email + 'password')
                    # if not new_password:
                    #
                    #     if u.profile.get_organization().default_password:
                    #         new_password = u.profile.get_organization().default_password
                    #     else:
                    #         new_password = random_key(8)
                    #
                    #     cache.set(user.email, new_password, 60 * 60 * 2)

                    # user.set_password(new_password)
                    # user.profile.set_new_password(new_password)
                    user, new_password = set_new_password(user.id, actor=request.user)
                    # below code commented as email is sent in set_new_password
                    # email = user.email
                    # full_name = p.display_name
                    # send_new_password_to_email(email, full_name, new_password)
                if len(subordinate) == 0:
                    return HttpResponse("No members in group")
                return HttpResponse('ok')
            except:
                pass
        return HttpResponse("error")


class AddEmpSerializer(serializers.Serializer):
    # max_length lấy từ 500 message
    # Use allow_blank to allow field to be blank and allow_null to allow field to be null in test
    # But I think it is not good in real world
    # Refer: http://www.django-rest-framework.org/api-guide/fields/#charfield
    name = serializers.CharField(max_length=200, allow_blank=False)
    email = serializers.CharField(max_length=254, allow_blank=False)
    phone = serializers.CharField(max_length=100, allow_null=True, allow_blank=True)
    skype = serializers.CharField(max_length=100, allow_null=True, allow_blank=True)
    employee_code = serializers.CharField(max_length=100, allow_null=True, allow_blank=True)
    position = serializers.CharField(max_length=255, allow_null=True, allow_blank=True)
    department = serializers.CharField(max_length=300, allow_null=True, allow_blank=True)
    unit_code = serializers.CharField(max_length=20, allow_blank=True, allow_null=True)


class AddEmployee(PerformanceBaseView):

    def post(self, request, *args, **kwargs):
        organization = self.get_organization(request.user)
        name = request.POST.get('name', 'No Name')
        email = request.POST.get('email', '').lower()
        start_date = request.POST.get('start_date', '')
        send_pass = True if request.POST.get('send_pass', 'false') == 'true' else False
        if start_date:
            start_date = parse(start_date, dayfirst=True)
        else:
            start_date = None

        role_start = request.POST.get('role_start', '')
        if role_start:
            role_start = parse(role_start, dayfirst=True)
        else:
            role_start = None
        position = request.POST.get('position', '')
        manager = request.POST.get('manager', '')
        employee_code = request.POST.get('employee_code')
        job_title_id = request.POST.get('job_title_id', '')
        unit_code = request.POST.get('unit_code')
        department = request.POST.get('department')
        skype = request.POST.get('skype')
        phone = request.POST.get('phone')
        data_ser = {
            'name': name,
            'email': email,
            'phone': phone,
            'skype': skype,
            'employee_code': employee_code,
            'position': position,
            'department': department,
            'unit_code': unit_code,
        }
        serializer = AddEmpSerializer(data=data_ser)
        try:
            serializer.is_valid(raise_exception=True)
        except Exception as e:
            return APIJsonResponseBadRequest(message=e.message)

        errors = ""

        deleted_user = None

        if User.objects.filter(email__iexact=email).exists():
            # u = User.objects.get(email=email)
            # if not Profile.objects.filter(user__email=email).exists():
            # Case: User of the email already have a profile, but dose not belong to any organizaton
            if Profile.objects.filter(user__email__iexact=email, organization__isnull=True).exists()\
                    or not Profile.objects.filter(user__email__iexact=email).exists():
                # pass
                deleted_user = User.objects.get(email=email)
            else:
                errors = _("Email existed")
                # elif deleted_user.profile.report_to:## THE FUCK?
                #
                #     errors = _("Email existed")
                # if not job_title_id:
                # errors += "Role type bắt buộc nhập."

        if errors:
            return HttpResponse(errors)

        if not deleted_user:
            username = unsigned_vi(name).lower().replace(' ', '')
            # Truncate username to 29 chars, to make it match with max_length of Users model of Django
            if len(username) > 30:
                username = username[0:29]
            if User.objects.filter(username=username).exists():
                count = 1
                while (True):
                    temp = username + str(count)
                    if not User.objects.filter(username=temp).exists():
                        username = temp
                        break
                    count += 1

            p = Profile.objects.get(pk=int(manager[1:]))
            print 'before create new user'
            new_user = create_new_user(manager=p.user, username=username, email=email,
                                       name=name, unit_code=unit_code, position=position,
                                       department=department,
                                       phone=phone,
                                       skype=skype,
                                       start_date=start_date,
                                       role_start=role_start, employee_code=employee_code,
                                       organization=organization,
                                       send_pass=send_pass)
            if not new_user:  # pragma: no cover - can't reach this
                return HttpResponse(_("Can not create user"))  # "Không thể tạo được user")
            # fix caching
            parent = Profile.objects.get(id=p.id)
            return HttpResponse(parent.get_json(), content_type="application/json")

        else:
            p = Profile.objects.get(pk=int(manager[1:]))
            u_profile = deleted_user.get_profile()
            # u_profile.rep ort_to_id = p.user_id
            u_profile.parent = p

            u_profile.display_name = name
            u_profile.phone = request.POST.get('phone')
            u_profile.skype = request.POST.get('skype')
            u_profile.title = position
            u_profile.employee_code = employee_code
            u_profile.unit_code = unit_code
            u_profile.department = department
            try:
                if request.POST.get('job_title_id', None) and JobTitle.objects.filter(
                        pk=int(request.POST.get('job_title_id', ''))).exists():
                    u_profile.current_job_title = JobTitle.objects.get(pk=int(job_title_id))
            except ValueError:  # pragma: no cover
                pass
            u_profile.save()

            sub_user = p.get_all_subordinate(type='user_id', include_self=True)
            cache.set('sub_user_' + str(p.user_id), sub_user, 60 * 20)

            #             usr_org = u_profile.get_user_organization()
            #             if not usr_org:
            #                 UserOrganization.objects.create(user=deleted_user,
            #                                                 organization=organization)
            return HttpResponse(p.get_json(), content_type="application/json")


class AddMember(PerformanceBaseView, EmailResetPassword):

    def post(self, request, *args, **kwargs):
        organization = self.get_organization(request.user)
        _args = {}

        if request.POST == {}:  # pragma: no cover - cause RawPostDataException in this case
            args = json.loads(request.body)
            for arg in args:
                _args.update({arg['name']: arg['value']})
        else:
            _args = request.POST

        print _args
        form = MemberForm(organization, _args)
        # #
        if form.is_valid():
            print 'Your form is valid'
            data = form.cleaned_data
            email = data['email'].lower()

            new_user = User.objects.filter(email=email)

            if new_user.exists():  # pragma: no cover - can't reach this
                user = new_user.first()  # <-- Cho nay nhu the nay la tao lao roi . User da ton tai la ko cho add vao --> return luon

                return HttpResponseJson({'status': 'Error', 'message': 'User existed!'})

            else:
                username = unsigned_vi(data['full_name']).lower().replace(' ', '')
                if User.objects.filter(username=username).exists():
                    count = 1
                    while True:
                        temp = username + str(count)
                        if not User.objects.filter(username=temp).exists():
                            username = temp
                            break
                        count += 1
                manager = None
                if _args.get('report_to'):
                    manager = User.objects.get(id=_args['report_to'])

                if data['send_password']:
                    send_pass = True
                else:  # pragma: no cover - can't reach this
                    send_pass = False
                employee_code = ''
                if data['employee_code']:
                    employee_code = data['employee_code']

                department = ''
                if data['department']:
                    department = data['department']

                new_user = create_new_user(manager=manager, username=username, email=email,
                                           name=data['full_name'], unit_code=request.POST.get('unit_code'),
                                           position=data['position'],
                                           phone=data['phone'], skype=data['skype'],
                                           send_pass=send_pass,
                                           employee_code=employee_code,
                                           department=department,
                                           organization=organization
                                           )

                if new_user:
                    u_profile = new_user.profile

                if request.GET.get('teamview') == 'true':
                    return HttpResponseJson(u_profile)
                response = {
                    'user_id': u_profile.user_id,
                    'profile': u_profile.id,
                    'name': u_profile.display_name,
                    'department': u_profile.department,
                    'email': new_user.email,
                    'phone': u_profile.phone,
                    'skype': u_profile.skype,
                    'start_date': '',
                    'role_start': '',
                    'employee_code': get_employee_code(u_profile),
                    'unit_code': u_profile.unit_code,
                    'avatar': u_profile.get_avatar(),
                    'job_title_name': data['position'],
                    'position': u_profile.title,
                    'brief_name': brief_name(u_profile.display_name),
                    'competencies': [],
                    'kpis': [],
                    'share_to_manager': False,
                    'short_term_career': None,
                    'short_term_development': None,
                    'long_term_career': None,
                    'long_term_development': None,
                    'competency_final_score': None,
                    'kpi_final_score': None,
                    'min_score_kpi': None,
                    'max_score_kpi': None,
                    'min_score_competency': None,
                    'max_score_competency': None,
                    'strength_kpi': None,
                    'strength_competency': None,
                    'weak_kpi': None,
                    'weak_competency': None,
                    'self_percent_finished': 0,
                    'share_to_self': False,
                    'reviews': [],
                    'is_deputy_manager': False,
                    'review_progress_html': '',
                    'previous_competency_final_score': None,
                    'previous_kpi_final_score': None,
                    'history_scores': [],
                    'is_active': True,
                    'status': 'active' if True else "inactive"
                }
                return HttpResponse(json.dumps(response), content_type="application/json")
        else:
            response = HttpResponseJson({'status': 'Error', 'message': ugettext('CHECK FORM AGAIN')})
            response.status_code = 403
            return response
        # return APIJsonResponse403("CHECK FORM AGAIN")

'''
obsolete
'''


@login_required
@csrf_exempt
def delete_people(request):  # pragma: no cover - used API instead
    if request.method == 'POST' and request.is_ajax():
        try:
            profile_id = request.POST.get('id', '')
            user_id = request.POST.get('user_id', None)
            if profile_id:
                p = Profile.objects.get(id=profile_id[1:], user_id=user_id)
                uo = p.get_user_organization()
                quarter_period = uo.organization.get_current_quarter()
                if uo.is_superuser:
                    return HttpResponse("User này đang có quyền quản trị. Bạn không thể xoá user này.")

                has_child = False
                if p.has_subordinate():
                    subordinates = p.get_subordinate()
                    # parent = p.report_to
                    for pr in subordinates:
                        # pr.report_to = p.report_to
                        pr.parent = p.parent
                        pr.save()

                    has_child = True

                uo.delete()

                # Review.objects.filter(reviewee=p.user,
                #                       quarter_period=quarter_period).delete()

                # p.report_to = None
                p.parent = None
                parent = p.parent
                p.save()

                # WE NEED TO CHANGE EMAIL TO A BACKUP-EMAIL OF THE USER HERE
                orig_email = p.user.email
                # base_emailname = orig_email.split('@')[0]
                # email_domain = orig_email.split('@')[1]
                # emailname = base_emailname
                #
                # while User.objects.filter(email='@'.join([emailname, email_domain, ])).exists():
                #     emailname_suffix = random_string(5).lower()
                #     emailname = base_emailname + '.bk' + emailname_suffix
                p.user.email = orig_email + '.bk.' + random_string(10)
                p.user.save()

                if has_child:
                    p = parent.profile
                    return HttpResponse(p.get_json(), content_type="application/json")
                else:
                    return HttpResponse('ok')
        except:
            pass
        # return HttpResponse('error')
        return HttpResponseJson({'status': 'error'})
    else:
        # return HttpResponse("<h1>Bad Request</h1>")
        return HttpResponseJson({'status': 'Bad Request'})


@login_required
@csrf_exempt
def people_json(request):
    organization = request.user.get_profile().get_organization()
    if request.method == "POST":
        user_id = request.POST.get('user_id', None)

        user_list = list(Profile.objects.filter(organization=organization, active=True).values('user_id', 'user__email',
                                                                                               'display_name'))
        for u in user_list:
            u['display_name'] = unicode(u['display_name']) + u' - ' + u['user__email']
        return HttpResponse(json.dumps(user_list, indent=2), content_type='application/json')
    return HttpResponseJson({'status': 'Bad Request'})


class TEAM(PerformanceBaseView):

    def get(self, request, *args, **kwargs):
        user_viewed = get_current_user_viewed(request, request.GET.get('user_id', None))
        organization = request.user.get_profile().get_organization()
        list_uc = UnitCode.objects.filter(organization=organization).values('code', 'name')
        variables = {'list_uc': list(list_uc),
                     'is_admin': request.user.get_profile().is_admin,
                     'user_viewed': user_viewed,
                     'profile_id': user_viewed.profile.id
                     }
        return render(request, 'performance/team/index.html', variables)


class PeopleNode(PerformanceBaseView):
    # TODO: this function has no tests and sometimes generate errors

    def get(self, request, *args, **kwargs):
        lineup = request.GET.get('lineup', None)

        try:
            lineup = int(lineup)
        except:
            lineup = 0

        # org_id = request.GET.get('org_id', 0)
        if lineup:
            startnode_name = lineup
            #           print startnode_name
            org = request.user.profile.get_organization()
            p = Profile.objects.filter(user_id=lineup,
                                       organization_id=org.id).first()
            #            print p.id, p.user.email
            # p=Profile.objects.get(id=startnode_id)
            line = []
            if p:
                line = self.get_lineup(p)
                # line1=self.get_lineup__v1(p)
                # print line, type(line)
                # print line1, type(line1)

            return HttpResponse(json.dumps(line), content_type="application/json")

        node_id = request.GET.get('node_id', None)
        if node_id:
            profile = get_object_or_404(Profile, pk=node_id[1:])
            children = profile.get_subordinate_dict(break_level=1)

            response = {
                'children': children
            }
            return HttpResponse(json.dumps(response), content_type="application/json")

        return HttpResponse("Invalid")

    def get_lineup__v1(self, p):  # pragma: no cover
        _p = p
        arr = [_p.id, ]
        # while _p.report_to:
        while _p.parent:
            _p = _p.parent
            arr.append(_p.id)
        return arr

    def get_lineup(self, p):
        ancestors = p.get_ancestors(ascending=True, include_self=True)
        return list(ancestors.values_list('id', flat=True))
        #
        # _p = p
        # arr = [_p.id, ]
        # while _p.report_to:
        #     _p = _p.report_to.profile
        #     arr.append(_p.id)
        # return arr


class PositionNode(PerformanceBaseView):
    # TODO: this function has no tests and sometimes generate errors

    def get(self, request, *args, **kwargs):
        lineup = request.GET.get('lineup', None)

        try:
            lineup = int(lineup)
        except:
            lineup = 0

        # org_id = request.GET.get('org_id', 0)
        if lineup:
            startnode_name = lineup
            #           print startnode_name
            org = request.user.profile.get_organization()
            p = Position.objects.filter(id=lineup,
                                        organization_id=org.id).first()
            #            print p.id, p.user.email
            # p=Profile.objects.get(id=startnode_id)
            line = []
            if p:
                line = self.get_lineup(p)
                # line1=self.get_lineup__v1(p)
                # print line, type(line)
                # print line1, type(line1)

            return HttpResponse(json.dumps(line), content_type="application/json")

        node_id = request.GET.get('node_id', None)
        if node_id:
            position = get_object_or_404(Position, pk=node_id)
            children = position.get_sub_positions(break_level=1)

            response = {
                'children': children
            }
            return HttpResponse(json.dumps(response), content_type="application/json")

        return HttpResponse("Invalid")

    def get_lineup__v1(self, p):  # pragma: no cover
        _p = p
        arr = [_p.id, ]
        # while _p.report_to:
        while _p.parent:
            _p = _p.parent
            arr.append(_p.id)
        return arr

    def get_lineup(self, p):
        ancestors = p.get_ancestors(ascending=True, include_self=True)
        return list(ancestors.values_list('id', flat=True))
        #
        # _p = p
        # arr = [_p.id, ]
        # while _p.report_to:
        #     _p = _p.report_to.profile
        #     arr.append(_p.id)
        # return arr


class MoveToExistPosition(PerformanceBaseView):

    def post(self, request, *args, **kwargs):
        try:
            data = request.POST or json.loads(request.body)
            user_id = data.get('user_id')
            keep_kpi_history = data.get('keep_kpi_history')
            target_user = data.get('to_user')
        except:
            user_id = None
            keep_kpi_history = None
            target_user = None
            return APIJsonResponseBadRequest(message="Bad Request")

        try:
            profile = get_profile(user_id, actor=request.user)
        except Profile.DoesNotExist:
            profile = None

        try:
            target_profile = get_profile(target_user, actor=request.user)
        except Profile.DoesNotExist:
            target_profile = None

        if not target_profile:
            return APIJsonResponse404(ugettext("Target employee does not exist"))

        new_manager = target_profile.parent.display_name if target_profile.parent else "N/A"
        if profile and target_profile and profile.user_id != target_profile.user_id:
            cache.set('start_thread' + str(profile.user_id), 'moving')
            migrate_employee_to_exist_position_use_celery_or_thread(profile, target_profile, keep_kpi_history, request.user)
            return HttpResponseJson({'new_manager': new_manager})

        return APIJsonResponse403(messsage=ugettext("Employee does not exist"))

    def get(self, request, *args, **kwargs):
        try:
            user_id = int(request.GET.get('user_id', None))
            per = request.GET.get('req', None)
        except:
            user_id = None
            per = None

        archived_employees = get_archived_employee(request.user, user_id, per)

        if archived_employees is None:  # pragma: no cover - can't reach this
            return HttpResponseJsonForbidden(messsage="")

        return HttpResponseJson(archived_employees)

"""
Confirmed with Mr. Phi, following code for people_view_v5() not in use
"""


@login_required
@csrf_exempt
# #@consultant_permission
def people_view_v5(request):  # pragma: no cover # confirmed with Mr. Phi,
    organization = request.user.get_profile().get_organization()
    user_organization = request.user.get_profile()

    if not organization or not user_organization:
        return render(request, '403.html', {"message": "Don't have permission to access this page."})

    user_id = request.GET.get('user_id', None)
    if user_id is None:
        ceo_id = organization.ceo_id
    else:
        ceo_id = int(request.GET.get('user_id'))

    try:
        profile = Profile.objects.get(user_id=user_id)
        if profile.is_deleted:
            from django.core.urlresolvers import reverse
            return redirect(reverse('kpi_dashboard'))
    except Profile.DoesNotExist:
        pass

    if request.method == "POST":
        if not request.POST.get('user_id', None):
            ceo_id = organization.ceo_id
        else:
            ceo_id = int(request.POST.get('user_id'))

    ceo = User.objects.filter(id=ceo_id).first()  # dat ten bien tao lao qua
    can_delete = has_perm(DELETE_USER, actor=request.user, target=ceo)

    target_user_id_rule_by_actor = get_rules_by_actor(request.user)
    if target_user_id_rule_by_actor:
        target_user_id_rule_by_actor = target_user_id_rule_by_actor.first().target.id

    if not has_perm(SETTINGS__VIEW_ORG_CHART, request.user, ceo):
        # return render(request,'404.html',{"message":"Don't have permission to access this page."})
        return render(request, '403.html')

        # return HttpResponseJsonForbidden()
        # ceo = User.objects.filter(id=request.GET.get('user_id', None)).first()

    ceo_profile_json = ceo.profile.get_json(break_level=1)

    if request.method == "POST":
        if 'is_checked' in request.POST:
            user_id = request.POST.get('user_id', None)
            username = request.POST.get('username', None)
            is_checked = True if request.POST.get('is_checked') == 'true' else False
            org = Profile.objects.filter(user_id=user_id).first()

            if org and request.user.profile.is_superuser_org():
                org.is_admin = is_checked
                org.save()
            return HttpResponse('ok')
        elif request.POST.get('active') == 'active':
            user_id = request.POST.get('user_id', None)
            user = get_user_by_id(user_id)
            is_checked = True
            if not has_perm(MANAGER_REVIEW, actor=request.user, target=user):
                return APIJsonResponse403("You do not have permission to active this user")
            toggle_active(is_checked, user_id, request.user, ignore_perm_check=True)

            profile = get_user_by_id(user_id).profile
            profile.reason = ''
            profile.save(update_fields=['reason'])

            data = {
                'name': render_to_string('performance/chart/node.html',
                                         {'username': profile.user.username,
                                          'short_name': short_name(profile.display_name),
                                          'brief_name': brief_name(profile.display_name),
                                          'STATIC_URL': settings.STATIC_URL,
                                          'active': is_checked,
                                          'avatar': profile.get_avatar(),
                                          'color': None}),
                'active': is_checked
            }
            return HttpResponse(json.dumps(data), content_type="application/json")

        elif request.FILES:
            print request.user
            user_id = request.POST.get('user_id', None)
            user = get_user_by_id(user_id)
            profile = get_profile(user_id, request.user)
            if not profile:
                return APIJsonResponse403("You not permission change avatar")
            if request.FILES.get('file') and user is not None:
                try:
                    avatar = request.FILES.get('file')
                    profile.avatar.save(avatar.name, avatar)
                    profile.save()
                    cache.set('avatar_user' + str(user.id), profile.get_avatar(), 60 * 20)
                    cache.set('avatar_user' + user.email, profile.get_avatar(), 60 * 60 * 2)
                except Exception as e:
                    error = str(e)
                    print error

                    # duan temporary disable EVN
                    # print 'disable EVN '
                    # FUCK FUCK Duan
                    #     if organization.id == 1333:
                    #         return None

        profile_id = request.POST.get('id', None)
        if profile_id:
            try:
                p = Profile.objects.get(pk=profile_id[1:])
                organization = p.get_organization()
                current_quarter = organization.get_current_quarter()
                user = p.user
                email = request.POST.get('email').lower().strip()
                if user.email != email:
                    if get_user_by_email(email):  # User.objects.filter(email=email).exists():
                        return HttpResponse('Email này đã tồn tại.' + user.email)
                    old_email = user.email
                    user.email = email
                    KPI.objects.filter(user_id=user.id, owner_email=old_email).update(owner_email=email)

                    log_change_user_email(request.user, user, old_email, user.email,
                                          user.profile.get_current_quarter())
                if p.display_name != request.POST.get('name') and request.POST.get('name'):
                    username = unsigned_vi(request.POST.get('name')).lower().replace(' ', '')
                    if User.objects.filter(username=username).exists():
                        count = 1
                        while (True):
                            temp = username + str(count)
                            if not User.objects.filter(username=temp).exists():
                                username = temp
                                break
                            count += 1
                    user.username = username
                user.save()

                p.display_name = request.POST.get('name')
                p.title = request.POST.get('position')
                p.phone = request.POST.get('phone')
                p.skype = request.POST.get('skype')
                manager = request.POST.get('manager')
                p.unit_code = request.POST.get('unit_code')
                p.department = request.POST.get('department')
                changed_manager = False
                # if p.report_to and manager and p.report_to.get_profile().id != int(manager[1:]):
                if p.parent and manager and p.parent.id != int(manager[1:]):
                    #
                    # r = Review.objects.filter(organization=organization,
                    #                           reviewee_id=p.user_id,
                    #                           review_type='manager',
                    #                           quarter_period=current_quarter).first()
                    manager = Profile.objects.get(pk=manager[1:])
                    # p.report_to = manager.user
                    p.parent = manager
                    # if r:
                    #     r.reviewer = manager.user
                    #     r.save()
                    changed_manager = True
                try:
                    role_changed = False
                    job_title_id = request.POST.get('job_title_id', '')
                    if job_title_id and JobTitle.objects.filter(pk=int(request.POST.get('job_title_id', ''))).exists():
                        if p.current_job_title_id != int(job_title_id):
                            p.current_job_title_id = int(job_title_id)
                            role_changed = True

                except ValueError:
                    pass
                p.save()
                # set_profile_cache(Profile, p)

                # if role_changed:
                # p.copy_competency(quarter_period, remove=False)
                # p.copy_kpi(quarter_period, remove=False)

                usr_org = p
                if not usr_org:
                    usr_org = Profile.objects.create(organization=organization,
                                                     user=user)
                usr_org.position = request.POST.get('position')
                #   usr_org.employee_code = request.POST.get('employee_code')
                p.employee_code = request.POST.get('employee_code')
                p.unit_code = request.POST.get('unit_code')
                p.save()
                try:
                    usr_org.start_date = parse(request.POST.get('start_date'), dayfirst=True)
                except:
                    pass
                try:
                    usr_org.role_start = parse(request.POST.get('role_start'), dayfirst=True)
                except:
                    pass
                usr_org.save()

                job_title = ""
                if p.current_job_title_id:
                    job_title = p.current_job_title.name
                return HttpResponse(json.dumps({'short_name': short_name(p.display_name, "<br>"),
                                                'changed_manager': changed_manager,
                                                'job_title': job_title}),
                                    content_type="application/json")
            except Exception as e:
                return HttpResponse(str(e))

    from django.db.models import Value, CharField
    from django.db.models.functions import Concat

    # user_id_list = UserOrganization.objects.filter(organization=organization).values_list('user_id', flat=True)
    # we must check None for dispplay_name
    list_name_1 = Profile.objects.filter(organization=organization,
                                         display_name__isnull=False) \
        .exclude(is_deleted=True) \
        .values('display_name', 'user__email', 'user_id') \
        .annotate(nameofuser=Concat('display_name', Value(' ('), 'user__email',
                                    Value(') |'), 'user_id',
                                    output_field=CharField())) \
        .values_list('nameofuser', flat=True)

    list_name_2 = Profile.objects.filter(organization=organization,
                                         display_name__isnull=True).exclude(is_deleted=True) \
        .values('user_id', 'user__email') \
        .annotate(nameofuser=Concat('user__email', Value('|'), 'user_id', output_field=CharField())) \
        .values_list('nameofuser', flat=True)

    list_name = list(list_name_1) + list(list_name_2)
    seachable_user_list = list_name
    # cache.set('searchable_user_list_{}'.format(target_organization.id), list_name, 60 * 10)

    # job_categories = JobCategory.objects.filter(industry_id=organization.industry_id)

    list_uc = UnitCode.objects.filter(organization=organization).values('code', 'name')
    variables = RequestContext(request, {
        'json_data': ceo_profile_json,
        'csrf_token': get_token(request),
        # 'job_categories': job_categories,
        'user_organization': user_organization,
        'can_delete': can_delete,
        'list_name': json.dumps(list(seachable_user_list)),
        'organization': organization,
        'list_uc': list(list_uc),
        'user_request': request.user,
        'target_user_id_rule_by_actor': target_user_id_rule_by_actor
    })
    return render_to_response('performance/people_v5.html', variables)


class PositionChartView(PerformanceBaseView):

    def get(self, request, *args, **kwargs):
        profile = request.user.get_profile()
        organization = profile.get_organization()

        if not organization or not profile:
            return render(request, '403.html', {"message": "Don't have permission to access this page."})

        user_id = request.GET.get('user_id', None)
        if user_id is None:
            ceo_id = organization.ceo_id
        else:
            ceo_id = int(request.GET.get('user_id'))

        try:
            profile = Profile.objects.get(user_id=user_id)
            if profile.is_deleted:  # pragma: no cover  # if is_deleted, it will jump to exception-code can't be reached
                from django.core.urlresolvers import reverse
                return redirect(reverse('kpi_dashboard'))
        except Profile.DoesNotExist:
            pass

        if request.method == "POST":  # pragma: no cover # not reachable as method is get
            if not request.POST.get('user_id', None):
                ceo_id = organization.ceo_id
            else:
                ceo_id = int(request.POST.get('user_id'))

        ceo = User.objects.filter(id=ceo_id).first()  # dat ten bien tao lao qua
        can_delete = has_perm(DELETE_USER, actor=request.user, target=ceo)

        target_user_id_rule_by_actor = get_rules_by_actor(request.user)
        if target_user_id_rule_by_actor:
            target_user_id_rule_by_actor = target_user_id_rule_by_actor.first().target.id

        if not has_perm(SETTINGS__VIEW_ORG_CHART, request.user, ceo):
            # return render(request,'404.html',{"message":"Don't have permission to access this page."})
            return render(request, '403.html')

            # return HttpResponseJsonForbidden()
            # ceo = User.objects.filter(id=request.GET.get('user_id', None)).first()

        position = ceo.profile.get_position()
        if not position:
            position = ceo.profile.get_position(auto_create=True)
            generate_position_chart(organization)
            # if settings.ENABLE_CELERY:
            #     start_generate_position_chart.apply_async(args=[organization])
            # else:
            #     with transaction.atomic():
            #         generate_position_chart(organization)

        chart = {}
        if position:
            chart = position.get_position_tree_data(break_level=1)

        context = {
            'chart': json.dumps(chart, cls=DjangoJSONEncoder)
        }
        return render(request, 'performance/people_v5.html', context)


class NodePositionView(PerformanceBaseView):

    def get(self, request, *args, **kwargs):
        node_id = request.GET.get('node_id', None)
        if node_id:
            position = get_position(node_id[1:])
            children = position.get_sub_positions(break_level=2)

            response = {
                'children': children
            }
            return HttpResponse(json.dumps(response), content_type="application/json")

        return HttpResponse("Invalid")
