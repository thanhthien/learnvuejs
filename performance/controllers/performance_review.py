#!/usr/bin/python
# -*- coding: utf-8 -*-

import datetime
import json
import traceback

from django.utils.translation import ugettext

from authorization.rules import SETTINGS__VIEW_ORG_CHART, KPI__EDITING
from authorization.rules import has_perm
from dateutil.parser import parse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.paginator import Paginator
from django.http import HttpResponseRedirect
from django.http.response import HttpResponse, Http404
from django.shortcuts import get_object_or_404, render
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.template.defaultfilters import linebreaksbr
from django.utils.dateformat import format
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from performance.base.views import PerformanceBaseView

from performance.controllers.competency import update_competency
from performance.db_models.competency import Competency
from performance.db_models.kpi_library import JobTitle
# from performance.documents import FinalScoreHistory
from performance.forms import MemberForm
from performance.forms import PerformanceForm, InvitationReviewForm
from performance.forms import SmartKPIForm
from performance.logger import KPILogger
from performance.models import KPI, \
    DescriptionCell, QuarterPeriod
from performance.services import add_new_cascadeKPIQueue
# from performance.services.Review import create_review, create_core_value
from performance.views import update_kpi
from user_profile.models import Profile
from user_profile.services.profile import get_employee_code
from utils import unsigned_vi, brief_name
from utils.common import HttpResponseJson


@login_required
@csrf_exempt
# @consultant_permission
def teamperformance_view(request): #pragma: no cover
    user_id = request.GET.get('team', None)
    detail_id = request.GET.get('user_id', None)

    if user_id and detail_id and user_id != detail_id:
        if request.user.id == int(detail_id):
            return HttpResponseRedirect(request.user.profile.get_kpi_url(self_review=True))

    super_permission = False
    if user_id:
        try:
            user_profile = Profile.f_profile_kpi(request.user.id)
            current_organization = user_profile.get_organization()

            view_user_profile = Profile.f_profile_kpi(user_id)
            view_user_organization = view_user_profile.get_organization()

            if view_user_organization != current_organization:
                raise Http404
            # user_profile_team = request_user.get_profile().get_my_team(me=True).values_list('id', flat=True)

            # maybe check for none permission request
            # then, get all users of the request user's team,
            # what user kpis to get?
            user_can_edit = False  # unuse
            user_can_view = False  # unuse
            searchable_users = None
            searchable_data = []
            permission = False
            super_permission = False
            # if  request_user.is_staff or is_consultant(request_user)\
            #     or UserOrganization.objects.filter(Q(is_superuser=True) | Q(is_admin=True),
            #                                             user=request_user).exists():
            target = User.objects.get(id=detail_id)
            if not has_perm(SETTINGS__VIEW_ORG_CHART, request.user, target):
                return HttpResponse(ugettext("Permission Denied!"))
        except:
            raise Http404

    profile = Profile.f_profile_kpi(request.user.id)
    organization = profile.get_organization()
    current_quarter_period = organization.get_current_quarter()
    quarter_period = None

    if 'quarter' in request.GET:
        try:
            due_date = parse(request.GET['quarter'], dayfirst=True)
            quarter_period = QuarterPeriod.objects.get(due_date=due_date,
                                                       organization=organization)
        except:
            quarter_period = None

    if not quarter_period:
        quarter_period = current_quarter_period

    user_org = profile.get_user_organization()

    user = request.user

    if user_id and user_id.isdigit() and (
                (user_org.is_superuser or user_org.is_admin or user) or request.user.is_staff):
        try:
            if request.user.is_staff:
                uo = Profile.objects.get(user_id=int(user_id))
                organization = uo.organization
                quarter_period = organization.get_current_quarter()
            else:
                uo = Profile.objects.get(organization=organization, user_id=int(user_id))
            user = uo.user
        except:
            if request.user.is_staff:
                user = get_object_or_404(User, id=int(user_id))

    if request.method == "POST" and 'user_id' in request.POST:
        user_id = request.POST['user_id']
        share_to_self = True if request.POST['share_to_self'] == 'true' else False
        share_to_all = True if request.POST['share_to_all'] == 'true' else False
        # Review.objects.filter(organization=organization,
        #                       reviewer=user,
        #                       reviewee_id=user_id,
        #                       review_type='manager',
        #                       quarter_period=quarter_period).update(share_to_all=share_to_self)
        #
        # Review.objects.filter(organization=organization,
        #                       reviewer=user,
        #                       reviewee=None,
        #                       review_type='manager',
        #                       quarter_period=quarter_period).update(share_to_all=share_to_all)
        return HttpResponse('ok')

    if request.method == "POST" and request.is_ajax() and 'share_to_manager' in request.POST:
        share_to_manager = True if request.POST.get('share_to_manager', '') == "true" else False
        share_to_all = True if request.POST.get('share_to_all', '') == 'true' else False
        # Review.objects.filter(organization=organization,
        #                       reviewer=user,
        #                       review_type='manager',
        #                       quarter_period=quarter_period).update(share_to_all=share_to_all,
        #                                                             share_to_manager=share_to_manager)
        return HttpResponse('ok')

    u_profile = user.get_profile()
    team = u_profile.get_my_team(only_this_user_id=detail_id)

    # if detail_id:
    #     only_this_user_id = Profile.objects.filter(user_id=detail_id)
    #     only_this_user_id.fix_kpi_errors()


    # create_review(organization, user, team, quarter_period)

    can_edit = organization.enable_to_edit()
    if request.GET.get('json_data', None):

        data = []

        pages = Paginator(team, 2)
        page = request.GET.get('page', 1)
        team_list = pages.page(page)
        next_page = None
        if team_list.has_next():
            next_page = int(page) + 1

        ignore_zero = True
        if organization.id == 47:
            ignore_zero = False

        no_cache = request.GET.get('no_cache', None)

        for i in team_list:

            i.fix_kpi_errors(no_cache)

            usr_org = i.get_user_organization()
            if not usr_org:
                continue
            job_title_name = ""
            if i.current_job_title_id:
                job_title_name = i.current_job_title.name
            #
            # self_review = Review.objects.get_or_create(organization=organization,
            #                                            reviewee_id=i.user_id,
            #                                            reviewer_id=i.user_id,
            #                                            review_type='self',
            #                                            quarter_period=quarter_period)[0]
            share_to_manager = True
            share_to_self = True

            max_min_kpi = i.get_bound_score_kpi(quarter_period)
            max_min_competency = i.get_bound_score_competency(quarter_period)
            #  create_core_value(i.user, organization, quarter_period)

            # try:
            #     scores = FinalScoreHistory.objects.filter(organization=organization.id,
            #                                               quarter_period__lt=quarter_period.id,
            #                                               user_id=i.user_id) \
            #                  .order_by('quarter_period') \
            #                  .values_list('due_date', 'quarter_name',
            #                               'competency_score',
            #                               'kpi_score')[:10]
            #     history_scores = json.loads(scores.to_json())
            # except:
            #     history_scores = []

            kpis = i.kpis_dict('self', quarter_period, share_to_manager, user.id)
            if organization.enable_competency:
                competencies = i.competencies_dict('self', quarter_period, share_to_manager, user.id)
            else:
                competencies = []
            user_data = {
                'user_id': i.user_id,
                'profile': i.id,
                # 'report_to': i.report_to_id,
                'report_to': i.parent.user_id if i.parent else None,
                'name': i.display_name,
                'email': i.user.email,
                'phone': i.phone,
                'skype': i.skype,
                'start_date': format(usr_org.start_date, 'd-m-Y') if usr_org.start_date else '',
                'role_start': format(usr_org.role_start, 'd-m-Y') if usr_org.role_start else '',
                'employee_code': get_employee_code(i),  # usr_org.employee_code,
                'avatar': i.get_avatar(),
                'job_title_name': job_title_name,
                'position': i.title,
                'brief_name': brief_name(i.display_name),
                'competencies': competencies,
                'kpis': kpis,
                'share_to_manager': share_to_manager,
                'short_term_career': i.short_term_career,
                'short_term_development': i.short_term_development,
                'long_term_career': i.long_term_career,
                'long_term_development': i.long_term_development,
                'competency_final_score': i.score_full_competency(quarter_period, ignore_zero),
                'kpi_final_score': i.score_full_kpi_percent(quarter_period, ignore_zero),
                'min_score_kpi': max_min_kpi[0],
                'max_score_kpi': max_min_kpi[1],
                'min_score_competency': max_min_competency[0],
                'max_score_competency': max_min_competency[1],
                'strength_kpi': i.get_strength_kpi(quarter_period),
                'strength_competency': i.get_strength_competency(quarter_period),
                'weak_kpi': i.get_weak_kpi(quarter_period),
                'weak_competency': i.get_weak_competency(quarter_period),
                'self_percent_finished': 0,  # self_review.completion,
                'share_to_self': share_to_self,
                'reviews': i.get_360_review_dict(quarter_period),
                'is_deputy_manager': i.is_deputy_manager,
                'review_progress_html': i.review_progress_html(),
                'previous_competency_final_score': i.previous_competency_final_score,
                'previous_kpi_final_score': i.previous_kpi_final_score,
                'history_scores': history_scores,
                'is_active': profile.is_active(),
                'status': 'active' if profile.is_active() else "inactive"
            }
            data.append(user_data)

        items = {}
        for i in DescriptionCell.objects.all():
            temp = i.slug.replace('-', '_')
            items[temp] = linebreaksbr(i.description)

        current = current_quarter_period.id == quarter_period.id
        expired_review = current_quarter_period.due_date < datetime.date.today()
        return HttpResponse(
            json.dumps([data, items, {'next_page': next_page, 'current': current, 'quarter_period': quarter_period.id,
                                      'enable_competency': organization.enable_competency,
                                      "enable_bsc": organization.enable_bsc,
                                      'can_edit': can_edit,
                                      'expired_review': expired_review}], indent=4),
            content_type="application/json")

    if request.method == "POST":
        if 'type' in request.POST and request.POST['type'] == 'kpi':
            return update_kpi(request, quarter_period, organization.enable_bsc)
        elif 'type' in request.POST and request.POST['type'] == 'comp':
            return update_competency(request, quarter_period)

    form = PerformanceForm()
    member_form = MemberForm(organization)
    kpi_form = SmartKPIForm(True)

    jobs_title = []
    for i in team:
        if i.current_job_title and i.current_job_title.name not in jobs_title:
            jobs_title.append(i.current_job_title.name)

    cells = DescriptionCell.objects.all()
    jobs = JobTitle.objects.filter(category__industry_id=organization.industry_id)

    invitation_form = InvitationReviewForm()

    remain = (quarter_period.due_date - datetime.date.today()).days
    remain = remain if remain > 0 else 0
    profile = Profile.f_profile_kpi(user.id)
    subordinate_team = profile.get_all_subordinate()

    quarter_periods = organization.get_all_quarter_period()

    # # Khang fix bug admin don't have permission to create new competency when enable_to_edit return false
    fresh_user = User.objects.get(id=request.user.id)
    target_user = fresh_user.profile.get_organization().ceo  # # This must be the ceo, due to the super_permission definition
    if has_perm(KPI__EDITING, fresh_user, target_user):
        super_permission = True
    variables = RequestContext(request, {
        'organization': organization,
        'team': team,
        'jobs_title': jobs_title,
        'share_to_manager': True,
        'share_to_all': True,
        'cells': cells,
        'jobs': jobs,
        'form': form,
        'kpi_form': kpi_form,
        'member_form': member_form,
        'c_user': user,
        'fresh_user': fresh_user,
        'remain': remain,
        'quarter_period': quarter_period,
        'invitation_form': invitation_form,
        'manager': True,
        'subordinate_team': subordinate_team,
        'quarter_periods': quarter_periods,

        'super_permission': super_permission,
        #         'user_can_edit':user_can_edit,
        #         'user_can_view':user_can_view,
        #         'permission':permission,
    })

    return render_to_response('performance/teamperformance.html', variables)


@login_required
@csrf_exempt
def teamperformance_update(request): #pragma: no cover
    profile = request.user.get_profile()
    organization = profile.get_organization()
    quarter_period = organization.get_current_quarter()
    score = 0
    if request.method == "POST":
        if 'review' in request.GET:
            percent = request.POST.get('percent', 0)
            user_id = request.POST.get('user_id', 0)
            profile = Profile.objects.filter(user_id=user_id).first()

            # if profile and not profile.report_to_id:
            if profile and not profile.parent:
                return HttpResponse(percent)
            if profile:
                # manager = profile.report_to
                manager = profile.parent.user if profile.parent else None

            try:
                pass
                # review = Review.objects.filter(organization=organization,
                #                                reviewee_id=user_id,
                #                                reviewer=manager,
                #                                review_type='manager',
                #                                quarter_period=quarter_period).first()
                # percent = float(percent)
                # if review and review.completion != percent:
                #     review.completion = percent
                #     review.save()
                # print float(percent),
            except:
                print traceback.format_exc()

            return HttpResponse(percent)
        elif 'kpi' in request.GET:
            kpi_id = request.POST.get('kpi_id', None)
            value = request.POST.get('value', None)
            user_id = request.POST.get('person', None)
            reset = request.POST.get('reset', None)
            try:
                kpi = KPI.objects.get(pk=kpi_id, user_id=user_id)
                kpi.set_score(value, request.user.id)
                if kpi.parent:
                    score, m1, m2, m3 = kpi.parent.calculate_avg_score(request.user.id)
                    kpi.parent.set_score(score, request.user.id)
                    # CascadeKPIQueue.objects.create(kpi=kpi.parent)
                    add_new_cascadeKPIQueue(kpi.parent)
                elif reset == "1":
                    kpi.reset_child_score(request.user.id)

                if not kpi.parent_id and kpi.cascaded_from_id:
                    # CascadeKPIQueue.objects.create(kpi=kpi)
                    add_new_cascadeKPIQueue(kpi)

                profile = User.objects.get(pk=user_id).get_profile()
                final_score = profile.score_full_kpi_percent(quarter_period)
                min_score, max_score = profile.get_bound_score_kpi(quarter_period)
                strength_kpi = profile.get_strength_kpi(quarter_period)
                weak_kpi = profile.get_weak_kpi(quarter_period)

            except Exception as e:
                return HttpResponse(str(e))

            return HttpResponse(json.dumps({'status': 'ok',
                                            'score': score,
                                            'parent_id': kpi.parent_id,
                                            'final_score': final_score,
                                            'min_score': min_score,
                                            'max_score': max_score,
                                            'strength_kpi': strength_kpi,
                                            'weak_kpi': weak_kpi}),
                                content_type="application/json")
        elif 'comp' in request.GET:
            comp_id = request.POST.get('comp_id', None)
            value = request.POST.get('value', None)
            user_id = request.POST.get('person', None)
            reset = request.POST.get('reset', None)
            try:
                comp = Competency.objects.get(pk=comp_id, user_id=user_id)
                comp.set_score(value, request.user.id)
                if comp.parent:
                    score = comp.parent.calculate_avg_score(request.user.id)
                    comp.parent.set_score(score, request.user.id)
                elif reset == "1":
                    comp.reset_child_score(request.user.id)

                profile = User.objects.get(pk=user_id).get_profile()
                final_score = profile.score_full_competency(quarter_period)
                min_score, max_score = profile.get_bound_score_competency(quarter_period)
                strength_competency = profile.get_strength_competency(quarter_period)
                weak_competency = profile.get_weak_competency(quarter_period)
            except Exception as e:
                return HttpResponse(str(e))

            return HttpResponse(json.dumps({'status': 'ok',
                                            'score': score,
                                            'parent_id': comp.parent_id,
                                            'final_score': final_score,
                                            'min_score': min_score,
                                            'max_score': max_score,
                                            'strength_competency': strength_competency,
                                            'weak_competency': weak_competency}),
                                content_type="application/json")
        elif 'current_goal' in request.POST:
            kpi_id = request.POST.get('id', None)
            value = request.POST.get('current_goal', None)
            user_id = request.POST.get('user_id', None)
            try:
                kpi = KPI.objects.get(pk=kpi_id, user_id=user_id)
                kpi.current_goal = value
                changed = kpi.get_changed_history()
                kpi.save()
                if kpi.assigned_to_id:
                    cascade_kpi = KPI.objects.filter(user_id=kpi.assigned_to_id,
                                                     cascaded_from=kpi).first()
                    if cascade_kpi:
                        cascade_kpi.current_goal = value
                        cascade_kpi.save()
                elif kpi.cascaded_from_id:
                    cascaded_from = kpi.cascaded_from
                    if cascaded_from:
                        cascaded_from.current_goal = value
                        cascaded_from.save()

                KPILogger(request.user, kpi, 'update', changed).start()
                return HttpResponse('ok')
            except Exception as e:
                return HttpResponse(str(e))
        elif 'future_goal' in request.POST:
            kpi_id = request.POST.get('id', None)
            value = request.POST.get('future_goal', None)
            user_id = request.POST.get('user_id', None)
            try:
                kpi = KPI.objects.get(pk=kpi_id, user_id=user_id)
                kpi.future_goal = value
                changed = kpi.get_changed_history()
                kpi.save()

                if kpi.assigned_to_id:
                    cascade_kpi = KPI.objects.filter(user_id=kpi.assigned_to_id,
                                                     cascaded_from=kpi).first()
                    if cascade_kpi:
                        cascade_kpi.future_goal = value
                        cascade_kpi.save()
                elif kpi.cascaded_from_id:
                    cascaded_from = kpi.cascaded_from
                    if cascaded_from:
                        cascaded_from.future_goal = value
                        cascaded_from.save()

                KPILogger(request.user, kpi, 'update', changed).start()
                return HttpResponse('ok')
            except Exception as e:
                return HttpResponse(str(e))
        elif 'manager_note' in request.POST:
            kpi_id = request.POST.get('id', None)
            value = request.POST.get('manager_note', None)
            user_id = request.POST.get('user_id', None)
            try:
                kpi = KPI.objects.get(pk=kpi_id, user_id=user_id)
                kpi.outcome_notes = value
                changed = kpi.get_changed_history()
                kpi.save()
                KPILogger(request.user, kpi, 'update', changed).start()
                return HttpResponse('ok')
            except Exception as e:
                return HttpResponse(str(e))
        elif 'self_note' in request.POST:
            kpi_id = request.POST.get('id', None)
            value = request.POST.get('self_note', None)
            user_id = request.POST.get('user_id', None)
            try:
                kpi = KPI.objects.get(pk=kpi_id, user_id=user_id)
                kpi.outcome_notes_self = value
                kpi.save()
                return HttpResponse('ok')
            except Exception as e:
                return HttpResponse(str(e))
        elif 'evidence_self' in request.POST:
            evidence = request.POST.get('evidence_self')
            comp_id = request.POST.get('id', None)
            user_id = request.POST.get('user_id', None)
            try:
                com = Competency.objects.get(id=comp_id, user_id=user_id)
                com.comment_evidence_self = evidence
                changed = com.get_changed_history()
                com.save()
                KPILogger(request.user, com, 'update', changed).start()
                return HttpResponse('ok')
            except Exception as e:
                return HttpResponse(str(e))
        elif 'evidence_manager' in request.POST:
            evidence = request.POST.get('evidence_manager')
            comp_id = request.POST.get('id', None)
            user_id = request.POST.get('user_id', None)
            try:
                com = Competency.objects.get(id=comp_id, user_id=user_id)
                com.comment_evidence = evidence
                changed = com.get_changed_history()
                com.save()
                KPILogger(request.user, com, 'update', changed).start()
                return HttpResponse('ok')
            except Exception as e:
                return HttpResponse(str(e))
        else:
            # return HttpResponse("Request Invalid")
            return HttpResponseJson({'status': 'Bad Request'})
    else:
        return HttpResponseJson({'status': 'Bad Request'})

#
# @login_required
# @csrf_exempt
# def selfperformance_update(request):
#     profile = request.user.get_profile()
#     organization = profile.get_organization()
#     quarter_period = organization.get_current_quarter()
#     score = 0
#     if request.is_ajax() and request.method == "POST":
#         if 'review' in request.GET:
#             # review = Review.objects.get_or_create(organization=organization,
#             #                                       reviewee=request.user,
#             #                                       reviewer=request.user,
#             #                                       review_type='self',
#             #                                       quarter_period=quarter_period)[0]
#             percent = request.POST.get('percent', 0)
#             # try:
#             #     if review.completion != float(percent):
#             #         review.completion = float(percent)
#             #         review.save()
#             # except:
#             #     pass
#             return HttpResponse(percent)
#
#         elif 'kpi' in request.GET:
#             kpi_id = request.POST.get('kpi_id', None)
#             value = request.POST.get('value', None)
#             user_id = request.POST.get('person', None)
#             reset = request.POST.get('reset', None)
#             try:
#                 kpi = KPI.objects.get(pk=kpi_id, user_id=user_id)
#                 kpi.set_score(value, request.user.id)
#                 if kpi.parent:
#                     score, m1, m2, m3 = kpi.parent.calculate_avg_score(request.user.id)
#                     kpi.parent.set_score(score, request.user.id)
#                     if kpi.parent.cascaded_from_id:
#                         add_new_cascadeKPIQueue(kpi.parent)
#                         # CascadeKPIQueue.objects.create(kpi=kpi.parent)
#                 elif reset == "1":
#                     kpi.reset_child_score(request.user.id)
#
#                 if kpi.parent_id is None and kpi.cascaded_from_id:
#                     add_new_cascadeKPIQueue(kpi)
#                     # CascadeKPIQueue.objects.create(kpi=kpi)
#
#                 profile = User.objects.get(pk=user_id).get_profile()
#                 final_score = profile.score_full_kpi_percent(quarter_period)
#                 min_score, max_score = profile.get_bound_score_kpi(quarter_period)
#                 strength_kpi = profile.get_strength_kpi(quarter_period)
#                 weak_kpi = profile.get_weak_kpi(quarter_period)
#             except Exception as e:
#                 return HttpResponse(str(e))
#
#             return HttpResponse(json.dumps({'status': 'ok',
#                                             'score': score,
#                                             'parent_id': kpi.parent_id,
#                                             'final_score': final_score,
#                                             'min_score': min_score,
#                                             'max_score': max_score,
#                                             'strength_kpi': strength_kpi,
#                                             'weak_kpi': weak_kpi}),
#                                 content_type="application/json")
#         elif 'comp' in request.GET:
#             comp_id = request.POST.get('comp_id', None)
#             value = request.POST.get('value', None)
#             user_id = request.POST.get('person', None)
#             reset = request.POST.get('reset', None)
#             try:
#                 comp = Competency.objects.get(pk=comp_id, user_id=user_id)
#                 comp.set_score(value, request.user.id)
#                 if comp.parent:
#                     score = comp.parent.calculate_avg_score(request.user.id)
#                     comp.parent.set_score(score, request.user.id)
#                 elif reset == "1":
#                     comp.reset_child_score(request.user.id)
#
#                 profile = User.objects.get(pk=user_id).get_profile()
#                 final_score = profile.score_full_competency(quarter_period)
#                 min_score, max_score = profile.get_bound_score_competency(quarter_period)
#                 strength_competency = profile.get_strength_competency(quarter_period)
#                 weak_competency = profile.get_weak_competency(quarter_period)
#             except Exception as e:
#                 return HttpResponse(str(e))
#
#             return HttpResponse(json.dumps({'status': 'ok',
#                                             'score': score,
#                                             'parent_id': comp.parent_id,
#                                             'final_score': final_score,
#                                             'min_score': min_score,
#                                             'max_score': max_score,
#                                             'strength_competency': strength_competency,
#                                             'weak_competency': weak_competency}),
#                                 content_type="application/json")
#         else:
#             # return HttpResponse("Request Invalid")
#             return HttpResponseJson({'status': 'Bad Request'})
#     else:
#         # return HttpResponse("Bad Request")
#         return HttpResponseJson({'status': 'Bad Request'})
#

# class PerformanceExporter(PerformanceBaseView):
#     @method_decorator(login_required)
#     @method_decorator(csrf_exempt)
#     def dispatch(self, *args, **kwargs):
#         return super(PerformanceExporter, self).dispatch(*args, **kwargs)
#
#     def post(self, request, *args, **kwargs):
#         organization = self.get_organization(request.user)
#
#         try:
#             quarter_period = get_object_or_404(QuarterPeriod,
#                                                id=request.POST.get('quarter_period'),
#                                                organization=organization)
#         except:
#             quarter_period = organization.get_current_quarter()
#
#         # try:
#         if 'export' in request.POST and request.POST['export'] == 'pdf':
#             user_id = request.POST['user_id']
#             if not user_id:
#                 user_id = request.user.id
#             u = Profile.objects.get(user_id=user_id)
#             return export_kpi_to_pdf(u.get_my_team(me=True), organization, quarter_period)
#         elif 'export' in request.POST:
#             user_id = request.POST['user_id']
#             if not user_id:
#                 user_id = request.user.id
#             u = Profile.objects.get(user_id=user_id)
#             profile_list = u.get_my_team(me=True)
#             # profile_list = [u]
#             return  Http404() #export_kpi_list(profile_list, organization, quarter_period)
#         if 'person_export' in request.POST:
#             user_id = request.POST['user_id']
#             if not user_id:
#                 user_id = request.user.id
#             u = Profile.objects.get(user_id=user_id)
#             file_name = u'[%s] KPIs & nang luc - %s' % (
#                 format(quarter_period.due_date, 'd-m-Y'), unsigned_vi(u.display_name),)
#             raise Http404# return export_kpi_list([u, ], organization, quarter_period, file_name)

#
# class ExportFull(PerformanceBaseView):
#     def get(self, request, *args, **kwargs):
#         user_id = kwargs['user_id']
#         user = User.objects.get(id=user_id)
#         request.session[str(request.user.id)] = {'4309': 1}
#         if str(request.user.id) not in request.session:
#             request.session[str(request.user.id)] = {'4309':1}
#
#         from utils.common import to_json, json_default_handler, FloatEncoder
#         # get list all subordinates of sepecified user
#         # all_subordinates_ids= user.profile.get_all_subordinate(type='profile_id')
#
#         # all_subordinates= user.profile.get_all_subordinate()
#         # all_subordinates=all_subordinates.annotate(exported=Value(request.session[request.user.id].get(F('user_id'), 0), output_field=IntegerField()))
#
#         all_subordinates = to_json(user.profile.get_all_subordinate())
#         all_subordinates = json.dumps(all_subordinates, default=json_default_handler, cls=FloatEncoder, indent=2)
#         sessions = json.dumps(request.session[str(request.user.id)])
#         return render(request, 'performance/bsc/worksheet/export-full.html', { 'subordinates':all_subordinates, 'sessions':sessions})
#         pass
#


# it's no use
# class KPIMobileReview(PerformanceBaseView):
#     def get(self, request, *args, **kwargs):
#         response = render(request, 'performance/mobile/self_review.html', {})
#
#         return response
