#!/usr/bin/python
# -*- coding: utf-8 -*-
import json

from dateutil.parser import parse
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from user_profile.services.profile import get_current_user_viewed
from authorization.rules import has_perm, SETTINGS__VIEW_ORG_CHART, REPORT_ADMIN
from elms import settings
from performance.documents import KpiQuarterFinalScore
from performance.models import QuarterPeriod  #, Review

# from tracking.models import Visitor
from user_profile.models import Profile
from user_profile.services.user import get_user

from utils.common import HttpResponseJson, HttpResponseJson404


# @login_required
# # #@consultant_permission
# def helicopter(request):
#     organization = request.user.get_profile().get_organization()
#     quarter_period = organization.get_current_quarter()
#
#     if 'quarter_period' in request.GET:
#         try:
#             due_date = parse(request.GET['quarter_period'], dayfirst=True)
#             q = QuarterPeriod.objects.get(organization=organization,
#                                           due_date=due_date)
#             quarter_period = q
#         except:
#             pass
#
#     if request.GET.get('json_data', None):
#         people = Profile.objects.filter(organization=organization)
#         names = list(people.values_list('display_name', flat=True))
#
#         data = KpiQuarterFinalScore.objects.filter(organization=organization.id,
#                                                    quarter_period=quarter_period.id).to_json()
#         data = json.loads(data)
#
#         return HttpResponse(json.dumps([data, names], indent=4),
#                             content_type="application/json")
#
#     # cal = KPIFinalScoreCalculator(organization, quarter_period)
#     # cal.start()
#
#     variables = RequestContext(request, {
#         'organization': organization,
#         'quarter_periods': organization.get_all_quater_period(),
#         'current_quarter_period': quarter_period,
#         'STATIC_URL': settings.STATIC_URL,
#     })
#
#     if settings.LOCAL:
#         template = 'performance/helicopter.html'
#     else:
#         template = 'performance/helicopter.html'
#     return render_to_response(template, variables)

#
# class ReportScore(View):
#     # score calculated at def report_exporter(request):
#
#
#     @method_decorator(login_required)
#     @method_decorator(csrf_exempt)
#     def dispatch(self, request, *args, **kwargs):
#         return View.dispatch(self, request, *args, **kwargs)
#
#     # @method_decorator(login_required)
#     # @method_decorator(csrf_exempt)
#     # @classmethod
#     # def get_all_subordinate_ids(cls, manager_id, type="user_id", ignore_inactive=True, join_before_date=None, usecache=True, all_user=False):
#     #     pass
#
#     @classmethod
#     def get_all_subordinate(cls, manager_id, type="user_id", ignore_inactive=False, join_before_date=None,
#                             usecache=True):
#         '''
#
#         :param manager_id:
#         :param type: user_id, profile
#         :param ignore_inactive:
#         :param join_before_date:
#         :param usecache:
#         :return:
#         '''
#         manager = User.objects.filter(id=manager_id).first()
#         if not manager:
#             return None
#
#         subordinates = None
#         join_before_date_str = join_before_date.strftime("%Y-%m-%d")
#
#         cache_key = 'all_subordinate_{}_{}_{}_{}'.format(type, manager_id, ignore_inactive, join_before_date_str)
#
#         if usecache:
#             subordinates = cache.get(cache_key)
#             print 'use caching subordinate {}'.format(type,)
#         if not subordinates or not usecache:
#             print 'no caching subordinate {}'.format(type,)
#             print '(type={}, ignore_inactive={}, join_before_date={})'.format(type, ignore_inactive,
#                                                                                              join_before_date)
#             subordinates = manager.profile.get_all_subordinate(type=type, ignore_inactive=ignore_inactive,
#                                                                join_before_date=join_before_date, include_self=True)
#             cache.set(cache_key, subordinates, 60 * 60 * 2)  # caching 2 hours
#             try:
#                 print 'len(subordinates) {}'.format(len(subordinates),)
#             except:
#                 pass
#         return subordinates
#
#     def get(self, request, *args, **kwargs):
#         profile = request.user.get_profile()
#         organization = profile.get_organization()
#
#         quarter_id = kwargs['quarter_id']
#         quarter_id = quarter_id.split('-')
#
#         manager_id = request.GET.get('manager_id', None)
#         if request.method == "POST":
#             manager_id = request.POST.get('manager_id', None)
#
#         try:
#             if manager_id:
#                 uo = Profile.objects.get(user_id=manager_id)
#             else:
#                 manager_id = organization.ceo_id
#
#             quarter_periods = QuarterPeriod.objects.filter(id__in=quarter_id,
#                                                            organization=organization).order_by('id')
#         except:
#             raise Http404
#
#         if quarter_periods.count() <= 1:
#             return self.export_one_quarter(request, organization,
#                                            quarter_periods.first(),
#                                            manager_id=manager_id)
#         else:
#             return self.export_many_quarter(request, organization,
#                                             quarter_periods, manager_id=manager_id)
#
#     def post(self, request, *args, **kwargs):
#         return self.get(request, *args, **kwargs)
#
#     def export_one_quarter(self, request, organization, quarter_period, manager_id):
#         report = []
#
#
#         from utils.common import get_redis
#
#         # report_count=report.count()
#
#
#         report_count = cache.get('report_score_quarter{}_user{}_current'.format(quarter_period.id, manager_id), 0)
#         # report_count=get_redis('report_score_quarter{}_user{}_current'.format(quarter_period.id, manager_id))
#         print 'report_count', report_count
#         if not report_count:
#             report_count = 0
#         else:
#             report_count = int(report_count)
#
#         #
#         # if not report_count:
#         #     report_count=0
#
#         # is_running= total_users = get_redis('report_score_quarter{}_user{}'.format(quarter_period.id, manager_id))
#
#         # total_users = get_redis('report_score_quarter{}_user{}'.format(quarter_period.id, manager_id))
#         total_users = cache.get('report_score_quarter{}_user{}'.format(quarter_period.id, manager_id))
#         print 'total_users', total_users
#
#         # if not is_running and report.count() == 0:
#         # if not is_running and report_count == 0:
#         #     ReportScoreCalculator(organization=organization, quarter_period=quarter_period, manager_id=manager_id).start()
#         if not total_users:
#             total_users = 1
#         else:
#             total_users = int(total_users)
#         # total_users = len(report_user_ids)
#
#
#         # need fix for multi-quarter report
#         # if total_users > report.count():
#
#         if total_users > report_count:
#
#             # loading
#             return render(request, 'report/report_score.html', {
#                 'loading': True,
#                 'calculated_users': report_count,
#                 # 'calculated_users': report.count(),
#                 'total_users': total_users,
#                 'quarter_id': quarter_period.id
#             })
#         else:
#             due_date = datetime.datetime.combine(quarter_period.due_date, datetime.time(0, 0))
#
#             report_user_ids = []
#
#             # include nhung nguoi inactive
#             # report_user_ids = self.get_all_subordinate(manager_id, type='user_id', ignore_inactive=True, join_before_date=due_date, all_user=all_user)
#             report_user_ids = self.get_all_subordinate(manager_id, type='user_id', ignore_inactive=True, join_before_date=due_date, include_self=True)
#
#
#             # TODO: CHECK HERE
#             # ReportScoreFinal dd tinh tai file performance/schedule.py
#             report = ReportScoreFinal.objects.filter(organization=organization.id,
#                                                      quarter_period=quarter_period.id,
#                                                      user_id__in=report_user_ids,
#                                                      is_computed=True).order_by('created_at')
#             # print 'report_count', report_count
#             # print 'total_users', total_users
#             paginator = Paginator(report, 50)
#             page = request.GET.get('page')
#             try:
#                 report_cut = paginator.page(page)
#                 current = report_cut.number
#             except PageNotAnInteger:
#                 report_cut = paginator.page(1)
#                 current = 1
#             except EmptyPage:
#                 report_cut = paginator.page(paginator.num_pages)
#
#             if 'posted' in request.POST:
#                 if request.POST['posted'] == 'yes':
#                     report_competency = ['Potential (Self Review)', 'Potential (Manager Review)', 'Potential Score', ]
#                     # if True: # not organization.enable_competency:
#                     if not organization.enable_competency:
#                         report_competency = []
#                     header = ['STT', _('Name'), _('Email'),
#                               _('Manager'),  # 'Manager ID',
#                               _('Org Chart'),
#                               'Delayed KPIs', 'Total KPIs (Self)',  # 'Delayed KPIs (Assigned)',
#                               'Total KPIs (Assigned)', _('Total Weighting Score')
#                               #   ,'Confirm KPIs'
#                               ] + report_competency + ['Performance Score (Quarter KPIs)',
#                                                        'Performance Score (Monthly Average)',
#                                                        quarter_period.month_1_name(),
#                                                        quarter_period.month_2_name(),
#                                                        quarter_period.month_3_name(),
#
#                                                        # 'Grid Position'
#                                                        ]  # 'Performance (Self Review)', 'Performance (Manager Review)',
#                     results = []
#                     results.insert(0, header)
#                     company_info = {
#                         'name': organization.name,
#                         'logo': organization.get_logo(),
#                         'date': format(datetime.date.today(), 'd-m-Y'),
#                         'quarter_period': quarter_period.quarter_name
#                     }
#                     widths = [5, 25, 15, 25, 15, 30, 15, 15,
#                               20, 20, 20, 20, 20,
#                               # 20, 20,
#                               20, 20, 20]
#
#                     count = 1
#                     url = settings.DOMAIN  # "http://" + Site.objects.get_current().domain
#
#                     for obj in report:
#                         score_comp = [obj.potential_score_self, obj.potential_score_manger, obj.potential_score, ]
#                         if not organization.enable_competency:
#                             score_comp = []
#
#                             # delayed_kpi = "%s/%s" % (obj.delayed_kpi, obj.total_self_kpi)
#                         # delayed_kpi
#                         item = [count,
#                                 # [obj.employee_name, url + reverse('kpi_dashboard_emp',kwargs={'year': 2016, 'user_id':obj.user_id}) ],
#                                 [obj.employee_name, url + reverse('kpi_editor_emp', kwargs={'user_id': obj.user_id})],
#                                 obj.email,
#                                 obj.manager_email,
#                                 #  obj.manager_code,
#                                 obj.org_chart,
#                                 obj.delayed_self_kpi,
#                                 obj.total_self_kpi,
#                                 # obj.delayed_assigned_kpi,
#                                 obj.total_assigned_kpi,
#                                 obj.total_weighting_score  # , obj.get_confirm_kpis
#                                 ] + \
#                                score_comp + \
#                                [
#                                    # obj.kpi_percent_self,
#                                    # obj.kpi_percent_manager,
#                                    obj.kpi_percent,
#                                    round((obj.kpi_percent_m1 + obj.kpi_percent_m2 + obj.kpi_percent_m3) / 3.0, 2),
#                                    obj.kpi_percent_m1,
#                                    obj.kpi_percent_m2,
#                                    obj.kpi_percent_m3,
#                                    # obj.grid_position,
#                                ]
#                         results.append(item)
#                         count += 1
#
#                     file_name = u'[%s] Employee List and Scores' % (unsigned_vi(quarter_period.quarter_name),)
#                     file_name = file_name.replace('\n', ' ').replace('\r', '')
#                     return ReportTemplate(results, company_info, widths, output_name=file_name)
#
#             return render(request, 'report/report_score.html', {
#                 'report': report_cut,
#                 'pagenum': paginator.page_range,
#                 'quarter_period': quarter_period,
#                 'enable_competency': organization.enable_competency,
#                 'quarter_id': quarter_period.id,
#                 'current': current,
#                 'manager_id': manager_id})
#
#     def export_many_quarter(self, request, organization, quarter_periods, manager_id):
#         current_quarter = organization.get_current_quarter()
#         due_date = datetime.datetime.combine(current_quarter.due_date, datetime.time(0, 0))
#         # return HttpResponse('we temporory disable this feature')
#         report_user_ids = []
#         all_user = False
#         if manager_id == organization.ceo_id:
#             all_user = True
#
#         # report_user_ids = self.get_all_subordinate(manager_id, type='user_id', ignore_inactive=True, join_before_date=due_date, all_user=all_user)
#         report_user_ids = self.get_all_subordinate(manager_id, type='user_id', ignore_inactive=True, join_before_date=due_date, include_self=True)
#
#         if quarter_periods.filter(id=current_quarter.id).exists():
#
#             report = ReportScoreFinal.objects.filter(organization=organization.id,
#                                                      quarter_period=current_quarter.id,
#                                                      user_id__in=report_user_ids,
#                                                      is_computed=True).order_by('created_at')
#
#             total_users = len(report_user_ids)
#             if total_users > report.count():
#                 # loading
#                 return render(request, 'report/report_score.html', {
#                     'loading': True,
#                     'calculated_users': report.count(),
#                     'total_users': total_users,
#                     'quarter_id': current_quarter.id,
#                     'multi_quarter': True
#                 })
#
#         data = []
#         list_user_ids = []
#         count_quarter = quarter_periods.count()
#         for quarter_period in quarter_periods:
#             if quarter_period:
#                 due_date = datetime.datetime.combine(quarter_period.due_date, datetime.time(0, 0))
#                 # subordinate_ids = self.get_all_subordinate_ids(manager_id, ignore_inactive=False, join_before_date=due_date)
#                 # report_user_ids = [manager_id, ] + list(subordinate_ids)
#                 list_user_ids.append(set(report_user_ids))
#                 objs = ReportScoreFinal.objects.filter(organization=organization.id,
#                                                        quarter_period=quarter_period.id,
#                                                        user_id__in=report_user_ids,
#                                                        is_computed=True).order_by('created_at').to_json()
#
#                 data_quarter = {}
#                 objs = json.loads(objs)
#                 for obj in objs:
#                     data_quarter[str(obj['user_id'])] = obj
#
#                 data.append(data_quarter)
#
#         user_ids = set().union(*list_user_ids)
#         profiles = Profile.objects.select_related('user').filter(user_id__in=user_ids).order_by('display_name')
#         header = ['STT', 'Employee Name', 'Email', ]
#         quarter_names = []
#         for quarter_period in quarter_periods:
#             # quarter_name = u"%s (%s)" % (quarter_period.quarter_name, format(quarter_period.due_date, 'd-m-Y'))
#             quarter_name = u"{} (Due: {})".format(quarter_period.quarter_name,
#                                                   datetime.date.strftime(quarter_period.due_date, "%d-%b-%y"))
#             quarter_names.append(quarter_name)
#
#         count = 0
#         results = []
#         for p in profiles:
#             count += 1
#             item = [
#                 count,
#                 p.display_name,
#                 p.user.email
#             ]
#             for records_quarter in data:
#                 if str(p.user_id) in records_quarter:
#                     obj = records_quarter[str(p.user_id)]
#                     item.extend([obj['kpi_percent']])  # , obj['potential_score']])
#                 else:
#                     item.extend([None])  # , None])
#
#             results.append(item)
#
#         company_info = {
#             'name': organization.name,
#             'logo': organization.get_logo(),
#             'date': format(datetime.date.today(), 'd-m-Y'),
#         }
#         widths = [5, 25, 30]
#         widths.extend([20, 20] * count_quarter)
#         file_name = u'Employee List and Scores'
#         return ReportMultiQuartersTemplate(results, company_info, widths, file_name, header, quarter_names)

#
# @login_required
# @csrf_exempt
# def report_goal(request):
#     print 'report_goal'
#     profile = request.user.get_profile()
#     organization = profile.get_organization()
#     users = Profile.objects.filter(organization=organization, active=True)
#     quarter = request.session.get('quarter')
#
#     try:
#         quarter_period = QuarterPeriod.objects.get(id=quarter, organization=organization)
#     except:
#
#         raise Http404
#     report = []
#     for user in users:
#         # for each user get his KPIs
#         kpis = KPI.objects.filter(user_id=user.user.id,
#                                   quarter_period=quarter_period).exclude(parent__isnull=True)
#         for kpi in kpis:
#             profile = user
#             factor_name = ''
#             if kpi.parent:
#                 factor_name = kpi.parent.name
#
#             role_type = ""
#             if profile.title:
#                 role_type = profile.title
#             elif profile.current_job_title:
#                 role_type = profile.current_job_title.name
#
#             # manager_name = display_name(profile.report_to_id) if profile.report_to_id else ''
#             manager_name = display_name(profile.parent.user_id)  if profile.parent else ''
#             target = "%s%s" % (kpi.operator or '', kpi.target or '')
#             row = [
#                 profile.display_name,
#              #   profile.get_user_organization().employee_code,
#                 get_employee_code(profile),
#                 manager_name,
#                 factor_name,
#                 kpi.name,
#                 target,
#                 kpi.current_goal,
#                 role_type
#             ]
#             report.append(row)
#     paginator = Paginator(report, 50)
#     page = request.GET.get('page')
#     try:
#         report_cut = paginator.page(page)
#         current = report_cut.number
#     except PageNotAnInteger:
#         report_cut = paginator.page(1)
#         current = 1
#     except EmptyPage:
#         report_cut = paginator.page(paginator.num_pages)
#
#     header = ['Employee Name', 'ID', 'Manager Name', 'Factor Name',
#               'Goal Name', 'Target', 'Measurement', 'Role Type']
#
#     if 'posted' in request.POST:
#         if request.POST['posted'] == 'yes':
#             report.insert(0, header)
#             company_info = {
#                 'name': organization.name,
#                 'logo': organization.get_logo(),
#                 'date': format(datetime.date.today(), 'd-m-Y'),
#                 'quarter_period': quarter_period.quarter_name
#             }
#             widths = [20, 5, 20, 30, 40, 40, 40, 20]
#
#             file_name = u'[%s] Entered Goals by Person' % (unsigned_vi(quarter_period.quarter_name),)
#             file_name = file_name.replace('\n', ' ').replace('\r', '')
#             return ReportTemplate(report, company_info, widths, output_name=file_name)
#
#     return render(request, 'report/report_goal.html', {
#         'report': report_cut,
#         'pagenum': paginator.page_range,
#         'quarter_period': quarter_period,
#         'current': current,
#         'header': header})
#
#
# @login_required
# @csrf_exempt
# def report_dev_plan(request):
#     profile = request.user.get_profile()
#     organization = profile.get_organization()
#     users = Profile.objects.filter(organization=organization, active=True)
#
#     quarter = request.session.get('quarter')
#     try:
#         quarter_period = QuarterPeriod.objects.get(id=quarter, organization=organization)
#     except:
#         raise Http404
#     report = []
#     for user in users:
#         profile_user = user
#         employee_name = profile_user.display_name
#         user_id = get_employee_code(profile)  # profile_user.get_user_organization().employee_code
#         # if not profile_user.report_to:
#         if not profile_user.parent:
#             manager_name = ""
#         else:
#             # manager_name = profile_user.report_to.get_profile().display_name
#             manager_name = profile_user.parent.display_name
#         devplan_tab = 'Development plan'
#
#         short_term_career = profile_user.short_term_career
#         short_term_development = profile_user.short_term_development
#         long_term_career = profile_user.long_term_career
#         long_term_development = profile_user.long_term_development
#         if short_term_career is None:
#             short_term_career = ""
#         if short_term_development is None:
#             short_term_development = ""
#         if long_term_career is None:
#             long_term_career = ""
#         if long_term_development is None:
#             long_term_development = ""
#         if profile_user.current_job_title is None:
#             role_type = ""
#         elif profile_user.current_job_title.name is None:
#             role_type = ""
#         else:
#             role_type = profile_user.current_job_title.name
#
#         row = [employee_name, user_id, manager_name, devplan_tab,
#                short_term_career, short_term_development, long_term_career,
#                long_term_development, role_type]
#
#         report.append(row)
#
#     paginator = Paginator(report, 50)
#     page = request.GET.get('page')
#     try:
#         report_cut = paginator.page(page)
#         current = report_cut.number
#     except PageNotAnInteger:
#         report_cut = paginator.page(1)
#         current = 1
#     except EmptyPage:
#         report_cut = paginator.page(paginator.num_pages)
#
#     if 'posted' in request.POST:
#         if request.POST['posted'] == 'yes':
#             header = ['Employee Name', 'ID', 'Manager Name', 'Devplan Tab',
#                       'Short term career objective', 'Short term development activities',
#                       'Short term development activities', 'Long term career objective', 'Role Type']
#             report.insert(0, header)
#
#             company_info = {
#                 'name': organization.name,
#                 'logo': organization.get_logo(),
#                 'date': format(datetime.date.today(), 'd-m-Y'),
#                 'quarter_period': quarter_period.quarter_name
#             }
#             widths = [20, 5, 20, 10,
#                       30, 30, 30, 30, 20]
#             return ReportTemplate(report, company_info, widths, output_name='Development Plan Contents')
#
#     return render(request, 'report/report_dev_plan.html', {
#         'report': report_cut,
#         'pagenum': paginator.page_range,
#         'quarter_period': quarter_period,
#         'current': current})
#
#
# @login_required
# @csrf_exempt
# def report_self_assessment_review_status(request):
#     profile = request.user.get_profile()
#     organization = profile.get_organization()
#
#     d = request.session.get('date', None)
#     if not d:
#         return HttpResponseJson({'status': 'error'})
#     date = parse(d, dayfirst=True)
#     try:
#         quarter_period = QuarterPeriod.objects.get(due_date=date, organization=organization)
#     except:
#         raise Http404
#
#     # reviews = Review.objects.filter(organization=organization.id, review_type='self',
#     #                                 quarter_period=quarter_period)
#
#     user_list = Profile.objects.filter(organization=organization,
#                                        active=True).values_list('user_id', flat=True)
#     report = ReportSelfAssessmentReviewStatus.objects.filter(organization=organization.id,
#                                                              quarter_period=quarter_period.id,
#                                                              user_id__in=user_list).order_by('created_at')
#
#
#     paginator = Paginator(report, 50)
#     page = request.GET.get('page')
#     try:
#         report_cut = paginator.page(page)
#         current = report_cut.number
#     except PageNotAnInteger:
#         report_cut = paginator.page(1)
#         current = 1
#     except EmptyPage:
#         report_cut = paginator.page(paginator.num_pages)
#
#     if 'posted' in request.POST:
#         if request.POST['posted'] == 'yes':
#             header = ['STT', 'Employee Name', 'ID', 'Manager Name', 'Manager ID',
#                       'Completion %', 'Sharing Status', 'Grid Position']
#             results = []
#             results.insert(0, header)
#
#             count = 1
#             for obj in report:
#                 item = [count,
#                         obj.employee_name,
#                         obj.employee_code,
#                         obj.manager_name,
#                         obj.manager_code,
#                         obj.completion,
#                         obj.share_status,
#                         obj.grid_position]
#                 count += 1
#                 results.append(item)
#
#             company_info = {
#                 'name': organization.name,
#                 'logo': organization.get_logo(),
#                 'date': format(datetime.date.today(), 'd-m-Y'),
#                 'quarter_period': quarter_period.quarter_name
#             }
#             widths = [5, 20, 15, 20, 15,
#                       30, 30, 30]
#
#             return ReportTemplate(results, company_info, widths, output_name="Self Assessment Review Status")
#
#     return render(request, 'report/report_self_assessment_review_status.html', {
#         'report': report_cut,
#         'pagenum': paginator.page_range,
#         'quarter_period': quarter_period,
#         'current': current})
#
#
# @csrf_exempt
# def xls_export(request):
#     report_type = request.POST.get('report_type')
#     quarter_id = request.POST.get('quarter_id')
#
#     if report_type == 'score':
#         return ReportScore.as_view()(request, quarter_id=quarter_id)
#         # return report_score(request,)
#
#     if report_type == 'goal':
#         return report_goal(request)
#
#     if report_type == 'dev_plan':
#         return report_dev_plan(request)
#
#     if report_type == 'self_assessment_review_status':
#         return report_self_assessment_review_status(request)
#
#     return HttpResponseJson({'status': 'error'})


# @login_required
# @csrf_exempt
# def report_exporter(request):
#     quarter = request.POST.get('quarter')
#     request.session['quarter'] = quarter
#     report_type = request.POST.get('report_type', None)
#
#     if not report_type:
#         return HttpResponseJson({'status': 'error'})
#
#     profile = request.user.get_profile()
#     organization = profile.get_organization()
#
#     try:
#         quarter_period = QuarterPeriod.objects.get(id=quarter, organization=organization)
#     except:
#         raise Http404
#
#     if report_type == 'goal':
#         return HttpResponseRedirect(reverse('report_goal'))
#     elif report_type == 'score':
#         quarter = request.POST.getlist('quarter')
#
#         manager_id = request.POST.get('manager_id', None)
#         if not manager_id:
#             manager_id = organization.ceo_id
#         all_user = False
#         if manager_id == organization.ceo_id:
#             all_user = True
#
#
#
#             # note: removed
#
#     elif report_type == 'dev_plan':
#         return HttpResponseRedirect(reverse('report_dev_plan'))
#     elif report_type == 'self_assessment':
#         # SelfAssessmentReviewStatusCalculator(organization=organization, quarter_period=quarter_period).start()
#         return HttpResponseRedirect(reverse('self_assessment_review_status'))
#
#     return HttpResponseJson({'status': 'error'})

#
# class ExportAssignedKPIs(PerformanceBaseView):
#     def post(self, request, *args, **kwargs):
#         organization = self.get_organization(request.user)
#         kpi_id = request.POST.get('kpi_obj_id')
#         kpi = get_object_or_404(KPI, pk=kpi_id)
#
#         company_info = {
#             'name': organization.name,
#             'logo': organization.get_logo(),
#             'date': format(datetime.date.today(), 'd-m-Y'),
#             'full_name': display_name(kpi.user_id),
#             'email': kpi.user.email
#         }
#
#         data = self.get_children_kpi(kpi, 0)
#         headers = ['ID', u'Tên', u'NGƯỜI ĐẢM NHIỆM', 'EMAIL', u'TOÁN TỬ', 'Q.1', 'Q.2', 'Q.3', 'Q.4', u'PP. ĐO LƯỜNG']
#         widths = [10, 60, 25, 25, 10, 10, 10, 10, 10, 30]
#         return KPIExcelTemplate(data, company_info, widths=widths, headers=headers)
#
#     def kpi_info(self, kpi, level):
#         prefix = u'    | ' * level
#
#         if kpi:
#             email = ''
#             if kpi.user:
#                 email = kpi.user.email
#             return [
#                 kpi.get_prefix_category() + kpi.get_kpi_id(),
#                 prefix + kpi.name,
#                 display_name(kpi.user_id),
#                 email,
#                 kpi.operator,
#                 kpi.quarter_one_target,
#                 kpi.quarter_two_target,
#                 kpi.quarter_three_target,
#                 kpi.quarter_four_target,
#                 kpi.current_goal
#             ]
#         else:
#             return []
#
#     def get_children_kpi(self, kpi, level):
#         data = []
#         data.append(self.kpi_info(kpi, level))
#         level += 1
#         for child in kpi.get_children_refer_order():  # kpi.get_children():
#             # data.append(self.kpi_info(child, level))
#             data += self.get_children_kpi(child, level)
#         return data
#         #
#         # def get_cascaded_kpi(self, kpi, level):
#         #
#         #
#         #     kpis = kpi.get_children_refer_order()
#         #     for k in kpis:
#         #         if k.assigned_to :
#         #             k = KPI.objects.filter(cascaded_from = k).first()
#         #
#         #     if not kpis:
#         #         kpis = []
#         #
#         #     return kpis
#
#         # # cascaded_from = kpi._cascaded_from.all().first()
#         #  if cascaded_from:
#         #      return self.get_children_kpi(cascaded_from, level)
#         #  else:
#         #      return []
#

#
# class ActivityReport(PerformanceBaseView):
#     @method_decorator(login_required)
#     @method_decorator(csrf_exempt)
#     def dispatch(self, *args, **kwargs):
#         return super(ActivityReport, self).dispatch(*args, **kwargs)
#
#     def get(self, request, *args, **kwargs):
#         errors = []
#         date_filter = request.GET.get('date', 'today')
#         start_date, end_date = None, None
#
#         today = datetime.date.today()
#         if date_filter == "today":
#             start_date = today
#             end_date = today
#         elif date_filter == 'yesterday':
#             start_date = today + datetime.timedelta(days=-1)
#             end_date = today + datetime.timedelta(days=-1)
#         elif date_filter == 'this_week':
#             start_date = today - datetime.timedelta(days=today.weekday())
#             end_date = start_date + datetime.timedelta(days=6)
#         elif date_filter == 'last_week':
#             start_date = today - datetime.timedelta(days=today.weekday()) - datetime.timedelta(days=7)
#             end_date = start_date + datetime.timedelta(days=6)
#         elif date_filter == 'this_month':
#             days = calendar.monthrange(today.year, today.month)
#             start_date = datetime.date(today.year, today.month, 1)
#             end_date = datetime.date(today.year, today.month, days[1])
#         elif date_filter == 'last_month':
#             last_month = today - relativedelta(months=1)
#             days = calendar.monthrange(last_month.year, last_month.month)
#             start_date = datetime.date(last_month.year, last_month.month, 1)
#             end_date = datetime.date(last_month.year, last_month.month, days[1])
#         else:
#             start_date = today
#             end_date = today
#             date_filter = "today"
#
#         organization = self.get_organization(request.user)
#
#         user_ids = Profile.objects.filter(organization=organization, is_consultant=False) \
#             .values_list('user_id', flat=True)
#
#         user_stats = []  # list(Visitor.objects.user_stats(start_date, end_date, user_ids))
#
#         track_start_time = datetime.now()  # Visitor.objects.order_by('start_time')[0].start_time
#
#         export = request.GET.get('export', None)
#         if export:
#
#             header = ['Employee Name', 'Visits', "Avg. Time on Site", 'Avg. Pages/Visit']
#             results = []
#             results.append(header)
#             for obj in user_stats:
#                 item = []
#                 item.append(obj.profile.display_name)
#                 item.append(obj.visit_count)
#                 item.append(str(obj.time_on_site))
#                 item.append(obj.pages_per_visit)
#
#                 results.append(item)
#             return ExcelResponse(results, 'Visits-Report',)
#
#         context = {
#             'errors': errors,
#             'track_start_time': track_start_time,
#             'visitor_stats': None,  # Visitor.objects.stats(start_date, end_date),
#             'user_stats': user_stats,
#             'tracked_dates': None,  # Visitor.objects.tracked_dates(),
#             'organization': organization,
#             'date_filter': date_filter
#         }
#
#         return render(request, 'report/activity_report.html', context)
#


@login_required
def report_home(request):
    return render(request, 'report/report-home-v5.html',{'user_viewed': request.user})


@login_required
def report_score(request):
    actor = request.user
    # fix case error caused by front-end: user_id=undefined (need validating request input)
    user_id = request.GET.get('user_id', None)
    # if user_id is not None:
    #     if not user_id.isdigit():
    #         return HttpResponseJson404("User id is invalid!")
    #
    target = get_user(user_id=user_id, actor=actor)
    if target is None:
        target = actor.profile.get_organization().ceo

    # for list of employee and score
    # user_viewed = get_current_user_viewed(request, user_id)

    user_viewed = target
    if has_perm(SETTINGS__VIEW_ORG_CHART, actor=actor, target=target):
        return render(request, 'report/report-score-excel.html',{'user_viewed': user_viewed})
    else:
        raise PermissionDenied


@login_required
def report_strategymap(request):
    return render(request, 'report/report-strategymap.html')


@login_required
def report_personal(request):
    return render(request, 'report/report-personal.html')
