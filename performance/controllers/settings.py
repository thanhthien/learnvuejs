#!/usr/bin/python
# -*- coding: utf-8 -*-

import datetime
import json
import time
from dateutil.parser import parse
from django.contrib.auth.decorators import login_required
from django.core.cache import cache
from django.core.mail import EmailMessage
from django.core.urlresolvers import reverse
from django.db import models
from rest_framework import serializers

from performance.cloner import Cloner
from performance.db_models.timeline import TaskEveryStage, Timeline
from rest_framework.parsers import JSONParser

from elms import settings
from django.db.models.expressions import Case, When, Value
from django.http import HttpResponseRedirect
from django.http.response import HttpResponse
from django.shortcuts import render_to_response, render
from django.template import RequestContext
from django.utils import dateformat
from django.utils.dateformat import format
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext
from django.views.decorators.csrf import csrf_exempt

from elms.celery import app
from elms.settings import FROM_EMAIL
from performance.base.views import PerformanceBaseView
# from performance.cloner import PerformanceCloner
from performance.decorators import kpi_admin_permission
from performance.decorators import org_admin_perm
from performance.forms import DueDateForm, EditDueDateForm
from performance.models import QuarterPeriod, REVIEW_TYPE
from performance.services.EmailTemplate import add_new_email_template, get_all_template, delete_email_template, \
    edit_email_template, get_template_by_id
# from performance.services.Review import get_manager_review_progress
from performance.services.schedule import update_basic_settings
from performance.services.timeline import get_timeline, get_TaskEveryStage
from performance.tasks import start_cloner_in_celery
from user_profile.services.profile import send_mail_with_perm
from user_profile.services.user import get_user
from user_profile.templatetags.user_tags import display_name
from utils import ExcelTemplateResponse, postpone
from utils import unsigned_vi, ReportTemplate
from utils.api.response import APIJsonResponseOK, APIJsonResponseBadRequest, APIJsonResponseNotFound
from utils.common import HttpResponseJson  # , CustomJsonEncoder
import logging
logger = logging.getLogger()
# from performance.decorators import consultant_permission
# from performance.decorators import consultant_permission
class PerformanceAdminBaseView(PerformanceBaseView):

    @method_decorator(kpi_admin_permission)
    # @method_decorator(consultant_permission)
    def dispatch(self, *args, **kwargs):
        return super(PerformanceAdminBaseView, self).dispatch(*args, **kwargs)


class SettingSerialize(serializers.Serializer):
    self_review_date = serializers.DateField(input_formats="%d/%m/%Y")
    edit_to_date = serializers.DateField(input_formats="%d/%m/%Y")
    max_score = serializers.IntegerField()
    allow_delay_kpi = serializers.BooleanField()
    allow_edit_monthly_target = serializers.BooleanField()
    enable_weight_edit = serializers.BooleanField()
    enable_email_nottification = serializers.BooleanField()
    enable_abc_feature = serializers.BooleanField()
    enable_require_target = serializers.BooleanField()


class BasicSettings(PerformanceAdminBaseView):

    def get(self, request, *args, **kwargs):
        organization = self.get_organization(request.user)
        if not organization.enable_competency and organization.review_360:
            organization.review_360 = False
            organization.save()

        variables = {
            'organization': organization,
            'user_viewed': request.user
        }

        return render(request, 'performance/basic.html', variables)

    def post(self, request, *args, **kwargs):
        organization = self.get_organization(request.user)
        '''
        Todo:
        1. Tao serialzier verify input
        2. tao service xu ly business logic
        '''
        data = {
            'self_review_date': request.POST.get('self_review_date', None),
            'edit_to_date': request.POST.get('edit_to_date', None),
            'max_score': request.POST.get('max_score', None),
            'allow_delay_kpi': request.POST.get('allow_delay_kpi', False),
            'allow_edit_monthly_target': request.POST.get('allow_edit_monthly_target', False),
            'enable_weight_edit': request.POST.get('enable_weight_edit', False),
            'enable_email_notification': request.POST.get('enable_email_notification', False),
            'exscore': request.POST.get('exscore', False),
            'name': request.POST.get('name', None),
            'module': request.POST.get('module', None),
            'default_auto_score_parent': request.POST.get('default_auto_score_parent',None),
            'enable_abc_feature': request.POST.get('abc_feature', False),
            'enable_require_target': request.POST.get('enable_require_target', False)
        }
        serializer = SettingSerialize(data=data)
        try:
            serializer.is_valid(raise_exception=True)
        except Exception as e:
            APIJsonResponseBadRequest(message=e.__str__())
        #
        # if 'enable_edit' in request.POST:
        #     enable_edit = request.POST['enable_edit']
        #     enable_to_date = request.POST['date']
        #     try:
        #         enable_to_date = parse(enable_to_date, dayfirst=True)
        #     except:
        #         enable_to_date = datetime.datetime.now() + datetime.timedelta(days=30)
        #
        #     organization.enable_edit = True if enable_edit == 'true' else False
        #     organization.edit_to_date = enable_to_date.date()
        #     organization.save()
        #     cache.delete('org_user' + str(request.user.id))  # TODO: dat ten cache tum lum o day tao ra bug --> delete
        #     # cache.set('org_user' + str(request.user.id), organization, 60 * 2)
        #     #return HttpResponse('ok')
        #
        # elif 'enable_competency' in request.POST:
        #     enable_competency = request.POST['enable_competency']
        #     organization.enable_competency = True if enable_competency == 'true' else False
        #     if not organization.enable_competency:
        #         organization.review_360 = False
        #
        #     organization.save()
        #     #return HttpResponse('ok')
        #
        # elif 'company_name' in request.POST:
        #     organization.name = request.POST['company_name']
        #     organization.save()
        #     #return HttpResponse('ok')
        #
        # elif 'workflow' in request.POST:
        #     if request.POST.get('workflow') == 'self-assessment':
        #         organization.self_assessment = True if request.POST.get('value') == '1' else False
        #     elif request.POST.get('workflow') == '360-preview':
        #         organization.review_360 = True if request.POST.get('value') == '1' else False
        #         if not organization.enable_competency and organization.review_360:
        #             organization.review_360 = False
        #
        #     organization.save()
        #     #return HttpResponse('ok')
        #
        # elif 'enable_drag_review' in request.POST:
        #     organization.enable_drag_review = True if request.POST['enable_drag_review'] == 'true' else False
        #     organization.save()
        #
        update_basic_settings(request=request, data=data)
        return HttpResponseRedirect(reverse('basic_view'))


class AutoMail(PerformanceAdminBaseView):

    def get(self, request, *args, **kwargs):
        organization = self.get_organization(request.user)
        create_template = request.GET.get('create_template', None) == 'true'
        template_email = get_all_template(organization)
        variables = {
            'organization': organization,
            'create_template': create_template,
            'template_email': template_email,
            'user_viewed': request.user
        }
        get_all = request.GET.get('get_all', None) == 'true'
        if get_all:
            return APIJsonResponseOK(template_email)
        return render(request, 'performance/auto-mail.html', variables)

    def put(self, request, *args, **kwargs):
        organization = self.get_organization(request.user)
        data = JSONParser().parse(request)
        title = data.get('title', None)
        content = data.get('content', None)
        if title and content:
            create_template = add_new_email_template(organization, title, content)
            if create_template:
                return APIJsonResponseOK(create_template)

        return APIJsonResponseBadRequest()

    def delete(self, request, *args, **kwargs):
        template_id = request.GET.get('id', None)
        if template_id:
            template_id = int(template_id)
            delete_template = delete_email_template(template_id)
            if delete_template:
                return APIJsonResponseOK(message=ugettext('Delete template success!'))
            return APIJsonResponseNotFound(message=ugettext('Id not exist!'))

    def post(self, request, *args, **kwargs):
        template_id = request.GET.get('id', None)
        if template_id:
            template_id = int(template_id)
            data = JSONParser().parse(request)
            title = data.get('title', None)
            content = data.get('content', None)
            edit_template = edit_email_template(title, content, template_id)
            if edit_template:
                return APIJsonResponseOK(edit_template, message=u'Edit template success!')
            return APIJsonResponseNotFound(message=u'Id not exist!')
        set_remind_evaluation = request.GET.get('set_remind_evaluation', None) == 'true'
        if set_remind_evaluation:
            test = request.GET.get('test', None) == 'true'
            if test:
                user_email = request.user.email
                data = JSONParser().parse(request)
                title = data.get('title', None)
                id = data.get('id', None)
                template = get_template_by_id(id)
                send_mail = send_email_remind_evaluation_no_thread(title=title, content=template.content,
                                                                   list_email=[user_email])
                if send_mail:
                    return APIJsonResponseOK(message=ugettext('Send mail test success, Please check mail!'))
                return APIJsonResponseNotFound(message=ugettext('Can not send mail test please recheck!'))
            else:
                send_mail = send_email_remind_evaluation(request=request)
                if send_mail == True:
                    return APIJsonResponseOK(message=ugettext('In the process of sending mail'))
                return APIJsonResponseNotFound(message=send_mail)

#
# class TimelineView(PerformanceBaseView):
#
#     def get(self, request, *args, **kwargs):
#         organization = self.get_organization(request.user)
#
#         hash = organization.get_hash
#
#         if organization.show_timeline:
#             timeline = get_timeline(organization)  # Timeline.objects.filter(organization=organization).order_by('id')
#             tasks = get_TaskEveryStage(
#                 organization)  # TaskEveryStage.objects.filter(organization=organization).order_by('stage_id', 'due_date',
#             #                                                                  'due_time_start', 'end_date')
#         else:
#             timeline = []
#             tasks = []
#
#         return render(request, 'performance/timeline/timeline_public.html', {
#             'timeline': timeline,
#             'hash': hash,
#             'tasks': tasks,
#             'organization': organization
#         })
#
#     def post(self, request, *args, **kwargs):
#         print 'export csv'
#         if 'export_csv' in request.POST:
#             return self.export_timeline_to_excel(request)
#         pass
#
#     def export_timeline_to_excel(self, request):
#         header = ['', u'Bước', u'Tên bước', u'Bắt đầu', u'Kết thúc', u'Công việc', u'Ghi chú', u'Hoàn thành (%)',
#                   u'MEETING MINUTES', u'NEXT ACTIONS']
#
#         org_hash = request.POST.get('org_hash', '')
#         if org_hash:
#             from company.models import get_organization_via_hash
#             organization = get_organization_via_hash(org_hash)
#         else:
#             organization = self.get_organization(request.user)
#
#         if organization.show_timeline:
#             timeline = Timeline.objects.filter(organization=organization).order_by('id')
#         else:
#             timeline = []
#             tasks = []
#
#         data = []
#         for tl in timeline:
#             tasks = TaskEveryStage.objects.filter(organization=organization, stage_id=tl.id).order_by('stage_id',
#                                                                                                       'due_date',
#                                                                                                       'due_time_start',
#                                                                                                       'end_date')
#             first_step = True
#             step = 0
#             for t in tasks:
#                 if step == 0:
#                     stage = tl.stage
#                 else:
#                     stage = ''
#                 step += 1
#                 jobs = t.get_kpis().values_list('name', flat=True)
#                 things = "\r\n".join(jobs)
#
#                 due_date = dateformat.format(t.due_date, 'd-m-Y') if t.due_date else ''
#                 end_date = dateformat.format(t.end_date, 'd-m-Y') if t.end_date else ''
#
#                 # print 'due_date: {}'.format(due_date)
#                 data.append([stage, str(step), t.name, due_date, end_date, things, t.note,
#                              unicode(t.finished_percentage()) + '%', t.meeting_minutes, t.next_steps])
#             data.append([''] * len(header))
#
#         # from utils.common import string_to_asscii
#         from utils import unsigned_vi
#         filename = u'Timeline-' + unsigned_vi(organization.name)
#         title = u"Timelime %s" % (organization.name)
#         return ExcelTemplateResponse(title, organization, data, widths=[10, 10, 20, 10, 10,
#                                                                         20, 20, 20, 20, 20], output_name=filename,
#                                      headers=header)
#     #
#     # def csv_export_timeline(self, request):
#     #     # ref: http://stackoverflow.com/questions/17311366/how-can-i-add-encoding-to-the-python-generated-csv-file
#     #     # https://docs.djangoproject.com/ja/1.9/howto/outputting-csv/#using-the-python-csv-library
#     #    # from utils.csvutils import UnicodeWriter
#     #     from django.http import HttpResponse
#     #
#     #     organization = self.get_organization(request.user)
#     #
#     #     hash = organization.get_hash
#     #
#     #     if organization.show_timeline:
#     #         timeline = Timeline.objects.filter(organization=organization).order_by('id')
#     #         # tasks = TaskEveryStage.objects.filter(organization=organization).order_by('stage_id', 'due_date',
#     #         #                                                                           'due_time_start', 'end_date')
#     #     else:
#     #         timeline = []
#     #         tasks = []
#     #
#     #     # Create the HttpResponse object with the appropriate CSV header.
#     #     # filename=u'Timeline-' + organization.name
#     #     # FILE NAME DISPOSITION ERROR:
#     #     # REF: http://stackoverflow.com/questions/1207457/convert-a-unicode-string-to-a-string-in-python-containing-extra-symbols
#     #     # http://stackoverflow.com/questions/93551/how-to-encode-the-filename-parameter-of-content-disposition-header-in-http
#     #
#     #     # filename=u'CÔNG TY TNHH TTCL - VIỆT NAM '
#     #
#     #     # from utils.no_accent_vietnamese_unicodedata import no_accent_vietnamese
#     #     from utils import unsigned_vi
#     #     filename = u'Timeline-' + unsigned_vi(organization.name)
#     #
#     #     content_disp = u'attachment; filename="{}.csv"'.format(filename,)
#     #     # print content_disp
#     #     response = HttpResponse(content_type='text/csv')
#     #     response['Content-Disposition'] = content_disp
#     #
#     #     # writer = csv.writer(response)
#     #     writer = UnicodeWriter(response)
#     #
#     #     # for tl in timeline:
#     #     #     writer.writerow([unicode(tl.stage), unicode(tl.content)])
#     #
#     #     writer.writerow(['', u'TIMELINE-' + organization.name])
#     #     writer.writerow([])
#     #     writer.writerow([])
#     #
#     #     writer.writerow(['', u'Bước', u'Tên bước', u'Bắt đầu', u'Kết thúc', u'Công việc', u'Ghi chú', u'Hoàn thành (%)',
#     #                      u'MEETING MINUTES', u'NEXT ACTIONS'])
#     #     writer.writerow([])
#     #     for tl in timeline:
#     #         tasks = TaskEveryStage.objects.filter(organization=organization, stage_id=tl.id).order_by('stage_id',
#     #                                                                                                   'due_date',
#     #                                                                                                   'due_time_start',
#     #                                                                                                   'end_date')
#     #         first_step = True
#     #         step = 0
#     #         for t in tasks:
#     #             writer.writerow([])
#     #             if step == 0:
#     #                 stage = tl.stage
#     #             else:
#     #                 stage = ''
#     #             step += 1
#     #             jobs = t.get_kpis().values_list('name', flat=True)
#     #             things = "\r\n".join(jobs)
#     #
#     #             due_date = dateformat.format(t.due_date, 'd-m-Y') if t.due_date else ''
#     #             end_date = dateformat.format(t.end_date, 'd-m-Y') if t.end_date else ''
#     #
#     #             # print 'due_date: {}'.format(due_date)
#     #             writer.writerow([stage, str(step), t.name, due_date, end_date, things, t.note,
#     #                              unicode(t.finished_percentage()) + '%', t.meeting_minutes, t.next_steps])
#     #
#     #             pass
#     #     # for t in tasks:
#     #     #     writer.writerow([t.stage.stage, t.name])
#     #     #     writer.writerow(['------', '-----------'])
#     #     #     for things in t.get_kpis():
#     #     #         writer.writerow(['', things.name,])
#     #
#     #     # writer.writerow(['First row', 'Foo', 'Bar', 'Baz'])
#     #     # writer.writerow(['Second row', 'A', 'B', 'C', '"Testing"', "Here's a quote"])
#     #
#     #     return response
#     #
#
# @login_required
# @org_admin_perm
# @csrf_exempt
# def schedule_view(request):
#     organization = request.user.get_profile().get_organization()
#     quarter_period = organization.get_current_quarter()
#     eform = EditDueDateForm(initial={'organization': organization.id, 'quarter_id': quarter_period.id})
#     if 'quarter' in request.GET:
#         quarter_period = QuarterPeriod.objects.get(id=request.GET['quarter'], organization=organization)
#     print 'here'
#     if True:  # request.method == 'GET':
#
#         CURRENT_QUARTER_CHOICES = (
#             ('1', 'Quarter 1 - {0}'.format(quarter_period.quarter_1_year)),
#             ('2', 'Quarter 2 - {0}'.format(quarter_period.quarter_2_year)),
#             ('3', 'Quarter 3 - {0}'.format(quarter_period.quarter_3_year)),
#             ('4', 'Quarter 4 - {0}'.format(quarter_period.quarter_4_year)),
#         )
#
#         current_quarter = quarter_period.quarter
#         year = datetime.datetime.now().year
#         quarter = quarter_period.quarter
#
#         if quarter_period.quarter == 4:
#             year = year + 1
#             quarter = 1
#         else:
#             quarter += 1
#         current_quarter = quarter
#
#         if quarter_period.quarter == 4:
#             if isinstance(quarter_period.quarter_4_year, int) \
#                     and quarter_period.quarter_1_year == (quarter_period.quarter_4_year + 1):
#                 current_quarter = 1
#
#         form = DueDateForm(
#             initial={'organization': organization.id,
#                      'current_quarter': current_quarter,
#                      'year': year,
#                      'quarter': quarter
#                      })
#         # form.set_CURRENT_QUARTER_CHOICES(CURRENT_QUARTER_CHOICES)
#
#     if request.method == "POST":
#         if 'due_date' in request.POST:
#
#             print 'due_date'
#             print request.POST
#
#             from utils.common import is_same_thead_host, post_to_thread_server
#             # from django.core.cache import cache
#             if not is_same_thead_host(request):
#                 post_to_thread_server(request, reverse('schedule', kwargs={}))
#
#             else:
#                 print 'server_run'
#                 print request.META['HTTP_HOST']
#                 form = DueDateForm(request.POST)
#                 if form.is_valid():
#                     due_date = form.cleaned_data['due_date']
#                     year = form.cleaned_data['year']
#                     quarter = form.cleaned_data['quarter']
#                     # current_quarter = form.cleaned_data['current_quarter']
#                     quarter_name = u"Review due: %s" % (format(due_date, 'M Y'))
#                     q = QuarterPeriod.objects.create(organization=organization,
#                                                      due_date=due_date,
#                                                      year=year,
#                                                      quarter=quarter,
#                                                      #   clone_from_quarter=current_quarter,
#                                                      clone_from_quarter_period=quarter_period,
#                                                      quarter_name=quarter_name,
#                                                      quarter_1_year=year,  # quarter_period.quarter_1_year,
#                                                      quarter_2_year=year,  # quarter_period.quarter_2_year,
#                                                      quarter_3_year=year,  # quarter_period.quarter_3_year,
#                                                      quarter_4_year=year,  # quarter_period.quarter_4_year
#                                                      )
#                     q.update_quarter()
#
#                     q.status = 'IN'
#                     q.save()
#
#                     quarter_period.status = 'AR'  # QuarterPeriod.STATUS_CHOICE.
#                     quarter_period.save()
#
#                     cloner = PerformanceCloner(organization, q)
#                     cloner.start()
#
#                     organization.delete_current_quarter_cache()
#             # from utils.common import get_redis
#             # from utils.caching_name import CREATING_ASSESSMENT
#             # print 'Redis value:', get_redis(CREATING_ASSESSMENT)
#             return HttpResponseRedirect(reverse('schedule'))
#
#         elif 'edit_due_date' in request.POST:
#             eform = EditDueDateForm(request.POST)
#             if eform.is_valid():
#                 due_date = eform.cleaned_data['edit_due_date']
#                 old_due = parse(request.POST['old_due'], dayfirst=True)
#                 try:
#                     q = QuarterPeriod.objects.get(id=eform.cleaned_data['quarter_id'], organization=organization)
#                     q.due_date = due_date
#                     q.save()
#                 except:
#                     pass
#         elif 'archive_review' in request.POST:
#             quarter_period.status = "AR"
#             quarter_period.save()
#             current_due = organization.get_current_quarter()
#             return HttpResponseRedirect(reverse('schedule'))
#
#         elif 'quarter' in request.POST:
#             try:
#                 percent = int(request.POST['percent'])
#             except:
#                 percent = 0
#
#             q = QuarterPeriod.objects.filter(id=request.POST['quarter'],
#                                              organization=organization,
#                                              status='IN').first()
#
#             if q:
#                 q.completion = percent
#                 q.save()
#                 return HttpResponse('ok')
#             else:
#                 return HttpResponseJson({'status': 'error'})
#         elif 'export_data' in request.POST:
#             quarter_period = q = QuarterPeriod.objects.get(id=request.POST['active_quarter'],
#                                                            organization=organization)
#             export_data = request.POST['export_data']
#             kwarg = {}
#             if export_data == "not_start":
#                 kwarg['completion'] = 0
#             elif export_data == "in_progress":
#                 kwarg['completion__gt'] = 0
#                 kwarg['completion__lt'] = 100
#             elif export_data == "complete":
#                 kwarg['completion'] = 100
#
#             if request.POST.get('employee') == 'false' and request.POST.get('manager') == 'true':
#                 kwarg['review_type'] = 'manager'
#             elif request.POST.get('employee') == 'true' and request.POST.get('manager') == 'false':
#                 kwarg['review_type'] = 'self'
#
#             # reviews = get_manager_review_progress(organization=organization,
#             #                                       quarter_period=quarter_period)
#             #
#             # uo = UserOrganization.objects.filter(organization=organization,
#             #                                      active=True).values_list('user', flat=True)
#             # reviews = Review.objects.filter(organization=organization, reviewee__isnull=False,
#             #                                 quarter_period=quarter_period,
#             #                                 reviewee_id__in=uo, **kwarg).values_list('reviewee__profile__display_name',
#             #                                                                          'reviewee__userorganization__employee_code',
#             #                                                                          'reviewee__email',
#             #                                                                          'reviewer__profile__display_name',
#             #                                                                          'review_type',
#             #                                                                          'completion').order_by(
#             #     'review_type',
#             #     'reviewer',
#             #     'reviewee__profile__display_name')
#             company_info = {
#                 'name': organization.name,
#                 'logo': organization.get_logo(),
#                 'date': format(datetime.date.today(), 'd-m-Y'),
#                 'quarter_period': quarter_period.quarter_name
#             }
#             widths = [5, 25, 20, 30, 30, 20, 20]
#             data = []
#             count = 1
#             # for obj in reviews:
#             #     r = obj.to_json_export()  # list(obj)
#             #     # if r[4] == 'self':
#             #     #     r[4] = ugettext('Employee self assessment')
#             #     # elif r[4] == 'manager':
#             #     #     r[4] = ugettext('Manager team review')
#             #
#             #     # r.insert(0, count)
#             #
#             #     data.append(r)
#             #     count += 1
#
#             file_name = u'[%s] Review Progress' % (unsigned_vi(quarter_period.quarter_name),)
#             file_name = file_name.replace('\n', ' ').replace('\r', '')
#             headers = [ugettext('ID'), ugettext('Employee'),
#                        #   ugettext('Employee code'),
#                        'Email',
#                        #  ugettext('Reviewer'), ugettext('Review task'),
#                        ugettext('Complete')]
#             return ReportTemplate(data, company_info, widths, output_name=file_name, headers=headers)
#
#     current_due = organization.get_current_quarter()
#     archived_due = organization.get_archived_quarter().order_by('?').order_by('-due_date')
#     pending_due = organization.get_pending_quarter()
#
#     if 'json_data' in request.GET:
#         # data = {
#         #     'current_due': {'date': format(current_due.due_date, 'd-m-Y'), "id": current_due.id},
#         #     'archived_due': [{'date': format(due.due_date, 'd-m-Y'), 'id': due.id} for due in archived_due],
#         #     'pending_due': [{'date': format(due.due_date, 'd-m-Y'), 'id': due.id} for due in pending_due],
#         #     'active_quarter': {'date': format(quarter_period.due_date, 'd-m-Y'), 'id': quarter_period.id}
#         # }
#         #
#         data = {
#             'current_due': current_due.to_json(),
#             'archived_due': [due.to_json() for due in archived_due],
#             'pending_due': [due.to_json() for due in pending_due],
#             'active_quarter': quarter_period.to_json()
#         }
#
#         #         uo = UserOrganization.objects.filter(organization=organization,
#         #                                              active=True).values_list('user', flat=True)
#
#         # user_id_list = []
#         # user_id_list1 = UserOrganization.objects.filter(organization=organization, active=True).values('user_id')
#         # for user_item in user_id_list1:
#         #     user_id_list.append(user_item['user_id'])
#
#         # reviews = Review.objects.filter(organization=organization, reviewee__isnull=False,
#         #                                 quarter_period=quarter_period,
#         #                                 reviewee_id__in=user_id_list)
#         # from performance.services.Review import get_manager_review_progress
#         # reviews = get_manager_review_progress(organization=organization,
#         #                                       quarter_period=quarter_period)
#
#         review_dict = dict(REVIEW_TYPE)
#         for key in review_dict:
#             review_dict[key] = unicode(review_dict[key])
#
#         # data_list = reviews.annotate(name=F("reviewee__profile__display_name"),
#         #                              reviewer_name=F("reviewer__profile__display_name"),
#         #                              complete=F('completion'),
#         #                              task=Case(When(review_type='self', then=Value(review_dict['self'])),
#         #                                        When(review_type='manager', then=Value(review_dict['manager'])),
#         #                                        When(review_type='direct_report',
#         #                                             then=Value(review_dict['direct_report'])),
#         #                                        When(review_type='senior_manager',
#         #                                             then=Value(review_dict['senior_manager'])),
#         #                                        When(review_type='peer', then=Value(review_dict['peer'])),
#         #                                        default=Value(''), output_field=models.CharField()),
#         #                              r360=Case(When(review_type='self', then=Value('0')),
#         #                                        When(review_type='manager', then=Value('0')),
#         #                                        default=Value('1'), output_field=models.CharField())) \
#         #     .values('name', 'reviewer_name', 'task', 'r360', 'complete')
#         #         data_list = []
#         #         for review in reviews:
#         #             item = review.to_json()
#         #             item = {
#         #                 'name': display_name(review.reviewee_id),
#         #                 'reviewer': display_name(review.reviewer_id),
#         #                 'task': review.get_review_type_display(),
#         #                 'type': review.review_type,
#         #                 'complete': review.completion,
#         #                 'sharing': review.get_share_status(),
#         #                 'r360': '1' if review.review_type not in ['self', 'manager'] else '0'
#         #             }
#         #             data_list.append(item)
#
#         return HttpResponse(json.dumps([data, {}], indent=4, cls=CustomJsonEncoder),
#                             content_type="application/json")
#
#     variables = RequestContext(request, {
#         'organization': organization,
#         'current_due': current_due,
#         'archived_due': archived_due,
#         'pending_due': pending_due,
#         'form': form,
#         'eform': eform
#     })
#
#     return render_to_response('performance/schedule.html', variables)


@login_required
@org_admin_perm
@csrf_exempt
def schedule_view_v2(request):
    organization = request.user.get_profile().get_organization()
    quarter_period = organization.get_current_quarter()
    eform = EditDueDateForm(initial={'organization': organization.id, 'quarter_id': quarter_period.id})
    if 'quarter' in request.GET:
        quarter_period = QuarterPeriod.objects.get(id=request.GET['quarter'], organization=organization)
    # print 'here'
    logger.info('update scheduling')
    logger.info('request.POST: {}'.format(request.POST))
    if True:  # request.method == 'GET':

        CURRENT_QUARTER_CHOICES = (
            ('1', 'Quarter 1 - {0}'.format(quarter_period.quarter_1_year)),
            ('2', 'Quarter 2 - {0}'.format(quarter_period.quarter_2_year)),
            ('3', 'Quarter 3 - {0}'.format(quarter_period.quarter_3_year)),
            ('4', 'Quarter 4 - {0}'.format(quarter_period.quarter_4_year)),
        )

        current_quarter = quarter_period.quarter
        year = datetime.datetime.now().year
        quarter = quarter_period.quarter

        if quarter_period.quarter == 4:
            year = year + 1
            quarter = 1
        else:
            quarter += 1
        current_quarter = quarter

        if quarter_period.quarter == 4:
            if isinstance(quarter_period.quarter_4_year, int) \
                    and quarter_period.quarter_1_year == (quarter_period.quarter_4_year + 1):
                current_quarter = 1

        form = DueDateForm(
            initial={'organization': organization.id,
                     'current_quarter': current_quarter,
                     'year': year,
                     'quarter': quarter,
                     })
        # form.set_CURRENT_QUARTER_CHOICES(CURRENT_QUARTER_CHOICES)

    if request.method == "POST":
        logger.info('request.POST: {}'.format(request.POST))
        if 'due_date' in request.POST:

            # print 'due_date'
            # print request.POST

            # from utils.common import is_same_thead_host, post_to_thread_server
            # from django.core.cache import cache
            if False:  # not is_same_thead_host(request):
                pass
            # post_to_thread_server(request, reverse('schedule', kwargs={}))

            else:
                # print 'server_run'
                # print request.META['HTTP_HOST']
                form = DueDateForm(request.POST)
                logger.info('form.is_valid(): {}'.format(form.is_valid()))
                # return HttpResponseRedirect(reverse('schedule'))

                if form.is_valid():
                    due_date = form.cleaned_data['due_date']
                    year = form.cleaned_data['year']
                    quarter = form.cleaned_data['quarter']
                    quarter_name = form.cleaned_data['quarter_name']
                    # current_quarter = form.cleaned_data['current_quarter']
                    # quarter_name = u"Review due: %s" % (format(due_date, 'M Y'))
                    q = QuarterPeriod.objects.create(organization=organization,
                                                     due_date=due_date,
                                                     year=year,
                                                     quarter=quarter,
                                                     #   clone_from_quarter=current_quarter,
                                                     clone_from_quarter_period=quarter_period,
                                                     quarter_name=quarter_name,
                                                     quarter_1_year=year,  # quarter_period.quarter_1_year,
                                                     quarter_2_year=year,  # quarter_period.quarter_2_year,
                                                     quarter_3_year=year,  # quarter_period.quarter_3_year,
                                                     quarter_4_year=year,  # quarter_period.quarter_4_year
                                                     )
                    q.update_quarter()

                    q.status = 'IN'
                    q.save()



                    quarter_period.status = 'AR'  # QuarterPeriod.STATUS_CHOICE.
                    quarter_period.save()


                    # organization.last_copy_time = datetime.datetime.today()
                    # organization.self_review_date = due_date
                    # organization.save()



                    # we remove the following and switch to using threading_or_worker decorator
                    # if settings.ENABLE_CELERY:
                    #     logger.info('Start create new assessment by Celery')
                    #     start_cloner_in_celery.apply_async(args=[organization, q])
                    # else:
                    #     logger.info('Start create new assessment by Threading')
                    #     cloner = PerformanceCloner(organization, q)
                    #     cloner.start()

                    # Because Cloner take time to start, we should mark system as in creating new assessment state right away
                    Cloner.mark_system_as_in_cloning_process(organization)
                    start_cloner_in_celery(organization, q)

                    organization.delete_current_quarter_cache()
                else:
                    print "Error: settings.py:787"
                    print form.errors
            # from utils.common import get_redis
            # from utils.caching_name import CREATING_ASSESSMENT
            # print 'Redis value:', get_redis(CREATING_ASSESSMENT)
            return HttpResponseRedirect(reverse('schedule'))

        elif 'edit_due_date' in request.POST:
            eform = EditDueDateForm(request.POST)
            if eform.is_valid():
                due_date = eform.cleaned_data['edit_due_date']
                old_due = parse(request.POST['old_due'], dayfirst=True)
                try:
                    q = QuarterPeriod.objects.get(id=eform.cleaned_data['quarter_id'], organization=organization)
                    q.due_date = due_date
                    q.save()
                except:
                    pass
        elif 'archive_review' in request.POST:
            quarter_period.status = "AR"
            quarter_period.save()
            current_due = organization.get_current_quarter()
            return HttpResponseRedirect(reverse('schedule'))

        elif 'quarter' in request.POST:
            try:
                percent = int(request.POST['percent'])
            except:
                percent = 0

            q = QuarterPeriod.objects.filter(id=request.POST['quarter'],
                                             organization=organization,
                                             status='IN').first()

            if q:
                q.completion = percent
                q.save()
                return HttpResponse('ok')
            else:
                return HttpResponseJson({'status': 'error'})
                # elif 'export_data' in request.POST:
                #     quarter_period = q = QuarterPeriod.objects.get(id=request.POST['active_quarter'],
                #                                                    organization=organization)
                #     export_data = request.POST['export_data']
                #     kwarg = {}
                #     if export_data == "not_start":
                #         kwarg['completion'] = 0
                #     elif export_data == "in_progress":
                #         kwarg['completion__gt'] = 0
                #         kwarg['completion__lt'] = 100
                #     elif export_data == "complete":
                #         kwarg['completion'] = 100
                #
                #     if request.POST.get('employee') == 'false' and request.POST.get('manager') == 'true':
                #         kwarg['review_type'] = 'manager'
                #     elif request.POST.get('employee') == 'true' and request.POST.get('manager') == 'false':
                #         kwarg['review_type'] = 'self'
                #
                #     reviews = get_manager_review_progress(organization=organization,
                #                                           quarter_period=quarter_period)
                #
                #     company_info = {
                #         'name': organization.name,
                #         'logo': organization.get_logo(),
                #         'date': format(datetime.date.today(), 'd-m-Y'),
                #         'quarter_period': quarter_period.quarter_name
                #     }
                #     widths = [5, 25, 20, 30, 30, 20, 20]
                #     data = []
                #     count = 1
                #     for obj in reviews:
                #         r = obj.to_json_export()  # list(obj)
                #         # if r[4] == 'self':
                #         #     r[4] = ugettext('Employee self assessment')
                #         # elif r[4] == 'manager':
                #         #     r[4] = ugettext('Manager team review')
                #
                #         # r.insert(0, count)
                #
                #         data.append(r)
                #         count += 1
                #
                #     file_name = u'[%s] Review Progress' % (unsigned_vi(quarter_period.quarter_name),)
                #     file_name = file_name.replace('\n', ' ').replace('\r', '')
                #     headers = [ugettext('ID'), ugettext('Employee'),
                #                #   ugettext('Employee code'),
                #                'Email',
                #                #  ugettext('Reviewer'), ugettext('Review task'),
                #                ugettext('Complete')]
                #     return ReportTemplate(data, company_info, widths, output_name=file_name, headers=headers)

    current_due = organization.get_current_quarter()
    archived_due = organization.get_archived_quarter().order_by('-due_date')
    deleted_quarters = organization.get_deleted_quarter()
    pending_due = organization.get_pending_quarter()
    #
    # if 'json_data' in request.GET:
    #
    #     data = {
    #         'current_due': current_due.to_json(),
    #         'archived_due': [due.to_json() for due in archived_due],
    #         'pending_due': [due.to_json() for due in pending_due],
    #         'active_quarter': quarter_period.to_json()
    #     }
    #
    #
    #     reviews = get_manager_review_progress(organization=organization,
    #                                           quarter_period=quarter_period)
    #
    #     review_dict = dict(REVIEW_TYPE)
    #     for key in review_dict:
    #         review_dict[key] = unicode(review_dict[key])
    #
    #     data_list = reviews.annotate(name=F("reviewee__profile__display_name"),
    #                                  reviewer_name=F("reviewer__profile__display_name"),
    #                                  complete=F('completion'),
    #                                  task=Case(When(review_type='self', then=Value(review_dict['self'])),
    #                                            When(review_type='manager', then=Value(review_dict['manager'])),
    #                                            When(review_type='direct_report', then=Value(review_dict['direct_report'])),
    #                                            When(review_type='senior_manager', then=Value(review_dict['senior_manager'])),
    #                                            When(review_type='peer', then=Value(review_dict['peer'])),
    #                                            default=Value(''), output_field=models.CharField()),
    #                                  r360=Case(When(review_type='self', then=Value('0')),
    #                                            When(review_type='manager', then=Value('0')),
    #                                            default=Value('1'), output_field=models.CharField())) \
    #                        .values('name', 'reviewer_name', 'task', 'r360', 'complete')
    #
    #
    #     return HttpResponse(json.dumps([data, data_list], indent=4, cls=CustomJsonEncoder),
    #                         content_type="application/json")

    variables = RequestContext(request, {
        'organization': organization,
        'current_due': current_due,
        'archived_due': archived_due,
        'pending_due': pending_due,
        'deleted_quarters': deleted_quarters,
        'form': form,
        'eform': eform,
        'user_viewed': request.user
    })

    return render_to_response('performance/schedule_v2.html', variables)


@login_required
@org_admin_perm
@csrf_exempt
def schedule_delete(request):
    if request.method == "POST" and request.is_ajax():
        organization = request.user.get_profile().get_organization()
        quarter_id = request.POST.get('quarter_id', None)
        due_date = parse(request.POST.get('due_date', None), dayfirst=True)
        q = QuarterPeriod.objects.filter(organization=organization,
                                         id=quarter_id,
                                         due_date=due_date).first()
        if q:
            q.delete()
        return HttpResponse('ok')
    return HttpResponseJson({'status': 'Bad Request'})


def send_email_remind_evaluation(request):
    if request.user.profile.is_superuser_org():
        organization = request.user.profile.get_organization()

        if not organization:
            return 'Organization not exist!'

        ceo = organization.ceo
        list_user = []
        list_user_ids = ceo.profile.get_all_subordinate(type="user_id", include_self=True)
        data = JSONParser().parse(request)
        title = data.get('title', None)
        id = data.get('id', None)
        template = get_template_by_id(id)
        for user_id in list_user_ids:
            user = get_user(request.user, user_id)
            list_user.append(user.email)
            if len(list_user) % 100 == 0:
                if settings.ENABLE_CELERY == True:
                    send_email_remind_evaluation_celery.apply_async(args=[title, template.content, list_user])
                else:
                    send_email_remind_evaluation_thread(title=title, content=template.content,
                                                        list_email=list_user)
                list_user = []
                time.sleep(1)

        # ## if len(list_user) < 100
        if settings.ENABLE_CELERY == True:
            send_email_remind_evaluation_celery.apply_async(args=[title, template.content, list_user])
        else:
            send_email_remind_evaluation_thread(title=title, content=template.content, list_email=list_user)
        return True
    else:
        return 'Not Permission!'


@app.task(name="send_mail", queue='default')
def send_email_remind_evaluation_celery(title, content, list_email):
    print 'Run celery send email'
    try:
        msg = send_mail_with_perm(title, content, FROM_EMAIL, list_email)
    except KeyError:
        return False
    return True


@postpone
def send_email_remind_evaluation_thread(title, content, list_email):
    print 'Run thread send email'
    try:
        msg = send_mail_with_perm(title, content,
                                  FROM_EMAIL, list_email)
    except KeyError:
        return False
    return True


def send_email_remind_evaluation_no_thread(title, content, list_email):
    try:
        msg = send_mail_with_perm(title, content, FROM_EMAIL, list_email)
        msg.content_subtype = "html"  # Main content is now text/html
        msg.send(fail_silently=True)
    except KeyError:
        return False
    # following added to catch attribute error if msg is None/False: Taiga #3455
    except AttributeError:
        return False
    return True