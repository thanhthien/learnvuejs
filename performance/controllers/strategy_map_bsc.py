#!/usr/bin/python
# -*- coding: utf-8 -*-

import datetime
import json

from authorization.rules import has_perm, KPI__EDITING
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.cache import cache
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.http.response import HttpResponse, Http404
from django.shortcuts import get_object_or_404, render_to_response
from django.shortcuts import render
from django.template import RequestContext
from django.views.decorators.cache import cache_page, never_cache
from django.views.generic.base import View
from django.views.generic.edit import CreateView

from company.models import Organization
from performance.base.views import PerformanceBaseView
from performance.db_models.strategy_map import StrategyMap, GroupKPI
from performance.forms import StrategyMapForBranchForm
from performance.models import KPI
from performance.services.strategymap import get_kpis_group, get_map
from user_profile.models import Profile
from utils.common import HttpResponseJson
from performance.services.quarter import get_quarter_by_org

from user_profile.services.profile import get_current_user_viewed
# class UpdateGroupStrategy(PerformanceBaseView):
#     def post(self, request, *args, **kwargs):
#
#         # TODO: check phan quyen
#
#         year = kwargs.get('year', None)
#
#         new_group = request.POST.get('new_group', 0)
#         old_group = request.POST.get('old_group', 0)
#         kpi = get_object_or_404(KPI, pk=request.POST.get('kpi'))
#         # year=year)
#         # year=year, quarter_period=None)
#
#         if old_group:
#             group = get_object_or_404(GroupKPI, pk=old_group)
#             kpi.groupkpi_set.remove(group)  # https://docs.djangoproject.com/en/1.9/topics/db/queries/#related-objects
#
#         if new_group:
#             group = get_object_or_404(GroupKPI, pk=new_group)
#             kpi.groupkpi_set.add(group)
#
#         return HttpResponse('ok')


# class DeleteGroupStrategy(PerformanceBaseView):
#     def post(self, request, *args, **kwargs):
#         organization = self.get_organization(request.user)
#         year = kwargs.get('year', None)
#         group_id = request.POST.get('group_id')
#         GroupKPI.objects.filter(pk=group_id,
#                                 map__organization=organization).delete()
#
#         return HttpResponse("ok")


# class DeleteKPIStrategy(PerformanceBaseView):
#     def post(self, request, *args, **kwargs):
#         year = kwargs.get('year', None)
#         KPI.objects.filter(id=request.POST.get('kpi_id')).delete()
#         return HttpResponse('ok')


#
# class BSCReportView(PerformanceBaseView):
#
#     def get(self, request, *args, **kwargs):
#         year = datetime.date.today().year
#         organization = self.get_organization(request.user)
#         quarter_period = organization.get_current_quarter()
#         user_ids = Profile.objects.filter(organization=organization, active=True) \
#             .exclude(user_id=organization.ceo_id).values_list(
#             # why exclude???
#             'user_id', flat=True)
#
#         # select kpi lá
#         leap_kpis = KPI.get_leap_kpis(user_ids, quarter_period.id, None)
#
#         # select duoc phan cong
#         indirect_child__kpis = KPI.get_indirect_child__kpis(user_ids, quarter_period.id, None)
#
#         leap_kpis__other = KPI.get_leap_kpis(user_ids, quarter_period.id, 'other')
#         indirect_child__kpis__other = KPI.get_indirect_child__kpis(user_ids, quarter_period.id, 'other')
#         assigned_kpis = KPI.get_assigned_kpis(user_ids, quarter_period.id, None)
#
#         # EXPLAIN for this formula: _document/kpi-bsc-report.md
#         kpis_count = leap_kpis.count() + indirect_child__kpis.count()  # OK
#         assigned_kpis_count = assigned_kpis.count()
#         other_kpis_count = leap_kpis__other.count() + indirect_child__kpis__other.count()
#
#         reviewed_kpis_count = leap_kpis.filter(latest_score__gt=0).count() + indirect_child__kpis.filter(
#             latest_score__gt=0).count()
#         reviewed_assigned_kpis_count = assigned_kpis.filter(latest_score__gt=0).count()
#         reviewed_other_kpis_count = leap_kpis__other.filter(
#             latest_score__gt=0).count() + indirect_child__kpis__other.filter(latest_score__gt=0).count()
#
#         profiles = Profile.objects.filter(user_id__in=user_ids).exclude(user_id=organization.ceo_id)
#
#         # report_to_ids = profiles.filter(report_to__isnull=False).values_list('report_to_id', flat=True).distinct()
#
#         parent_user_ids = profiles.filter(parent__isnull=False).values_list('parent__user_id', flat=True).distinct()
#
#         employees_ids = profiles.exclude(user_id__in=parent_user_ids).values_list('user_id', flat=True)
#
#         # TODO: CHECK LOGIC HERE. THE FOLLOWING DO NOT MATCHED ABOVE
#         # kpis_manager = KPI.objects.filter(user_id__in=report_to_ids,
#         #                                   parent=None,
#         #                                   quarter_period=quarter_period).count()
#         # kpis_employee = KPI.objects.filter(user_id__in=employees_ids,
#         #                                    parent=None,
#         #                                    quarter_period=quarter_period).count()
#         manager_leap_kpis = KPI.get_leap_kpis(parent_user_ids, quarter_period.id, None)
#         manager_indirect_child__kpis = KPI.get_indirect_child__kpis(parent_user_ids, quarter_period.id, None)
#         manager_kpis = manager_leap_kpis.count() + manager_indirect_child__kpis.count()
#
#         employee_leap_kpis = KPI.get_leap_kpis(parent_user_ids, quarter_period.id, None)
#         employee_indirect_child__kpis = KPI.get_indirect_child__kpis(parent_user_ids, quarter_period.id, None)
#         employee_kpis = employee_leap_kpis.count() + employee_indirect_child__kpis.count()
#
#         avg_kpi_manager = 0
#         avg_kpi_employee = 0
#         if parent_user_ids.count():
#             avg_kpi_manager = round(manager_kpis / float(parent_user_ids.count()), 1)
#
#         if employees_ids.count():
#             avg_kpi_employee = round(employee_kpis / float(employees_ids.count()), 1)
#
#         return render(request, 'performance/bsc/bsc-report.html', {
#             'organization': organization,
#             'kpis_count': kpis_count,
#             'assigned_kpis_count': assigned_kpis_count,
#             'other_kpis_count': other_kpis_count,
#             'reviewed_kpis_count': reviewed_kpis_count,
#             'reviewed_assigned_kpis_count': reviewed_assigned_kpis_count,
#             'reviewed_other_kpis_count': reviewed_other_kpis_count,
#             'quarter_period': quarter_period,
#             'avg_kpi_manager': avg_kpi_manager,
#             'avg_kpi_employee': avg_kpi_employee,
#
#             'year': year
#         })


class PublicStrategyMap(View):
    def get(self, request, *args, **kwargs):
        try:
            organization = Organization.objects.get(hash=kwargs['org_hash'])
        except:
            raise Http404
        year = kwargs.get('year', None)

        smap_hash = kwargs.get('smap_hash', None)
        quarter_period = None

        s_map = None
        try:
            if smap_hash:
                s_map = StrategyMap.objects.filter(organization=organization,
                                                   hash=smap_hash).last()
                quarter_period = s_map.quarter_period
            # not covered as url that needs year argument is not in use.
            elif year:  # pragma: no cover
                s_map = StrategyMap.objects.filter(organization=organization,
                                                   quarter_period__year=year,
                                                   director=None).last()
                return HttpResponseRedirect(reverse('strategy_map_public_by_hash',
                                                    kwargs={'org_hash': organization.get_hash(),
                                                            'smap_hash': s_map.get_hash()}))
            # smap_hash cannot be None (check url)
            else:  # pragma: no cover
                s_map = StrategyMap.objects.filter(organization=organization, director=None).order_by(
                    '-quarter_period_id').first()
                return HttpResponseRedirect(reverse('strategy_map_public_by_hash',
                                                    kwargs={'org_hash': organization.get_hash(),
                                                            'smap_hash': s_map.get_hash()}))

        except Exception as e:  # pragma: no cover
            pass

        s_maps = StrategyMap.objects.filter(organization=organization, director=s_map.get_director()).order_by(
            '-quarter_period_id')

        # years = []
        # for y in range(2014, datetime.date.today().year + 2):
        #     years.append(y)

        if s_map:
            group_financial = GroupKPI.objects.filter(map=s_map,
                                                      category='financial').order_by('id')
            group_customer = GroupKPI.objects.filter(map=s_map,
                                                     category='customer').order_by('id')
            group_internal = GroupKPI.objects.filter(map=s_map,
                                                     category='internal').order_by('id')
            group_learning = GroupKPI.objects.filter(map=s_map,
                                                     category='learninggrowth').order_by('id')
        # s_map cannot be none
        else:  # pragma: no cover
            group_financial = []
            group_customer = []
            group_internal = []
            group_learning = []

        lang = 'en'
        if 'lang' in request.GET and request.GET.get('lang') in ['en', 'vi']:
            lang = request.GET.get('lang')

        return render(request, 'performance/bsc/map/public_strategy_map.html', {
            'map': s_map,
            's_maps': s_maps,
            'group_financial': group_financial,
            'group_customer': group_customer,
            'group_internal': group_internal,
            'group_learning': group_learning,
            'year': year,
            # 'years': years,
            'org': organization,
            'organization': organization,
            'lang': lang,
            'public': True,
            'quarter_period': quarter_period
        })


class OrganizationStrategyMap(PerformanceBaseView):#pragma: no cover
    def get(self, request, *args, **kwargs):

        allow_edit = False

        type = request.GET.get('type', None)
        hash = kwargs.get('hash', None)
        user_map_hash = kwargs.get('user_map_hash', None)

        quarter_period_id = kwargs.get('quarter_period', None)
        year = kwargs.get('year', None)

        quarter_period = None
        organization = self.get_organization(request.user)
        # return HttpResponse('ok')
        # return HttpResponse(organization.id)

        current_quarter = organization.get_current_quarter()

        # return HttpResponse("test")

        cache.set('org_user' + str(request.user.id), organization, 60 * 2)

        # s_map = get_map(organization, current_quarter, hash, None )

        # map of ceo is organization map

        user_id = kwargs.get('user_id', None)

        s_map, use_hashurl_type = get_map(organization, current_quarter, hash, user_map_hash, user_id)

        if s_map and use_hashurl_type:
            if use_hashurl_type == 'user':
                # if user is CEO, need to redirect to strategymap of organization, unless ceo's map is duplicated
                if user_id and int(user_id) == organization.ceo.id:
                    # return HttpResponse( "user_id == ceo_id")
                    return HttpResponseRedirect(reverse('company_strategy_map_detail'))
                    # return HttpResponse("user_id != ceo_id")

                return HttpResponseRedirect(
                    reverse('sub_strategy_map_by_hash', kwargs={'user_map_hash': s_map.get_hash()}))
            else:
                #   return HttpResponse("***********")
                return HttpResponseRedirect(reverse('strategy_map_by_hash', kwargs={'hash': s_map.get_hash()}))

        s_maps = StrategyMap.get_maps(organization.id)

        if s_map.director_id:
            allow_edit = has_perm(KPI__EDITING, request.user, s_map.director)
        else:
            allow_edit = request.user.profile.is_superuser_org()

        group_financial, group_customer, group_internal, group_learning = get_kpis_group(s_map)

        data = {
            'map': s_map,
            'group_financial': group_financial,
            'group_customer': group_customer,
            'group_internal': group_internal,
            'group_learning': group_learning,
            # 'year': year,
            's_maps': s_maps,
            'organization': organization,
            'quarter_period': quarter_period,

            'allow_edit': allow_edit
        }
        # return HttpResponse("BUg")
        if not type:
            return render(request, 'performance/bsc/map/strategy.html', data)
        # if data:
        #     return HttpResponseJson({'status': 'ok', 'data': json.dumps(data)})

        return HttpResponseJson({'status': 'failed'})

    def post(self, request, *args, **kwargs):
        pass


class OrganizationStrategyMapV2(PerformanceBaseView):
    def get(self, request, *args, **kwargs):

        allow_edit = False

        type = request.GET.get('type', None)
        hash = kwargs.get('hash', None)
        user_map_hash = kwargs.get('user_map_hash', None)

        quarter_period_id = kwargs.get('quarter_period', None)
        year = kwargs.get('year', None)

        quarter_period = None
        organization = self.get_organization(request.user)
        # return HttpResponse('ok')
        # return HttpResponse(organization.id)
        if not quarter_period_id:
            current_quarter = organization.get_current_quarter()
        else:
            current_quarter = get_quarter_by_org(request.user, quarter_period_id, organization)

        # return HttpResponse("test")

      #  cache.set('org_user' + str(request.user.id), organization, 60 * 2)

        # s_map = get_map(organization, current_quarter, hash, None )

        # map of ceo is organization map

        user_id = kwargs.get('user_id', None)

        s_map, use_hashurl_type = get_map(organization, current_quarter, hash, user_map_hash, user_id)

        if s_map and use_hashurl_type:
            # if use_hashurl_type == 'user':
            #     # if user is CEO, need to redirect to strategymap of organization, unless ceo's map is duplicated
            #     if user_id and int(user_id) == organization.ceo.id:
            #         # return HttpResponse( "user_id == ceo_id")
            #         return HttpResponseRedirect(reverse('company_strategy_map_detail_v2'))
            #         # return HttpResponse("user_id != ceo_id")
            #
            #     return HttpResponseRedirect(
            #         reverse('sub_strategy_map_by_hash_v2', kwargs={'user_map_hash': s_map.get_hash()}))
            # else:
            #     #   return HttpResponse("***********")
            return HttpResponseRedirect(reverse('strategy_map_by_hash_v2', kwargs={'hash': s_map.get_hash()}))

        if not s_map:
            raise Http404


        s_maps = StrategyMap.get_maps(organization.id)

        if s_map.director_id:
            allow_edit = has_perm(KPI__EDITING, request.user, s_map.director)
        else:
            allow_edit = request.user.profile.is_superuser_org()

        # group_financial, group_customer, group_internal, group_learning = get_kpis_group(s_map)
        # For list employees and score
        user_viewed = get_current_user_viewed(request, request.GET.get('user_id', None))

        data = {
            'map': s_map,
            # 'group_financial': group_financial,
            # 'group_customer': group_customer,
            # 'group_internal': group_internal,
            # 'group_learning': group_learning,
            # 'year': year,
            's_maps': s_maps,
            'organization': organization,
            'quarter_period': quarter_period,
            'user_viewed': user_viewed,
            'allow_edit': allow_edit
        }
        # return HttpResponse("BUg")
        if not type:
            return render(request, 'performance/bsc/map-v2/strategy.html', data)
        # if data:
        #     return HttpResponseJson({'status': 'ok', 'data': json.dumps(data)})

        return HttpResponseJson({'status': 'failed'})


#
# class SubOrganizationStrategyMapV2(PerformanceBaseView):
#     def get(self, request, *args, **kwargs):
#         hash = kwargs.get('hash', None)
#         quarter_period_id = kwargs.get('quarter_period', None)
#         year = kwargs.get('year', None)
#
#         quarter_period = None
#         organization = self.get_organization(request.user)
#         current_quarter = organization.get_current_quarter()
#
#         cache.set('org_user' + str(request.user.id), organization, 60 * 2)
#
#         user_id = kwargs.get('director_id', None)
#         print 'user_id:', user_id
#         s_map = get_map(organization, current_quarter, hash, user_id)
#         print s_map
#         # if hash:  # get smap by hash
#         #     s_map = StrategyMap.objects.filter(organization=organization, hash=hash).first()
#         #     if not s_map:
#         #         raise Http404
#         #     quarter_period = s_map.quarter_period
#         # else:  # get smap of current quarter
#         #     director = get_object_or_404(User, pk=kwargs['director_id'])
#         #     s_map = StrategyMap.objects.get_or_create(organization=organization,
#         #                                               quarter_period=current_quarter,
#         #                                               director=director)[0]
#
#         #          return HttpResponseRedirect(reverse('sub_strategy_map_by_hash', kwargs={'hash': s_map.get_hash()}))
#         # \
#         #        director = s_map.get_director()
#         # group_financial, group_customer, group_internal, group_learning = get_kpis_group(s_map)
#
#         # s_maps = StrategyMap.get_maps(organization.id, director=director)
#         #
#         # if s_map:
#         #
#         #     if s_map.quarter_period is None:
#         #         s_map.save()
#         #
#         #     group_financial = GroupKPI.objects.filter(map=s_map,
#         #                                               category='financial').order_by('id')
#         #     group_customer = GroupKPI.objects.filter(map=s_map,
#         #                                              category='customer').order_by('id')
#         #     group_internal = GroupKPI.objects.filter(map=s_map,
#         #                                              category='internal').order_by('id')
#         #     group_learning = GroupKPI.objects.filter(map=s_map,
#         #                                              category='learninggrowth').order_by('id')
#         # else:
#         #     group_financial = []
#         #     group_customer = []
#         #     group_internal = []
#         #     group_learning = []
#
#         return render(request, 'performance/bsc/map-v2/strategy.html', {
#             'map': s_map,
#             # 'group_financial': group_financial,
#             # 'group_customer': group_customer,
#             # 'group_internal': group_internal,
#             # 'group_learning': group_learning,
#             # 'year': year,
#             #    's_maps': s_maps,
#             'organization': organization,
#             'quarter_period': quarter_period,
#         })
#
#     def post(self, request, *args, **kwargs):
#         pass
#
#
#
# class SubOrganizationStrategyMap(PerformanceBaseView):
#     def get(self, request, *args, **kwargs):
#         hash = kwargs.get('hash', None)
#         quarter_period_id = kwargs.get('quarter_period', None)
#         year = kwargs.get('year', None)
#
#         quarter_period = None
#         organization = self.get_organization(request.user)
#         current_quarter = organization.get_current_quarter()
#
#         cache.set('org_user' + str(request.user.id), organization, 60 * 2)
#
#         user_id = kwargs.get('director_id', None)
#         print 'user_id:', user_id
#         s_map = get_map(organization, current_quarter, hash, user_id)
#         print s_map
#         # if hash:  # get smap by hash
#         #     s_map = StrategyMap.objects.filter(organization=organization, hash=hash).first()
#         #     if not s_map:
#         #         raise Http404
#         #     quarter_period = s_map.quarter_period
#         # else:  # get smap of current quarter
#         #     director = get_object_or_404(User, pk=kwargs['director_id'])
#         #     s_map = StrategyMap.objects.get_or_create(organization=organization,
#         #                                               quarter_period=current_quarter,
#         #                                               director=director)[0]
#
#         #          return HttpResponseRedirect(reverse('sub_strategy_map_by_hash', kwargs={'hash': s_map.get_hash()}))
#         # \
#         #        director = s_map.get_director()
#         group_financial, group_customer, group_internal, group_learning = get_kpis_group(s_map)
#
#         # s_maps = StrategyMap.get_maps(organization.id, director=director)
#         #
#         # if s_map:
#         #
#         #     if s_map.quarter_period is None:
#         #         s_map.save()
#         #
#         #     group_financial = GroupKPI.objects.filter(map=s_map,
#         #                                               category='financial').order_by('id')
#         #     group_customer = GroupKPI.objects.filter(map=s_map,
#         #                                              category='customer').order_by('id')
#         #     group_internal = GroupKPI.objects.filter(map=s_map,
#         #                                              category='internal').order_by('id')
#         #     group_learning = GroupKPI.objects.filter(map=s_map,
#         #                                              category='learninggrowth').order_by('id')
#         # else:
#         #     group_financial = []
#         #     group_customer = []
#         #     group_internal = []
#         #     group_learning = []
#
#         return render(request, 'performance/bsc/map/strategy.html', {
#             'map': s_map,
#             'group_financial': group_financial,
#             'group_customer': group_customer,
#             'group_internal': group_internal,
#             'group_learning': group_learning,
#             # 'year': year,
#             #    's_maps': s_maps,
#             'organization': organization,
#             'quarter_period': quarter_period,
#         })
#
#     def post(self, request, *args, **kwargs):
#         pass

#
# @login_required
# @staff_member_required
# def import_bsc(request):
#     if request.method == "POST":
#         name = request.POST.get('name');
#         measurement = request.POST.get('measurement');
#         job_title = request.POST.get('job_title');
#         department = request.POST.get('department');
#
#         BscKPI.add(name=name, measurement=measurement, job_title=job_title, department=department)
#
#     variables = {
#         'mission': 'mission'
#     }
#
#     return render(request, 'performance/goallib.html', variables)
#


@login_required
@cache_page(60 * 60 * 10)
def bsc_json(request):#pragma: no cover
    goals = []
    # bscs = BscKPI.objects.select_related('department').all()
    # for k in bscs:
    #     goals.append(k.to_json)

    variables = RequestContext(request, {
        'goals': json.dumps(goals)
    })

    return render_to_response('performance/goallib.json',
                              variables, content_type="application/json")


class SWOT(PerformanceBaseView):
    def get(self, request, *args, **kwargs):
        variables = {}

        return render(request, 'performance/swot/swot-online.html', variables)


class BSC_KSF(PerformanceBaseView):
    def get(self, request, *args, **kwargs):
        variables = {}

        return render(request, 'performance/ksf/index.html', variables)


class BHAG(PerformanceBaseView):
    def get(self, request, *args, **kwargs):
        variables = {}

        return render(request, 'performance/bhag/index.html', variables)


class CreateStrategyMapForBranch(CreateView, PerformanceBaseView):#pragma: no cover
    model = StrategyMap
    form_class = StrategyMapForBranchForm
    template_name = 'performance/bsc/map/create-sm-branch.html'

    def get_form_kwargs(self):
        kwargs = CreateView.get_form_kwargs(self)
        kwargs['organization'] = self.get_organization(self.request.user)
        return kwargs

    def form_invalid(self, form):
        return CreateView.form_invalid(self, form)

    def form_valid(self, form):
        director = form.cleaned_data['director']
        organization = self.get_organization(self.request.user)
        current_quarter = organization.get_current_quarter()
        sm = StrategyMap.get_create(organization, current_quarter, director)
        return HttpResponseRedirect(sm.get_absolute_url())
