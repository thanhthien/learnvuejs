#!/usr/bin/python
# -*- coding: utf-8 -*-

import json
from django.core.urlresolvers import reverse
import pytest
from utils.random_str import random_string

from performance.db_models.competency import Competency
#
#
# @pytest.mark.django_db(transaction=False)
# def test_competency_complete(client_authed, userA, organization):
#     link = reverse('competency-autocomplete')
#     _random_string = random_string()
#     quarter_period = organization.get_current_quarter()
#     comp = Competency(name=_random_string,
#                       user=userA,
#                       in_library=True,
#                       quarter_period=quarter_period, )
#     comp.save()
#     response = client_authed.get(link)
#     content = json.loads(response.content)
#     assert content['query'] == 'Unit'
#     assert content['suggestions'] == []
