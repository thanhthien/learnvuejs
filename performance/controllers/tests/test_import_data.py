# !/usr/bin/python
# -*- coding: utf-8 -*-
from random import randint

import pytest
from django.core.urlresolvers import reverse


@pytest.mark.django_db(transaction=False)
def test_import_users(client_authed, userA, client_admin):
    link = reverse('import_users')
    response = client_admin.get(link)
    assert response.status_code == 200
    assert response.templates[0].name == 'imports/import_users.html'

    client_admin.force_login(userA)
    assert userA.profile.is_superuser_org() is False
    assert userA.is_authenticated() is True
    response = client_admin.get(link)
    assert response.status_code == 302
    assert response.url == '/403'


@pytest.mark.django_db(transaction=False)
def test_import_kpis(client_authed, client):
    link = reverse('import_kpis')
    response = client_authed.get(link)
    assert response.status_code == 200
    assert response.templates[0].name == 'imports/import_kpis.html'

    client_authed.logout()
    response = client_authed.get(link)
    assert response.status_code == 302
    assert response.url == '/login/?next=/performance/import/kpis/'


@pytest.mark.django_db(transaction=False)
def import_general_kpis(client_authed, client):
    link = reverse('import_general_kpis')
    response = client_authed.get(link)
    assert response.status_code == 200
    assert response.templates[0].name == 'imports/import_general_kpis.html'

    client_authed.logout()
    response = client_authed.get(link)
    assert response.status_code == 302
    assert response.url == '/login/?next=/performance/import/general-kpis/'


@pytest.mark.django_db(transaction=False)
def test_import_kpis_copy(client_authed):
    link = reverse('import_kpis_copy')
    response = client_authed.get(link)
    assert response.status_code == 200
    assert response.templates[0].name == 'imports/import_kpis_copy.html'

    client_authed.logout()
    response = client_authed.get(link)
    assert response.status_code == 302
    assert response.url == '/login/?next=/performance/import/kpis/copy/'


@pytest.mark.django_db(transaction=False)
def test_history_deleted_kpi(client_authed, userA):
    link = reverse('history_deleted_kpi')
    response = client_authed.get(link)
    assert response.status_code == 200
    assert response.templates[0].name == 'performance/history_deleted_kpi.html'

    response = client_authed.get(link, {"user_id": userA.id})
    assert response.status_code == 200
    assert response.templates[0].name == 'performance/history_deleted_kpi.html'

    random_id = randint(1, 10000)
    link1 = reverse('history_deleted_kpi') + "?user_id=" + format(random_id)
    response = client_authed.get(link1)
    if userA.id != random_id:
        assert response.status_code == 404

    client_authed.logout()
    response = client_authed.get(link)
    assert response.status_code == 302
    assert response.url == '/login/?next=/performance/deleted_kpi/'


@pytest.mark.django_db(transaction=False)
def test_integration(client_authed):
    link = reverse('integration')
    response = client_authed.get(link)
    assert response.status_code == 200
    assert response.templates[0].name == 'integration/dashboard.html'

    client_authed.logout()
    response = client_authed.get(link)
    assert response.status_code == 302
    assert response.url == '/login/?next=/performance/integration/'


@pytest.mark.django_db(transaction=False)
def test_email_redirect(client_authed, userA):
    link = reverse('email_redirect')

    response = client_authed.get(link)
    assert response.status_code == 404

    response = client_authed.get(link, {'email': userA.email})
    assert response.status_code == 302
    assert response.url == '/performance/kpi-editor/emp/{}/'.format(userA.id)

    response = client_authed.get(link, {'email': userA.email, 'type': 'team'})
    assert response.status_code == 302
    assert response.url == '/performance/team/?user_id={}'.format(userA.id)

    response = client_authed.get(link, {'email': userA.email, 'type': 'people'})
    assert response.status_code == 302
    assert response.url == '/performance/people/?user_id={}'.format(userA.id)
