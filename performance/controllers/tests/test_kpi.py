# !/usr/bin/python
# -*- coding: utf-8 -*-
import random
import time

import datetime
from django.core.cache import cache
from django.test.utils import override_settings
from django.utils.translation import ugettext
from gevent import sleep

from company.models import Organization
from performance.controllers.kpi import set_review_type
from performance.models import KPI, KpiComment, date, Ecal

import pytest
from django.core.urlresolvers import reverse

from performance.services.children_data_services import get_children_data, set_children_data
from user_profile.services.user import get_user
import json

# @pytest.mark.django_db(transaction=False)
# def test_KPIWorkSheetDetail(client_authed, kpi, userA):
#
#
#     # test if  auto fix running or not
#     key= 'fix_kpi_self_' + str(kpi.id)
#     response = client_authed.get(reverse( 'KPIWorkSheetDetail', kwargs={'kpi_id': kpi.id}))
#     assert cache.get(key) is None
#     assert response.status_code == 200
#
#     response = client_authed.get(reverse( 'KPIWorkSheetDetail', kwargs={'kpi_id': kpi.id}) + "?follow_page=1")
#     assert response.status_code == 200
#     assert response.content == 200
#     assert cache.get(key)
#
#
from performance.services import get_ecal_link, PermissionDenied, create_child_kpi
from user_profile.services.profile import can_view
from utils import random_string


# Lam #2447 taiga

@pytest.mark.django_db(transaction=False)
def test_UpdateScoreKPI(kpi, client_authed, organization, userA):
    url = reverse('kpi_update_score') #, kwargs={'kpi_id': kpi.id})
    kpi.month_1 = 10
    kpi.month_1_target = 20
    kpi.month_2_target = 25
    kpi.month_3_target = 30
    kpi.save()

    kpi.month_1 = 15
    kpi.month_2 = 25
    kpi.month_3 = 15
   # kpi.month_1_target = 20

    response = client_authed.post(url, kpi.to_json())

    kpi_result = KPI.objects.get(id=kpi.id)

    assert kpi_result.month_1 == kpi.month_1
    assert kpi_result.month_2 == kpi.month_2
    assert kpi_result.month_3 == kpi.month_3

    assert kpi_result.month_1_score == 75
    assert kpi_result.month_2_score == 100
    assert kpi_result.month_3_score == 50

    kpi.weight = 0
    kpi.save()
    response = client_authed.post(url, kpi.to_json())
    assert response.status_code == 400


@pytest.mark.django_db(transaction=False)
def test_cascade_kpi_name(kpi, client_authed, organization):
        url = reverse('cascade-kpi-name', kwargs={'kpi_id': kpi.id})
        response = client_authed.get(url)
        assert response.status_code == 200
        assert response.content.find(kpi.get_kpi_id_with_cat()) >= 0

        url = reverse('cascade-kpi-name', kwargs={'kpi_id': kpi.id + 1000})
        response = client_authed.get(url)
        assert response.content == "KPI không tồn tại"


@pytest.mark.django_db(transaction=False)
def test_category_update(kpi, client_authed, organization, userA):
    url = reverse('update_category')
    response = client_authed.get(url)
    assert response.status_code == 200
    js = json.loads(response.content)
    assert len(js) == 6
    for index in js:
        if index['text'] == "Tài chính":
            assert index['value'] == "financial"
    kpi.quarter_period = None
    kpi.save()
    data = {'pk': kpi.id, 'value': 'financial'}
    response = client_authed.post(url, data)
    assert response.status_code == 200
    assert response.content == "Kpi doesn't exist."

    kpi.user = userA
    kpi.quarter_period = organization.get_current_quarter()
    kpi.save()
    data = {'pk': kpi.id, 'value': 'financial'}
    response = client_authed.post(url, data)
    assert response.status_code == 200
    js = json.loads(response.content)
    assert js['id'] == kpi.id


@pytest.mark.django_db(transaction=False)
def test_enable_edit_kpi(kpi, client_authed, organization, userA):
    url = reverse('enable-edit-kpi')
    response = client_authed.post(url)
    assert response.status_code == 200
    assert response.content == "ok"
    assert organization.enable_edit is True

@pytest.mark.django_db(transaction=False)
def test_set_review_type(kpi, organization, userA, userB):
    """
    Test for performance/controllers/kpi.py::set_review_type()
    """
    """
    Case 1: month1 is not none. returns kpi
    """
    organization.monthly_review_lock = 1
    organization.save()

    month_1 = random_string()
    month_2 = None
    month_3 = None

    result = set_review_type(kpi, month_1, month_2, month_3)
    assert result.review_type == "monthly"

    """
    Case 2: No parameter is None. returns kpi
    """
    month_1 = random_string()
    month_2 = random_string()
    month_3 = random_string()

    result = set_review_type(kpi, month_1, month_2, month_3)
    assert result.review_type == "monthly"

    '''
    Case 3: Allow all, all month
    '''
    organization.monthly_review_lock = 'allow_all'
    organization.save()
    month_1 = random_string()
    month_2 = random_string()
    month_3 = random_string()
    result = set_review_type(kpi, month_1, month_2, month_3)
    assert result.review_type == "monthly"

    """
    Case 4: kpi.user in None, returns `exception` value -> None
    """
    kpi.user = None
    kpi.save()
    result1 = set_review_type(kpi, None, None, None)
    assert result1 is None




@pytest.mark.django_db(transaction=False)
def test_kpi_logs(client_authed, userA_manager, userA):
    """
    Test Case for performance/controllers/kpi.py::kpi_logs
    :param client_authed:
    :param userA_manager:
    :param userA:
    """
    """
    Case 1: Success- Status code 200
    """
    link = '/performance/kpi_log/' + str(userA_manager.id) + '/'
    response = client_authed.get(link, {'page': 1})
    assert response.status_code == 200

    link1 = '/performance/kpi_log/' + str(userA.id) + '/'
    response1 = client_authed.get(link1)
    assert response1.status_code == 200

    link = '/performance/kpi_log/' + str(userA.id) + '/'
    response = client_authed.get(link, {'page': 99999})
    assert response.status_code == 200

    """
    Case 2: Forbidden as user and client belong to different organization
    """
    organization1 = Organization()
    organization1.user = userA
    organization1.ceo = userA

    organization1.name = "clee"
    organization1.save()

    ou = userA.profile
    ou.organization = organization1
    ou.save()

    link2 = '/performance/kpi_log/' + str(userA_manager.id) + '/'
    response2 = client_authed.get(link2)
    assert response2.status_code == 403

    """
    Case 3: Fail- Status code 404
    """
    trial_id = random.randint(1, 1500)
    link3 = '/performance/kpi_log/' + str(trial_id) + '/'
    user = get_user(userA, trial_id)
    response3 = client_authed.get(link3)
    if user is None:
        assert response3.status_code == 404


@pytest.mark.django_db(transaction=False)
def test_update_score_kpi(client_authed, client_userA_manager, userA_manager, organization, userA):
    k = KPI()
    k.target = 100
    k.operator = ">="
    k.user = userA_manager
    k.quarter_period = organization.get_current_quarter()
    k.save()
    url = reverse('kpi_update_score')
    """
    Test for unauthorized access (Permission Denied)
    userA updating kpi of userA_manager --> Denied
    """
    response = client_authed.post(url, {'id': k.id,
                                        'quarter_one_target': 0,
                                        'quarter_two_target': 0,
                                        'quarter_three_target': 0,
                                        'quarter_four_target': 0,
                                        'update_quarter_target': '1234',
                                        'operator': '<=',
                                        "month_2": 0, "month_3": 0})

    assert response.status_code == 400

    """
    Test for 404
    kpi_id not found
    """
    k.user = userA
    k.save()

    key1 = random.randint(1,10000)
    if key1 != k.id:
        response = client_authed.post(url, {'id': key1,
                                        'quarter_one_target': 0,
                                        'quarter_two_target': 0,
                                        'quarter_three_target': 0,
                                        'quarter_four_target': 0,
                                        'update_quarter_target': '1234',
                                        'operator': '<=',
                                        "month_2": 0, "month_3": 0})

        assert response.status_code == 404



    '''
    Test month_2 == 0 and target == 0
    '''

    response = client_authed.post(url, {'kpi_id': k.id,
                                        'quarter_one_target': 0,
                                        'quarter_two_target': 0,
                                        'quarter_three_target': 0,
                                        'quarter_four_target': 0,
                                        'update_quarter_target': '1234',
                                        'operator': '<=',
                                        "month_1": 0, "month_2": 0, "month_3": 0})
    assert response.status_code == 200
    time.sleep(10) #why?
    assert KPI.objects.get(id=k.id).latest_score == 120

    '''
    CHECK ERROR when change Quarter Target, not update quarter target now
    '''

    cache.clear()

    response = client_authed.post(url, {'kpi_id': k.id,
                                        'quarter_one_target': 10,
                                        'month_3_target': 10,
                                        'update_quarter_target': '1',
                                        'operator': '>=',
                                        "month_1": 10, "month_2": 10, "month_3": 10})
    assert response.status_code == 200
    assert KPI.objects.get(id=k.id).latest_score == 10

    response = client_authed.post(url, {'kpi_id': k.id,
                                        'quarter_one_target': 10,
                                        'operator': '>=',
                                        'update_quarter_target': '1',
                                        "month_1": 10, "month_2": 15, "month_3": 6})
    assert response.status_code == 200
    assert KPI.objects.get(id=k.id).latest_score == 6

    quarter = KPI.objects.get(id=k.id).quarter_period

    '''
    GIVEN: change quarter
    THEN: not change score when update target now
    '''

    quarter.quarter = 2
    quarter.save()
    response = client_authed.post(url, {'kpi_id': k.id,
                                        'quarter_two_target': 20,
                                        'month_3_target': 20,
                                        'update_quarter_target': '2',
                                        'operator': '>=',
                                        "month_1": 10, "month_2": 10, "month_3": 10})
    assert response.status_code == 200
    assert KPI.objects.get(id=k.id).latest_score == 10

    '''
    GIVEN: change quarter
    THEN: not change score when update target now
    '''

    quarter.quarter = 3
    quarter.save()
    response = client_authed.post(url, {'kpi_id': k.id,
                                        'quarter_three_target': 25,
                                        'month_3_target': 25,
                                        'update_quarter_target': '3',
                                        'operator': '>=',
                                        "month_1": 10, "month_2": 10, "month_3": 10})
    assert response.status_code == 200
    assert KPI.objects.get(id=k.id).latest_score == 10

    '''
        GIVEN: change quarter
        THEN: not change score when update target now
        '''

    quarter.quarter = 4
    quarter.save()
    response = client_authed.post(url, {'kpi_id': k.id,
                                        'quarter_four_target': 40,
                                        'month_3_target': 40,
                                        'update_quarter_target': '4',
                                        'operator': '>=',
                                        "month_1": 10, "month_2": 10, "month_3": 10})
    assert response.status_code == 200
    assert KPI.objects.get(id=k.id).latest_score == 10
    '''
    TEST Update Kpi calculate real with score_calculation_type = most_recent
    '''
    response = client_authed.post(url, {'kpi_id': k.id,
                                        'quarter_one_target': 10,
                                        'update_quarter_target': '1',
                                        'score_calculation_type': 'most_recent',
                                        'operator': '>=',
                                        "month_1": 10, "month_2": 20, "month_3": 30})
    assert response.status_code == 200
    assert KPI.objects.get(id=k.id).real == 30

    '''
    TEST Update Kpi calculate real with score_calculation_type = average
    '''
    response = client_authed.post(url, {'kpi_id': k.id,
                                        'quarter_one_target': 10,
                                        'update_quarter_target': '1',
                                        'score_calculation_type': 'average',
                                        'operator': '>=',
                                        "month_1": 40, "month_2": 20, "month_3": 30})
    assert response.status_code == 200
    assert KPI.objects.get(id=k.id).real == 30

    '''
    TEST Update Kpi calculate real with score_calculation_type = sum
    '''
    response = client_authed.post(url, {'kpi_id': k.id,
                                        'quarter_one_target': 10,
                                        'update_quarter_target': '1',
                                        'score_calculation_type': 'sum',
                                        'operator': '>=',
                                        "month_1": 40, "month_2": 20, "month_3": 60})
    assert response.status_code == 200
    assert KPI.objects.get(id=k.id).real == 120

    '''
        TEST Change score_calculation_type = average
    '''
    url = reverse('kpi_services')
    response = client_authed.post(url, {'id': k.id,
                                        'score_calculation_type': 'average',
                                        'command': 'update_quarter_target'})
    assert response.status_code == 200
    assert KPI.objects.get(id=k.id).real == 40
    '''
        TEST Change score_calculation_type = sum
    '''
    response = client_authed.post(url, {'id': k.id,
                                        'score_calculation_type': 'sum',
                                        'command': 'update_quarter_target'})
    assert response.status_code == 200
    assert KPI.objects.get(id=k.id).real == 120
    '''
        TEST Change score_calculation_type = most_recent
    '''
    response = client_authed.post(url, {'id': k.id,
                                        'score_calculation_type': 'most_recent',
                                        'command': 'update_quarter_target'})
    assert response.status_code == 200
    assert KPI.objects.get(id=k.id).real == 60


    '''
    Test for calculation with parent_score_auto = False
    '''
    update_score_url = reverse('kpi_update_score')
    url_update_kpi =   reverse('kpi_services')

    '''
    update targets
    '''
    response = client_authed.post(url_update_kpi, json.dumps({'id': k.id,
                                                   'command': 'update_month_target',

                                                   'month_1_target': 100,
                                        'month_2_target': 100,
                                        'month_3_target': 100,
                                        # "month_1": 40, "month_2": 20, "month_3": 30
                                        }),content_type="application/json")
    assert response.status_code == 200
    assert KPI.objects.get(id=k.id).month_1_target == 100
    assert KPI.objects.get(id=k.id).month_2_target == 100
    assert KPI.objects.get(id=k.id).month_3_target == 100

    '''
    init children data
    '''

    # userA_manager assign kpis into userA
    k1 = create_child_kpi(k, k.quarter_period, actor=userA_manager)
    assert k1.kpi_type_v2() == 'kpi_con_normal'
    # k1 = k1.assign_to(userA)
    '''
    update-target
    '''
    response = client_authed.post(url_update_kpi, json.dumps({'id': k1.id,
                                                   'command': 'update_month_target',

                                                   'month_1_target': 10,
                                                   'month_2_target': 10,
                                                   'month_3_target': 10,
                                                   # "month_1": 40, "month_2": 20, "month_3": 30
                                                   }),content_type="application/json")
    assert response.status_code == 200
    assert KPI.objects.get(id=k1.id).month_1_target == 10
    assert KPI.objects.get(id=k1.id).month_2_target == 10
    assert KPI.objects.get(id=k1.id).month_3_target == 10
    assert KPI.objects.get(id=k1.id).target == 10
    '''
    update-score
    '''
    data = k1.to_json()
    data['month_1'] = 40
    data['month_2'] = 20
    data['month_3'] = 30
    data['real'] = 40
    response = client_authed.post(update_score_url, data)
    assert response.status_code == 200
    assert KPI.objects.get(id=k1.id).month_1_score == 120
    assert KPI.objects.get(id=k1.id).month_2_score == 120
    assert KPI.objects.get(id=k1.id).month_3_score == 120
    assert KPI.objects.get(id=k1.id).latest_score == 120

    # userA_manager assign kpis into userA from one of userA's kpis
    k2 = create_child_kpi(k, k.quarter_period, actor=userA_manager)
    k2.save()
    # k2 = k2.assign_to(userA)
    '''
    update-target
    '''
    response = client_authed.post(url_update_kpi, json.dumps({'id': k2.id,
                                                    'command':'update_month_target',
                                                   'month_1_target': 40,
                                                   'month_2_target': 20,
                                                   'month_3_target': 30,
                                                   # "month_1": 40, "month_2": 20, "month_3": 30
                                                   }),content_type="application/json")
    assert response.status_code == 200
    assert KPI.objects.get(id=k2.id).month_1_target == 40
    assert KPI.objects.get(id=k2.id).month_2_target == 20
    assert KPI.objects.get(id=k2.id).month_3_target == 30
    assert KPI.objects.get(id=k2.id).target == 30
    '''
    update-score
    '''
    data = k2.to_json()
    data['month_1'] = 40
    data['month_2'] = 20
    data['month_3'] = 30
    data['real'] = 40
    response = client_authed.post(update_score_url, data)
    assert response.status_code == 200
    assert KPI.objects.get(id=k2.id).month_1_score == 100
    assert KPI.objects.get(id=k2.id).month_2_score == 100
    assert KPI.objects.get(id=k2.id).month_3_score == 100
    assert KPI.objects.get(id=k2.id).latest_score == 100

    '''
    assert score for k
    '''
    assert KPI.objects.get(id=k.id).month_1_score == 110
    assert KPI.objects.get(id=k.id).month_2_score == 110
    assert KPI.objects.get(id=k.id).month_3_score == 110
    assert KPI.objects.get(id=k.id).latest_score == 110

    '''
    test update-score with parent_auto_score = True
    '''

    '''
    update score
    '''
    data = k.to_json()
    data['month_1'] = 0
    data['month_2'] = 0
    data['month_3'] = 0
    data['real'] = 0
    response = client_authed.post(update_score_url, data)
    assert response.status_code == 200
    assert KPI.objects.get(id=k.id).month_1_score == 110
    assert KPI.objects.get(id=k.id).month_2_score == 110
    assert KPI.objects.get(id=k.id).month_3_score == 110
    assert KPI.objects.get(id=k.id).latest_score == 110 ### Write this when confused about logic, need to make convention first, modify later

    '''
    init parent_auto_score
    '''
    organization.default_auto_score_parent = True
    organization.save()
    children_data = get_children_data(k.user, k)
    children_data['parent_score_auto'] = False
    set_children_data(k.user, k, children_data)

    '''
    update score
    '''
    data = k.to_json()
    data['month_1'] = 40
    data['month_2'] = 20
    data['month_3'] = 30
    data['real'] = 40
    response = client_authed.post(update_score_url, data)
    assert response.status_code == 200
    assert KPI.objects.get(id=k.id).month_1_score == 40
    assert KPI.objects.get(id=k.id).month_2_score == 20
    assert KPI.objects.get(id=k.id).month_3_score == 30
    assert KPI.objects.get(id=k.id).latest_score == 30

@pytest.mark.django_db(transaction=False)
def test_kpi_deleted(client_admin, kpi, organization):
    link = reverse('kpi_deleted')
    response = client_admin.get(link)
    assert response.status_code == 200
    assert response.templates[0].name == 'admin/deleted_kpis_view.html'

    response = client_admin.get(link, {'search': 'advance', 'operator': 'NONE'})
    assert response.status_code == 200
    assert response.templates[0].name == 'admin/deleted_kpis_view.html'

    response = client_admin.get(link, {'search': 'advance', 'operator': 'AND'})
    assert response.status_code == 200
    assert response.templates[0].name == 'admin/deleted_kpis_view.html'

    data = {
        'search': 'advance',
        'operator': 'OR',
        'kpi_name': random_string(),
        'kpi_org': random_string(),
        'kpi_user_email': random_string(),
        'kpi_user_name': random_string(),
        'from': str(datetime.date.today() + datetime.timedelta(days=-30)),
        'to': str(datetime.date.today()),
    }
    response = client_admin.get(link, data)
    assert response.status_code == 200
    assert response.templates[0].name == 'admin/deleted_kpis_view.html'

    search_choice = ['kpi_name', 'kpi_org', 'kpi_user_email', 'kpi_user_name', 'kpi_date_range']
    for search in search_choice:
        response = client_admin.get(link, {'search': search, 'operator': 'AND'})
        assert response.status_code == 200
        assert response.templates[0].name == 'admin/deleted_kpis_view.html'



@pytest.mark.django_db(transaction=False)
def test_kpi_comment(kpi, client_authed, organization, userA):
    content1 = random_string()
    content = random_string()
    kpi.user = userA
    kpi.save()
    unique_key = kpi.unique_key
    link = reverse('kpicomment', kwargs={'unique_key': unique_key})
    response = client_authed.get(link)
    assert response.status_code == 200

    kpi_comment = KpiComment()
    kpi_comment.kpi_unique_key = kpi.unique_key
    kpi_comment.user = userA
    kpi_comment.kpi = kpi
    kpi_comment.content = content
    kpi_comment.save()


    kpi_comment1 = KpiComment()
    kpi_comment1.kpi_unique_key = kpi.unique_key
    kpi_comment1.user = userA
    kpi_comment1.kpi = kpi
    kpi_comment1.content = content1
    kpi_comment1.save()

    unique_key = kpi.unique_key
    link = reverse('kpicomment', kwargs={'unique_key': unique_key})
    response = client_authed.post(link, {'comment':content, 'obj_id':kpi_comment.id})
    assert response.status_code == 200
    js = json.loads(response.content)
    assert js['content'] == content

    response = client_authed.get(link, {'comment': content, 'id': kpi.id})
    assert response.status_code == 200
    js = json.loads(response.content)
    b = False
    for x in js:
        if x[u'content'] == u"":
            b = True
            break
    assert b == True


@pytest.mark.django_db(transaction=False)
def test_assign_kpi(client_authed, organization, userA):
    link = reverse('assign-kpi')

    kpi = KPI()
    kpi.user = userA
    kpi.quarter_period = organization.get_current_quarter()
    kpi.quarter_one_target = 10
    kpi.target = 10
    kpi.score_calculation_type = 'sum'
    kpi.quarter_one_target = 1
    kpi.quarter_two_target = 2
    kpi.quarter_three_target = 3
    kpi.quarter_four_target = 4
    kpi.save()

    response = client_authed.post(link, {'parent': 'true', 'kpi_id': kpi.id, 'user_id': userA.id})
    assert response.status_code == 200

    kpi1 = KPI()
    kpi1.user = userA
    kpi1.quarter_period = organization.get_current_quarter()
    kpi1.quarter_one_target = 10
    kpi1.target = 10
    kpi1.score_calculation_type = 'sum'
    kpi1.quarter_one_target = 1
    kpi1.quarter_two_target = 2
    kpi1.quarter_three_target = 3
    kpi1.quarter_four_target = 4
    kpi1.save()
    response1 = client_authed.post(link, {'parent': 'false', 'kpi_id': kpi.id, 'user_id': userA.id})
    assert response1.status_code == 200
    js = json.loads(response1.content)
    assert js['user_id'] == userA.id


@pytest.mark.django_db(transaction=False)
def test_update_review_type_kpi(organization, userA, client_authed):
    link = reverse('update-review-type')
    response = client_authed.get(link)
    result = json.loads(response.content)
    assert result == [
            {'value': '', 'text': ugettext('Final Review')},
            {'value': 'daily', 'text': ugettext('Daily')},
            {'value': 'weekly', 'text': ugettext('Weekly')},
            {'value': 'monthly', 'text': ugettext('Monthly')},
        ]

    kpi = KPI()
    kpi.user = userA
    kpi.quarter_period = organization.get_current_quarter()
    kpi.quarter_one_target = 10
    kpi.target = 10
    kpi.score_calculation_type = 'sum'
    kpi.quarter_one_target = 1
    kpi.quarter_two_target = 2
    kpi.quarter_three_target = 3
    kpi.quarter_four_target = 4
    kpi.save()

    link = reverse('update-review-type')

    response = client_authed.post(link, {'pk': kpi.id, 'value': 'monthly'})
    assert response.status_code == 200
    content = json.loads(response.content)
    assert content == {'id': kpi.id,
                       'parent_id': kpi.parent_id,
                       'review_type': 'monthly'}
    kpi = KPI.objects.get(pk=kpi.id)
    assert kpi.review_type == 'monthly'

    response = client_authed.post(link, {'pk': kpi.id + 12000, 'value': 'monthly'})
    assert response.content == "Kpi doesn't exist."

    # response = client_authed.post(link, {'pk': kpi.id, 'value': 'xxxx'})
    # assert response.status_code == 200


@pytest.mark.django_db(transaction=True)
def test_delete_kpi(organization, userA, userB, client_authed):
    kpi = KPI()
    kpi.user = userA
    kpi.quarter_period = organization.get_current_quarter()
    kpi.quarter_one_target = 10
    kpi.target = 10
    kpi.score_calculation_type = 'sum'
    kpi.quarter_one_target = 1
    kpi.quarter_two_target = 2
    kpi.quarter_three_target = 3
    kpi.quarter_four_target = 4
    kpi.save()

    link = reverse('delete_kpi')
    response = client_authed.post(link, {'kpi_id': kpi.id})
    assert response.status_code == 200
    content = json.loads(response.content)
    assert content['status'] == 'OK'

    kpi = KPI()
    kpi.user = userB
    kpi.quarter_period = organization.get_current_quarter()
    kpi.quarter_one_target = 10
    kpi.target = 10
    kpi.score_calculation_type = 'sum'
    kpi.quarter_one_target = 1
    kpi.quarter_two_target = 2
    kpi.quarter_three_target = 3
    kpi.quarter_four_target = 4
    kpi.save()
    response = client_authed.post(link, {'kpi_id': kpi.id})
    assert response.status_code == 403
    content = json.loads(response.content)
    assert content['status'] == 'Forbidden'
#    assert content == {'data': None, 'message': 'Bạn không được phép xoá KPI này', 'status': 'Forbidden', 'version': 2}

    response = client_authed.post(link, {'kpi_id': kpi.id + 11111})
    assert response.status_code == 404


@pytest.mark.django_db(transaction=False)
def test_kpicomment(kpi, kpi_userA_manager, userA_manager, client_authed, organization, userA):

    content1 = random_string()
    content = random_string()
    kpi.user = userA
    kpi.save()

    kpi_comment = KpiComment()
    kpi_comment.kpi_unique_key = kpi.unique_key
    kpi_comment.user = userA
    kpi_comment.kpi = kpi
    kpi_comment.content = content
    kpi_comment.save()

    unique_key = kpi.unique_key
    link = reverse('kpi-comment', kwargs={'unique_key': unique_key})
    data = {
        'id': kpi.id
    }
    response = client_authed.get(link, data)
    assert response.status_code == 200  
    assert response.templates[0].name == "kpi_modules/notes.html"

    unique_key = kpi.unique_key
    link = reverse('kpi-comment', kwargs={'unique_key': unique_key})
    data = {
        'id': kpi.id
    }
    extra = {'id': kpi.id}
    KPI.objects.filter(unique_key=unique_key, **extra).order_by('-id').first().delete()
    response = client_authed.get(link, data)
    assert response.status_code == 200
    assert response.templates[0].name == "kpi_modules/notes.html"

    data = {
        'id': kpi.id,
        'b3': 'True'
    }

    response = client_authed.get(link, data)
    assert response.status_code == 200
    assert response.templates[0].name == "kpi_modules/notes_3.html"

@pytest.mark.django_db(transaction=False)
def test_align_up_kpi(kpi, kpi_userA_manager, client_authed, organization, userA, userA_manager):

    link = reverse('align-up-kpi')
    response = client_authed.get(link)
    assert response.status_code == 200
    js = json.loads(response.content)
    assert js == []

    bsc_category = "financial"
    kpi.bsc_category = bsc_category
    kpi.quarter_period = organization.get_current_quarter()
    kpi.parent = None
    kpi.save()
    kpi_userA_manager.bsc_category = bsc_category
    kpi_userA_manager.quarter_period = organization.get_current_quarter()
    kpi_userA_manager.save()
    link = reverse('align-up-kpi') + "?user_id={}".format(userA.id) + "&bsc_category={}".format(bsc_category)
    response = client_authed.get(link)
    assert response.status_code == 200
    js = json.loads(response.content)
    assert len(js) == KPI.objects.filter(quarter_period=organization.get_current_quarter(),
                                      user_id=userA_manager.id,
                                      parent=None,
                                      bsc_category=bsc_category,
                                      weight__gt=0).count()+1


@pytest.mark.django_db(transaction=False)
def test_ecal(kpi, organization, userA, client_authed):
    kpi_unique_key = kpi.unique_key
    e = Ecal(kpi_unique_key=kpi_unique_key)
    e.save()
    link = reverse('ecal', kwargs={'kpi_unique_key': kpi_unique_key})
    response = client_authed.get(link)
    assert response.status_code == 200


@pytest.mark.django_db(transaction=False)
def test_load_comments(kpi, organization, client_authed):
    link = reverse('load-comments')
    data = {
        'kpi_id': kpi.id
    }
    response = client_authed.post(link, data)
    assert response.status_code == 200


@pytest.mark.django_db(transaction=False)
def test_logs(client_authed):
    link = reverse('logs')
    response = client_authed.get(link)
    assert response.status_code == 200

