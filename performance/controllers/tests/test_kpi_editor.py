# !/usr/bin/python
# -*- coding: utf-8 -*-
import json
import time
from random import randint

import selenium
from django.core.cache import cache

from performance.controllers.kpi_editor import fix_quarter, KPIWorkSheetEmployeeNew
from performance.models import KPI
import pytest
from django.core.urlresolvers import reverse
from django.core.cache import cache
from django.test.utils import override_settings
from selenium.webdriver.support.select import Select

# @pytest.mark.django_db(transaction=False)
# def test_KPIWorkSheetDetail(client_authed, kpi, userA):
#
#
# # test if  auto fix running or not
#     key= 'fix_kpi_self_' + str(kpi.id)
#     response = client_authed.get(reverse( 'KPIWorkSheetDetail', kwargs={'kpi_id': kpi.id}))
#     assert cache.get(key) is None
#     assert response.status_code == 200
#
#     response = client_authed.get(reverse( 'KPIWorkSheetDetail', kwargs={'kpi_id': kpi.id}) + "?follow_page=1")
#     assert response.status_code == 200
#     assert response.content == 200
#     assert cache.get(key)
#
#
from utils.common import get_redis
from selenium.common.exceptions import NoSuchElementException
from authorization.rules import has_perm, SETTINGS__VIEW_ORG_CHART
from utils.random_str import random_string


@pytest.mark.django_db(transaction=False)
def test_kpi_worksheet_detail_url(kpi, client_authed, organization, userA, userA_manager, client_authed_userB, userB):
    # DON'T DISABLE THIS TEST


    assert userA.profile.get_organization() == organization

    '''
    Test if user can go to kpi-editor page
    '''
    # response = client_authed_userB.get(reverse('kpi_dashboard_emp', kwargs={'year': 2015, 'user_id': userA.id}))
    response = client_authed.get(reverse('kpi_editor_emp', kwargs={'user_id': userA.id}))

    assert response.status_code == 200

    url = reverse('KPIWorkSheetDetail', kwargs={'kpi_id': kpi.id}) + "?target_org=" + str(randint(1, 1000))
    # print url
    response = client_authed.get(url)

    key = 'fix_kpi_self_' + str(kpi.id)

    assert response.status_code == 200

    # content = b''.join(response.streaming_content)

    # assert content.find(kpi.name) >=0 #return về phải có kpi.name
    # TODO: add this assert content.find(kpi.name) >= 0  # return về phải có kpi.name

    # test auto_fix for kpi_con
    assert kpi.refer_to_id is not None
    assert kpi.parent_id is not None

    # kpi.fix_self()
    # assert kpi.refer_to_id is not None #TODO: fix this

    '''
     GIVEN: create a bug for KPI
     THEN: load KPI it will fix the bug
    '''
    """
    Case for month_x_target is None
    """
    kpi.refer_to_id = None  # create bug to test
    kpi.save()

    kpi.month_1_target = None
    kpi.month_2_target = None
    kpi.month_3_target = None
    kpi.save(update_fields=['month_1_target', 'month_2_target', 'month_3_target'])

    # test if  auto fix running when reload=1
    url = reverse('KPIWorkSheetDetail', kwargs={'kpi_id': kpi.id}) + "?target_org="+str(organization.id)+"&follow_page=0&load_child=1&reload=1&level=1&has_child_loaded=0"

    response = client_authed.get(url)
    assert response.status_code == 200

    # test for case of kpi.owner_email != kpi.user.email ---> we can load the kpi with no error
    kpi.owner_email = kpi.user.email + '[{}]'.format(random_string())
    kpi.save(update_fields=['owner_email'])
    response = client_authed.get(url)
    assert response.status_code == 200
   # content = b''.join(response.streaming_content)  # need to streaming content to make this work

    time.sleep(20)  # wait for self_fix thread to be done

    # assert cache.get(key) is not  None
    # TODO: add this assert get_redis(key) is not None
    # assert cache.get('fix_kpi_expanding{}'.format(kpi.id)) == kpi.id
    assert int(get_redis('fix_kpi_expanding{}'.format(kpi.id))) == kpi.id

    # assert  KPI.objects.get(id =  kpi.id).refer_to_id is not None

    '''
    Test whether an ordinary user can view kpi editor or not
    '''

    org = userB.profile.get_organization()

    # response = client_authed_userB.get(reverse('kpi_dashboard_emp', kwargs={'year': 2015, 'user_id': userB.id}))
    response = client_authed_userB.get(reverse('kpi_editor_emp', kwargs={'user_id': userB.id}))

    assert response.status_code == 200

    # assert cache.get('follow_team_running') == 1
    assert int(get_redis('follow_team_running')) == 1

    # # case: userA has no permission to view kpi of userA_manager in Kpi-editor
    # # ==> userA still access self kpi in kpi-editor
    # # + To produce this case, set userA_manager.profile.organization to None
    pAm = userA_manager.profile
    pAm.organization = None
    pAm.save()

    # assert that userA has no permission view kpis of userA_manager
    perm = has_perm(SETTINGS__VIEW_ORG_CHART, userA, userA_manager)
    assert perm is False

    # assert userA can view it's kpi
    response = client_authed.get(reverse('kpi_editor_emp', kwargs={'user_id': userA.id}))
    assert response.status_code == 200


@pytest.mark.django_db(transaction=False)
def test_fix_quarter(organization, userA):
    @override_settings(
            CACHES={
                'default': {
                    'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
                    'LOCATION': 'unique-snowflake',
                }
            }
        )
    def test_runner():
        qp = organization.get_current_quarter()
        key = "fix_quarter{}".format(userA.id)
        cache.set(key, True, 60 * 60 * 24)
        fix_quarter(userA, qp)
        assert cache.get(key, None) is not None

    test_runner()


@pytest.mark.django_db(transaction=False)
def test_kpi_mindmappub(userA, client_authed, kpi, client_ceo, organization_standard, ceo):
    link = reverse('kpi_mindmappub',  kwargs={'user_id': userA.id})
    response = client_authed.get(link)
    assert response.status_code == 403
    link = reverse('kpi_mindmappub', kwargs={'user_id': ceo.id})
    response = client_ceo.get(link)

    assert ceo.profile.is_admin is True
    assert response.status_code == 200


@pytest.mark.django_db(transaction=False)
def test_kpi_worksheet_employee_new(userA, ceo, client_ceo, kpi, parent_kpi, organization, client_authed):
    """
    Case: post
    """
    link = reverse('kpi_editor_emp', kwargs={'user_id': ceo.id})
    data = {
        'parent_kpi': parent_kpi.id,
        'level': 1,
        'new_template': True,
        'category': 'financial',
    }
    response = client_ceo.post(link, data)
    assert response.status_code == 200
    js = json.loads(response.content)
    assert js['status'] == 'ok'


    """
    Case: Success
    """
    link = reverse('kpi_editor_emp',  kwargs={'user_id': userA.id})
    response = client_authed.get(link)
    assert response.status_code == 200

    """
    Case: profile.allow_public_view is False
    """
    user_id = userA.profile.get_hash()
    link = reverse('kpi_editor_emp', kwargs={'user_id': user_id})
    response = client_authed.get(link)
    assert response.status_code == 404

    """
    Case: profile.allow_public_view is False
    """
    pA = userA.profile
    pA.allow_public_view = True
    pA.save()

    link = reverse('kpi_editor_emp', kwargs={'user_id': user_id})
    response = client_authed.get(link)
    assert response.status_code == 200

    """
    Case: is_deleted
    """
    pA = userA.profile
    pA.allow_public_view = True
    pA.organization_id = None
    # profile.parent = None
    pA.is_deleted = True
    pA.save(update_fields=['organization_id', 'is_deleted', ])
    link = reverse('kpi_editor_emp', kwargs={'user_id': user_id})
    response = client_authed.get(link)
    assert response.status_code == 302


# @pytest.mark.django_db(transaction=False)
# def test_get_kpi_editor_permission(userA, kpi, userC, organization):
#     user_viewed = userA.profile
#     inst = KPIWorkSheetEmployeeNew()
#     result = inst.get_kpi_editor_permission(user_viewed, True, request_public=True)
#     assert result == True

@pytest.mark.django_db(transaction=False)
def test_kpi_worksheet_detail_url_onboard(kpi, client_authed, organization, userA, client_authed_userB, userB):

    p = userA.profile
    p.is_onboarded = False
    p.save()
    #print userA.profile.should_onboarding()

    # if False: #TODO: re-enable this test
    #     assert userA.profile.should_onboarding() == True
    #     response = client_authed.get(reverse('kpi_dashboard'))
    #
    #   #  print response
    #     assert response.status_code == 302
    #     assert response.url == reverse('onboarding')
    #
    #     response = client_authed.get(reverse('kpi_dashboard') + "?noonboard=true")
    #
    #     assert response.status_code == 200

#
# # Note: Hong disable to increase speed.
# @pytest.mark.django_db(transaction=False)
# def test_add_kpi(selenium_ceo_client, organization, live_server):
#
#
#     # Note: Hong disable to increase speed.
#     # TODO: enable
#     return
#
#     selenium_ceo_client.get(live_server.url + reverse('kpi_dashboard') + "?noonboard=true")
#     print selenium_ceo_client.current_url
#     time.sleep(2)
#     selenium_ceo_client.execute_script("$('body > div.introjs-tooltipReferenceLayer > div > div.introjs-tooltipbuttons > a.introjs-button.introjs-skipbutton').click()")
#     # skip_introjs = selenium_userA_client.find_element_by_css_selector('body > div.introjs-tooltipReferenceLayer > div > div.introjs-tooltipbuttons > a.introjs-button.introjs-skipbutton')
#     # skip_introjs.click()
#     # btn_add = selenium_userA_client.find_element_by_css_selector('#btn-add')
#     '''
#     Category
#     '''
#     BSC_CATEGORY = (
#         'financial',
#      # HONG note: only need test financial , to save t
#         #    'customer',
#      #   'internal',
#      #   'learninggrowth',
#      #   'other'
#     )
#     '''
#     Iterate BSC_CATEGORY and process add KPIs
#     '''
#     for GROUP in BSC_CATEGORY:
#         '''
#         Open modal add KPIs
#         '''
#         selenium_ceo_client.execute_script("$('#btn-add').click()")
#         time.sleep(1)
#         '''
#         Get select element
#         '''
#         select = Select(selenium_ceo_client.find_element_by_css_selector(
#             '#modal-categories > div > div > div.modal-body > select'))
#         print GROUP
#         '''
#         Add parent KPI
#         '''
#         select.select_by_value(GROUP)
#         selenium_ceo_client.execute_script("$('#btn-choose-category').click()")
#         time.sleep(4)
#         new_kpi = KPI.objects.all().order_by('id').last()
#         new_kpi_ui = selenium_ceo_client.find_element_by_css_selector('div[data-kpi-id="{}"]'.format(new_kpi.id))
#         assert (new_kpi_ui is not None)
#         '''
#         Add child KPI
#         '''
#         selenium_ceo_client.execute_script("$('#kpi-wrapper-{} > div.col-xs-10.col-table-10.kpi-detail > div > div.row.rows-table-sm.rows-table-xs > div.col.col-xs-5.col-table-5.kpi-name > div:nth-child(2) > span > span').click()".format(new_kpi.id))
#         selenium_ceo_client.execute_script("$('#kpi-wrapper-{} > div.col-xs-10.col-table-10.kpi-detail > div > div.row.rows-table-sm.rows-table-xs > div.col.col-xs-5.col-table-5.kpi-name > div:nth-child(2) > span > ul > li:nth-child(1) > a').click()".format(new_kpi.id))
#         time.sleep(6)
#         new_child_kpi = KPI.objects.all().order_by('id').last()
#         # new_child_kpi_ui = new_kpi_ui.find_element_by_css_selector('div[data-kpi-id="{}"]'.format(new_child_kpi.id))
#         new_child_kpi_ui = new_kpi_ui.find_element_by_css_selector('div[data-kpi-id="{}"]'.format(new_child_kpi.id))
#         assert(new_child_kpi_ui is not None)
#         '''
#         Delete child KPI
#         '''
#         kpi_id_temp = new_child_kpi.id
#         selenium_ceo_client.execute_script(
#             "$('#kpi-wrapper-{} > div.col-xs-10.col-table-10.kpi-detail > div > div.row.rows-table-sm.rows-table-xs > div.col.col-xs-5.col-table-5.kpi-name > div:nth-child(2) > span > span').click()".format(
#                 new_child_kpi.id))
#         selenium_ceo_client.execute_script(
#             "$('#kpi-wrapper-{} > div.col-xs-10.col-table-10.kpi-detail > div > div.row.rows-table-sm.rows-table-xs > div.col.col-xs-5.col-table-5.kpi-name > div:nth-child(2) > span > ul > li:nth-child(3) > a').click()".format(
#                 new_child_kpi.id))
#         time.sleep(2)
#         selenium_ceo_client.execute_script(
#             "$('body > div.sweet-alert.showSweetAlert.visible > div.sa-button-container > div > button').click()")
#         time.sleep(2)
#         selenium_ceo_client.execute_script(
#             "$('body > div.sweet-alert.showSweetAlert.visible > div.sa-button-container > div > button').click()")
#         time.sleep(3)
#
#         '''
#         TODO: Hong temporarily disable
#                 Khang to recheck and fix
#         '''
#
#         #
#         # assert (kpi_id_temp not in KPI.objects.all().values_list('id', flat=True))
#         # try:
#         #     new_child_kpi_ui = new_kpi_ui.find_element_by_css_selector('div[data-kpi-id="{}"]'.format(kpi_id_temp))
#         #     raise Exception('KPI chua duoc xoa' + ' id=' + kpi_id_temp)
#         #     assert False
#         # except:
#         #     print 'KPI not found, pass test.'
#         #     pass
#         #
#         #
#         '''
#         Delete parent KPI
#         '''
#         kpi_id_temp = new_kpi.id
#         selenium_ceo_client.execute_script(
#             "$('#kpi-wrapper-{} > div.col-xs-10.col-table-10.kpi-detail > div > div.row.rows-table-sm.rows-table-xs > div.col.col-xs-5.col-table-5.kpi-name > div:nth-child(2) > span > span').click()".format(
#                 new_kpi.id))
#         selenium_ceo_client.execute_script(
#             "$('#kpi-wrapper-{} > div.col-xs-10.col-table-10.kpi-detail > div > div.row.rows-table-sm.rows-table-xs > div.col.col-xs-5.col-table-5.kpi-name > div:nth-child(2) > span > ul > li:nth-child(3) > a').click()".format(
#                 new_kpi.id))
#         time.sleep(2)
#         selenium_ceo_client.execute_script(
#             "$('body > div.sweet-alert.showSweetAlert.visible > div.sa-button-container > div > button').click()")
#         time.sleep(2)
#         selenium_ceo_client.execute_script(
#             "$('body > div.sweet-alert.showSweetAlert.visible > div.sa-button-container > div > button').click()")
#         time.sleep(3)
#
#         '''
#         TODO: Hong temporarily disable
#                 Khang to recheck and fix
#         '''
#         #
#         # assert (kpi_id_temp not in KPI.objects.all().values_list('id', flat=True))
#         # try:
#         #     new_kpi_ui = selenium_ceo_client.find_element_by_css_selector('div[data-kpi-id="{}"]'.format(kpi_id_temp))
#         #     raise Exception('KPI chua duoc xoa' + ' id=' + kpi_id_temp)
#         #     assert False
#         # except:
#         #     print 'KPI not found, pass test'
#         #     pass
#
#
#
#
# @pytest.mark.django_db(transaction=False)
# def test_permission_view_deparment_strategy_map_button(selenium_ceo_client, ceo, organization, live_server, client_authed):
#     profile = ceo.profile
#     profile.is_onboarded = True
#     profile.save()
#     # Test hide department strategy map button
#     selenium_ceo_client.get(live_server.url + reverse("kpi_dashboard"))
#     with pytest.raises(NoSuchElementException) as e_info:
#         selenium_ceo_client.find_element_by_id("sub_strategy_map")
#
#     # Test access department strategy map with ceo id -> redirect to strategy mapp company
#     res = client_authed.get(reverse('sub_company_strategy_map_detail_v2', kwargs={'user_id': organization.ceo_id}))
#     assert res.status_code == 302


@pytest.mark.django_db(transaction=False)
def test_kpi_dashboard_search(userA, kpi, parent_kpi, organization, client_authed):
    link = reverse('kpi_editor_search') + "?q=" + random_string(15) + "&lang=en"
    result = client_authed.get(link)
    assert result.status_code == 200
