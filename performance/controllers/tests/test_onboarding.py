# !/usr/bin/python
# -*- coding: utf-8 -*-

import pytest
from django.core.urlresolvers import reverse


@pytest.mark.django_db(transaction=False)
def test_onboarding(client_authed):
    link = reverse('onboarding')
    response = client_authed.get(link)
    assert response.status_code == 302
    assert response.url == '/performance/onboarding/step-1/'


@pytest.mark.django_db(transaction=False)
def test_onboarding_step_1(client_authed):
    link = reverse('onboarding-step-1')
    response = client_authed.get(link)
    assert response.status_code == 200
    assert response.templates[0].name == 'performance/onboarding/onboarding-step-1.html'


@pytest.mark.django_db(transaction=False)
def test_onboarding_step_2(client_authed):
    link = reverse('onboarding-step-2')
    response = client_authed.get(link)
    assert response.status_code == 200
    assert response.templates[0].name == 'performance/onboarding/onboarding-step-2.html'


@pytest.mark.django_db(transaction=False)
def test_onboarding_step_3(client_authed):
    link = reverse('onboarding-step-3')
    response = client_authed.get(link)
    assert response.status_code == 200
    assert response.templates[0].name == 'performance/onboarding/onboarding-step-3.html'


@pytest.mark.django_db(transaction=False)
def test_onboarding_step_3_2(client_authed):
    link = reverse('onboarding-step-3-2')
    response = client_authed.get(link)
    assert response.status_code == 200
    assert response.templates[0].name == 'performance/onboarding/onboarding-step-3-2.html'


@pytest.mark.django_db(transaction=False)
def test_onboarding_step_4(client_authed):
    link = reverse('onboarding-step-4')
    response = client_authed.get(link)
    assert response.status_code == 200
    assert response.templates[0].name == 'performance/onboarding/onboarding-step-4.html'


@pytest.mark.django_db(transaction=False)
def test_onboarding_step_5(client_authed):
    link = reverse('onboarding-step-5')
    response = client_authed.get(link)
    assert response.status_code == 200
    assert response.templates[0].name == 'performance/onboarding/onboarding-step-5.html'


@pytest.mark.django_db(transaction=False)
def test_onboarding_step_6(client_authed):
    link = reverse('onboarding-step-6')
    response = client_authed.get(link)
    assert response.status_code == 200
    assert response.templates[0].name == 'performance/onboarding/onboarding-step-6.html'


@pytest.mark.django_db(transaction=False)
def test_onboarding_step_7(client_authed):
    link = reverse('onboarding-step-7')
    response = client_authed.get(link)
    assert response.status_code == 200
    assert response.templates[0].name == 'performance/onboarding/onboarding-step-7.html'
