# !/usr/bin/python
# -*- coding: utf-8 -*-
import json
import random

from django.contrib.auth.models import User
from django.core import mail
from django.core.cache import cache
from django.core.urlresolvers import reverse
import pytest
from django.test.utils import override_settings
from django.utils.translation import ugettext_lazy as _, ugettext

from authorization.models import RuleTable
from company.models import Position
from performance.db_models.kpi_library import JobTitle
from user_profile.models import Profile
from user_profile.services.profile import remove_user_from_organization
from utils import reload_model_obj
from utils.random_str import random_string
import datetime
import time

# Lam #2447 taiga


@pytest.mark.django_db(transaction=False)
def test_people_view(userA,
                     client_authed,
                     ceo,
                     client_ceo,
                     client_authed_userB,
                     organization,
                     userB,
                     userA_manager,
                     userA_manager_manager
                     ):
    link = reverse('people')
    """
    Case 1: request=post, user_id=None
    """
    response0 = client_authed.post(link)
    assert response0.status_code == 200
    assert response0.templates[0].name == '403.html'

    """
    Case 2: No organization
    """
    userA.organization = None
    userA.save()

    p = userA.profile
    p.organization = None
    p.save()
    response = client_authed.post(link)
    assert response.status_code == 200
    assert response.templates[0].name == '403.html'

    """
    Case 3: is_deleted=True
    """
    q = organization.get_current_quarter()
    remove_user_from_organization(userB.profile, q)
    link = reverse('people') + "?user_id=" + format(userB.id)
    p = userB.profile
    p.organization = organization
    p.save()
    response1 = client_authed_userB.get(link)
    assert response1.status_code == 302

    """
    Case 4: ceo login-->is_super_user
    """
    link = reverse('people')
    data = {"user_id": ceo.id, "username": ceo.username, "is_checked": "True"}
    response3 = client_ceo.post(link, data)
    assert response3.status_code == 200
    assert response3.content == "ok"


@pytest.mark.django_db(transaction=False)
def test_people_view_1(userA,
                       ceo,
                       client_ceo,
                       organization,
                       userA_manager,
                       userA_manager_manager
                       ):
    """
        test email existed in case have profile_id
    """
    link = reverse('people')
    data = {"user_id": ceo.id,
            "username": ceo.username,
            "id": 'u{}'.format(ceo.profile.id),
            "email": userA.email
            }
    response = client_ceo.post(link, data)
    assert response.content == ugettext("Email is existed") + ' ' + userA.email

    """
            case have profile_id, username existed
    """
    u = User(username="nameofuser", email='emailofnewuser@gmail.com')
    u.save()
    u1 = User(username="nameofuser1", email='emailofnewuser11@gmail.com')
    u1.save()
    job1 = JobTitle(name="Job 1")
    job1.save()
    userA_manager.profile.current_job_title = job1
    userA_manager.profile.current_job_title_id = job1.id
    userA_manager.save()
    job2 = JobTitle(name="Job 2")
    job2.save()
    link = reverse('people')
    data = {"user_id": userA_manager.id,
            "username": userA_manager.username,
            "id": 'u{}'.format(userA_manager.profile.id),
            "email": "exampleemail@example.com",
            "name": 'nameofuser',
            "position": random_string(),
            "manager": 'u{}'.format(ceo.profile.id),
            "job_title_id": job2.id
            }
    response = client_ceo.post(link, data)
    assert response.status_code == 200
    result = json.loads(response.content)
    assert result['changed_manager'] == True
    assert result['job_title'] == "Job 2"

    '''
        test for exception
    '''
    data = {"user_id": userA_manager.id,
            "username": userA_manager.username,
            "id": userA_manager.id,
            }
    response = client_ceo.post(link, data)
    assert response.content == "Profile matching query does not exist."


@pytest.mark.django_db(transaction=False)
def test_people_view_2(userA,
                       ceo,
                       client_ceo,
                       organization,
                       userA_manager,
                       userA_manager_manager,
                       client_userA_manager
                       ):
    link = reverse('people')
    response = client_ceo.get(link)
    assert response.status_code == 200

    rule_1 = RuleTable(actor=userA_manager, target=userA_manager_manager, roles='role1')
    rule_1.save()
    rule_2 = RuleTable(actor=userA_manager, target=userA, roles='role2')
    rule_2.save()
    link = reverse('people')
    response = client_userA_manager.get(link)
    assert response.status_code == 200

@pytest.mark.django_db(transaction=False)
def test_people_view_3(ceo,
                       client_ceo,
                       organization,
                       ):
    from io import BytesIO
   # from PIL import Image
    file = BytesIO()
    #image = Image.new('RGBA', size=(50, 50), color=(155, 0, 0))
   # image.save(file, 'png')
    #Hong note: we do not need to write image here since it's hard to make test env working
    file.name = 'test.png'
    file.seek(0)

    link = reverse('people')
    data = {
        'file': file,
        'user_id': ceo.id
    }
    response = client_ceo.post(link, data)
    assert response.status_code == 200
    data = {
        'file': file
    }
    response = client_ceo.post(link, data)
    assert response.status_code == 403


@pytest.mark.django_db(transaction=False)
def test_email(client_authed, client_authed_userB, organization, userA, userB, userA_manager):
    default_password = random_string()
    organization.default_password = default_password
    organization.save()
    data = {'name': 'test',
            'email': 't@gami.com',
            'manager': 'u{}'.format(userA.profile.id),
            'send_pass': 'true'
            }
    url = reverse('add_people')
    client_authed.post(url, data)
    assert len(mail.outbox) >= 1
    # print organization.default_password
    assert mail.outbox[-1].body.find(default_password) > 0

    a = client_authed_userB.get(reverse('people'))

    assert a.templates[0].name == '403.html'
    assert client_authed.get(reverse('people') + "?user_id={}".format(userA_manager.id)).templates[0].name == '403.html'
    assert client_authed.get(reverse('people') + "?user_id={}".format(userA_manager.id)).templates[0].name == '403.html'


@pytest.mark.django_db(transaction=False)
def test_group_user_reset_password(userA, client_authed, organization, client_userA_manager, userA_manager, userB):
    link = reverse('group-reset_password')

    data = {'user_id': userB.id, 'email': userB.email, 'username': userB.username}
    response = client_authed.post(link, data)
    assert response.status_code == 200
    assert response.content == "No members in group"

    data = {'user_id': userA_manager.id, 'email': userA_manager.email, 'username': userA_manager.username}
    response = client_userA_manager.post(link, data)
    assert response.status_code == 200
    assert response.content == "ok"

    data1 = {'user_id': userA.id}
    response1 = client_authed.post(link, data1)
    assert response1.status_code == 200
    assert response1.content == "error"

    data2 = {'user_id': userA.id, 'username': userA.username, 'email': random_string()}
    response2 = client_authed.post(link, data2)
    assert response2.status_code == 200
    assert response2.content == "error"

    p = userB.profile
    p.parent = userA_manager.profile
    p.save()
    data = {'user_id': userA_manager.id, 'email': userA_manager.email, 'username': userA_manager.username}
    response = client_authed.post(link, data)
    assert response.status_code == 200
    assert response.content == "error"


@pytest.mark.django_db(transaction=False)
def test_people_node(userA, client_authed, client_userA_manager, userA_manager, organization):
    link = reverse('people_node') + "?lineup=" + format(userA_manager.id)

    response = client_userA_manager.get(link)
    assert response.status_code == 200

    link = reverse('people_node') + "?node_id=" + 'u{}'.format(userA_manager.profile.id)
    response = client_userA_manager.get(link)
    result = json.loads(response.content)
    assert response.status_code == 200
    f = False
    for x in result['children']:
        if x['data']['user_id'] == userA.id:
            f = True
    assert f == True

    link = reverse('people_node')
    response = client_userA_manager.get(link)
    assert response.content == "Invalid"

@pytest.mark.django_db(transaction=False)
def test_position_node(client_authed, organization):
    position = Position(name="Pos 1", organization=organization)
    position.save()
    link = reverse('position_node') + "?lineup=" + format(position.id)

    response = client_authed.get(link)
    assert response.status_code == 200

    link = reverse('position_node') + "?node_id=" + '{}'.format(position.id)
    response = client_authed.get(link)
    result = json.loads(response.content)
    assert response.status_code == 200

    link = reverse('people_node')
    response = client_authed.get(link)
    assert response.content == "Invalid"

@pytest.mark.django_db(transaction=False)
def test_move_to_exist_position(userA, client_userA_manager, userA_manager_manager, userA_manager, organization):
    """

    :type userA_manager: object
    """
    link = reverse('move-to-exist-position')
    p = userA_manager.profile
    p.parent = userA_manager_manager.profile
    p.save()

    data = {'user_id': random.randint(1, 100000), 'keep_kpi_history': 'True', 'to_user': userA.id}
    if data['user_id'] != userA_manager.id:
        response = client_userA_manager.post(link, data)
        assert response.status_code == 403
        js = json.loads(response.content)
        #assert js['message'] == ""

    data = {'user_id': userA_manager.id, 'keep_kpi_history': 'True', 'to_user': userA.id}
    response = client_userA_manager.post(link, data)
    assert response.status_code == 200
    js = json.loads(response.content)
    assert js['new_manager'] == p.display_name

    response = client_userA_manager.post(link)
    assert response.status_code == 400
    js = json.loads(response.content)
    assert js['message'] == "Bad Request"

    '''
        test GET
    '''
    data = {
        'user_id': userA.id,
        'req': "",
    }
    response = client_userA_manager.get(link, data)
    assert response.status_code == 200
    result = json.loads(response.content)
    assert result['data']['username'] == userA.username

    response = client_userA_manager.get(link)
    assert response.status_code == 200


@pytest.mark.django_db(transaction=False)
def test_add_emp(organization, client_admin, userA, client_authed, userA_manager):
    link = reverse('add_people')

    start_date = datetime.date.today()
    role_start = datetime.date.today()
    phone = random.randint(1, 10000)
    pA = userA.profile

    """
        status 200 for name have length < 30
    """
    data = {
        'name': random_string(),
        'email': '33333333@wnd.xnb',
        'manager': 'u{}'.format(pA.id),
        'phone': str(phone),
        'position': random_string(5),
        'employee_code': None,
        'skype': None,
        'unit_code': None,
        'send_pass': False,
        'department': None,
        'start_date': start_date,
        'role_start': role_start,
    }
    response = client_admin.post(link, data)
    assert response.status_code == 200

    """
        status 200 for name have length > 30
    """
    data = {
        'name': random_string(100),
        'email': '222222@csa.xnb',
        'manager': 'u{}'.format(pA.id),
        'phone': str(phone),
        'position': random_string(5),
        'employee_code': None,
        'skype': None,
        'unit_code': None,
        'send_pass': False,
        'department': None,
        'start_date': start_date,
        'role_start': role_start,
    }
    response = client_admin.post(link, data)
    assert response.status_code == 200

    """
        status 400 for empty name and empty email
    """
    data = {
        'manager': 'u{}'.format(pA.id),
        'phone': str(phone),
        'position': random_string(5),
        'employee_code': None,
        'skype': None,
        'unit_code': None,
        'send_pass': False,
        'department': None,
        'start_date': start_date,
        'role_start': role_start,
    }
    response = client_admin.post(link, data)
    assert response.status_code == 400

    """
        test serializer
    """
    data = {
        'name': random_string(201),
        'email': '1111wss@wnd.xnb',
        'manager': 'u{}'.format(pA.id),
        'phone': random_string(101, alphabet="+0123456789"),
        'position': random_string(256),
        'employee_code': random_string(101),
        'skype': random_string(101),
        'unit_code': None,
        'send_pass': False,
        'department': random_string(301),

    }
    response = client_admin.post(link, data)
    assert response.status_code == 400

    '''
        test email existed
    '''
    data = {
        'name': random_string(15),
        'email': userA.email,
        'manager': 'u{}'.format(pA.id),
    }
    response = client_admin.post(link, data)
    assert response.status_code == 200
    assert response.content == _("Email existed")



    '''
        test email existed but not in profile
    '''
    data = {
        'name': 'this is user X',
        'email': '1234@gmailhihi.com',
        'manager': 'u{}'.format(pA.id),
    }
    client_admin.post(link, data)
    Profile.objects.filter(user__email='1234@gmailhihi.com').delete()
    x = JobTitle(name="Job 1")
    x.save()
    data = {
        'name': 'this is user X',
        'email': '1234@gmailhihi.com',
        'manager': 'u{}'.format(pA.id),
        'job_title_id': x.id,
    }
    response = client_admin.post(link, data)
    result = json.loads(response.content)
    u = User.objects.get(email="1234@gmailhihi.com")
    assert u.email == "1234@gmailhihi.com"
    assert response.status_code == 200



    '''
        test add new employee with existing email in system, but the email has profile which does not belong to any organization
        ==> should be success! 
    '''
    link = reverse('add_people')
    new_random_email='{}@demo.cjs.vn'.format(random_string()).lower()
    # create new User and profile for that user
    new_u=User.objects.create(username=random_string(), email=new_random_email)
    new_p=new_u.profile
    assert new_p.organization  is  None

    # now add new employee with that email
    data = {
        'name': random_string(),
        'email': new_random_email,
        'manager': 'u{}'.format(pA.id),
        'position': 'test position'
    }

    response = client_admin.post(link, data)
    assert response.status_code == 200
    # also assert that new employee profile is the existing user profile
    new_p=reload_model_obj(new_p)
    assert new_p.parent==userA.profile



    '''
        test username existed
    '''
    data = {
        'name': 'username',
        'email': '1234@sfsdfgsv.com',
        'manager': 'u{}'.format(pA.id),
    }
    client_admin.post(link, data)
    data = {
        'name': 'username1',
        'email': '1234555@gmaisdlhihi.com',
        'manager': 'u{}'.format(pA.id),
    }
    client_admin.post(link, data)
    data = {
        'name': 'username',
        'email': '12345655@hidfhi.com',
        'manager': 'u{}'.format(pA.id),
    }
    client_admin.post(link, data)
    assert response.status_code == 200




@pytest.mark.django_db(transaction=False)
def test_Add_Member(organization, userA, client_authed, userA_manager):
    """
    Test for class AddMember
    :organization organization:
    :user userA:
    :client client_authed:
    """

    """
    Case 1: Status 200 : Successfully added member
    """
    name = random_string()
    position = random_string()
    data = {u'unit_code': [u''], u'employee_code': [u''], u'report_to': str(userA.id), u'phone': [u''],
            u'full_name': 'u{}'.format(name), u'skype': [u''], u'position': 'u{}'.format(position),
            u'send_password': [u'on'],
            u'email': [u'wss@wnd.xnb']}

    url = reverse('add_member') + "?teamview=true"
    response = client_authed.post(url, data)
    assert response.status_code == 200

    """
    Case 2: Status 403 : Invalid Form Data
    """
    invaliddata = {u'unit_code': [u''], u'employee_code': [u''], u'report_to': str(userA.id), u'phone': [u''],
                   u'full_name': 'u{}'.format(name), u'skype': [u''], u'position': 'u{}'.format(position),
                   u'send_password': [u'on']}
    url = reverse('add_member') + "?teamview=true"
    response = client_authed.post(url, invaliddata)
    assert response.status_code == 403

    """
    Case 3: Status 403 : Invalid manager - report_to ID invalid
    """
    name = random_string()
    position = random_string()
    data = {u'unit_code': [u''], u'employee_code': [u''], u'report_to': random_string(), u'phone': [u''],
            u'full_name': 'u{}'.format(name), u'skype': [u''], u'position': 'u{}'.format(position),
            u'send_password': [u'on'],
            u'email': [u'wss@wnd.xnb']}

    url = reverse('add_member') + "?teamview=true"
    response = client_authed.post(url, data)
    assert response.status_code == 403

    u = User(username="nameofuser", email='emailofnewuser@gmail.com')
    u.save()
    u1 = User(username="nameofuser1", email='emailofnewuser11@gmail.com')
    u1.save()
    position = random_string()
    email = random_string(l=10) + "@example.com"
    data = {u'unit_code': [u''], u'employee_code': [u'zxcsaq'], u'report_to': str(userA.id), u'phone': [u''],
            u'full_name': u.username, u'skype': [u''], u'position': 'u{}'.format(position),
            u'department': [u'BLAAA'],
            u'send_password': [u'on'],
            u'email': email}
    url = reverse('add_member')
    response = client_authed.post(url, data)
    assert response.status_code == 200
    result = json.loads(response.content)
    assert result['name'] == u.username
    assert result['email'] == email.lower()


@pytest.mark.django_db(transaction=False)
def test_active_user1(organization, userA, userA_manager, client_admin):
    link = reverse('people')

    active = 'active'

    '''
        Case 1 test status 200
    '''

    # super_user active userA

    pA = userA.profile
    pA.active = False
    pA.save()

    response = client_admin.post(link, {'active': active, 'user_id': userA.id})
    assert response.status_code == 200
    data = json.loads(response.content)
    assert data['active'] == True

    # super_user active userA_manager
    pA_manager = userA_manager.profile
    pA_manager.active = False
    pA_manager.save()
    response = client_admin.post(link, {'active': active, 'user_id': userA_manager.id})
    assert response.status_code == 200


@pytest.mark.django_db(transaction=False)
def test_active_user2(organization, userA, userB, userA_manager, client_authed):
    link = reverse('people')

    active = 'active'

    '''
        test case userA status 403
    '''

    # userA active userA
    response = client_authed.post(link, {'active': active, 'user_id': userA.id})
    assert response.status_code == 403
    # assert response.templates[0].name == '403.html'

    # userA active userA_manager
    response = client_authed.post(link, {'active': active, 'user_id': userA_manager.id})
    # assert response.status_code == 403
    assert response.templates[0].name == '403.html'

    # userA active userB
    response = client_authed.post(link, {'active': active, 'user_id': userB.id})
    # assert response.status_code == 403
    assert response.templates[0].name == '403.html'


@pytest.mark.django_db(transaction=False)
def test_active_user3(organization, userA, userB, userA_manager, client_userA_manager):
    link = reverse('people')

    active = 'active'
    '''
        test case 3 userA_manager
    '''
    # userA_manager active userA
    response = client_userA_manager.post(link, {'active': active, 'user_id': userA.id})
    assert response.status_code == 200

    # userA_manager active userA_manager
    response = client_userA_manager.post(link, {'active': active, 'user_id': userA_manager.id})
    assert response.status_code == 403
    # assert response.templates[0].name == '403.html'

    # userA_manager active userB
    response = client_userA_manager.post(link, {'active': active, 'user_id': userB.id})
    # assert response.status_code == 403
    assert response.templates[0].name == '403.html'


@pytest.mark.django_db(transaction=False)
def test_people_json(userA, client_authed):
    link = reverse('people_json')
    data = {
        'user_id': userA.id
    }
    response = client_authed.post(link, data)
    result = json.loads(response.content)
    found = False
    for item in result:
        if item['user_id'] == userA.id:
            found = True
            break
    assert found == True
    response = client_authed.get(link)
    result = json.loads(response.content)
    assert result['status'] == "Bad Request"


@pytest.mark.django_db(transaction=False)
def test_team(client_admin, userA):
    link = reverse('team_view')
    response = client_admin.get(link)
    assert response.status_code == 200


@pytest.mark.django_db(transaction=False)
def test_creating_assessment(organization, userAdmin, client_admin):
    link = reverse('creating_assessment')
    response = client_admin.get(link)
    assert response.status_code == 200


@pytest.mark.django_db(transaction=False)
def test_position_chart(userA, client_authed, client_ceo, ceo, organization):
    """
    Redirected as no permission
    """
    link = reverse('position-chart')
    result = client_authed.get(link)
    assert result.status_code == 200
    assert result.templates[0].name == '403.html'

    """
    Successful as ceo has permission
    """
    result = client_ceo.get(link)
    assert result.status_code == 200
    assert result.templates[0].name == 'performance/people_v5.html'

    """
    Successful as ceo has permission & user_id given
    """
    link = link + "?user_id=" + str(ceo.id)
    result = client_ceo.get(link)
    assert result.status_code == 200
    assert result.templates[0].name == 'performance/people_v5.html'

    """
    Exception: Profile.DoesNotExist->pass   #confirm case with Mr. Duan/ Mr. Phi
    """

    ceo.profile.is_deleted = True
    ceo.profile.save(update_fields=['is_deleted'])

    link = reverse('position-chart')
    result = client_ceo.get(link)
    assert result.templates[0].name == 'performance/people_v5.html'

# check following url with Mr. Phi
# @pytest.mark.django_db(transaction=False)
# def test_position_chart_node(client_authed, client_ceo, ceo, organization, userA):
#     link = reverse('position-chart-node') + "?node_id=" + 'u{}'.format(ceo.profile.id)
#     response = client_ceo.get(link)
#     js = json.loads(response.content)
#     assert js['children'] is not None