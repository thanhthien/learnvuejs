import pytest
from django.core.urlresolvers import reverse
import datetime
from company.models import Organization
import json

from performance.db_models.strategy_map import StrategyMap, GroupKPI
from performance.models import KpiComment
import time
import logging
from performance.cloner import start_clone_strategy_map

from elms.settings_test import CELERY_RESULT_BACKEND


logger = logging.getLogger()
@pytest.mark.django_db(transaction=False)
def test_get_order_quarter(client_authed, organization, userA):
    current_quarter = organization.get_current_quarter()
    response = client_authed.get(reverse('kpi_dashboard') + "?noonboard=true&quarter_id=" + str(current_quarter.id))
    assert response.status_code == 200



def wait_for_creating_new_assessment(organization):
    # wait for create new assessment
    # time.sleep(20)
    count_time = 0
    # while system not ready, continue to wait
    # to prevent the worst case of infinity loop, we only polling in specified time interval
    while organization.is_ready() is False:
        logger.info('wait for create new assessment in test_start_clone_strategy_map')
        time.sleep(3)
        count_time = count_time + 1
        if count_time > 10:
            break

    return organization.is_ready()

def create_strategymap_data(organization, quarter_period, kpi):

    strategymap = StrategyMap.objects.filter(
        organization=organization,
        director=kpi.user,
        quarter_period=quarter_period).first()

    if not strategymap:

        strategymap = StrategyMap(
            organization=organization,
            # year=2017,
            director=kpi.user,
            name='test map',
            # description=random_string(),
            quarter_period=quarter_period,
        )
        strategymap.save()

    group_kpi = GroupKPI(
        map=strategymap,
        name='test group'
    )

    group_kpi.save()

    kpi.group_kpi = group_kpi
    kpi.quarter_period = quarter_period
    kpi.save()

    # k = KPI()
    # k.user = userA
    # k.quarter_period = organization.get_current_quarter()
    # k.name = "Copied kpi"
    # k.group_kpi = group_kpi
    # k.copy_from = kpi
    # k.save()

    # groups = [group_kpi]
    # count = GroupKPI.objects.count()
    # clone_group(groups, strategymap, organization.get_current_quarter())


def create_new_assessment_for_testing(organization, client_admin, kpi):
    current_quarter = organization.get_current_quarter()
    create_strategymap_data(organization, current_quarter, kpi)
    prev_due_date, due_date = Organization.generate_due_date(prev_quarter=current_quarter)

    quarter_name = 'Testing quarter for creating new assessment'
    quarter = 4 #(due_date.month / 3 + 1) --> temporarily set to 4 since due_date.month / 3 + 1 can be = 5
    year = due_date.year

    ''' Sample data:
        due_date: 01/06/2018
        organization: 1
        year: 2018
        quarter: 2
        quarter_name: dot danh gia thu nghiem
    '''

    data = {'due_date': due_date.strftime('%d/%m/%Y'),
            'organization': organization.id,
            'year': year,
            'quarter': quarter,
            'quarter_name': quarter_name,
            }

    link = reverse('schedule')
    # https://groups.google.com/forum/#!topic/django-users/02wV1mpLH4A
    response = client_admin.post(link, data)
    # response = client_admin.post(link, data, content_type='text/html')


    return response


@pytest.mark.django_db(transaction=True)
def test_schedule_view_v2(organization, kpi, client_admin, client_authed, userA, ceo,   celery_app):
    # test create new assessment by submit (POST) due_date
    # since update_last_copy_time() function keep creating_assessment status for interval of approx. 5 minutes,
    # we can create new assessment & test something while the assessment creating
    orig_quarter=organization.get_current_quarter()
    response=create_new_assessment_for_testing(organization, client_admin, kpi)


    assert response.status_code==302
    # wait for thread start for creating new assessment
    # time.sleep(10)

    #TODO: need documentation for different settings_test.py
    assert organization.is_ready() is False

    def count_kpi_comments():
        kc = KpiComment.objects.filter(user=userA,
                        kpi_unique_key=kpi.unique_key)
        return kc.count()

    # when creating new assessment, make sure user cannot do anything by request to any URL
    # we try to post a comment on kpi `kpi`
    # we will test this action is not successful & user will be redirected to other page

    # count number of comments of the kpi first
    pre_count=count_kpi_comments()

    unique_key=kpi.unique_key
    sample_api_link=reverse('api_v2.kpi_comment', kwargs={'unique_key':unique_key})
    data = {'content': 'example comment on the kpi'}
    r1=client_authed.post(sample_api_link, data=json.dumps(data), content_type="application/json")

    # make sure user will be redirect to creating_assessment page
    assert r1.status_code == 302
    # make sure the user have no change on number of comments on the kpi
    post_count=count_kpi_comments()
    assert pre_count == post_count
    # assert False

    logger.info('r1: {}'.format(r1))


    is_ready=wait_for_creating_new_assessment(organization)

    assert is_ready is True


    pass

@pytest.mark.django_db(transaction=True)
def test_generate_due_date(organization):
    current_quarter = organization.get_current_quarter()

    # test generate due_date with prev_quarter argument
    prev_due_date, due_date = Organization.generate_due_date(prev_quarter=current_quarter)
    assert isinstance(prev_due_date, datetime.date)
    assert isinstance(due_date, datetime.date)
    assert due_date > prev_due_date

    # test generate due_date WITHOUT prev_quarter argument
    prev_due_date, due_date = Organization.generate_due_date()
    assert isinstance(prev_due_date, datetime.date)
    assert isinstance(due_date, datetime.date)
    # when not pass prev_quarter, the prev_due_date should be current date
    assert prev_due_date == datetime.date.today()
    assert due_date > prev_due_date


# @pytest.mark.celery(result_backend=CELERY_RESULT_BACKEND)
@pytest.mark.django_db(transaction=True)
def test_start_clone_strategy_map(organization, client_admin, kpi, celery_app):
    orig_quarter = organization.get_current_quarter()
    # todo: need create map for the orig_quarter first

    response=create_new_assessment_for_testing(organization, client_admin, kpi)

    # wait for create new assessment
    wait_for_creating_new_assessment(organization)

    organization.delete_current_quarter_cache()
    current_quarter = organization.get_current_quarter()
    # now, the assessment already created successfully!
    assert current_quarter.id != orig_quarter.id
    assert current_quarter.is_ready is True
    assert organization.is_ready() is True


    # comment out because not clear why delete this ?
    StrategyMap.objects.filter(organization=organization, quarter_period=current_quarter).delete()

    old_map = StrategyMap.objects.filter(organization=organization, director = kpi.user,
                                      quarter_period=orig_quarter).first()

    assert old_map is not None
    assert old_map.director == kpi.user

    new_map = StrategyMap.objects.filter(organization=organization,
                                                         director=old_map.director,
                                                         quarter_period=current_quarter)

    assert new_map.count() == 0


    '''
    TODO re-enable this --> done
    '''
    start_clone_strategy_map(s_map=old_map, organization=organization, quarter_period=current_quarter)

    assert StrategyMap.objects.filter(organization=organization, quarter_period=current_quarter, director=old_map.director).exists() is True

