# !/usr/bin/python
# -*- coding: utf-8 -*-
import pytest
from django.core.urlresolvers import reverse

from performance.db_models.strategy_map import StrategyMap


@pytest.mark.django_db(transaction=False)
def test_PublicStrategyMap(userA, kpi, client_authed, organization, userA_manager):

    current_quarter = organization.get_current_quarter()

    smap3 = StrategyMap()
    smap3.organization = organization
    smap3.director = userA
    smap3.name = "TEST"
    smap3.quarter_period = current_quarter
    smap3.save()

    hashorg = organization.get_hash

    """
    Case 1: 404, no org hash
    """
    link = reverse('strategy_map_public_by_hash', kwargs={'org_hash': None, 'smap_hash': smap3.get_hash()})
    result = client_authed.get(link)
    assert result.status_code == 404

    """
    Case 2: smap exists, 
    """
    link = reverse('strategy_map_public_by_hash', kwargs={'org_hash': hashorg, 'smap_hash': smap3.get_hash()})
    result = client_authed.get(link)
    assert result.status_code == 200
    assert 'performance/bsc/map/public_strategy_map.html' in result.templates[0].name

#
#
# # TODO: fix this test
# def test_strategymap(selenium_ceo_client, organization, live_server):
#     # Note: Hong disable to increase speed.
#     # TODO: enable
#     return
#     selenium_ceo_client.get(live_server.url + reverse("company_strategy_map_detail"))
#     # Wait for browser load page successfully
#     time.sleep(3)
#     BSC_CATEGORY = (
#         'financial',
#         'customer',
#         'internal',
#         'learninggrowth'
#     )
#     for GROUP in BSC_CATEGORY:
#         # Test add KPI and add Group
#
#
#         """
#         Test add KPI
#         """
#         # Click button to add KPI with category
#         selenium_ceo_client.find_element_by_id('btn-add-kpi-{}'.format(GROUP)).click()
#         # Wait for client posting data to server
#         time.sleep(3)
#         # Get lastest KPI
#         kpi = KPI.objects.all().order_by("id").last()
#         # Wait for client reloading page successfully
#         time.sleep(10)
#         # Get new kpi if it exist in front end
#         kpi_ui = selenium_ceo_client.find_element_by_id('id-kpi-{}'.format(kpi.id))
#         # Test if new element was created.
#         assert kpi_ui is not None
#
#         """
#         Test add Group
#         """
#         # Click button to add Group with category
#         selenium_ceo_client.find_element_by_id('btn-add-group-{}'.format(GROUP)).click()
#         # Wait for client posting data to server
#         time.sleep(3)
#         # Get lastest GroupKPI
#         group = GroupKPI.objects.all().order_by("id").last()
#         # Wait for client reloading page successfully
#         time.sleep(10)
#         # Get new group if it exist in front end
#         group_ui = selenium_ceo_client.find_element_by_id('group-{}'.format(group.id))
#         # Test if new element was created.
#         assert group_ui is not None
#
#         """
#         Test move KPI into another Group
#         """
#         # Use ActionChains to stimulate drag and drop in front end.
#         actions = ActionChains(selenium_ceo_client)
#         # Find elements again, if not, cause "Element not found in the cache - perhaps the page has changed since it was looked up"
#         # PhantomJS will not raise this error so make the test fail
#         kpi_ui = selenium_ceo_client.find_element_by_id('id-kpi-{}'.format(kpi.id))
#         group_ui = selenium_ceo_client.find_element_by_id('group-{}'.format(group.id))
#         # Move mouse to kpi
#         actions.move_to_element(kpi_ui).click_and_hold(kpi_ui).move_to_element(group_ui).release().perform()
#         # Drag kpi into group kpi
#         actions.drag_and_drop(kpi_ui, group_ui).perform()
#         # Wait for server update data when change KPI's group
#         time.sleep(3)
#         # Test if new group had the kpi
#         assert kpi.id in group.get_kpis().values_list('id', flat=True)
#
#         """
#         Test delete group
#         """
#         # Alert(driver).accept() not work in PhantomJS so we follow below tutorial
#         # Excute script to set window.confirm = <your option>; before the alert dialog is launched.
#         option = 'true'  # 'true' if you want to accept the prompt
#         selenium_ceo_client.execute_script("window.confirm = function(){return %s;}" % option)
#         # Store the ui id of the group
#         group_ui_id = group.id
#         # Fire remove group event
#         selenium_ceo_client.find_element_by_css_selector('#group-{}'.format(group.id) + ' ' + 'a.remove-btn').click()
#         # Wait for the page is loaded successfully
#         time.sleep(10)
#         # Get group
#         group = GroupKPI.objects.all().order_by("id").last()
#         # Assert group is none (GroupKPI is not permanent model)
#         assert group is None
#         # If we still find group in UI so the test failed
#         try:
#             if selenium_ceo_client.find_element_by_id('group-{}'.format(group_ui_id)) is not None:
#                 raise Exception("Element is not deleted")
#         except NoSuchElementException:
#             print "Pass test"
#             pass
#         # No need to check KPI's group because we get KPI from GroupKPI
#
#
#
#
#         """
#         Test delete kpi
#         """
#         # Alert(driver).accept() not work in PhantomJS so we follow below tutorial
#         # Excute script to set window.confirm = <your option>; before the alert dialog is launched.
#         option = 'true'  # True if you want to accept the prompt
#         selenium_ceo_client.execute_script("window.confirm = function(){return %s;}" % option)
#         # Fire remove kpi event
#         selenium_ceo_client.find_element_by_css_selector('#id-kpi-{}'.format(kpi.id) + ' ' + 'a.remove-kpi').click()
#         # Wait for the page is loaded successfully
#         time.sleep(10)
#         # kpi is permanent model, so can't assert kpi is None after deleting. kpi_query will return None if
#         # kpi was deleted.
#         query_kpi = KPI.objects.filter(id=kpi.id)
#         # Assert query_kpi is empty
#
#         '''
#         TODO: Hong temporarily disable
#                 Khang to recheck and fix
#         '''
#
#         assert not query_kpi
#         # If we still find kpi in UI so the test failed
#         try:
#             if selenium_ceo_client.find_element_by_id('id-kpi-{}'.format(kpi.id)) is not None:
#                 raise Exception("Element is not deleted")
#         except NoSuchElementException:
#             print "Pass test"
#             pass
