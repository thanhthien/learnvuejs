import StringIO
import datetime

import os
from PyPDF2.merger import PdfFileMerger
from django.conf import settings
from django.http.response import HttpResponse
from django.template.loader import get_template
from django.utils.dateformat import format
from performance.models import KPI
from user_profile.templatetags.user_tags import display_name
from xhtml2pdf import pisa


def construct_kpi_row(kpi, is_parent=False, is_double_child=False):
    def _value_to_display(value):
        return value if value is not None else ''

    score = kpi.get_score()#kpi.user_id)
    if score:
        score = score
    else:
        score = ''

    row = ['',

           kpi.id]

    kpi_ordering_str = str(kpi.get_prefix_category())
    # if kpi.ordering:
    #     kpi_ordering_str = kpi_ordering_str + str(int(kpi.ordering))

    kpi_ordering_str += kpi.get_kpi_id()
    row.append(kpi_ordering_str)

    if is_parent:

        row.append(kpi.get_name())
    else:
        # row.append('')
        if is_double_child:
            row.append("          " + kpi.get_name())
        else:
            row.append("     " + kpi.get_name())

    #round decimal 2
    if kpi.month_1_target!='' and isinstance(kpi.month_1_target, float):
        kpi.month_1_target=round(kpi.month_1_target,2)
    if kpi.month_2_target!='' and isinstance(kpi.month_2_target, float):
        kpi.month_2_target=round(kpi.month_2_target,2)
    if kpi.month_3_target!='' and isinstance(kpi.month_3_target, float):
        kpi.month_3_target=round(kpi.month_3_target,2)
    if kpi.month_1!='' and isinstance(kpi.month_1, float):
        kpi.month_1=round(kpi.month_1,2)
    if kpi.month_2!='' and isinstance(kpi.month_2, float):
        kpi.month_2=round(kpi.month_2,2)
    if kpi.month_3!='' and isinstance(kpi.month_3, float):
        kpi.month_3=round(kpi.month_3,2)
    if kpi.month_1_score!='' and isinstance(kpi.month_1_score, float):
        kpi.month_1_score=round(kpi.month_1_score,2)
    if kpi.month_2_score!='' and isinstance(kpi.month_2_score, float):
        kpi.month_2_score=round(kpi.month_2_score,2)
    if kpi.month_3_score!='' and isinstance(kpi.month_3_score, float):
        kpi.month_3_score=round(kpi.month_3_score,2)
    #round quarter
    if kpi.quarter_one_target!='' and isinstance(kpi.quarter_one_target, float):
        kpi.quarter_one_target=round(kpi.quarter_one_target,2)
    if kpi.quarter_two_target!='' and isinstance(kpi.quarter_two_target, float):
        kpi.quarter_two_target=round(kpi.quarter_two_target,2)
    if kpi.quarter_three_target!='' and isinstance(kpi.quarter_three_target, float):
        kpi.quarter_three_target=round(kpi.quarter_three_target,2)
    if kpi.quarter_four_target!='' and isinstance(kpi.quarter_four_target, float):
        kpi.quarter_four_target=round(kpi.quarter_four_target,2)

    if kpi.weight!='' and isinstance(kpi.weight,float):
        kpi.weight=round(kpi.weight,2)
    if kpi.real!='' and isinstance(kpi.real,float):
        kpi.real=round(kpi.real,2)

    row += [_value_to_display(kpi.unit),
            _value_to_display(kpi.current_goal),
            _value_to_display(kpi.future_goal),

            _value_to_display(kpi.operator),
            _value_to_display(kpi.quarter_one_target),
            _value_to_display(kpi.quarter_two_target),
            _value_to_display(kpi.quarter_three_target),
            _value_to_display(kpi.quarter_four_target),

            _value_to_display(kpi.month_1_target),
            _value_to_display(kpi.month_1),
            _value_to_display(kpi.month_1_score),

            _value_to_display(kpi.month_2_target),
            _value_to_display(kpi.month_2),
            _value_to_display(kpi.month_2_score),

            _value_to_display(kpi.month_3_target),
            _value_to_display(kpi.month_3),
            _value_to_display(kpi.month_3_score),

            ]

    if score:
        score = round(score, 1)

    if kpi.weight == 0:
        score = 'Delayed'

    row += [_value_to_display(kpi.real),
            _value_to_display(score),
            # , outcome_notes
            ]

    if is_parent:
        row += [_value_to_display(kpi.weight)]  # , '']
        row += ['']
    else:
        row += ['']
        row += [display_name(kpi.user_id)]

    return row


def construct_kpi_dict_row(kpi, is_parent=False, is_double_child=False):
    def _value_to_display(value):
        return value if value is not None else ''

    score = kpi.get_score()#kpi.user_id)
    if score:
        score = score
    else:
        score = ''

    data = {}
    data['id'] = kpi.id

    kpi_ordering_str = str(kpi.get_prefix_category())
    # if kpi.ordering:
    #     kpi_ordering_str = kpi_ordering_str + str(int(kpi.ordering))

    kpi_ordering_str += kpi.get_kpi_id()
    data['ordering'] = kpi_ordering_str

    if is_parent:
        data['name'] = kpi.get_name()
    else:
        if is_double_child:
            data['name'] = "          " + kpi.get_name()
        else:
            data['name'] = "     " + kpi.get_name()

    data['unit'] = _value_to_display(kpi.unit)
    data['current_goal'] = _value_to_display(kpi.current_goal)
    data['future_goal'] = _value_to_display(kpi.future_goal)
    data['operator'] = _value_to_display(kpi.operator)
    data['quarter_one_target'] = _value_to_display(kpi.quarter_one_target)
    data['quarter_two_target'] = _value_to_display(kpi.quarter_two_target)
    data['quarter_three_target'] = _value_to_display(kpi.quarter_three_target)
    data['quarter_four_target'] = _value_to_display(kpi.quarter_four_target)

    if is_parent:
        data['weight'] = _value_to_display(kpi.weight)
    else:
        data['assigned_to'] = display_name(kpi.user_id)

    if score:
        score = round(score, 1)

    if kpi.weight == 0:
        score = 'Delayed'

    data['real'] = _value_to_display(kpi.real)
    data['score'] = _value_to_display(score)
    data['children'] = []
    return data
#
#
# def export_kpi_list(profiles, organization, quarter_period, file_name="kpi"):
#     results = []
#     company_info = {
#         'name': organization.name,
#         'logo': organization.get_logo(),
#         'date': format(datetime.date.today(), 'd-m-Y')
#     }
#     for profile in profiles:
#         results.extend([[], [], []])
#         user = profile.user
#         extra_select = '''CASE WHEN performance_kpi.bsc_category = 'financial' THEN 1
#             WHEN performance_kpi.bsc_category = 'customer' THEN 2
#             WHEN performance_kpi.bsc_category = 'internal' THEN 3
#             WHEN performance_kpi.bsc_category = 'learninggrowth' THEN 4 ELSE 5 END '''
#
#         kpis = KPI.objects.filter(user=user, parent=None,
#                                   quarter_period=quarter_period) \
#             .extra(select={'order_category': extra_select}) \
#             .order_by('order_category', 'ordering')
#
#         # TODO: seperate competency export into another feature, put all into this is too hard
#
#         #        competencies = Competency.objects.filter(user=user, parent=None,
#         #                                                quarter_period=quarter_period).order_by('id')
#         full_name = profile.display_name
#
#         name = ''
#         if profile.current_job_title:
#             name = profile.current_job_title.name
#         job_title = profile.title if profile.title else name
#
#         results.append([8, ugettext('Name'), '', full_name])
#         results.append([9, ugettext('Email'), '', profile.email()])
#         results.append([10, ugettext('Position'), '', job_title])
#         # results.append([ full_name, '','', ])
#
#         # continue
#         kpi_score = profile.score_full_kpi_percent(quarter_period)
#
#         # TODO: seperate competency export into another feature, put all into this is too hard
#
#         # if organization.enable_competency and False:
#         #
#         #     competency_score = profile.score_full_competency(quarter_period)
#         #     if not competency_score:
#         #         competency_score = "N/A"
#         #     else:
#         #         competency_score = kpi_score_to_percent(competency_score)
#         #
#         #     results.append(
#         #         ['', ugettext('Avg. Performance Score: ') + str(kpi_score),
#         #          '', '',
#         #          ugettext('Avg. Potential Score: ') + str(competency_score)])
#         # else:
#         #     results.append(
#         #         ['', ugettext('Avg. Performance Score: ') + str(kpi_score)])
#
#         results.append([ugettext('Performance (KPI)'), ])
#
#         results.append([3,
#                         ugettext('UniqueID'),
#                         ugettext('ID'),
#                         ugettext('KPI'),
#                         # '',
#                         ugettext('Unit'),
#                         ugettext('MEASUREMENT METHODS'),
#                         ugettext('Note'),
#
#                         ugettext('Operator'),
#
#                         ugettext('Q.1'),
#                         ugettext('Q.2'),
#                         ugettext('Q.3'),
#                         ugettext('Q.4'),
#
#                         ugettext(quarter_period.month_1_name()),
#                         ugettext('Real'),
#                         ugettext('Performance'),
#                         ugettext(quarter_period.month_2_name()),
#                         ugettext('Real'),
#                         ugettext('Performance'),
#                         ugettext(quarter_period.month_3_name()),
#                         ugettext('Real'),
#                         ugettext('Performance'),
#
#                         "{0} ({1})".format(ugettext('Real'), ugettext('Quarter')),
#                         "{0} ({1})".format(ugettext('Performance'), ugettext('Quarter')),
#
#                         ugettext('Weighting Score'),
#                         #   '',
#                         ugettext('Assigned to'),
#                         #   ugettext('Performance'),
#                         # ugettext('Notes'),
#                         ])
#
#         # continue
#         for kpi in kpis:
#             move_next = True
#             # children = kpi.get_children().order_by('ordering')
#             children = kpi.get_children_refer_order()
#             # outcome_notes = kpi.get_kpi_notes_list()
#             # child_count = children.count()
#             child_count = len(children)
#
#             target = '%s %s' % (kpi.operator or '', str(kpi.target or ''))
#             target = kpi.operator or ''
#
#             results.append([[4, child_count + 1], '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''])
#
#             row = construct_kpi_row(kpi, True)
#             row[0] = [4, child_count + 1]
#             results.append(row)
#
#             for child in children:
#                 name = ''
#
#                 row = construct_kpi_row(child)
#                 row[0] = [4, child_count + 1]
#                 results.append(row)
#                 child_children = child.get_children_refer_order()
#                 for c_child in child_children:
#                     row = construct_kpi_row(c_child, is_double_child=True)
#                     row[0] = [4, child_count + 1]
#                     results.append(row)
#
#         results.append([[7, 0], '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''])
#
#         # <editor-fold desc="competency">
#         # TODO: seperate competency export into another feature, put all into this is too hard
#         if organization.enable_competency and False:
#             results.append([
#                 # 20,
#                 ugettext('Competency'), ])
#             results.append(
#                 [
#                     # 3,
#                     ugettext('Competency'), ugettext('Sub competency'), ugettext('Notes'), '', '', '', '', '', '', '',
#                     '', '', ''], )
#             total = competencies.count()
#             count = 0
#             for competency in competencies:
#                 move_next = True
#                 count += 1
#                 child_total = competency.get_children().count()
#                 child_count = 0
#                 children = competency.get_children().order_by('id')
#                 if children.count() == 0:
#                     index = 5 if count == total else 4
#                     row = [
#                         # [index, 1],
#                         competency.name, '', '', '', '', '', '', '', '', '', '', '', '']
#                     results.append(row)
#                     continue
#
#                 for child in children:
#                     child_count += 1
#                     name = ''
#                     comment_evidence = ""
#                     if move_next:
#                         name = competency.name
#                         comment_evidence = competency.comment_evidence
#                         move_next = False
#
#                     index = 4
#                     if count == total and child_count == child_total:
#                         index = 5
#                     row = [
#                         # [index, competency.get_children().count()],
#                         name, child.name, comment_evidence, '',
#                         '', '', '', '', '', '', '', '', '']
#                     results.append(row)
#             results.append([6, ''])
#             results.append([6, ''])
#             # </editor-fold>
#     return ExcelReportResponse(results, company_info, file_name)
#

# def export_kpi_to_pdf(profiles, organization, quarter_period, file_name="kpi"):  --> not used
#     company_info = {
#         'name': organization.name,
#         'logo': organization.get_logo(),
#         'date': format(datetime.date.today(), 'd-m-Y')
#     }
#
#     pdf_merge = PdfFileMerger()
#     pdf_output = StringIO.StringIO()
#     for profile in profiles:
#         user = profile.user
#         extra_select = '''CASE WHEN performance_kpi.bsc_category = 'financial' THEN 1
#             WHEN performance_kpi.bsc_category = 'customer' THEN 2
#             WHEN performance_kpi.bsc_category = 'internal' THEN 3
#             WHEN performance_kpi.bsc_category = 'learninggrowth' THEN 4 ELSE 5 END '''
#
#         kpis = KPI.objects.filter(user=user, parent=None,
#                                   quarter_period=quarter_period) \
#             .extra(select={'order_category': extra_select}) \
#             .order_by('order_category', 'ordering')
#
#         pdf = export_kpi_employee(profile, kpis, quarter_period)
#         pdf_merge.append(fileobj=pdf)
#
#     pdf_merge.write(fileobj=pdf_output)
#     pdf_output.seek(0)
#     response = pdf_output.getvalue()
#     pdf_output.close()
#     return HttpResponse(response, content_type="application/pdf")


# def export_kpi_employee(profile, kpis, quarter_period):
#     full_name = profile.display_name
#
#     name = ''
#     if profile.current_job_title:
#         name = profile.current_job_title.name
#     job_title = profile.title if profile.title else name
#
#     user_info = {
#         'full_name': full_name,
#         'email': profile.email(),
#         'position': job_title
#     }
#     # continue
#     results = []
#     kpi_score = profile.score_full_kpi_percent(quarter_period)
#     for kpi in kpis:
#         # children = kpi.get_children().order_by('ordering')
#         children = kpi.get_children_refer_order()
#         row = construct_kpi_dict_row(kpi, True)
#
#         for child in children:
#             name = ''
#             row_sub = construct_kpi_dict_row(child)
#             results.append(row)
#         #             child_children = child.get_children_refer_order()
#         #             for c_child in child_children:
#         #                 row_sub1 = construct_kpi_dict_row(c_child, is_double_child=True)
#         #                 row_sub['children'].append(row_sub1)
#         #             row['children'].append(row_sub)
#         results.append(row)
#
#     font = os.path.join(settings.ROOT_DIR, 'performance/templates/fonts/arial.ttf')
#     font_bd = os.path.join(settings.ROOT_DIR, 'performance/templates/fonts/arialbd.ttf')
#
#     extra_context = {
#         'organization': quarter_period.organization,
#         'employee': user_info,
#         'font': font,
#         'font_bd': font_bd,
#         'kpis': results
#     }
#     template = get_template('performance/bsc/pdf/kpi_pdf_template.html')
#     html = template.render(extra_context)
#
#     output = StringIO.StringIO()
#     pisaStatus = pisa.CreatePDF(html, dest=output,
#                                 link_callback=link_callback)
#
#     return output
