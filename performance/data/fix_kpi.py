# !/usr/bin/python
# -*- coding: utf-8 -*-
from django.core.exceptions import ObjectDoesNotExist
from elms import settings
from performance.base.kpi_type import *
from user_profile.services.user import get_system_user

from utils import notify_slack

__author__ = 'hongleviet'


# system_user = get_system_user()

def fix_target_not_synced(k, current_quarter=None):
    # <fix> FIX CATEGORY
    if not k.bsc_category:
        if k.parent_id:
            try:
                parent = k.parent
            except ObjectDoesNotExist:

                parent = None

            if parent:
                k.bsc_category = parent.get_bsc_category()


        elif k.refer_to_id:

            try:
                refer_to = k.refer_to
            except ObjectDoesNotExist:

                refer_to = None
            k.bsc_category = refer_to.get_bsc_category()
        else:
            k.bsc_category = 'financial'
        if k.bsc_category:
            k.save(update_fields=["bsc_category"],
                   force_update=True)  # http://stackoverflow.com/questions/40243640/save-with-update-fields-did-not-affect-any-rows-while-changing-class
    # </fix> END FIX CATEOGRY

    # print 'hererrrrrr'
    if current_quarter is None:
        current_quarter = k.user.profile.get_current_quarter()

    # children = k.get_children()

    # if current_quarter and children.count() == 0:
    if current_quarter:

        if current_quarter.quarter == 1 and k.target != k.quarter_one_target:
            print "k.target {0}".format(k.target)
            print "k.quarter_one_target {0}".format(k.quarter_one_target)
            print "k.quarter_two_target {0}".format(k.quarter_two_target)
            print "k.quarter_three_target {0}".format(k.quarter_three_target)
            print "k.quarter_four_target {0}".format(k.quarter_four_target)
            # pdb.set_trace()
            if k.quarter_one_target is not None:

                print "----- k.quarter_one_target is not None"

                k.target = k.quarter_one_target
            elif k.target is not None:

                print "----- k.target is not None"
                k.quarter_one_target = k.target

            k.save(update_fields=["quarter_one_target", "target"])

            return True

        if current_quarter.quarter == 2 and k.target != k.quarter_two_target:

            print "k.target {0}".format(k.target)
            print "k.quarter_one_target {0}".format(k.quarter_one_target)
            print "k.quarter_two_target {0}".format(k.quarter_two_target)
            print "k.quarter_three_target {0}".format(k.quarter_three_target)
            print "k.quarter_four_target {0}".format(k.quarter_four_target)

            if k.quarter_two_target is not None:
                k.target = k.quarter_two_target
            elif k.target is not None:
                k.quarter_two_target = k.target

            k.save(update_fields=["quarter_two_target", "target"])
            return True

        if current_quarter.quarter == 3 and k.target != k.quarter_three_target:

            print "k.target {0}".format(k.target)
            print "k.quarter_one_target {0}".format(k.quarter_one_target)
            print "k.quarter_two_target {0}".format(k.quarter_two_target)
            print "k.quarter_three_target {0}".format(k.quarter_three_target)
            print "k.quarter_four_target {0}".format(k.quarter_four_target)

            if k.quarter_three_target is not None:
                k.target = k.quarter_three_target
            elif k.target is not None:
                k.quarter_three_target = k.target

            k.save(update_fields=["quarter_three_target", "target"])
            return True

        if current_quarter.quarter == 4 and k.target != k.quarter_four_target:

            print "k.target {0}".format(k.target)
            print "k.quarter_one_target {0}".format(k.quarter_one_target)
            print "k.quarter_two_target {0}".format(k.quarter_two_target)
            print "k.quarter_three_target {0}".format(k.quarter_three_target)
            print "k.quarter_four_target {0}".format(k.quarter_four_target)

            if k.quarter_four_target is not None:
                k.target = k.quarter_four_target
            elif k.target is not None:
                k.quarter_four_target = k.target

            k.save(update_fields=["quarter_four_target", "target"])
            return True

    return False


def fix_kpi_cha_duoc_phan_cong_wrong_email(kpi):
    is_error = False
    if kpi.owner_email != kpi.user.email:
        kpi.owner_email = kpi.user.email
        kpi.save()

        is_error = True

    return is_error


def fix_children_assigned(kpi):
    is_error = False

    if kpi.kpi_type_v2() == kpi_con_normal:
        if kpi.assigned_to_id > 0:
            assigned_to = None
            try:
                assigned_to = kpi.assigned_to
            except:
                pass

            if assigned_to:
                is_error = True
                '''
                kpi_con_normal nhưng được phân công --> error
                --> convert thành KPI trung gian
                '''
                kpi.assign_to(assigned_to)

    return is_error


def fix_children_wrong_type(kpi):
    is_error = False

    if kpi.kpi_type_v2() == kpi_con_normal:

        try:
            refer_to = kpi.refer_to
        except ObjectDoesNotExist:
            refer_to = None

        if kpi and kpi.parent and kpi.parent.user != kpi.user:  # bug: neu la KPI_con_normal --> k.parent.user == k.user

            is_error = True

            # --> day la kpi cha_duoc_phan_cong --> kpi.parent = None
            kpi.parent = None
            kpi.save()
            #
            #
            # if refer_to:
            #     if kpi.parent_id > 0 and kpi.refer_to_id != kpi.parent_id:
            #         kpi.parent_id = None
            #         kpi.save()
            #         is_error = True

    return is_error


def fix_children_refer_not_equal_parent(kpi):
    if kpi.refer_to_id is not None and kpi.refer_to_id != kpi.parent_id:
        kpi.refer_to_id = kpi.parent_id
        kpi.save()

        is_error = True

        return is_error


def fix_children_no_refer_no_assign(kpi):
    if kpi.refer_to_id is None and (kpi.assigned_to_id is None or kpi.get_assigned_kpi() is None):
        err = (u'bug: kpi_con_normal nếu không được phân công thì cần có refer_to = parent .  = {}').format(kpi.id)

        kpi.error_log(err, 'kpi_con_normal:children_no_refer_no_assign')
        # print('kpi_con_normal:children_no_refer_no_assign')
        # notify_slack(settings.SLACK_CRONJOB_CHANNEL, '`kpi_con_normal:children_no_refer_no_assign`' + " " + err )


        # print 'fix_refer'
        '''
            GIVEN: Truong hop chuyen kpi_trung_gian thanh kpi_con_normal khi khong con kpi_cha_duoc_phan_cong
            THEN: kpi.assigned_to = None
        '''
        kpi.assigned_to = None
        kpi.refer_to = kpi.parent
        #   print self.refer_to_id
        kpi.save()

        is_error = True

        return is_error

        #  print KPI.objects.get(id =  self.id).refer_to_id
