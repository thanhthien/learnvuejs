# !/usr/bin/python
# -*- coding: utf-8 -*-
import pytest
from django.core.exceptions import ObjectDoesNotExist
from elms import settings
from performance.data.export import construct_kpi_row

from utils import notify_slack

__author__ = 'hongleviet'


@pytest.mark.django_db(transaction=False)
def test_construct_kpi_row(kpi, ):
   # kpi = parent_kpi
    kpi.bsc_category = 'Financial'

    row = construct_kpi_row(kpi, is_parent = True)
    assert row[1] == kpi.id

    kpi_ordering_str = str(kpi.get_prefix_category())
    kpi_ordering_str += kpi.get_kpi_id()

    #print kpi_ordering_str


    #print row

    assert row[2] == kpi_ordering_str

    assert row[3].find(kpi.name) >= 0