from performance.data.fix_kpi import *
import pytest
from performance.models import KPI

__author__ = 'hongleviet'

@pytest.mark.django_db(transaction=False)
def test_fix_children_wrong_type(kpi, userA, userB, organization):

    k = KPI()
    k.name = 'kpi con normal '
    k.user = userA
    k.save()

    k_parent = KPI()
    k_parent.name = 'kpi cha normal '
    k_parent.user = userA
    k_parent.save()

    k.parent = k_parent
    k.save()

    assert k.kpi_type_v2() == kpi_con_normal

    k_parent2 = KPI()
    k_parent2.name = 'kpi cha normal 2'
    k_parent2.user = userB
    k_parent2.save()

    kpi_cha_duoc_phancong = k_parent2.assign_to(userA)

   # k.assign_up(k_parent2)

    assert kpi_cha_duoc_phancong.kpi_type_v2() == kpi_cha_duoc_phan_cong

    assert kpi_cha_duoc_phancong.parent == None

    kpi_cha_duoc_phancong.parent = k_parent2
    kpi_cha_duoc_phancong.save()

    print kpi_cha_duoc_phancong.kpi_type_v2()
    is_error =  fix_children_wrong_type(kpi_cha_duoc_phancong)

    print kpi_cha_duoc_phancong.kpi_type_v2()
    assert is_error == True
    is_error = fix_children_wrong_type(kpi_cha_duoc_phancong)
    assert is_error == False

    assert k.kpi_type_error() != 'unknown'



@pytest.mark.django_db(transaction=False)
def test_fix_target_not_synced(kpi, userA, organization):






    current_quarter = kpi.user.profile.get_current_quarter()


    current_quarter.quarter = 1
    fix_target_not_synced(kpi, current_quarter)
    assert kpi.bsc_category is not None
    assert kpi.target == kpi.quarter_one_target



    current_quarter.quarter = 2
    kpi.quarter_two_target = 100
    fix_target_not_synced(kpi, current_quarter)
    assert kpi.target == 100


    current_quarter.quarter = 3
    kpi.quarter_three_target = 110
    fix_target_not_synced(kpi, current_quarter)
    assert kpi.target == kpi.quarter_three_target


    current_quarter.quarter = 4
    kpi.quarter_four_target = 120
    fix_target_not_synced(kpi, current_quarter)
    assert kpi.target == kpi.quarter_four_target

@pytest.mark.django_db(transaction=False)
def test_fix_children_refer_not_equal_parent(kpi):

    kpi.refer_to_id = kpi.parent_id + 1
    assert  kpi.refer_to_id == kpi.parent_id + 1

    is_error = fix_children_refer_not_equal_parent(kpi)

    assert  is_error == True
    assert KPI.objects.get(id = kpi.id).refer_to_id == kpi.parent_id

@pytest.mark.django_db(transaction=False)
def test_fix_children_no_refer_no_assign(kpi):

    kpi.refer_to_id = None

    is_error = fix_children_no_refer_no_assign(kpi)

    assert  is_error == True
    assert kpi.refer_to is not None
