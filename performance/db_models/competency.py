# -*- coding: utf-8 -*-
import redis
from django.contrib.auth.models import User
from django.db import models
from elms import settings
from model_utils import FieldTracker
from performance.db_models.kpi_library import JobTitle
from django.utils.translation import ugettext_lazy as _
# from performance.documents import ScoreHistory
from performance.models import QuarterPeriod
from utils import notify_slack
from utils.common import is_redis_available, is_nan

COMPETENCY_CATEGORY = (
    ('basic', _('Basic Competency')),
    ('domain', _('Domain Competency')),
    ('culture', _('Culture')),
)


class CompetencyLib(models.Model):
    name = models.CharField(max_length=2000, default='', null=True, blank=True)
    description = models.TextField(default='', blank=True)
    parent = models.ForeignKey('self', null=True, blank=True)
    category = models.CharField(max_length=30, choices=COMPETENCY_CATEGORY, null=True, blank=True)
    level = models.IntegerField(default=1)
    code = models.CharField(max_length=10, default='', null=True, blank=True)

    def __unicode__(self):
        try:
            return self.name + " - level: " + str(self.level)
        except:
            return ""

    def children_level1(self):
        return CompetencyLib.objects.filter(parent_id=self.id, level=1)

    def copy_to_user(self, user):
        parent = Competency()
        parent.user = user
        parent.quarter_period = user.profile.get_organization().get_current_quarter()
        parent.name = self.name
        # parent.name_vi = self.name_vi
        # parent.name_en = self.name_en
        parent.description = self.description
        # parent.description_vi = self.description_vi
        # parent.description_en = self.description_en
        parent.category = self.category
        parent.code = self.code
        parent.level = self.level
        parent.save()

        children = self.children_level1()
        for child in children:
            c = Competency()
            c.user = user
            c.quarter_period = user.profile.get_organization().get_current_quarter()
            c.name = child.name + u" - Level " + str(child.level)
            c.name_vi = child.name_vi + u" - Level " + str(child.level)
            c.name_en = child.name_en + u" - Level " + str(child.level)
            c.description = child.description
            c.description_vi = child.description_vi
            c.description_en = child.description_en
            c.category = child.category
            c.code = child.code
            c.level = child.level
            c.parent = parent
            c.save()


class Competency(models.Model):
    name = models.CharField(verbose_name=_("Name"), max_length=2000, default='', null=True, blank=True)
    description = models.TextField(verbose_name=_("Description"), default='', blank=True)
    parent = models.ForeignKey('self', verbose_name=_("Parent competency"), null=True, blank=True)
    user = models.ForeignKey(User, null=True, blank=True)
    copy_from = models.ForeignKey('self', null=True, blank=True,
                                  related_name='comp_copy_from', on_delete=models.SET_NULL)
    job = models.ForeignKey(JobTitle, null=True, blank=True,
                            on_delete=models.SET_NULL,
                            related_name='competencies')
    data_input_approved = models.BooleanField(default=False)
    in_library = models.BooleanField(default=False)
    quarter = models.IntegerField(default=1, blank=True)
    year = models.IntegerField(default=0, blank=True)
    is_core_value = models.BooleanField(default=False, blank=True)
    quarter_period = models.ForeignKey(QuarterPeriod, blank=True, null=True)
    belong_to_jobs = models.ManyToManyField(JobTitle,
                                            related_name='competency_list',
                                            blank=True)
    comment_evidence_self = models.TextField(verbose_name=_("Comments and Evidence (Self)"), default="", blank=True)
    comment_evidence = models.TextField(verbose_name=_("Comments and Evidence (Manager)"), default="", blank=True)
    is_owner = models.BooleanField(default=False)  # is_owner = True se ko bi xoa khi change job title
    category = models.CharField(verbose_name=_("Category"), max_length=30, choices=COMPETENCY_CATEGORY, null=True,
                                blank=True)

    level = models.IntegerField(verbose_name=_("Level"), default=1, null=True, blank=True)
    code = models.CharField(verbose_name="Code", max_length=10, default='', null=True, blank=True)
    reviewer = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL,
                                 related_name="reviewer_competency")

    tracker = FieldTracker()

    # history = HistoricalRecords()

    def get_changed_history(self):
        changed = self.tracker.changed()
        descs = []
        descs.append(self.name + ":")

        for f in changed:
            if hasattr(self, f):
                if f.endswith("_id"):
                    continue
                title = self._meta.get_field(f).verbose_name.title()
                item = u"- %s: %s → %s" % (title, changed[f], getattr(self, f))
                descs.append(item)

        return "\n".join(descs)

    def get_name(self):
        if self.name == self.name_en:
            # TODO: this have a problem
            return self.name_vi
        else:
            return self.name

    class Meta:
        db_table = 'performance_competency'
        ordering = ('id',)

    def __unicode__(self):

        try:
            return self.name or ''

        except:
            print 'duan error'
            return 'fk'

    def clone_to_user(self, to_user, quarter_period):
        new_comp = Competency(name=self.name,
                              name_vi=self.name_vi,
                              name_en=self.name_en,
                              description=self.description,
                              description_vi=self.description_vi,
                              description_en=self.description_en,
                              user=to_user,
                              quarter_period=quarter_period,
                              is_owner=self.is_owner,
                              comment_evidence_self='',
                              comment_evidence='',
                              is_core_value=self.is_core_value,
                              copy_from=self)
        new_comp.save()

        for child in self.get_children():
            new_child_comp = Competency(name=child.name,
                                        name_vi=child.name_vi,
                                        name_en=child.name_en,
                                        description=child.description,
                                        description_vi=child.description_vi,
                                        description_en=child.description_en,
                                        user=to_user,
                                        quarter_period=quarter_period,
                                        is_owner=child.is_owner,
                                        comment_evidence_self='',
                                        comment_evidence='',
                                        is_core_value=child.is_core_value,
                                        copy_from=child)
            new_child_comp.parent = new_comp
            new_child_comp.save()

        return new_comp

    #
    # def calculate_avg_score_allreviewers(self):
    #     reviews = Review.objects.filter(reviewee_id=self.user_id, quarter_period_id=self.quarter_period_id)
    #     count = len(reviews)
    #
    #     total_score = 0
    #     for review in reviews:
    #
    #         score = self.calculate_avg_score(review.reviewer_id)
    #         total_score += score
    #
    #         if score == 0:
    #             count -= 1
    #
    #     if count <= 0:
    #         count = 1
    #
    #     return total_score / count

    def calculate_avg_score(self, reviewer_id=None):
        children = self.get_children()
        value = None
        count = 0

        if children.count() > 0:
            for child in children:
                score = child.get_score()  # reviewer_id)
                if score:
                    value = (value or 0) + score
                    count += 1

            if count:
                value = value / float(count)  # tinh trung binh cong
        else:
            value = self.get_score()  # reviewer_id)

        return value

    def get_key(self, reviewer_id):
        key = "competency:" + str(self.id) + ":" + str(reviewer_id)
        return key

    def set_score(self, value, reviewer_id):
        rdis = redis.from_url(settings.redis_url)
        key = self.get_key(reviewer_id)
        rdis.set(key, value)

    def get_score(self, reviewer_id=None):
        key = self.get_key(reviewer_id)
        value = 0

        rdis = redis.from_url(settings.redis_url)
        if is_redis_available():
            value = rdis.get(key)
        if value:
            value = float(value)
        return value

    def set_comment_360(self, value, reviewer_id):

        rdis = redis.from_url(settings.redis_url)

        key = self.get_key('comment360:%s:%s' % (str(self.id), str(reviewer_id)))

        rdis.set(key, value)

    def get_comment_360(self, reviewer_id):
        key = self.get_key('comment360:%s:%s' % (str(self.id), str(reviewer_id)))

        rdis = redis.from_url(settings.redis_url)
        value = rdis.get(key)
        if not value:
            value = ""
        return value

    def reset_child_score(self, reviewer_id):
        for child in self.get_children():
            child.set_score(0, reviewer_id)

    def get_final_score(self):
        score = 0
        count = 0
        total_score = 0
        if self.reviewer_id:
            total = 0
            reviewers = [self.reviewer_id, self.user_id]
            for reviewer in reviewers:
                score = self.get_score()  # reviewer)
                if score:
                    total += score
                    count += 1

            if count > 0:
                score = total / float(count)
        else:
            # reviewers = Review.objects.only('reviewer_id').filter(reviewee_id=self.user_id,
            #                                                       quarter_period_id=self.quarter_period_id,
            #                                                       review_type__in=['manager', 'self'])
            #
            # for reviewer in reviewers:
            #     score = self.get_score()  # reviewer.reviewer_id)
            #     if score:
            #         total_score += score
            #         count += 1
            #
            # if count > 0:
            #     score = total_score / float(count)
            score = 0

        return score

    def get_children(self):  # TODO
        children = Competency.objects.filter(parent=self)
        if children.filter(quarter_period_id=None).exists() and self.quarter_period_id:
            children.filter(quarter_period_id=None).update(quarter_period_id=self.quarter_period_id)
        return children

    def get_children_dict(self, person, shared=False, reviewer=None):
        data = []
        for c in self.get_children():
            item = {}
            item['id'] = c.id
            item['name'] = c.name
            item['score'] = None

            # try:
            #     history = ScoreHistory.objects.get(obj_id=c.id,
            #                                        type='competency')
            #     history = history.history
            # except Exception as e:
            #
            #     # TODO: FUCK THIS BUG
            #     # notify_slack(SLACK_CRONJOB_CHANNEL,
            #     #              "history = ScoreHistory.objects.get(obj_id=self.id, type ='competency': error: {0}".format(
            #     #                      str(e)))
            #     history = []

            item['histories'] = []#history

            if reviewer:
                item['score'] = c.get_score()  # reviewer)
                item['list_score_360'] = c.get_360_review_score()

            item['self_score'] = None
            if c.user_id:
                item['self_score'] = c.get_score(c.user_id)
                item['list_score_360'] = c.get_360_review_score()

            if person == 'self' and not shared:
                item['self_score'] = None
                item['list_score_360'] = []

            if person == "manager" and not shared:
                item['score'] = None

            if person == "360":
                item['score'] = c.get_score("360" + str(reviewer))

            data.append(item)
        return data

    def get_360_review_score(self):
        result = []
        # reviews = Review.objects.filter(reviewee_id=self.user_id,
        #                                 quarter_period_id=self.quarter_period_id) \
        #     .exclude(review_type__in=['self', 'manager']) \
        #     .values_list('reviewer', flat=True)
        # for review in reviews:
        #     score = self.get_score('360' + str(review))
        #     result.append(score)

        return result

    def get_list_comment_360(self):
        from user_profile.models import Profile

        result = []

        # reviews = Review.objects.only('reviewer', 'review_type').filter(reviewee_id=self.user_id,
        #                                                                 quarter_period_id=self.quarter_period_id). \
        #     exclude(review_type__in=['self', 'manager'])
        #
        # for review in reviews:
        #     p = Profile.objects.only('display_name',).get(user_id=review.reviewer_id)
        #     comment = u"%s (%s): %s" % (p.display_name, review.get_review_type_display(),
        #                                 self.get_comment_360(review.reviewer_id).decode(encoding="utf-8",
        #                                                                                 errors="ignore"))
        #     result.append(comment)
        return result

    def competency_dict(self, person, shared=False, reviewer=None):
        score = None
        if reviewer:
            score = self.get_score(reviewer)

        self_score = None
        if self.user_id:
            self_score = self.get_score(self.user_id)

        if person == 'self' and not shared:
            self_score = None

        if person == "manager" and not shared:
            score = None

        comment_evidence_self = self.comment_evidence_self

        if person == 'self' and not shared:
            comment_evidence_self = ""
            comment_360 = []
        else:
            comment_360 = self.get_list_comment_360()

        comment_evidence = self.comment_evidence
        if person == "manager" and not shared:
            comment_evidence = ""

        if person == "360":
            comment_evidence = self.get_comment_360(reviewer)

        # try:
        #     history = ScoreHistory.objects.get(obj_id=self.id,
        #                                        type='competency')
        #     history = history.history
        # except Exception as e:
        #     notify_slack(settings.SLACK_CRONJOB_CHANNEL, 'performance/models.py:2356 {0}'.format(str(e)))
        #
        #     history = []

        data = {
            'id': self.id,
            'name': self.name,
            'description': self.description,
            'score': score,
            'self_score': self_score if not is_nan(self_score) else 0,
            'comment_evidence_self': comment_evidence_self,
            'comment_evidence': comment_evidence,
            'comment_360': comment_360,
            'children': self.get_children_dict(person, shared, reviewer),
            'histories': []
        }
        return data

    def newbie_to_json(self):
        if self.parent_id:
            return {
                "self_score": None,
                "score": None,
                "id": self.id,
                "name": self.name
            }
        else:
            return {
                "name": self.name,
                "id": self.id,
                "description": self.description,
                "comment_evidence_self": "",
                "score": None,
                "comment_evidence": "",
                "self_score": None,
                "children": []
            }
