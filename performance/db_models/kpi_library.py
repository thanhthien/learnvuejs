from django.db import models
import tagging.registry
from django.utils import timezone
from django_permanent.models import PermanentModel
from tagging.fields import TagField
from jsonfield.fields import JSONField
from performance.models import KPI, BSC_CATEGORY, LANGUAGES
from django.utils.translation import ugettext_lazy as _, ugettext


class Industry(models.Model): # pragma: no cover
    name = models.CharField(max_length=255, default='', null=True, blank=True)
    in_library = models.BooleanField(default=False)

    def __unicode__(self):

        try:
            return self.name or ''

        # no case to cover exception
        except:  # pragma: no cover
            return ''


class JobCategory(models.Model):  # pragma: no cover
    name = models.CharField(max_length=256, default='', null=True, blank=True)
    industry = models.ForeignKey(Industry, null=True, blank=True, on_delete=models.SET_NULL)
    ordering = models.IntegerField(default=0)

    class Meta:
        db_table = 'performance_job_category'
        ordering = ('ordering',)

    def __unicode__(self):

        try:
            if self.industry_id > 0:
                return self.industry.name + " - " + self.name
            return "zGeneral industry - " + self.name

        # no case to cover exception
        except:  # pragma: no cover
            return ''

class JobTitle(models.Model):# pragma: no cover
    name = models.CharField(max_length=256, default='', null=True, blank=True)
    category = models.ForeignKey(JobCategory, default=0, null=True, blank=True)
    # category = models.ForeignKey(JobCategory, null=True, blank=True)
    description = models.TextField(default='', null=True, blank=True)
    factors = models.TextField(default='', null=True, blank=True)

    class Meta:
        db_table = 'performance_job_title'
        ordering = ('category__name',)

    def __unicode__(self):
        try:
            if self.category:

                if self.category.industry:
                    return self.name + " - " + self.category.name + " [" + self.category.industry.name + "]"
                else:
                    return self.category.name + " - " + self.name
            else:
                return self.name

        # no case to cover exception
        except:  # pragma: no cover
            return ''


class BscDepartment(models.Model):# pragma: no cover
    name = models.CharField(max_length=2000, default='', null=True, blank=True)

    def __unicode__(self):

        try:
            return self.name or ''

        except:
            return ''


class BscJobTitle(models.Model):  # pragma: no cover
    name = models.CharField(max_length=2000, default='', null=True, blank=True)
    department = models.ForeignKey(BscDepartment, on_delete=models.SET_NULL, null=True)

    def __unicode__(self):
        try:

            if self.department:
                return self.name + u" [" + self.department.name + u"]"
            else:
                return self.name
        except:
            return ''


# not used as confirmed with Mr. Phi
class BscKPI(models.Model):  # pragma: no cover
    name = models.CharField(max_length=2000, default='', null=True, blank=True)
    measurement = models.CharField(max_length=500, default='', null=True, blank=True)
    department = models.ForeignKey(BscDepartment, on_delete=models.SET_NULL, null=True)
    job_title = models.ForeignKey(BscJobTitle, on_delete=models.SET_NULL, null=True)
    translated = models.BooleanField(default=False)
    checked = models.BooleanField(default=False)

    @classmethod
    def add(cls, name, measurement, department, job_title):
        d = BscDepartment.objects.filter(name=department)
        if d.count() == 0:
            d = BscDepartment(name=department)
            d.save()
        else:
            d = d[0]
        # d.save()
        j = BscJobTitle.objects.filter(name=job_title)
        if j.count() == 0:
            j = BscJobTitle(name=job_title, department=d)
            j.save()
        else:
            j = j[0]

        if j.department is None:
            j.department = d
            j.save()

        k = BscKPI.objects.filter(name=name)
        if k.count() == 0:
            k = BscKPI(name=name)
            k.measurement = measurement
            k.department = d
            k.job_title = j
            k.save()

    @property
    def to_json(self):
        element = {}
        element['name'] = self.name + u" (" + self.measurement + u")"
        element['value'] = self.name + u" (" + self.measurement + u")"
        element['data'] = self.name + u" (" + self.measurement + u")"
        element['measurement'] = self.measurement
        element['department'] = self.department.name

        return element

    def __unicode__(self):
        try:
            return self.name + " [" + self.job_title.name + "]"
        except:
            return ''

    def save(self, *args, **kwargs):

        if self.department is None and self.job_title is not None:
            self.department = self.job_title.department

        # if not self.name_vi:
        #     self.translated = True

        super(BscKPI, self).save(*args, **kwargs)


class KPILib(PermanentModel, models.Model):
    EXTRA_FIELDS = (
        ('unit', _('Unit')),
        ('functions', _('Functions')),
        ('data_source', _("Data sources")),
        ('frequency', _("Frequency")),
        ('example', _("Example")),
        ('tips', _("Tips")),
        ('kpi_source', _("KPI Source"))
    )

    organization = models.ForeignKey('company.Organization', null=True, blank=True)
    objective = models.CharField(max_length=1000, verbose_name=_("Objectives"))
    name = models.CharField(max_length=1000, verbose_name=_("Name"))
    category = models.CharField(choices=BSC_CATEGORY, max_length=30, default="financial")
    description = models.TextField(blank=True, verbose_name=_("Description"))
    measurement_method = models.TextField(blank=True, verbose_name=_("Measurement method"))
    operator = models.CharField(choices=KPI.OPERATOR_CHOICES, max_length=10, verbose_name=_("Operator"))
    extra_data = JSONField(default={}, blank=True, editable=False)
    language = models.CharField(choices=LANGUAGES ,max_length=10, verbose_name=_("Language"), default='vi')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __unicode__(self):
        return self.name

    def update_attribute(self):
        for (field, name) in self.EXTRA_FIELDS:
            setattr(self, field, self.extra_data.get(field))

    def save(self, *args, **kwargs):
        PermanentModel.save(self, *args, **kwargs)
        self.update_attribute()
        return self


tagging.registry.register(KPILib)


class TagItem(models.Model):
    name = models.CharField(max_length=200)
    parent = models.ForeignKey("self", blank=True, null=True)

    def __unicode__(self):
        return self.name

