from company.models import Organization
from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _
from performance.models import QuarterPeriod


# didn't find any use so no cover
class KPIImportLog(models.Model):  # pragma: no cover
    organization = models.ForeignKey(Organization)
    quarter_period = models.ForeignKey(QuarterPeriod)
    employee_name = models.CharField(max_length=200)
    import_date = models.DateField()
    employee_code = models.CharField(max_length=100, blank=True)
    description = models.TextField(blank=True)

    def __unicode__(self):

        try:
            return self.employee_name or ''

        except:
            print 'duan error'
            return 'fk'

class AlignLog(models.Model):
    user = models.ForeignKey(User)
    quarter_period = models.ForeignKey(QuarterPeriod)
    is_done = models.BooleanField(default=False)
    status = models.IntegerField(default=0)
    log = models.TextField(default='')

    created_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return u"{0} | Quarter ID: {1}".format(self.user.username, self.quarter_period_id)


class UserChangeLog(models.Model):
    user = models.ForeignKey(User)
    actor = models.ForeignKey(User, related_name='actor')
    quarter = models.ForeignKey(QuarterPeriod)
    pre_email = models.CharField(max_length=100, default='')
    post_email = models.CharField(max_length=100, default='')
    created_at = models.DateTimeField(auto_now_add=True)



class KpiLog(models.Model):
    STATUS_CHOICES = (
        ('new', _('Add new')),
        ('update', _('Update')),
        ('delete', _('Delete')),
        ('transfer', _('Transfer')),
        ('assigned', _('Assigned to')),
    )
    ROLE_EDIT = (
        ('', "---"),
        ('self', "Self"),
        ('manager', "Manager"),
        ('admin', "Admin")
    )
    LOG_TYPE = (
        ('kpi', "KPI"),
        ('competency', _("Competency")),
        ('devplan', _("Development Plan"))
    )
    editor = models.ForeignKey(User, related_name="editor", on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name="user", blank=True, null=True, on_delete=models.CASCADE)
    report_to = models.ForeignKey(User, blank=True, null=True, on_delete=models.CASCADE, db_index=True)
    role_edit = models.CharField(choices=ROLE_EDIT, max_length=10)
    status = models.CharField(choices=STATUS_CHOICES, max_length=10)
    log_type = models.CharField(choices=LOG_TYPE, max_length=10)
    description = models.TextField(blank=True, null=True)
    edited_time = models.TimeField(auto_now_add=True)
    edited_date = models.DateField(auto_now_add=True)
    quarter = models.ForeignKey(QuarterPeriod, null=True, blank=True)
    main_field = models.CharField(max_length=100, null=True, blank=True, db_index=True)

    def save(self, *args, **kwargs):
        if self.quarter is None and self.user:
            self.quarter = self.user.get_profile().get_organization().get_current_quarter()

        super(KpiLog, self).save(*args, **kwargs)