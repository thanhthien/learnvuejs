import json

from auditlog.models import AuditlogHistoryField
from auditlog.registry import auditlog
from django.core import serializers
from django.db import models
from django_permanent.models import PermanentModel
from jsonfield import JSONField

from company.models import Position

'''
Remove PermanentModel vi se gap bug khi delete position
'''
class PositionKPI(models.Model):

    BACKUP_TYPE = (
        ('kpilib', 'KPILib',),
        ('performanceresult', 'PerformanceResult')
    )

    position = models.ForeignKey(Position)
    create_at = models.DateField(auto_now_add=True)
    backup_type = models.CharField(choices=BACKUP_TYPE, max_length=50)
    kpi_json_data = JSONField(default={})
    month = models.IntegerField(null=True, blank=True)
    logs = AuditlogHistoryField()

    """
        kpi_json_data la 1 JsonField ca dang nhu sau:

        {
            'score': Int,
            'employee_name':  
            'employee_email'
            'kpis' : [  
               { 
                    'kpilib_unique_id' : string
                    'name': ,
                    'group_name': ,
                    'bsc_category: ,
                    'target'
                    'real'
                    'result'
                    'refer_relationship' : [ 
                        {
                            'type': 'direct',
                            'position_id': Int,
                            'kpilib_unique_id' : string
                        }   
                    ]
                }
            ]
        1 list cua KPIs, moi element la 1 dict ]
        }  
    """

    def to_json(self):

        js = json.loads(serializers.serialize('json', [self]))[0]

        j = js['fields']
        j['id'] = self.id
        j['position_id'] = self.position.id
        j['position_name'] = self.position.name
        j['backup_type'] = self.backup_type
        j['month'] = self.month
        j['kpi_json_data'] = self.kpi_json_data

        return j


auditlog.register(PositionKPI)