import datetime

import markdown
from company.models import Organization
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import models
from django_permanent.models import PermanentModel
from elms import settings
from django.utils.translation import ugettext_lazy as _
from performance.models import QuarterPeriod, BSC_CATEGORY, KPI
from utils import random_string, notify_slack


class SWOT(PermanentModel):
    SWOT_TYPE = (
        ('strength', _('Strength')),
        ('weakness', _('Weakness')),
        ('opportunity', _('Opportunity')),
        ('threat', _('Threat')),
    )

    name = models.CharField(max_length=500)
    user = models.ForeignKey(User, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    organization = models.ForeignKey(Organization)
    order = models.IntegerField(default=1)
    weighting = models.IntegerField(default=10)
    swot_type = models.CharField(max_length=100, choices=SWOT_TYPE, blank=True, default='strength')
    is_archive = models.BooleanField(default=False)

    def __unicode__(self): # pragma: no documentation
        try:
            # changed from organiSation to organiZation
            return self.name + " " + self.organization.name
        except:  # pragma: no cover
            return ''


class BSC_KSF(PermanentModel):
    name = models.CharField(max_length=500)
    user = models.ForeignKey(User, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    organization = models.ForeignKey(Organization)
    order = models.IntegerField(default=1)
    weighting = models.IntegerField(default=10)
    is_hidden = models.BooleanField(default=False)

    important_1 = models.IntegerField(null=True)
    important_2 = models.IntegerField(null=True)
    important_3 = models.IntegerField(null=True)
    important_4 = models.IntegerField(null=True)
    important_5 = models.IntegerField(null=True)

    achivable_1 = models.IntegerField(null=True)
    achivable_2 = models.IntegerField(null=True)
    achivable_3 = models.IntegerField(null=True)
    achivable_4 = models.IntegerField(null=True)
    achivable_5 = models.IntegerField(null=True)

    product = models.FloatField(null=True)

    bsc_type = models.CharField(max_length=100, choices=BSC_CATEGORY, blank=True, default='financial')

    def __unicode__(self):
        try:
            # changed from organiSation to organiZation
            return self.name + " " + self.organization.name
        except:  # pragma: no cover
            return ''







class StrategyMap( PermanentModel):#(models.Model):
    organization = models.ForeignKey(Organization)
    director = models.ForeignKey(User, blank=True, null=True)
    year = models.IntegerField(blank=True, null=True)  # unuse
    name = models.TextField(blank=True)
    description = models.TextField(blank=True)
    quarter_period = models.ForeignKey(QuarterPeriod, blank=True, null=True)
    hash = models.CharField(max_length=100, blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)

    class Meta:
   #     unique_together = [['organization', 'director', 'quarter_period']]
        ordering = ['-id']


    def get_name(self):
        if not self.name:
            prev = self.organization.get_previous_quarter()
            map = StrategyMap.objects.filter(quarter_period=prev).first()

            if prev and map:
                self.name = map.name
                self.save()

        return self.name

    def get_description(self):
        # changed from self.name to self.description
        if not self.description:
            prev = self.organization.get_previous_quarter()
            map = StrategyMap.objects.filter(quarter_period=prev).first()

            if prev and map:
                self.description = map.description
                self.save()
        return self.description

    def get_director(self):
        if self.director_id:
            return self.director
        return self.organization.ceo

    def get_absolute_url_v2(self):
        if self.director_id:
            # before: return reverse("sub_strategy_map_by_hash_v2", kwargs={'hash': self.hash})
            # changed to :
            return reverse('sub_strategy_map_by_hash_v2', kwargs={'user_map_hash': self.get_hash()})

        # changed from self.hash self.get_hash():
        return reverse('strategy_map_by_hash_v2', kwargs={'hash': self.get_hash()})

    def get_absolute_url(self):

        return self.get_absolute_url_v2()
        #
        # if self.director_id:
        #     # before: return reverse("sub_strategy_map_by_hash", kwargs={'hash': self.hash})
        #     # changed to :
        #     return reverse('sub_strategy_map_by_hash', kwargs={'user_map_hash': self.get_hash()})
        # # changed from self.hash self.get_hash():
        # return reverse('strategy_map_by_hash', kwargs={'hash': self.get_hash()})

    @classmethod
    def get_create(cls, organization, current_quarter, director=None):
        s_map = StrategyMap.objects.filter(organization=organization,
                                           director=director,
                                           quarter_period=current_quarter
                                           ).first()

        if not s_map:
            s_map = StrategyMap(organization=organization,
                                year=datetime.datetime.now().year,
                                director=director,
                                quarter_period=current_quarter,
                                name=organization.name + " - VISION"
                                )

            s_map.save()

        if len(s_map.description) == 0:
            previous_quarter = current_quarter.get_previous_quarter()
            if previous_quarter:

                previous_strategy_map = previous_quarter.get_strategy_map()
                if previous_strategy_map:
                    # changed the following code as quarter has no attributes name & description
                    # old code:
                    # s_map.name = previous_quarter.name
                    # s_map.description = previous_quarter.description
                    # new code
                    # thus, assigned old strategy_map name & description to new s_map
                    s_map.name = previous_strategy_map.name
                    s_map.description = previous_strategy_map.description

            s_map.save()

        return s_map

    @classmethod
    def create(cls, organization_id, quarter_period_id, name, director=None):
        return cls.objects.create(organization_id=organization_id,
                                  quarter_period_id=quarter_period_id,
                                  name=name,
                                  director=director)

    @classmethod
    def get(cls, organization_id, quarter_period_id, director=None):
        return cls.objects.filter(organization_id=organization_id, quarter_period_id=quarter_period_id,
                                  director=director).order_by('-id').first()

    @classmethod
    def get_by_id(cls, smap_id, organization_id):
        return cls.objects.filter(id=smap_id, organization_id=organization_id).first()

    @classmethod
    def get_by_year(cls, organization_id, year, director=None):
        return StrategyMap.objects.filter(organization_id=organization_id, quarter_period__year=year,
                                          director=director).order_by('-quarter_period_id').first()

    @classmethod
    def get_maps(cls, organization_id, director=None):
        return StrategyMap.objects.filter(organization_id=organization_id,
                                          director=director).order_by('-quarter_period_id')

    def get_hash(self, save=True):
        if not self.hash:
            self.hash = "strategymap-" + random_string(5)
            if save:
                self.save()

        return self.hash

    def save(self, *args, **kwargs):
        # if self.get_hash(True) --> will raise error when
        # call  StrategyMap.objects.create(organization_id=organization_id,quarter_period_id=quarter_period_id)
        self.get_hash(False)
        if self.quarter_period is None:
            try:
                self.quarter_period = self.organization.get_current_quarter()
            except Exception as e:  # pragma: no cover
                notify_slack(settings.SLACK_CRONJOB_CHANNEL, 'performance/models.py:2865 {0}'.format(str(e)))
        super(StrategyMap, self).save(*args, **kwargs)

    def __unicode__(self):
        # if self.quarter_period is None:
        #     self.save()

        try:
            return u'%s - %d - %s' % (self.name, self.quarter_period.year, self.organization.name)
        except:  # pragma: no cover
            return ''

    def get_upgroup_kpis(self, category, quarter_period):
        # kpis_ungroup = KPI.objects.filter(user=self.organization.ceo,
        #                                   #   quarter_period=None,
        #                                   # year=self.year,
        #                                   parent__isnull=True,
        #                                   bsc_category=category,
        #                                   groupkpi__isnull=True)

        # current_quarter = self.organization.get_current_quarter()

        # for kpi in kpis_ungroup:
        #     if kpi.quarter_period is None:
        #         kpi.quarter_period = current_quarter
        #     if kpi.year == 0:
        #         kpi.year = datetime.datetime.today().year
        #         kpi.save()
        def lowest_kpi_order(kpi):
            if kpi:
                return kpi.ordering
            else:
                return 0

        user = self.get_director()
        kpis_ungroup = KPI.objects.filter(models.Q(group_kpi__isnull=True) |
                                          models.Q(group_kpi__name__in=['...']),
                                          # If add group_name = '' when create KPI with KPI editor will display double KPI in strategymap
                                          ###  group name is empty when create KPI with KPI editor
                                          ###  group name is ... when create KPI with strategy map
                                          user=user,
                                          quarter_period=quarter_period,
                                          bsc_category=category,
                                          parent__isnull=True,
                                          # groupkpi__isnull=True,
                                          ).order_by('ordered', 'ordering', 'id')

        # sorted_kpis_list = sorted(kpis_ungroup, key=lambda x: lowest_kpi_order(x))

        return kpis_ungroup


# BSC STRATEGY MAP GROUP
#class GroupKPI(models.Model):
class GroupKPI(PermanentModel):
    map = models.ForeignKey(StrategyMap, null=True, blank=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=1000)
    category = models.CharField(max_length=30, choices=BSC_CATEGORY, db_index=True, default='')
    kpis = models.ManyToManyField(KPI, blank=True)  # TODO: remove

    # TODO: copy this n-to-n relationship after create new review quarter
    # TODO: n-to-n must be 1-to-n

    # __json_hidden__ =  ['kpis']

    def __unicode__(self):
        try:
            return self.name + " - " + self.map.__unicode__()
        except:
            return ''

    def save(self, *args, **kwargs):
        if self.category is None:
            self.category = ''

        if self.name is None:
            self.name = ''

        '''
        To check if map object for group_kpi.map is saved #3776 fix
        '''
        if self.map_id is None:
            pass
        #    self.map.save()

        super(GroupKPI, self).save(*args, **kwargs)

    def get_kpis(self, only_parent=True):

        query_set = KPI.objects.filter(group_kpi=self).order_by('ordered', 'ordering', 'id')

        if query_set.count() == 0:
            '''
            update legacy data &  set group_kpi

            remove these code later
            '''

            kpis = self.kpis.all()
            for k in kpis:
                k.group_kpi = self
                k.save()

                # DELETE old many to many data .
                self.kpis.remove(k)

        if only_parent:
            query_set = query_set.filter(parent__isnull=True)

        kpis = query_set

        return kpis

    def copy_group_kpi(self, group):
        self.name = group.name
        self.category = group.category
        self.save()

    def to_json(self, include_kpi=True, actor=None):
        js = {}  # json.loads(serializers.serialize('json', [self]))[0]
       # print js

        js['name'] = self.name
        js['map'] = self.map_id
        js['category'] = self.category
        result = js
        result['id'] = self.id
        result['group'] = True
        result['kpis'] = []
        if include_kpi:
            for kpi in self.get_kpis()  :  # KPI.objects.filter(groupkpi__id=self.id):
                # if kpi.kpi_type_v2() != 'kpi_cha_duoc_phan_cong': # theo li thuyet thi moi kpi 1 group, nhung neu group do chua KPI con cua user do thi khong lay ra. Chi lay nhung group co kpi cha
                #     result['contain_children'] = True
                #     break
                result['kpis'].append(kpi.to_json_nochildren(actor=actor))

        # try:
        #     for kpi in list(self.kpis.all()):
        #         result['kpis'].append(kpi.to_json_nochildren())
        # except:
        #     pass

        return result
