import pytest
import random
from datetime import datetime

from performance.db_models.kpi_library import Industry, JobCategory, JobTitle
from utils.random_str import random_string

#
# @pytest.mark.django_db(transaction=False)
# def test_Industry():
#     """
#     Case 1: No name
#     """
#     industry = Industry()
#     industry.save()
#     result = industry.__unicode__()
#     assert result == ''
#
#     """
#     Case 2: Name given
#     """
#     industry.name = random_string()
#     industry.in_library = True
#     industry.save()
#
#     result = industry.__unicode__()
#     assert result == industry.name
#
#
# @pytest.mark.django_db(transaction=False)
# def test_JobCategory():
#     ind = Industry()
#     ind.name = random_string()
#     ind.in_library = True
#     ind.save()
#
#     """
#     Case 1: self.industry = None
#     """
#     jc = JobCategory()
#     jc.name = random_string()
#     jc.ordering = random.randint(0, 100)
#     jc.save()
#     assert jc.__unicode__() == "zGeneral industry - " + jc.name
#
#     """
#     Case 2: self.industry != None
#     """
#     jc.industry = ind
#     jc.save()
#     assert jc.__unicode__() == ind.name + " - " + jc.name
#
#
# @pytest.mark.django_db(transaction=False)
# def test_JobTitle():
#     """
#     Case 1: No category assigned
#     """
#     jt = JobTitle()
#     jt.name = random_string()
#     jt.description = random_string()
#     jt.factors = random_string()
#     jt.save()
#     assert jt.__unicode__() == jt.name
#
#     """
#     Case 2: Category assigned but industry not assigned to category
#     """
#     jc = JobCategory()
#     jc.name = random_string()
#     jc.ordering = random.randint(0, 100)
#     jc.save()
#
#     jt.category = jc
#     assert jt.__unicode__() == jt.category.name + " - " + jt.name
#
#     """
#     Case 3: Industry assigned to category
#     """
#     ind = Industry()
#     ind.name = random_string()
#     ind.in_library = True
#     ind.save()
#
#     jc.industry = ind
#
#     assert jt.__unicode__() == jt.name + " - " + jt.category.name + " [" + jt.category.industry.name + "]"
