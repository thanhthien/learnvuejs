from monthdelta import monthdelta
import pytest
from datetime import datetime
from performance.db_models.strategy_map import *
from performance.services.strategymap import create_group_kpi, StrategyMapServices
from utils.random_str import random_string
from django.forms import DateTimeField

@pytest.mark.django_db(transaction=False)
def test_swot(userA, kpi, organization):
    swot = SWOT()
    swot.user = userA
    swot.swot_type = "Strength"
    swot.organization = organization
    swot.name = "Strength1"
    swot.save()
    result = swot.__unicode__()
    assert result == swot.name + " " + swot.organization.name


@pytest.mark.django_db(transaction=False)
def test_bsc_ksf(userA, kpi, organization):
    bscksf = BSC_KSF()
    bscksf.name = random_string(100)
    bscksf.user = userA
    bscksf.organization = organization
    bscksf.save()

    result = bscksf.__unicode__()
    assert result == bscksf.name + " " + bscksf.organization.name


@pytest.mark.django_db(transaction=False)
def test_strategy_map(userA, kpi, ceo, client_authed, organization):
    current_quarter = organization.get_current_quarter()
    smap = StrategyMap()
    smap.organization = organization
    smap.director = userA
    smap.quarter_period = current_quarter
    smap.save()

    d = datetime.date.today() - monthdelta(4)
    old_quarter = QuarterPeriod.objects.create(organization=organization,
                                     due_date=d,
                                     clone_from_quarter_period=current_quarter)

    old_quarter.status = 'AR'
    old_quarter.save()

    smap1 = StrategyMap()
    smap1.organization = organization
    smap1.director = userA
    smap1.name = random_string(100)
    smap1.description = random_string(100)
    smap1.quarter_period = old_quarter
    smap1.save()

    """
    Test for get_name
    """
    result = smap.get_name()
    assert result == smap1.name

    """
    Test for get_description
    """
    result1 = smap.get_description()
    assert result1 == smap1.description

    """
    Test for get_absolute_url_v2
    """
    result2 = smap.get_absolute_url_v2()
    assert result2 == reverse('sub_strategy_map_by_hash_v2', kwargs={'user_map_hash': smap.get_hash()})

    smap.director = None

    smap.save()

    result2 = smap.get_absolute_url_v2()
    assert result2 == reverse('strategy_map_by_hash_v2', kwargs={'hash': smap.get_hash()})
    
    """
    Test for get_absolute_url
    """
    result2 = smap.get_absolute_url()
    #assert result2 == reverse('strategy_map_by_hash', kwargs={'hash': smap.get_hash()})

    smap.director = userA
    smap.save()

    # result2 = smap.get_absolute_url()
    # assert result2 == reverse('sub_strategy_map_by_hash', kwargs={'user_map_hash': smap.get_hash()})


@pytest.mark.django_db(transaction=False)
def test_strategy_map_get_create(userA, organization, kpi):
    old_quarter = organization.get_current_quarter()

    d = datetime.date.today() + monthdelta(4)
    new_quarter = QuarterPeriod.objects.create(organization=organization,
                                               due_date=d,
                                               clone_from_quarter_period=old_quarter)

    new_quarter.status = "IN"
    new_quarter.save()

    organization.self_review_date = d
    organization.save()

    old_quarter.status = "AR"
    old_quarter.save()

    smap1 = StrategyMap()
    smap1.organization = organization
    smap1.director = userA
    smap1.name = random_string(100)
    smap1.description = random_string(100)
    smap1.quarter_period = old_quarter
    smap1.save()

    director = userA
    smap = StrategyMap.get_create(organization, new_quarter, director)
    assert smap.name == smap1.name


@pytest.mark.django_db(transaction=False)
def test_strategy_map_get_methods(userA, organization, kpi):
    """
    Test for db_models/strategy_map.py->StrategyMap::get_by_year
    """
    current_quarter = organization.get_current_quarter()
    d = datetime.date.today() - monthdelta(10)
    old_quarter = QuarterPeriod.objects.create(organization=organization,
                                               due_date=d,
                                               clone_from_quarter_period=current_quarter)

    old_quarter.status = "AR"
    old_quarter.save()

    smap1 = StrategyMap()
    smap1.organization = organization
    smap1.director = userA
    smap1.name = random_string(100)
    smap1.description = random_string(100)
    smap1.quarter_period = old_quarter
    smap1.save()

    smap = StrategyMap()
    smap.organization = organization
    smap.director = userA
    smap.quarter_period = current_quarter
    smap.save()
    today = datetime.date.today()
    result = StrategyMap.get_by_year(organization.id, today.year, userA)
    assert result == smap

    """
    Test for db_models/strategy_map.py->StrategyMap::get_maps
    """
    result = StrategyMap.get_by_year(organization.id, today.year, userA)
    assert result == smap


@pytest.mark.django_db(transaction=False)
def test_strategy_map_get_methods2(userA, ceo, organization, parent_kpi, kpi):

    """
    Test for db_models/strategy_map.py->StrategyMap::get_hash
    """
    current_quarter = organization.get_current_quarter()
    smap3 = StrategyMap()
    smap3.organization = organization
    smap3.director = userA
    smap3.name = "TEST"
    smap3.quarter_period = current_quarter
    assert smap3.id is None
    result = smap3.get_hash(save=True)
    assert "strategymap-" in result
    assert smap3.id is not None

    """
    Test for db_models/strategy_map.py->StrategyMap::__unicode__
    """
    result1 = smap3.__unicode__()
    assert result1 == u'%s - %d - %s' % (smap3.name, smap3.quarter_period.year, smap3.organization.name)


@pytest.mark.django_db(transaction=False)
def test_groupkpi_methods(userA, ceo, organization, kpi, parent_kpi):

    """
    Test for db_models/strategy_map.py->StrategyMap: get_kpis
    Case 1: legacy relation, erasing legacy data
    """
    current_quarter = organization.get_current_quarter()
    smap3 = StrategyMap()
    smap3.organization = organization
    smap3.director = userA
    smap3.name = "TEST"
    smap3.quarter_period = current_quarter
    smap3.save()
    category = 'financial'

    group = create_group_kpi(organization.id, smap3.id, category, 'Hello World')
    assert group.id > 0
    group.kpis = [kpi, parent_kpi]
    group.save()
    result2 = group.get_kpis()
    assert group.kpis.all().count() == 0

    """
    Case 2: one-to-many relation
    """
    StrategyMapServices.update_group_kpi(organization.id, parent_kpi.id, group.id, None,
                                         ceo)
    kpi.group_kpi = group
    kpi.save()

    result2 = group.get_kpis(only_parent=False)
    assert len(result2) == 2 # kpi and parent kpi

    result2 = group.get_kpis(only_parent=True)
    assert len(result2) == 1 # only parent Kpi
    assert result2[0].id == parent_kpi.id

    """
    Test for __unicode__
    """

    result = group.__unicode__()
    assert group.name in result