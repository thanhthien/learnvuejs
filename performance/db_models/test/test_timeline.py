import pytest
import markdown
from dateutil.relativedelta import relativedelta

from performance.db_models.timeline import TaskEveryStage, KPITask
from utils.random_str import random_string
import datetime


@pytest.mark.django_db(transaction=False)
def test_task_every_stage(organization, userA):
    tes = TaskEveryStage.objects.create(organization=organization, name=random_string())

    """ test meeting minutes html """
    assert tes.meeting_minutes_html() == markdown.markdown("")
    _random_string = random_string()
    tes.meeting_minutes = _random_string
    tes.save()
    assert tes.meeting_minutes_html() == markdown.markdown(_random_string)

    """ test next steps html"""
    assert tes.next_steps_html() == markdown.markdown("")
    tes.next_steps = _random_string
    tes.save()
    assert tes.next_steps_html() == markdown.markdown(_random_string)

    """ test kpi task"""
    kpi_task1 = KPITask.objects.create(task=tes,
                                       name=_random_string)
    assert (tes.get_kpis()).count() == 1

    """ test get_finished_tasks """
    kpi_task1.done = True
    kpi_task1.save()
    assert (tes.get_finished_tasks()).count() == 1

    """ test finished_percentage """
    kpi_task2 = KPITask.objects.create(task=tes,
                                       name=_random_string)
    assert tes.finished_percentage() == 50

    kpi_task2.done = True
    kpi_task2.save()
    assert tes.finished_percentage() == 100

    """ test remain_days """
    tes.end_date = datetime.date.today() + relativedelta(days=7)
    tes.save()
    assert tes.remain_days() == 7

    tes.end_date = datetime.date.today() + relativedelta(days=-7)
    tes.save()
    assert tes.remain_days() == 0

    """ test total_days """
    tes.due_date = datetime.date.today()
    tes.end_date = datetime.date.today() + relativedelta(days=7)
    tes.save()
    assert tes.total_days() == 7

    tes.end_date = datetime.date.today()
    tes.due_date = datetime.date.today() + relativedelta(days=7)
    tes.save()
    assert tes.total_days() == 1

    """ test passed_days """
    tes.due_date = datetime.date.today() + relativedelta(days=-7)
    tes.save()
    assert tes.passed_days() == 7

    tes.due_date = datetime.date.today() + relativedelta(days=7)
    tes.save()
    assert tes.passed_days() == 0

    """ test timeline_percentage """
    tes.due_date = datetime.date.today() + relativedelta(days=-7)
    tes.end_date = datetime.date.today() + relativedelta(days=30)
    tes.save()
    assert tes.timeline_percentage() == int(round((float)(7) / (float)(7 + 30) * 100))

    tes.end_date = datetime.date.today() + relativedelta(days=-3)
    tes.save()
    assert tes.timeline_percentage() == 100

    tes.due_date = datetime.date.today() + relativedelta(days=+1)
    tes.save()
    assert tes.timeline_percentage() == 0