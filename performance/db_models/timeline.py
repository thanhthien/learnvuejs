# -*- coding: utf-8 -*-

import datetime

import markdown
from company.models import Organization
from django.core.urlresolvers import reverse
from django.db import models
from elms import settings
from django.utils.translation import ugettext_lazy as _


class Timeline(models.Model):
    organization = models.ForeignKey(Organization)
    stage = models.CharField(max_length=255)
    content = models.TextField()
    deadline = models.DateField()
    created_at = models.DateField(auto_now_add=True)
    done = models.BooleanField(default=False)

    class Meta:
        ordering = ('created_at',)

    def __unicode__(self):  # pragma: no cover
        try:
            return self.stage or ''
        except:
            return None


class TaskEveryStage(models.Model):
    TASK_STATUS_CHOICES = (
        ('notstart', _('Not start')),
        ('start', _('On process')),
        ('finished', _('Finished')),
        ('late', _('Overdue')),
    )
    DATE_CHOICES = (
        ('0', u"Trong ngày"),
        ('1', u"Nhiều ngày")
    )
    organization = models.ForeignKey(Organization)
    stage = models.ForeignKey(Timeline, blank=True, null=True)
    name = models.TextField(null=True, blank=True)  # models.CharField(max_length=500)
    date_range = models.CharField(choices=DATE_CHOICES, default='0', max_length=5)
    due_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)
    due_time_start = models.TimeField(blank=True, null=True)
    due_time_end = models.TimeField(blank=True, null=True)
    status = models.CharField(max_length=100, choices=TASK_STATUS_CHOICES, blank=True, default='notstart')
    note = models.TextField(blank=True)
    meeting_minutes = models.TextField(blank=True, null=True)
    next_steps = models.TextField(blank=True, null=True)

    def get_url(self):
        # return settings.DOMAIN + reverse('organzation-timeline-public',
        #                                  args=(self.organization.get_hash(),)) + "?step=" + str(self.id)
        return ""

    def meeting_minutes_html(self):
        if not self.meeting_minutes:
            self.meeting_minutes = ""
            self.save()

        return markdown.markdown(self.meeting_minutes)

    def next_steps_html(self):
        if not self.next_steps:
            self.next_steps = ""
            self.save()

        return markdown.markdown(self.next_steps)

    def __unicode__(self):  # pragma: no cover

        try:
            return self.name or ''

        except:
            print 'duan error'
            return 'fk'

    def get_kpis(self):  # THis method name is stupid --> agreed
        return KPITask.objects.filter(task=self).order_by('id')

    def get_finished_tasks(self):
        return KPITask.objects.filter(task=self, done=True).order_by('id')

    def finished_percentage(self):
        tasks = self.get_kpis()
        finished_tasks = self.get_finished_tasks()

        tasks_count = (tasks.count())
        if tasks_count < 1:
            tasks_count = 1

        return int(round((float)(finished_tasks.count()) / (float)(tasks_count) * 100))

    def remain_days(self):
        today = datetime.datetime.now().date()

        r = 0
        if self.end_date:
            r = (self.end_date - today).days

        if r < 0:
            r = 0

        return r

    def total_days(self):

        r = 1
        if self.end_date and self.due_date:
            r = (self.end_date - self.due_date).days

        if r < 1:
            r = 1
        return r

    def passed_days(self):
        today = datetime.datetime.now().date()

        r = (today - self.due_date).days

        if r < 0:
            r = 0

        return r

    def timeline_percentage(self):
        remain_days = self.remain_days()
        today = datetime.datetime.now().date()

        if self.passed_days() >= 0:
            r = int(round((float)(self.passed_days()) / (float)(self.total_days()) * 100))
            if r > 100:
                r = 100
            elif r < 0:
                r = 0

            return r
        else:
            return 0


class KPITask(models.Model):
    task = models.ForeignKey(TaskEveryStage)
    name = models.TextField(null=True, blank=True)  # models.CharField(max_length=500)
    done = models.BooleanField(default=False)

    def __unicode__(self):  # pragma: no cover

        try:
            return self.name or ''
        except:
            return None
