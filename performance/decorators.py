from django.contrib.auth.views import logout
from django.core.exceptions import PermissionDenied
import datetime

from django.http.response import HttpResponse
from django.utils.translation import ugettext
from rest_framework.authtoken.models import Token

from company.services.organization import is_expired_v2
from user_profile.services.profile import org_super_permission
from user_profile.models import Profile
#
# def consultant_permission(function):
#     def _inner(request, *args, **kwargs):
#         if not request.user.is_authenticated():
#             raise PermissionDenied
#
#         try:
#             profile = request.user.profile
#             if profile.is_consultant:
#                 if profile.expired_date < datetime.date.today() or not profile.expired_date:
#                     raise PermissionDenied
#         except:
#             raise PermissionDenied
#
#         return function(request, *args, **kwargs)
#     return _inner
from user_profile.services.user import authenticate_token
from utils.api.response import APIJsonResponseForbidden


def validate_license(function):

    def _inner(request, *args, **kwargs):
        response = function(request, *args, **kwargs)

        if request.method == "POST":
            actor = request.user

            if is_expired_v2(actor):
                #return APIJsonResponseForbidden(ugettext("Your license is expired. Please contact Cloudjet to renew. Thank you!"))
                #
                logout(request)
                return HttpResponse(
                    "<h2 style='text-align:center; margin-top: 300px; '>Your license is expired. Please contact Cloudjet to renew. Thank you!</h2>")

        return response

    return _inner


def validate_license_apiview(function):

    def _inner(request, *args, **kwargs):
        response = function(request, *args, **kwargs)
        if request.method == "POST":
            actor,token_object = authenticate_token(response.data.get('token',''))
            if is_expired_v2(actor):
                token_object.delete()
                return APIJsonResponseForbidden(ugettext("Your license is expired. Please contact Cloudjet to renew. Thank you!"))

        return response

    return _inner




def org_admin_perm(function):

    def _inner(request, *args, **kwargs):
        if not request.user.is_authenticated():
            raise PermissionDenied

        try:
            profile = request.user.profile
        except:
            raise PermissionDenied

        if not profile.is_superuser_org():
            raise PermissionDenied

        return function(request, *args, **kwargs)

    return _inner


def kpi_admin_permission(function):

    def _inner(request, *args, **kwargs):
        if not request.user.is_authenticated():
            raise PermissionDenied

        try:
            profile = request.user.profile

            uo = Profile.objects.get(user=request.user)
            if not uo.is_admin and not uo.is_superuser:
                raise PermissionDenied
        except:
            raise PermissionDenied

        return function(request, *args, **kwargs)

    return _inner
