#!/usr/bin/python
# -*- coding: utf-8 -*-
import datetime

from mongoengine import document
from mongoengine import fields



class KpiQuarterFinalScore(document.Document):
    organization = fields.IntField(required=True)
    quarter_period = fields.IntField(required=True)

    user_id = fields.IntField(required=True)
    profile = fields.IntField()
    name = fields.StringField()
    avatar = fields.StringField()
    job_title_name = fields.StringField()
    competency_final_score = fields.FloatField()
    kpi_final_score = fields.FloatField()

    email = fields.StringField()
    position = fields.StringField()
    employee_id = fields.StringField()
    manager_name = fields.StringField()
    manager_id = fields.IntField()

    meta = {
        'indexes': [('organization', 'quarter_period', 'user_id')]
    }

    def __unicode__(self):

        try:
            return self.name

        except:
            print 'duan error'
            return 'fk'

class ReportScoreFinal(document.Document):
    BIND_GLOBAL_CHOICE = (
        (1, 'Yes'),
        (0, 'No'),
    )

    organization = fields.IntField()
    quarter_period = fields.IntField()
    user_id = fields.IntField()
    employee_name = fields.StringField()
    org_chart = fields.StringField()
    employee_code = fields.StringField()
    manager_name = fields.StringField()
    manager_email = fields.StringField()
    manager_code = fields.StringField()
    potential_score = fields.FloatField()
    potential_score_self = fields.FloatField()
    potential_score_manger = fields.FloatField()
    performance_score = fields.FloatField()
    performance_score_self = fields.FloatField()
    performance_score_manager = fields.FloatField()
    grid_position = fields.StringField()
    kpi_percent = fields.FloatField()
    kpi_percent_m1 = fields.FloatField()
    kpi_percent_m2 = fields.FloatField()
    kpi_percent_m3 = fields.FloatField()
    kpi_percent_self = fields.FloatField()
    kpi_percent_manager = fields.FloatField()
    delayed_kpi = fields.IntField(default=0)
    delayed_self_kpi = fields.IntField(default=0)
    delayed_assigned_kpi = fields.IntField(default=0)
    confirmed_kpi = fields.IntField(default=0)
    total_self_kpi = fields.IntField(default=0)
    total_assigned_kpi = fields.IntField(default=0)
    total_kpi = fields.IntField(default=0)
    total_weighting_score = fields.IntField(default=0)
    get_confirm_kpis = fields.BooleanField(default=False)
    created_at = fields.DateTimeField(default=datetime.datetime.now())

   # email = fields.EmailField(default=None)
    email = fields.StringField()
    start_date = fields.DateTimeField(default=None)
    net_salary = fields.IntField(default=0)
    start_rewarding = fields.IntField(default=None)
    end_rewarding = fields.IntField(default=None)
    max_rewarding = fields.IntField(default=None)
    monthly_trend = fields.ListField(fields.DictField(), default=[])
    est_reward = fields.IntField(default=0)
    final_reward = fields.IntField(default=0)

    is_computed = fields.BooleanField(default=True)

    bind_global = fields.IntField(choices=BIND_GLOBAL_CHOICE, default=1)

    meta = {
        'indexes': [('organization', 'quarter_period', 'user_id')]
    }
    def save(self, force_insert=False, validate=True, clean=True,
             write_concern=None, cascade=None, cascade_kwargs=None,
             _refs=None, save_condition=None, **kwargs):

        self.created_at = datetime.datetime.now()
        super( ReportScoreFinal, self).save()



class ReportSelfAssessmentReviewStatus(document.Document):
    organization = fields.IntField()
    quarter_period = fields.IntField()
    user_id = fields.IntField()
    employee_name = fields.StringField()
    employee_code = fields.StringField()
    manager_name = fields.StringField()
    manager_code = fields.StringField()
    completion = fields.IntField()
    share_status = fields.StringField()
    grid_position = fields.StringField()
    created_at = fields.DateTimeField(default=datetime.datetime.now())

    meta = {
        'indexes': [('organization', 'quarter_period', 'user_id')]
    }

#
# class FinalScoreHistory(document.Document):
#     organization = fields.IntField()
#     quarter_period = fields.IntField()
#     quarter_name = fields.StringField()
#     due_date = fields.StringField()
#     user_id = fields.IntField()
#     kpi_score = fields.FloatField()
#     competency_score = fields.FloatField()
#
#     meta = {
#         'indexes': [('organization', 'quarter_period', 'user_id')]
#     }
#
# #
# class ScoreHistory(document.Document):
#     MODEL_TYPE = (('kpi', 'kpi'), ('competency', 'competency'))
#     obj_id = fields.IntField(unique_with='type')
#     type = fields.StringField(max_length=10, choices=MODEL_TYPE)  # kpi or competency
#     history = fields.ListField()
#
#     meta = {
#         'indexes': [('obj_id', 'type')]
#     }


class KPIRviewByStage(document.Document):
    kpi_id = fields.IntField()
    review_date = fields.DateTimeField()
    operator = fields.StringField()
    target = fields.FloatField(default=None)
    unit = fields.StringField(default='')
    real = fields.FloatField(default=None)
    score = fields.FloatField(default=None)
    note = fields.StringField(default='')

    def score_to_percent(self):
        if self.score is None:
            return None
        return self.score


class ExcelFile(document.Document):
    organization = fields.IntField()
    quarter_period = fields.IntField()
    file_name = fields.StringField()
    file = fields.FileField()
