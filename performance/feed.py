# from stream_framework.storage.redis.models import Activity
from django.utils.translation import ugettext as _
from stream_framework.verbs.base import Verb


class KPICreate(Verb):
    id = 1
    infinitive = _('create')
    past_tense = _('created')


class KPIUpdate(Verb):
    id = 2
    infinitive = _('update')
    past_tense = _('updated')


class KPIComment(Verb):
    id = 3
    infinitive = _('comment')
    past_tense = _('commented')


class KPIDelete(Verb):
    id = 4
    infinitive = _('delete')
    past_tense = _('deleted')


class KPIReview(Verb):
    id = 5
    infinitive = _('review')
    past_tense = _('reviewed')
