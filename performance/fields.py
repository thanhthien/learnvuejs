# !/usr/bin/python
# -*- coding: utf-8 -*-
from datetime import datetime

from memoize import delete_memoized, delete_memoized_verhash

from company.services.organization import get_organization
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.template.defaultfilters import safe
from django.template.loader import render_to_string
from django.utils.translation import ugettext
from inplaceeditform.fields import AdaptorBooleanField, AdaptorTextField, \
    AdaptorIntegerField, AdaptorTextAreaField, AdaptorEmailField, \
    AdaptorFloatField, AdaptorDecimalField, AdaptorURLField, BaseAdaptorField
from performance.services import add_auto_comment, add_user_comment
from company.models import Organization
from performance.logger import KPILogger
from performance.forms import KPIForm, KPIWeightForm, KPIAssignEmailForm, \
    KPIDetailsForm
from performance.models import KpiComment, KPI, get_kpi_id
from performance.services.quarter import QuarterServices
from user_profile.templatetags.user_tags import avatar_email, get_avatar_email_v2
from performance.base.kpi_type import *
from performance.services.kpi import track_change


class KPIFieldLog:#pragma: no cover

    def write_log(self, editor, kpi, field, old_value, new_value):
        description = u"%s:\n%s: %s: %s → %s" % (kpi.name,
                                                 ugettext("Update"),
                                                 field, old_value, new_value)
        KPILogger(editor, kpi, 'update', description).start()

        description = u"%s: %s: %s → %s" % (ugettext("Update"),
                                            field, old_value, new_value)
        # kc = KpiComment(user=editor,
        #                 kpi_unique_key=kpi.unique_key,
        #                 kpi=kpi, send_from_ui='auto',
        #                 content=description,
        #                 is_update=True)
        # kc.save()

        # Change tp auto comment
        add_auto_comment(editor, kpi, description)


class DoneAdaptorBooleanField(AdaptorBooleanField):#pragma: no cover

    def render_value(self, field_name=None, template_name="inplaceeditform/adaptor_boolean/render_value.html"):
        value = super(AdaptorBooleanField, self).render_value(field_name)
        return value


class KPINameAdaptor(AdaptorTextField, KPIFieldLog):#pragma: no cover

    #     def classes(self):
    #         return "testclass"
    def get_form_class(self):
        return KPIForm

    def get_form(self):
        if self.obj.cascaded_from_id:

            cascaded_from = KPI.objects.filter(id=self.obj.cascaded_from_id)
            if cascaded_from.count() == 0:
                self.obj.cascaded_from_id = None
                self.obj.save()

        self.old_value = getattr(self.obj, self.field_name)
        return AdaptorTextField.get_form(self)

    def save(self, value):
        AdaptorTextField.save(self, value)
        # self.obj.name_vi = self.obj.name
        # self.obj.name_en = self.obj.name
        # self.obj.description_vi = self.obj.description_vi or self.obj.description_en
        # self.obj.description_en = self.obj.description_vi
        if self.obj.parent_id is None and self.obj.user_id is not None:
            self.obj.owner_email = self.obj.user.email
        self.obj.save()

        cascaded_from = None
        if self.obj.cascaded_from_id:

            cascaded_from = KPI.objects.filter(id=self.obj.cascaded_from_id)
            if cascaded_from.count() == 0:
                self.obj.cascaded_from_id = None
                self.save()
            else:

                cascaded_from = self.obj.cascaded_from
                cascaded_from.name = value
                # cascaded_from.name_vi = value
                # cascaded_from.name_en = value
                cascaded_from.save()

        if not self.request.user.is_staff and self.obj.user_id:
            field = unicode(self.obj._meta.get_field(self.field_name).verbose_name)
            self.write_log(self.request.user, self.obj, field, self.old_value, value)


class KPIAssignUserAdaptor(AdaptorEmailField):#pragma: no cover

    def get_form_class(self):
        return KPIAssignEmailForm

    def get_form(self):
        self.before_email = self.obj.owner_email
        self.initial = {'request': self.request}
        form_class = self.get_form_class()
        return form_class(instance=self.obj,
                          initial=self.initial,
                          prefix=id(form_class))

    def render_value(self, field_name=None, template_name="inplaceeditform/adaptor_url/render_value.html"):
        value = super(AdaptorTextField, self).render_value(field_name)

        # if self.obj.owner_email and self.obj.user_id:
        if self.obj.user:
            year = datetime.now().year
            # url = reverse('kpi_dashboard_emp',args =(year, self.obj.user_id,)) #+ '?team_id=' + str(self.obj.user_id)
            url = reverse('kpi_editor_emp', args=(self.obj.user_id,))  # + '?team_id=' + str(self.obj.user_id)
            title = ugettext("Go to performance review")
            value = u"""
            <a title='%s' target='_blank' href='%s'><img align='left' class='user-thumb' src='%s' alt='Avatar'></a>
            <div class="email_user">%s</div>
            """ % (title, url, get_avatar_email_v2(self.obj.user.email), self.obj.user.email)
        return safe(value)

    # def __cjs_old__save(self, value):#pragma: no cover
    #     value = value.strip()
    #
    #     # self.request.user
    #     # organization=self.request.user.get_profile().get_organization()
    #     #
    #     if value:
    #         user = User.objects.filter(email=value.strip()).first()
    #
    #         kpi = self.obj
    #         parent = kpi.parent if kpi.parent else kpi.refer_to
    #         perm = False
    #         if parent:
    #
    #             # '''
    #             # Trường hợp 1: nếu user được phân công là cấp dưới của KPI cha
    #             # '''
    #             if parent.user and user and user.profile.is_subordinate_of(parent.user.profile):
    #                 perm = True
    #
    #             # '''
    #             # Trường hợp 2: nếu user được phân công chính là owner của KPI cha (tự phân công cho chính mình)
    #             # '''
    #             elif user and parent.user and user.id == parent.user.id:
    #                 perm = True
    #
    #             # '''
    #             # Trường hợp 3: nếu user được phân công ngang cấp với owner của KPI Cha (Phân công chéo)
    #             # '''
    #             # elif parent.user and user and parent.user.profile.report_to \
    #             #         and user.profile.is_subordinate_of(parent.user.profile.report_to.profile ):
    #             elif parent.user and user and parent.user.profile.parent \
    #                     and user.profile.is_subordinate_of(parent.user.profile.parent, directly=True):
    #                 perm = True
    #
    #         else:  # kpi nay la kpi cha doc lap, khong phai la duoc assign xuong
    #             perm = True
    #
    #         # if user and kpi.user and user.profile.is_subordinate_of(kpi.user.profile): # <-- duan note: trường hợp này bị sai, ví dụ trong taiga #310, user tạo kpi, assign cho user này, sau đó đổi ý assign tiếp cho user khác (2 user được assigned này cùng cấp)
    #         if user and kpi.user and perm:
    #
    #             AdaptorEmailField.save(self, value)
    #
    #             ############
    #             # Only allow to change to subordinate
    #             ############
    #
    #             organization = user.profile.get_organization()
    #             current_quarter = organization.get_current_quarter()
    #             if not self.before_email or not self.obj.user_id:  # trường hơp này sẽ luôn fail , không cần thiết nữa
    #                 # set user
    #                 self.obj.user = user
    #                 self.obj.quarter_period = current_quarter
    #                 self.obj.save()
    #                 self.obj.create_interchange_kpi()
    #                 # create interchange kpi
    #             elif (self.before_email != value and value and self.obj.user) or (
    #                 self.obj.user and self.obj.user.email != value):
    #
    #                 # nếu chuyển email từ email qua cho 1 người mới hoàn toàn mà không phải là cùng 1 người của KPI cha
    #                 # --> tạo KPI trung gian
    #                 if self.obj.refer_to and self.obj.refer_to.user is not None:  # con truc tiep hoac con duoc phan cong
    #                     # con truc tiep or con duoc phan cong
    #                     if self.before_email == self.obj.refer_to.user.email \
    #                             and value != self.obj.refer_to.user.email:
    #                         self.obj.create_interchange_kpi()
    #
    #                 # self.obj.create_interchange_kpi()
    #
    #                 self.obj.user = user
    #                 self.obj.quarter_period = current_quarter  # maybe no need
    #
    #                 # change email to Parent KPI email
    #                 # nếu đây là KPI trung gian, và sửa email mới  thành kpi.refer_to.email
    #                 # --> set parent = refer_to
    #                 # --> xoá KPI được phân công xuống dưới (cascaded_from)
    #                 if self.obj.refer_to and self.obj.refer_to.user and value == self.obj.refer_to.user.email:
    #                     self.obj.parent = self.obj.refer_to
    #                     if self.obj.cascaded_from_id:
    #                         self.obj.cascaded_from.delete()
    #                         self.obj.cascaded_from_id = None
    #
    #                 # nếu đây là KPI trung gian, nhưng email mới != với email của KPI cha
    #                 # --> tách KPI này ra thành 1 KPI riêng không còn quan hệ cha con (parent = None) nhưng vẫn giữ lại quan hệ refer_to
    #                 # --> Liên kế KPI này lên KPI cha: self.obj.assign_up(self.obj.refer_to)
    #                 # --> Lấy KPI trung gian: cascaded_from = self.obj.cascaded_from
    #
    #                 elif self.obj.refer_to:
    #                     self.obj.parent = None
    #                     self.obj.assign_up(self.obj.refer_to)
    #                     cascaded_from = self.obj.cascaded_from
    #                     if cascaded_from:
    #                         cascaded_from.assigned_to = user
    #                         cascaded_from.quarter_period = current_quarter
    #                         cascaded_from.save()
    #                 else:
    #                     self.obj.parent = None
    #                     self.obj.save()
    #
    #                 self.obj.save()
    #             else:
    #                 pass
    #         else:
    #             raise PermissionDenied
    #     else:
    #         pass
    #         # self.obj.user = None
    #         # if self.obj.cascaded_from_id:
    #         #     try:
    #         #         self.obj.cascaded_from.delete()
    #         #     except:
    #         #         pass
    #         # self.obj.cascaded_from = None
    #         # self.obj.parent = None
    #         # self.obj.save()
    #
    #         # USECASEs:
    #         # 1. kpi nay co kpi con duoc phan cho nguoi khac
    #         # --> nhung kpi con nay van ton tai va hien thi trong kpi-editor cua nguoi khac
    #         # 2. kpi nay co kpi con duoc phan cho chinh nguoi do
    #         # --> nhung kpi nay van ton tai nhung khong hien thi trong kpi-editor cua nguoi do nua
    #
    #         # self.obj.delete() ????

    def save(self, value):
        value = value.strip()

        # self.request.user
        # organization=self.request.user.get_profile().get_organization()
        #
        if value:
            user = User.objects.filter(email=value.strip()).first()
            if not user.profile.active:
                raise PermissionDenied(ugettext("You can't assign KPI to the disabled user"))

            kpi = self.obj
            if kpi.weight == 0:
                raise PermissionDenied(ugettext("You can't assign a delayed KPI to other user"))

            parent = kpi.parent if kpi.parent else kpi.refer_to
            perm = False
            if parent:

                # '''
                # Trường hợp 1: nếu user được phân công là cấp dưới của KPI cha
                # '''
                if parent.user and user and user.profile.is_subordinate_of(parent.user.profile):
                    perm = True

                # '''
                # Trường hợp 2: nếu user được phân công chính là owner của KPI cha (tự phân công cho chính mình)
                # '''
                elif user and parent.user and user.id == parent.user.id:
                    perm = True

                # '''
                # Trường hợp 3: nếu user được phân công ngang cấp với owner của KPI Cha (Phân công chéo)
                # '''
                # elif parent.user and user and parent.user.profile.report_to \
                #         and user.profile.is_subordinate_of(parent.user.profile.report_to.profile ):
                elif parent.user and user and parent.user.profile.parent \
                        and user.profile.is_subordinate_of(parent.user.profile.parent, directly=True):
                    perm = True

            else:  # kpi nay la kpi cha doc lap, khong phai la duoc assign xuong
                perm = True

            # if user and kpi.user and user.profile.is_subordinate_of(kpi.user.profile): # <-- duan note: trường hợp này bị sai, ví dụ trong taiga #310, user tạo kpi, assign cho user này, sau đó đổi ý assign tiếp cho user khác (2 user được assigned này cùng cấp)
            if user and kpi.user and perm:

                setattr(self.obj, self.field_name, value)
                track_change(self.request.user, self.obj)
                self.obj.save()

                ############
                # Only allow to change to subordinate
                ############

                organization = user.profile.get_organization()
                current_quarter = organization.get_current_quarter()
                if not self.before_email or not self.obj.user_id:  # trường hơp này sẽ luôn fail , không cần thiết nữa
                    # set user
                    self.obj.user = user
                    self.obj.quarter_period = current_quarter
                    self.obj.save()
                    self.obj.create_interchange_kpi()
                # docs: https://cloudjet.atlassian.net/wiki/spaces/CKD/blog/2017/09/14/1634354/Psuedo+workflow+when+assign+kpi+to+user
                elif (self.before_email != value and value and self.obj.user) or (
                    self.obj.user and self.obj.user.email != value):

                    self.obj.change_kpi_user(user, self.before_email)

                else:
                    pass
            else:
                raise PermissionDenied
        else:
            pass
            # self.obj.user = None
            # if self.obj.cascaded_from_id:
            #     try:
            #         self.obj.cascaded_from.delete()
            #     except:
            #         pass
            # self.obj.cascaded_from = None
            # self.obj.parent = None
            # self.obj.save()

            # USECASEs:
            # 1. kpi nay co kpi con duoc phan cho nguoi khac
            # --> nhung kpi con nay van ton tai va hien thi trong kpi-editor cua nguoi khac
            # 2. kpi nay co kpi con duoc phan cho chinh nguoi do
            # --> nhung kpi nay van ton tai nhung khong hien thi trong kpi-editor cua nguoi do nua

            # self.obj.delete() ????


class KPIDecriptionAdaptor(AdaptorTextAreaField, KPIFieldLog):#pragma: no cover

    def get_form_class(self):
        return KPIForm

    def get_form(self):
        form_class = self.get_form_class()
        return form_class(instance=self.obj,
                          initial=self.initial,
                          prefix=id(form_class))

    def save(self, value):
        AdaptorTextAreaField.save(self, value)
        # self.obj.name_vi = self.obj.name_vi or self.obj.name_en
        # self.obj.name_en = self.obj.name_vi
        # self.obj.description_vi = self.obj.description
        # self.obj.description_en = self.obj.description
        self.obj.save()


class KPIOrdering(AdaptorIntegerField):#pragma: no cover

    def render_value(self, field_name=None):
        delete_memoized_verhash(get_kpi_id)
        return str(self.obj.get_prefix_category()) + self.obj.get_kpi_id()  # + ("%d" % self.obj.ordering)


class KPIQuaterYearHeader(AdaptorIntegerField):#pragma: no cover

    def render_value(self, field_name=None, template_name="inplaceeditform/adaptor_quarter_header/render_value.html"):
        field_name = field_name or self.field_name_render

        # get quarter start
        target_organization_id = int(self.config.get('target_org'))
        target_organization = get_organization(
            target_organization_id)  # Organization.objects.get(id=target_organization_id)
        quarter_start = QuarterServices.get_quarter_start(target_organization)

        value = getattr(self.obj, field_name)

        return render_to_string(template_name, {'value': value, 'quarter_start': quarter_start})


# class KPIQuarterTargetAdaptor(AdaptorFloatField, KPIFieldLog):#pragma: no cover
#
#     def get_form_class(self):
#         return KPIDetailsForm
#
#     def get_form(self):
#         self.old_value = getattr(self.obj, self.field_name)
#         return AdaptorFloatField.get_form(self)
#
#     def save(self, value):
#         AdaptorFloatField.save(self, value)
#         from performance.models import KPI
#         cascaded_from = None
#         if self.field_name in ['quarter_one_target', 'quarter_two_target', 'quarter_three_target',
#                                'quarter_four_target']:
#             if self.obj.quarter_period_id and (
#                         (self.obj.quarter_period.quarter == 1 and self.field_name == 'quarter_one_target') or
#                         (self.obj.quarter_period.quarter == 2 and self.field_name == 'quarter_two_target') or
#                     (self.obj.quarter_period.quarter == 3 and self.field_name == 'quarter_three_target') or
#                 (self.obj.quarter_period.quarter >= 4 and self.field_name == 'quarter_four_target')):
#                 self.obj.target = value
#                 if self.obj.cascaded_from_id:
#                     try:
#                         cascaded_from = self.obj.cascaded_from
#                         cascaded_from.target = value
#                         cascaded_from.save()
#                     except KPI.DoesNotExist:
#                         self.obj.cascaded_from_id = None
#                         self.obj.save()
#         self.obj.save()
#         self.obj.update_score()
#
#         if not self.request.user.is_staff and self.obj.user_id:
#             field = unicode(self.obj._meta.get_field(self.field_name).verbose_name)
#             self.write_log(self.request.user, self.obj, field, self.old_value, value)
#

class KPIWeightAdaptor(AdaptorIntegerField):#pragma: no cover

    def get_form_class(self):
        return KPIWeightForm

    def get_form(self):
        form_class = self.get_form_class()
        return form_class(instance=self.obj,
                          initial=self.initial,
                          prefix=id(form_class))


class KPIUnitOperatorAdaptor(AdaptorTextField, KPIFieldLog):#pragma: no cover

    def get_form_class(self):
        return KPIDetailsForm

    def get_form(self):
        self.old_value = getattr(self.obj, self.field_name)
        return AdaptorTextField.get_form(self)

    def save(self, value):
        from performance.models import KPI

        setattr(self.obj, self.field_name, value)
        track_change(self.request.user, self.obj)
        self.obj.save()

        cascaded_from = None

        if self.obj.cascaded_from_id and not KPI.objects.filter(id=self.obj.cascaded_from_id).exists():
            self.obj.cascaded_from_id = None
            self.obj.save()

        if self.field_name == "unit":
            if self.obj.cascaded_from_id:
                cascaded_from = self.obj.cascaded_from
                cascaded_from.unit = value
                cascaded_from.save()

        # THE FOLLOWING CODES NOT USE ANYMORE
        # elif self.field_name == "operator":
        #     if self.obj.cascaded_from_id:
        #         cascaded_from = self.obj.cascaded_from
        #         cascaded_from.operator = value
        #         cascaded_from.save()
        #         self.obj.update_score()

        if not self.request.user.is_staff and self.obj.user_id:
            field = unicode(self.obj._meta.get_field(self.field_name).verbose_name)
            # self.write_log(self.request.user, self.obj, field, self.old_value, value)

    def render_value(self, field_name=None):
        value = super(AdaptorTextField, self).render_value(field_name)
        value = u'<div class="none-inplaceedit"><article>{}</article></div>'.format(value,)
        return safe(value)


class KPIGoalAdaptor(AdaptorTextAreaField):#pragma: no cover

    def get_form_class(self):
        return KPIDetailsForm

    def get_form(self):
        self.old_value = getattr(self.obj, self.field_name)
        return AdaptorTextAreaField.get_form(self)

    def save(self, value):
        setattr(self.obj, self.field_name, value)
        track_change(self.request.user, self.obj)
        self.obj.save()

        from performance.models import KPI
        if self.obj.cascaded_from_id and not KPI.objects.filter(id=self.obj.cascaded_from_id).exists():
            self.obj.cascaded_from_id = None
            self.obj.save()

        cascaded_from = None
        if self.obj.cascaded_from_id:
            cascaded_from = self.obj.cascaded_from
            cascaded_from.current_goal = value
            cascaded_from.save()

        if not self.request.user.is_staff and self.obj.user_id:
            field = unicode(self.obj._meta.get_field(self.field_name).verbose_name)
            # duplicate write comment
            # self.write_log(self.request.user, self.obj, field, self.old_value, value)

    def render_value(self, field_name=None):
        value = super(AdaptorTextAreaField, self).render_value(field_name)
        value = u'<div class="none-inplaceedit"><article>{}</article></div>'.format(value,)
        return safe(value)
