#!/usr/bin/python
# -*- coding: utf-8 -*-
import datetime

from django import forms
from django.contrib import admin
from django.contrib.auth.models import User
from django.db.models.query_utils import Q
from django.utils.translation import ugettext_lazy as _, ugettext_lazy

# from crm.models import Team
from performance.db_models.competency import Competency
from performance.db_models.kpi_library import JobTitle, KPILib
from performance.db_models.strategy_map import StrategyMap
from performance.db_models.timeline import TaskEveryStage, Timeline, KPITask
from performance.models import  KPI, QuarterPeriod, \
    REVIEW_TYPE, BSC_CATEGORY
from user_profile.models import Profile


class CompetencyForm(forms.ModelForm):

    class Meta:
        model = Competency
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(CompetencyForm, self).__init__(*args, **kwargs)
        # self.fields['belong_to_jobs'].queryset = JobTitle.objects.filter(category__industry__isnull=False)


class KPIForm(forms.ModelForm):

    class Meta:
        model = KPI
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(KPIForm, self).__init__(*args, **kwargs)
        if self.instance.cascaded_from_id:
            if not KPI.objects.filter(id=self.instance.cascaded_from_id).exists():
                self.instance.cascaded_from_id = None
        jobs = JobTitle.objects.filter(category__industry__isnull=False)
        # self.fields['belong_to_jobs'].queryset = jobs


class KPIDetailsForm(forms.ModelForm):

    class Meta:
        model = KPI
        fields = ('current_goal', 'future_goal', 'unit', 'operator', 'quarter_one_target',
                  'quarter_two_target', 'quarter_three_target', 'quarter_four_target')


class KPIAssignEmailForm(forms.ModelForm):

    class Meta:
        model = KPI
        fields = ('owner_email',)
        widgets = {
            'owner_email': forms.TextInput(attrs={'autocomplete': 'off'}),
        }

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(KPIAssignEmailForm, self).__init__(*args, **kwargs)

    def clean_owner_email(self):
        if self.cleaned_data['owner_email']:

            # TODO: check security: adding wrong email
            # if not self.request:
            #     raise forms.ValidationError("Something error!")
            # else:

               # organization = self.request.user.get_profile().get_organization()
               # if not UserOrganization.objects.filter(user__email=self.cleaned_data['owner_email'].lower(), organization=organization).exists():
                if not Profile.objects.filter(user__email=self.cleaned_data['owner_email'].lower()) .exists():
                    raise forms.ValidationError(ugettext_lazy("Email does not exist in the system."))
#             if not User.objects.filter(email=self.cleaned_data['owner_email']).exists():
#                 raise forms.ValidationError("Email does not e xist in the system.")

            # user = User.objects.get(email=self.cleaned_data['owner_email'])
#             if self.instance.refer_to:
#                 if not self.refer_to.user:
#                     raise forms.ValidationError("KPI parent must be assigned to a user.")

        return self.cleaned_data['owner_email'].lower()

    def save(self, commit=True):  # not trigger anymore

        # https://stackoverflow.com/questions/3927305/django-how-to-override-form-save
        instance = super(KPIAssignEmailForm, self).save(commit=True)
        return instance
        # correct kpi type

        # case the instance is child of other
        if instance.parent or instance.refer_to:
            if instance.parent:
                if instance.user_id != instance.parent.user_id:
                    pass

        # case kpi has childs
        if instance.has_children():  # get_children_refer()
            # loop through childrens & correct type of children
            pass


class KPIWeightForm(forms.ModelForm):

    class Meta:
        model = KPI
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(KPIWeightForm, self).__init__(*args, **kwargs)
        jobs = JobTitle.objects.filter(category__industry__isnull=False)
        # self.fields['belong_to_jobs'].queryset = jobs
        self.fields['weight'] = forms.IntegerField(max_value=100, min_value=0)

    def clean_weight(self):
        return self.cleaned_data['weight']


class PerformanceForm(forms.Form):
    name = forms.CharField(label=_('Name'), max_length=2000, required=False)
    description = forms.CharField(label=_('Description'), widget=forms.Textarea(), required=False)


class SmartKPIForm(forms.Form):
    name = forms.CharField(label=_('Name'), max_length=2000, required=False)
    description = forms.CharField(label=_('Description'), widget=forms.Textarea(), required=False)

    def __init__(self, is_bsc=False, *args, **kwargs):
        super(SmartKPIForm, self).__init__(*args, **kwargs)
        if is_bsc:
            self.fields['operator'] = forms.CharField(label=_('Operator'), required=False)
            self.fields['target'] = forms.FloatField(label=_('Target'), required=False)
            self.fields['unit'] = forms.CharField(label=_('Unit'), required=False)
            self.fields['deadline'] = forms.DateField(label=_('Deadline'), input_formats=["%d-%m-%Y", ],
                                                      widget=forms.DateInput(format='%d-%m-%Y'), required=False)
            self.fields['category'] = forms.ChoiceField(label=_('Cateogry'), choices=BSC_CATEGORY, required=False)


class DueDateForm(forms.Form):
    QUARTER_CHOICES = (
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
    )
    organization = forms.IntegerField(widget=forms.HiddenInput)
    due_date = forms.DateField(input_formats=["%d-%m-%Y", "%d/%m/%Y"],
                               widget=forms.DateInput(format='%d-%m-%Y'))
    quarter = forms.ChoiceField(choices=QUARTER_CHOICES)
    #  current_quarter = forms.ChoiceField(choices=QUARTER_CHOICES)
    year = forms.ChoiceField()
    quarter_name = forms.CharField(required=True, widget=forms.TextInput(attrs={ 'required': 'true' }),)

    #
    # def set_CURRENT_QUARTER_CHOICES(self,CURRENT_QUARTER_CHOICES):
    #
    #     current_year = datetime.datetime.now().year
    #     if not CURRENT_QUARTER_CHOICES:
    #         self.fields['current_quarter'].choices = (
    #             ('1', 'Quarter 1 - {0}'.format(current_year)),
    #             ('2', 'Quarter 2 - {0}'.format(current_year)),
    #             ('3', 'Quarter 3 - {0}'.format(current_year)),
    #             ('4', 'Quarter 4 - {0}'.format(current_year)),
    #         )
    #     else:
    #         self.fields['current_quarter'].choices  =CURRENT_QUARTER_CHOICES

    def __init__(self, *args, **kwargs):
        super(DueDateForm, self).__init__(*args, **kwargs)
        today = datetime.date.today()
        years = [(str(today.year - 1), str(today.year - 1)), (str(today.year), str(today.year)),
                 (str(today.year + 1), str(today.year + 1))]
        self.fields['year'].choices = years

    def clean_due_date(self):
        due_date = self.cleaned_data['due_date']
        # if due_date <= datetime.date.today():
        #     raise forms.ValidationError(u'Ngày đánh giá phải lớn hơn ngày hiện tại')

        organization = self.cleaned_data['organization']
        # FUCK THIS if QuarterPeriod.objects.filter(due_date__gte=due_date, organization_id=organization):
        #     raise forms.ValidationError(u'Ngày đánh giá phải nằm ngoài phạm vi đánh giá trước.')

        return due_date


class EditDueDateForm(forms.Form):
    organization = forms.IntegerField(widget=forms.HiddenInput)
    edit_due_date = forms.DateField(input_formats=["%d/%m/%Y", ],
                                    widget=forms.DateInput(format='%d/%m/%Y'))
    quarter_id = forms.IntegerField(widget=forms.HiddenInput)

    def clean_edit_due_date(self):
        due_date = self.cleaned_data['edit_due_date']
        # if due_date < datetime.date.today():
        #     raise forms.ValidationError(u'Ngày đánh giá phải lớn hơn ngày hiện tại')

        return due_date


class InvitationReviewForm(forms.Form):
    name = forms.CharField(label=_('Name'), widget=forms.TextInput(attrs={'autocomplete': 'off'}))
    email = forms.CharField(label=_('Email'))
    review_type = forms.ChoiceField(label=_("Type"),
                                    choices=(('', _('Please select a 360 category')),) + REVIEW_TYPE[2:])
    content = forms.CharField(label=_("Message to reviewer"), widget=forms.Textarea())
    reviewee = forms.CharField(widget=forms.HiddenInput())

    def clean_email(self):
        if User.objects.filter(email=self.cleaned_data['email']).exists():
            return self.cleaned_data['email']
        raise forms.ValidationError(u'Email không tồn tại trong hệ thống')


class GoalForm(forms.Form):
    OPERATOR_CHOICES = (
        (">=", ">="),
        ("<=", "<="),
        ("=", "="),
    )
    specific = forms.CharField(max_length=2000)
    measurable = forms.CharField(max_length=400, required=False)
    deadline = forms.DateField(input_formats=["%d-%m-%Y", ],
                               widget=forms.DateInput(format='%d-%m-%Y'),
                               required=False)
    operator = forms.ChoiceField(choices=OPERATOR_CHOICES, required=False)
    target = forms.FloatField(required=False)
    unit = forms.CharField(max_length=255, required=False)
    category = forms.ChoiceField(choices=BSC_CATEGORY, required=False)


class MemberForm(forms.Form):
    full_name = forms.CharField(label=_('Full name*'), max_length=200)
    email = forms.EmailField(label=_('E-mail*'), max_length=70)
    phone = forms.CharField(max_length=50, required=False, label=_('Phone'),)
    skype = forms.CharField(max_length=100, required=False, label=_('Skype'),)
    employee_code = forms.CharField(label=_('Employee code'), max_length=20, required=False)
    position = forms.CharField(label=_('Position*'), max_length=200)
    department = forms.CharField(label=_('Deparment*'), max_length=200, required=False)
    send_password = forms.BooleanField(label=_('Send password*'), initial=True)

    def __init__(self, organization, *args, **kwargs):
        self.organization = organization
        super(MemberForm, self).__init__(*args, **kwargs)

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email).exists():
            if Profile.objects.filter(~Q(organization=self.organization), user__email=email).exists():
                raise forms.ValidationError("Email already exist.")

            p = Profile.objects.get(user__email=email)
            if p.parent_id and Profile.objects.filter(user__email=email).exists():
                raise forms.ValidationError("Email already exist.")

                # if Team.objects.filter(~Q(organization=self.organization), user__email=email).exists():
                #     raise forms.ValidationError("Email already exist.")

        return self.cleaned_data['email']


class TimelineForm(forms.ModelForm):

    class Meta:
        model = Timeline
        exclude = ('organization', 'created_at', 'done')

    def __init__(self, organization, *args, **kwargs):
        super(TimelineForm, self).__init__(*args, **kwargs)
        self.instance.organization = organization
        self.fields['deadline'].input_formats = ["%d-%m-%Y", ]
        self.fields['deadline'].widget = forms.DateInput(format='%d-%m-%Y')


class TaskEveryStageForm(forms.ModelForm):

    class Meta:
        model = TaskEveryStage
        exclude = ('organization',)

    def __init__(self, organization, *args, **kwargs):
        super(TaskEveryStageForm, self).__init__(*args, **kwargs)
        self.instance.organization = organization
        choices = Timeline.objects.filter(organization=organization).values_list('id', 'stage')
        self.fields['stage'].choices = [('', u'-- Chọn giai đoạn --')] + list(choices)
        self.fields['due_date'].input_formats = ["%d-%m-%Y", ]
        self.fields['due_date'].widget = forms.DateInput(format='%d-%m-%Y')
        self.fields['end_date'].input_formats = ["%d-%m-%Y", ]
        self.fields['end_date'].widget = forms.DateInput(format='%d-%m-%Y')
        self.fields['date_range'].widget = forms.RadioSelect(choices=self.fields['date_range'].choices)


class KPITaskForm(forms.ModelForm):

    class Meta:
        model = KPITask
        fields = ('name',)

    def __init__(self, task, *args, **kwargs):
        super(KPITaskForm, self).__init__(*args, **kwargs)
        self.instance.task = task


class ImportKPIForm(forms.Form):
    employee = forms.ChoiceField()
    file_data = forms.FileField()

    def __init__(self, *args, **kwargs):
        organization = kwargs.pop('organization')
        users = kwargs.pop('users')
        super(ImportKPIForm, self).__init__(*args, **kwargs)
        self.fields['employee'].choices = users


class ImportKPIUserForm(forms.Form):
    file_data = forms.FileField()
    clear_old_data = forms.BooleanField(required=False)
#
#
# class CategoryKPIFeedbackAdminForm(forms.ModelForm):
#     class Meta:
#         model = CategoryKPIFeedback
#         exclude = []
#         widgets = {
#             'color': admin.widgets.AdminTextInputWidget(attrs={'type': 'color'})
#         }

#
# class KPIFieldErrorMappingForm(forms.ModelForm):
#     # http://stackoverflow.com/questions/14597937/show-multiple-choices-to-admin-in-django
#     field = forms.ChoiceField(widget=forms.Select(), choices=KPIFieldErrorMapping.kpi_list_fields())
#
#     class Meta:
#         model = KPIFieldErrorMapping
#         exclude = []
#
#     def __init__(self, *args, **kwargs):
#         super(KPIFieldErrorMappingForm, self).__init__(*args, **kwargs)
#


class StrategyMapForBranchForm(forms.ModelForm):

    class Meta:
        model = StrategyMap
        fields = ('director',)

    def __init__(self, *args, **kwargs):
        organization = kwargs.pop('organization')
        super(StrategyMapForBranchForm, self).__init__(*args, **kwargs)
        users = Profile.objects.filter(organization=organization).values_list('user_id', 'user__profile__display_name')
        self.fields['director'].choices = users


class KPILibForm(forms.ModelForm):

    class Meta:
        model = KPILib
        fields = ('objective', 'name', 'description', 'measurement_method', 'operator')

    def __init__(self, *args, **kwargs):
        super(KPILibForm, self).__init__(*args, **kwargs)
        for (field, name) in KPILib.EXTRA_FIELDS:
            self.fields[field] = forms.CharField(widget=forms.Textarea(), label=name)
