# !/usr/bin/python
# -*- coding: utf-8 -*-
import threading

from performance.db_models.competency import Competency
from performance.db_models.log import KpiLog
from performance.models import KPI


class KPILogger(threading.Thread):

    def __init__(self, editor, obj, status, description='', *args, **kwargs):
        super(KPILogger, self).__init__(*args, **kwargs)
        self.editor = editor
        self.obj = obj
        self.status = status

        if description is None:
            description = ''
        self.description = description

    def run(self):
        if isinstance(self.obj, KPI):
            log_type = 'kpi'
        elif isinstance(self.obj, Competency):
            log_type = 'competency'
        else:
            log_type = 'devplan'

        if isinstance(self.obj, KPI) or isinstance(self.obj, Competency):
            # #print self.obj.user
            try:
                self.kpi(log_type)
            except:
                pass
        else:
            self.devplan(log_type)

    def kpi(self, log_type):

        if self.obj.user_id and self.obj.user == self.editor:
            role_edit = 'self'
        # elif self.obj.user_id and self.obj.user and self.obj.user.get_profile().report_to == self.editor:
        elif self.obj.user_id and self.obj.user and self.obj.user.get_profile().parent == self.editor.profile:
            role_edit = 'manager'
        elif not self.obj.user_id:
            role_edit = ''
        else:
            role_edit = "admin"

        parent_profile = None
        if self.obj.user and self.obj.user.profile and self.obj.user.profile.parent:
            # report_to = self.obj.user.get_profile().report_to
            parent_profile = self.obj.user.profile.parent.user

        KpiLog.objects.create(editor=self.editor,
                              user=self.obj.user,
                              report_to=parent_profile,
                              #parent=parent_profile,
                              role_edit=role_edit,
                              status=self.status,
                              log_type=log_type,
                              description=self.description,
                              quarter=self.obj.quarter_period)


    def devplan(self, log_type):
        if self.obj.user == self.editor:
            role_edit = 'self'
        # elif self.obj.report_to == self.editor:
        elif self.obj.parent == self.editor.profile:
            role_edit = 'manager'
        else:
            role_edit = "admin"

        KpiLog.objects.create(editor=self.editor,
                              user=self.obj.user,
                              # report_to=self.obj.report_to,
                              parent=self.obj.parent,
                              role_edit=role_edit,
                              status=self.status,
                              log_type=log_type,
                              description=self.description,
                              quarter=self.obj.get_organization().get_current_quarter())

