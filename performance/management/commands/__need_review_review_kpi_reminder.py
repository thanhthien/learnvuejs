#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from company.models import Organization
from elms.settings import from_email
from performance.models import Review
import datetime
from django.template.loader import render_to_string
from user_profile.models import Profile
from django.core.mail.message import EmailMessage
from django.db.models.query_utils import Q
#from performance.cloner import PerformanceCloner
from performance.documents import KpiQuarterFinalScore
from django.db.models.aggregates import Avg

from user_profile.services.profile import send_mail_with_perm


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        organizations = Organization.objects.filter(enable_performance=True,
                                                    trial=False,
                                                    enable_email_notification=True)

        for organization in organizations:

            if organization.demo_account:
                continue

            quarter_period = organization.get_current_quarter()
            due_date = quarter_period.due_date
            today = datetime.date.today()
            days = (due_date - today).days
            if days == 7 or days == 1 or days == 3:
                email_users = list(Profile.objects.select_related('user') \
                                                  .filter(organization=organization,
                                                          parent_id__isnull=False) \
                                                  .values_list('user__email', flat=True))
                if organization.ceo:
                    email_users.append(organization.ceo.email)
                    print organization.ceo.email

                bccs = ['phi@cloudjetsolutions.com', 'thanhtruc@cloudjetsolutions.com', 'hong@cloudjetsolutions.com']
                for email in email_users:
                    title = u'Nhắc nhở đánh giá từ Cloudjet KPI'
                    message = render_to_string('performance/mails/reminder_' + str(days) + '_days.html', {
                        "due_date": due_date
                    })

                    if bccs:
                        bcc = bccs
                        bccs = None
                    else:
                        bcc = None

                    print email
                    msg = send_mail_with_perm(subject=title,
                                       body=message,
                                       from_email=from_email,
                                       to=[email],
                                       bcc=bcc)

            elif days == -4:
                self.send_statistic(organization, quarter_period)
            elif days == -5:
                email_users = list(Profile.objects.select_related('user') \
                                                  .filter(Q(is_admin=True) | Q(is_superuser=True),
                                                           organization=organization) \
                                                  .values_list('user__email', flat=True))

                bccs = ['phi@cloudjetsolutions.com', 'thanhtruc@cloudjetsolutions.com', 'tuan@cloudjetsolutions.com']
                for email in email_users:
                    title = u'Nhắc nhở mở đợt đánh giá mới từ Cloudjet KPI'
                    message = render_to_string('performance/mails/reminder_open_new_review.html', {
                    })

                    if bccs:
                        bcc = bccs
                        bccs = None
                    else:
                        bcc = None

                    print email
                    msg = send_mail_with_perm(subject=title,
                                       body=message,
                                       from_email='Cloudjet KPI <hello@cloudjetsolutions.com>',
                                       to=[email],
                                       bcc=bcc)
            elif days == -15:
                pass
#                 quarter_period.status = "AR"
#                 quarter_period.save()
#                 new_quarter_period = organization.get_current_quarter()
#                 cloner = PerformanceCloner(organization,
#                                            quarter_period,
#                                            new_quarter_period)
#                 cloner.start()

    def send_statistic(self, organization, quarter_period):
        email_users = list(Profile.objects.select_related('user') \
                                          .filter(Q(is_admin=True) | Q(is_superuser=True),
                                                         organization=organization) \
                                          .values_list('user__email', flat=True))

        reviews = Review.objects.filter(quarter_period=quarter_period,
                                        review_type='self',
                                        organization=organization)

        employee_not_review = reviews.filter(completion=0).count()
        employee_not_finished_review = reviews.filter(completion__gt=0, completion__lt=100).count()
        employee_finished_review = reviews.filter(completion=100).count()

        reviews = Review.objects.filter(quarter_period=quarter_period,
                                        review_type='manager',
                                        reviewee__isnull=False,
                                        organization=organization).values_list('reviewer').annotate(avg_completion=Avg("completion"))

        manager_not_review = reviews.filter(avg_completion=0).count()
        manager_not_finished_review = reviews.filter(avg_completion__gt=0, avg_completion__lt=100).count()
        manager_finished_review = reviews.filter(avg_completion=100).count()

        scores = KpiQuarterFinalScore.objects.filter(organization=organization.id,
                                                     quarter_period=quarter_period.id)
        people_1_1 = scores.filter(competency_final_score__lt=33.3,
                                   kpi_final_score__lt=33.3).count()
        people_1_2 = scores.filter(competency_final_score__gt=33.3,
                                   competency_final_score__lt=66.6,
                                   kpi_final_score__lt=33.3).count()
        people_1_3 = scores.filter(competency_final_score__gt=66.6,
                                   kpi_final_score__lt=33.3).count()

        people_2_1 = scores.filter(competency_final_score__lt=33.3,
                                   kpi_final_score__gt=33.3,
                                   kpi_final_score__lt=66.6).count()
        people_2_2 = scores.filter(competency_final_score__gt=33.3,
                                   competency_final_score__lt=66.6,
                                   kpi_final_score__gt=33.3,
                                   kpi_final_score__lt=66.6).count()
        people_2_3 = scores.filter(competency_final_score__gt=66.6,
                                   kpi_final_score__gt=33.3,
                                   kpi_final_score__lt=66.6).count()

        people_3_1 = scores.filter(competency_final_score__lt=33.3,
                                   kpi_final_score__gt=66.6).count()
        people_3_2 = scores.filter(competency_final_score__gt=33.3,
                                   competency_final_score__lt=66.6,
                                   kpi_final_score__gt=66.6).count()
        people_3_3 = scores.filter(competency_final_score__gt=66.6,
                                   kpi_final_score__gt=66.6).count()

        message = render_to_string('performance/mails/statistic_review.html', {
            "employee_not_review": employee_not_review,
            "employee_not_finished_review": employee_not_finished_review,
            "employee_finished_review": employee_finished_review,
            "manager_not_review": manager_not_review,
            "manager_not_finished_review": manager_not_finished_review,
            "manager_finished_review": manager_finished_review,
            "people_1_1": people_1_1,
            "people_1_2": people_1_2,
            "people_1_3": people_1_3,
            "people_2_1": people_2_1,
            "people_2_2": people_2_2,
            "people_2_3": people_2_3,
            "people_3_1": people_3_1,
            "people_3_2": people_3_2,
            "people_3_3": people_3_3,
        })

        bccs = ['phi@cloudjetsolutions.com', 'thanhtruc@cloudjetsolutions.com', 'hong@cloudjetsolutions.com']
        for email in email_users:
            title = u'Thống kê tình hình đánh giá và hiệu suất của toàn công ty'
            print email
            if bccs:
                bcc = bccs
                bccs = None
            else:
                bcc = None
            msg = send_mail_with_perm(subject=title,
                               body=message,
                               from_email=from_email,
                               to=[email],
                               bcc=bcc)


