# !/usr/bin/python
# -*- coding: utf-8 -*-
import random
import traceback
from time import sleep

from django.core.cache import cache
from django.core.management.base import BaseCommand
from elms import settings
from performance.management.commands.update_cascade_kpi_score import calculate_score
from performance.models import CascadeKPIQueue
from performance.services import KPIServices
from utils import notify_slack


# def calculate_score()

class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        calculate_score()
