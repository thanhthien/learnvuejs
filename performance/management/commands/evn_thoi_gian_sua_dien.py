# !/usr/bin/python
# -*- coding: utf-8 -*-
import datetime
import logging
import random
import traceback
from time import sleep

from django.core.cache import cache
from django.core.mail import mail_admins
from django.core.management.base import BaseCommand

from company.models import UnitCode, Organization
from elms import settings
from performance.models import CascadeKPIQueue, QuarterPeriod
from user_profile.models import Profile

import requests
import json
from utils.common import first_date_of_month, last_date_of_month

logger = logging.getLogger('django')

'''
USAGE:
python manage.py evn_thoi_gian_sua_dien
python manage.py evn_thoi_gian_sua_dien --quarter_period_id=123 --month=2

'''

class Command(BaseCommand):
    help = 'Update thoi gian sua dien online'
    ORG_ID=getattr(settings, 'EVN_ORG_ID', 1333)
    authorization_key_service_url = getattr(settings, 'EVN_AUTHORIAZATION_KEY_SERVICE_URL', '')
    data_service_url = getattr(settings, 'EVN_DATA_SERVICE_URL', '')
    # email_domain=getattr(settings, 'EVN_EMAIL_DOMAIN', None)

    time_type = ('ngayGiao', 'ngayHoanTat')
    # for ease to training & deploy, we only filter by ngayGiao
    request_time_type = 'ngayGiao'

    def add_arguments(self, parser):
        # Positional arguments
        # parser.add_argument('poll_id', nargs='+', type=int)

        # Named (optional) arguments
        # https://docs.python.org/2/library/argparse.html#action
        parser.add_argument(
            '--quarter_period_id',
            # action='store_true',
            action='store', # default action
            dest='quarter_period_id',
            default=-1,
            help='Quarter Period ID',
        )

        parser.add_argument(
            '--month',
            action='store',  # default action
            dest='month',
            default=1, # default month
            help='Month of KPI to update',
        )

    def handle(self, *args, **options):
        try:
            self.initiate(*args, **options)


            unit_codes=UnitCode.get_unit_codes(organization=self.organization)

            for uc in unit_codes:
                unit_code=uc.code

                authorization_key = self.get_authorization_key(self.authorization_key_service_url, unit_code, self.from_date, self.to_date,
                                                               self.request_time_type)
                service_data = self.get_service_data(self.data_service_url, authorization_key, unit_code, self.from_date, self.to_date,
                                                     self.request_time_type)


                for employee_service_data in service_data:
                    update_kpi_by_unique_code(employee_service_data, self.quarter_period, self.month_of_year)

        except Exception as ex:
            err = traceback.format_exc()
            mail_admins(subject="Failed to update kpi from EVN service", message=u"Error messasge:{}\nTraceback: {}".format(str(ex),err))

        mail_admins(subject=u'[CloudjetKPI-EVN Service] Lấy dữ liệu "Thời gian sửa điện online" thành công', message='Lấy dữ liệu "Thời gian sửa điện online" thành công')

    def initiate(self, *args, **options):
        print 'options:', options
        quarter_period = None
        month = 1

        # quarter_period.last_month >= current_month (realtime month) ??
        today = datetime.date.today()
        if options['quarter_period_id'] > 0:
            quarter_period_id = options['quarter_period_id']
            quarter_period = QuarterPeriod.objects.get(id=int(quarter_period_id))

            month = int(options['month'])
        else:
            org = Organization.objects.get(id=self.ORG_ID)
            quarter_period = org.get_current_quarter()

            is_passed, message = is_quarter_period_passed(quarter_period)
            if is_passed is True:
                # mail_admins(subject="KPI Service: Thoi gian sua dien online", message=message)
                raise Exception(message)

            month = (today.month % 3) + 1

        quarter = quarter_period.quarter
        # quarter=2, month=1 => month_of_year=(2-1)*3 + 1 = 4 (thang 4)
        month_of_year = (quarter - 1) * 3 + month

        firstdateofmonth = datetime.date(quarter_period.year, month_of_year, 1)

        # store variables
        self.from_date = firstdateofmonth
        self.to_date = last_date_of_month(firstdateofmonth)
        self.month_of_year=month_of_year
        self.organization = quarter_period.organization
        self.quarter_period = quarter_period




    def get_authorization_key(self, authorization_key_service_url, unit_code, from_date, to_date, request_time_type):
        ## get secret key
        # maChungThuc được tạo thành từ cấu trúc : md5(donVi+tuNgay+denNgay+secret_key) #old
        # maChungThuc được tạo thành từ cấu trúc : md5(donVi+tuNgay+denNgay+loaiThoiGian+secret_key) # new
        # hash_tring = "{}{}{}{}{}".format(unit_code, from_date_str, to_date_str, request_time_type, evn_service_secret_key)
        # print 'hash_tring:', hash_tring
        # m = hashlib.md5()
        # m.update(hash_tring)
        # authorization_key = m.hexdigest()
        from_date_str=from_date.strftime('%d/%m/%Y')
        to_date_str=to_date.strftime('%d/%m/%Y')
        hash_tring1 = "{}{}{}{}".format(unit_code, from_date_str, to_date_str, request_time_type, )

        r = requests.post(authorization_key_service_url, data={'chuoiMd5': hash_tring1})

        authorization_key = r.text.strip()

        return authorization_key

    def get_service_data(self, data_service_url, authorization_key, unit_code, from_date, to_date, request_time_type):
        ''' Sample postdata:
            donVi:PE0100
            tuNgay:15/01/2017
            denNgay:30/01/2017
            loaiThoiGian:ngayGiao
            maChungThuc:0b91b73a7d8b5e3df999bdd2eb1d3441
        '''
        from_date_str = from_date.strftime('%d/%m/%Y')
        to_date_str = to_date.strftime('%d/%m/%Y')
        data = {
            'donVi': unit_code,
            'tuNgay': from_date_str,
            'denNgay': to_date_str,
            'loaiThoiGian': request_time_type,
            'maChungThuc': authorization_key,
        }

        r = requests.post(data_service_url, data=data)
        print 'type(r.text): ', type(r.text)

        result = json.loads(r.text)
        print 'type(result): ', type(result)
        # print result

        # get nhan vien
        if result.get('code') == '1':
            return result['data']
        else:
            raise Exception('Error when try to get data from EVN service')

        return None


def update_kpi_by_unique_code(employee_service_data, quarter_period, month):
    # todo: test here
    unit_code = employee_service_data['donVi']
    employee_code = employee_service_data['maNhanVien']
    org=quarter_period.organization
    value = float(employee_service_data.get('thoiGianTrungBinh'))
    # p=Profile.objects.filter(organization=org, unit_code=unit_code, employee_code__iexact=item['maNhanVien']).first()
    # because of heavy performance when query with iexact, we avoid to use it
    # make sure there are existed employee
    p = Profile.objects.filter(organization=org, unit_code=unit_code, employee_code=employee_code).first()
    # p=Profile.objects.filter(organization=org, unit_code=unit_code).first()
    if p is not None:
        print "PROfile: {}".format(p.user.email)
        # update month_x for kpi sua dien
        from performance.services.kpi import update_score_kpi_code

        score = {
            # 'year':year,
            'month': month,
            'value': value,
            'unique_code': 'THOIGIANSUADIENONLINE_{}'.format(employee_code, ).lower(),  # unique_code is always lower
        }

        print score
        update_score_kpi_code(score, quarter_period)

def is_quarter_period_passed(quarter_period):
    # todo: test here
    # current month > quarter_period.last_month ??? ==> manual run cronjob with quarter & month
    # get last_month (year based) of quarter_period
    today=datetime.date.today()
    last_month_of_quarter_period = quarter_period.quarter * 3  # year based
    last_date_of_quarter_period = last_date_of_month(
        datetime.date(quarter_period.year, last_month_of_quarter_period, 01))
    last_date_of_realtime_month = last_date_of_month(today)

    if last_date_of_realtime_month > last_date_of_quarter_period:  # this mean quarter_period is passed regardless active or not
        first_month_of_quarter_period = last_month_of_quarter_period - 2
        first_date_of_quarter_period = datetime.date(quarter_period.year, first_month_of_quarter_period, 01)
        message = u"Error: Thời gian hiện tại ({}) đã vượt quá thời gian của Quý đánh giá (Quý: {} | {} - {})\n".format(
            today.strftime('%d/%m/%Y'), quarter_period.quarter, first_date_of_quarter_period.strftime('%d/%m/%Y'),
            last_date_of_quarter_period.strftime('%d/%m/%Y'))
        # mail_admins(subject="KPI Service: Thoi gian sua dien online", message=message)
        return True, message
        # raise Exception('Cannot initiate')
    else:
        return False, ''
