#!/usr/bin/python
# -*- coding: utf-8 -*-
import datetime
import random
import traceback

import requests

from django.contrib.auth.models import User
from django.core.mail.message import EmailMessage
from django.core.management.base import BaseCommand
from django.db.models import Q
from django.template.loader import render_to_string
from elms.settings import FROM_EMAIL
from performance.models import KpiComment, KPI
from performance.services.kpi_comment import group_comment_by_kpi
from performance.services.notification import send_notification, COMPLETE_REVIEW_VERB, NEW_COMMENT_VERB
from user_profile.services.profile import allow_sending_email, send_mail_with_perm

from elms import settings


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        today = datetime.datetime.today()

        last_2_days = today + datetime.timedelta(days=-1)  # 1 day only

        kcs = KpiComment.objects.filter(Q(is_user_notified=False) |
                                        Q(is_user_notified__isnull=True),
                                        # created_at__year=today.year,
                                        # created_at__month=today.month
                                        # created_at__day=today.day
                                        ).order_by("?")[:50]

        print 'ksc count {}'.format(kcs.count())

        users = User.objects.filter(id__in=kcs.values_list('kpi__user_id', flat=True))
        for user in users:
            profile = user.get_profile()
            # manager = user.get_profile().report_to
            manager = user.profile.parent.user if user.profile.parent else None

            to_list = [user.email]
            if manager and manager.email:
                to_list.append(manager.email)

            notes = KpiComment.objects.filter(kpi__user=user, is_user_notified=False  # ,
                                              # created_at__year=today.year,
                                              # created_at__month=today.month,
                                              # created_at__day=today.day
                                              ).order_by('kpi_id', '-id')  # [:5]

            print "Notes count ", notes.count()
            if notes.count() > 0:
                print "Allow send ", allow_sending_email(profile)
                if not allow_sending_email(profile):  # .get_organization().demo_account:
                    for note in notes:
                        note.is_user_notified = True
                        note.save()
                elif not profile.last_kpicomment_notified or (profile.last_kpicomment_notified <= last_2_days):

                    print "Starting send.........."
                    profile.last_kpicomment_notified = datetime.datetime.now()
                    profile.save()

                    kpi = notes[0].kpi
                    for note in notes:
                        note.is_user_notified = True
                        note.save()

                    kpi_comments = group_comment_by_kpi(notes)

                    # grouped_comments = []
                    # for kc in kpi_comments:
                    #     grouped_comment = {}
                    #     grouped_comment = {}
                    #
                    #     grouped_comments.append(grouped_comment)

                    # print "send notification to {}".format(to_list[0])
                    try:
                        context = {
                            'notes': notes,
                            'profile': profile,
                            'STATIC_URL': settings.STATIC_URL,
                        }
                        message = render_to_string('mail/performance/kpicomment-notification.html', context)
                        title = u"KPI Comment: " + kpi.get_name().replace('\n', '')

                        r = random.randint(1, 100)
                        if r <= 8:
                            msg = send_mail_with_perm(subject=title,
                                               body=message,
                                               from_email=FROM_EMAIL,
                                               to=to_list,
                                               bcc=['hello@cloudjetsolutions.com']
                                               )
                        else:
                            msg = send_mail_with_perm(subject=title,
                                               body=message,
                                               from_email=FROM_EMAIL,
                                               to=to_list
                                               )

                    except:
                        print traceback.format_exc()

                    # for note in notes:

                    for key, value in kpi_comments.iteritems():

                        kpi = KPI.objects.filter(id=key).first()
                        if kpi:
                            description = kpi.name + ": "
                            description += value

                            send_notification(user, user, recipient=user,
                                              verb=NEW_COMMENT_VERB,
                                              target=user,
                                              action_object=kpi,
                                              description=description, organization=user.profile.organization.id)

        requests.get("https://hchk.io/26c9d7e4-591c-45b4-b150-01a52c77cc09")
