#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from performance.report import KPIReport


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        report = KPIReport()
        report.weekly_report()
