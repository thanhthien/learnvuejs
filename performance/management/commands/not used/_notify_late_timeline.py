#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand

from performance.services.timeline import notify_late_timeline


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        notify_late_timeline()