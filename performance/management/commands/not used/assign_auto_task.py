#!/usr/bin/python
# -*- coding: utf-8 -*-
import datetime

from dateutil.relativedelta import relativedelta

from company.models import Organization
from crm.models import Deal, Pipeline, Activity
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        org = Organization.objects.get(pk=16)
        pipeline = Pipeline.objects.get(organization=org, pipeline_id=10)
        deals = Deal.objects.filter(organization=org, stage__pipeline=pipeline, status='0')
        for deal in deals:
            due_date = datetime.date.today() + relativedelta(days=1)
            activity = Activity.objects.create(title=u'Gọi điện cho khách hàng để thông báo về việc dùng thử',
                                               assigned_to=deal.owner,
                                               activity_type='task',
                                               note=u"Gọi điện cho khách hàng để thông báo về việc dùng thử và hướng dẫn họ cách cung cấp thông tin (danh sách nhân viên)",
                                               due_date=due_date,
                                               creator=deal.owner,
                                               content_object=deal,
                                               organization_id=deal.organization_id)
            deal.next_activity_date = activity.get_due_datetime()
            deal.save()

            due_date = datetime.date.today() + relativedelta(days=1)
            activity = Activity.objects.create(title=u'Email mẫu danh sách nhân viên cho khách hàng',
                                               assigned_to=deal.owner,
                                               activity_type='task',
                                               note=u"Email mẫu danh sách nhân viên cho khách hàng",
                                               due_date=due_date,
                                               creator=deal.owner,
                                               content_object=deal,
                                               organization_id=deal.organization_id)

            due_date = datetime.date.today() + relativedelta(days=3)
            activity = Activity.objects.create(title=u'Nhận danh sách nhân viên và thiết lập hệ thống cho khách hàng',
                                               assigned_to=deal.owner,
                                               activity_type='task',
                                               note=u"Nhận danh sách nhân viên và thiết lập hệ thống cho khách hàng",
                                               due_date=due_date,
                                               creator=deal.owner,
                                               content_object=deal,
                                               organization_id=deal.organization_id)

            due_date = datetime.date.today() + relativedelta(days=4)
            activity = Activity.objects.create(title=u'Hẹn lịch để tiến hành mini training',
                                               assigned_to=deal.owner,
                                               activity_type='task',
                                               note=u"Hẹn lịch để tiến hành mini training",
                                               due_date=due_date,
                                               creator=deal.owner,
                                               content_object=deal,
                                               organization_id=deal.organization_id)

            due_date = datetime.date.today() + relativedelta(days=6)
            activity = Activity.objects.create(title=u'Tiến hành mini training',
                                               assigned_to=deal.owner,
                                               activity_type='task',
                                               note=u"Tiến hành mini training",
                                               due_date=due_date,
                                               creator=deal.owner,
                                               content_object=deal,
                                               organization_id=deal.organization_id)

            due_date = datetime.date.today() + relativedelta(days=9)
            activity = Activity.objects.create(title=u'Review KPI',
                                               assigned_to=deal.owner,
                                               activity_type='task',
                                               note=u"Review KPI",
                                               due_date=due_date,
                                               creator=deal.owner,
                                               content_object=deal,
                                               organization_id=deal.organization_id)

            due_date = datetime.date.today() + relativedelta(days=15)
            activity = Activity.objects.create(title=u'Kiểm tra tình hình sử dụng của khách hàng',
                                               assigned_to=deal.owner,
                                               activity_type='task',
                                               note=u"Kiểm tra tình hình sử dụng của khách hàng",
                                               due_date=due_date,
                                               creator=deal.owner,
                                               content_object=deal,
                                               organization_id=deal.organization_id)

            due_date = datetime.date.today() + relativedelta(days=20)
            activity = Activity.objects.create(title=u'Lập báo cáo về tình hình sử dụng và chuyển sang cho bộ phận kinh doanh',
                                               assigned_to=deal.owner,
                                               activity_type='task',
                                               note=u"Lập báo cáo về tình hình sử dụng và chuyển sang cho bộ phận kinh doanh",
                                               due_date=due_date,
                                               creator=deal.owner,
                                               content_object=deal,
                                               organization_id=deal.organization_id)
