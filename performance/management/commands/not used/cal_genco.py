#!/usr/bin/python
# -*- coding: utf-8 -*-
import threading

from django.core.management.base import BaseCommand
from company.models import Organization
from performance.models import KPI, QuarterPeriod
import requests


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        current_quarter = QuarterPeriod.objects.get(id=12)
        kpis = KPI.objects.filter(quarter_period=current_quarter, month_2=2.5, month_1=None, month_1_target=0,
                                  month_2_target=0, month_3_target=0)
        print "kpis count {}".format(kpis.count())
        i = 0
        for kpi in kpis:
            i = i + 1
            r = requests.get('http://cloudjet-dev.herokuapp.com/api/tempkpi/?kpi_id=' + str(kpi.id))
            if (r.status_code == 200):
                data = r.json()
                kpi.month_1 = data['month_1']
                kpi.month_2 = data['month_2']

                kpi.month_1_target = data['month_1_target']
                kpi.month_2_target = data['month_2_target']
                kpi.month_3_target = data['month_3_target']

                kpi.target = data['target']

                if (kpi.month_3 == None):
                    kpi.month_3 = data['month_3']

                print "kpi info: {}".format(kpi.id)
                kpi.save()

            print "kpis fixed count {}".format(i)
