#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand

from company.models import Organization
from performance.models import KPI, QuarterPeriod
from utils import unsigned_vi
import time
from user_profile.models import Profile
from user_profile.templatetags.user_tags import display_name


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        organization = Organization.objects.get(id=args[0])
        current_quarter = QuarterPeriod.objects.get(id=args[1])
        print unsigned_vi(organization.name), current_quarter

        self.show_kpi_not_match(organization.ceo, current_quarter)

    def show_kpi_not_match(self, user, quarter_period):
        print '-----------------------------------------'
        print unsigned_vi(display_name(user)), user.email
        kpis = KPI.objects.filter(user=user, quarter_period=quarter_period, parent=None)
        for kpi in kpis:
            child = kpi.get_children()
            temp = KPI.objects.filter(refer_to=kpi)
            if child.count() != temp.count():
                print unsigned_vi(kpi.name), kpi.id
                time.sleep(2)

        # profile = Profile.objects.filter(report_to=user)
        profile = Profile.objects.filter(parent=user.profile)
        for p in profile:
            self.show_kpi_not_match(p.user, quarter_period)
