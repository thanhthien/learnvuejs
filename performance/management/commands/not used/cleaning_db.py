from company.models import Organization
from django.core.management import BaseCommand
from performance.services.CascadeKPIQueue import cleaning_db


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
         orgs = Organization.objects.filter(name__icontains='WS1')
         for org in orgs:
             org.demo_account = True
             org.save()


         cleaning_db()