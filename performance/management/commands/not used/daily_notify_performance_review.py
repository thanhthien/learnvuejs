#!/usr/bin/python
# -*- coding: utf-8 -*-
from datetime import datetime, timedelta

from company.models import Organization
from django.core.management.base import BaseCommand
from elms import settings
import requests
from utils import percent_to_score


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):

        if datetime.now().day % 3 != 0:
            return


        ten_days_after = (datetime.now() + timedelta(days = 10)).date()
        #print ten_days_before
        slack_token = settings.SLACK_TOKEN

        url = u'https://slack.com/api/chat.postMessage?token=' + slack_token
        url += u"&channel=" + settings.SLACK_CS_CHANNEL
        message =u''




        orgs = Organization.objects.filter(enable_performance = True, demo_account = False)
        i = 1
        for org in orgs:
         #   print org
            if i == 1:
                message = u'-------------------- \r\n\r\n '
                message +=  u" Những công ty đang chuẩn bị đánh giá hoặc hết hạn đánh giá \r\n"


                data = {'text':message}
                r = requests.post(url, data=data)


            i+= 1


            current_quarter = org.get_current_quarter()
            if current_quarter.due_date < ten_days_after: #and current_quarter.due_date < (ten_days_after + timedelta(days=30)):
             #   print(str(org.id) + " " + unicode(org))
               # pass
                if current_quarter.due_date < datetime.now().date():
                    message += u' alert: `Công ty này đã quá hạn. team liên hệ xử lý để tránh lost khách hàng`'

                message = str(org.id) + " "  +  unicode(current_quarter)
                score = (org.ceo.profile.score_full_kpi_percent(current_quarter))
                message += u' [Company Performance Score: {0}%]'.format(score)



                data = {'text':message}
                r = requests.post(url, data=data)


