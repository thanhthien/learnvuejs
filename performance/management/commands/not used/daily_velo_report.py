#!/usr/bin/python
# -*- coding: utf-8 -*-
import json

import datetime



import intercom
import requests
from django.core.management.base import BaseCommand
from elms import settings
from utils import notify_slack


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        headers = {'X-TrackerToken': '6fd0d7e6f3b87492c5664443549da860'}

        seven_days = datetime.datetime.now() + datetime.timedelta(days=-6)
        seven_days = seven_days.date().isoformat()


        r = requests.get('https://www.pivotaltracker.com/services/v5/projects/1327980/history/days?start_date={}'.format(seven_days),
                         headers=headers)

        days = json.loads(r.content)['data']

        total_points = days[-1][1] - days[0][1]

        message = "***** TOTAL POINTS for last 7 days: *{}* *******" \
                  "\r\nhttps://www.pivotaltracker.com/n/projects/1327980 ".format(total_points)

        notify_slack( settings.SLACK_DEV_CHANNEL,message )
        #print message
        requests.get("https://hchk.io/db359824-871a-49bd-b29a-bf7259c20d9e")

