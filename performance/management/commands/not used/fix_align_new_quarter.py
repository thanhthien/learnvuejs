#!/usr/bin/python
# -*- coding: utf-8 -*-
import json
import pdb
import time
from optparse import make_option

import intercom
import requests
from company.models import Organization
from django.core.management.base import BaseCommand
from performance.cloner import align_kpi_to_new_quarter
from performance.data.fix_kpi import fix_target_not_synced
from user_profile.models import Profile
from user_profile.services.user import get_user_by_email


class Command(BaseCommand):
    args = ''
    help = ''

    option_list = BaseCommand.option_list + (
        make_option(
            "-e",
            "--email",
            dest="email",
            help="specify email",

        ),
    )

    def handle(self, *args, **options):
       # print options['email']

        try:
            email = options['email']
        except:
            print "Please run with command : python manage.py fix_align_new_quarter -e email@domain.com"
            return False

        user = get_user_by_email(email)
        if not user:
            print "User with email {} not found".format(email)
            return False

        new_quarter = user.profile.get_current_quarter()
        old_quarter = new_quarter.clone_from_quarter_period

        print "*"*60
        print "New Quarter ID: {}".format(new_quarter.id)
        print "Old Quarter ID: {}".format(old_quarter.id)
        print "*"*60

        uo = Profile.objects.select_related('user').filter(organization=new_quarter.organization)
        for p in uo:
            align_kpi_to_new_quarter(p.user, old_quarter, new_quarter, new_quarter.organization)


        print "*" * 60
        print "Done."
        print "Please Check /challenge-make-strong-soldier/performance/alignlog/"
        print "*" * 60
