#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand

from performance.models import KPI, KpiComment
from django.db.models.aggregates import Count
from django.db import connection, transaction
import uuid


class Command(BaseCommand):
    args = ''
    help = ''

    @transaction.atomic
    def handle(self, *args, **options):
        cursor = connection.cursor()
#         cursor.execute("""select unique_key from performance_kpi
#                           where unique_key <> '' and user_id in (select user_id from company_userorganization where organization_id = 65)
#                           group by unique_key
#                           having count(unique_key) > 1""")

        cursor.execute("""select id from performance_kpi where unique_key in
                        (select a.unique_key from performance_kpi as a
                        where a.unique_key <> '' and a.user_id in (select c.user_id from company_userorganization as c where c.organization_id = 47) and quarter_period_id = 63
                        and a.id not in ( select kpi_id from performance_kpicomment)
                        group by a.unique_key
                        having count(a.unique_key) > 1) and id not in ( select kpi_id from performance_kpicomment)""")

        rows = cursor.fetchall()
        if rows:
            unique_keys = zip(*rows)[0]
            count = 0
            kpis = KPI.objects.filter(id__in=unique_keys)
            for kpi in kpis:
                kpi.unique_key = str(uuid.uuid1())
                kpi.save()
                count += 1

            print 'fix: ' + str(count)

#             for comment in KpiComment.objects.filter(kpi_unique_key__in=unique_keys):
#                 comment.kpi_unique_key = comment.kpi.unique_key
#                 comment.save()
