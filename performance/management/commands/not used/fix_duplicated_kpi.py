#!/usr/bin/python
# -*- coding: utf-8 -*-
import json
import logging
import pdb
import time
from optparse import make_option

import intercom
import requests
from company.models import Organization
from django.core.management.base import BaseCommand
from performance.cloner import align_kpi_to_new_quarter
from performance.data.fix_kpi import fix_target_not_synced
from performance.models import KPI, kpi_cha_duoc_phan_cong, kpi_con_normal, kpi_trung_gian
from performance.services import remove_kpi, get_system_user
from user_profile.services.user import get_user_by_email

logger = logging.getLogger("django");
class Command(BaseCommand):
    args = ''
    help = ''

    option_list = BaseCommand.option_list + (
        make_option(
            "-q",
            "--quarter_id",
            dest="quarter_id",
            help="specify quarter_id",

        ),
    )

    def handle(self, *args, **options):
        # print options['email']

        try:
            quarter_id = options['quarter_id']
        except:
            print "Please run with command : python manage.py fix_duplicated_kpi -q quarter_id"
            # Handspan id : 1873
            return False

        logger.info("fix KPI for quarter: {}".format(quarter_id))



        kpis = KPI.objects.filter(quarter_period_id=quarter_id).order_by('id')
        for kpi in kpis:
            fix_duplicate(kpi)



def fix_duplicate(kpi):
    system_user = get_system_user()

    duplicated = KPI.objects.filter(quarter_period_id = kpi.quarter_period_id,
                                    name=kpi.name,
                                    user=kpi.user).exclude(id=kpi.id).first()
    if duplicated:

        if kpi.kpi_type_v2() == kpi_cha_duoc_phan_cong:

            if kpi.get_children().count() == 0:
                # kpi is duplicated
                print "kpi.get_children().count() == 0:"
                print "DELETE KPI_ID  {} | {} | {} ".format(kpi.id, kpi.name, kpi.user)
                remove_kpi(duplicated, system_user)
            elif kpi.get_children().count() == 0 and duplicated.get_children().count() == 0:
                print "kpi.get_children().count() == 0 and duplicated.get_children().count() == 0"
                print "DELETE KPI_ID  {} | {} | {} ".format(kpi.id, kpi.name, kpi.user)
                remove_kpi(duplicated, system_user)

        if kpi.kpi_type_v2() == kpi_con_normal:
            if duplicated.kpi_type_v2() == kpi_trung_gian:
                print "kpi.kpi_type_v2() == kpi_con_normal"
                print "DELETE KPI_ID  {} | {} | {} ".format(kpi.id, kpi.name, kpi.user)
                remove_kpi(kpi, system_user)

    else:

        print 'not duplicated '




def fix_mobi():
    kpis_dot5 = KPI.objects.filter(quarter_period_id = 5)

    for k in kpis_dot5:
        k_dot6 = KPI.objects.filter( copy_from = k).first()

        if k_dot6:
            # set ket qua month_1 cua dot 5 len dot 6
            k_dot6.month_1 = k.month_1
            k_dot6.save()
