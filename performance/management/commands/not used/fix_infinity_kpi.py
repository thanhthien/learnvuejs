#!/usr/bin/python
# -*- coding: utf-8 -*-
import json
import logging
import pdb
import time
from optparse import make_option

import intercom
import requests
from company.models import Organization
from django.core.management.base import BaseCommand
from performance.cloner import align_kpi_to_new_quarter
from performance.data.fix_kpi import fix_target_not_synced
from performance.models import KPI, kpi_cha_duoc_phan_cong, kpi_con_normal, kpi_trung_gian
from performance.services import remove_kpi, get_system_user
from user_profile.services.user import get_user_by_email

logger = logging.getLogger("django");
class Command(BaseCommand):
    args = ''
    help = ''

    option_list = BaseCommand.option_list + (
        make_option(
            "-q",
            "--quarter_id",
            dest="quarter_id",
            help="specify quarter_id",

        ),
    )

    def handle(self, *args, **options):
        # print options['email']

        try:
            quarter_id = options['quarter_id']
        except:
            print "Please run with command : python manage.py fix_duplicated_kpi -q quarter_id"
            # Handspan id : 1873
            return False

        logger.info("fix KPI for quarter: {}".format(quarter_id))



        kpis = KPI.objects.filter(quarter_period_id=quarter_id).order_by('id')
        logger.info("Got the kpis")

        for kpi in kpis:
            check_loop(kpi)



def check_loop(kpi):
   # logger.info(" check {}".format(kpi.id))
    
    temp = kpi
    
    i = 0
    while i < 20:
        
        if temp.cascaded_from_id:
            temp = temp.cascaded_from
            i += 1;
        else:
            break
        
    if i > 18:
        logger.info('duplicated {} '.format(kpi.id))