#!/usr/bin/python
# -*- coding: utf-8 -*-
import datetime

from django.core.management.base import BaseCommand
from performance.models import KPI


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        today = datetime.datetime.now()
        #one_month_ago = today - datetime.timedelta(days = -30)


        kpis =  KPI.objects.filter( real = 0,  quarter_period__status = "IN" )
        print "total {}".format(kpis.count())

        for k in kpis:
            print k.id
            k.save()
            #CascadeKPIQueue.objects.create(kpi=self.parent)


