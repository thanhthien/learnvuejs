#!/usr/bin/python
# -*- coding: utf-8 -*-
import datetime

from django.core.management.base import BaseCommand
from inplaceeditform import settings
from performance.models import KPI
from utils import notify_slack


##### MOVED TO fix_kpi.py

class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        start = datetime.datetime.now()


        #!/usr/bin/python
        ks = KPI.objects.filter(bsc_category__isnull = True)


        for k in ks:
            print k.id
            if k.parent:
                k.bsc_category = k.parent.bsc_category

            elif k.refer_to:
                k.bsc_category = k.refer_to.bsc_category
            else:
                k.bsc_category = 'financial'


            k.save()
        end = datetime.datetime.now()
        notify_slack(settings.SLACK_CRONJOB_CHANNEL, 'fix_null_bsc_category.py {0} seconds'.format((end-start).seconds))

