#!/usr/bin/python
# -*- coding: utf-8 -*-
import json
import pdb
import time

import intercom
import requests
from company.models import Organization
from django.core.management.base import BaseCommand
from performance.data.fix_kpi import fix_target_not_synced


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):

        try:
            org_id = args[0]
        except:
            pass

        if org_id:
            org = Organization.objects.get(id = org_id)

            current_quarter = org.get_current_quarter()

            users = org.get_users()
            for u in users:
                print "User: {0}\n".format(u.email)
                for k in u.profile.get_kpis():

                    print u"--- KPI: {0}\n".format(k.name)

                    fix_target_not_synced(k,current_quarter )

                    #
                    # if current_quarter.quarter == 1 and k.target != k.quarter_one_target:
                    #     print "k.target {0}".format(k.target)
                    #     print "k.quarter_one_target {0}".format(k.quarter_one_target)
                    #     print "k.quarter_two_target {0}".format(k.quarter_two_target)
                    #     print "k.quarter_three_target {0}".format(k.quarter_three_target)
                    #     print "k.quarter_four_target {0}".format(k.quarter_four_target)
                    #     #pdb.set_trace()
                    #     if k.quarter_one_target is not None:
                    #
                    #         print "----- k.quarter_one_target is not None"
                    #
                    #         k.target = k.quarter_one_target
                    #     elif k.target is not None:
                    #
                    #         print "----- k.target is not None"
                    #         k.quarter_one_target = k.target
                    #
                    #     k.save()
                    #
                    # if current_quarter.quarter == 2 and k.target != k.quarter_two_target:
                    #
                    #     print "k.target {0}".format(k.target)
                    #     print "k.quarter_one_target {0}".format(k.quarter_one_target)
                    #     print "k.quarter_two_target {0}".format(k.quarter_two_target)
                    #     print "k.quarter_three_target {0}".format(k.quarter_three_target)
                    #     print "k.quarter_four_target {0}".format(k.quarter_four_target)
                    #
                    #     if k.quarter_two_target is not None:
                    #         k.target = k.quarter_two_target
                    #     elif k.target is not None:
                    #         k.quarter_two_target = k.target
                    #
                    #
                    #     k.save()
                    #
                    # if current_quarter.quarter == 3 and k.target != k.quarter_three_target:
                    #
                    #     print "k.target {0}".format(k.target)
                    #     print "k.quarter_one_target {0}".format(k.quarter_one_target)
                    #     print "k.quarter_two_target {0}".format(k.quarter_two_target)
                    #     print "k.quarter_three_target {0}".format(k.quarter_three_target)
                    #     print "k.quarter_four_target {0}".format(k.quarter_four_target)
                    #
                    #     if k.quarter_three_target is not None:
                    #         k.target = k.quarter_three_target
                    #     elif k.target is not None:
                    #         k.quarter_three_target = k.target
                    #
                    #     k.save()
                    #
                    # if current_quarter.quarter == 4 and k.target != k.quarter_four_target:
                    #
                    #     print "k.target {0}".format(k.target)
                    #     print "k.quarter_one_target {0}".format(k.quarter_one_target)
                    #     print "k.quarter_two_target {0}".format(k.quarter_two_target)
                    #     print "k.quarter_three_target {0}".format(k.quarter_three_target)
                    #     print "k.quarter_four_target {0}".format(k.quarter_four_target)
                    #
                    #     if k.quarter_four_target is not None:
                    #         k.target = k.quarter_four_target
                    #     elif k.target is not None:
                    #         k.quarter_four_target = k.target
                    #
                    #     k.save()

