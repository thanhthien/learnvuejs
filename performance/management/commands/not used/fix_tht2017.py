#!/usr/bin/python
# -*- coding: utf-8 -*-
import threading

from django.core.management.base import BaseCommand
from company.models import Organization
from performance.models import KPI, QuarterPeriod
import requests


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        
        current_quarter = QuarterPeriod.objects.get(id=2017)
        kpis = KPI.objects.filter( quarter_period=current_quarter, month_1=35.800, month_1_target = 3, month_2_target = 3, month_3_target = 3)
        
        print "kpis count {}".format(kpis.count())
        i = 1
        for kpi in kpis:
            
            print "i = {} | id {}".format(i, kpi.id)
            i += 1
            
            r = requests.get('http://cjs-app.herokuapp.com/api/tempkpi/?kpi_id=' + str(kpi.id))
            print r.status_code
            
            if (r.status_code == 200):
                
                data = r.json()
                
                print "*" * 20
                print data['month_1']
                
                kpi.month_1 = data['month_1']
                kpi.month_2 = data['month_2']
                
                if (kpi.month_3 == 16.600):
                    kpi.month_3 = data['month_3']
                    
                kpi.month_1_target = data['month_1_target']
                kpi.month_2_target = data['month_2_target']
                kpi.month_3_target = data['month_3_target']

                kpi.target = data['target']
                
                kpi.real = data['real']
                
                kpi.latest_score = data['latest_score']

                kpi.save_calculate_score(calculate_now_thread = True)
            else:
                print "error id {}".format(kpi.id)
                
            
            


