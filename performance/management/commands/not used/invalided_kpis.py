# !/usr/bin/python
# -*- coding: utf-8 -*-

import requests
from company.models import Organization
from django.core.management.base import BaseCommand
from elms import settings
from performance.models import KPI


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        
        return 'no need any more'
        
        self.slack_token = settings.SLACK_TOKEN
        self.url = u'https://slack.com/api/chat.postMessage?token=' + self.slack_token
        self.url += u"&channel=" + settings.SLACK_INVALIDED_KPI_CHANNEL
        self.break_line='\r\n ***************************\r\n'
        self.break_char='\r\n'
        self.indent_text='--------'
        data={'text':u"Run from `invalided_kpis.py` {}".format(self.break_line)}
        r = requests.post(self.url, data=data)
        if len(args) > 0:
            org = Organization.objects.get(id=args[0])
            data = {'text': u'Run for only one organization: {}'.format(org.name,)}
            r = requests.post(self.url, data=data)
            self.check_invalided_kpis(org)
        else:
            orgs = Organization.objects.filter(demo_account=False, enable_performance = True).order_by('?')[:100]
            print u'Total organizations: {}'.format(orgs.count())
            data = {'text': u'Total organizations: {}'.format(orgs.count())}
            r = requests.post(self.url, data=data)

            i=0
            for org in orgs:
                i+=1
                print u"Organizations no.{}".format(i)
                self.check_invalided_kpis(org)

        data = {'text': u"End run from `invalided_kpis.py` {}".format(self.break_line)}
        r = requests.post(self.url, data=data)

    def check_invalided_kpis(self, org):
        print u"Check invalided kpi for: {} {}".format(org.name, self.break_line)

        data = {'text':u"Check invalided kpi for: `{}`  {}".format(org.name, self.break_line)}
        r = requests.post(self.url, data=data)

        quarter = org.get_current_quarter()

        kpis = KPI.objects.filter(quarter_period_id=quarter.id)
        for k in kpis:
            if k.kpi_type() == 'unknown':
                # print 'Invalided KPI: {}'.format(k.id)
                error=k.kpi_type_error()
                messages=u"Invalided kpi: `{}`-`{}`-`{}`".format(k.id, k.user.email, k.name, )
                messages+=self.break_char + self.indent_text +'Best matched:' + error['best_matched']
                messages+=self.break_char + self.indent_text + 'Suggest: ' + error['suggest']
                data = {'text': messages}
                r = requests.post(self.url, data=data)
        data = {'text': u"End check invalided kpi for: `{}`  {}".format(org.name, self.break_line)}
        r = requests.post(self.url, data=data)



