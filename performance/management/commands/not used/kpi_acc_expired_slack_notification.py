#!/usr/bin/python
# -*- coding: utf-8 -*-
from datetime import datetime, timedelta, date

from company.models import Organization
from django.core.management.base import BaseCommand
from elms import settings
import requests


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):

        slack_token = settings.SLACK_TOKEN

        url = u'https://slack.com/api/chat.postMessage?token=' + slack_token
        url += u"&channel=" + settings.SLACK_CS_CHANNEL



        today=date.today()
        orgs = Organization.objects.filter(enable_performance = True,
                                            #kpi_expired=True,
                                           demo_account = False,
                                           kpi_end_date__lte=today)

        # print "Count: "+ str(orgs.count())
        data = {'text':u"Run from `kpi_acc_expired_slack_notification` \r\n ***************************\r\n"}
        r = requests.post(url, data=data)

        i = 0
        for org in orgs:
            if i == 0:
                message = u'-------------------- \r\n\r\n '
                message +=  u" Những tài khoản KPI đã hết hạn: \r\n"
                message+=u"*************************************\r\n"


                data = {'text':message}
                r = requests.post(url, data=data)


            i+= 1

            message=str(i)+u". Công ty: `"+ org.name + "`"
            message+=u"\r\nNgày hết hạn: `"+org.kpi_end_date.strftime("%d-%m-%Y")+"`"
            message+=u"\r\n--------------------------------"
            data = {'text':message}
            r = requests.post(url, data=data)

            #print org.name

        data = {'text':u"End run from `kpi_acc_expired_slack_notification` \r\n ***************************\r\n"}
        r = requests.post(url, data=data)


