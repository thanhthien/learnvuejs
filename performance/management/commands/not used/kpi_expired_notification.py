#!/usr/bin/python
# -*- coding: utf-8 -*-
import datetime
import random
from company.models import Organization
from crm.models import Deal
from django.core.mail import EmailMessage
from django.core.management.base import BaseCommand
from elms import settings
from elms.settings import from_email
from django.db.models import Q
from django.template.loader import render_to_string

from user_profile.services.profile import send_mail_with_perm


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        pass

        now = datetime.datetime.utcnow()

        if now.weekday() == 6 or now.day % 5 != 0 :  #sunday and 3 times a week
            return False

        if now.hour != 1:  # 1am utc == 8am Vietnam time
            return False

        #companies = Organization.objects.exclude(trial__isnull=True)
        #companies.update(trial=False)
        companies = Organization.objects.filter(Q(trial=False) | Q(trial__isnull=True)).filter(enable_performance = True)

        context = {
            'companies': companies,
            'STATIC_URL':settings.STATIC_URL,

        }

        message = render_to_string('mail/reports/clients_usage.html', context)

        to_list = settings.INTERNAL_REPORT  #[up.user.email] [up.user.email] [up.user.email]

        title = u"CJS - Internal Report - Clients Usage KPI"
        msg = send_mail_with_perm(subject=title,
                           body=message,
                           from_email=from_email,
                           to=to_list)