#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand

from company.models import Organization
from elms import settings
from performance.models import KPI
from performance.documents import KPIRviewByStage
import datetime
from django.template.loader import render_to_string
from django.core.mail.message import EmailMessage
from user_profile.services.profile import allow_sending_email, send_mail_with_perm


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        kpis = KPI.objects.filter(review_type__in=['daily', 'weekly', 'monthly'],
                                  quarter_period__status='IN')
        today = datetime.datetime.now()
        for k in kpis:
            if k.review_type == 'daily' and k.quarter_period.due_date >= datetime.datetime.now().date():
                obj = KPIRviewByStage.objects.filter(kpi_id=k.id).order_by('-review_date').first()
                if obj and (today - obj.review_date).days > 2:
                    pass
            elif k.review_type == "weekly" and k.quarter_period.due_date >= datetime.datetime.now().date():
                obj = KPIRviewByStage.objects.filter(kpi_id=k.id).order_by('-review_date').first()
                if obj and (today - obj.review_date).days >= 7:
                    pass
            elif k.review_type == 'monthly' and k.quarter_period.due_date >= datetime.datetime.now().date():
                obj = KPIRviewByStage.objects.filter(kpi_id=k.id).order_by('-review_date').first()
                if obj and (today - obj.review_date).days >= 29:
                    pass

    def send_email_reminder(self, kpi):

        if  allow_sending_email(kpi.user.profile) == False:
            return

        message = render_to_string('performance/mails/kpi_review_reminder.html', {
            'name': kpi.name,
        })
        title = u"[Cloudjet KPI] Đánh giá KPI: " + kpi.get_name()
        msg = send_mail_with_perm(subject=title,
                           body=message,
                           from_email=settings.FROM_EMAIL,  # 'Cloudjet KPI <hello@cloudjetsolutions.com>',
                           to=[kpi.user.email],)
