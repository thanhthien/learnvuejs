#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand

from user_profile.services.profile import notify_slack_user_no_kpi


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        notify_slack_user_no_kpi()