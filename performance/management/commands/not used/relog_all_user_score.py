#!/usr/bin/python
# -*- coding: utf-8 -*-
import json
import time

import intercom
import requests
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from performance.models import log_user_score_all
from company.models import Organization
from user_profile.models import check_update_score_log, get_full_scores_v2
from performance.models import relog_user_score_all
from company.models import Organization


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):

        if len(args) > 0:
            org_id = int(args[0])

        try:
            organization = Organization.objects.get(id=org_id)
        except Organization.DoesNotExist:
            return

        relog_user_score_all(organization)
