#!/usr/bin/python
# -*- coding: utf-8 -*-
import threading

from django.core.management.base import BaseCommand
from company.models import Organization
from performance.models import KPI


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        organization = Organization.objects.get(pk=16)
        current_quarter = organization.get_current_quarter()
        pre_quarter = organization.get_archived_quarter().last()
        kpis = KPI.objects.select_related('copy_from').filter(quarter_period=current_quarter,
                                                              parent__isnull=True)
        for kpi in kpis:
            if kpi.copy_from and kpi.user_id == kpi.copy_from.user_id and kpi.copy_from.bsc_category \
                and kpi.copy_from.quarter_period_id == pre_quarter.id:
                kpi.bsc_category = kpi.copy_from.bsc_category
                print kpi.id
                kpi.save()

