#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from company.models import Organization
from performance.schedule import KPIFinalScoreCalculator
from utils import unsigned_vi


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        organizations = Organization.objects.filter(enable_performance=True)

        for organization in organizations:
            threads = []
            quarter_period = organization.get_current_quarter()
            cal = KPIFinalScoreCalculator(organization, quarter_period)
            threads.append(cal)
            cal.start()

            print u"Calculating %s ..." % (unsigned_vi(organization.name),)

            for thread in threads:
                thread.join()

        print "*********** Update KPI final score succeed ***********"
