#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand

from performance.models import KPI


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        count = 0
        print "Starting ..."
        while True:
            kpis = KPI.objects.filter(quarter_period__isnull=False, hash__isnull=True)[:1000]
            if not kpis.exists():
                break

            for kpi in kpis:
                kpi.get_hash()
            count += 1
            print count * 1000

        print "Done."


