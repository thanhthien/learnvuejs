#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.utils.dateformat import format

from company.models import Organization


from performance.models import KPI, Competency, Review
# from performance.documents import FinalScoreHistory, ScoreHistory
from user_profile.models import Profile
from utils import unsigned_vi


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        organizations = Organization.objects.filter(enable_performance=True)

        for organization in organizations:
            print unsigned_vi(organization.name)
            quarter_periods = list(organization.get_passed_quarter())

            if len(quarter_periods) < 1:
                return

            previous = 0
            for quarter_period in quarter_periods[1:]:
                print quarter_period.due_date
                profiles = Profile.objects.select_related('user').filter(organization=organization, active=True)
                for uo in profiles:
                    print uo.user.username
                    if KPI.objects.filter(user_id=uo.user_id,
                                          quarter_period=quarter_period).exists():
                        kpis = KPI.objects.filter(user_id=uo.user_id,
                                                  quarter_period=quarter_period) \
                                          .select_related('copy_from')

                        for kpi in kpis:
                            if kpi.copy_from and kpi.copy_from.user_id == kpi.user_id and kpi.copy_from.quarter_period_id == quarter_periods[previous].id:
                                # try:
                                #     history = ScoreHistory.objects.get(obj_id=kpi.copy_from_id,
                                #                                        type='kpi')
                                #     pre_history = history.history
                                # except:
                                #     pre_history = []
                                #
                                # h = ScoreHistory.objects.get_or_create(obj_id=kpi.id,
                                #                                        type='kpi')[0]

                                r = Review.objects.filter(organization=organization,
                                                          reviewee_id=uo.user_id,
                                                          review_type='manager',
                                                          quarter_period=quarter_periods[previous]).first()

                                p = None
                                if r and r.reviewer_id:
                                    p = Profile.objects.get(user_id=r.reviewer_id)

                                if not p:
                                    # if uo.report_to_id:
                                    if uo.parent:
                                        # p = Profile.objects.get(user_id=uo.report_to_id)
                                        p = Profile.objects.get(user_id=uo.parent.user_id if uo.parent else None)

                                score = None
                                reviewer = ''
                                if p:
                                    score = kpi.copy_from.get_score()  # p.user_id)
                                    reviewer = p.display_name

                                self_score = kpi.copy_from.get_score()  # uo.user_id)

                                data = {
                                    'quarter_period': format(quarter_periods[previous].due_date, 'd-m-Y'),
                                    'quarter_period_id': quarter_periods[previous].id,
                                    'quarter_name': quarter_periods[previous].quarter_name,
                                    'score': score,
                                    'self_score': self_score,
                                    'manager': reviewer
                                }
                                pre_history.append(data)
                                h.history = pre_history
                                h.save()

                    if Competency.objects.filter(user_id=uo.user_id,
                                                 quarter_period=quarter_period).exists():
                        competencies = Competency.objects.filter(user_id=uo.user_id,
                                                                 quarter_period=quarter_period)
                        for comp in competencies:
                            if comp.copy_from and comp.copy_from.user_id == comp.user_id and comp.copy_from.quarter_period_id == quarter_periods[previous].id:
                                # try:
                                #     history = ScoreHistory.objects.get(obj_id=comp.copy_from_id,
                                #                                        type='competency')
                                #     pre_history = history.history
                                # except:
                                #     pre_history = []
                                #
                                # h = ScoreHistory.objects.get_or_create(obj_id=comp.id,
                                #                                        type='competency')[0]

                                r = Review.objects.filter(organization=organization,
                                                          reviewee_id=uo.user_id,
                                                          review_type='manager',
                                                          quarter_period=quarter_periods[previous]).first()

                                p = None
                                if r and r.reviewer_id:
                                    p = Profile.objects.get(user_id=r.reviewer_id)

                                if not p:
                                    # if uo.report_to_id:
                                    if uo.parent:
                                        # p = Profile.objects.get(user_id=uo.report_to_id)
                                        p = Profile.objects.get(user_id=uo.parent.user_id if p.parent else None)

                                score = None
                                reviewer = ''
                                if p:
                                    score = comp.copy_from.get_score(p.user_id)
                                    reviewer = p.display_name

                                self_score = comp.copy_from.get_score(uo.user_id)
                                data = {
                                    'quarter_period': format(quarter_periods[previous].due_date, 'd-m-Y'),
                                    'quarter_period_id': quarter_periods[previous].id,
                                    'quarter_name': quarter_periods[previous].quarter_name,
                                    'score': score,
                                    'self_score': self_score,
                                    'manager': reviewer
                                }
                                pre_history.append(data)
                                h.history = pre_history
                                h.save()
                previous += 1
