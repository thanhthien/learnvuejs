#!/usr/bin/python
# -*- coding: utf-8 -*-
import datetime
from django.core.management.base import BaseCommand
from django.utils.dateformat import format

from company.models import Organization
from elms import settings

from performance.models import KPI
# from performance.documents import FinalScoreHistory
from user_profile.models import Profile
from utils import unsigned_vi, percent_to_score, notify_slack


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        start = datetime.datetime.now()

        organizations = Organization.objects.filter(enable_performance=True).exclude(demo_account=True)

        for organization in organizations:
            print unsigned_vi(organization.name)
            quarter_periods = organization.get_archived_quarter()

            if quarter_periods:
                quarter_periods = quarter_periods
            else:
                quarter_periods = []

            for quarter_period in quarter_periods:
                print quarter_period.due_date
                profiles = Profile.objects.filter(organization=organization)
                for uo in profiles:
                    if KPI.objects.filter(user_id=uo.user_id,
                                          quarter_period=quarter_period).exists():
                        # score, created = FinalScoreHistory.objects.get_or_create(organization=organization.id,
                        #                                                          quarter_period=quarter_period.id,
                        #                                                          due_date=format(quarter_period.due_date, 'd-m-Y'),
                        #                                                          user_id=uo.user_id)
                        if created:
                            kpi_score = uo.score_full_kpi_percent(quarter_period)
                            competency_score = uo.score_full_competency(quarter_period)

                            print kpi_score, competency_score
                            score.kpi_score = kpi_score
                            score.quarter_name = quarter_period.quarter_name
                            score.competency_score = competency_score
                            score.save()
        end = datetime.datetime.now()
        notify_slack(settings.SLACK_CRONJOB_CHANNEL, 'update_person_score_history.py {0} seconds'.format((end - start).seconds))
