#!/usr/bin/python
# -*- coding: utf-8 -*-
from company.models import Organization
from django.core.management.base import BaseCommand
from user_profile.models import Profile


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        organizations = Organization.objects.filter(enable_performance=True)

        for organization in organizations:
            uos = Profile.objects.filter(organization=organization)
            quarter_period = organization.get_archived_quarter().last()
            for uo in uos:
                profile = uo
                kpi_score = profile.score_full_kpi_percent(quarter_period)
                competency_score = profile.score_full_competency(quarter_period)
                if kpi_score:
                    profile.previous_kpi_final_score = kpi_score
                else:
                    profile.previous_kpi_final_score = None

                if competency_score:
                    profile.previous_competency_final_score = competency_score
                else:
                    profile.previous_competency_final_score = None
                profile.save()

        print "*********** Update previous quarter score succeed ***********"
