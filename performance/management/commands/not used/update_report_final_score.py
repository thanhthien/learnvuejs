#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from company.models import Organization
from elms import settings
from performance.schedule import KPIFinalScoreCalculator, ReportScoreCalculator
from utils import unsigned_vi, notify_slack
import datetime


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):

        start = datetime.datetime.now()

        organizations = Organization.objects.filter(enable_performance=True).exclude(demo_account = True)
        today = datetime.date.today()
        for organization in organizations:
            threads = []
            quarter_period = organization.get_current_quarter()

            start_date = quarter_period.due_date - datetime.timedelta(days=3)
            end_date = quarter_period.due_date + datetime.timedelta(days=1)
            if today >= start_date and today <= end_date:
                cal = ReportScoreCalculator(organization, quarter_period)
                threads.append(cal)
                cal.start()

                print u"Calculating report score %s ..." % (unsigned_vi(organization.name),)

                for thread in threads:
                    thread.join()

        #start = datetime.datetime.now()
        end = datetime.datetime.now()
        notify_slack(settings.SLACK_CRONJOB_CHANNEL, 'update_report_final_score.py {0} seconds'.format((end-start).seconds))

        print "*********** Update KPI final score succeed ***********"
