#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand

from company.models import Organization
from performance.models import  KPI, Competency,   QuarterPeriod
from performance.templatetags.kpi import get_list_kpi
from django.contrib.auth.models import User
from user_profile.models import Profile


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        organization = Organization.objects.get(id=args[0])
        current_quarter = QuarterPeriod.objects.get(id=args[1])

        self.fix_percent(organization.ceo, current_quarter, organization)

    def fix_percent(self, user, current_quarter, organization):
        # team = Profile.objects.filter(report_to=user)
        team = Profile.objects.filter(parent=user.profile)

        for u in team:
            if not Profile.objects.filter(user_id=u.user_id, active=True).exists():
                continue

            total = KPI.objects.filter(user_id=u.user_id,
                                       quarter_period=current_quarter,
                                       weight__gt=0,
                                       parent=None).count() + Competency.objects.filter(user_id=u.user_id,
                                                                                        quarter_period=current_quarter,
                                                                                        parent=None).count()

            comps = Competency.objects.filter(user_id=u.user_id,
                                              quarter_period=current_quarter,
                                              parent=None)

            comp_count = 0
            for comp in comps:
                score = comp.get_score(user.id)
                if score > 0:
                    comp_count += 1

            review_count = KPI.objects.filter(user_id=u.user_id,
                                              quarter_period=current_quarter,
                                              parent=None,
                                              weight__gt=0,
                                              latest_score__gt=0).count() + comp_count

            if total:
                percent = review_count / float(total) * 100
            else:
                percent = 0

            print u.user.username, percent
            r = Review.objects.filter(organization=organization,
                                      reviewer_id=user,
                                      reviewee_id=u.user_id,
                                      review_type='manager',
                                      quarter_period=current_quarter).first()
            if r:
                print 'Old', r.completion, 'New', percent
                r.completion = int(round(percent, 0))
                r.save()
            else:
                print 'Review not exists', user, u.user

            self.fix_percent(u.user, current_quarter, organization)





