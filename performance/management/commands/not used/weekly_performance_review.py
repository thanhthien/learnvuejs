#!/usr/bin/python
# -*- coding: utf-8 -*-
import datetime
from datetime import date
import random

from django.core.management.base import BaseCommand
from django.db.models import Q
from django.template.loader import render_to_string
from elms import settings
from django.core.mail.message import EmailMessage
import requests
from elms.settings import from_email
from user_profile.models import Profile
from dateutil import tz
from user_profile.services.profile import allow_sending_email, send_mail_with_perm


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):

        today = datetime.datetime.utcnow()
        today_min = datetime.datetime.combine(date.today(), datetime.time.min)
        three_weeks = datetime.datetime.utcnow() + datetime.timedelta(days=-28)

        # check is today is friday:
        if today.weekday() == 4:

            # orgs = Organization.objects.filter(enable_performance=False).exclude(ceo=None)
            # list_users = UserOrganization.objects.filter(organization__in=orgs).values_list('user_id')
            # if list_users:
            #     list_users = zip(*list_users)[0]
            # else:
            #     list_users = []

            # fixing query again
            # http://stackoverflow.com/questions/14056306/comparing-date-in-django-queryset
            user_profiles = Profile.objects.filter(Q(weekly_performance_review_email_sent__lt=today_min) | \
                                                   Q(weekly_performance_review_email_sent__isnull=True)). \
                                                   order_by("?")[:38]
                                                   # filter(having_subordinate= True).order_by("?")[:8]

            utc_tz = tz.gettz('UTC')

            print ("count profiles " + str(user_profiles.count()))

            for up in user_profiles:

                subs = up.get_subordinate()
                org = up.get_organization()
                if not org and  allow_sending_email(up): #or org.demo_account or org.trial:
                    continue

                print "subs :  " + str(len(subs))

                # check if have sub
                if len(subs) > 0 \
                        and org.demo_account == False \
                        and org.enable_performance == True \
                        and org.ceo_id > 0 \
                        and org.create_date.replace(tzinfo=utc_tz) < three_weeks.replace(tzinfo=utc_tz) :

                    up.weekly_performance_review_email_sent = today + datetime.timedelta(days=18)  # after 3 weeks
                    up.save()
                    to_list = [up.user.email]
                    users = []

                    print (" profile  " + (up.user.email))

                    for sub in subs:
                        to_list.append(sub.user.email)
                        users.append(sub.user)

                    r = random.randint(0, 30)

                    context = {
                        'users':users,
                        'u': up.user,
                        'random':r,
                        'STATIC_URL':settings.STATIC_URL,
                    }
                    bcc = []
                    if r >= 8 or org.ceo_id == up.user_id:
                        bcc = [settings.customer_happiness_email ]
                        # bcc =  [settings.customer_happiness_email , 'tuan@cloudjetsolutions.com']

                    message = render_to_string('mail/performance/weekly_team_performance_view.html', context)

                    if not org.enable_email_notification:
                        continue

                    if not settings.LOCAL:
                        title = up.display_name + u" : KPI Reviews"
                        msg = send_mail_with_perm(subject=title,
                                           body=message,
                                           from_email=from_email,
                                           to=to_list,
                                           bcc=bcc
                                            )
                else:
                    up.weekly_performance_review_email_sent = today + datetime.timedelta(days=30)  # no sub
                    up.save()
        requests.get("http://hchk.io/22fe2d0b-0a9d-4bb5-b5f4-529103630d9a")