#!/usr/bin/python
# -*- coding: utf-8 -*-
import random
from django.core.management.base import BaseCommand
import datetime
from django.db.models import Q
from django.template.loader import render_to_string
from elms import settings
from django.core.mail.message import EmailMessage
from elms.settings import from_email
from performance.models import KpiLog
import requests
from user_profile.models import Profile
from user_profile.services.profile import allow_sending_email, send_mail_with_perm


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):

        # TODO: random send to ceo or super users
        today = datetime.date.today()
        three_week = today - datetime.timedelta(days=38)

        user_profiles = Profile.objects.filter(Q(last_weekly_review_email_sent__lt=three_week) |
                                               Q(last_weekly_review_email_sent__isnull=True)).order_by("?")[:20]
        for up in user_profiles:

            org = up.get_organization()
            if not org and  allow_sending_email(up):  # or org.demo_account or org.trial:
                    continue

            if Profile.objects.filter(user_id=up.user_id,
                                      organization__enable_email_notification=False).exists():
                continue

            # if up.report_to and up.user:
            if up.parent and up.user:

                kpilog = KpiLog.objects.filter(user=up.user).count()
                if kpilog > 0:
                    to_list = [up.user.email]
                    # if up.report_to:
                    if up.parent and up.parent.user:
                        to_list.append(up.parent.user.email)

                    # no sending to super users any more
                    # r = random.randint(1, 80)
                    # if r <= 10:
                    #     # just randomly send to super user with ratio 1/8
                    #     super_user = up.get_organization().get_random_super_users()
                    #     to_list.append(super_user.email)

                    context = {
                        'user': up.user,
                        'STATIC_URL':settings.STATIC_URL,
                    }
                    message = render_to_string('mail/performance/weekly_random_performance_review.html', context)

                    title = u"Review KPIs của " + up.display_name

                    r = random.randint(1, 80)
                    if r <= 1:

                        msg = send_mail_with_perm(subject=title,
                                           body=message,
                                           from_email=from_email,
                                           to=to_list,
                                           bcc=[settings.customer_happiness_email, ]
                                            )
                    else:
                        msg = send_mail_with_perm(subject=title,
                                           body=message,
                                           from_email=from_email,
                                           to=to_list)

                   # yammer = yampy.Yammer(access_token='rySg5dVtKG37WfKNjyzGGQ')
                    # yammer.messages.create(message, group_id=settings.yammer_kpi_approval_group) #

            up.last_weekly_review_email_sent = datetime.datetime.now()
            up.save()

        requests.get("http://hchk.io/deebe24f-cad1-4b56-93a0-02929655ef8e")
