#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand

from user_profile.services.profile import job_remind_user_evaluate


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        job_remind_user_evaluate()
