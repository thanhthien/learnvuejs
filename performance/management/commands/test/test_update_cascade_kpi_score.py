import pytest
from performance.management.commands.update_cascade_kpi_score import remove_old_records

@pytest.mark.django_db(transaction=False)
def test_remove_old_records():
    remove_old_records()