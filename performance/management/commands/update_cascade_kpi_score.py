# !/usr/bin/python
# -*- coding: utf-8 -*-
import datetime
import logging
import random
import traceback
from time import sleep

from django.core.cache import cache
from django.core.mail import mail_admins
from django.core.management.base import BaseCommand

from elms import settings
from performance.models import CascadeKPIQueue
from performance.services import calculate_score_cascading, EmailMessage, get_kpi, get_system_user
from user_profile.services.profile import send_mail_with_perm
from utils import notify_slack
# def calculate_score()
from utils.threading_timer_decorator_exit import exit_after

logger = logging.getLogger('django')


@exit_after(180)
def calculate_cascade_score_thread(queue):
    actor = get_system_user()
    kpi = get_kpi(queue.kpi_id, actor)

    # ## exclude kpi if kpi.quarter_period is None
    # ## ref to this http://agile.cloudjet.solutions/project/fountainhead-cloudjet-kpi/us/1791

    quarter = None
    try:
        quarter = kpi.quarter_period
    except:
        pass

    # ## exclude KPI if kpi.quarter_period is None
    try:
        if kpi and quarter:
            calculate_score_cascading(kpi.id)  # KPIServices().calculate_cascade_score(kpi)
        queue.is_run = True
        queue.save()
        CascadeKPIQueue.objects.filter(kpi_id=queue.kpi_id, is_run=False).delete()  # delete duplicated

    except Exception as e:
        # Note: just for internal
        DOMAIN = settings.DOMAIN
        err = traceback.format_exc()
        email = [
            'khang@cjs.vn',
            'toan@cjs.vn',
            'duan@cjs.vn',
            'hong@cjs.vn',
            'tuan.vd@cloudjetkpi.com'
        ]
        em = send_mail_with_perm(from_email=settings.FROM_EMAIL, subject='[{}]CALCULATE KPI THREADING ERROR'.format(DOMAIN), to=email,
                          body='Error when calculating score for KPI {} {} \n {}'.format(queue.kpi_id, e.message, err),
                                 check_subscribe=False)
        # sleep(35)


def remove_old_records():
    two_weeks = datetime.datetime.now() + datetime.timedelta(days=-17)
    CascadeKPIQueue.objects.filter(is_run=True, created_at__lt=two_weeks).delete()


def calculate_score():
    try:
        total_crons = cache.get('update_cascade_kpi_score_is_running', 1)
        if total_crons > 15:
            logger.info('TOTAL CRONS: {} --> Not create more process'.format(total_crons))
            return
        i = 1

        if random.randint(1, 100) >= 80:
            remove_old_records()

        total_crons = total_crons + 1
        logger.info('TOTAL CRONS: {}'.format(total_crons))
        cache.set('update_cascade_kpi_score_is_running', total_crons, 60 * 5)  # five minutes
        while True:
            print 'calculate_score is running '

            # queues = list(CascadeKPIQueue.objects.select_related('kpi').filter(is_run=False).order_by('?')[:20])
            # queues_2 = list(CascadeKPIQueue.objects.select_related('kpi').filter(is_run=False).order_by('-id')[:1])
            queues = list(CascadeKPIQueue.objects.filter(is_run=False).order_by('?')[:20])  # lay random de dam bao khong bi stuck vi mot item nao
            queues_2 = list(CascadeKPIQueue.objects.filter(is_run=False).order_by('-id')[:1])  # lay cai gan nhat
            queues.extend(queues_2)

            for q in queues:
                print 'start process ID: {}'.format(q.id)
                # KPIServices().calculate_cascade_score(q.kpi)

                try:
                    key = "calculate_cascade_score_thread{}".format(q.id)
                    if cache.get(key, None):
                        continue

                    cache.set(key, True, 60 * 2)

                    calculate_cascade_score_thread(q)
                        # sleep(1)

                    # q.is_run = True
                    # q.save()
                    print 'finish process ID: {}'.format(q.id)

                    # pass
                except Exception as e:
                    message = u"KPI calculating score is failed: KPI.id =  {0} {1}".format(q.kpi_id, str(e))
                    message += u"{0}".format(traceback.format_exc())
                    print (u"{0}".format(traceback.format_exc()))
                    notify_slack(settings.SLACK_CRONJOB_CHANNEL, message)
                    err = traceback.format_exc()
                    email = [
                        'khang@cjs.vn',
                        'toan@cjs.vn',
                        'duan@cjs.vn',
                        'hong@cjs.vn',
                        'tuan.vd@cloudjetkpi.com'
                    ]
                    em = send_mail_with_perm(from_email=settings.FROM_EMAIL,
                                      subject='CALCULATE KPI THREADING ERROR', to=email,
                                      body='Error when calculating score Queue {} for KPI {} {} \n {}'.format(q.id, q.kpi_id, e.message,
                                                                                                     err), check_subscribe=False)

            if i == 99 and random.randint(1, 100) > 95:
                print ('update_cascade_kpi_score_is_running {} times continously'.format(i))
                notify_slack(settings.SLACK_CRONJOB_CHANNEL,
                             'update_cascade_kpi_score_is_running {} times continously'.format(i))

            i += 1

            # 2 seconds * 30 = 60 seconds = 1 minutes
            # Chạy 100 lần thì return, lúc này 1 cronjob mới sẽ chạy.
            # điều này để tránh bị overload bộ nhớ cũng như là chạy code cũ
            if i >= 30:
                return

                # CascadeKPIScore(q.kpi, None).run()
    except:
        err = traceback.format_exc()
        mail_admins(subject="Thread Calculate Score Failed", message="Error, main thread killed \n {}".format(err))


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        calculate_score()
        # def handle(self, *args, **options):
        #
        #     if cache.get('update_cascade_kpi_score_is_running', False):
        #         return
        #     i = 1
        #     while True:
        #         cache.set('update_cascade_kpi_score_is_running', True, 30)
        #         queues = CascadeKPIQueue.objects.select_related('kpi').filter(is_run=False).order_by('?')[:10]
        #         for q in queues:
        #             # KPIServices().calculate_cascade_score(q.kpi)
        #
        #             try:
        #
        #                 KPIServices().calculate_cascade_score(q.kpi)
        #
        #                 q.is_run = True
        #                 q.save()
        #
        #                 CascadeKPIQueue.objects.filter(kpi_id=q.kpi_id, is_run=False).delete()  # delete duplicated
        #
        #                 # pass
        #             except Exception as e:
        #                 message = u"KPI calculating score is failed: KPI.id =  {0} {1}".format(q.kpi.id, str(e))
        #                 message += u"{0}".format(traceback.format_exc())
        #                 print (u"{0}".format(traceback.format_exc()))
        #                 notify_slack(settings.SLACK_CRONJOB_CHANNEL, message)
        #
        #         sleep(2)
        #         print ('update_cascade_kpi_score_is_running {} times continously'.format(i))
        #
        #         if i == 99 and random.randint(1, 100) > 80:
        #             notify_slack(settings.SLACK_CRONJOB_CHANNEL,
        #                          'update_cascade_kpi_score_is_running {} times continously'.format(i))
        #
        #         i += 1
        #
        #         # 2 seconds * 300 = 600 seconds = 10 minutes
        #         # Chạy 100 lần thì return, lúc này 1 cronjob mới sẽ chạy.
        #         # điều này để tránh bị overload bộ nhớ cũng như là chạy code cũ
        #         if i >= 300:
        #             return
        #
        #             # CascadeKPIScore(q.kpi, None).run()
