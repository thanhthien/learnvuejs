# -*- coding: utf-8 -*-
# middleware.py

# ref: https://gist.github.com/mindlace/3918300
"""Add user created_by and modified_by foreign key refs to any model automatically.
   Almost entirely taken from https://github.com/Atomidata/django-audit-log/blob/master/audit_log/middleware.py"""
from django.core.cache import cache
from django.core.urlresolvers import reverse
from django.db.models import signals
from django.http import HttpResponseRedirect
from django.utils.functional import curry
from performance.models import KPI
from performance.services.common import PerformanceApi


class WhodidMiddleware(object):

    def process_request(self, request):
        KPI._request_user = request.user
        if not request.method in ('GET', 'HEAD', 'OPTIONS', 'TRACE'):
            if hasattr(request, 'user') and request.user.is_authenticated():
                user = request.user
            else:
                user = None

            mark_whodid = curry(self.mark_whodid, user)
            # signals.pre_save.connect(mark_whodid,  sender=KPI, dispatch_uid = (self.__class__, request,), weak = False)
            signals.pre_delete.connect(mark_whodid, sender=KPI, dispatch_uid=(self.__class__, request,), weak=False)
            # signals.post_delete.connect(mark_whodid, sender=GroupKPI, dispatch_uid = (self.__class__, request,), weak = False)

    def process_response(self, request, response):
        # signals.pre_save.disconnect(dispatch_uid =  (self.__class__, request,))
        signals.pre_delete.disconnect(dispatch_uid=(self.__class__, request,))
        # signals.post_delete.disconnect(dispatch_uid =  (self.__class__, request,))
        return response

    def mark_whodid(self, user, sender, instance, **kwargs):
        if not getattr(instance, 'created_by_id', None):
            instance.created_by = user
        if hasattr(instance, 'modified_by_id'):
            instance.modified_by = user
        instance.actor = user


# class RedirectEVN(object):    class not use
#
#     def process_response(self, request, response):
#         if (
#                     request.resolver_match and request.resolver_match.view_name == 'posts.views.logout_page') or request.path == '/logout':  # if logout, do nothing
#             return response
#
#         # try: bõ cái try except để tăng tốc độ xử lý
#         if hasattr(request, 'user') and request.user.is_authenticated():
#             if not cache.get('redirect{}'.format(request.user.id)):
#
#                 user_org = PerformanceApi.get_user_organization(request.user)
#                 if user_org and user_org.organization.redirect_to:  # and not request.user.profile.is_superuser() :  # and user_org.organization_id==1333 #and not request.user.profile.is_consultant:#EVN org
#                     link = user_org.organization.redirect_to + request.user.profile.get_login_link_nodomain()
#                     link = link.replace('//login', '/login')
#
#                     return HttpResponseRedirect(link)
#                 cache.set('redirect{}'.format(request.user.id), True, 30)
#
#         # except:
#         #     pass
#
#         return response


class creatingAssessment(object):
    def process_request(self, request):
        # print 'I\'m in creatingAssessment.process_request()'
        # if request.path.find(reverse('creating_assessment')) == -1:
        #     return HttpResponseRedirect(reverse('creating_assessment'))

        # OMG, if user post something to APIs, or post to some endpoint other than performance,
        #  user still can interfer to system when creating assessment
        # if request.user.is_authenticated() and request.path.startswith("/performance"):
        if request.user.is_authenticated():
            try:
                org = request.user.profile.get_organization()
            except:
                org = None

            if org and org.last_copy_time:
                if not org.is_ready():
                    if request.path.find(reverse('creating_assessment')) == -1 \
                            and request.path.find(reverse('DeployData')) == -1:
                        return HttpResponseRedirect(reverse('creating_assessment'))

    def process_response(self, request, response):
        # print 'I\'m in creatingAssessment.process_respone()'
        # try: bõ cái try except để tăng tốc độ xử lý

        # if request.user.is_authenticated() and request.path.startswith("/performance"):
        #     try:
        #         org = request.user.profile.get_organization()
        #     except:
        #         org = None
        #
        #     if org and org.last_copy_time:
        #         if not org.is_ready():
        #             if request.path.find(reverse('creating_assessment')) == -1:
        #                 print 'HttpResponseRedirect'
        #                 return HttpResponseRedirect(reverse('creating_assessment'))

        return response
