# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import performance.models
#import tinymce.models
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('company', '__first__'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='BscDepartment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=2000, null=True, blank=True)),
                ('name_vi', models.CharField(default=b'', max_length=2000, null=True, blank=True)),
                ('name_en', models.CharField(default=b'', max_length=2000, null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BscJobTitle',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=2000, null=True, blank=True)),
                ('name_vi', models.CharField(default=b'', max_length=2000, null=True, blank=True)),
                ('name_en', models.CharField(default=b'', max_length=2000, null=True, blank=True)),
                ('department', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, to='performance.BscDepartment', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BscKPI',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=2000, null=True, blank=True)),
                ('name_vi', models.CharField(default=b'', max_length=2000, null=True, blank=True)),
                ('name_en', models.CharField(default=b'', max_length=2000, null=True, blank=True)),
                ('measurement', models.CharField(default=b'', max_length=500, null=True, blank=True)),
                ('measurement_vi', models.CharField(default=b'', max_length=500, null=True, blank=True)),
                ('measurement_en', models.CharField(default=b'', max_length=500, null=True, blank=True)),
                ('translated', models.BooleanField(default=False)),
                ('checked', models.BooleanField(default=False)),
                ('department', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, to='performance.BscDepartment', null=True)),
                ('job_title', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, to='performance.BscJobTitle', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Competency',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=2000, null=True, blank=True)),
                ('name_vi', models.CharField(default=b'', max_length=2000, null=True, blank=True)),
                ('name_en', models.CharField(default=b'', max_length=2000, null=True, blank=True)),
                ('description', models.TextField(default=b'', blank=True)),
                ('description_vi', models.TextField(default=b'', null=True, blank=True)),
                ('description_en', models.TextField(default=b'', null=True, blank=True)),
                ('data_input_approved', models.BooleanField(default=False)),
                ('in_library', models.BooleanField(default=False)),
                ('quarter', models.IntegerField(default=1, blank=True)),
                ('year', models.IntegerField(default=0, blank=True)),
                ('is_core_value', models.BooleanField(default=False)),
                ('comment_evidence_self', models.TextField(default=b'', blank=True)),
                ('comment_evidence', models.TextField(default=b'', blank=True)),
                ('is_owner', models.BooleanField(default=False)),
            ],
            options={
                'ordering': ('id',),
                'db_table': 'performance_competency',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('email', models.EmailField(max_length=100)),
                ('message', models.TextField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DescriptionCell',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slug', models.CharField(max_length=20)),
                ('title', models.CharField(default=b'', max_length=100, blank=True)),
                ('title_vi', models.CharField(default=b'', max_length=100, null=True, blank=True)),
                ('title_en', models.CharField(default=b'', max_length=100, null=True, blank=True)),
                ('description', models.TextField(default=b'', blank=True)),
                ('description_vi', models.TextField(default=b'', null=True, blank=True)),
                ('description_en', models.TextField(default=b'', null=True, blank=True)),
            ],
            options={
                'ordering': ('id',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Ebook',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=1024)),
                ('description', models.TextField()),
                ('ebook', models.FileField(max_length=500, upload_to=performance.models.ebook_file_path)),
                ('image', models.ImageField(max_length=500, upload_to=performance.models.img_file_path)),
                ('product', models.CharField(default=b'kpi', max_length=30, choices=[(b'kpi', b'Cloudjet KPI'), (b'saleup', b'Cloudjet SaleUp')])),
            ],
            options={
                'ordering': ('id',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GoalComment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('content', models.TextField(default=b'')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Industry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=255, null=True, blank=True)),
                ('in_library', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='JobCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=256, null=True, blank=True)),
                ('ordering', models.IntegerField(default=0)),
                ('industry', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='performance.Industry', null=True)),
            ],
            options={
                'ordering': ('ordering',),
                'db_table': 'performance_job_category',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='JobTitle',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=256, null=True, blank=True)),
                ('name_vi', models.CharField(default=b'', max_length=256, null=True, blank=True)),
                ('name_en', models.CharField(default=b'', max_length=256, null=True, blank=True)),
                ('description', models.TextField(default=b'', null=True, blank=True)),
                ('description_vi', models.TextField(default=b'', null=True, blank=True)),
                ('description_en', models.TextField(default=b'', null=True, blank=True)),
                ('factors', models.TextField(default=b'', null=True, blank=True)),
                ('factors_vi', models.TextField(default=b'', null=True, blank=True)),
                ('factors_en', models.TextField(default=b'', null=True, blank=True)),
                ('category', models.ForeignKey(default=0, blank=True, to='performance.JobCategory', null=True)),
            ],
            options={
                'ordering': ('category__name',),
                'db_table': 'performance_job_title',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='KPI',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=2000, null=True, blank=True)),
                ('name_vi', models.CharField(default=b'', max_length=2000, null=True, blank=True)),
                ('name_en', models.CharField(default=b'', max_length=2000, null=True, blank=True)),
                ('description', models.TextField(default=b'', blank=True)),
                ('description_vi', models.TextField(default=b'', null=True, blank=True)),
                ('description_en', models.TextField(default=b'', null=True, blank=True)),
                ('weight', models.IntegerField(default=50, blank=True)),
                ('in_library', models.BooleanField(default=False)),
                ('quarter', models.IntegerField(default=1)),
                ('year', models.IntegerField(default=0)),
                ('current_goal', models.TextField(default=b'', blank=True)),
                ('future_goal', models.TextField(default=b'', blank=True)),
                ('outcome_notes', models.TextField(default=b'', blank=True)),
                ('outcome_notes_self', models.TextField(default=b'', blank=True)),
                ('is_owner', models.BooleanField(default=False)),
                ('data_input_approved', models.BooleanField(default=False)),
                ('last_qc_check', models.DateTimeField(null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('operator', models.CharField(max_length=10, blank=True)),
                ('target', models.FloatField(null=True, blank=True)),
                ('real', models.FloatField(null=True, blank=True)),
                ('unit', models.CharField(max_length=255, null=True, blank=True)),
                ('last_email_sent', models.DateTimeField(null=True, blank=True)),
                ('latest_score', models.FloatField(default=0, null=True, blank=True)),
                ('unique_key', models.CharField(default=b'', max_length=100, null=True, blank=True)),
                ('ordering', models.FloatField(default=1, blank=True)),
                ('is_started', models.NullBooleanField(default=False)),
                ('bsc_category', models.CharField(blank=True, max_length=30, null=True, choices=[(b'financial', 'Financial'), (b'customer', 'Customer'), (b'internal', 'Internal Process'), (b'learninggrowth', 'Learning & Growth')])),
                ('status', models.CharField(default=b'not_started', max_length=30, null=True, blank=True, choices=[(b'not_started', 'Not Started'), (b'on_track', 'On Track'), (b'completed', 'Completed'), (b'postponed', 'Postponed'), (b'cancelled', 'Cancelled'), (b'behind', 'Behind')])),
                ('is_private', models.BooleanField(default=False)),
                ('start_date', models.DateField(null=True, blank=True)),
                ('end_date', models.DateField(null=True, blank=True)),
                ('real_end_date', models.DateField(null=True, blank=True)),
                ('assigned_to', models.ForeignKey(related_name=b'assigned_to_relation', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('belong_to_jobs', models.ManyToManyField(related_name=b'kpi_list', to='performance.JobTitle', blank=True)),
                ('cascaded_from', models.ForeignKey(related_name=b'cascaded from', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='performance.KPI', null=True)),
                ('copy_from', models.ForeignKey(related_name=b'kpi_copy_from', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='performance.KPI', null=True)),
                ('job', models.ForeignKey(related_name=b'kpis', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='performance.JobTitle', null=True)),
                ('parent', models.ForeignKey(blank=True, to='performance.KPI', null=True)),
            ],
            options={
                'db_table': 'performance_kpi',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='KPIApproval',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('manager_status', models.CharField(default=b'PE', max_length=20, choices=[(b'PE', 'Pending'), (b'OK', 'OK'), (b'NEED_REVISED', 'Need Revised')])),
                ('manager_note', models.TextField(default=b'')),
                ('employee_status', models.CharField(default=b'PE', max_length=20, choices=[(b'PE', 'Pending'), (b'OK', 'OK'), (b'NEED_REVISED', 'Need Revised')])),
                ('employee_note', models.TextField(default=b'')),
                ('send_first_notification', models.BooleanField(default=False)),
                ('manager_approval_time', models.DateTimeField(null=True, blank=True)),
                ('employee_approval_time', models.DateTimeField(null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='KPIApprovalNotificationMessage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('phase', models.CharField(default=b'P0', max_length=10, choices=[(b'P0', 'Phase 0 - KPI Alignment'), (b'P1', 'Phase 1 - Announce'), (b'P2', 'Phase 2 - Remind'), (b'P3', 'Phase 3 - Late Remind')])),
                ('content', models.TextField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='KPIApprovelNotification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('content', models.TextField(default=b'')),
                ('phase', models.CharField(default=b'P0', max_length=20, choices=[(b'P0', 'Phase 0 - KPI Alignment'), (b'P1', 'Phase 1 - Announce'), (b'P2', 'Phase 2 - Remind'), (b'P3', 'Phase 3 - Late Remind')])),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('kpi_approval', models.ForeignKey(to='performance.KPIApproval')),
                ('send_by', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='KpiComment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('kpi_unique_key', models.CharField(default=b'', max_length=100, null=b'')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('content', models.TextField(default=b'')),
                ('file', models.FileField(max_length=500, null=True, upload_to=b'comment/attach/%Y/%m/%d/', blank=True)),
                ('send_from_ui', models.CharField(default=b'web', max_length=30, null=True)),
                ('is_user_notified', models.BooleanField(default=False)),
                ('kpi', models.ForeignKey(to='performance.KPI')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='KpiLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('role_edit', models.CharField(max_length=10, choices=[(b'self', b'Self'), (b'manager', b'Manager'), (b'admin', b'Admin')])),
                ('status', models.CharField(max_length=10, choices=[(b'new', 'Add new'), (b'update', 'Update'), (b'delete', 'Delete')])),
                ('log_type', models.CharField(max_length=10, choices=[(b'kpi', b'KPI'), (b'competency', 'Competency'), (b'devplan', 'Development Plan')])),
                ('desciption', models.TextField(blank=True)),
                ('edited_time', models.TimeField(auto_now_add=True)),
                ('edited_date', models.DateField(auto_now_add=True)),
                ('editor', models.ForeignKey(related_name=b'editor', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='QuarterPeriod',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quarter_name', models.CharField(default=b'', max_length=200, blank=True)),
                ('due_date', models.DateField()),
                ('quarter', models.IntegerField(default=1)),
                ('year', models.IntegerField(default=0)),
                ('status', models.CharField(default=b'PE', max_length=10, choices=[(b'PE', 'Pending'), (b'IN', 'In Progress'), (b'AR', 'Archive')])),
                ('completion', models.IntegerField(default=0)),
                ('organization', models.ForeignKey(to='company.Organization')),
            ],
            options={
                'ordering': ('organization', 'due_date'),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('review_type', models.CharField(max_length=20, choices=[(b'self', 'Employee self assessment'), (b'manager', 'Manager team review'), (b'peer', 'Peer Review'), (b'senior_manager', 'Senior Management'), (b'direct_report', 'Direct Reports')])),
                ('invitation_accepted', models.BooleanField(default=False)),
                ('quarter', models.IntegerField(default=1)),
                ('year', models.IntegerField(default=0)),
                ('share_to_manager', models.BooleanField(default=True)),
                ('share_to_all', models.BooleanField(default=True)),
                ('completion', models.IntegerField(default=0)),
                ('date_completed', models.DateField(null=True, blank=True)),
                ('created_at', models.DateField(auto_now_add=True)),
                ('updated_at', models.DateField(auto_now=True)),
                ('number_tasks', models.IntegerField(default=0)),
                ('is_complete', models.BooleanField(default=False)),
                ('organization', models.ForeignKey(to='company.Organization')),
                ('quarter_period', models.ForeignKey(blank=True, to='performance.QuarterPeriod', null=True)),
                ('reviewee', models.ForeignKey(related_name=b'reviewees', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('reviewer', models.ForeignKey(related_name=b'reviewers', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ReviewProgress',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('score', models.IntegerField(default=100)),
                ('note_score', models.IntegerField(default=0)),
                ('kpi_score', models.IntegerField(default=0)),
                ('late_score', models.IntegerField(default=0)),
                ('score_ladder', models.IntegerField(default=5)),
                ('quarter', models.ForeignKey(blank=True, to='performance.QuarterPeriod', null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Weeklygoal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=1000)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('is_done', models.BooleanField(default=False)),
                ('done_at', models.DateTimeField(null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='YearPlan',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('year', models.IntegerField()),
                ('organization', models.ForeignKey(to='company.Organization')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='quarterperiod',
            name='yearplan',
            field=models.ForeignKey(blank=True, to='performance.YearPlan', null=True),
            #preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='quarterperiod',
            unique_together=set([('organization', 'due_date')]),
        ),
        migrations.AddField(
            model_name='kpilog',
            name='quarter',
            field=models.ForeignKey(blank=True, to='performance.QuarterPeriod', null=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpilog',
            name='report_to',
            field=models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpilog',
            name='user',
            field=models.ForeignKey(related_name=b'user', to=settings.AUTH_USER_MODEL),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpiapproval',
            name='quarter',
            field=models.ForeignKey(to='performance.QuarterPeriod'),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpiapproval',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpi',
            name='quarter_period',
            field=models.ForeignKey(blank=True, to='performance.QuarterPeriod', null=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpi',
            name='user',
            field=models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='goalcomment',
            name='goal',
            field=models.ForeignKey(to='performance.Weeklygoal'),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='goalcomment',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='customer',
            name='review',
            field=models.ForeignKey(to='performance.Review'),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='competency',
            name='belong_to_jobs',
            field=models.ManyToManyField(related_name=b'competency_list', to='performance.JobTitle', blank=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='competency',
            name='copy_from',
            field=models.ForeignKey(related_name=b'comp_copy_from', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='performance.Competency', null=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='competency',
            name='job',
            field=models.ForeignKey(related_name=b'competencies', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='performance.JobTitle', null=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='competency',
            name='parent',
            field=models.ForeignKey(blank=True, to='performance.Competency', null=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='competency',
            name='quarter_period',
            field=models.ForeignKey(blank=True, to='performance.QuarterPeriod', null=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='competency',
            name='user',
            field=models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True),
            #preserve_default=True,
        ),
    ]
