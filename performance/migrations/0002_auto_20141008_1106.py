# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0005_auto_20141008_1106'),
        ('performance', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='KPITask',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=500)),
                ('done', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TaskEveryStage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('organization', models.ForeignKey(to='company.Organization')),
                ('name', models.CharField(max_length=500)),
                ('due_date', models.DateField(null=True, blank=True)),
                ('due_time_start', models.TimeField(null=True, blank=True)),
                ('due_time_end', models.TimeField(null=True, blank=True)),
                ('duration', models.IntegerField(default=0, blank=True)),
                ('status', models.CharField(blank=True, max_length=100, choices=[(b'start', '\u0110ang th\u1ef1c hi\u1ec7n'), (b'finished', '\u0110\xe3 ho\xe0n th\xe0nh'), (b'late', 'Tr\u1ec5 h\u1eb9n')])),
                ('note', models.TextField(blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Timeline',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('stage', models.CharField(max_length=255)),
                ('content', models.TextField()),
                ('deadline', models.DateField()),
                ('created_at', models.DateField(auto_now_add=True)),
                ('organization', models.ForeignKey(to='company.Organization')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='taskeverystage',
            name='stage',
            field=models.ForeignKey(blank=True, to='performance.Timeline', null=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpitask',
            name='task',
            field=models.ForeignKey(to='performance.TaskEveryStage'),
            #preserve_default=True,
        ),
    ]
