# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0002_auto_20141008_1106'),
    ]

    operations = [
        migrations.AddField(
            model_name='timeline',
            name='done',
            field=models.BooleanField(default=False),
            #preserve_default=True,
        ),
    ]
