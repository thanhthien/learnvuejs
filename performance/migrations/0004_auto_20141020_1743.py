# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0003_timeline_done'),
    ]

    operations = [
        migrations.CreateModel(
            name='CompetencyLib',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=2000, null=True, blank=True)),
                ('description', models.TextField(default=b'', blank=True)),
                ('category', models.CharField(blank=True, max_length=30, null=True, choices=[(b'basic', 'Basic Competency'), (b'domain', 'Domain Competency'), (b'culture', 'Culture')])),
                ('level', models.IntegerField(default=1)),
                ('code', models.CharField(default=b'', max_length=10, null=True, blank=True)),
                ('parent', models.ForeignKey(blank=True, to='performance.CompetencyLib', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='competency',
            name='category',
            field=models.CharField(blank=True, max_length=30, null=True, choices=[(b'basic', 'Basic Competency'), (b'domain', 'Domain Competency'), (b'culture', 'Culture')]),
            #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='taskeverystage',
            name='status',
            field=models.CharField(default=b'notstart', max_length=100, blank=True, choices=[(b'notstart', 'Ch\u01b0a th\u1ef1c hi\u1ec7n'), (b'start', '\u0110ang th\u1ef1c hi\u1ec7n'), (b'finished', '\u0110\xe3 ho\xe0n th\xe0nh'), (b'late', 'Tr\u1ec5 h\u1eb9n')]),
        ),
    ]
