# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0004_auto_20141020_1743'),
    ]

    operations = [
        migrations.AddField(
            model_name='competencylib',
            name='description_en',
            field=models.TextField(default=b'', null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='competencylib',
            name='description_vi',
            field=models.TextField(default=b'', null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='competencylib',
            name='name_en',
            field=models.CharField(default=b'', max_length=2000, null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='competencylib',
            name='name_vi',
            field=models.CharField(default=b'', max_length=2000, null=True, blank=True),
            #preserve_default=True,
        ),
    ]
