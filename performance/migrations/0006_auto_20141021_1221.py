# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0005_auto_20141020_1810'),
    ]

    operations = [
        migrations.AddField(
            model_name='kpi',
            name='manager_confirmed',
            field=models.BooleanField(default=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpi',
            name='self_confirmed',
            field=models.BooleanField(default=True),
            #preserve_default=True,
        ),
    ]
