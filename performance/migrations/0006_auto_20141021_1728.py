# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0005_auto_20141020_1810'),
    ]

    operations = [
        migrations.AddField(
            model_name='competency',
            name='code',
            field=models.CharField( max_length=10, null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='competency',
            name='level',
            field=models.IntegerField(max_length=10, null=True, blank=True),
            #preserve_default=True,
        ),
    ]
