# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0009_auto_20141112_1709'),
    ]

    operations = [
        migrations.CreateModel(
            name='GroupKPI',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('category', models.CharField(max_length=30, choices=[(b'financial', 'Financial'), (b'customer', 'Customer'), (b'internal', 'Internal Process'), (b'learninggrowth', 'Learning & Growth')])),
                ('kpis', models.ManyToManyField(to='performance.KPI', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='StrategyMap',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('year', models.IntegerField()),
                ('name', models.CharField(max_length=500)),
                ('description', models.TextField(blank=True)),
                ('organization', models.ForeignKey(to='company.Organization')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='strategymap',
            unique_together=set([('organization', 'year')]),
        ),
        migrations.AddField(
            model_name='groupkpi',
            name='map',
            field=models.ForeignKey(to='performance.StrategyMap'),
            #preserve_default=True,
        ),
    ]
