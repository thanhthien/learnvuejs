# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0010_auto_20141203_1457'),
    ]

    operations = [
        migrations.AlterField(
            model_name='historicalkpi',
            name='ordering',
            field=models.FloatField(default=1),
            #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kpi',
            name='ordering',
            field=models.FloatField(default=1),
            #preserve_default=True,
        ),
    ]
