# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0011_auto_20150106_1303'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalkpi',
            name='current_result',
            field=models.FloatField(null=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='map_id',
            field=models.IntegerField(db_index=True, null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='quarter_four_target',
            field=models.FloatField(null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='quarter_one_target',
            field=models.FloatField(null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='quarter_three_target',
            field=models.FloatField(null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='quarter_two_target',
            field=models.FloatField(null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpi',
            name='current_result',
            field=models.FloatField(null=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpi',
            name='map',
            field=models.ForeignKey(blank=True, to='performance.StrategyMap', null=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpi',
            name='quarter_four_target',
            field=models.FloatField(null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpi',
            name='quarter_one_target',
            field=models.FloatField(null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpi',
            name='quarter_three_target',
            field=models.FloatField(null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpi',
            name='quarter_two_target',
            field=models.FloatField(null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='taskeverystage',
            name='status',
            field=models.CharField(default=b'notstart', max_length=100, blank=True, choices=[(b'notstart', 'Not start'), (b'start', 'On process'), (b'finished', 'Finished'), (b'late', 'Overdue')]),
            #preserve_default=True,
        ),
    ]
