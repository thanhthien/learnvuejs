# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0012_auto_20150209_2028'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalkpi',
            name='year_target',
            field=models.FloatField(null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpi',
            name='year_target',
            field=models.FloatField(null=True, blank=True),
            #preserve_default=True,
        ),
    ]
