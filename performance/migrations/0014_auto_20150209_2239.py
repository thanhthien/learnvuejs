# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0013_auto_20150209_2044'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalkpi',
            name='owner_email',
            field=models.CharField(default=b'', max_length=200, null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpi',
            name='owner_email',
            field=models.CharField(default=b'', max_length=200, null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='historicalkpi',
            name='current_result',
            field=models.FloatField(null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kpi',
            name='current_result',
            field=models.FloatField(null=True, blank=True),
            #preserve_default=True,
        ),
    ]
