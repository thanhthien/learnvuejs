# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0014_auto_20150209_2239'),
    ]

    operations = [
        migrations.AlterField(
            model_name='historicalkpi',
            name='operator',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='Operator', choices=[(b'>=', b'>='), (b'=', b'='), (b'<=', b'<=')]),
            #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kpi',
            name='operator',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='Operator', choices=[(b'>=', b'>='), (b'=', b'='), (b'<=', b'<=')]),
            #preserve_default=True,
        ),
    ]
