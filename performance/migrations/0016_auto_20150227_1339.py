# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0015_auto_20150210_2212'),
    ]

    operations = [
        migrations.AlterField(
            model_name='historicalkpi',
            name='owner_email',
            field=models.EmailField(max_length=200, null=True, blank=True),
           # #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kpi',
            name='owner_email',
            field=models.EmailField(max_length=200, null=True, blank=True),
           # #preserve_default=True,
        ),
    ]
