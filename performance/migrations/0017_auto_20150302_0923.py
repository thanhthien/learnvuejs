# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0016_auto_20150227_1339'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalkpi',
            name='refer_to_id',
            field=models.IntegerField(db_index=True, null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpi',
            name='refer_to',
            field=models.ForeignKey(related_name='refer_to_fpk', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='performance.KPI', null=True),
            #preserve_default=True,
        ),
    ]
