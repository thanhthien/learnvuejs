# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0017_auto_20150302_0923'),
    ]

    operations = [
        migrations.AlterField(
            model_name='historicalkpi',
            name='owner_email',
            field=models.EmailField(db_index=True, max_length=200, null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kpi',
            name='owner_email',
            field=models.EmailField(db_index=True, max_length=200, null=True, blank=True),
            #preserve_default=True,
        ),
    ]
