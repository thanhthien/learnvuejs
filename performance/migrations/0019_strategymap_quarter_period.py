# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0018_auto_20150304_1642'),
    ]

    operations = [
        migrations.AddField(
            model_name='strategymap',
            name='quarter_period',
            field=models.ForeignKey(blank=True, to='performance.QuarterPeriod', null=True),
            #preserve_default=True,
        ),
    ]
