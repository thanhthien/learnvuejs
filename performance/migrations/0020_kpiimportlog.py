# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0014_auto_20150318_1431'),
        ('performance', '0019_strategymap_quarter_period'),
    ]

    operations = [
        migrations.CreateModel(
            name='KPIImportLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('employee_name', models.CharField(max_length=200)),
                ('import_date', models.DateField()),
                ('employee_code', models.CharField(max_length=100, blank=True)),
                ('description', models.TextField(blank=True)),
                ('organization', models.ForeignKey(to='company.Organization')),
                ('quarter_period', models.ForeignKey(to='performance.QuarterPeriod')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
