# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0020_kpiimportlog'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kpilog',
            name='role_edit',
            field=models.CharField(max_length=10, choices=[(b'', b'---'), (b'self', b'Self'), (b'manager', b'Manager'), (b'admin', b'Admin')]),
            #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kpilog',
            name='user',
            field=models.ForeignKey(related_name='user', blank=True, to=settings.AUTH_USER_MODEL, null=True),
            #preserve_default=True,
        ),
    ]
