# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('performance', '0021_auto_20150403_0007'),
    ]

    operations = [
        migrations.CreateModel(
            name='KPIData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('date', models.DateField()),
                ('value', models.FloatField()),
                ('input_by', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('kpi', models.ForeignKey(to='performance.KPI')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='hash',
            field=models.CharField(max_length=100, null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpi',
            name='hash',
            field=models.CharField(max_length=100, null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kpilog',
            name='status',
            field=models.CharField(max_length=10, choices=[(b'new', 'Add new'), (b'update', 'Update'), (b'delete', 'Delete'), (b'transfer', 'Transfer')]),
            #preserve_default=True,
        ),
    ]
