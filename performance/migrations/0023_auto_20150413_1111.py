# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0022_auto_20150405_0006'),
    ]

    operations = [
        migrations.CreateModel(
            name='CascadeKPIQueue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_run', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('kpi', models.ForeignKey(to='performance.KPI')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='historicalkpi',
            name='current_goal',
            field=models.TextField(default=b'', verbose_name='Measurement', blank=True),
      #      #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='historicalkpi',
            name='quarter_four_target',
            field=models.FloatField(null=True, verbose_name='Quarter four target', blank=True),
      #      #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='historicalkpi',
            name='quarter_one_target',
            field=models.FloatField(null=True, verbose_name='Quarter one target', blank=True),
        #    #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='historicalkpi',
            name='quarter_three_target',
            field=models.FloatField(null=True, verbose_name='Quarter three target', blank=True),
      #      #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='historicalkpi',
            name='quarter_two_target',
            field=models.FloatField(null=True, verbose_name='Quarter two target', blank=True),
      #      #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='historicalkpi',
            name='year_target',
            field=models.FloatField(null=True, verbose_name='Year target', blank=True),
    #        #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kpi',
            name='current_goal',
            field=models.TextField(default=b'', verbose_name='Measurement', blank=True),
       #     #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kpi',
            name='quarter_four_target',
            field=models.FloatField(null=True, verbose_name='Quarter four target', blank=True),
        #    #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kpi',
            name='quarter_one_target',
            field=models.FloatField(null=True, verbose_name='Quarter one target', blank=True),
     #       #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kpi',
            name='quarter_three_target',
            field=models.FloatField(null=True, verbose_name='Quarter three target', blank=True),
       #     #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kpi',
            name='quarter_two_target',
            field=models.FloatField(null=True, verbose_name='Quarter two target', blank=True),
     #       #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kpi',
            name='year_target',
            field=models.FloatField(null=True, verbose_name='Year target', blank=True),
       #     #preserve_default=True,
        ),
    ]
