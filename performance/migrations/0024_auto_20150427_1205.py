# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('performance', '0023_auto_20150413_1111'),
    ]

    operations = [
        migrations.AddField(
            model_name='competency',
            name='reviewer',
            field=models.ForeignKey(related_name='reviewer_competency', on_delete=django.db.models.deletion.SET_NULL, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalcompetency',
            name='reviewer_id',
            field=models.IntegerField(db_index=True, null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='reviewer_id',
            field=models.IntegerField(db_index=True, null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpi',
            name='reviewer',
            field=models.ForeignKey(related_name='reviewer_kpi', on_delete=django.db.models.deletion.SET_NULL, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            #preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='strategymap',
            unique_together=set([]),
        ),
    ]
