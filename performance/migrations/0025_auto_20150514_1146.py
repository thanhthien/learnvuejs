# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0024_auto_20150427_1205'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalkpi',
            name='review_type',
            field=models.CharField(default=b'', max_length=50, blank=True, choices=[(b'', 'Final'), (b'daily', 'Daily'), (b'weekly', 'Weekly'), (b'monthly', 'Monthly')]),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpi',
            name='review_type',
            field=models.CharField(default=b'', max_length=50, blank=True, choices=[(b'', 'Final'), (b'daily', 'Daily'), (b'weekly', 'Weekly'), (b'monthly', 'Monthly')]),
            #preserve_default=True,
        ),
    ]
