# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0025_auto_20150514_1146'),
    ]

    operations = [
        migrations.AddField(
            model_name='quarterperiod',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='quarterperiod',
            name='end_rewarding',
            field=models.FloatField(null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='quarterperiod',
            name='max_rewarding',
            field=models.FloatField(null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='quarterperiod',
            name='reward_cap',
            field=models.FloatField(null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='quarterperiod',
            name='start_rewarding',
            field=models.FloatField(null=True, blank=True),
            #preserve_default=True,
        ),
    ]
