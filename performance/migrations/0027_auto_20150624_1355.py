# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0026_auto_20150602_1528'),
    ]

    operations = [
        migrations.AddField(
            model_name='quarterperiod',
            name='saved',
            field=models.BooleanField(default=False),
            #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kpilog',
            name='status',
            field=models.CharField(max_length=10, choices=[(b'new', 'Add new'), (b'update', 'Update'), (b'delete', 'Delete'), (b'transfer', 'Transfer'), (b'assigned', 'Assigned to')]),
            #preserve_default=True,
        ),
    ]
