# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0027_auto_20150624_1355'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='quarterperiod',
            name='saved',
        ),
        migrations.AddField(
            model_name='quarterperiod',
            name='unsaved',
            field=models.BooleanField(default=True),
            #preserve_default=True,
        ),
    ]
