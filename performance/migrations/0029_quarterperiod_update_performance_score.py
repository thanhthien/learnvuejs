# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0028_auto_20150624_1408'),
    ]

    operations = [
        migrations.AddField(
            model_name='quarterperiod',
            name='update_performance_score',
            field=models.BooleanField(default=False),
            #preserve_default=True,
        ),
    ]
