# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0029_quarterperiod_update_performance_score'),
    ]

    operations = [
        migrations.RenameField(
            model_name='quarterperiod',
            old_name='update_performance_score',
            new_name='update_perform_reward',
        ),
    ]
