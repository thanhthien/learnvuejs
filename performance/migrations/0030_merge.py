# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0029_quarterperiod_update_performance_score'),
        ('performance', '0029_auto_20150701_1149'),
    ]

    operations = [
    ]
