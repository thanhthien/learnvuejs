# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0030_auto_20150706_1642'),
        ('performance', '0030_merge'),
    ]

    operations = [
    ]
