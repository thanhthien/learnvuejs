# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0031_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='quarterperiod',
            name='is_ready',
            field=models.BooleanField(default=True),
            #preserve_default=True,
        ),
    ]
