# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0032_quarterperiod_is_ready'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalkpi',
            name='removed',
            field=models.DateTimeField(default=None, null=True, editable=False, blank=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpi',
            name='removed',
            field=models.DateTimeField(default=None, null=True, editable=False, blank=True),
            #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='strategymap',
            name='name',
            field=models.TextField(blank=True),
            #preserve_default=True,
        ),
    ]
