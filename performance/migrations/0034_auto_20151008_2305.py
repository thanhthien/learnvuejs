# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('performance', '0033_auto_20150925_1742'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='historicalcompetency',
            options={'ordering': ('-history_date', '-history_id'), 'get_latest_by': 'history_date', 'verbose_name': 'historical competency'},
        ),
        migrations.AlterModelOptions(
            name='historicalkpi',
            options={'ordering': ('-history_date', '-history_id'), 'get_latest_by': 'history_date', 'verbose_name': 'historical kpi'},
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='copy_from_id',
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='job_id',
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='parent_id',
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='quarter_period_id',
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='reviewer_id',
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='user_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='assigned_to_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='cascaded_from_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='copy_from_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='job_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='map_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='parent_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='quarter_period_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='refer_to_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='reviewer_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='user_id',
        ),
        migrations.AddField(
            model_name='historicalcompetency',
            name='copy_from',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.Competency', null=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalcompetency',
            name='job',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.JobTitle', null=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalcompetency',
            name='parent',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.Competency', null=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalcompetency',
            name='quarter_period',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.QuarterPeriod', null=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalcompetency',
            name='reviewer',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalcompetency',
            name='user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='assigned_to',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='cascaded_from',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.KPI', null=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='copy_from',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.KPI', null=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='job',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.JobTitle', null=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='map',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.StrategyMap', null=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='parent',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.KPI', null=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='quarter_period',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.QuarterPeriod', null=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='refer_to',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.KPI', null=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='reviewer',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='groupkpi',
            name='category',
            field=models.CharField(max_length=30, choices=[(b'financial', 'Financial'), (b'customer', 'Customer'), (b'internal', 'Internal Process'), (b'learninggrowth', 'Learning & Growth'), (b'other', 'Other')]),
            #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='historicalcompetency',
            name='history_user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True),
            #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='historicalkpi',
            name='bsc_category',
            field=models.CharField(blank=True, max_length=30, null=True, choices=[(b'financial', 'Financial'), (b'customer', 'Customer'), (b'internal', 'Internal Process'), (b'learninggrowth', 'Learning & Growth'), (b'other', 'Other')]),
            #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='historicalkpi',
            name='history_user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True),
            #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kpi',
            name='bsc_category',
            field=models.CharField(blank=True, max_length=30, null=True, choices=[(b'financial', 'Financial'), (b'customer', 'Customer'), (b'internal', 'Internal Process'), (b'learninggrowth', 'Learning & Growth'), (b'other', 'Other')]),
            #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kpi',
            name='cascaded_from',
            field=models.ForeignKey(related_name='_cascaded_from', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='performance.KPI', null=True),
            #preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kpiapprovalnotificationmessage',
            name='content',
            field=models.TextField(default=b''),
            #preserve_default=True,
        ),
    ]
