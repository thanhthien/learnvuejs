# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
                    ('performance', '0034_auto_20151008_2305'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalkpi',
            name='approval_status',
            field=models.CharField(default=b'', max_length=50, blank=True, choices=[(b'', 'Not Finished'), (b'finished', 'Finished'), (b'rejected', 'Rejected'), (b'approved', 'Approved')]),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpi',
            name='approval_status',
            field=models.CharField(default=b'', max_length=50, blank=True, choices=[(b'', 'Not Finished'), (b'finished', 'Finished'), (b'rejected', 'Rejected'), (b'approved', 'Approved')]),
            #preserve_default=True,
        ),
    ]
