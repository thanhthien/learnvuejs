# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0035_auto_20151018_1744'),
    ]

    operations = [
        migrations.AddField(
            model_name='quarterperiod',
            name='quarter_1_year',
            field=models.IntegerField(null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='quarterperiod',
            name='quarter_2_year',
            field=models.IntegerField(null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='quarterperiod',
            name='quarter_3_year',
            field=models.IntegerField(null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='quarterperiod',
            name='quarter_4_year',
            field=models.IntegerField(null=True, blank=True),
            #preserve_default=True,
        ),
        migrations.AddField(
            model_name='quarterperiod',
            name='review_mode',
            field=models.CharField(default=b'quarterly', max_length=20, null=True, blank=True),
            #preserve_default=True,
        ),
    ]
