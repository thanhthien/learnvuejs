# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0036_auto_20151024_1523'),
    ]

    operations = [
        migrations.CreateModel(
            name='KPIFeedback',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('removed', models.DateTimeField(default=None, null=True, editable=False, blank=True)),
                ('name', models.CharField(default=b'', max_length=1000)),
                ('category', models.CharField(default=b'', max_length=1000)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
    ]
