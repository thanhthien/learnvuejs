# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0037_kpifeedback'),
    ]

    operations = [
        migrations.AddField(
            model_name='taskeverystage',
            name='meeting_minutes',
            field=models.TextField(blank=True, null=True,),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='taskeverystage',
            name='next_steps',
            field=models.TextField(blank=True, null=True,),
            preserve_default=True,
        ),
    ]
