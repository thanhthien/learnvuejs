# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0038_auto_20151029_1707'),
    ]

    operations = [
        migrations.AlterField(
            model_name='taskeverystage',
            name='meeting_minutes',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='taskeverystage',
            name='next_steps',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
