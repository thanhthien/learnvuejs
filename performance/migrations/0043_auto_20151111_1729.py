# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0042_auto_20151110_1422'),
    ]

    operations = [
        migrations.CreateModel(
            name='CategoryKPIFeedback',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('color', models.CharField(default=b'FFFFFF', max_length=6)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='kpifeedback',
            name='category',
        ),
        migrations.AddField(
            model_name='kpifeedback',
            name='category',
            field=models.ForeignKey(blank=True, to='performance.CategoryKPIFeedback', null=True),
            preserve_default=True,
        ),
    ]
