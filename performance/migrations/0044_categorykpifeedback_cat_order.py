# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0043_auto_20151111_1729'),
    ]

    operations = [
        migrations.AddField(
            model_name='categorykpifeedback',
            name='cat_order',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
