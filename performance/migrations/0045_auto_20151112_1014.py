# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0044_categorykpifeedback_cat_order'),
    ]

    operations = [
        migrations.AlterField(
            model_name='categorykpifeedback',
            name='color',
            field=models.CharField(default=b'e7d457', max_length=6),
            preserve_default=True,
        ),
    ]
