# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0045_auto_20151112_1014'),
    ]

    operations = [
        migrations.AlterField(
            model_name='categorykpifeedback',
            name='color',
            field=models.CharField(default=b'#e7d457', max_length=100),
            preserve_default=True,
        ),
    ]
