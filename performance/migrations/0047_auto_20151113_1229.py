# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0046_auto_20151113_1219'),
    ]

    operations = [
        migrations.AddField(
            model_name='categorykpifeedback',
            name='name_vi',
            field=models.CharField(default=b'', max_length=200),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpifeedback',
            name='name_vi',
            field=models.CharField(default=b'', max_length=1000),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='categorykpifeedback',
            name='name',
            field=models.CharField(default=b'', max_length=200),
            preserve_default=True,
        ),
    ]
