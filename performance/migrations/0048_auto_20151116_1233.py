# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('performance', '0047_auto_20151113_1229'),
    ]

    operations = [
        migrations.CreateModel(
            name='Quotation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('client_name', models.CharField(default=b'', max_length=1024)),
                ('client_contact', models.CharField(default=b'', max_length=1024)),
                ('client_email', models.CharField(default=b'', max_length=1024)),
                ('client_phone', models.CharField(default=b'', max_length=1024)),
                ('employees', models.IntegerField(default=200)),
                ('subsidary', models.IntegerField(default=0)),
                ('managers', models.IntegerField(default=35)),
                ('job_descriptions', models.IntegerField(default=55)),
                ('discount_percentage', models.IntegerField(default=0)),
                ('hash', models.CharField(max_length=100, null=True, blank=True)),
                ('package', models.CharField(default=b'standard', max_length=30, choices=[(b'training', b'Training'), (b'standard', b'Standard'), (b'premium', b'Premium')])),
            ],
            options={
                'ordering': ('id',),
            },
            bases=(models.Model,),
        ),
        migrations.AlterModelOptions(
            name='historicalcompetency',
            options={'ordering': ('-history_date', '-history_id'), 'get_latest_by': 'history_date', 'verbose_name': 'historical competency'},
        ),
        migrations.AlterModelOptions(
            name='historicalkpi',
            options={'ordering': ('-history_date', '-history_id'), 'get_latest_by': 'history_date', 'verbose_name': 'historical kpi'},
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='copy_from_id',
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='job_id',
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='parent_id',
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='quarter_period_id',
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='reviewer_id',
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='user_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='assigned_to_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='cascaded_from_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='copy_from_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='job_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='map_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='parent_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='quarter_period_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='refer_to_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='reviewer_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='user_id',
        ),
        migrations.AddField(
            model_name='historicalcompetency',
            name='copy_from',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.Competency', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalcompetency',
            name='job',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.JobTitle', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalcompetency',
            name='parent',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.Competency', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalcompetency',
            name='quarter_period',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.QuarterPeriod', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalcompetency',
            name='reviewer',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalcompetency',
            name='user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='assigned_to',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='cascaded_from',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.KPI', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='copy_from',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.KPI', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='job',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.JobTitle', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='map',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.StrategyMap', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='parent',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.KPI', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='quarter_period',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.QuarterPeriod', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='refer_to',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.KPI', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='reviewer',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='historicalcompetency',
            name='history_user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='historicalkpi',
            name='history_user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
    ]
