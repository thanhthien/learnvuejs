# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0048_auto_20151116_1233'),
    ]

    operations = [
        migrations.AddField(
            model_name='quotation',
            name='consultant_email',
            field=models.CharField(default=b'', max_length=1024),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='quotation',
            name='consultant_name',
            field=models.CharField(default=b'', max_length=1024),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='quotation',
            name='consultant_phone',
            field=models.CharField(default=b'', max_length=1024),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='quotation',
            name='created_date',
            field=models.DateTimeField(auto_now_add=True, null=True),
            preserve_default=True,
        ),
    ]
