# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0049_auto_20151116_1248'),
    ]

    operations = [
        migrations.CreateModel(
            name='KPIFieldErrorMapping',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('field', models.CharField(default=b'', max_length=100)),
                ('error', models.ForeignKey(to='performance.KPIFeedback')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterModelOptions(
            name='historicalcompetency',
            options={'ordering': ('-history_date', '-history_id'), 'verbose_name': 'historical competency'},
        ),
        migrations.AlterModelOptions(
            name='historicalkpi',
            options={'ordering': ('-history_date', '-history_id'), 'verbose_name': 'historical kpi'},
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='copy_from',
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='job',
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='parent',
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='quarter_period',
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='reviewer',
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='user',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='assigned_to',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='cascaded_from',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='copy_from',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='job',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='map',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='parent',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='quarter_period',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='refer_to',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='reviewer',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='user',
        ),
        migrations.AddField(
            model_name='historicalcompetency',
            name='copy_from_id',
            field=models.IntegerField(db_index=True, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalcompetency',
            name='job_id',
            field=models.IntegerField(db_index=True, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalcompetency',
            name='parent_id',
            field=models.IntegerField(db_index=True, null=True, verbose_name='Competency parent', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalcompetency',
            name='quarter_period_id',
            field=models.IntegerField(db_index=True, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalcompetency',
            name='reviewer_id',
            field=models.IntegerField(db_index=True, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalcompetency',
            name='user_id',
            field=models.IntegerField(db_index=True, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='assigned_to_id',
            field=models.IntegerField(db_index=True, null=True, verbose_name='Assigned to', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='cascaded_from_id',
            field=models.IntegerField(db_index=True, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='copy_from_id',
            field=models.IntegerField(db_index=True, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='job_id',
            field=models.IntegerField(db_index=True, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='map_id',
            field=models.IntegerField(db_index=True, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='parent_id',
            field=models.IntegerField(db_index=True, null=True, verbose_name='KPI parent', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='quarter_period_id',
            field=models.IntegerField(db_index=True, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='refer_to_id',
            field=models.IntegerField(db_index=True, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='reviewer_id',
            field=models.IntegerField(db_index=True, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='user_id',
            field=models.IntegerField(db_index=True, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='historicalcompetency',
            name='history_user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='historicalkpi',
            name='history_user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
    ]
