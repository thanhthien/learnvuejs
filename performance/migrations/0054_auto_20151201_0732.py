# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0053_auto_20151124_0742'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalkpi',
            name='score_calculation_automation',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='score_calculation_type',
            field=models.CharField(default=b'', max_length=50, blank=True, choices=[(b'sum', 'Sum'), (b'average', 'Average'), (b'most_recent', 'Most Recent')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpi',
            name='score_calculation_automation',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpi',
            name='score_calculation_type',
            field=models.CharField(default=b'', max_length=50, blank=True, choices=[(b'sum', 'Sum'), (b'average', 'Average'), (b'most_recent', 'Most Recent')]),
            preserve_default=True,
        ),
    ]
