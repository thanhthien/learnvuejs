# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0056_auto_20151202_0807'),
    ]

    operations = [
        migrations.AddField(
            model_name='quotation',
            name='commitment_1',
            field=models.TextField(default=b''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='quotation',
            name='commitment_2',
            field=models.TextField(default=b''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='quotation',
            name='commitment_3',
            field=models.TextField(default=b''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='quotation',
            name='commitment_4',
            field=models.TextField(default=b''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='quotation',
            name='commitment_5',
            field=models.TextField(default=b''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='quotation',
            name='commitment_6',
            field=models.TextField(default=b''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='quotation',
            name='commitment_8',
            field=models.TextField(default=b''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='quotation',
            name='employees',
            field=models.IntegerField(default=200, null=True),
            preserve_default=True,
        ),
    ]
