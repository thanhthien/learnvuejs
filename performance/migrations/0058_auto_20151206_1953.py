# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0057_auto_20151206_1916'),
    ]

    operations = [
        migrations.AddField(
            model_name='quotation',
            name='package_premium_1',
            field=models.IntegerField(default=0, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='quotation',
            name='package_premium_2',
            field=models.IntegerField(default=0, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='quotation',
            name='package_premium_3',
            field=models.IntegerField(default=0, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='quotation',
            name='package_premium_total',
            field=models.IntegerField(default=50000000, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='quotation',
            name='package_standard_1',
            field=models.IntegerField(default=0, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='quotation',
            name='package_standard_2',
            field=models.IntegerField(default=0, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='quotation',
            name='package_standard_3',
            field=models.IntegerField(default=0, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='quotation',
            name='package_standard_total',
            field=models.IntegerField(default=35000000, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='quotation',
            name='package_training_1',
            field=models.IntegerField(default=0, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='quotation',
            name='package_training_2',
            field=models.IntegerField(default=0, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='quotation',
            name='package_training_3',
            field=models.IntegerField(default=0, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='quotation',
            name='package_training_total',
            field=models.IntegerField(default=15000000, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='quotation',
            name='commitment_3',
            field=models.TextField(default=b''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='quotation',
            name='commitment_6',
            field=models.TextField(default=b''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='quotation',
            name='commitment_8',
            field=models.TextField(default=b''),
            preserve_default=True,
        ),
    ]
