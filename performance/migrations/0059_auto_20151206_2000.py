# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0058_auto_20151206_1953'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='quotation',
            name='package_premium_3',
        ),
        migrations.RemoveField(
            model_name='quotation',
            name='package_standard_3',
        ),
        migrations.RemoveField(
            model_name='quotation',
            name='package_training_3',
        ),
        migrations.AddField(
            model_name='quotation',
            name='package_app',
            field=models.IntegerField(default=0, null=True),
            preserve_default=True,
        ),
    ]
