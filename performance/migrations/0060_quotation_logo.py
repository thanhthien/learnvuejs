# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0059_auto_20151206_2000'),
    ]

    operations = [
        migrations.AddField(
            model_name='quotation',
            name='logo',
            field=models.FileField(null=True, upload_to=b''),
            preserve_default=True,
        ),
    ]
