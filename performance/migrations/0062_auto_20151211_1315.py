# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('company', '0027_organization_kpi_end_date'),
        ('performance', '0061_auto_20151210_0125'),
    ]

    operations = [
        migrations.CreateModel(
            name='SWOT',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('removed', models.DateTimeField(default=None, null=True, editable=False, blank=True)),
                ('name', models.CharField(max_length=500)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('order', models.IntegerField(default=1)),
                ('weighting', models.IntegerField(default=10)),
                ('swot_type', models.CharField(default=b'strength', max_length=100, blank=True, choices=[(b'strength', 'Strength'), (b'weakness', 'Weakness'), (b'opportunity', 'Opportunity'), (b'thread', 'Thread')])),
                ('organisation', models.ForeignKey(to='company.Organization')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.AlterModelOptions(
            name='historicalcompetency',
            options={'ordering': ('-history_date', '-history_id'), 'get_latest_by': 'history_date', 'verbose_name': 'historical competency'},
        ),
        migrations.AlterModelOptions(
            name='historicalkpi',
            options={'ordering': ('-history_date', '-history_id'), 'get_latest_by': 'history_date', 'verbose_name': 'historical kpi'},
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='copy_from_id',
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='job_id',
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='parent_id',
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='quarter_period_id',
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='reviewer_id',
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='user_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='assigned_to_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='cascaded_from_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='copy_from_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='job_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='map_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='parent_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='quarter_period_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='refer_to_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='reviewer_id',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='user_id',
        ),
        migrations.AddField(
            model_name='historicalcompetency',
            name='copy_from',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.Competency', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalcompetency',
            name='job',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.JobTitle', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalcompetency',
            name='parent',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.Competency', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalcompetency',
            name='quarter_period',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.QuarterPeriod', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalcompetency',
            name='reviewer',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalcompetency',
            name='user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='assigned_to',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='cascaded_from',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.KPI', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='copy_from',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.KPI', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='job',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.JobTitle', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='map',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.StrategyMap', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='parent',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.KPI', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='quarter_period',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.QuarterPeriod', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='refer_to',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='performance.KPI', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='reviewer',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='historicalcompetency',
            name='history_user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='historicalkpi',
            name='history_user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
    ]
