# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0063_auto_20151211_1919'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalkpi',
            name='month_1',
            field=models.FloatField(null=True, verbose_name='month 1', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='month_2',
            field=models.FloatField(null=True, verbose_name='month 1', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='month_3',
            field=models.FloatField(null=True, verbose_name='month 1', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpi',
            name='month_1',
            field=models.FloatField(null=True, verbose_name='month 1', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpi',
            name='month_2',
            field=models.FloatField(null=True, verbose_name='month 1', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpi',
            name='month_3',
            field=models.FloatField(null=True, verbose_name='month 1', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='historicalkpi',
            name='score_calculation_type',
            field=models.CharField(default=b'most_recent', max_length=50, null=True, blank=True, choices=[(b'', 'N/A'), (b'sum', 'Sum'), (b'average', 'Average'), (b'most_recent', 'Most Recent')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kpi',
            name='score_calculation_type',
            field=models.CharField(default=b'most_recent', max_length=50, null=True, blank=True, choices=[(b'', 'N/A'), (b'sum', 'Sum'), (b'average', 'Average'), (b'most_recent', 'Most Recent')]),
            preserve_default=True,
        ),
    ]
