# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0065_auto_20151216_0633'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalkpi',
            name='temp_weight',
            field=models.IntegerField(null=True, verbose_name='Temp Weight', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpi',
            name='temp_weight',
            field=models.IntegerField(null=True, verbose_name='Temp Weight', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='historicalkpi',
            name='weight',
            field=models.IntegerField(default=10, verbose_name='Weighting', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kpi',
            name='weight',
            field=models.IntegerField(default=10, verbose_name='Weighting', blank=True),
            preserve_default=True,
        ),
    ]
