# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0066_auto_20151218_1333'),
    ]

    operations = [
        migrations.AddField(
            model_name='bscdepartment',
            name='name_fi',
            field=models.CharField( max_length=2000, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bscjobtitle',
            name='name_fi',
            field=models.CharField( max_length=2000, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bsckpi',
            name='measurement_fi',
            field=models.CharField( max_length=500, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bsckpi',
            name='name_fi',
            field=models.CharField( max_length=2000, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='competency',
            name='description_fi',
            field=models.TextField( null=True, verbose_name='Description', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='competency',
            name='name_fi',
            field=models.CharField( max_length=2000, null=True, verbose_name='Name', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='competencylib',
            name='description_fi',
            field=models.TextField( null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='competencylib',
            name='name_fi',
            field=models.CharField( max_length=2000, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='descriptioncell',
            name='description_fi',
            field=models.TextField( null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='descriptioncell',
            name='title_fi',
            field=models.CharField( max_length=100, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalcompetency',
            name='description_fi',
            field=models.TextField( null=True, verbose_name='Description', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalcompetency',
            name='name_fi',
            field=models.CharField( max_length=2000, null=True, verbose_name='Name', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='description_fi',
            field=models.TextField( null=True, verbose_name='Description', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='historicalkpi',
            name='name_fi',
            field=models.CharField( max_length=2000, null=True, verbose_name='Name', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='jobtitle',
            name='description_fi',
            field=models.TextField( null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='jobtitle',
            name='factors_fi',
            field=models.TextField( null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='jobtitle',
            name='name_fi',
            field=models.CharField( max_length=256, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpi',
            name='description_fi',
            field=models.TextField( null=True, verbose_name='Description', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpi',
            name='name_fi',
            field=models.CharField( max_length=2000, null=True, verbose_name='Name', blank=True),
            preserve_default=True,
        ),
    ]
