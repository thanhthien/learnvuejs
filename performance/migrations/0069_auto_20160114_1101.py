# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0068_auto_20151223_1129'),
    ]

    operations = [
        # migrations.AddField(
        #     model_name='historicalkpi',
        #     name='archivable_score_score',
        #     field=models.IntegerField(default=0, editable=False, blank=True),
        #     preserve_default=True,
        # ),
        # migrations.AddField(
        #     model_name='historicalkpi',
        #     name='archivable_score_votes',
        #     field=models.PositiveIntegerField(default=0, editable=False, blank=True),
        #     preserve_default=True,
        # ),
        # migrations.AddField(
        #     model_name='kpi',
        #     name='archivable_score_score',
        #     field=models.IntegerField(default=0, editable=False, blank=True),
        #     preserve_default=True,
        # ),
        # migrations.AddField(
        #     model_name='kpi',
        #     name='archivable_score_votes',
        #     field=models.PositiveIntegerField(default=0, editable=False, blank=True),
        #     preserve_default=True,
        # ),
    ]
