# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0069_auto_20160114_1101'),
    ]

    operations = [
        # migrations.RemoveField(
        #     model_name='historicalkpi',
        #     name='archivable_score_score',
        # ),
        # migrations.RemoveField(
        #     model_name='historicalkpi',
        #     name='archivable_score_votes',
        # ),
        # migrations.RemoveField(
        #     model_name='kpi',
        #     name='archivable_score_score',
        # ),
        # migrations.RemoveField(
        #     model_name='kpi',
        #     name='archivable_score_votes',
        # ),
        migrations.AddField(
            model_name='historicalkpi',
            name='archivable_score',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='kpi',
            name='archivable_score',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='swot',
            name='swot_type',
            field=models.CharField(default=b'strength', max_length=100, blank=True, choices=[(b'strength', 'Strength'), (b'weakness', 'Weakness'), (b'opportunity', 'Opportunity'), (b'threat', 'Threat')]),
            preserve_default=True,
        ),
    ]
