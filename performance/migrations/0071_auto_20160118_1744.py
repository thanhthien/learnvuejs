# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0070_auto_20160114_1141'),
    ]

    operations = [
        migrations.AlterField(
            model_name='strategymap',
            name='year',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
