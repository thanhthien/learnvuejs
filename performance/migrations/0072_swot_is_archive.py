# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0071_auto_20160118_0907'),
    ]

    operations = [
        migrations.AddField(
            model_name='swot',
            name='is_archive',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
    ]
