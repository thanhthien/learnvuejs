# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0072_merge'),
        ('performance', '0072_swot_is_archive'),
    ]

    operations = [
    ]
