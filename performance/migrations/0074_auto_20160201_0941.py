# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0073_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='historicalkpi',
            name='ordering',
            field=models.FloatField(default=1, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kpi',
            name='ordering',
            field=models.FloatField(default=1, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='swot',
            name='is_archive',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
