# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0074_auto_20160201_0941'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kpilog',
            name='desciption',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
