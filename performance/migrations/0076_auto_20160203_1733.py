# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0075_auto_20160202_1407'),
    ]

    operations = [
        migrations.AlterField(
            model_name='taskeverystage',
            name='name',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
