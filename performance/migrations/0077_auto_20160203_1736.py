# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0076_auto_20160203_1733'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kpitask',
            name='name',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
