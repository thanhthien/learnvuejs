# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0077_auto_20160203_1736'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cascadekpiqueue',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, db_index=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='cascadekpiqueue',
            name='is_run',
            field=models.BooleanField(default=False, db_index=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='review',
            name='updated_at',
            field=models.DateField(auto_now=True, db_index=True),
            preserve_default=True,
        ),
    ]
