# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0078_auto_20160315_1159'),
    ]

    operations = [

        # migrations.AlterField(
        #     model_name='historicalkpi',
        #     name='name',
        #     field=models.CharField(db_index=True, max_length=2000, null=True, verbose_name='Name', blank=True),
        #     preserve_default=True,
        # ),
        # migrations.AlterField(
        #     model_name='historicalkpi',
        #     name='name_en',
        #     field=models.CharField(db_index=True, max_length=2000, null=True, verbose_name='Name', blank=True),
        #     preserve_default=True,
        # ),
        # migrations.AlterField(
        #     model_name='historicalkpi',
        #     name='name_fi',
        #     field=models.CharField(db_index=True, max_length=2000, null=True, verbose_name='Name', blank=True),
        #     preserve_default=True,
        # ),
        # migrations.AlterField(
        #     model_name='historicalkpi',
        #     name='name_vi',
        #     field=models.CharField(db_index=True, max_length=2000, null=True, verbose_name='Name', blank=True),
        #     preserve_default=True,
        # ),
        # migrations.AlterField(
        #     model_name='kpi',
        #     name='name',
        #     field=models.CharField(db_index=True, max_length=2000, null=True, verbose_name='Name', blank=True),
        #     preserve_default=True,
        # ),
        # migrations.AlterField(
        #     model_name='kpi',
        #     name='name_en',
        #     field=models.CharField(db_index=True, max_length=2000, null=True, verbose_name='Name', blank=True),
        #     preserve_default=True,
        # ),
        # migrations.AlterField(
        #     model_name='kpi',
        #     name='name_fi',
        #     field=models.CharField(db_index=True, max_length=2000, null=True, verbose_name='Name', blank=True),
        #     preserve_default=True,
        # ),
        # migrations.AlterField(
        #     model_name='kpi',
        #     name='name_vi',
        #     field=models.CharField(db_index=True, max_length=2000, null=True, verbose_name='Name', blank=True),
        #     preserve_default=True,
        # ),
    ]
