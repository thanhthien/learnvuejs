# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('company', '0033_auto_20160315_1159'),
        ('performance', '0079_auto_20160413_1137'),
    ]

    operations = [
        migrations.CreateModel(
            name='BSC_KSF',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('removed', models.DateTimeField(default=None, null=True, editable=False, blank=True)),
                ('name', models.CharField(max_length=500)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('order', models.IntegerField(default=1)),
                ('weighting', models.IntegerField(default=10)),
                ('is_hidden', models.BooleanField(default=False)),
                ('important_1', models.IntegerField(null=True)),
                ('important_2', models.IntegerField(null=True)),
                ('important_3', models.IntegerField(null=True)),
                ('important_4', models.IntegerField(null=True)),
                ('important_5', models.IntegerField(null=True)),
                ('achivable_1', models.IntegerField(null=True)),
                ('achivable_2', models.IntegerField(null=True)),
                ('achivable_3', models.IntegerField(null=True)),
                ('achivable_4', models.IntegerField(null=True)),
                ('achivable_5', models.IntegerField(null=True)),
                ('product', models.FloatField(null=True)),
                ('bsc_type', models.CharField(default=b'financial', max_length=100, blank=True, choices=[(b'financial', 'Financial'), (b'customer', 'Customer'), (b'internal', 'Internal Process'), (b'learninggrowth', 'Learning & Growth'), (b'other', 'Other')])),
                ('organization', models.ForeignKey(to='company.Organization')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        # migrations.RemoveField(
        #     model_name='ksf',
        #     name='organization',
        # ),
        # migrations.RemoveField(
        #     model_name='ksf',
        #     name='user',
        # ),
        # migrations.DeleteModel(
        #     name='KSF',
        # ),
    ]
