# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0080_auto_20160417_2331'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='copy_from',
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='history_user',
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='job',
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='parent',
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='quarter_period',
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='reviewer',
        ),
        migrations.RemoveField(
            model_name='historicalcompetency',
            name='user',
        ),
        migrations.DeleteModel(
            name='HistoricalCompetency',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='assigned_to',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='cascaded_from',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='copy_from',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='history_user',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='job',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='map',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='parent',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='quarter_period',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='refer_to',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='reviewer',
        ),
        migrations.RemoveField(
            model_name='historicalkpi',
            name='user',
        ),
        migrations.DeleteModel(
            name='HistoricalKPI',
        ),
        migrations.RemoveField(
            model_name='ksf',
            name='organization',
        ),
        migrations.RemoveField(
            model_name='ksf',
            name='user',
        ),
        migrations.DeleteModel(
            name='KSF',
        ),
        # migrations.AlterField(
        #     model_name='kpi',
        #     name='name',
        #     field=models.CharField(db_index=True, max_length=2000, null=True, verbose_name='Name', blank=True),
        #     preserve_default=True,
        # ),
        # migrations.AlterField(
        #     model_name='kpi',
        #     name='name_en',
        #     field=models.CharField(db_index=True, max_length=2000, null=True, verbose_name='Name', blank=True),
        #     preserve_default=True,
        # ),
        # migrations.AlterField(
        #     model_name='kpi',
        #     name='name_fi',
        #     field=models.CharField(db_index=True, max_length=2000, null=True, verbose_name='Name', blank=True),
        #     preserve_default=True,
        # ),
        # migrations.AlterField(
        #     model_name='kpi',
        #     name='name_vi',
        #     field=models.CharField(db_index=True, max_length=2000, null=True, verbose_name='Name', blank=True),
        #     preserve_default=True,
        # ),
    ]
