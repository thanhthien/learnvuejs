# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0081_auto_20160418_2133'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kpi',
            name='approval_status',
            field=models.CharField(default=b'', max_length=100, blank=True, choices=[(b'', 'Not Finished'), (b'finished', 'Finished'), (b'rejected', 'Rejected'), (b'approved', 'Approved')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kpi',
            name='name',
            field=models.CharField(db_index=True, max_length=2000, null=True, verbose_name='Name', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kpi',
            name='name_en',
            field=models.CharField(db_index=True, max_length=2000, null=True, verbose_name='Name', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kpi',
            name='name_fi',
            field=models.CharField(db_index=True, max_length=2000, null=True, verbose_name='Name', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kpi',
            name='name_vi',
            field=models.CharField(db_index=True, max_length=2000, null=True, verbose_name='Name', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kpi',
            name='prefix_id',
            field=models.CharField(max_length=100, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kpi',
            name='review_type',
            field=models.CharField(default=b'', max_length=100, blank=True, choices=[(b'', 'Final'), (b'daily', 'Daily'), (b'weekly', 'Weekly'), (b'monthly', 'Monthly')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='kpi',
            name='score_calculation_type',
            field=models.CharField(default=b'most_recent', max_length=100, null=True, blank=True, choices=[(b'', 'N/A'), (b'sum', 'Sum'), (b'average', 'Average'), (b'most_recent', 'Most Recent')]),
            preserve_default=True,
        ),
    ]
