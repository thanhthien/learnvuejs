# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-05-29 22:40
from __future__ import unicode_literals
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0088_auto_20160529_1332'),
    ]

    operations = [
        migrations.AddField(
            model_name='strategymap',
            name='director',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE,
                                    to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterUniqueTogether(
            name='strategymap',
            unique_together=set([('organization', 'director', 'quarter_period')]),
        ),
    ]
