# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-07-17 18:40
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0097_auto_20160715_2044'),
    ]

    operations = [
        migrations.CreateModel(
            name='KPILock',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('permission', models.IntegerField(choices=[(0, 'Allow all'), (1, 'Lock Employee'), (2, 'Lock Manager')], default=0)),
                ('kpi', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='performance.KPI')),
            ],
        ),
    ]
