# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2017-02-11 18:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0111_auto_20170207_2214'),
    ]

    operations = [
        migrations.AddField(
            model_name='kpi',
            name='log_trace',
            field=models.TextField(blank=True, default=b'', null=True, verbose_name='log'),
        ),
        migrations.AlterField(
            model_name='kpi',
            name='month_1_target',
            field=models.FloatField(blank=True, default=0, null=True, verbose_name='month 1 Target'),
        ),
        migrations.AlterField(
            model_name='kpi',
            name='month_2_target',
            field=models.FloatField(blank=True, default=0, null=True, verbose_name='month 2 Target'),
        ),
        migrations.AlterField(
            model_name='kpi',
            name='month_3_target',
            field=models.FloatField(blank=True, default=0, null=True, verbose_name='month 3 Target'),
        ),
        migrations.AlterField(
            model_name='kpi',
            name='target',
            field=models.FloatField(blank=True, default=0, null=True, verbose_name='Target'),
        ),
        migrations.AlterField(
            model_name='quarterperiod',
            name='status',
            field=models.CharField(choices=[(b'PE', 'Pending'), (b'IN', 'In Progress'), (b'AR', 'Archive'), (b'DELETED', 'Deleted')], db_index=True, default=b'PE', max_length=10),
        ),
    ]
