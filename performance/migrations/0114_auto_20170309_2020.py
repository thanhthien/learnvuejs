# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2017-03-09 20:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0113_auto_20170220_1025'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kpicomment',
            name='is_user_notified',
            field=models.BooleanField(db_index=True, default=False),
        ),
    ]
