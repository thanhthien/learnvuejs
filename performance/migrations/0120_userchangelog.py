# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2017-04-25 11:04
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('performance', '0119_auto_20170418_1639'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserChangeLog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pre_email', models.CharField(default=b'', max_length=100)),
                ('post_email', models.CharField(default=b'', max_length=100)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('actor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='actor', to=settings.AUTH_USER_MODEL)),
                ('quarter', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='performance.QuarterPeriod')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
