# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2017-08-04 01:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0127_reportquery'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reportquery',
            name='created_at',
            field=models.DateField(),
        ),
    ]
