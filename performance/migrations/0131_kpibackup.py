# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2017-08-28 13:21
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('performance', '0130_auto_20170804_0128'),
    ]

    operations = [
        migrations.CreateModel(
            name='KPIBackup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('removed', models.DateTimeField(blank=True, db_index=True, default=None, editable=False, null=True)),
                ('month', models.IntegerField(blank=True, null=True)),
                ('result', models.FloatField(blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('data', jsonfield.fields.JSONField()),
                ('created_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='created_by', to=settings.AUTH_USER_MODEL)),
                ('quarter', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='performance.QuarterPeriod')),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
