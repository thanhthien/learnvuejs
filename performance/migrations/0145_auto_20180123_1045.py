# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2018-01-23 10:45
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0144_auto_20180118_1609'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kpi',
            name='ordering',
            field=models.IntegerField(blank=True, db_index=True, default=1, null=True, verbose_name='Ordering'),
        ),
    ]
