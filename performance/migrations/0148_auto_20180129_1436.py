# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2018-01-29 14:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0147_auto_20180129_1228'),
    ]

    operations = [

        migrations.AddField(
            model_name='kpi',
            name='year_result',
            field=models.FloatField(blank=True, null=True, verbose_name='Year result'),
        ),
        migrations.AddField(
            model_name='kpi',
            name='year_score',
            field=models.FloatField(blank=True, null=True, verbose_name='Year score'),
        ),

    ]
