# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2018-02-28 17:04
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0155_auto_20180228_1703'),
    ]

    operations = [
        migrations.AddField(
            model_name='kpilib',
            name='extra_data',
            field=jsonfield.fields.JSONField(blank=True, default={}, editable=False),
        ),
        migrations.AddField(
            model_name='kpilib',
            name='industry',
            field=jsonfield.fields.JSONField(blank=True, default=[]),
        ),
        migrations.AddField(
            model_name='kpilib',
            name='objective',
            field=models.CharField(default='', max_length=1000),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='kpilib',
            name='operator',
            field=models.CharField(choices=[(b'>=', b'>='), (b'<=', b'<='), (b'=', b'=')], default='>=', max_length=10),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='kpilib',
            name='position',
            field=jsonfield.fields.JSONField(blank=True, default=[]),
        ),
    ]
