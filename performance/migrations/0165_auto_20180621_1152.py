# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2018-06-21 11:52
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('performance', '0164_auto_20180615_1519'),
    ]

    operations = [
        migrations.CreateModel(
            name='KPIActionPlan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file', models.FileField(blank=True, max_length=500, null=True, upload_to=b'kpi_action_plan/attach/v.cloudjetkpi.com/%Y/%m/%d/')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('quarter_period', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='performance.QuarterPeriod')),
                ('user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
            ],
        ),

    ]
