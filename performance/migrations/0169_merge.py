# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0168_merge'),
        ('performance', '0167_merge'),
    ]

    operations = [
    ]
