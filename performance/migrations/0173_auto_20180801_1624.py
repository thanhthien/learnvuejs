# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2018-08-01 16:24
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0172_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='strategymap',
            name='removed',
            field=models.DateTimeField(blank=True, db_index=True, default=None, editable=False, null=True),
        ),
        migrations.AlterField(
            model_name='kpiactionplan',
            name='file',
            field=models.FileField(blank=True, max_length=500, null=True, upload_to=b'kpi_action_plan/attach/localhost:8000/%Y/%m/%d/'),
        ),
        migrations.AlterField(
            model_name='kpicomment',
            name='file',
            field=models.FileField(blank=True, max_length=500, null=True, upload_to=b'comment/attach/localhost:8000/%Y/%m/%d/'),
        ),
    ]
