# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2018-08-17 15:48
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('performance', '0178_auto_20180817_1114'),
        ('performance', '0178_merge'),
    ]

    operations = [
    ]
