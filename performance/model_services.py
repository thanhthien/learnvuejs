from django.core.exceptions import ObjectDoesNotExist


def set_cascaded_from_attrs(self, actor=None):
    if self.cascaded_from_id:

        try:

            k = self.cascaded_from
            self.clone_attrs(k, actor)

        except ObjectDoesNotExist:  # pragma: no cover
            pass


def set_assigned_kpi_attrs(kpi, actor=None):
    if kpi.assigned_to_id:
        assigned_kpi = kpi.get_assigned_kpi()

        kpi.clone_attrs(assigned_kpi, actor)

        return assigned_kpi


def clone_attrs(kpi, other_kpi, actor, update_prefix_id=True):
    if other_kpi:
        other_kpi.name = kpi.name
        other_kpi.unit = kpi.unit
        other_kpi.current_goal = kpi.current_goal
        other_kpi.future_goal = kpi.future_goal
        other_kpi.operator = kpi.operator
        other_kpi.target = kpi.target

        other_kpi.quarter_one_target = kpi.quarter_one_target
        other_kpi.quarter_two_target = kpi.quarter_two_target
        other_kpi.quarter_three_target = kpi.quarter_three_target
        other_kpi.quarter_four_target = kpi.quarter_four_target

        other_kpi.month_1 = kpi.month_1
        other_kpi.month_2 = kpi.month_2
        other_kpi.month_3 = kpi.month_3

        other_kpi.month_1_target = kpi.month_1_target
        other_kpi.month_2_target = kpi.month_2_target
        other_kpi.month_3_target = kpi.month_3_target

        other_kpi.month_1_score = kpi.month_1_score
        other_kpi.month_2_score = kpi.month_2_score
        other_kpi.month_3_score = kpi.month_3_score
        other_kpi.latest_score = kpi.latest_score

        other_kpi.score_calculation_type = kpi.score_calculation_type
        other_kpi.review_type = kpi.review_type

        other_kpi.real = kpi.real
        other_kpi.latest_score = kpi.latest_score

        if update_prefix_id:
            other_kpi.prefix_id = kpi.prefix_id
            other_kpi.ordering = kpi.ordering

        # if score:
        #     k.set_score(score, kpi.user_id,save = False)
        changed = None
        try:
            changed = other_kpi.get_changed_history()
        except:  # pragma: no cover
            pass
        if actor is None:
            actor = kpi.user

        # will not enter if block. hence, no cover
        if False and changed:  # pragma: no cover

            try:
                from performance.logger import KPILogger

                KPILogger(actor, other_kpi, 'update', changed).start()
            except:  # pragma: no cover
                pass

        other_kpi.save(add_queue=False)
        return other_kpi
