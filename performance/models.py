# !/usr/bin/python
# -*- coding: utf-8 -*-
import datetime
import hashlib
import json

from auditlog.models import AuditlogHistoryField
from auditlog.registry import auditlog
from django.template.defaultfilters import truncatechars

from utils import postpone, reload_model_obj
import os
# import dill
# from backports.functools_lru_cache import lru_cache
# from django.core.cache import caches
import markdown
import redis
import requests
# from elms.settings import DOMAIN
# from elms.settings import STATIC_URL
# from elms.settings import redis_url, SLACK_CRONJOB_CHANNEL, SLACK_ERROR_CHANNEL
from django.conf import settings
from django.contrib.auth.models import User
from django.core import serializers
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.db.backends.postgresql.base import DatabaseError
from django.db.models import Q
from django.db.models.aggregates import Max, Avg
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _
from django_permanent.models import PermanentModel
# from django.conf.settings import *
from jsonfield import JSONField
from model_utils.tracker import FieldTracker

from authorization.rules import KPI__EDITING, KPI__REVIEWING, DELETE_KPI
from authorization.rules import has_perm
from company.models import Organization, UserOrganization

from elms.models import CJSModel
from performance.data.fix_kpi import *
# from performance.documents import ScoreHistory
from performance.model_services import clone_attrs, set_assigned_kpi_attrs, set_cascaded_from_attrs
from utils.caching_name import CREATING_ASSESSMENT_ORG, ORGANIZATION__GET_CURRENT_QUARTER
from utils.common import is_redis_available, fix_input_float
from utils.kpi_utils import kpi_score_calculator, calculate_score
from utils.log import log_trace
from utils.random_str import random_string
from user_profile.models import *
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.db.models.expressions import Case, When, Value

from utils.common import get_redis, set_redis, remove_redis
from performance.tracker import CJSFieldTracker

logger = logging.getLogger('django')

REVIEW_TYPE = (
    ('self', _('Employee self assessment')),
    ('manager', _('Manager team review')),
    ('peer', _('Peer Review')),
    ('senior_manager', _('Senior Management')),
    ('direct_report', _('Direct Reports')),
    # ('clients', _('Clients')),
)

#   Not use
# def comment_file_path(instance, filename):
#     ext = filename.split('.')[-1]
#     name = filename.split('.')[0]
#     today = datetime.date.today()
#     filename = "comment/%d/%d/%d/%s.%s" % (today.year, today.month, today.day, name, ext)
#     return filename


def is_nan(value):
    # http://stackoverflow.com/questions/944700/how-to-check-for-nan-in-python
    return value != value

# @classmethod      Not use
# def get_full_competency(owner, time):
#     comps = Competency.objects.filter(user=owner)
#     final_value = 0
#     counter = 0
#     for c in comps:
#         final_value += c.get_value(time)
#         counter += 1
#
#     final_value = final_value / counter


def get_symble_category():
    return Case(When(bsc_category='financial', then=Value('A')),
                When(bsc_category='customer', then=Value('B')),
                When(bsc_category='internal', then=Value('C')),
                When(bsc_category='learninggrowth', then=Value('D')),
                default=Value('O'), output_field=models.CharField())


class YearPlan(models.Model):
    year = models.IntegerField()
    organization = models.ForeignKey(Organization)

    def save(self, *args, **kwargs):
        if self.year is None:
            self.year = datetime.now().year
        super(YearPlan, self).save(*args, **kwargs)

    @classmethod
    def get_current_yearplan(cls, organization):
        yps = YearPlan.objects.filter(organization=organization).order_by('-id')
        if yps.count() == 0:
            yp = YearPlan(organization=organization)
            yp.save()

            return yp
        else:
            return yps[0]


class QuarterPeriod(PermanentModel):  # models.Model):
    STATUS_CHOICE = (
        ('PE', _('Pending')),  # ALERT: NOT USING THIS CHOICE ANY MORE since FEB 2017
        ('IN', _('In Progress')),
        ('AR', _('Archive')),
        ('DELETED', _('Deleted')),
    )
    history = AuditlogHistoryField()
    organization = models.ForeignKey(Organization)
    quarter_name = models.CharField(max_length=200, blank=True, default='')
    due_date = models.DateField(db_index=True)
    last_send_email_remind = models.DateTimeField(null=True, blank=True)
    # this field indicate what current quarter in kpi is (ex. quarter=1 --> quarter_one_target)
    quarter = models.IntegerField(default=1)
    #  clone_from_quarter = models.IntegerField(null=True, blank=True)
    clone_from_quarter_period = models.ForeignKey('self', null=True, blank=True)
    # this year may different to due_date.year,
    # ex. trường hợp đánh giá lố qua tháng 1/2016 nhưng quarter_period là của năm 2015
    year = models.IntegerField(default=0)

    status = models.CharField(choices=STATUS_CHOICE, max_length=10, default="PE", db_index=True)
    completion = models.IntegerField(default=0)

    yearplan = models.ForeignKey(YearPlan, null=True, blank=True)  # unuse

    start_rewarding = models.FloatField(null=True, blank=True)
    end_rewarding = models.FloatField(null=True, blank=True)
    max_rewarding = models.FloatField(null=True, blank=True)
    reward_cap = models.FloatField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    is_ready = models.BooleanField(default=True)
    unsaved = models.BooleanField(default=True)
    update_perform_reward = models.BooleanField(default=False)
    # kpi_score_changed = models.BooleanField(default=False)
    # TODO: set quarter/ year khi update or insert new quarter

    # NOTE: quater_*_year là các năm tương ứng với 4 fields trong KPI object:
    #   quarter_one_target
    #   quarter_two_target
    #   quarter_three_target
    #   quarter_four_target
    #
    # từ các fields này, ta tính toán ra thứ tự của 4 field quarter_*_target trong KPI object
    # (vì các năm có thể khác nhau nên tên quarter_*_target không ám chỉ thứ tự của chúng)
    quarter_1_year = models.IntegerField(null=True, blank=True)
    quarter_2_year = models.IntegerField(null=True, blank=True)
    quarter_3_year = models.IntegerField(null=True, blank=True)
    quarter_4_year = models.IntegerField(null=True, blank=True)

    def mark_quarter_ready(self):
        '''
        Currently, the is_ready field seem not affect to our system logic
        :return:
        '''
        self.is_ready = True
        self.save(update_fields=['is_ready', ])

    def month_1_name(self):
        if self.quarter == 1:
            return ugettext("January")
        elif self.quarter == 2:
            return ugettext("April")
        elif self.quarter == 3:
            return ugettext("July")
        else:
            return ugettext("October")

    def month_2_name(self):
        if self.quarter == 1:
            return ugettext("February")
        elif self.quarter == 2:
            return ugettext("May")
        elif self.quarter == 3:
            return ugettext("August")
        else:
            return ugettext("November")

    def month_3_name(self):
        if self.quarter == 1:
            return ugettext("March")
        elif self.quarter == 2:
            return ugettext("June")
        elif self.quarter == 3:
            return ugettext("September")
        else:
            return ugettext("December")

    def to_json(self):
        # temp = serializers.serialize('json', [self])
        js = json.loads(serializers.serialize('json', [self]))[0]

        # TODO: limit fields to be exposed
        j = js['fields']

        j['id'] = self.id
        j['date'] = format(self.due_date, 'd-m-Y')
        j['month_1_name'] = self.month_1_name()
        j['month_2_name'] = self.month_2_name()
        j['month_3_name'] = self.month_3_name()

        return j

    review_mode = models.CharField(default='quarterly', blank=True, null=True, max_length=20)  # quarterly or monthly

    def get_quarter_order(self):
        if self.quarter_1_year is None \
                or self.quarter_2_year is None \
                or self.quarter_3_year is None \
                or self.quarter_4_year is None:
            if self.quarter_1_year is None:
                self.quarter_1_year = self.year
            if self.quarter_2_year is None:
                self.quarter_2_year = self.year
            if self.quarter_3_year is None:
                self.quarter_3_year = self.year
            if self.quarter_4_year is None:
                self.quarter_4_year = self.year

            self.save()

        orders = {}
        orders['quarter1_order'] = 0
        orders['quarter2_order'] = 1 + (self.quarter_2_year - self.quarter_1_year) * 4
        orders['quarter3_order'] = 2 + (self.quarter_3_year - self.quarter_1_year) * 4
        orders['quarter4_order'] = 3 + (self.quarter_4_year - self.quarter_1_year) * 4

        orders['quarter1year'] = self.quarter_1_year
        orders['quarter2year'] = self.quarter_2_year
        orders['quarter3year'] = self.quarter_3_year
        orders['quarter4year'] = self.quarter_4_year

        orders['month_1_name'] = self.month_1_name()
        orders['month_2_name'] = self.month_2_name()
        orders['month_3_name'] = self.month_3_name()
        return orders

    def save(self, *args, **kwargs):

        # if self.id is None:
        #     last_quarter = QuarterPeriod.objects.filter(organization=self.organization,
        #                                                 year=datetime.datetime.today().year).order_by('-id')
        #     last_quarter = last_quarter.first()
        #     if last_quarter:
        #         self.quarter = last_quarter.quarter + 1
        #         if self.quarter > 4 and self.review_mode == 'quarterly':
        #             self.quarter = 1
        #             self.year += 1
        #
        #         if self.quarter > 12 and self.review_mode == 'monthly':
        #             self.quarter = 1
        #             self.year += 1

        key = ORGANIZATION__GET_CURRENT_QUARTER.format(self.organization_id)
        cache.delete(key)

        if self.yearplan is None:
            self.yearplan = YearPlan.get_current_yearplan(self.organization)
        super(QuarterPeriod, self).save(*args, **kwargs)
        from performance.services.notification import add_notifications
        add_notifications('quarterperiod', self.organization)

    class Meta:
        ordering = ('organization', 'due_date',)
        # unique_together = ('organization', 'due_date')

    def get_strategy_map(self):
        from performance.db_models.strategy_map import StrategyMap
        return StrategyMap.objects.filter(quarter_period=self).first()

    def get_previous_quarter(self):
        q = QuarterPeriod.objects.filter(organization=self.organization, id__lt=self.id).order_by('-id').first()
        return q

    def __unicode__(self):  # pragma: no cover
        try:
            return u" #{0} {1} | {2}".format(self.id, self.quarter_name, format(self.due_date, "d-m-Y"))
        except:
            return ''

    def is_expired(self):
        if self.due_date >= date.today():
            return False
        return True

        # def percent_complete(self):
        #     return 0
        # avg = Review.objects.filter(organization_id=self.organization_id,
        #                             quarter_period=self).aggregate(percent=Avg('completion'))['percent']
        # if avg:
        #     return int(avg)
        # else:
        #     return 0

    def update_quarter(self):
        # max_quarter = QuarterPeriod.objects.filter(organization=self.organization,
        #                                            year=self.year).aggregate(max_quarter=Max('quarter'))
        # if max_quarter['max_quarter']:
        #     self.quarter = max_quarter['max_quarter'] + 1
        # else:
        #     self.quarter = 1
        #
        # self.save()
        return self.quarter

    def get_logs(self):
        return self.history.all()


class KPIActionPlan(models.Model):
    user = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    file = models.FileField(upload_to="kpi_action_plan/attach/{0}/%Y/%m/%d/".format(settings.DOMAIN.split('//')[-1]),
                            max_length=500, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    quarter_period = models.ForeignKey(QuarterPeriod, blank=True, null=True)
    description = models.TextField(default='')
    actor = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, related_name='action_plan_actor')

    def to_json(self):
        from user_profile.templatetags.user_tags import avatar
        # temp = serializers.serialize('json', [self])
        js = json.loads(serializers.serialize('json', [self]))[0]

        j = js['fields']
        j['file'] = self.file.url if self.file else None
        #        j['date'] = format(self.due_date, 'd-m-Y')
        j['user_email'] = self.user.email
        j['id'] = self.id
        j['avatar'] = avatar(self.user_id)
        j['display_name'] = self.user.profile.get_display_name()

        return j


class Review(models.Model):
    organization = models.ForeignKey(Organization)
    reviewer = models.ForeignKey(User, related_name="reviewers")
    reviewee = models.ForeignKey(User, related_name="reviewees", null=True, blank=True)
    review_type = models.CharField(max_length=20, choices=REVIEW_TYPE)
    invitation_accepted = models.BooleanField(default=False)

    quarter = models.IntegerField(default=1)
    year = models.IntegerField(default=0)
    quarter_period = models.ForeignKey(QuarterPeriod, blank=True, null=True)
    share_to_manager = models.BooleanField(default=True)
    share_to_all = models.BooleanField(default=True)
    completion = models.IntegerField(default=0)
    date_completed = models.DateField(null=True, blank=True)
    created_at = models.DateField(auto_now_add=True, blank=True)
    updated_at = models.DateField(auto_now=True, blank=True, db_index=True)
    number_tasks = models.IntegerField(default=0)
    is_complete = models.BooleanField(default=False)

    # def is_reviewee_active(self): Not use
    #
    #     user_org = self.reviewee.profile.get_user_organization()
    #     return user_org.user and user_org.user.profile.is_active()
    # TODO: not use
    # def to_json_export(self): Not use
    #
    #     data = []
    #
    #     data.append(self.reviewee_id)
    #     data.append(self.reviewee.profile.display_name)
    #     # data.append(self.reviewee.userorganization.employee_code)
    #     data.append(self.reviewee.email)
    #     data.append(self.completion)
    #
    #     return data

    def to_json(self):
        # temp = serializers.serialize('json', [self])
        js = json.loads(serializers.serialize('json', [self]))[0]

        # TODO: limit fields to be exposed
        j = js['fields']

        return j

    def __unicode__(self):  # pragma: no cover
        try:
            return self.reviewer.username + ' - ' + self.get_review_type_display()
        except:
            return ""

            # def get_hash_key(self):   # ToDo: Not use
            #     return signing.dumps({'organization': self.organization_id,
            #                           'reviewee': self.reviewee_id,
            #                           'reviewer': self.reviewer_id,
            #                           'review_type': self.review_type})
            #
            # def get_share_status(self):
            #     if self.review_type == "self":
            #         if self.share_to_manager:
            #             return ugettext("Share with manager")
            #
            #     if self.review_type == "manager":
            #         if self.share_to_all:
            #             return ugettext("Share to all")
            #
            #     if self.review_type in ["peer", 'senior_manager', 'direct_report']:
            #         r = Review.objects.filter(reviewee=self.reviewee, review_type='manager',
            #                                   quarter_period=self.quarter_period)
            #         if r.count() > 0:
            #             if r[0].share_to_manager:
            #                 return ugettext("Share with employee")
            #
            #     return ugettext("Not shared")


REMINDPHASE_CHOICE = (
    ('P0', _('Phase 0 - KPI Alignment')),
    ('P1', _('Phase 1 - Announce')),
    ('P2', _('Phase 2 - Remind')),
    ('P3', _('Phase 3 - Late Remind')),

)

BSC_FINANCIAL = 'financial'
BSC_CUSTOMER = 'customer'
BSC_PROCESS = 'internal'
BSC_LEARNINGGROWTH = 'learninggrowth'
BSC_OTHER = 'other'

BSC_CATEGORY = (
    ('financial', _('Financial')),
    ('customer', _('Customer')),
    ('internal', _('Internal Process')),
    ('learninggrowth', _('Learning & Growth')),
    ('other', _('Other')),

)

KPI_STATUS = (
    ('not_started', _('Not Started')),
    ('on_track', _('On Track')),
    ('completed', _('Completed')),
    ('postponed', _('Postponed')),
    ('cancelled', _('Cancelled')),
    ('behind', _('Behind')),
)

PERMISSION_CHOICE = (
    (0, ugettext('Allow all')),
    (1, ugettext('Lock Employee')),
    (2, ugettext('Lock Manager')),
)


# @lru_cache_function(max_size=1024, expiration=300)
# @postpone
def update_group_name(kpi, _root_id=None):
    # need to fix group at very first
    try:
       group = kpi.get_group()
    except Exception as ex:
        logger.info(str(ex))
        group = None

    key = "get_group_name{0}_{1}".format(kpi.id, _root_id)
    if cache.get(key, -1) != -1:
        return cache.get(key, '')
        # we add try/except here because of some None exception raise from get_group method
        # the exception occur rare time, and will auto-fix for the next user attempt to load kpi
        # so, we do not need to fix

    # <fix-bug-data>
    if _root_id > 0:  # pragma: no cover
        if kpi.refer_to_id > 0 and kpi.refer_to_id == _root_id:
            kpi.refer_to_id = None

            kpi.save(update_fields=['refer_to_id'])
    else:  # pragma: no cover
        _root_id = kpi.id
        # </fix-bug-data>

    if kpi.refer_to_id > 0:

        refer_to = kpi.get_refer_to()

        name = ""

        if refer_to:

            # <fix-bug-data>

            if refer_to.refer_to_id == kpi.id and refer_to.refer_to_id > 0:  # pragma: no cover
                refer_to.refer_to_id = None
                refer_to.save(update_fields=['refer_to_id'])
            # </fix-bug-data>

            name = get_group_name(refer_to, _root_id)
        else:
            name = kpi.name  # "[{0}] {1}".format(kpi.get_kpi_id_with_cat(), kpi.name)
    else:

        if group:
            name = group.name
        else:
            name = kpi.name

    if kpi.refer_group_name != name and name:  # trong truong hop name = rong thi khong update nua
        kpi.refer_group_name = name
        try:
            kpi.save(update_fields=['refer_group_name'])
        except DatabaseError:  # pragma: no cover
            pass

    cache.set(key, name, 60 * 5)

    return name


# @memoize(30)
def get_group_name(kpi, root_id=None):
    update_group_name(kpi, root_id)
    return kpi.refer_group_name


class KPIBackup(PermanentModel, CJSModel):
    quarter = models.ForeignKey(QuarterPeriod, null=True, blank=True, on_delete=models.SET_NULL)
    month = models.IntegerField(null=True, blank=True)
    year = models.IntegerField(null=True, blank=True)
    result = models.FloatField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL)
    created_by = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL, related_name='created_by')
    data = JSONField()


class KVStore(PermanentModel, CJSModel):
    key = models.CharField(max_length=100, default='', db_index=True)
    data = JSONField()

    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL, related_name='owner')
    organization = models.ForeignKey(Organization, null=True, blank=True, on_delete=models.SET_NULL)
    created_by = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL,
                                   related_name='kvstore_created_by')

    created_at = models.DateTimeField(auto_now_add=True)


def are_other_children_delayed(kpi):
    '''
    Step 1. Get the refer_to kpi (parent kpi)
    '''

    refer_to = None
    if kpi.refer_to_id:
        try:
            refer_to = kpi.refer_to
        except ObjectDoesNotExist:  # pragma: no cover
            pass

    if not refer_to and kpi.parent_id:
        try:
            refer_to = kpi.parent
        except ObjectDoesNotExist:  # pragma: no cover
            pass

    '''
    Step 2. Check if other children are delayed
    '''
    if refer_to:
        children = refer_to.get_children_refer()
        if children.exists():
            for k in children:
                if k.weight > 0 and kpi.id != k.id:
                    return False
            return True
        else:
            return False
    else:
        return False


class KPI(PermanentModel, CJSModel):
    OPERATOR_CHOICES = (
        ('>=', ">="),

        ('<=', "<="),
        ('=', "="),
    )

    ARCHIVEMENT_CALCULATION_METHOD_CHOICES = (
        ('topbottom', "Top-Bottom"),
    )

    KPI_REVIEW_TYPE = (
        ('', _('Quarterly')),
        ('daily', _('Daily')),
        ('weekly', _('Weekly')),
        ('monthly', _('Monthly')),
    )

    KPI_GROUP = (
        ('', ''),
        ('A', 'A'),
        ('B', 'B'),
        ('C', 'C'),
        ('O', 'O'),
        ('G', 'G'),
    )
    KPI_CALCULATION_TYPE = (

        ('', _('N/A')),
        ('sum', _('Sum')),
        ('average', _('Average')),
        ('most_recent', _('Latest month')),
    )

    KPI_APPROVAL_STATUS = (
        ('', _('Not Finished')),
        ('finished', _('Finished')),
        ('rejected', _('Rejected')),
        ('approved', _('Approved')),
    )

    name = models.CharField(verbose_name=_("KPI name"), max_length=2000, null=True, blank=True, db_index=True)  # <--
    code = models.CharField(verbose_name=_("Code"), max_length=100, null=True, blank=True, db_index=True)  # <--

    assigned_kpi = None

    description = models.TextField(verbose_name=_("Description"), null=True, blank=True)
    parent = models.ForeignKey('self', verbose_name=_("KPI parent"), null=True, blank=True)
    # user = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL)  # duan set models.SET_NULL
    # weight = models.IntegerField(verbose_name=_("Weighting"), default=10, blank=True)
    weight = models.FloatField(verbose_name=_("Weighting"), default=10, blank=True)

    # temp_weight = models.IntegerField(verbose_name=_("Temp Weight"), null=True, blank=True)
    temp_weight = models.FloatField(verbose_name=_("Temp Weight"), null=True, blank=True)
    in_library = models.BooleanField(default=False)
    quarter_period = models.ForeignKey(QuarterPeriod, blank=True, null=True, db_index=True)
    quarter = models.IntegerField(default=1)  # unuse
    year = models.IntegerField(default=0)
    current_goal = models.TextField(verbose_name=_("Measurement"), blank=True, default='')
    future_goal = models.TextField(verbose_name=_("Note"), blank=True, default='')  # measurement methods
    outcome_notes = models.TextField(blank=True, default='')
    outcome_notes_self = models.TextField(blank=True, default='')
    is_owner = models.BooleanField(default=False)  # is_owner = True se ko bi xoa khi change job title

    copy_from = models.ForeignKey('self', null=True, blank=True, related_name='kpi_copy_from',
                                  on_delete=models.SET_NULL)

    # job = models.ForeignKey(JobTitle, null=True, blank=True, on_delete=models.SET_NULL, related_name="kpis")
    data_input_approved = models.BooleanField(default=False)
    #  belong_to_jobs = models.ManyToManyField(JobTitle, related_name='kpi_list', blank=True)
    last_qc_check = models.DateTimeField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    operator = models.CharField(verbose_name=_("Operator"), max_length=10, blank=True, null=True,
                                choices=OPERATOR_CHOICES, default='>=')

    # field này là target trong phần đánh giá
    target = models.FloatField(verbose_name=_("Target"), blank=True, null=True,
                               default=0)  # TODO for performance review

    # field này là kết quả đánh giá thực tế, hiển thị trong phần đánh giá
    real = models.FloatField(verbose_name=_("Real"), blank=True, null=True)

    default_real = models.FloatField(verbose_name=_("Default Real Value"), blank=True, null=True)

    unit = models.CharField(verbose_name=_("Unit"), max_length=255, blank=True, null=True, db_index=True)

    last_email_sent = models.DateTimeField(blank=True, null=True)
    latest_score = models.FloatField(default=0, null=True, blank=True)

    unique_key = models.CharField(max_length=200, default='', null=True, blank=True, db_index=True)
    # TODO: check if copy unique key when clone kpi to the same user
    ordering = models.IntegerField(default=1, null=True, blank=True, db_index=True, verbose_name=_("Ordering"))
    ordered = models.IntegerField(default=1, blank=True, null=True)  # Only use for order KPIs view on strategy map
    is_started = models.NullBooleanField(default=False, null=True)

    cascaded_from = models.ForeignKey('self', null=True, blank=True, on_delete=models.SET_NULL,
                                      related_name='_cascaded_from')
    refer_to = models.ForeignKey('self', null=True, blank=True, on_delete=models.SET_NULL,
                                 related_name='refer_to_fpk')
    bsc_category = models.CharField(max_length=30, choices=BSC_CATEGORY, null=True,
                                    blank=True, verbose_name=_("BSC category"))
    status = models.CharField(max_length=30, choices=KPI_STATUS, default='not_started', null=True, blank=True)
    is_private = models.BooleanField(default=False)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(verbose_name=_("Deadline"), null=True, blank=True)
    real_end_date = models.DateField(null=True, blank=True)
    assigned_to = models.ForeignKey(User, verbose_name=_("Assigned to"), null=True, blank=True,
                                    on_delete=models.SET_NULL,
                                    related_name="assigned_to_relation")
    self_confirmed = models.BooleanField(default=False)
    manager_confirmed = models.BooleanField(default=False)

    tracker = CJSFieldTracker()
    # history = HistoricalRecords()

    current_result = models.FloatField(null=True, blank=True)

    # quarter_*_target là target dự kiến cho 4 quarter.
    # các giá trị này không phụ thuộc QuarterPeriod
    # các giá trị này là những giá trị mang tính chất tham khảo, mục tiêu chứ không dùng cho việc đánh giá
    # do đó, 4 quarter này không nhất thiết phải cùng 1 năm
    quarter_one_target = models.FloatField(blank=True, null=True, verbose_name=_("Quarter one target"))
    quarter_two_target = models.FloatField(blank=True, null=True, verbose_name=_("Quarter two target"))
    quarter_three_target = models.FloatField(blank=True, null=True, verbose_name=_("Quarter three target"))
    quarter_four_target = models.FloatField(blank=True, null=True, verbose_name=_("Quarter four target"))

    # year_target = models.FloatField(blank=True, null=True, verbose_name=_("Year target"))

    map = models.ForeignKey('StrategyMap', blank=True, null=True)  # unuse

    owner_email = models.EmailField(max_length=200, null=True, blank=True, db_index=True,
                                    verbose_name=_("KPI's responsible person"))

    # owner_user = models.ForeignKey(User, null=True, blank=True) # user == onwer_user or user == None

    reviewer = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL, related_name="reviewer_kpi")
    hash = models.CharField(max_length=200, null=True, blank=True)

    review_type = models.CharField(max_length=200, choices=KPI_REVIEW_TYPE, blank=True, default='')
    score_calculation_automation = models.NullBooleanField(default=False)
    score_calculation_type = models.CharField(max_length=100, choices=KPI_CALCULATION_TYPE, blank=True,
                                              #   default='average',
                                              default='most_recent', null=True,
                                              verbose_name=_("Score calculation method"))
    group = models.CharField(max_length=15, choices=KPI_GROUP, blank=True,
                             default='',
                             null=True)

    approval_status = models.CharField(max_length=350, choices=KPI_APPROVAL_STATUS, blank=True, default='')
    prefix_id = models.CharField(max_length=500, blank=True, null=True, db_index=True)

    month_1 = models.FloatField(verbose_name=_("Month 1"), blank=True, null=True)
    month_2 = models.FloatField(verbose_name=_("Month 2"), blank=True, null=True)
    month_3 = models.FloatField(verbose_name=_("Month 3"), blank=True, null=True)

    month_1_target = models.FloatField(verbose_name=_("Month 1 target"), blank=True, null=True)
    month_2_target = models.FloatField(verbose_name=_("Month 2 target"), blank=True, null=True)
    month_3_target = models.FloatField(verbose_name=_("Month 3 target"), blank=True, null=True)

    month_1_score = models.FloatField(verbose_name=_("month 1 score"), blank=True, null=True)
    month_2_score = models.FloatField(verbose_name=_("month 2 score"), blank=True, null=True)
    month_3_score = models.FloatField(verbose_name=_("month 3 score"), blank=True, null=True)

    archivable_score = models.IntegerField(null=True, blank=True)

    reviewer_email = models.CharField(max_length=150, null=True, blank=True)

    # count_children = models.IntegerField( null=True, blank=True)
    # count_refer = models.IntegerField( null=True, blank=True)
    refer_group_name = models.CharField(max_length=1000, null=True, blank=True)
    log_trace = models.TextField(verbose_name=_("log"), null=True, blank=True, default='')
    reason = models.TextField(verbose_name=_("reason"), null=True, blank=True, default='')

    # unique_code is code use for update score
    unique_code = models.CharField(max_length=100, null=True, blank=True, db_index=True,
                                   verbose_name=_("KPI Code"))

    group_kpi = models.ForeignKey('GroupKPI', null=True, blank=True, on_delete=models.SET_NULL)

    year_target = models.FloatField(verbose_name=_("Year target"), blank=True, null=True)
    year_result = models.FloatField(verbose_name=_("Year result"), blank=True, null=True)
    year_score = models.FloatField(verbose_name=_("Year score"), blank=True, null=True)

    year_data = JSONField(null=True, blank=True, default={})
    children_data = JSONField(null=True)
    '''
    #doc: https://cloudjet.atlassian.net/wiki/spaces/CKD/pages/124354579/New+KPI+Editor+v5+-+Alpha+Scope

    children_data = {
        "parent_score_auto": "on | off",
        # default là = Organization.parent_score_auto cho v5,
        # "on" --> Tính theo thành tích ,
        # "off" --> Không tính theo thành tích


        children_weights: [
            {   "kpi_id" : ...,
                "weight": ... ,
                "relation_type": "none | cause-effect | sum"
                # default: "none" --> không phân loại | "cause-effect" --> nhân quả | "sum" --> Tổng kết quả ### Không cần ràng buộc với Organization.relation_definition, khi cần user tư tra cứu ở Organization.relation_definition
            },

            {   "kpi_id" : ...,
                "weight": ... ,
                "relation_type": "none | cause-effect | sum"
            },

            ....
            ]

    }
    '''

    achievement_calculation_method = models.CharField(max_length=100, choices=ARCHIVEMENT_CALCULATION_METHOD_CHOICES,
                                                      null=True, blank=True, default=None)
    '''
    {
        month_1:{
            top:'',
            bottom:'',
        },
        month_2:{
            top:'',
            bottom:'',
        },
        month_3:{
            top:'',
            bottom:'',
        },
        quarter:{
            top:'',
            bottom:'',
        },

    }

    '''
    achievement_calculation_method_extra = JSONField(null=True, blank=True)

    history = AuditlogHistoryField()
    actor = None

    # http://stackoverflow.com/questions/28290998/return-a-default-value-for-empty-field-on-a-django-model
    # def __getattribute__(self, name):
    #     attr = models.Model.__getattribute__(self, name)
    #     # if name == 'month_3':# and not attr:
    #     #     return 20
    #     return attr

    def __repr__(self):  # for memoize to work
        return "%s(%s)" % (self.__class__.__name__, self.id)

    def get_parent(self):

        parent = None
        try:
            parent = self.parent
        except KPI.DoesNotExist:  # pragma: no cover
            pass

        return parent

    def get_refer_to(self):

        refer_to = None
        try:
            refer_to = self.refer_to
        except KPI.DoesNotExist:  # pragma: no cover
            pass

        return refer_to

    def get_kpi_refer_group(self):

        r = slugify(self.group_name())
        # print "get_kpi_refer_group {}".format(r)
        return r

    #
    # def get_kpi_refer_group_old(self):
    #     if self.refer_to_id > 0:
    #         return self.refer_to_id
    #     if self.get_group():
    #         return "group{}".format(self.get_group().id)
    #     return self.id

    # TODO: call setting-group in strategymap instead of in KPI model --> need codereview
    # for any attempt to load kpi data, if we do some modifying kpi data, we may be throwed exceptions,
    # so, the kpi cannot be load.
    # Therefore, if we can move the modifying kpi data to other actions rather than load/get, we can avoid the problem to which we cannot load kpi
    def get_group(self):

        if self.group_kpi_id:
            user_kpi = self.user
            user_map = self.group_kpi.map.get_director() if self.group_kpi.map else None
            '''
            in case: user assign a KPI to another user, then change owner email of KPI to another one, then move group to kpi's user strategy map
            '''
            if user_kpi != user_map:  # signal: when user_kpi different from user_map, then we start migrate. we need to compare begin from user_kpi, not user_map
                from performance.services.strategymap import get_map
                organization = self.user.profile.get_organization()
                quarter_period = self.quarter_period
                group = self.group_kpi
                if user_kpi == organization.ceo:  # user_kpi is ceo, move the kpi to the company strategy map
                    company_map, use_hashurl_type = get_map(organization=organization, current_quarter=quarter_period)
                    group.map = company_map
                    group.save()
                else:  # user is not ceo, move group into user's map
                    map_user, use_hashurl_type = get_map(organization=organization, current_quarter=quarter_period, user_id=user_kpi.id)
                    group.map = map_user
                    group.save()

            return self.group_kpi
        else:

            '''
            Start migration from many-to-many to 1-many and delete old data
            '''

            key = 'get_group{}'.format(self.id)
            # if can not get cache -> None != -1 -> False
            # if cache.get(key) = None ( cache exist but value = None) -> None != -1 -> True
            if cache.get(key) is not None:
                return cache.get(key)

            group = self.groupkpi_set.all().first()

            # cache.set(key, group, 60 * 5)  ===>  not catche here

            if not group:
                #
                # if self.refer_to:
                #     return self.refer_to.get_group()
                '''
                    Every KPI from now will have group_kpi, which is Goal as described in Cloudjet KPI v5
                    So, if KPI does not have group, then create group for it .
                '''
                from performance.db_models.strategy_map import GroupKPI, StrategyMap

                group = GroupKPI()
                if self.name is not None:
                    # data error is returned for kpi.name>1000 so no cover needed
                    name_max_length = GroupKPI._meta.get_field('name').max_length
                    if len(self.name) > name_max_length:  # pragma: no cover
                        group.name = truncatechars(self.name, (name_max_length - 1))
                    else:
                        group.name = self.name
                else:
                    group.name = self.name

                # if self.user.profile.is_ceo():
                #     group.name = ''

                group.category = self.bsc_category

                from performance.services.strategymap import get_user_current_map
                user_map = get_user_current_map(self.user)

                # If user is CEO, get_user_current_map will get map of user. map.id of CEO user different with map.id of organization
                ceo_map = StrategyMap.objects.filter(organization=self.user.profile.get_organization(),
                                                                  quarter_period=self.user.profile.get_organization().get_current_quarter(),
                                                                  director__isnull=True).first()
                # we only save group if map is not None
                # map == None ==> error raised
                if self.user.profile.is_ceo():
                    if ceo_map is not None:
                        group.map = ceo_map
                        group.save()
                else:
                    if user_map is not None:
                        group.map = user_map
                        group.save()

            else:  # pragma: no cover

                '''
                Clean old data
                '''

                self.groupkpi_set.remove(group)

            # prevent error: ValueError:  FIX GROUP_KPI.MAP is NONE) prohibited to prevent data loss due to unsaved related object 'group_kpi'.
            if group and group.id:
                self.group_kpi = group
                self.save(update_fields=['group_kpi', ])
        # cache.set(key, group, 60 * 5)
        return self.group_kpi

    # @property
    def group_name(self):
        # return 333
        # if (get_group_name(self)):
        return get_group_name(self)
        # return self.name

    def month_1_score_rounded(self):
        if self.month_1_score:
            return round(self.month_1_score, 2)
        else:
            return 0

    def month_2_score_rounded(self):
        if self.month_2_score:
            return round(self.month_2_score, 2)
        else:
            return 0

    def month_3_score_rounded(self):
        if self.month_3_score:
            return round(self.month_3_score, 2)
        else:
            return 0

    def latest_score_rounded(self):
        if self.latest_score:
            return round(self.latest_score, 2)
        else:
            return 0

    def get_latest_score(self):
        pass

    def get_month_1_score_icon(self):
        score = self.get_month_1_score()
        if score >= 80:
            return settings.STATIC_URL + "img/kpi/good-performance.png"
        elif score >= 50:
            return settings.STATIC_URL + "img/kpi/avg-performance.png"

        else:
            return settings.STATIC_URL + "img/kpi/bad-performance.png"

    def get_month_2_score_icon(self):
        score = self.get_month_2_score()
        if score >= 80:
            return settings.STATIC_URL + "img/kpi/good-performance.png"
        elif score >= 50:
            return settings.STATIC_URL + "img/kpi/avg-performance.png"

        else:
            return settings.STATIC_URL + "img/kpi/bad-performance.png"

    def get_month_3_score_icon(self):
        score = self.get_month_3_score()
        if score >= 80:
            return settings.STATIC_URL + "img/kpi/good-performance.png"
        elif score >= 50:
            return settings.STATIC_URL + "img/kpi/avg-performance.png"

        else:
            return settings.STATIC_URL + "img/kpi/bad-performance.png"

    def get_score_icon(self):
        score = self.get_score()
        if score >= 80:
            return settings.STATIC_URL + "img/kpi/good-performance.png"
        elif score >= 50:
            return settings.STATIC_URL + "img/kpi/avg-performance.png"

        else:
            return settings.STATIC_URL + "img/kpi/bad-performance.png"

    def get_month_1_score(self):

        assigned_kpi = self.get_assigned_kpi()

        '''
            assigned_kpi.assigned_to_id != self.user_id and assigned_kpi.assigned_to_id != assigned_kpi.user_id:
            This is to avoid infinity recursive
            '''

        if assigned_kpi and assigned_kpi.assigned_to_id != self.user_id and assigned_kpi.assigned_to_id != assigned_kpi.user_id:
            return assigned_kpi.month_1_score  # fix loop assigned_kpi.get_month_1_score()

        return self.month_1_score if self.month_1_score is not None else None

    def set_month_1_score(self, save=True):

        children = list(self.get_children())

        from performance.services.children_data_services import get_children_data
        children_data = get_children_data(self.user, self)

        if len(children) == 0 or children_data['parent_score_auto'] == False:

            kpi = calculate_score(self)
            # score = kpi_score_calculator(self.operator, self.target, self.real, self, month=1)
            if kpi.month_1_score != self.month_1_score:  # pragma: no cover
                self.month_1_score = kpi.month_1_score

          #  logger.info('performance/models.py:  line 995')

        else:

            total_weight = reduce(lambda x, y: x + y, [i['weight'] for i in children_data['children_weights']])
            # logger.info('performance/models.py:  line 1000')

            if total_weight > 0:
                total_score = None
                for i in children_data['children_weights']:
                    ki = KPI.objects.filter(id=i['kpi_id']).first()

                    if ki and ki.get_month_1_score() is not None:
                        total_score = (total_score or 0) + ki.get_month_1_score() * i['weight']

                self.month_1_score = total_score / total_weight if total_score is not None else None

               # logger.info('performance/models.py:  line 1005')

       # logger.info('performance/models.py:  line 1004')
      #  logger.info(save)
        if save: self.save(update_fields=["month_1_score"])
        return self.month_1_score if self.month_1_score is not None else None

    def get_month_2_score(self):

        assigned_kpi = self.get_assigned_kpi()

        '''
            assigned_kpi.assigned_to_id != self.user_id and assigned_kpi.assigned_to_id != assigned_kpi.user_id:
            This is to avoid infinity recursive
            '''

        if assigned_kpi and assigned_kpi.assigned_to_id != self.user_id and assigned_kpi.assigned_to_id != assigned_kpi.user_id:
            return assigned_kpi.month_2_score  # fix loop assigned_kpi.get_month_2_score()

        return self.month_2_score

    def set_month_2_score(self, save=True):

        children = list(self.get_children())

        from performance.services.children_data_services import get_children_data
        children_data = get_children_data(self.user, self)

        if len(children) == 0 or children_data['parent_score_auto'] == False:

            kpi = calculate_score(self)
            # score = kpi_score_calculator(self.operator, self.target, self.real, self, month=1)
            #     print "kpi.month_2_score: {}".format(kpi.month_2_score)
            if kpi.month_2_score != self.month_2_score:  # pragma: no cover
                self.month_2_score = kpi.month_2_score

        else:

            total_weight = reduce(lambda x, y: x + y, [i['weight'] for i in children_data['children_weights']])
       #     logger.info('performance/models.py:  line 1000')

            if total_weight > 0:
                total_score = None
                for i in children_data['children_weights']:
                    ki = KPI.objects.filter(id=i['kpi_id']).first()
                    if ki and ki.get_month_2_score() is not None:
                        total_score = (total_score or 0) + ki.get_month_2_score() * i['weight']

                self.month_2_score = total_score / total_weight if total_score is not None else None

        if save: self.save(update_fields=["month_2_score"])
        return self.month_2_score

    def get_month_3_score(self):

        assigned_kpi = self.get_assigned_kpi()

        '''
            assigned_kpi.assigned_to_id != self.user_id and assigned_kpi.assigned_to_id != assigned_kpi.user_id:
            This is to avoid infinity recursive
            '''

        if assigned_kpi and assigned_kpi.assigned_to_id != self.user_id and assigned_kpi.assigned_to_id != assigned_kpi.user_id:
            return assigned_kpi.month_3_score  # fix loop assigned_kpi.get_month_3_score()

        return self.month_3_score

    def set_month_3_score(self, save=True):

        children = list(self.get_children())

        from performance.services.children_data_services import get_children_data
        children_data = get_children_data(self.user, self)

        if len(children) == 0 or children_data['parent_score_auto'] == False:

            kpi = calculate_score(self)
            # score = kpi_score_calculator(self.operator, self.target, self.real, self, month=1)
            if kpi.month_3_score != self.month_3_score:  # pragma: no cover
                self.month_3_score = kpi.month_3_score

        else:
            total_weight = reduce(lambda x, y: x + y, [i['weight'] for i in children_data['children_weights']])
       #     logger.info('performance/models.py:  line 1000')

            if total_weight > 0:
                total_score = None
                for i in children_data['children_weights']:
                    ki = KPI.objects.filter(id=i['kpi_id']).first()
                    if ki and ki.get_month_3_score() is not None:
                        total_score = (total_score or 0) + ki.get_month_3_score() * i['weight']

                self.month_3_score = total_score / total_weight if total_score is not None else None
        #                 self.month_3_score = reduce(lambda x, y: x + y,
        #                                             [(i.get_month_3_score() * i.weight if i.get_month_3_score() != None else 0)
        #                                              for i in
        #                                              children]) / total_weight

        if save: self.save(update_fields=["month_3_score"])
        return self.month_3_score

    #
    # def get_month_2_score(self):
    #     score = kpi_score_calculator(self.operator, self.target, self.real, self, month=2)
    #     if score != self.month_2_score:
    #         self.month_2_score = score
    #         self.save(update_fields=["month_2_score"])
    #     return score
    #
    # def get_month_3_score(self):
    #     score = kpi_score_calculator(self.operator, self.target, self.real, self, month=3)
    #     if score != self.month_3_score:
    #         self.month_3_score = score
    #         self.save(update_fields=["month_3_score"])
    #     return score

    def get_weighted_weight(self):

        parent = None
        try:
            parent = self.parent
        except KPI.DoesNotExist:  # pragma: no cover
            pass

        def calculate_total_weight(kpis):

            return reduce(lambda x, y: x + y, [i.weight for i in kpis], 0)

            # this is longer, not good
            # total_weight = 0
            # for parent in all_parents:
            #     try:
            #         total_weight += parent.weight
            #     except:
            #         pass
            #
            # return total_weight

        if parent:

            all_children = parent.get_children()
            total_weight = calculate_total_weight(all_children)
            if total_weight == 0:
                return 0

            return round(parent.get_weighted_weight() / 100 * (self.weight * 100 / total_weight), 2)

        else:

            all_parents = self.user.profile.get_kpis_parent(ordered=False)
            total_weight = calculate_total_weight(all_parents)
            if total_weight == 0:
                return 0

            return round(self.weight * 100 / total_weight, 2)

    def get_month_1_target(self):
        # ở đây vì month_1_target luôn luôn bằng None khi khởi tạo nên sẽ luôn luôn vào if nên
        # target bằng nhiêu thì month_1_target sẽ bằng bấy nhiêu.
        if self.month_1_target is None and self.target is not None:
            if self.review_type == "quarterly" or self.review_type == "":
                if self.month_1_target != self.target:
                    self.month_1_target = self.target
                    if self.id: self.save(update_fields=["month_1_target"])
                    # self.save()
            else:  # monthly

                if self.score_calculation_type == "sum":

                    if self.target and self.month_1_target != self.target * (1.0 / 3.0):
                        self.month_1_target = self.target * (1.0 / 3.0)
                        if self.id: self.save(update_fields=["month_1_target"])
                        #     self.save()

                else:

                    if self.month_1_target != self.target:
                        self.month_1_target = fix_input_float(self.target)
                        if self.id: self.save(update_fields=["month_1_target"])
                        #    self.save()

                        # self.save()
        return self.month_1_target

    def get_month_2_target(self):
        # if self.target is None:
        #   self.target = 0
        # if self.target = 0 then  => self month_2_target = 0 and self.month_3_target = 0
        # can delete self.target = 0
        if self.month_2_target is None and self.target is not None:

            if self.review_type == "quarterly" or self.review_type == "":
                if self.month_2_target != self.target:
                    self.month_2_target = self.target
                    if self.id: self.save(update_fields=["month_2_target"])
                    #   self.save()
            else:  # monthly

                if self.score_calculation_type == "sum":

                    if self.target and self.month_2_target != self.target * (1.0 / 3.0):
                        self.month_2_target = self.target * (1.0 / 3.0)
                        if self.id: self.save(update_fields=["month_2_target"])
                        #      self.save()

                else:

                    if self.month_2_target != self.target:
                        self.month_2_target = self.target
                        if self.id: self.save(update_fields=["month_2_target"])

                        # self.save()

        return self.month_2_target

    def get_month_3_target(self):
        if self.month_3_target is None and self.target is not None:

            if self.score_calculation_type == "sum":

                if self.target and self.month_3_target != self.target * (1.0 / 3.0):
                    self.month_3_target = self.target * (1.0 / 3.0)
                    if self.id: self.save(update_fields=["month_3_target"])
                    #      self.save()

            else:

                if self.month_3_target != self.target:
                    self.month_3_target = self.target
                    if self.id: self.save(update_fields=["month_3_target"])

                    # self.save()

        return self.month_3_target
        #
        # if self.month_3_target is None:
        #
        #     if self.month_3_target != self.target:
        #         self.month_3_target = self.target
        #         if self.id: self.save(update_fields=["month_3_target"])
        #         # self.save()
        #
        # return self.month_3_target

    def get_kpi_lock(self):
        k, created = KPILock.objects.get_or_create(kpi=self)

        return k

    # @postpone
    def fix_count_children_refer(self):
        children = KPI.objects.filter(parent_id=self.id)
        refer = KPI.objects.filter(refer_to_id=self.id)

        count_children = children.count()
        count_refer = refer.count()

        is_error = False

        if count_children != count_refer:

            for bug in children:
                bug.fix_self(add_queue=False)
                continue
            #
            # for bug in refer:
            #     bug.fix_self()

            is_error = True

        return is_error

        # this causing error
        #     if bug.refer_to is None and KPI.objects.filter(cascaded_from_id = bug.id).count() == 0:
        #         # remove_kpi(bug)
        #         bug.delete()
        #
        #
        # pass

    # @classmethod
    # def get_leap_kpis(cls, user_ids, quarter_period_id, category=None):
    #     '''
    #     # select kpi lá
    #     '''
    #     args = []
    #     kwargs = {}
    #     if category is not None:
    #         if category == 'other':
    #             excluded_categories = ['financial', 'customer', 'internal', 'learninggrowth']
    #             args.append(~Q(bsc_category__in=excluded_categories))
    #         else:
    #             args.append(Q(bsc_category=category))
    #
    #     leap_kpis = cls.objects.filter(*args,
    #                                    kpi__parent=None,
    #                                    # kpi nay khong co kpi A nao ma A.parent = this    (khong co con)
    #                                    _cascaded_from__cascaded_from=None,
    #                                    # kpi nay khong co kpi A nao ma A.cascaded_from = this (khong la trung gian)
    #                                    user_id__in=user_ids,
    #                                    quarter_period_id=quarter_period_id)
    #
    #     return leap_kpis
    #
    # @classmethod
    # def get_independent_kpis(cls, user_ids, quarter_period_id, category=None):
    #     '''
    #     # select tat cac  KPI doc lap (khong co con, khong co cha, khong co trung gian)
    #     #https://docs.djangoproject.com/en/1.9/ref/models/fields/#django.db.models.ForeignKey.related_name
    #     '''
    #     args = []
    #     kwargs = {}
    #
    #     if category is not None:
    #         if category == 'other':
    #             excluded_categories = ['financial', 'customer', 'internal', 'learninggrowth']
    #             args.append(~Q(bsc_category__in=excluded_categories))
    #         else:
    #             args.append(Q(bsc_category=category))
    #
    #     independent_kpis = cls.objects.filter(
    #         *args,
    #         kpi__parent=None,
    #         user_id__in=user_ids,
    #         parent=None,
    #         cascaded_from=None,
    #         quarter_period_id=quarter_period_id)
    #     return independent_kpis

    # @classmethod
    # def get_indirect_parent_has_child__kpis(cls, user_ids, quarter_period_id, category=None):
    #     '''
    #     # select cha gián tiếp (mà có kpi con)
    #     '''
    #     args = []
    #     kwargs = {}
    #     if category is not None:
    #         if category == 'other':
    #             excluded_categories = ['financial', 'customer', 'internal', 'learninggrowth']
    #             args.append(~Q(bsc_category__in=excluded_categories))
    #         else:
    #             args.append(Q(bsc_category=category))
    #
    #     indirect__parent_has_child__kpi_ids = list(KPI.objects.filter(
    #         *args,
    #         kpi__parent__isnull=False,  # có con
    #         user_id__in=user_ids,
    #         # parent=None,
    #         refer_to__isnull=False,
    #         cascaded_from__isnull=False,  # cha gián tiếp or trực tiếp
    #         quarter_period_id=quarter_period_id).values_list('id',
    #                                                          flat=True).distinct())  # why distinct --> view _documents/KPI/kpi-bsc-report.md
    #     indirect__parent_has_child__kpis = KPI.objects.filter(id__in=indirect__parent_has_child__kpi_ids)
    #
    #     return indirect__parent_has_child__kpis

    # @classmethod
    # def get_indirect_child__kpis(cls, user_ids, quarter_period_id, category=None):
    #     '''
    #     # select con gián tiếp or kpi duoc phan cong (co the co con hoac khong co con)
    #     '''
    #     args = []
    #     kwargs = {}
    #     if category is not None:
    #         if category == 'other':
    #             excluded_categories = ['financial', 'customer', 'internal', 'learninggrowth']
    #             args.append(~Q(bsc_category__in=excluded_categories))
    #         else:
    #             args.append(Q(bsc_category=category))
    #
    #     indirect_child__kpi_ids = list(KPI.objects.filter(
    #         *args,
    #         user_id__in=user_ids,
    #         parent=None,
    #         refer_to__isnull=False,  # maybe not need
    #         cascaded_from__isnull=False,
    #         quarter_period_id=quarter_period_id).values_list('id', flat=True).distinct())
    #
    #     indirect_child__kpis = KPI.objects.filter(id__in=indirect_child__kpi_ids)
    #     return indirect_child__kpis

    # @classmethod
    # def get_assigned_kpis(cls, user_ids, quarter_period_id, category=None):
    #     '''
    #     the same as get_indirect_child__kpis
    #     get kpi duoc phan cong
    #     '''
    #     return cls.get_indirect_child__kpis(user_ids, quarter_period_id, category)
    #
    #     # args = []
    #     # kwargs = {}
    #     # if category is not None:
    #     #     if category == 'other':
    #     #         excluded_categories = ['financial', 'customer', 'internal', 'learninggrowth']
    #     #         args.append(~Q(bsc_category__in=excluded_categories))
    #     #     else:
    #     #         args.append(Q(bsc_category=category))
    #     #
    #     # assigned_kpis = cls.objects.filter(*args,
    #     #                                    user_id__in=user_ids,
    #     #                                    parent__isnull=True,
    #     #                                    cascaded_from__isnull=False,  # kpi trung gian
    #     #                                    quarter_period_id=quarter_period_id)
    #     #
    #     # return assigned_kpis

    @classmethod
    def get_parent_kpis_by_category(cls, user_id, quarter_period_id, category, only_id=True, use_cache=False):

        return get_parent_kpis_by_category(user_id, quarter_period_id, category, only_id=True, use_cache=False)

    @classmethod
    def _get_line_up(cls, kpi, kpis_list_break):
        k = kpi
        line = [k, ]
        line_break = False
        break_point = None

        while k.get_parent() or k.get_refer_to():
            k = k.get_parent() or k.get_refer_to()
            if k in kpis_list_break:
                line_break = True
                break_point = k
                break

            line.append(k)

        # return [line, line_break, break_point]
        return {'line': line, 'line_break': line_break, 'break_point': break_point}

    def fix_no_owner_email(self):
        if not self.owner_email and self.user:
            self.owner_email = self.user.email
            self.save(update_fields=["owner_email"])

    @classmethod
    def get_root_kpi_by_search_terms(cls, search_terms, user_id, quarter_period_id, category):

        if not search_terms:
            return {}

        args = []
        kwargs = {}
        if category == 'other':
            excluded_categories = ['financial', 'customer', 'internal', 'learninggrowth']
            args.append(~Q(bsc_category__in=excluded_categories))
        else:
            args.append(Q(bsc_category=category))

        root = None
        # get first kpi that it's name contain search_terms

        # icontains --> case-insensitive
        # EXCLUDE: cascaded_from not null, parent not null

        kpis = cls.objects.filter(*args,
                                  assigned_to__isnull=True,  # khong tinh nhung kpi trung gian
                                  quarter_period_id=quarter_period_id)

        if search_terms.find('reviewer_email:') >= 0:
            kpis = kpis.filter(reviewer_email=search_terms.replace('reviewer_email:', ''))
        elif search_terms.find('id:') >= 0:
            id = search_terms.replace('id:', '')
            print "line 1367: {}".format(id)
            kpis = kpis.filter(id=id)

        elif search_terms.find('email:') >= 0:
            q = search_terms.split('email:')[0][
                :100].strip()  # only search first 100 character to avoid error in SQL Server
            email = search_terms.split('email:')[-1].strip()
            kpis = kpis.filter(Q(name__icontains=q) | Q(owner_email__icontains=email))
        else:
            q = search_terms.split('email:')[0][
                :100].strip()  # only search first 100 character to avoid error in SQL Server
            kpis = kpis.filter(name__icontains=q)

        # if not cast with list(...), --> Unable to get repr for <class 'django.db.models.query.FallbackValuesListQuerySet'>
        # ref: https://code.djangoproject.com/ticket/20408
        kpi_ids = list(kpis.values_list('id', flat=True))

        kpis = kpis.order_by('-id')[
               :50]  # max 50 kpis only TODO: #get new IDs first, larger IDs means child if it's cascaded #not sure, boi vi co the KPI cha duoc tao sau va lien ket kpi con len
        kpis_list = list(kpis)
        if kpi_ids:
            for kpi in kpis:

                kpi.fix_no_owner_email()

                if kpi.refer_to_id in kpi_ids \
                        or kpi.parent_id in kpi_ids \
                        or (kpi.cascaded_from_id > 0 and kpi.cascaded_from.parent_id in kpi_ids):
                    # check cascaded kpi

                    # REMOVE kpi that have parent, or refer_to, or cascaded_from already in the list

                    kpis_list.remove(kpi)

                elif kpi.refer_to_id > 0:
                    refer_to = None
                    try:
                        refer_to = kpi.refer_to
                    except KPI.DoesNotExist:  # pragma: no cover
                        pass

                    if refer_to:

                        if refer_to.refer_to_id in kpi_ids:
                            kpis_list.remove(kpi)
                        elif refer_to.cascaded_from_id > 0:
                            try:
                                if kpi.refer_to.cascaded_from.parent_id in kpi_ids:
                                    kpis_list.remove(kpi)
                            except:  # pragma: no cover
                                pass

                                # no need

        # NOW kpi_list is list of root kpis
        # we need check every root in list whether is a actual root?
        if len(kpis_list) < 30:
            kpis_list_tmp = list(kpis_list)  # need re-cast to return new list, not reference to kpis_list
            kpis_pool = []  # store all kpis in lines
            # we assumpt max-level kpi <=10 ==> max-cost = 10 * 30 = 300 lan query xuong database
            for kpi in kpis_list_tmp:
                if kpi in kpis_pool:
                    # remove this KPI from kpis_list
                    kpis_list.remove(kpi)
                else:
                    # lineup=cls._get_line_up(kpi, kpis_list) #{'line':line, 'line_break':line_break, 'break_point':break_point}
                    lineup = cls._get_line_up(kpi,
                                              kpis_pool)  # {'line':line, 'line_break':line_break, 'break_point':break_point}
                    if lineup['line_break'] is True:  # mean kpi is subordinate of other
                        kpis_pool = kpis_pool + lineup['line']
                        # remove this KPI from kpis_list
                        kpis_list.remove(kpi)
                        pass
                    else:
                        pass

        kpis_list = sorted(kpis_list, key=lambda k: k.id)
        return kpis_list

    def get_comment_count(self):
        pass

    def name_mindmap(self):
        try:

            if self.assigned_to_id:
                return self.name + u" [" + self.assigned_to.username + u"]"
            elif self.owner_email and self.refer_to_id and self.refer_to.user_id != self.user_id and self.user_id and self.user.username:
                return unicode(self.name) + u" [" + self.user.username + u"]"
        except KPI.DoesNotExist:  # pragma: no cover
            return unicode(self.name)

        return self.name

    def get_assigned_kpi(self):

        assigned_kpi = None

        # only call first time or cache is deleted
        if self.assigned_kpi is None or settings.TESTING \
                or not cache.get('get_assigned_kpi_flags_{}'.format(self.id), None):

            assigned_kpi = get_assigned_kpi(self)

            cache.set('get_assigned_kpi_flags_{}'.format(self.id), True, 60)

            if assigned_kpi is None:
                self.assigned_kpi = 0
            else:
                self.assigned_kpi = assigned_kpi

        if self.assigned_kpi:
            assigned_kpi = self.assigned_kpi

        return assigned_kpi

    def kpi_type_v2(self):

        # logger.info("kpi_type_v2 {}".format(self.id))
        if not self.parent_id:
            # KPI Cha --> self.parent_id = None --> not self.parent_id = True
            if self.cascaded_from_id or self.refer_to_id:
                return 'kpi_cha_duoc_phan_cong'
            else:
                return 'kpi_cha_normal'

        else:
            # KPI Cha --> self.parent_id = None --> not self.parent_id = True
            assigned_kpi = self.get_assigned_kpi()
            if assigned_kpi:
                # KPI Con, được phân công --> KPI Trung gian
                return 'kpi_trung_gian'
            else:
                return 'kpi_con_normal'

    def kpi_type(self):

        try:
            cascaded_from = self.cascaded_from
        except:  # pragma: no cover
            cascaded_from = None

        if (self.assigned_to_id and self.parent_id) and not (self.cascaded_from_id or self.refer_to_id):
            return 'trunggian'  # u'1: Trung gian'
        elif not (self.assigned_to_id or self.parent_id):
            if not (self.cascaded_from_id or self.refer_to_id):
                return 'chatructiep'  # u'2.1: cha trực tiếp'
            elif (cascaded_from and self.refer_to_id) and cascaded_from.parent_id == self.refer_to_id:
                return 'chacongiantiep'  # u'2.2 or 3.2: cha gián tiếp or con gián tiếp'
        elif not (self.assigned_to_id or self.cascaded_from_id) and (
                self.parent_id and self.refer_to_id) and self.refer_to_id == self.parent_id:
            return 'contructiep'  # '3.1: con gián tiếp'
        else:  # pragma: no cover
            return 'unknown'

        return 'unknown'

    def kpi_type_error(self):
        error = {'is_error': True,
                 'best_matched': '',
                 'suggest': ''}
        if (self.assigned_to and self.parent):
            if not (self.cascaded_from or self.refer_to):
                error['is_error'] = False
                # return 'trunggian' #u'1: Trung gian'
            else:
                print 'Best match: Trung gian'
                error['best_matched'] = 'trunggian'

                if self.cascaded_from_id > 0:
                    error['suggest'] = 'cascaded_from should be None'
                else:
                    error['suggest'] = 'refer_to should be None'

            return error
        elif not (self.assigned_to or self.parent):
            if not (self.cascaded_from or self.refer_to):
                error['is_error'] = False
                # return 'chatructiep' #u'2.1: cha trực tiếp'
            else:
                if (self.cascaded_from and self.refer_to):
                    if self.cascaded_from.parent == self.refer_to:
                        error['is_error'] = False
                        # return 'chacongiantiep'  # u'2.2 or 3.2: cha gián tiếp or con gián tiếp'
                    else:
                        error['best_matched'] = 'chacongiantiep'
                        error['suggest'] = "KPI shoulded be: {}".format("self.cascaded_from.parent == self.refer_to",)
                else:
                    if self.cascaded_from_id > 0:
                        error['best_matched'] = 'chacongiantiep'
                        error['suggest'] = 'refer_to should be Not None'
                    else:
                        error['best_matched'] = 'chatructiep'
                        error['suggest'] = 'refer_to should be None'
            return error
        elif not (self.assigned_to or self.cascaded_from):
            if (self.parent and self.refer_to):
                if self.refer_to == self.parent:
                    error['is_error'] = False
                    # return 'contructiep' #'3.1: con gián tiếp'
                else:
                    error['best_matched'] = 'contructiep'
                    error['suggest'] = 'KPI should be {}'.format('self.refer_to==self.parent',)
            else:
                if self.parent_id:
                    error['best_matched'] = 'contructiep'
                    error['suggest'] = 'refer_to should be Not None'
                else:
                    error['best_matched'] = 'contructiep'
                    error['suggest'] = 'parent should be Not None'
            return error
        # not reachable
        else:  # pragma: no cover
            error['best_matched'] = 'unkown'
            return error
            # return 'unknown'

        # return 'unknown'
        # following line is not reachable
        error['best_matched'] = 'unkown'  # pragma: no cover

        return error  # pragma: no cover

    def delay_toggle(self, user=None, start=True,
                     recursive=True, force_to_state=None,
                     up_recursive=True,
                     down_recursive=True,
                     only_up_recursive=False,
                     ignore_perm_check=False,
                     reason=''):

        ignore_perm = ignore_perm_check
        #####
        # Step 0.0: Make sure we have all variables we need
        #####

        if user is None:
            user = self.user
        actor = user

        if not user.profile:  # pragma: no cover
            return self

        current_latest_score = self.latest_score

        #####
        # Step 0.1: Utility functions to be used
        #####
        def toggle_self(_force_to_state):
            '''
            This function will toggle delay for current KPI and return `to_state`
            '''
            _refer_to = None
            if self.refer_to_id:
                try:
                    _refer_to = KPI.objects.get(id=self.refer_to_id)  # get to avoid caching
                except ObjectDoesNotExist:  # pragma: no cover
                    pass

            last_weight = self.weight
            if _force_to_state == 'delay':
                _to_state = 'delay'
            elif _force_to_state == 'active':
                _to_state = 'active'
            else:
                if self.weight > 0:
                    _to_state = 'delay'
                    self.reason = reason
                else:
                    _to_state = 'active'
                    self.reason = ''

            if _to_state == 'delay':
                if self.weight > 0:  # dang active
                    self.temp_weight = self.weight
                    self.weight = 0
                    self.reason = reason

                    '''
                    # Start  update refer_to KPI children_weight
                    '''
                    if _refer_to:

                        from performance.services.children_data_services import get_children_data, set_children_data
                        children_data = get_children_data(user, _refer_to)

                        def delay_children_weights(weight_elm):
                            if weight_elm['kpi_id'] == self.id:
                                weight_elm['weight_temp'] = weight_elm['weight']
                                weight_elm['weight'] = 0

                            return weight_elm

                        children_data['children_weights'] = map(delay_children_weights, children_data['children_weights'])
                        set_children_data(user, _refer_to, children_data)

                    '''
                    # End  update refer_to KPI children_weight
                    '''

            else:  # to_state=='active'
                if is_parent_delay():
                    raise PermissionDenied(ugettext("KPI parent is delayed. You must active KPI parent first"))

                if self.weight == 0:  # dang delay
                    if not self.temp_weight:
                        self.temp_weight = 10
                    self.weight = self.temp_weight or 10  # Check if temp_weight is zero set default 10
                    self.temp_weight = 0
                    self.reason = ""

                    '''
                    # Start  update refer_to KPI children_weight
                    '''
                    if _refer_to:
                        from performance.services.children_data_services import get_children_data, set_children_data
                        children_data = get_children_data(_refer_to.user, _refer_to)

                        def activate_children_weights(weight_elm):
                            if weight_elm['kpi_id'] == self.id:
                                temp_weight = 10

                                try:
                                    temp_weight = weight_elm['weight_temp']
                                except:
                                    pass

                                if temp_weight is None:
                                    temp_weight = 10

                                weight_elm['weight'] = temp_weight

                            return weight_elm

                        children_data['children_weights'] = map(activate_children_weights,
                                                                children_data['children_weights'])
                        set_children_data(user, _refer_to, children_data)

                    '''
                    # End  update refer_to KPI children_weight
                    '''

                else:  # dang active --> do nothing
                    pass

            #  self.save(update_fields=['reason', ])

            self.save()

            # This to make sure after toggle , we recalculate score

            from performance.services import add_new_cascadeKPIQueue
            add_new_cascadeKPIQueue(self)

            if last_weight != self.weight:
                from performance.services.kpi import add_auto_comment
                DELAY = ugettext('Deactivated KPI')
                ACTIVE = ugettext('Activated KPI')
                Reason = ugettext('Reason')
                if _to_state == 'delay':
                    to_state = DELAY
                    content = u'{} {} \n- {}: {}'.format(to_state, self.name, Reason, self.reason)
                else:
                    to_state = ACTIVE
                    content = u'{} {}'.format(to_state, self.name)
                add_auto_comment(actor, self, content=content)

            self.save()

            # This to make sure after toggle , we recalculate score

            # from performance.services import add_new_cascadeKPIQueue
            # add_new_cascadeKPIQueue(self)

            return _to_state

        def toggle_children(_to_state):

            if recursive is False:  # this condition is extremely for recursion programming
                return

            kpitype_v2 = self.kpi_type_v2()

            if kpitype_v2 == kpi_cha_normal:

                children = self.get_children()
                for child in children:
                    child.delay_toggle(user=user, recursive=down_recursive, force_to_state=_to_state,
                                       ignore_perm_check=ignore_perm, reason=self.reason)
            elif kpitype_v2 == kpi_trung_gian and down_recursive:

                assigned_kpi = self.get_assigned_kpi()
                if assigned_kpi:
                    # Only delay_toggle on the active user
                    if self.parent_id and self.parent.get_children_refer().filter(
                            user__profile__active=True).count() == 0 and _to_state == 'active':
                        raise PermissionDenied

                    if assigned_kpi.user_id and assigned_kpi.user.profile and assigned_kpi.user.profile.active:
                        assigned_kpi.delay_toggle(user=user, recursive=True, force_to_state=_to_state,
                                                  ignore_perm_check=ignore_perm, reason=self.reason)
            elif kpitype_v2 == kpi_cha_duoc_phan_cong:

                children = self.get_children()
                for child in children:
                    child.delay_toggle(user=user, recursive=down_recursive, force_to_state=_to_state,
                                       ignore_perm_check=ignore_perm, reason=self.reason)

                if up_recursive and self.cascaded_from_id:
                    self.cascaded_from.delay_toggle(user=user, down_recursive=False, force_to_state=_to_state,
                                                    ignore_perm_check=ignore_perm, reason=self.reason)

        def toggle_parent(kpi, _to_state):

            refer_to = None
            if kpi.refer_to_id:
                try:
                    refer_to = kpi.refer_to
                except ObjectDoesNotExist:  # pragma: no cover
                    pass

            if not refer_to and kpi.parent_id:
                try:
                    refer_to = kpi.parent
                except ObjectDoesNotExist:  # pragma: no cover
                    pass

            if refer_to: refer_to.delay_toggle(user=user, force_to_state=_to_state, only_up_recursive=True,
                                               ignore_perm_check=ignore_perm, reason=kpi.reason)

        def is_parent_delay():
            refer_to = None
            if self.refer_to_id:
                try:
                    refer_to = self.refer_to
                except ObjectDoesNotExist:  # pragma: no cover
                    return False

            if not refer_to and self.parent_id:
                try:
                    refer_to = self.parent
                except ObjectDoesNotExist:  # pragma: no cover
                    return False

            if refer_to:
                # refer_to = KPI.objects.using('default').get(id=refer_to.id)
                if not refer_to.weight:
                    return True
            return False

        #####
        # End Utility functions
        #####

        #####
        # Step 1: check permission and raise Exception if needed
        #####
        if ignore_perm_check is False and not has_perm(KPI__EDITING, user, self.user):
            raise PermissionDenied

        #####
        # Step 2.1: then set state for current kpi
        #####
        to_state = toggle_self(force_to_state)

        #####
        # Step 3: make all the children of this KPI are toggled
        #####
        if not only_up_recursive:
            toggle_children(to_state)

        #######
        # Step 3.Extra: if parent KPI is delayed, the old latest_score is reserved
        #######
        if KPI.objects.get(id=self.id).latest_score != current_latest_score:
            self.latest_score = current_latest_score
            self.save()

        #####
        # if all other children are delayed and force_to_state == 'delay'
        #         then : toggle_parent and return
        #####

        if are_other_children_delayed(self) and to_state == 'delay':  # and recursive_from_parent == False :
            toggle_parent(self, 'delay')
            return self

        return self

    ########
    # following delay_toggle... functions have different improvements but need more clear document
    # because this is a hard function, so we try to avoid technical debt as much as possible
    ########
    #
    # def delay_toggle_advance(self, user=None,
    #                          force_to_state=None,
    #                          force_toggle_all_childs=False,
    #
    #                          recursive=True,
    #                          up_recursive=True,
    #                          down_recursive=True):
    #
    #     '''
    #
    #     Switch state of kpi to revert of current state. State should be 'delay' or 'active'
    #
    #     :param user: actor of the action
    #     :param force_to_state: None|'active'|'delay'   --> force the kpi to specific state, using commbine with force_toggle_all_childs
    #     :param force_toggle_all_childs: force all children switch to specific state, using combine with recursive & down_recursive
    #
    #     :param recursive:
    #     :param up_recursive: only effected when recursive is True
    #     :param down_recursive: only effected when recursive is True
    #     :return: self
    #
    #     '''
    #     # <editor-fold desc="util functions">
    #
    #     actor = user
    #
    #     def add_autocomment_when_toggle(k, to_state):
    #         from performance.services.kpi import add_auto_comment
    #         add_auto_comment(actor, k, content=u'{} {} kpi {}'.format(actor.email, to_state, k.name))
    #
    #     def is_delay(k):
    #         return k.weight == 0
    #
    #     def is_active(k):
    #         return k.weight > 0
    #
    #     def is_state_active(state):
    #         return state == 'active'
    #
    #     def state_is_delay(state):
    #         return state == 'delay'
    #
    #     def is_the_state(k, state):
    #         if state == 'active' and k.weight > 0:
    #             return True
    #         elif state == 'delay' and k.weight == 0:
    #             return True
    #         else:
    #             return False
    #
    #     def set_to_state(k, to_state, save=False):
    #         if to_state == 'active':
    #             k.weight = k.temp_weight if k.temp_weight > 0 else 10
    #         else:  # to_state == 'delay':
    #             k.temp_weight = k.weight if k.weight > 0 else 10
    #             k.weight = 0
    #         if save is True:
    #             k.save(update_fields=['weight', 'temp_weight'])
    #             add_autocomment_when_toggle(k, to_state)
    #
    #             from performance.services import add_new_cascadeKPIQueue
    #             add_new_cascadeKPIQueue(self)
    #
    #         return k
    #
    #     def non_recursive_toggle(k, to_state,
    #                              # force_toggle=False,
    #                              save=True):
    #         # to_state is in ['delay', 'active']
    #         # if not is_the_state(k, to_state) or force_toggle is True:# redundant
    #         set_to_state(k, to_state, save=save)
    #         if k.cascaded_from:
    #             set_to_state(k.cascaded_from, to_state, save=save)
    #
    #         return k
    #
    #     def all_childs_is_delayed(kp):
    #         childs = kp.get_children_visible()
    #         for k in childs:
    #             if is_active(k):
    #                 return False
    #
    #         return True
    #
    #     def produce_to_state(k, force_to_state=None):
    #         if force_to_state == 'delay':
    #             to_state = 'delay'
    #             # k = set_to_state(k, to_state='delay', save=False)
    #             # must switch to reverse state of to_state
    #             k = set_to_state(k, to_state='active', save=False)
    #         elif force_to_state == 'active':
    #             to_state = 'active'
    #             # k=set_to_state(k, to_state='active', save=False)
    #             k = set_to_state(k, to_state='delay', save=False)
    #         else:
    #             if self.weight > 0:
    #                 to_state = 'delay'
    #             else:
    #                 to_state = 'active'
    #
    #         return k, to_state
    #
    #     def propagate_up_toggle(kx, to_state):
    #         if is_state_active(to_state):
    #             # do nothing
    #             pass
    #         else:  # to_state == 'delay'
    #             kp = kx.refer_to
    #             if kp and all_childs_is_delayed(kp):
    #                 if is_active(kp):  # We only proceed when kp is active; if kp is delayed, do nothing.
    #                     non_recursive_toggle(kp, to_state=to_state)
    #                     propagate_up_toggle(kp, to_state=to_state)
    #                 else:
    #                     pass
    #             else:
    #                 pass
    #
    #     def propagate_down_toggle(kx, to_state, force_toggle_all_childs=False):
    #         refer_childs = kx.get_children_visible()
    #         for child in refer_childs:
    #             if not is_the_state(child,
    #                                 to_state) or force_toggle_all_childs is True:  # We only proceed when child is not to_state; if child is to_state, do nothing.
    #                 non_recursive_toggle(child, to_state)
    #                 propagate_down_toggle(child, to_state, force_toggle_all_childs=force_toggle_all_childs)
    #             else:
    #                 pass
    #
    #     # </editor-fold>
    #
    #     if user is None:
    #         user = self.user
    #
    #     if not user.profile:
    #         return self
    #
    #     if has_perm(KPI__EDITING, user, self.user):
    #         k, to_state = produce_to_state(self, force_to_state=force_to_state)
    #
    #         if not is_the_state(k, to_state):
    #             non_recursive_toggle(k, to_state)
    #             if recursive:
    #                 if up_recursive:
    #                     propagate_up_toggle(k, to_state)
    #                 if down_recursive:
    #                     propagate_down_toggle(k, to_state, force_toggle_all_childs=force_toggle_all_childs)
    #
    #             return k
    #
    #     return self
    #
    # def delay_toggle_new2017(self, user=None,
    #                          force_to_state=None,
    #                          recursive=True,
    #                          up_recursive=True,
    #                          down_recursive=True, reason=''):
    #
    #     '''
    #
    #     Switch state of kpi to revert of current state. State should be 'delay' or 'active'
    #
    #     :param user: actor of the action
    #     :param force_to_state: None|'active'|'delay'   --> force the kpi to specific state
    #
    #     :param recursive:
    #     :param up_recursive: only effected when recursive is True
    #     :param down_recursive: only effected when recursive is True
    #     :return: self
    #
    #     '''
    #
    #     # <editor-fold desc="util functions">
    #
    #     actor = user
    #
    #     def add_autocomment_when_toggle(k, to_state):
    #         from performance.services.kpi import add_auto_comment
    #         add_auto_comment(actor, k, content=u'{} {} kpi {}'.format(actor.email, to_state, k.name))
    #
    #     def is_delay(k):
    #         return k.weight == 0
    #
    #     def is_active(k):
    #         return k.weight > 0
    #
    #     def is_state_active(state):
    #         return state == 'active'
    #
    #     def state_is_delay(state):
    #         return state == 'delay'
    #
    #     def is_the_state(k, state):
    #         if state == 'active' and k.weight > 0:
    #             return True
    #         elif state == 'delay' and k.weight == 0:
    #             return True
    #         else:
    #             return False
    #
    #     def set_to_state(k, to_state, save=False):
    #         if to_state == 'active':
    #             k.weight = k.temp_weight if k.temp_weight > 0 else 10
    #         else:  # to_state == 'delay':
    #             k.temp_weight = k.weight if k.weight > 0 else 10
    #             k.weight = 0
    #         if save is True:
    #             k.save(update_fields=['weight', 'temp_weight'])
    #             add_autocomment_when_toggle(k, to_state)
    #
    #             from performance.services import add_new_cascadeKPIQueue
    #             add_new_cascadeKPIQueue(self)
    #
    #         return k
    #
    #     def non_recursive_toggle(k, to_state,
    #                              # force_toggle=False,
    #                              save=True):
    #         # to_state is in ['delay', 'active']
    #         set_to_state(k, to_state, save=save)
    #         if k.cascaded_from:
    #             set_to_state(k.cascaded_from, to_state, save=save)
    #
    #         return k
    #
    #     def all_childs_is_delayed(kp):
    #         childs = kp.get_children_visible()
    #         for k in childs:
    #             if is_active(k):
    #                 return False
    #
    #         return True
    #
    #     def propagate_up_toggle(kx, to_state):
    #         if is_state_active(to_state):
    #             # do nothing
    #             pass
    #         else:  # to_state == 'delay'
    #             kp = kx.refer_to
    #             if kp and all_childs_is_delayed(kp):
    #                 if is_active(kp):  # We only proceed when kp is active; if kp is delayed, do nothing.
    #                     non_recursive_toggle(kp, to_state=to_state)
    #                     propagate_up_toggle(kp, to_state=to_state)
    #                 else:
    #                     pass
    #             else:
    #                 pass
    #
    #     def propagate_down_toggle(kx, to_state):
    #         refer_childs = kx.get_children_visible()
    #         for child in refer_childs:
    #             non_recursive_toggle(child, to_state)
    #             propagate_down_toggle(child, to_state)
    #
    #     # </editor-fold>
    #
    #     if user is None:
    #         user = self.user
    #
    #     if not user.profile:
    #         return self
    #
    #     if has_perm(KPI__EDITING, user, self.user):
    #         k = self
    #         if force_to_state == 'delay':
    #             to_state = 'delay'
    #             # must switch to reverse state of to_state
    #             k = set_to_state(k, to_state='active', save=False)
    #         elif force_to_state == 'active':
    #             to_state = 'active'
    #             k = set_to_state(k, to_state='delay', save=False)
    #         else:
    #             if self.weight > 0:
    #                 to_state = 'delay'
    #                 self.reason = reason
    #             else:
    #                 to_state = 'active'
    #                 self.reason = ''
    #
    #         self.save()
    #
    #         if not is_the_state(k, to_state):
    #             non_recursive_toggle(k, to_state)
    #             if recursive:
    #                 if up_recursive:
    #                     propagate_up_toggle(k, to_state)
    #                 if down_recursive:
    #                     propagate_down_toggle(k, to_state)
    #
    #             return k
    #
    #     return self

    def set_prefix_id(self):

        self.prefix_id = ''

        parent = None

        if self.parent_id:
            parent = self.parent
        elif self.refer_to_id:
            parent = self.refer_to

        if parent:
            parent_order = 1
            if parent.ordering > 0:
                parent_order = parent.get_int_ordering()

            parent_order = str(parent_order)  # .zfill(2)

            self.prefix_id = parent.get_prefix_id() + (parent_order) + "."

        return self.prefix_id

    def set_prefix_children(self, update_prefix_id):
        # print u'start set_prefix_children {0} {1}'.format(self.id, slugify( self.name))

        children = self.get_children_refer()  # KPI.objects.filter(parent=self)
        if not self.prefix_id:
            self.prefix_id = ''

        for child in children:
            child.bsc_category = self.bsc_category

            print 'SET child.bsc_category {0} - ID {1}'.format(self.bsc_category, self.id)
            child.prefix_id = self.prefix_id + str(self.get_int_ordering()) + "."
            print 'SET child.prefix_id {0} '.format(self.prefix_id)
            child.save(force_update_prefix=True)

        if update_prefix_id:
            self.update_cascade_from()

            # print 'child.bsc_category - after set: {0}'.format(KPI.objects.get(id = child.id).bsc_category)
        print 'end set_prefix_children {0}'.format(self.id)

    def update_score(self, save=True):
        if (self.target == 0 or self.target) and self.operator and (self.real or self.real == 0):

            if self.get_children().count() == 0:  # neu khong co KPI con

                score = kpi_score_calculator(self.operator, self.target, self.real, self)
                self.set_score(score, reviewer_id=self.user_id, save=save)
            else:
                score, m1, m2, m3 = self.calculate_avg_score(self.user_id)
                self.set_score(score, reviewer_id=self.user_id, save=save)

            from performance.services import add_new_cascadeKPIQueue

            if self.parent_id:  # neu co KPI cha
                # score = kpi_score_calculator(self.operator, self.target, self.real)
                # self.set_score(score, self.user_id, save)
                parent = None

                try:
                    parent = self.parent
                except KPI.DoesNotExist:  # pragma: no cover
                    self.parent_id = None

                if parent:
                    logger.info('parent.calculate_avg_score(self.user_id)')
                    score, m1, m2, m3 = parent.calculate_avg_score(self.user_id)
                    parent.set_score(score, self.user_id)
                    # CascadeKPIQueue.objects.create(kpi=self.parent)
                    add_new_cascadeKPIQueue(parent)

            if self.cascaded_from_id > 0 and self and self.id > 0:  # neu co KPI trung gian
                CascadeKPIQueue.objects.create(kpi=self)
                add_new_cascadeKPIQueue(self)

    # THE FUNCTION NOT USE ANYMORE
    # def update_score(self, save=True):
    #     if (self.target == 0 or self.target) and self.operator and (self.real or self.real == 0):
    #
    #         if self.get_children().count() == 0:  # neu khong co KPI con
    #
    #             score = kpi_score_calculator(self.operator, self.target, self.real, self)
    #             self.set_score(score, reviewer_id=self.user_id, save=save)
    #         else:
    #             score, m1, m2, m3 = self.calculate_avg_score(self.user_id)
    #             self.set_score(score, reviewer_id=self.user_id, save=save)
    #
    #         from performance.services import add_new_cascadeKPIQueue
    #
    #         if self.parent_id:  # neu co KPI cha
    #             # score = kpi_score_calculator(self.operator, self.target, self.real)
    #             # self.set_score(score, self.user_id, save)
    #             parent = None
    #
    #             try:
    #                 parent = self.parent
    #             except KPI.DoesNotExist:
    #                 self.parent_id = None
    #
    #             if parent:
    #                 score, m1, m2, m3 = parent.calculate_avg_score(self.user_id)
    #                 parent.set_score(score, self.user_id)
    #                 # CascadeKPIQueue.objects.create(kpi=self.parent)
    #                 add_new_cascadeKPIQueue(parent)
    #
    #         if self.cascaded_from_id > 0 and self and self.id > 0:  # neu co KPI trung gian
    #             CascadeKPIQueue.objects.create(kpi=self)
    #             add_new_cascadeKPIQueue(self)

    def finish_editing(self):
        self.approval_status = 'finished'

        self.save()

    def approve(self):
        self.approval_status = 'approved'
        self.save()

    def reject(self):
        self.approval_status = 'rejected'
        self.save()

    def get_hash(self):
        if not self.hash:
            self.hash = hashlib.sha1(str(self.id) + random_string(3)).hexdigest()
            self.save()

        return self.hash

        # @property

    def data_input_link(self):
        return settings.DOMAIN + "/performance/data-kpi/" + self.get_hash() + "/"

    class Meta:
        db_table = "performance_kpi"
        index_together = [
            ['user', 'quarter_period'],
            ['user', 'quarter_period', 'cascaded_from']
        ]
        ordering = ['-id']

    def __unicode__(self):  # pragma: no cover

        try:
            return self.name or ''

        except:
            print 'duan error'
            return 'fk'

    def set_owner_from_email(self):
        if self.owner_email:

            owner = User.objects.filter(email=self.owner_email).first()
            if owner:
                self.user = owner
                self.owner_email = owner.email

                self.save()

    # TODO: check this, make sure it's run ;
    def update_quarter_targets(self, old_target=None):
        quarter = self.quarter_period

        if not old_target and self.id:
            try:
                old = KPI.objects.get(id=self.id)
                old_target = old.target
            except:  # pragma: no cover
                pass
        try:
            if old_target and old_target != self.target:
                if quarter.quarter == 1:
                    self.quarter_one_target = self.target
                elif quarter.quarter == 2:
                    self.quarter_two_target = self.target
                elif quarter.quarter == 3:
                    self.quarter_three_target = self.target
                elif quarter.quarter == 4:
                    self.quarter_four_target = self.target
                else:
                    self.quarter_four_target = self.target
                    self.quarter_period.quarter = 1
                    self.quarter_period.save()

                self.update_cascade_target(self.target)
                # self.save()
        except:  # pragma: no cover
            pass
        return self

    def update_targets_from_quarter(self):
        update_targets_from_quarter(self)

    def update_assigned_kpi(self):
        pass

    def update_cascade_target(self, target):
        if self.parent_id and self.assigned_to_id:
            cascaded_kpi = KPI.objects.filter(user_id=self.assigned_to_id,
                                              cascaded_from=self).first()
            if cascaded_kpi:
                quarter = cascaded_kpi.quarter_period
                # update quarter target in performance review
                if quarter.quarter == 1:
                    cascaded_kpi.quarter_one_target = target
                elif quarter.quarter == 2:
                    cascaded_kpi.quarter_two_target = target
                elif quarter.quarter == 3:
                    cascaded_kpi.quarter_three_target = target
                elif quarter.quarter >= 4:
                    cascaded_kpi.quarter_four_target = target
                cascaded_kpi.save()

    def assign_up(self, kpi, add_queue=True, force_run=False):

        logger.info('assign up: 2015')

        if kpi and not kpi.parent_id:  # chỉ cho phép liên kết lên KPI cha
            if not force_run and kpi.weight == 0:
                raise PermissionDenied(ugettext("You can't link to the postponed KPI of your manager"))

            logger.info('assign up: 2019')
            attrs = {}
            for f in self._meta.fields:
                if f.name not in ['id', 'hash', 'cascaded_from', 'refer_to', 'cascaded_from_id', 'user_id']:
                    attrs[f.name] = getattr(self, f.name)

            logger.info('assign up: 2025')
            if self.cascaded_from_id and not KPI.objects.filter(id=self.cascaded_from_id).exists():
                self.cascaded_from_id = None
                self.save(add_queue=add_queue)

            logger.info('assign up: 2030')

            if self.cascaded_from_id and self.cascaded_from.assigned_to_id == kpi.user_id:

                logger.info('assign up: 2034')

                try:
                    child = self.cascaded_from
                except:
                    child = KPI(**attrs)
            elif self.cascaded_from_id and self.cascaded_from.assigned_to_id != kpi.user_id:

                logger.info('assign up: 2042')

                child = KPI(**attrs)
                self.unique_key = str(uuid.uuid1())
                self.save(add_queue=add_queue)
                child.unique_key = self.unique_key
            else:
                logger.info('assign up: 2049')
                child = KPI(**attrs)

            child.name = self.name
            # child.name_vi = self.name
            # child.name_en = self.name
            child.current_goal = self.current_goal
            child.future_goal = self.future_goal
            child.operator = self.operator
            child.target = self.target
            child.unit = self.unit
            child.end_date = self.end_date
            child.cascaded_from = None

            child.parent = kpi
            child.user = kpi.user
            child.user_id = kpi.user_id
            child.bsc_category = kpi.bsc_category
            child.assigned_to = self.user
            child.quarter_period_id = kpi.quarter_period_id
            child.copy_from = None
            child.owner_email = None
            child.refer_to = None

            logger.info('assign up: 2073')
            child.save(set_cascaded_from_attrs=False, add_queue=add_queue)

            logger.info('assign up: 2076')
            self.cascaded_from = child
            self.bsc_category = kpi.bsc_category

            self.refer_to = kpi
            if self.user:
                self.owner_email = self.user.email

            logger.info('assign up: 2084')
            # self.save(set_cascaded_from_attrs=False, add_queue=add_queue)
            # # FIX BUG EVN
            self.save(update_fields=['cascaded_from', 'bsc_category', 'refer_to', 'owner_email'])

            logger.info('assign up: 2085')
            kpi = child

        return kpi

    def get_int_ordering(self):
        ordering = 0
        try:
            ordering = int(self.ordering)
        except:
            pass
        return ordering

    def assign_to_email(self, email):
        from user_profile.services.user import get_user_by_email
        user = get_user_by_email(email)

        if user:
            return self.assign_to_no_duplicate(user)
        else:
            return False

    def assign_to_no_duplicate(self, user):

        '''
        Catching error of assign to self.user
        '''
        if self.user == user:
            return False

        dup_kpi = KPI.objects.filter(name=self.name, user=user).first()
        if not dup_kpi:
            self.assign_to(user)
        else:

            # Nếu đây là KPI chưa được liên kết -> Liên kết lên trên self
            # Có 2 trường hợp:
            # - Self có KPI cha
            # - Self không có KPI cha

            if not dup_kpi.cascaded_from_id:

                if self.parent_id:
                    dup_kpi.cascaded_from_id = self.id
                    dup_kpi.refer_to = self.parent
                    dup_kpi.save()
                else:

                    dup_kpi.assign_up(self,
                                      force_run=True)  # # permission for assign_up should check before call assign_to_no_duplicate ==> force_run=True

    def assign_to(self, user):

        '''
        Catching error of assign to self.user
        '''
        if self.user == user:
            return False

        from user_profile.models import Profile

        if not self.parent_id:
            from performance.services.kpi import create_child_kpi

            if not self.quarter_period:
                self.quarter_period = self.user.profile.get_current_quarter()

            new_kpi = create_child_kpi(self, self.quarter_period, None)

            new_kpi.assigned_to = user
            new_kpi.save()  # update_fields=['assigned_to_id'])

            return new_kpi.assign_to(user)
        else:

            self.assigned_to = user
            self.refer_to = None
            self.owner_email = None
            self.save()
            attrs = {}
            for f in self._meta.fields:
                if f.name not in ['id', 'hash']:
                    try:
                        attrs[f.name] = getattr(self, f.name)
                    except KPI.DoesNotExist:  # pragma:no cover
                        pass

            if KPI.objects.filter(cascaded_from=self, user=user).exists():
                # return HttpResponse('ok')
                return KPI.objects.filter(cascaded_from=self, user=user).first()

            kpis = KPI.objects.filter(cascaded_from=self)
            if kpis.exists():  # Đã có KPi được phân công --> đổi users
                new_kpi = kpis.first()
                new_kpi.user = user
                new_kpi.owner_email = user.email
                new_kpi.bsc_category = self.parent.bsc_category
                if new_kpi.bsc_category:
                    new_kpi.ordering = self.parent.ordering
                new_kpi.assigned_to = None
                # if Profile.objects.filter(report_to_id=self.user_id, user=user).exists():
                #     new_kpi.manager_confirmed = True

            else:
                new_kpi = KPI(**attrs)
                new_kpi.assigned_to = None
                new_kpi.user = user
                new_kpi.parent = None
                new_kpi.copy_from = None
                new_kpi.cascaded_from = self
                new_kpi.bsc_category = self.parent.bsc_category
                new_kpi.owner_email = user.email

                if new_kpi.bsc_category:
                    new_kpi.ordering = self.parent.ordering
                else:
                    new_kpi.ordering = self.get_next_ordering()
                # if Profile.objects.filter(report_to_id=self.user_id, user=user).exists():
                if Profile.objects.filter(parent=self.user.profile, user=user).exists():
                    new_kpi.manager_confirmed = True

            new_kpi.refer_to = self.parent
            new_kpi.owner_email = user.email
            new_kpi.save()
            return new_kpi

    def get_quarter(self):
        if not self.quarter_period:
            self.quarter_period = self.user.profile.get_organization().get_current_quarter()
            self.save()

        return self.quarter_period

    # @classmethod TODO: not use
    # def set_quarter_all_kpi(cls, organization):
    #     current_quarter = organization.get_current_quarter()
    #     kpis = KPI.objects.filter(quarter_period__isnull=True,
    #                               user_id__in=UserOrganization.objects.filter(organization=organization).values_list(
    #                                   'user_id', flat=True))
    #     kpis.update(quarter_period=current_quarter)

    # @classmethod
    # def get_company_kpis(cls, map):
    #     return KPI.objects.filter(map=map)

    def kpi_quarterly_real_calculator(self):
        if self.review_type == 'monthly':

            self.month_1 = fix_input_float(self.month_1)
            self.month_2 = fix_input_float(self.month_2)
            self.month_3 = fix_input_float(self.month_3)

            if self.score_calculation_type == 'most_recent':
                # if self.month_3 is not None:
                #     self.real = self.month_3
                # elif self.month_2 is not None:
                #     self.real = self.month_2
                # elif self.month_1 is not None:
                #     self.real = self.month_1

                self.month_1_target = self.get_month_1_target()
                self.month_2_target = self.get_month_2_target()
                self.month_3_target = self.get_month_3_target()

                if self.month_3_target is not None and self.month_3 is not None:
                    if self.target is None:
                        self.target = self.month_3_target
                    self.real = self.month_3
                elif self.month_2_target is not None and self.month_2 is not None:
                    if self.target is None:
                        self.target = self.month_2_target
                    self.real = self.month_2
                else:
                    if self.target is None:
                        self.target = self.month_1_target
                    self.real = self.month_1

            elif self.score_calculation_type == 'sum':
                month_1 = 0
                month_2 = 0
                month_3 = 0
                if self.month_1:
                    month_1 = self.month_1

                if self.month_2:
                    month_2 = self.month_2

                if self.month_3:
                    month_3 = self.month_3

                self.real = month_1 + month_2 + month_3

            elif self.score_calculation_type == 'average':

                month_1 = 0
                month_2 = 0
                month_3 = 0

                i = 0
                if self.month_1 is not None:
                    month_1 = self.month_1
                    i += 1

                if self.month_2 is not None:
                    month_2 = self.month_2
                    i += 1

                if self.month_3 is not None:
                    month_3 = self.month_3
                    i += 1

                sum = month_1 + month_2 + month_3
                if i > 0:
                    self.real = sum / i

        return self.real

    def delete(self):

        if self.refer_to_id:
            try:
                refer_to = self.refer_to

                refer_to.save_calculate_score()

            except ObjectDoesNotExist:  # pragma: no cover
                pass
        # if self.score_calculation_automation is None:
        #     self.score_calculation_automation = True
        # if self.score_calculation_type is None:
        #     self.score_calculation_type = ''
        kpis = KPI.objects.filter(cascaded_from=self)
        for kpi in kpis:
            kpi.cascaded_from_id = None
            kpi.save()
        #
        if self.cascaded_from_id > 0:
            try:
                self.cascaded_from.assigned_to_id = None

                self.cascaded_from.refer_to = self.parent
                self.cascaded_from.save()
            except Exception as e:
                notify_slack(settings.SLACK_ERROR_CHANNEL,
                             'self.cascaded_from.assigned_to_id error: {0} '.format(str(e)))

        super(KPI, self).delete()

        # ## When delete, check all children of parent whether are delayed or not, if all are delayed, delay parent kpi
        if are_other_children_delayed(self):
            current_db = self._state.db
            parent = self.refer_to
            # check for case of parent kpi just deleted right before, but runtime data still not update to self.refer_to
            # it's should be set to None, but it's not!
            # So, we must check directly from db/masked-db(PermanentModel) for whethe the parent is exist
            if parent and KPI.objects.using(current_db).filter(id=parent.id).exists():
                # if parent:
                # we donot need check permission here, because of the logic here is auto right, regardless of permissions!
                parent.delay_toggle(recursive=False, force_to_state='delay', ignore_perm_check=True)

    def get_custom_label(self, field_name):
        if field_name in ['month_1_target', 'month_2_target', 'month_3_target',
                          'month_1', 'month_2', 'month_3']:
            month_1 = self.quarter_period.month_1_name()
            month_2 = self.quarter_period.month_2_name()
            month_3 = self.quarter_period.month_3_name()

            msg = ugettext("{} target")
            if field_name == 'month_1_target':
                return msg.format(month_1)
            elif field_name == 'month_2_target':
                return msg.format(month_2)
            elif field_name == 'month_3_target':
                return msg.format(month_3)

            msg = ugettext("{} result")
            if field_name == 'month_1':
                return msg.format(month_1)
            elif field_name == 'month_2':
                return msg.format(month_2)
            elif field_name == 'month_3':
                return msg.format(month_3)

    def get_changed_history(self, show_name=True, delimiter="\n"):
        changed = self.tracker.changed()
        descs = []

        if show_name:
            descs.append(u"{}:".format(slugify(
                self.name)))  # need fix all error  of UnicodeEncodeError: 'ascii' codec can't encode character u'\u0103' in position 1: ordinal not in range(128)

        # remove real because it is not necessary into log
        if 'real' in changed:
            del changed['real']

        keys = changed.keys()
        keys.sort()

        for f in keys:
            if hasattr(self, f):
                if f == 'review_type':
                    continue

                if f.endswith("_id"):
                    continue

                if changed[f] is None and getattr(self, f) is None:
                    continue

                title = self.get_custom_label(f)
                if not title:
                    title = self._meta.get_field(f).verbose_name

                blank = ugettext("None")
                if f == "name":
                    blank = ugettext("Unknow")

                old_value = changed[f] if changed[f] not in ['', None] else blank
                new_value = getattr(self, f) if getattr(self, f) not in ['', None] else ugettext("None")

                if f == 'score_calculation_type':
                    map_value = dict(KPI.KPI_CALCULATION_TYPE)
                    old_value = map_value.get(changed[f], '')
                    new_value = map_value.get(getattr(self, f), '')

                if delimiter == "\n":
                    item = u"- %s: %s → %s" % (title, old_value, new_value)
                else:
                    item = u"%s: %s → %s" % (title, old_value, new_value)
                descs.append(item)

        return delimiter.join(descs)

        # @property

    # def status_color(self): Not use
    #     color = "#bbbbbb"
    #     if self.status == "not_started":
    #         color = "#aaaaaa"
    #     elif self.status == "behind":
    #         color = "#c00000"
    #     elif self.status == "on_track":
    #         color = "#54a413"
    #     elif self.status == "completed":
    #         color = "#1c42df"
    #     elif self.status == "postponed":
    #         color = "#7a7a7a"
    #     elif self.status == "cancelled":
    #         color = "#bbbbbb"
    #
    #     return color

    # def category_color(self): Not use
    #     if self.bsc_category == "financial":
    #         return "#ff5c00"
    #     elif self.bsc_category == "customer":
    #         return "#00a2dd"
    #     elif self.bsc_category == "internal":
    #         return "#54a413"
    #     elif self.bsc_category == "learninggrowth":
    #         return "#d79f00"
    #     else:
    #         return "inherit"

    def to_percent(self, num=False):
        # score = self.get_final_score()
        score = self.get_score()  # self.user_id)
        if is_nan(score):
            score = 0
        percent = 0
        if score:
            percent = score
            # excluded following line as score is changed to 0 if it not a string. thus, percent is not a string
            if percent == "N/A":  # pragma:no cover
                percent = 0

        if num:
            return percent
        return "{0}%".format(percent)

    # def to_percent_show(self): not used
    #     if self.real is None and not self.has_child():
    #         return ""
    #     else:
    #         percent = self.to_percent(True)
    #         return "{0}%".format(round(percent, 1))

    # def to_percent_progress(self):    Not used
    #     # score = self.get_final_score()
    #     score = self.get_score()  # self.user_id)
    #
    #     percent = 0
    #     if score:
    #         percent = score
    #         if percent == "N/A":
    #             percent = 0
    #
    #     width = round(percent * 100 / 120.0, 1)
    #     # width=100
    #     # red: 0-200 -->
    #     g_color = int(width * 2)
    #     b_color = int(width * 2)
    #     red_opacity = 1
    #     red_opacity = (100 - width) / 100.0
    #     # return str(width)
    #     return {'width': str(width), 'g_color': str(g_color), 'b_color': str(b_color), 'red_opacity': str(red_opacity)}

    # def get_assigned_to(self):    Not use
    #     users = list(KPI.objects.filter(parent=self,
    #                                     assigned_to__isnull=False) \
    #                  .values_list('assigned_to',
    #                               flat=True))
    #     users = list(set(users))
    #
    #     if KPI.objects.filter(parent=self,
    #                           assigned_to__isnull=True).exists() or not users:
    #         users.insert(0, self.user_id)
    #
    #     return users

    def get_unique_key(self):
        if not self.unique_key:
            self.save()

        return self.unique_key

    def get_name(self):
        if self.name:
            return self.name.upper()
        else:
            return ''

            # if self.name == self.name_en:
            #     # TODO: this have a problem
            #     return self.name_vi
            # else:
            #     return self.name

    def get_name_current_goal(self):
        if self.current_goal:
            return self.get_name() + u": " + self.current_goal
        else:
            return self.get_name()

    def get_kpi_mail_action_url(self, percentage, reviewer_id):
        return "http://www.cjs.vn/performance/kpi_mail_action/" + str(
            self.id) + "/" + self.get_unique_key() + "/" + str(percentage) + "/" + str(reviewer_id) + "/"

    def update_cascade_from(self):
        '''
        To make sure all update to KPI is updated to its' cascaded KPIs and vice vesa
        '''
        if self.cascaded_from_id > 0:  # neu `self` la KPI duoc phan cong (khong phai KPI trung gian)

            if self.cascaded_from.prefix_id != self.prefix_id \
                    or self.cascaded_from.bsc_category != self.bsc_category:  # set KPI trung gian
                self.cascaded_from.prefix_id = self.prefix_id
                self.cascaded_from.bsc_category = self.bsc_category
                self.cascaded_from.save(update_prefix_id=False)

                #        else:

        cascaded_to = KPI.objects.filter(cascaded_from=self)  # self la KPI trung gian

        if cascaded_to.count() > 0:
            # update KPI duoc phan cong
            cascaded_to = cascaded_to[0]
            if cascaded_to.prefix_id != self.prefix_id:
                cascaded_to.prefix_id = self.prefix_id
                cascaded_to.bsc_category = self.bsc_category
                cascaded_to.save(update_prefix_id=False)

        print 'END update_cascade_from ID {}'.format(self.id)

    def move_kpi(self, parent_id, actor=None):
        # TODO: check actor

        assigned_kpi = None

        if self.assigned_to_id > 0:  # Phan cong lai
            assigned_kpi = self.get_assigned_kpi()
            if parent_id:
                new_parent = KPI.objects.get(id=parent_id)

                # print new_parent.id
                if assigned_kpi:
                    assigned_kpi.assign_up(new_parent,
                                           force_run=True)  # permission for assign_up should check before call move_kpi ==> force_run=True
                    # self.delete()
                    # else:
                    #     assigned_kpi.assign_up(self)

        else:

            if parent_id and str(parent_id).isdigit() and \
                    KPI.objects.filter(user_id=self.user_id, id=parent_id).exists():

                # if self.parent_id is None or self.parent_id == 0:  # nếu chuyển KPI Cha vào trong thành KPI con
                #     self.refer_to_id = parent_id

                self.parent_id = parent_id

            else:
                self.parent_id = None
                self.refer_to = None

            self.save()

        return self

    def save_calculate_score(self, just_created=False, calculate_now_thread=True):

        '''

        :param just_created:
        :param calculate_now_thread:
        :return: kpi


        '''

        kpi = self.save(add_queue=True, just_created=just_created, calculate_now_thread=calculate_now_thread)

        update_kpi_same_unique_code(kpi)

        return kpi

    def save(self, update_prefix_id=True, force_update_prefix=False,
             set_cascaded_from_attrs=True, actor=None,
             just_created=False, add_queue=False,
             calculate_now_thread=True,
             log_msg=None,
             create_log=True,
             is_new_quarter=False, prev_quarter=None,
             *args, **kwargs):

        if self.id is None:
            # log trace back
            self.log_trace = log_trace(self.log_trace)

        '''
        Validate input
        '''
        self.weight = fix_input_float(self.weight)

        if not kwargs.get('update_fields') is None:
            return super(KPI, self).save(*args, **kwargs)

        if self.name:
            try:
                self.name = self.name.upper()
            except:  # pragma: no cover
                pass

        # if self.name_en:
        #     try:
        #         self.name_en = self.name_en.upper()
        #     except:
        #         pass

        # if self.name_vi:
        #     try:
        #         self.name_vi = self.name_vi.upper()
        #     except:
        #         pass

        if self.reviewer_email:
            self.reviewer_email = self.reviewer_email.strip()

        '''
        QUICK HACK -  SHOULD use Better way
        '''
        if self and self.score_calculation_type == 'most_recent':
            if self.month_1_target is None:
                self.month_1_target = self.get_month_1_target()

            if self.month_2_target is None:
                self.month_2_target = self.get_month_2_target()

            if self.month_3_target is None:
                self.month_3_target = self.get_month_3_target()
        '''
        END QUICK HACK
        '''

        # DELETING KPI-EDITOR CACHE
        if self.id:
            cache.delete('kpi_editor_{}'.format(self.id))
            cache.delete('fix_kpi_self_{}'.format(self.id))

            if self.parent_id:
                cache.delete('kpi_editor_{}'.format(self.parent_id))
                cache.delete('fix_kpi_self_{}'.format(self.parent_id))
            if self.refer_to_id:
                cache.delete('kpi_editor_{}'.format(self.refer_to_id))
                cache.delete('fix_kpi_self_{}'.format(self.refer_to_id))

                if self.refer_to_id > 0:
                    try:
                        refer_to = self.refer_to
                        if refer_to.refer_to_id:
                            cache.delete('kpi_editor_{}'.format(self.refer_to.refer_to_id))
                            cache.delete('fix_kpi_self_{}'.format(self.refer_to.refer_to_id))
                    except:  # pragma: no cover
                        pass

        # DELETE OTHER CACHEs:
        # #delete_memoized_verhash(score_calculator)
        delete_memoized_verhash(calculate_score)

        # fix user input
        self.real = fix_input_float(self.real)
        self.month_1 = fix_input_float(self.month_1)
        self.month_2 = fix_input_float(self.month_2)
        self.month_3 = fix_input_float(self.month_3)

        # END fix user input

        if self.score_calculation_automation is None:
            self.score_calculation_automation = True
        if self.score_calculation_type is None:
            self.score_calculation_type = ''

        if self.month_1 == '':
            self.month_1 = None
        if self.month_2 == '':
            self.month_2 = None
        if self.month_3 == '':
            self.month_3 = None

        # ## QuocDuan comment: we can use this way if only want to set prefix on the time kpi created or related fields got change!
        # if self.id is None and update_prefix_id:  # this is create new KPI
        #     try:
        #         self.set_prefix_id()
        #     except:
        #         pass
        # if self.id is not None:# this is updating, not create KPI
        #     # self.tracker.changed()
        #     changed_fields = self.tracker.changed().keys()
        #     if 'ordering' in changed_fields \
        #             or 'bsc_category' in changed_fields \
        #             or force_update_prefix:
        #         self.set_prefix_children(update_prefix_id)

        current_kpi = None
        try:
            if self.id:
                # https://docs.djangoproject.com/en/dev/topics/db/multi-db/#manually-selecting-a-database-for-a-queryset
                # https://stackoverflow.com/questions/6536783/django-getting-data-from-different-database
                # QuocDuan comment: make sure we use the current connection to get the KPI itself from database
                # this will avoid blocking transaction
                current_db = self._state.db
                current_kpi = KPI.objects.using(current_db).get(id=self.id)
        except:
            pass

        if not current_kpi and update_prefix_id:  # this is create new KPI
            # self.set_prefix_id()
            try:
                self.set_prefix_id()
            except:  # pragma: no cover
                pass

        if current_kpi:  # this is updating, not create KPI

            if current_kpi.ordering != self.ordering \
                    or current_kpi.bsc_category != self.bsc_category or force_update_prefix:
                self.set_prefix_children(update_prefix_id)

        # Hong remove june 2017
        #  Calculate score
        # if current_kpi \
        #         and (self.real or self.real == 0) and self.assigned_to is None \
        #         and (
        #                             current_kpi.real != self.real or current_kpi.target != self.target or current_kpi.operator != self.operator):
        #
        #     self.update_score(save=False)
        #     # score = kpi_score_calculator(self.operator, self.target, self.real)
        # else:
        #     score = None

        if self.current_goal:
            self.current_goal = self.current_goal.strip()  # remove spacesave

        if not self.hash:
            self.hash = hashlib.sha1(str(self.id) + random_string(3)).hexdigest()

        # self.last_review = datetime.now()
        if self.latest_score is None or is_nan(self.latest_score):
            self.latest_score = 0

        # self.latest_score = self.get_final_score()

        if self.unique_key is None or self.unique_key == '':
            if self.copy_from_id > 0:
                try:
                    if self.copy_from.user_id == self.user_id:  # the same user and kpi
                        if self.copy_from.unique_key is not None:
                            self.unique_key = self.copy_from.unique_key
                except:  # pragma: no cover
                    pass

            if self.unique_key is None or self.unique_key == '':
                self.unique_key = str(uuid.uuid1())

        if self.is_started is None:
            self.is_started = False

        if self.is_started == False:
            if self.latest_score > 0:
                self.is_started = True

        if set_cascaded_from_attrs == False:
            self.set_cascaded_from_attrs()

        if self.prefix_id and len(self.prefix_id) > 50:
            self.prefix_id = self.prefix_id[:50]

        if self.weight is None:
            self.weight = 10

        is_created = self.id == None

        if not self.refer_group_name and self.name:
            self.refer_group_name = self.name

        if self.unique_code: self.unique_code = self.unique_code.lower()

        super(KPI, self).save(*args, **kwargs)

        if is_created:
            from performance.services import add_auto_comment
            if not actor:
                from cuser.middleware import CuserMiddleware
                from user_profile.services.profile import get_user_by_id
                actor = CuserMiddleware.get_user()
                if not actor:
                    actor = get_system_user()

            content = ugettext("Created KPI")
            if is_new_quarter:
                if prev_quarter:
                    content = ugettext("Clone from quarter period: ") + str(prev_quarter)
            if log_msg:
                content = log_msg

            if create_log:
                add_auto_comment(actor, self, content)

        # print 'im here'

        if add_queue and not is_created:
            from performance.services import add_new_cascadeKPIQueue

            # add_new_cascadeKPIQueue(self, calculate_now_thread=calculate_now_thread)
            from performance.services import calculate_score_cascading
            from tasks import calculate_score_cascading_celery
            if calculate_now_thread:
                # calculate_score_cascading.delay(self.id)
                calculate_score_cascading_celery(self.id)
            else:
                self = calculate_score_cascading(self.id)
                # self = calculate_score_cascading_celery(self.id)
                super(KPI, self).save(*args, **kwargs)  # save kpi after calculate score

            print "line 2781: add_new_cascadeKPIQueue(self) : ID {}".format(self.id)

        # Hong remove this
        # if not is_created and not just_
        # created:
        #     print "2736 ScoreLog.log_user_score(self) : ID {}".format(self.id)
        #     ScoreLog.log_user_score(self)

        return self

    def clone_kpi_to_user(self, to_user, quarter_period):
        unique_key = str(uuid.uuid1())
        # create kpi this way will not auto trigger save method
        new_kpi = KPI(name=self.name,
                      # name_vi=self.name_vi,
                      # name_en=self.name_en,
                      description=self.description,
                      # description_vi=self.description_vi,
                      # description_en=self.description_en,
                      user=to_user,
                      weight=self.weight,
                      quarter_period=quarter_period,
                      current_goal=self.current_goal,
                      future_goal=self.future_goal,
                      outcome_notes=self.outcome_notes,
                      outcome_notes_self=self.outcome_notes_self,
                      operator=self.operator,
                      target=self.target,
                      unit=self.unit,
                      ordering=self.ordering,
                      bsc_category=self.bsc_category,
                      end_date=self.end_date,
                      unique_key=unique_key,
                      quarter_one_target=self.quarter_one_target,
                      quarter_two_target=self.quarter_two_target,
                      quarter_three_target=self.quarter_three_target,
                      quarter_four_target=self.quarter_four_target,
                      year_target=self.year_target,
                      group=self.group,
                      )

        if not self.parent_id:
            new_kpi.owner_email = to_user.email
        new_kpi.save()
        return new_kpi

    def update_review_mode(self, cal_type=None, review_type=None):
        if cal_type == None and review_type == None:
            return

        if cal_type is not None:
            self.score_calculation_type = cal_type
        if review_type is not None:
            self.review_type = review_type

        return self.save_calculate_score(calculate_now_thread=False)
        # from performance.services import KPIServices
        # KPIServices().calculate_cascade_score(self)
        # return None

    def clone_to_new_kpi(self, to_user, quarter_period, unique_key, clone_to_new_kpi=False,
                         copy_unique_code=False):
        new_kpi = KPI(name=self.name,
                      code=self.code,
                      # name_vi=self.name_vi,
                      # name_en=self.name_en,
                      description=self.description,
                      # description_vi=self.description_vi,
                      # description_en=self.description_en,
                      user=to_user,
                      weight=self.weight,
                      temp_weight=self.temp_weight,
                      quarter_period=quarter_period,
                      is_owner=self.is_owner,
                      current_goal=self.current_goal,
                      future_goal=self.future_goal,
                      outcome_notes=self.outcome_notes,
                      outcome_notes_self=self.outcome_notes_self,
                      copy_from=self if self.user_id == to_user.id else None,
                      operator=self.operator,
                      target=self.target,
                      unit=self.unit,
                      ordering=self.ordering,
                      bsc_category=self.bsc_category,
                      end_date=self.end_date,
                      unique_key=unique_key,
                      owner_email=to_user.email,
                      quarter_one_target=self.quarter_one_target,
                      quarter_two_target=self.quarter_two_target,
                      quarter_three_target=self.quarter_three_target,
                      quarter_four_target=self.quarter_four_target,

                      review_type=self.review_type,
                      score_calculation_type=self.score_calculation_type,
                      prefix_id=self.prefix_id,
                      reviewer_email=self.reviewer_email,
                      default_real=self.default_real,
                      group=self.group,

                      year_target=self.year_target,
                      year_result=self.year_result,
                      year_score=self.year_score,

                      year_data=self.year_data ,

                      children_data=self.children_data ,

                      achievement_calculation_method=self.achievement_calculation_method ,
                      achievement_calculation_method_extra=self.achievement_calculation_method_extra ,
                      )

        if copy_unique_code:
            new_kpi.unique_code = self.unique_code
        #
        # if clone_to_new_kpi:
        #     new_kpi.clone_to_new_kpi = clone_to_new_kpi

        return new_kpi

    def set_target_from_quarter_targets(self):
        quarter_period = self.quarter_period
        try:
            quarter_period.quarter = int(quarter_period.quarter)
        except:
            pass

        if quarter_period.quarter == 1:
            # if is_new_quarter and self.quarter_four_target:
            #     new_kpi.target = self.quarter_four_target
            # elif self.quarter_one_target:
            self.target = self.quarter_one_target
        elif quarter_period.quarter == 2 and self.quarter_two_target:
            self.target = self.quarter_two_target
        elif quarter_period.quarter == 3 and self.quarter_three_target:
            self.target = self.quarter_three_target
        elif quarter_period.quarter >= 4 and self.quarter_four_target:
            self.target = self.quarter_four_target

    def fix_clone_to_user_missing_child(self):
        """

        This fix is for the bug that sometime create new quarter happen, should NOT use without understanding
        """

        previous_kpi = KPI.objects.filter(unique_key=self.unique_key).exclude(id=self.id).order_by('-id').first()

        current_quarter = self.user.profile.get_current_quarter()

        if previous_kpi:
            print 'self ID {} - {} - previous_kpi ID {} : KPI_ID {} - {} ' \
                  ''.format(self.id, self.user.username, previous_kpi.id, previous_kpi.get_kpi_id(), slugify(self.name))

            if previous_kpi.get_children().count() != self.get_children().count():
                print 'ERROR --> previous_kpi.get_children().count() {}'.format(previous_kpi.get_children().count())
                print 'ERROR --> self.get_children().count() {}'.format(self.get_children().count())

                for child in self.get_children():
                    child.delete()

                for child in previous_kpi.get_children():
                    unique_key = child.unique_key

                    new_child_kpi = child.clone_to_new_kpi(self.user, current_quarter, unique_key)

                    new_child_kpi.parent = self

                    new_child_kpi.set_target_from_quarter_targets()
                    new_child_kpi.save()
            if previous_kpi.get_children().count() != self.get_children().count():
                print '-------------------> ERROR CAN NOT BE FIXED: self.get_children().count() {}'.format(
                    self.get_children().count())

    def clone_to_user(self, to_user, new_quarter_period=None, is_new_quarter=False, copy_refer_to=True,
                      copy_unique_code=False, update_prefix_id=True, create_log=True):
        from performance.services.kpi import update_month_targets_onchange_calculation_type, \
            update_year_data_to_kpi_target

        logger.info('clone_to_user: {0} - KPI.ID {1}'.format(to_user.id, self.id))

        unique_key = str(uuid.uuid1()) if self.user_id != to_user.id else self.unique_key
        prev_quarter = self.quarter_period
        if not new_quarter_period:
            new_quarter_period = self.quarter_period
        new_kpi = self.clone_to_new_kpi(to_user, new_quarter_period, unique_key, copy_unique_code=copy_unique_code)

        # if new_quarter_period.clone_from_quarter:
        #
        #     new_quarter_period.clone_from_quarter = int(new_quarter_period.clone_from_quarter)

        new_kpi.set_target_from_quarter_targets()
        new_kpi.month_1_target = None
        new_kpi.month_2_target = None
        new_kpi.month_3_target = None
        new_kpi = update_year_data_to_kpi_target(new_kpi, new_kpi.year_data)
        if new_kpi.month_1_target is None and new_kpi.month_2_target is None and new_kpi.month_3_target is None:
            new_kpi = update_month_targets_onchange_calculation_type(new_kpi)
        #
        # If there is default_real --> we will have to set it.
        #

        if new_kpi.default_real is not None:
            new_kpi.month_1 = new_kpi.default_real
            new_kpi.month_2 = new_kpi.default_real
            new_kpi.month_3 = new_kpi.default_real
            new_kpi.real = new_kpi.default_real

            new_kpi.save(update_prefix_id=update_prefix_id, create_log=create_log, is_new_quarter=is_new_quarter, prev_quarter=prev_quarter)

        else:
            new_kpi.save(add_queue=False, update_prefix_id=update_prefix_id, create_log=create_log, is_new_quarter=is_new_quarter, prev_quarter=prev_quarter)

        logger.info('clone_to_user: 2912 after new_kpi.save() {0} - KPI.ID {1}'.format(to_user.id, self.id))

        refer_to = None
        try:
            refer_to = self.refer_to
        except ObjectDoesNotExist:  # pragma: no cover
            pass

        # <copy refer_to >
        # only allow link to not-delayed(active) kpi
        if copy_refer_to and refer_to:
            logger.info('clone_to_user: 2922 before new_kpi.assign_up() {0} - KPI.ID {1}'.format(to_user.id, self.id))
            new_kpi.assign_up(refer_to, add_queue=False, force_run=True)
            logger.info('clone_to_user: 2924 after new_kpi.assign_up() {0} - KPI.ID {1}'.format(to_user.id, self.id))

        # </copy refer_to >

        # clone children:
        for child in self.get_children():
            unique_key = str(uuid.uuid1()) if self.user_id != to_user.id else child.unique_key

            new_child_kpi = child.clone_to_new_kpi(to_user, new_quarter_period, unique_key,
                                                   copy_unique_code=copy_unique_code)
            logger.info('clone_to_user: after child.clone_to_new_kpi() {0} - KPI.ID {1}'.format(to_user.id, self.id))

            new_child_kpi.parent = new_kpi

            # if child.kpi_type_v2() == kpi_con_normal:
            '''
            When copy KPI, we will set children KPIs into `kpi_con_normal`
            '''
            new_child_kpi.refer_to = new_kpi

            new_child_kpi.set_target_from_quarter_targets()

            new_child_kpi.save(add_queue=False, create_log=create_log, is_new_quarter=is_new_quarter, prev_quarter=prev_quarter)

            logger.info('clone_to_user: after new_child_kpi.save {0} - KPI.ID {1}'.format(to_user.id, self.id))
        return new_kpi

    #
    # def calculate_avg_score_allreviewers(self):
    #     reviews = Review.objects.filter(reviewee_id=self.user_id, quarter_period_id=self.quarter_period_id)
    #     count = 0
    #
    #     total_score = 0
    #     for review in reviews:
    #         score, m1, m2, m3 = self.calculate_avg_score(review.reviewer_id)
    #         if score:
    #             total_score += score
    #             count += 1
    #
    #     if count <= 0:
    #         count = 1
    #
    #     return float(total_score) / count

    def get_cascaded_from_weight(self):
        if self.cascaded_from_id:
            try:
                cascaded_from = self.cascaded_from
                return cascaded_from.weight
            except ObjectDoesNotExist:  # pragma: no cover
                pass

        return self.weight

    # backup calculate_avg_score: https://gist.github.com/anonymous/d17174701cb1926accb4ffd16bbab006

    def calculate_avg_score(self, reviewer_id=None):
        if reviewer_id is None:
            reviewer_id = self.user_id
        # get children-kpis, exclude delay-kpis and kpis that assigned to inactive user

        children = self.get_children_refer()

        # get total weight of children-kpis
        total_score = 0
        total_m1 = None
        total_m2 = None
        total_m3 = None
        total_weight = 0  # children.aggregate(total=Sum('weight'))['total']
        quarter_score = None
        m1_score = None
        m2_score = None
        m3_score = None

        # print 'total {}'.format(total)
        # if total > 0 and total != None:
        m1_none = True
        m2_none = True
        m3_none = True

        from performance.services.children_data_services import get_children_data
        children_data = get_children_data(self.user, self)

        if children_data['parent_score_auto'] and children.exists():

          #  children = self.get_children_refer()  # get_children().filter(weight__gt=0)  # .exclude(assigned_to__userorganization__active=False)

            for child_weight in children_data['children_weights']:
                from performance.services import get_kpi
                child = get_kpi(child_weight['kpi_id'])
                if not child:
                    continue

                score = child.get_score()
                score_m1 = child.month_1_score if child.month_1_score is not None else None
                score_m2 = child.month_2_score if child.month_2_score is not None else None
                score_m3 = child.month_3_score if child.month_3_score is not None else None

                total_weight += child_weight['weight']  # child.get_cascaded_from_weight()

                if score:
                    total_score += score * child_weight['weight']  # child.get_cascaded_from_weight()
                    if score_m1:
                        total_m1 = (total_m1 or 0) + score_m1 * child_weight['weight']  # child.get_cascaded_from_weight()

                    if score_m2:
                        total_m2 = (total_m2 or 0) + score_m2 * child_weight['weight']  # child.get_cascaded_from_weight()

                    if score_m3:
                        total_m3 = (total_m3 or 0) + score_m3 * child_weight['weight']  # child.get_cascaded_from_weight()

            if total_weight:

                quarter_score = (quarter_score or 0) + total_score / float(total_weight)
                if total_m1:
                    m1_score = (m1_score or 0) + total_m1 / float(total_weight)

                if total_m2:
                    m2_score = (m2_score or 0) + total_m2 / float(total_weight)

                if total_m3:
                    m3_score = (m3_score or 0) + total_m3 / float(total_weight)

            else:
                quarter_score = 0
        else:
            self = calculate_score(self)
            quarter_score = self.latest_score  # get_score()  # reviewer_id))

            m1_score = self.month_1_score  # reviewer_id))
            m2_score = self.month_2_score
            m3_score = self.month_3_score

            # print 'value {}'.format(value)
        return quarter_score, m1_score, m2_score, m3_score

    def get_key(self, reviewer_id=None):
        key = "kpi:" + str(self.id) + ":" + str(reviewer_id)
        return key

    def get_key_list(self):
        rdis = redis.from_url(settings.redis_url)
        key = "kpi:" + str(self.id) + ":*"

        return rdis.keys(key)

    def set_assigned_kpi_attrs(self, actor=None):
        set_assigned_kpi_attrs(self, actor)

    def clone_attrs(self, other_kpi, actor, update_prefix_id=True):
        clone_attrs(self, other_kpi, actor, update_prefix_id=True)

    def set_cascaded_from_attrs(self, actor=None):
        set_cascaded_from_attrs(self, actor=None)

    def set_score(self, value, month_1_score=None, month_2_score=None, month_3_score=None, reviewer_id=None, save=True):

        # set latest score to know whether this kpi is reviewed or not / this is not the exactly score of this kpi
        try:
            # if value:
            self.latest_score = fix_input_float(value)
            if month_1_score:
                self.month_1_score = fix_input_float(month_1_score)
            else:
                self.set_month_1_score(save=True)

            if month_2_score:

                self.month_2_score = fix_input_float(month_2_score)
            else:
                self.set_month_2_score(save=True)

            if month_3_score:

                self.month_3_score = fix_input_float(month_3_score)
            else:
                self.set_month_3_score(save=True)

        except ValueError:  # pragma: no cover
            pass

        except Exception as e:  # pragma: no cover
            print ('value = {}'.format(value))
            # notify_slack( settings.SLACK_CRONJOB_CHANNEL, 'set_score fail ID: {} - {}'.format(self.id, str(e)))
            print('set_score fail ID: {} - {}'.format(self.id, str(e)))
        #
        # logger.info('performance/models.py:  line 3081')
        # self.set_month_1_score(save=False)
        # # logger.info('performance/models.py:  line 3083')
        # self.set_month_2_score(save=False)
        # #  logger.info('performance/models.py:  line 3085')
        # self.set_month_3_score(save=False)
        #
        # #  logger.info('performance/models.py:  line 3087')

        if save:

            try:
                self.save(update_fields=["latest_score", "month_1_score", "month_2_score", "month_3_score"],
                          add_queue=False)
            except:  # pragma: no cover
                pass

                #  print self.latest_score

        # update last_performance_review
        # try:
        #     from user_profile.models import Profile
        #
        #   #  Profile.objects.get(user_id=reviewer_id).update_last_performance_review()
        # except:
        #     pass

        return value

    def get_score(self, reviewer_id=None, month=None):

        '''
        TODO: Chia lam 4 loai (self.kpi_type_v2) va tinh diem cho day du cac truong hop
        '''

        kpi_type_v2 = self.kpi_type_v2()

        if self.assigned_to_id > 0 and kpi_type_v2 == kpi_trung_gian:
            assigned_kpi = self.get_assigned_kpi()

            '''
            assigned_kpi.assigned_to_id != self.user_id and assigned_kpi.assigned_to_id != assigned_kpi.user_id:
            This is to avoid infinity recursive
            '''

            if assigned_kpi and assigned_kpi.assigned_to_id != self.user_id and assigned_kpi.assigned_to_id != assigned_kpi.user_id:
                return assigned_kpi.get_score(month=month)

        if month is None:

            if kpi_type_v2 == kpi_cha_duoc_phan_cong or kpi_type_v2 == kpi_cha_normal:

                return self.latest_score
            else:

                if self.review_type == 'monthly' and self.score_calculation_type == "most_recent":

                    month_3_score = self.get_month_3_score()
                    if self.month_3 is not None and month_3_score >= 0:
                        self.latest_score = self.get_month_3_score()
                    else:
                        month_2_score = self.get_month_2_score()
                        if self.month_2 is not None and month_2_score >= 0:
                            self.latest_score = self.get_month_2_score()
                        else:
                            self.latest_score = self.get_month_1_score()

                            # if self.score_calculation_type == "most_recent":
                            #     pass
                            #
                            # if self.score_calculation_type == "average":
                            #     pass
                            #
                            # if self.score_calculation_type == "sum":
                            #    pass

            if is_nan(self.latest_score) or self.latest_score < 0 or self.latest_score is None:
                self.latest_score = 0

            return self.latest_score
        else:
            if month == 1:
                return self.get_month_1_score()
            elif month == 2:
                return self.get_month_2_score()
            elif month == 3:
                return self.get_month_3_score()

                # def get_score_m1(self):  # NOT WORKING --> not used
                #     if self.review_type == 'monthly' and self.score_calculation_type == 'most_recent':
                #
                #         score = self.get_month_1_score()
                #         if is_nan(score):
                #             return None
                #     else:
                #         score = self.get_score()
                #     return score

                # def get_score_m2(self, reviewer_id=None): --> Not used
                #     if self.review_type == 'monthly' and self.score_calculation_type == 'most_recent':
                #         score = self.get_month_2_score()
                #         if is_nan(score):
                #             return None
                #     else:
                #         score = self.get_score()
                #     return score

                # def get_score_m3(self, reviewer_id=None): --> Not used
                #     if self.review_type == 'monthly' and self.score_calculation_type == 'most_recent':
                #         score = self.get_month_3_score()
                #         if is_nan(score):
                #             return None
                #         return score
                #     else:
                #         score = self.get_score()

                # no more redis
                # REMOVE - 20-30-2016 by Hong
                #
                # rdis = redis.from_url(redis_url)
                # if reviewer_id != None:
                #     key = self.get_key(reviewer_id)
                #
                #     value = rdis.get(key)
                # else:
                #     keys = self.get_key_list()
                #     value = 0  # chỗ này chỉ lấy value lớn nhất thôi
                #     for key in keys:
                #         new_value = rdis.get(key)
                #         try:
                #             new_value = float(new_value)
                #         except:
                #             new_value = 0
                #
                #         if new_value and (new_value) > value:
                #             value = new_value
                #
                # if value:
                #     try:
                #         value = float(value)
                #     except ValueError:
                #         value = None
                #
                #         # print key
                #         # print value
                # return value

    # KPI

    # def get_final_score_percentage(self):     --> Not used
    #     return int(self.get_final_score())  # TODO: devide by 120

    # Lôi khi KPI có con mà chỉ kéo điểm KPI cha
    #     def get_final_score(self):
    #         score = 0
    #         if self.get_children().count() > 0:
    #             # neu Competency co children thi phai tinh diem lai
    #             return self.calculate_avg_score_allreviewers()
    #         else:
    #             # diem final cua 1 Competency bang trung binh cong cua diem tat ca cac reviewer da cham
    #             reviews = Review.objects.filter(reviewee_id=self.user_id, quarter_period_id=self.quarter_period_id)
    #             count = 0
    #             total_score = 0
    #             avg = 0
    #             for review in reviews:
    #                 score = self.get_score(review.reviewer_id)
    #                 if score:
    #                     total_score += score
    #                     count += 1
    #
    #             if count > 0:
    #                 avg = float(total_score) / count
    #
    #         return avg

    def get_final_score(self):

        return self.get_score()
        #
        # score = 0
        # # diem final cua 1 KPI bang trung binh cong cua diem tat ca cac reviewer da cham
        # reviews = Review.objects.only('reviewer_id').filter(reviewee=self.user,
        #                                                     quarter_period_id=self.quarter_period_id,
        #                                                     reviewer__isnull=False,
        #                                                     review_type__in=['manager', 'self'])
        # count = 0
        # total_score = 0
        # avg = 0
        # for reviewer in reviews:
        #     score = self.get_score()  # reviewer.reviewer_id)
        #     if score:
        #         total_score += score
        #         count += 1
        #
        # if count > 0:
        #     avg = total_score / float(count)
        #
        # return avg

    def reset_child_score(self, reviewer_id):
        for child in self.get_children():
            child.set_score(0, reviewer_id)

    def get_children(self):
        children = KPI.objects.filter(parent=self)
        # children = self.get_children_refer()
        # for c in children:
        #     c.weight = c.get_cascaded_from_weight()
        #     ### Important notes: this is just hot fix data, do not save KPI here.

        return children

    def get_children_sorted(self):
        children = self.get_children() \
            .select_related('assigned_to', 'refer_to', 'user') \
            .annotate(order_category=get_symble_category()) \
            .order_by('order_category', 'ordering')
        # kpis_ordered = sorted(children, key=lambda kpi: kpi_order_comp(kpi))
        return children

    def get_children_refer_sorted(self):
        children = self.get_children_refer() \
            .select_related('assigned_to', 'refer_to', 'user') \
            .annotate(order_category=get_symble_category()) \
            .order_by('order_category', 'ordering')
        # kpis_ordered = sorted(children, key=lambda kpi: kpi_order_comp(kpi))
        return children

    def get_children_samecat(self):
        children = KPI.objects.filter(parent=self, bsc_category=self.bsc_category)
        return children

    def get_children_refer(self):
        children = KPI.objects.filter(refer_to=self)

        #         children_toupdate_email = KPI.objects.filter(refer_to=self,
        #                                                      owner_email__isnull=True,
        #                                                      user__isnull=False)
        #
        #         # TODO: set quarter for kpis with no quarter
        #         children_quarter = KPI.objects.filter(refer_to=self,
        #                                               quarter_period__isnull=True).update(
        #             quarter_period_id=self.quarter_period_id)
        #
        #         for k in children_toupdate_email:
        #             if k.user:
        #                 k.owner_email = k.user.email
        #                 k.save()

        return children

    def is_has_child_refer(self):
        count = KPI.objects.filter(refer_to=self).count()
        return count

    def create_interchange_kpi(self, actor=None):
        if self.refer_to:
            if self.refer_to.owner_email == self.owner_email:  # phan cong cho chinh minh
                cascaded_from = self.cascaded_from
                self.parent = self.refer_to
                self.cascaded_from = None
                self.save()

                if cascaded_from \
                        and (
                        cascaded_from.id != self.refer_to.id or cascaded_from.id != self.id):  # pragma: no cover  # need check for case of invalid data structure
                    pass
                    # cascaded_from.delete()
            else:
                # Tao kpi con lam kpi trung gian
                self.assign_up(self.refer_to, force_run=True)  # this function already implicity that force_run=True.

    @classmethod
    def create_interchange_kpi_v2(cls, parent, child, to_user):
        # if child.user_id == to_user.id:
        #     return None
        # child_id=child.id
        k_temp = cls.objects.get(id=child.id)
        k_temp.pk = None
        interchange_k = k_temp  # .save()

        interchange_k.parent = parent
        interchange_k.assigned_to = to_user
        interchange_k.refer_to = None
        interchange_k.cascaded_from = None
        interchange_k.user = parent.user
        interchange_k.owner_email = parent.user.email if parent.user else interchange_k.owner_email

        interchange_k.save()

        child.refer_to = parent
        child.cascaded_from = interchange_k
        child.parent = None
        child.assigned_to = None
        child.user = to_user
        child.owner_email = to_user.email

        child.save()

        return interchange_k

        pass

    def get_children_visible(self):
        '''
        get child kpi, include directly and indirectly children, not interchange kpi
        :return:
        '''
        childs = KPI.objects.filter(refer_to=self, assigned_to__isnull=True)
        return childs

    def change_kpi_user(self, to_user, before_email=None, actor=None):
        '''
        # docs: https://cloudjet.atlassian.net/wiki/spaces/CKD/blog/2017/09/14/1634354/Psuedo+workflow+when+assign+kpi+to+user
        :param to_user:
        :param before_email:
        :return:
        '''
        # following cases not used, hence no cover
        if before_email and before_email == to_user.email:  # pragma: no cover
            return False
        if self.user.email == to_user.email:  # pragma: no cover
            return False

        kpi = self
        before_email = before_email
        userA = kpi.user
        userB = to_user
        kpi_type = kpi.kpi_type_v2()

        if kpi_type == kpi_con_normal:  # case1: kpi la con truc tiep
            kp = kpi.parent
            interchange_k = KPI.create_interchange_kpi_v2(kp, kpi, userB)
        elif kpi_type == kpi_cha_duoc_phan_cong:  # case 2: kpi la con duoc phan cong
            kp = kpi.refer_to
            if kpi.refer_to.user and kpi.refer_to.user == userB:  # case 2.1: kpi.refer_to.user==userB:
                if kpi.has_child_no_cache():  # case 2.1.1: kpi has child kc:
                    kpi.cascaded_from.delete()
                    kpi.cascaded_from = None  # todo: check the value after delete the cascaded_from
                    kpi.user = userB
                    kpi.parent = kp
                    kpi.save()

                    children = kpi.get_children_visible()
                    for kc in children:
                        if kc.user == userB:  # case 2.1.1.1: kc.user==userB:
                            kc.cascaded_from.delete()
                            kc.cascaded_from = None
                            kc.parent = kpi
                            kc.save()  # todo: check the needed
                        elif kc.user == userA:  # case 2.1.1.2: kc.user==userA ( kc.user!=userB)
                            interchange_k = KPI.create_interchange_kpi_v2(kpi, kc, userA)
                        else:  # case 2.1.1.3: kc.user=userC
                            cascaded_from = kc.cascaded_from
                            cascaded_from.user = userB
                            cascaded_from.owner_email = userB.email
                            cascaded_from.save()

                else:  # case 2.1.2: kpi has no child:
                    kpi.cascaded_from.delete()
                    kpi.cascaded_from = None
                    kpi.user = userB
                    kpi.parent = kp
                    kpi.save()
            elif kpi.refer_to.user and kpi.refer_to.user != userB:  # case 2.2: kpi.refer_to.user!=userB:
                kp = kpi.refer_to
                userC = kp.user

                if kpi.has_child_no_cache():  # case 2.2.1: kpi has child kc:
                    kpi.user = userB
                    kpi.owner_email = userB.email
                    kpi.save()

                    children = kpi.get_children_visible()
                    for kc in children:
                        if kc.user == userB:  # case 2.2.1.1: kc.user==userB:
                            kc.cascaded_from.delete()
                            kc.parent = kpi
                            kc.save()
                        elif kc.user == userA:  # case 2.2.1.2: kc.user==userA ( kc.user!=userB)
                            interchange_k = KPI.create_interchange_kpi_v2(kpi, kc, userA)
                        else:  # case 2.2.1.3: kc.user=userD
                            cascaded_from = kc.cascaded_from
                            cascaded_from.user = userB
                            cascaded_from.owner_email = userB.email
                            cascaded_from.save()
                else:  # case 2.2.2: kpi has no child:
                    kpi.user = userB
                    kpi.owner_email = userB.email
                    kpi.save()

        elif kpi_type == kpi_cha_normal:  # case 3: kpi la kpi cha (khong la con cua kpi khac)
            if kpi.has_child_no_cache():  # case 3.1: kpi has child: kc
                kpi.user = userB
                kpi.owner_email = userB.email
                kpi.save()

                children = kpi.get_children_visible()
                for kc in children:
                    if kc.user == userA:  # case 3.1.1: kc.user == userA:
                        interchange_k = KPI.create_interchange_kpi_v2(kpi, kc, userA)
                    elif kc.user == userB:  # case 3.1.2: kc.user == userB
                        kc.cascaded_from.delete()
                        kc.parent = kpi
                        kc.cascaded_from = None
                        kc.save()
                    else:  # case 3.1.3: kc.user == userC:
                        kc.cascaded_from.user = userB
                        kc.cascaded_from.owner_email = userB.email
                        kc.cascaded_from.save()

            else:  # case 3.2: kpi has no child:
                kpi.user = userB
                kpi.owner_email = userB.email
                kpi.save()

        return self

    def get_kpi_id_with_cat(self):
        return self.get_prefix_category() + self.get_kpi_id()

    def get_kpi_id(self):

        return get_kpi_id(self)

        # order = self.ordering
        # if not order:
        #     # order = "01"
        #     order = ""
        # else:
        #     order = str(int(order)).zfill(2)
        #     # print "order = str(int(order)).zfill(2) --> {0}".format(order)
        #
        # return self.get_prefix_id() + order

    def get_children_refer_order(self):

        return get_children_refer_order(self)

        # children = KPI.objects.filter(refer_to=self).select_related('refer_to', 'cascaded_from', 'user').order_by(
        #     'ordering')
        #
        # c = sorted(children, key=lambda x: x.get_kpi_id())
        #
        # return c

    def has_child(self):
        key = "has_child{}".format(self.id)
        if cache.get(key, None) is not None:
            return cache.get(key, False)

        result = KPI.objects.filter(refer_to=self).exists()

        cache.set(key, result, 60)

        return result

    def has_child_no_cache(self):
        result = KPI.objects.filter(refer_to=self).exists()
        return result

    def get_children_order(self):
        children = KPI.objects.filter(parent=self).order_by('id')
        return children

    def get_kpi_notes(self, limit=None):
        kwargs = {}
        kwargs['created_at__gte'] = self.created_at if self.created_at else datetime.datetime(2013, 1, 1, 0, 0)
        notes = KpiComment.objects.filter(kpi_unique_key=self.get_unique_key(),
                                          **kwargs) \
            .values('user__profile__avatar',
                    'user__profile__display_name',
                    'content',
                    'created_at',
                    'file').order_by('-created_at')

        total = notes.count()
        if limit and total > 3:
            notes = notes[:3]

        results = []
        media_url = u"http://%s/" % (settings.AWS_S3_CUSTOM_DOMAIN,)
        for note in notes:
            try:
                file_url = media_url + note['file'] if note['file'] else ''
                file_name = os.path.basename(note['file'])
            except:
                file_url = ''
                file_name = ''
            item = {
                'avatar': media_url + unicode(note['user__profile__avatar']),
                'display_name': unicode(note['user__profile__display_name']),
                'content': note['content'],
                'view_date': note['created_at'].isoformat(),
                'created_at': format(note['created_at'], 'Y-m-d H:i:s'),
                'file_name': file_name,
                'file_url': file_url
            }
            results.append(item)

        return {'total': total if total else 0, 'results': results}

    def get_kpi_notes_list(self, to_string=True):
        kwargs = {}
        kwargs['created_at__gte'] = self.created_at if self.created_at else datetime.datetime(2013, 1, 1, 0, 0)
        notes = KpiComment.objects.filter(kpi_unique_key=self.get_unique_key(),
                                          # user__profile__display_name='CJS Support',
                                          **kwargs) \
            .values('user__profile__display_name',
                    'content').order_by('-created_at')
        if to_string:
            data = []
            for note in notes:
                if note['user__profile__display_name'] and note['content']:
                    data.append(note['user__profile__display_name'] + ": " + note['content'])

            if data:
                return ' - ' + "\n - ".join(data)
            return ''
        else:
            return notes

    def count_evidence_month_1(self):
        return KpiComment.objects.filter(kpi=self, month=1).count()

    def count_evidence_month_2(self):
        return KpiComment.objects.filter(kpi=self, month=1).count()

    def count_evidence_month_3(self):
        return KpiComment.objects.filter(kpi=self, month=1).count()

    def get_notes(self):
        kwargs = {}
        # kwargs['created_at__gte'] = self.created_at if self.created_at else datetime.datetime(2013, 1, 1, 0, 0)
        notes = KpiComment.objects.filter(kpi=self, month=None, **kwargs).order_by('-id')
        #        Comment bi lap lai nen xoa doan nay
        #         if notes.count() == 0:
        #             if self.outcome_notes_self is not None and self.outcome_notes_self != '':
        #                 kc = KpiComment(user_id=self.user_id, content=self.outcome_notes_self, send_from_ui='auto', kpi=self, kpi_unique_key=self.get_unique_key())
        #                 kc.save()
        #             if self.outcome_notes is not None and self.outcome_notes != '':
        #                 u = self.user.get_profile().report_to_id
        #                 if u:
        #                     kc = KpiComment(user_id=u, content=self.outcome_notes, send_from_ui='auto', kpi=self, kpi_unique_key=self.get_unique_key())
        #                     kc.save()
        return notes

    def get_notes_intemplate(self):
        notes = self.get_notes()

        context = {
            'notes': notes,
            'unique_key': self.get_unique_key(),
            'kpi_id': self.id
        }
        content = render_to_string('kpi_modules/notes.html', context)
        return content

    def get_notes_intemplate3(self):
        notes = self.get_notes()

        context = {
            'notes': notes,
            'unique_key': self.get_unique_key(),
            'kpi_id': self.id,
            'STATIC_URL': settings.STATIC_URL
        }
        content = render_to_string('kpi_modules/notes_3.html', context)
        return content

    def get_children_dict(self, person, shared=False, reviewer=None):
        from user_profile.templatetags.user_tags import display_name

        data = []
        for c in self.get_children():
            item = c.kpi_dict()
            #
            # #TODO: standardize to avoid repeat code here
            # item = {}
            # item['id'] = c.id
            # item['name'] = c.name
            # item['weight'] = c.weight
            # item['current_goal'] = c.current_goal
            # item['future_goal'] = c.future_goal
            # # item['get_notes_intemplate'] = c.get_notes_intemplate()
            # notes = c.get_kpi_notes(3)
            # item['notes'] = notes['results']
            # item['total_notes'] = notes['total']
            # item['outcome_notes'] = c.outcome_notes
            # item['outcome_notes_self'] = c.outcome_notes_self
            # item['operator'] = c.operator
            # item['unique_key'] = c.unique_key
            # item['target'] = c.target
            # item['real'] = c.real
            # item['unit'] = c.unit
            # item['ordering'] = c.ordering
            # item['score'] = None
            # item['self_confirmed'] = c.self_confirmed
            # item['manager_confirmed'] = c.manager_confirmed
            # item['hash'] = c.get_hash()
            # item['month_1'] = c.month_1
            # item['month_2'] = c.month_2
            # item['month_3'] = c.month_3
            # item['score_calculation_type'] = c.score_calculation_type
            # item['review_type'] = c.review_type or ''
            if c.end_date:
                item['deadline'] = format(c.end_date, "d-m-Y")
            else:
                item['deadline'] = None

            if c.assigned_to_id:
                item['assigned_to'] = {'user_id': c.assigned_to_id,
                                       'full_name': display_name(c.assigned_to_id)}
            else:
                item['assigned_to'] = None

            # try:
            #     history = ScoreHistory.objects.get(obj_id=c.id,
            #                                        type='kpi')
            #     history = history.history
            # except:
            history = []

            item['histories'] = history

            if reviewer:
                score = c.get_score()  # c.user_id)
                item['score'] = score if not is_nan(score) else 0

            item['self_score'] = None
            if c.user_id:
                self_score = c.get_score()  # c.user_id)
                item['self_score'] = self_score if not is_nan(self_score) else 0

            # if person == 'self' and not shared:
            #                 item['self_score'] = None
            #                 item['outcome_notes_self'] = None
            #
            #             if person == "manager" and not shared:
            #                 item['score'] = None
            #                 item['outcome_notes'] = None

            data.append(item)
        return data

    def get_category_order(self):
        if self.bsc_category == "financial":
            return 1
        elif self.bsc_category == "customer":
            return 2
        elif self.bsc_category == "internal":
            return 3
        elif self.bsc_category == "learninggrowth":
            return 4
        else:
            return 5

    def get_prefix_id(self):
        if self.prefix_id:
            return self.prefix_id
        else:
            return ""

    def get_prefix_category(self,):
        # prefix_id = ""
        # if self.prefix_id:
        #     prefix_id = self.prefix_id

        if self.bsc_category == "financial":
            return "F"  # + prefix_id
        elif self.bsc_category == "customer":
            return "C"  # + prefix_id
        elif self.bsc_category == "internal":
            return "P"  # + prefix_id
        elif self.bsc_category == "learninggrowth":
            return "L"  # + prefix_id
        else:
            return "O"  # + prefix_id

    def to_json_pure(self):
        # temp = serializers.serialize('json', [self])
        js = json.loads(serializers.serialize('json', [self]))[0]
        j = js['fields']

        j.pop('outcome_notes', None)
        j.pop('quarter_period', None)
        return j

    def to_json_nochildren(self, actor=None):
        # temp = serializers.serialize('json', [self])

        if self.actor:  # pragma: no cover
            actor = self.actor

        js = json.loads(serializers.serialize('json', [self]))[0]

        # TODO: limit fields to be exposed
        j = js['fields']

        j.pop('outcome_notes', None)
        j.pop('group_kpi')

        j['id'] = js['pk']
        j['prefix_id'] = self.get_kpi_id_with_cat()
        j['kpi_id'] = self.get_prefix_category() + self.get_kpi_id()

        j['children'] = []  # self.name_mindmap()

        j['get_month_1_score'] = self.get_month_1_score()
        j['get_month_2_score'] = self.get_month_2_score()
        j['get_month_3_score'] = self.get_month_3_score()

        j['get_month_1_score_icon'] = self.get_month_1_score_icon()
        j['get_month_2_score_icon'] = self.get_month_2_score_icon()
        j['get_month_3_score_icon'] = self.get_month_3_score_icon()
        j['get_score_icon'] = self.get_score_icon()

        j['year_data'] = self.year_data

        if self.assigned_to_id > 0:
            try:
                j['assigned_to_email'] = self.assigned_to.email
            except User.DoesNotExist:  # pragma: no cover
                pass

        j['kpi_refer_group'] = self.get_kpi_refer_group()
        j['has_child'] = self.has_child()
        j['group_kpi'] = self.group_kpi_id
        j['prefix_category'] = self.get_prefix_category()

        if actor:
            enable_edit = has_perm(KPI__EDITING, actor, self.user)
            enable_review = has_perm(KPI__REVIEWING, actor, self.user, self)
            j['enable_edit'] = enable_edit
            j['enable_review'] = enable_review
            j['enable_delete'] = has_perm(DELETE_KPI, actor, self.user, self)

            # If 360 degree review review ( self.reviewer_email != None)
            # Then only reviewer can review
            #
            # if self.reviewer_email and self.reviewer_email != actor.email:
            #     j['enable_review'] = False

        return j

    def get_kpi_signature(self):
        '''
        get signature of a kpi to check duplicated kpi
        :param kpi:
        :return:
        '''

        return "{0}|{1}|{2}|{3}|{4}|{5}|{6}".format(self.name,
                                                    self.unit,
                                                    self.unique_code,
                                                    self.quarter_one_target,
                                                    self.quarter_two_target,
                                                    self.quarter_three_target,
                                                    self.quarter_four_target
                                                    )

    def update_unique_code_with_same_signature(self):
        KPI.objects.filter(refer_to_id=self.refer_to_id,
                           name=self.name,
                           quarter_one_target=self.quarter_one_target,
                           quarter_two_target=self.quarter_two_target,
                           quarter_three_target=self.quarter_three_target,
                           quarter_four_target=self.quarter_four_target,

                           ).exclude(unique_code=self.unique_code).update(unique_code=self.unique_code)

    def to_json_excel(self, actor=None, exclude_duplicated_child=False):
        j = self.to_json_nochildren(actor)
        j['name_mindmap'] = self.name_mindmap()

        duplicated_children = {}

        for c in self.get_children_refer_sorted():

            if exclude_duplicated_child:

                if duplicated_children.get(c.get_kpi_signature(), None) is None:
                    duplicated_children[c.get_kpi_signature()] = True
                else:
                    continue

            c.weight = c.get_cascaded_from_weight()
            j['children'].append(c.to_json(actor=actor, include_children=False))
        return j

    # ## please note:
    # ## to_json will choose option to return either children only or children refer
    # ## when our refactor finished, this function will automatically work properly
    def to_json(self, actor=None, refer=False, include_children=True):
        if refer:
            return self.to_json_excel()
        else:
            j = self.to_json_nochildren(actor)
            j['name_mindmap'] = self.name_mindmap()

            if include_children:

                for c in self.get_children_sorted():
                    j['children'].append(c.to_json(actor=actor))

            return j

    def get_target(self):
        if self.target is None:
            self.update_targets_from_quarter()

        return self.target

    def get_monthly_month(self, month=None):

        _month = None

        if month in (1, 2, 3):
            _month = month
        else:  # get current month
            if self.month_3 is not None:
                _month = 3
            elif self.month_2 is not None:
                _month = 2
            else:
                _month = 1

        return _month

    def get_current_month_target(self, month=None):
        if self.score_calculation_type == "most_recent":
            # if month != None:
            #     if month == 1:
            #         return self.get_month_1_target()
            #
            #     if month == 2:
            #         return self.get_month_2_target()
            #
            #     if month == 3:
            #         return self.get_month_3_target()
            # else:
            #
            #     if self.month_3 is not None:
            #         return self.get_month_3_target()
            #     elif self.month_2 is not None:
            #         return self.get_month_2_target()
            #     else:
            #         return self.get_month_1_target()

            _month = self.get_monthly_month(month)
            # https://stackoverflow.com/questions/3061/calling-a-function-of-a-module-by-using-its-name-a-string
            get_target_method = getattr(self, 'get_month_{}_target'.format(_month))
            return get_target_method()

        return self.target

    def get_current_month_real(self, month=None):
        if self.score_calculation_type == "most_recent":

            # if month != None:
            #     if month == 1:
            #         return self.month_1
            #
            #     if month == 2:
            #         return self.month_2
            #
            #     if month == 3:
            #         return self.month_3
            # else:
            #     if self.month_3 is not None:
            #         return self.month_3
            #     elif self.month_2 is not None:
            #         return self.month_2
            #     elif self.month_1 is not None:
            #         return self.month_1
            #     else: ## (**)
            #         return self.real
            #
            _month = self.get_monthly_month(month)
            # for (**)
            if _month == 1 and self.month_1 is None:
                return self.real
            else:
                return getattr(self, 'month_{}'.format(_month))

        return self.target

    def to_json_dump(self, actor=None):
        # j = self.to_json(actor)
        j = self.to_json_nochildren(actor)
        j['get_target'] = self.get_target()

        if self.refer_to_id > 0 and self.kpi_type() == kpi_cha_duoc_phan_cong:
            j['refer_name'] = self.refer_to.name

        # del j['target']
        #   x =  json.load(json.dumps(j), object_pairs_hook=deunicodify_hook)
        return json.dumps(j)

    def to_dict(self):
        return self.kpi_dict()

    def kpi_dict(self, person=None, shared=False, reviewer=None):
        score = None
        # if reviewer: #FUCK
        score = self.get_score()  # self.user_id)

        self_score = None
        # if self.user_id: FUCK
        self_score = self.get_score()  # self.user_id)

        # if person == 'self' and not shared:
        #             self_score = None
        #
        #         if person == "manager" and not shared:
        #             score = None

        # try:
        # history_item = ScoreHistory.objects.filter(obj_id=self.id,
        #                                            type='kpi').first()
        history = []
        # if history_item:
        #     history = history_item.history
        # except Exception as e:
        #     notify_slack(SLACK_ERROR_CHANNEL, 'history = ScoreHistory.objects.get(obj_id=self.id: error: {0}'.format(str(e)))

        total_weight = self.get_children().aggregate(Sum('weight'))
        notes = self.get_kpi_notes(3)

        data = {
            'id': self.id,
            'name': self.name,
            'description': self.description,
            'score': score,
            'self_score': self_score if (not is_nan(self_score)) else 0,
            'total_weight': total_weight['weight__sum'],
            'children': self.get_children_dict(person, shared, reviewer) if person else [],
            'weight': self.weight,
            'temp_weight': self.temp_weight,
            'ordering': self.ordering,
            'histories': history,
            'operator': self.operator,
            'target': self.target,
            'unit': self.unit,
            'real': self.real if (not is_nan(self.real)) else 0,
            'deadline': format(self.end_date, 'd-m-Y') if self.end_date else None,
            'cascaded_from': self.cascaded_from_id,
            'category': self.bsc_category,
            'category_order': self.get_category_order(),
            'self_confirmed': self.self_confirmed,
            'manager_confirmed': self.manager_confirmed,
            'unique_key': self.unique_key,
            'notes': notes['results'],
            'total_notes': notes['total'],
            'is_postponed': self.status == "postponed",
            'current_goal': self.current_goal,
            'future_goal': self.future_goal,
            'hash': self.get_hash(),
            'month_1': self.month_1,
            'month_2': self.month_2,
            'month_3': self.month_3,
            'score_calculation_type': self.score_calculation_type,
            'cascaded_from_deleted': self.cascaded_from_deleted(),

            'review_type': self.review_type or ''
        }
        return data

    def cascaded_from_deleted(self):
        if self.cascaded_from_id > 0:
            cascaded_from = KPI.objects.filter(id=self.cascaded_from_id).first()
            if cascaded_from:
                return False
            else:
                return True
        else:
            return False

    def newbie_to_json(self):
        from user_profile.templatetags.user_tags import display_name

        if self.parent_id:
            if self.assigned_to_id:
                assigned_to = {'user_id': self.assigned_to_id,
                               'full_name': display_name(self.assigned_to_id)}
            else:
                assigned_to = None

            return {
                "name": self.name,
                "weight": self.weight,
                "outcome_notes": "",
                "future_goal": "",
                "outcome_notes_self": '',
                "score": None,
                "current_goal": self.current_goal,
                "weight": self.weight,
                "self_score": None,
                "id": self.id,
                'ordering': self.ordering,
                'operator': self.operator,
                'target': self.target,
                'real': self.real,
                'unit': self.unit,
                'deadline': format(self.end_date, 'd-m-Y') if self.end_date else None,
                'notes': [],
                'total_notes': 0,
                'histories': [],
                'unique_key': self.get_unique_key(),
                'self_confirmed': self.self_confirmed,
                'manager_confirmed': self.manager_confirmed,
                'cascaded_from': None,
                'assigned_to': assigned_to,
                'hash': self.get_hash(),
                'review_type': self.review_type or ''
            }
        else:
            total_weight = self.get_children().aggregate(Sum('weight'))
            return {
                "description": self.description,
                "score": None,
                "total_weight": total_weight['weight__sum'],
                "self_score": None,
                "weight": self.weight,
                "id": self.id,
                'histories': [],
                "name": self.name,
                'ordering': self.ordering,
                'children': [],
                'operator': self.operator,
                'target': self.target,
                'real': self.real,
                'unit': self.unit,
                'deadline': format(self.end_date, 'd-m-Y') if self.end_date else None,
                'cascaded_from': self.cascaded_from_id,
                'unique_key': self.get_unique_key(),
                'notes': [],
                'total_notes': 0,
                'self_confirmed': self.self_confirmed,
                'manager_confirmed': self.manager_confirmed,
                'category': self.bsc_category,
                "category_order": self.get_category_order(),
                'hash': self.get_hash(),
                'review_type': self.review_type or ''
            }

    def get_next_ordering(self):
        count = 1
        if self.parent_id:
            ordering = self.parent.get_children().aggregate(ordering=Max('ordering'))
            count = self.parent.get_children().count()

        elif self.refer_to:
            ordering = KPI.objects.filter(
                refer_to=self.refer_to

            ).aggregate(ordering=Max('ordering'))
            count = KPI.objects.filter(refer_to=self.refer_to).count()

            # return ordering + 1
        else:
            ordering = KPI.objects.filter(user_id=self.user_id,
                                          parent=None,
                                          quarter_period_id=self.quarter_period_id,
                                          bsc_category=self.bsc_category) \
                .aggregate(ordering=Max('ordering'))

            count = KPI.objects.filter(user_id=self.user_id,
                                       parent=None,
                                       quarter_period_id=self.quarter_period_id,
                                       bsc_category=self.bsc_category).count()

        if ordering['ordering'] and count > 1:
            return ordering['ordering'] + 1
        else:
            return 1

            # reversion.register(KPI)

    # def get_create_kpi_data(self):    --> Not used
    #     datas = KPIData.objects.filter(kpi=self).order_by('-date')
    #     date = datetime.datetime.now().date()
    #
    #     if datas.count() == 0:
    #         if self.score_calculation_type == 'monthly':
    #
    #             month1 = KPIData(kpi=self)
    #             month2 = KPIData(kpi=self)
    #             month3 = KPIData(kpi=self)
    #
    #             if self.quarter_period.year == 0:
    #                 self.quarter_period.year = datetime.datetime.now().year
    #                 self.quarter_period.save()
    #
    #             if self.quarter_period.quarter == 1:
    #                 month1 = date.replace(month=1, day=1, year=self.quarter_period.year)
    #                 month2 = date.replace(month=2, day=1, year=self.quarter_period.year)
    #                 month3 = date.replace(month=3, day=1, year=self.quarter_period.year)
    #             elif self.quarter_period.quarter == 2:
    #                 month1 = date.replace(month=4, day=1, year=self.quarter_period.year)
    #                 month2 = date.replace(month=5, day=1, year=self.quarter_period.year)
    #                 month3 = date.replace(month=6, day=1, year=self.quarter_period.year)
    #             elif self.quarter_period.quarter == 3:
    #                 month1 = date.replace(month=7, day=1, year=self.quarter_period.year)
    #                 month2 = date.replace(month=8, day=1, year=self.quarter_period.year)
    #                 month3 = date.replace(month=9, day=1, year=self.quarter_period.year)
    #             else:
    #
    #                 month1 = date.replace(month=10, day=1, year=self.quarter_period.year)
    #                 month2 = date.replace(month=11, day=1, year=self.quarter_period.year)
    #                 month3 = date.replace(month=12, day=1, year=self.quarter_period.year)
    #
    #             month1.save()
    #             month2.save()
    #             month3.save()
    #
    #             datas = KPIData.objects.filter(kpi=self).order_by('-date')
    #
    #     return datas

    # def score_calculate_from_data(self):
    #     datas = KPIData.objects.filter(kpi=self).order_by('-date')
    #     score = 0
    #
    #     if datas.count() > 0:
    #         if self.score_calculation_type == 'sum':
    #
    #             for d in datas:
    #                 score += d.value
    #         elif self.score_calculation_type == 'average':
    #
    #             i = 0
    #             for d in datas:
    #                 score += d.value
    #                 i += 1
    #
    #             if i == 0:
    #                 i = 1
    #
    #             score = float(score) / float(i)
    #         else:
    #             score = datas[0]
    #
    #         self.real = score
    #         self.update_score()

    def error_log(self, content, error_type):
        er = KPIErrorLog()
        er.content = content
        er.error_type = error_type
        er.save()
        return er

    def get_bsc_category(self):
        if self.bsc_category:
            return self.bsc_category
        else:
            return 'financial'

    # @postpone_process
    @postpone
    def fix_self_threading(self):  # pragma: no cover
        logger.info("*" * 10 + "fix_self_threading is called  : ID = {}".format(self.id))
        return self.fix_self()

    def fix_self(self, use_cache=True, add_queue=True):  # pragma: no cover

        logger.info("*" * 10 + "fix_self is called  : ID = {}".format(self.id))

        try:
            org = self.user.profile.get_organization()
        except:
            org = None

        # creating_assessment_org_id = cache.get(CREATING_ASSESSMENT_ORG.format(org_id))
        # if not creating_assessment_org_id:
        #     creating_assessment_org_id = get_redis(CREATING_ASSESSMENT_ORG.format(org_id))
        #
        # if creating_assessment_org_id and creating_assessment_org_id == org_id:
        #     return CREATING_ASSESSMENT_ORG.format(org_id)

        if org is not None and not org.is_ready():
            return org.id

        logger.info("start fix_self : ID = {}".format(self.id))
        # return "creating_assessment"

        key = 'fix_kpi_self_' + str(self.id)

        caching = cache.get(key)
        if not caching:
            caching = get_redis(key)

        if caching and use_cache:
            # print 'in cached'
            return caching

        caching = cache.set(key, True, 60 * 60 * 5)  # caching 5 hours or being deleted
        set_redis(key, True, 60)

        #  r = random.randint(1, 100)

        type = self.kpi_type_v2()

        is_error = False

        if not self.user:
            return True

        current_quarter = self.user.profile.get_current_quarter()
        is_error = fix_target_not_synced(self, current_quarter)

        '''
        Fix ≥ and ≤
        '''
        if self.operator == "≤":
            self.operator = "<="
            self.save(update_fields=['operator'])
        if self.operator == "≥":
            self.operator = ">="
            self.save(update_fields=['operator'])

        if self.operator and self.operator != self.operator.strip():
            self.operator = self.operator.strip()
            self.save(update_fields=['operator'])

        if self.operator != ">=" and self.operator != "<=" and self.operator != "=":
            self.operator = ">="
            self.save(update_fields=['operator'])

        '''
        Fix for the case KPI.quarter_period == null
        '''
        if not self.quarter_period_id:
            self.quarter_period = current_quarter
            try:
                self.save(update_fields=['quarter_period_id'], force_update=True)
            except DatabaseError:
                pass

        # fix recursive structure
        if self.refer_to is self:
            if self.cascaded_from and self.cascaded_from.parent and self.cascaded_from.parent is not self:
                self.refer_to = self.cascaded_from.parent
                self.save(update_fields=['refer_to', ])

        if type == kpi_cha_normal:
            # <fixDuplicate>

            dups = KPI.objects.filter(user=self.user, quarter_period=self.quarter_period, name=self.name,
                                      refer_to_id__gt=0).exclude(id=self.id).exclude(parent_id__gt=0)
            for d in dups:
                print "performance\models.py error-line 3762"

                if d.get_children().count() == 0:
                    self.assign_up(
                        d.refer_to)  # # in fix_self, we only fix the broken structure, not fix for the logic here!

                    print "d before delete {0} {1}".format(d, d.id)
                    d.cascaded_from.delete()
                    d.delete()
                    print "d after delete {0} {1}".format(d, d.id)
                    print "performance\models.py error-line 3768"
                    is_error = True
            # </fixDuplicate>
            pass

        elif type == kpi_cha_duoc_phan_cong:

            if self.id == self.refer_to_id:
                self.refer_to_id = None
                print "error-line 3830"
                self.save(update_fields=['refer_to_id'])

            is_error = fix_kpi_cha_duoc_phan_cong_wrong_email(self)

            # KPI cascaded_from bị deleted
            cascaded_from = None
            try:
                cascaded_from = self.cascaded_from
            except KPI.DoesNotExist:
                pass

            if cascaded_from == None:

                if self.refer_to_id:

                    refer_to = None
                    try:
                        refer_to = self.refer_to
                    except KPI.DoesNotExist:
                        pass

                    if refer_to:
                        self.assign_up(refer_to,
                                       force_run=True)  # in fix_self, we only fix the broken structure, not fix for the logic here!
                        is_error = True

            elif self.refer_to != cascaded_from.parent:
                self.refer_to = cascaded_from.parent
                self.save()
                is_error = True

        elif type == kpi_trung_gian:
            # print 'here'

            '''
            Mỗi KPI Trung Gian chỉ được có 1 KPI được phân công.
            Nếu dư thì remove ra
            '''

            assigned_kpis = KPI.objects.filter(cascaded_from_id=self.id)
            if assigned_kpis.count() > 1:
                assigned_kpis = list(assigned_kpis)
                assigned_kpis.pop()  # remove one element
                for assigned_k in assigned_kpis:
                    assigned_k.cascaded_from = None
                    assigned_k.save()
                print "error-line 3825"
                is_error = True

            assigned_kpi = self.get_assigned_kpi()

            '''
            KPI Duoc phan cong bi detect nhầm là KPI Trung gian
            '''
            if assigned_kpi and assigned_kpi.kpi_type_v2() == kpi_trung_gian:
                '''
                assigned_kpi phai co parent = None
                '''
                assigned_kpi.parent_id = None
                assigned_kpi.refer_to_id = self.parent_id
                assigned_kpi.save(update_fields=['parent_id', 'refer_to_id'])

            if self.refer_to_id:  # --> bug: kpi trung gian thi refer_to_id = None

                err = ('bug: kpi trung gian thi refer_to_id must be None - ID = {}').format(self.id)
                #  print err

                self.error_log(err, 'kpi_trung_gian:refer_to_id__must_be_None')
                # print err

                # notify_slack(settings.SLACK_CRONJOB_CHANNEL, '`kpi_trung_gian:refer_to_id__must_be_None`' + " " + err)

                if not assigned_kpi.refer_to_id:
                    assigned_kpi.refer_to_id = self.parent_id
                    assigned_kpi.save(update_fields=['refer_to_id'])

                self.refer_to_id = None
                self.save(update_fields=['refer_to_id'])
                print "error-line 3813"
                is_error = True

            # check if KPI have multiple assigned_kpi --> error

            if assigned_kpi and assigned_kpi.user != self.assigned_to:
                self.assigned_to = assigned_kpi.user
                self.save()
                print "error-line 3831"
                is_error = True

            # kpi_trung_gian wrong user
            if self.parent and self.user != self.parent.user:
                self.user = self.parent.user
                self.owner_email = self.parent.user.email
                self.save()
                print "error-line 3839"
                is_error = True

            '''
            Score not updated
            '''

            # Hong edit 7/3: no need because self.get_score() = assigned_kpi.get_score()
            # if assigned_kpi and assigned_kpi.latest_score != self.latest_score:
            #     self.latest_score = assigned_kpi.latest_score
            #     self.save()
            #     print "error-line 3849"
            #     is_error = True

            '''
            GIVEN:  kpi trung gian nay lai co them kpi con la : kpi_con1 va kpi_con2
            THEN: delete kpi_con1 va kpi_con2
            '''
            children = self.get_children()
            if children.count() > 0:
                for c in children:
                    cascaded_k = c.get_assigned_kpi()
                    if cascaded_k:
                        cascaded_k.delete()
                    c.delete()
                print "error-line 3864"
                is_error = True

            '''
            GIVEN: kpi_trung_gian co assigned_kpi.weight == 0
            THEN: self.weight = 0
            '''
            if assigned_kpi and assigned_kpi.weight == 0 and self.weight > 0:
                self.temp_weight = self.weight
                self.weight = 0
                self.save()
                print "error-line 3875"
                is_error = True

            '''
            GIVEN:  kpi_trung_gian co assigned_kpi.refer_to != self.parent
            THEN: assigned_kpi.refer_to == self.parent
            '''
            if assigned_kpi and assigned_kpi.refer_to != self.parent:
                assigned_kpi.refer_to_id = self.parent_id
                assigned_kpi.save()
                print "error-line 3885"
                is_error = True

            '''
                GIVEN:  kpi_trung_gian.weight == 0 and assigned_kpi.weight > 0
                THEN: toggle_weight(kpi_trung_gian.weight)
            '''
            if self.weight == 0 and assigned_kpi.weight > 0:
                self.delay_toggle(recursive=False, force_to_state='active')
                is_error = True

        elif type == kpi_con_normal:
            # print type

            is_error = fix_children_no_refer_no_assign(self) or \
                       fix_children_refer_not_equal_parent(self) or \
                       fix_children_wrong_type(self) or \
                       fix_children_assigned(self)

        children = KPI.objects.filter(parent=self)
        for c in children:
            c.fix_self()

        assigned_kpis = KPI.objects.filter(refer_to=self)
        for a in assigned_kpis:
            a.fix_self()

        if self.cascaded_from_id > 0 and KPI.objects.filter(cascaded_from_id=self.cascaded_from_id).exists():
            self.cascaded_from.fix_self()

        if not self.owner_email and self.user_id:
            self.owner_email = self.user.email
            self.save()

        if is_error and add_queue:  # fix score
            from performance.services import add_new_cascadeKPIQueue

            add_new_cascadeKPIQueue(self)

        '''
        FIX GROUP_KPI.MAP is NONE
        '''
        group_kpi = self.group_kpi  # fix for case map has been deleted
        if group_kpi:
            group_kpi = reload_model_obj(self.group_kpi)
        # ## fix added in GroupKPI.save()
        # from performance.services.strategymap import get_user_current_map
        # if group_kpi is not None and group_kpi.map is None:
        #     map = get_user_current_map(self.user)
        #     group_kpi.map = map
        #     group_kpi.save()
        '''
        FIX monthly target
        '''
        if self.target is not None:
            if self.month_1_target is None:
                if self.score_calculation_type == 'sum':
                    self.month_1_target = round(float(self.target) / 3, 2)
                    self.save()
                else:
                    self.month_1_target = self.target
                    self.save()

            if self.month_2_target is None:
                if self.score_calculation_type == 'sum':
                    self.month_2_target = round(float(self.target) / 3, 2)
                    self.save()
                else:
                    self.month_2_target = self.target
                    self.save()

            if self.month_3_target is None:
                if self.score_calculation_type == 'sum':
                    self.month_3_target = round(float(self.target) / 3, 2)
                    self.save()
                else:
                    self.month_3_target = self.target
                    self.save()
        #
        # # Khang add this to fix_self
        # if self.group_kpi is None: # kpi must have its own group KPI
        #     self.get_group()

        return is_error
        #
        # def add_cascadeKPIQueue(self):
        #     add_new_cascadeKPIQueue(self)

    @classmethod
    def kpi_debug_info(cls, kpi_id):
        # for full kpi data
        # try with url:
        # /api/kpi/?{param=value}
        # use one of params:user_id, kpi_id, parent_id, category
        k = KPI.objects.filter(id=kpi_id).first()
        if not k:
            data = {'msg': 'NOT FOUND!'}
        else:
            data = {
                'full-kpi-data': '''
                                # for full kpi data
                                # try with url:
                                # /api/kpi/?{param=value}
                                # use one of params:user_id, kpi_id, parent_id, category
                                ''',
                'id': k.id,
                'name': k.name,
                'kpi_type_v2': k.kpi_type_v2(),
                'kpi_type': k.kpi_type(),
                'kpi_type_error': k.kpi_type_error(),
                'created_at': k.created_at,
                'log_trace': k.log_trace,

            }

        return data


# class KPI(models.Model):
# class KPI(PermanentModel, CJSModel, CachingMixin):
# @postpone
def update_kpi_same_unique_code(kpi):
    '''

    :param kpi:
    :return:

    This function will update all kpis with the same unique code with following attributes:
    - month_1
    - month_2
    - month_3

    - month_1_target
    - month_2_target
    - month_3_target

    - month_1_score
    - month_2_score
    - month_3_score


    - target
    - real
    - latest_score

    '''

    if not kpi.unique_code:
        return

    kpis = KPI.objects.filter(unique_code=kpi.unique_code, quarter_period=kpi.quarter_period).exclude(
        unique_code=None).exclude(unique_code='').exclude(id=kpi.id)
    for k in kpis:
        k.month_1 = kpi.month_1
        k.month_2 = kpi.month_2
        k.month_3 = kpi.month_3

        k.month_1_score = kpi.month_1_score
        k.month_2_score = kpi.month_2_score
        k.month_3_score = kpi.month_3_score

        k.month_1_target = kpi.month_1_target
        k.month_2_target = kpi.month_2_target
        k.month_3_target = kpi.month_3_target

        k.target = kpi.target
        k.real = kpi.real
        k.latest_score = kpi.latest_score

        k.year_target = kpi.year_target
        k.year_result = kpi.year_result
        k.year_score = kpi.year_score
        k.year_data = kpi.year_data

        k.save(add_queue=True, calculate_now_thread=False)

    pass


# @memoize(30)
# @lru_cache
def get_children_refer_order(kpi):
    key = 'get_children_refer_order'.format(kpi.id)

    # # caching use `dill` to avoid `can't pickle function objects` error

    # c = dill.loads(cache.get(key))
    # if c:
    #     return c

    # children = KPI.objects.filter(refer_to=kpi).select_related('refer_to', 'cascaded_from', 'user').order_by(        'ordering')
    children = KPI.objects.filter(refer_to=kpi) \
        .select_related('refer_to', 'cascaded_from', 'user') \
        .order_by('ordering')

    c = sorted(children, key=lambda x: x.get_kpi_id())

    def assign_actor(k):
        k.actor = kpi.actor
        return k

    c = map(assign_actor, c)

    # cache.set(key,c, 30)

    return list(c)


@memoize(30)
def get_kpi_id(kpi):
    # logger.info("NOT CACHING: get_kpi_id({})".format(kpi.id))
    order = kpi.ordering
    if not order:
        # order = "01"
        order = ""
    else:
        order = str(int(order))  # .zfill(2) # remove zfill because KPI code is too long if it has assign many level
        # print "order = str(int(order)).zfill(2) --> {0}".format(order)

    return kpi.get_prefix_id() + order


@postpone
def update_targets_from_quarter(kpi):
    return update_targets_from_quarter_nothread(kpi)


def update_targets_from_quarter_nothread(kpi):
    key = 'update_targets_from_quarter{}'.format(kpi.id)
    # if cache.get(key, None):
    #    return

    # cache.set(key, True, 15)

    current_kpi = KPI.objects.filter(id=kpi.id).first()
    quarter = kpi.quarter_period
    if kpi.score_calculation_type != 'sum':
        if current_kpi and quarter:
            if quarter.quarter == 1 and current_kpi.quarter_one_target != kpi.quarter_one_target:
                kpi.target = kpi.quarter_one_target
                kpi.save(update_fields=['target'])

            elif quarter.quarter == 2 and current_kpi.quarter_two_target != kpi.quarter_two_target:
                kpi.target = kpi.quarter_two_target
                kpi.save(update_fields=['target'])

            elif quarter.quarter == 3 and current_kpi.quarter_three_target != kpi.quarter_three_target:
                kpi.target = kpi.quarter_three_target
                kpi.save(update_fields=['target'])

            elif quarter.quarter == 4 and current_kpi.quarter_four_target != kpi.quarter_four_target:
                kpi.target = kpi.quarter_four_target
                kpi.save(update_fields=['target'])

    return kpi


def get_assigned_kpi(cascaded_from):
    # key = 'get_assigned_kpi{}'.format(cascaded_from.id)
    #    kpi = cache.get(key, 0)
    #   if kpi != 0:
    #      return kpi

    # kpi = KPI.objects.filter(cascaded_from=cascaded_from)  # .first()
    kpi = KPI.objects.filter(cascaded_from=cascaded_from)  # .first()

    kpi = kpi.first()
    # TEMPO Disable cache.set(key, kpi, 30)
    return kpi


def get_parent_kpis_by_category(user_id, quarter_period_id, category, only_id=True, use_cache=False):
    key = "get_parent_kpis_by_category{0}{1}{2}{3}".format(user_id, quarter_period_id, category, only_id)
    if cache.get(key, None) and not use_cache:
        return cache.get(key, None)

    args = []
    kwargs = {}
    if category == 'other':
        excluded_categories = ['financial', 'customer', 'internal', 'learninggrowth']
        args.append(~Q(bsc_category__in=excluded_categories))
    else:
        args.append(Q(bsc_category=category))

    kpis = KPI.objects.filter(*args,
                              user_id=user_id,
                              parent=None,
                              quarter_period_id=quarter_period_id) \
        # .order_by('ordering')
    # if only_id:
    #     kpis = kpis.extra(
    #         select={"group_name_order": "COALESCE(CAST(refer_to_id as TEXT), refer_group_name)"}).order_by(
    #         'prefix_id', 'ordering', 'group_name_order').values('id', 'refer_group_name')
    #
    # else:
    #     kpis = kpis.extra(
    #         select={"group_name_order": "COALESCE(CAST(refer_to_id as TEXT), refer_group_name)"}).order_by(
    #         'prefix_id', 'ordering', 'group_name_order')

    # kpis = kpis.order_by('-refer_to_id', 'refer_group_name', 'prefix_id', 'ordering')
    kpis = kpis.order_by('refer_group_name', 'ordered', 'prefix_id', 'ordering')

    if only_id:
        kpis = kpis.values('id',
                           'refer_group_name')

        '''
        caching
        '''
        kpis = list(kpis)
        # cache.set(key, kpis, 60)
    return kpis


class KPIData(models.Model):
    kpi = models.ForeignKey(KPI)
    created_at = models.DateTimeField(auto_now_add=True)
    date = models.DateField()
    value = models.FloatField()
    input_by = models.ForeignKey(User)

    def __unicode__(self):  # pragma: no cover
        try:
            self.kpi.name + " - " + str(self.date)
        except:
            return ''

    def save(self, *args, **kwargs):
        # if self.kpi.score_calculation_automation:
        #     self.kpi.score_calculate_from_data()

        super(KPIData, self).save(*args, **kwargs)


# class KpiComment(models.Model):
class KpiComment(PermanentModel):
    kpi_unique_key = models.CharField(max_length=100, default='', null='', db_index=True)
    user = models.ForeignKey(User)
    created_at = models.DateTimeField(auto_now_add=True)
    content = models.TextField(default='')
    file = models.FileField(upload_to="comment/attach/{0}/%Y/%m/%d/".format(settings.DOMAIN.split('//')[-1]), max_length=500, blank=True, null=True)
    kpi = models.ForeignKey(KPI, on_delete=models.SET_NULL, null=True)
    send_from_ui = models.CharField(default='auto', max_length=30, null=True, db_index=True)  # email or web
    is_user_notified = models.BooleanField(default=False, db_index=True)  # notify to user via email,...
    is_update = models.BooleanField(default=False)
    month = models.IntegerField(blank=True, null=True)

    class Meta:
        index_together = [['kpi_unique_key', 'is_update', 'created_at'], ]
        ordering = ['-id']

    def __unicode__(self):  # pragma: no cover
        try:
            return str(self.user) + " " + self.content
        except:
            return ''

    def get_file_name(self):
        if self.file:
            name = self.file.name
            return name.split('/')[-1]
        return None

    def get_file_url(self):
        if self.file:
            return self.file.url
        return None

    def save(self, *args, **kwargs):
        # if self.content != self.kpi.outcome_notes:
        #   self.kpi.outcome_notes += "\r\n" + self.content
        #   self.kpi.save()

        # update_last_performance_review

        # self.user.profile.update_last_performance_review()
        super(KpiComment, self).save(*args, **kwargs)

    def to_json(self):
        from user_profile.templatetags.user_tags import avatar

        # temp = serializers.serialize('json', [self])
        js = json.loads(serializers.serialize('json', [self]))[0]

        j = js['fields']
        j['file'] = self.file.url if self.file else None

        #        j['date'] = format(self.due_date, 'd-m-Y')
        j['user_email'] = self.user.email
        j['id'] = self.id
        j['avatar'] = avatar(self.user_id)
        j['display_name'] = self.user.profile.get_display_name()

        return j

    def to_dict(self):
        from user_profile.templatetags.user_tags import avatar, display_name

        item = {
            'avatar': avatar(self.user_id),
            'display_name': display_name(self.user_id),
            'content': self.content,
            'view_date': self.created_at.isoformat(),
            'created_at': format(self.created_at, 'Y-m-d H:i:s'),
            'file_name': self.get_file_name(),
            'file_url': self.get_file_url()
        }
        return item


class DescriptionCell(models.Model):
    slug = models.CharField(max_length=20)
    title = models.CharField(max_length=100, blank=True, default="")
    description = models.TextField(blank=True, default="")

    class Meta:
        ordering = ('id',)

    def __unicode__(self):  # pragma: no cover

        try:
            return self.slug or ''

        except:
            print 'duan error'
            return 'fk'


def img_file_path(instance, filename):  # pragma: no cover
    ext = filename.split('.')[-1]
    name = filename.split('.')[0]
    filename = "ebook/___%s___%s.%s" % (instance.__class__.__name__, uuid.uuid4(), ext)
    return filename


def ebook_file_path(instance, filename):  # pragma: no cover
    return img_file_path(instance, filename)


class CascadeKPIQueue(models.Model):
    kpi = models.ForeignKey(KPI)
    is_run = models.BooleanField(default=False, db_index=True)
    created_at = models.DateTimeField(auto_now_add=True, db_index=True)

    def __unicode__(self):  # pragma: no cover

        try:
            return "{0} | KPI_ID: {1} | REFER_TO_ID: {2}".format(self.kpi.name, self.kpi.id, self.kpi.refer_to_id) or ''

        except:
            print 'duan error'
            return 'fk'


class MarkAsReadComment(models.Model):
    '''
    This class describe lasttime a user read comment on a kpi
    IF user never read any comment of a kpi, read_date is init with default value is kpi.created_at
        --> this mean: if read_date==kpi.created_at: user never read any comment of that kpi,
        because comments is created after kpi created
    '''
    kpi = models.ForeignKey(KPI)
    read_user = models.ForeignKey(User)
    read_date = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        index_together = [
            ['kpi', 'read_date'],
        ]


class ScoreLog(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User)
    score = models.FloatField(null=True)
    score_m1 = models.FloatField(null=True)
    score_m2 = models.FloatField(null=True)
    score_m3 = models.FloatField(null=True)
    is_calculdate = models.BooleanField(default=False)
    quarter_period = models.ForeignKey(QuarterPeriod, null=True)

    class Meta:
        ordering = ['-id']
        index_together = [
            ["quarter_period", "user", "id"],
        ]
        pass

    @classmethod
    def log_user_score(self, kpi, actor=None):  # pragma: no cover
        # only run if there is score changing for parent_kpi of user
        sl = None
        # print('5662: kpi.parent_id {}'.format(kpi.parent_id))
        # if kpi.parent_id is None and kpi.user is not None:
        if kpi.user is not None:
            print ('5664: log_user_score_all: user_id {}'.format(kpi.user_id))

            if settings.ENABLE_CELERY:

                return log_user_score_all.delay(kpi.user_id, kpi.quarter_period_id)
            else:
                return log_user_score_all_inthread(kpi.user_id, kpi.quarter_period_id)

    def __unicode__(self):  # pragma: no cover
        try:
            return u"{0} | Score: {1} | {2}".format(self.user.username, self.score, self.created_at)
        except:
            return ''

'''
re-calculate user score by month in thread
'''


@postpone
def relog_user_score_all_thread(organization, month=None, year=None):  # pragma: no cover
    relog_user_score_all(organization, month, year)

'''
re-calculate user score by month, store in ScoreLog table
input:
    organization: model object
    month: int
    year: int
'''


def relog_user_score_all(organization, month=None, year=None):
    from user_profile.models import Profile, get_full_scores_v2, check_update_score_log
    from user_profile.services.profile import get_quarter

    if not organization:
        return

    profiles = Profile.objects.filter(organization=organization)

    count = profiles.count()
    errors = []
    i = 0
    quarter_period = get_quarter(month, year, organization)
    for profile in profiles:
        i += 1
        email = profile.email()
        print '{}/{}'.format(i, count)
        print 'total error: {}'.format(len(errors))
        print 'relog for user: {}'.format(email)

        value_quarter, score_m1, score_m2, score_m3 = get_full_scores_v2(profile, quarter_period)
        check_update_score_log(value_quarter, score_m1, score_m2, score_m3, profile.id, quarter_period)


@postpone
def log_user_score_all_inthread(user_id, quarter_period_id=None):  # pragma: no cover
    log_user_score_all(user_id, quarter_period_id)


@app.task(name="log_user_score_all")
def log_user_score_all(user_id, quarter_period_id=None, sleep=30, not_disable=False):
    if not user_id:
        return False

    key = "log_user_score_all.{}.{}".format(user_id, quarter_period_id)

    #
    # if there is cached => process is waiting to run => skip
    #
    if cache.get(key, None):  # pragma: no cover
        return

    cache.set(key, True, sleep)

    set_redis('score_log_is_called_{}'.format(user_id), True, sleep)
    print('score_log_is_called_{}'.format(user_id))

    # if settings.TESTING and not_disable == False:  # disable when in testing env
    #     return False

    #######
    # sleep for 30 seconds before run to reduce number of call
    #######
    # time.sleep(sleep)

    system = get_system_user()

    from user_profile.models import get_user_by_id
    user = get_user_by_id(user_id)

    if not user:
        return False

    profile = user.profile

    if not quarter_period_id:
        quarter_period = profile.get_current_quarter()
        if quarter_period:
            quarter_period_id = quarter_period.id

    sl = ScoreLog()
    sl.user = user
    sl.save()

    sl.is_calculdate = True
    #
    # sl.score = profile.kpi_percent()
    # sl.score_m1 = profile.kpi_percent_m1()
    # sl.score_m2 = profile.kpi_percent_m2()
    # sl.score_m3 = profile.kpi_percent_m3()
    from user_profile.models import get_full_scores_by_quarter_id
    sl.score, sl.score_m1, sl.score_m2, sl.score_m3 = get_full_scores_by_quarter_id(user.profile, quarter_period_id)
    sl.quarter_period_id = quarter_period_id
    sl.save()

    print('5707: scorelog_id {}'.format(sl.id))
    print('5707: scorelog.score {}'.format(sl.score))
    print('5707: scorelog.score_m1 {}'.format(sl.score_m1))
    print('5707: scorelog.score_m2 {}'.format(sl.score_m2))


class KPILock(models.Model):
    kpi = models.ForeignKey(KPI)
    created_at = models.DateTimeField(auto_now_add=True)
    permission = models.IntegerField(choices=PERMISSION_CHOICE, default=0)

    def get_permission(self):  # pragma: no cover

        org_permission = self.kpi.user.profile.get_organization().permission
        if org_permission > self.permission:
            return org_permission
        else:
            return self.permission


class Ecal(models.Model):
    kpi_unique_key = models.CharField(max_length=200, default='', null=True, blank=True, db_index=True)
    ethercalc_id = models.CharField(max_length=50, default='', null=True, blank=True, db_index=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def get_ethercalc(self):
        if not self.ethercalc_id:
            try:
                r = requests.get("https://ethercalc.org/_new")

                self.ethercalc_id = r.url
                self.save()
            except:  # pragma: no cover
                return ""

        return self.ethercalc_id


class ReportQuery(models.Model):
    name = models.CharField(max_length=2000, default='', null=True, blank=True)
    organization = models.ForeignKey(Organization)
    created_at = models.DateField(null=True, blank=True)
    create_by = models.ForeignKey(User)
    query = models.TextField(default='', null=True, blank=True)


class EmailTemplate(models.Model):
    title = models.CharField(max_length=2000, default='', null=True, blank=True)
    content = models.TextField(default='', null=True, blank=True)
    organization = models.ForeignKey(Organization)


class KPIErrorLog(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    content = models.TextField(default='')
    error_type = models.CharField(max_length=100, default='')


class ReviewProgress(models.Model):
    user = models.ForeignKey(User)
    quarter = models.ForeignKey(QuarterPeriod, null=True, blank=True)
    score = models.IntegerField(default=100)
    note_score = models.IntegerField(default=0)
    kpi_score = models.IntegerField(default=0)
    late_score = models.IntegerField(default=0)

    score_ladder = models.IntegerField(default=5)

    def __unicode__(self):  # pragma: no cover
        try:
            return str(self.user) + " - " + str(self.quarter.due_date)
        except:
            return ''

    # def get_score_bar(self):
    #     score_bar = int(self.score * 5)  # 1 score = 5 px
    #     if score_bar > 980:
    #         score_bar = 0
    #     if score_bar <= 0:
    #         score_bar = 0
    #
    #     return score_bar

    # def get_color(self):
    #     if self.score <= 70:
    #         return "red"
    #     elif self.score >= 130:
    #         return "green"
    #     else:
    #         return '#008dcc'

    def calculate_score_ladder(self):

        kpis = KPI.objects.filter(user=self.user, quarter_period=self.quarter, parent_id__gt=0)
        total = kpis.count()
        if total == 0:
            total = 1
        self.score_ladder = int(100 / total)
        return self.score_ladder

    def save(self, *args, **kwargs):
        if self.quarter is None:
            self.quarter = self.user.get_profile().get_organization().get_current_quarter()

        self.score_ladder = self.calculate_score_ladder()

        super(ReviewProgress, self).save(*args, **kwargs)


class Customer(models.Model):
    review = models.ForeignKey(Review)
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    message = models.TextField()


auditlog.register(QuarterPeriod)
auditlog.register(KPI)

#class Display(models.Model):
#    code = models.CharField(max_length=100, blank= True)
#   unique_key = models.CharField(max_length=300)
#   owner_email = models.EmailField(max_length=200)
#   enable_review = models.CharField(max_legth =100)
#   get_score_icon = models.ImageField()
#   unique_code = models.CharField(max_length= 100, null= True)
#   default_real= models.CharField(max_length= 100, null= True)
#   reviewer = models.CharField(max_length= 100, null= True)
#   def __str__(self):
#s       return self.code