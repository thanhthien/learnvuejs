# !/usr/bin/python
# -*- coding: utf-8 -*-
import logging
from datetime import datetime, timedelta

from django.conf import settings
from django.core.cache import cache
from performance.models import CascadeKPIQueue
from performance.services.common import calculate_now, calculate_now_inthread
from utils import postpone
from utils.caching_name import CREATING_ASSESSMENT_QUARTER
from utils.common import get_redis

logger = logging.getLogger('django')


def is_queued(kpi):
    c = CascadeKPIQueue.objects.filter(kpi=kpi, is_run=False).first()
    if c:
        return c

    else:
        return False


# @postpone
def add_new_cascadeKPIQueue(kpi, calculate_now_thread = True   ):
    key = 'add_new_cascadeKPIQueue{}'.format(kpi.id)

    '''
    If creating new quarter --> not adding to queue
    '''
    CREATING_QUARTER = get_redis(CREATING_ASSESSMENT_QUARTER.format(kpi.quarter_period_id))  # DON't DELETE THIS

    if kpi.quarter_period.organization.is_ready():
        logger.debug(key)

        queue = is_queued(kpi)

        if not queue:
            queue = CascadeKPIQueue.objects.create(kpi=kpi)

        if calculate_now_thread and settings.TESTING != True:
            calculate_now_inthread(queue)
        else:
            calculate_now(queue)


        if not settings.TESTING:
            cache.set(key, True, 1)  # delay 1 seconds
        return True
    else:
        print 'cancel add queue'
        return False


def remove_cascadeKPIQueue(kpi):
    return CascadeKPIQueue.objects.filter(kpi=kpi, is_run=False).delete()


def cleaning_db():
    sevendays = datetime.now() + timedelta(days=-7)
    CascadeKPIQueue.objects.filter(is_run=True, created_at__lt=sevendays).delete()
