from performance.models import EmailTemplate

def add_new_email_template(organization, title, content):
    email_template = EmailTemplate.objects.filter(title = title, content = content)
    if email_template:
      return None
    et = EmailTemplate()
    et.organization = organization
    et.content = content
    et.title = title
    et.save()
    return et

def edit_email_template(title, content, id):
    email_template = EmailTemplate.objects.get(id=id)
    email_template.content = content
    email_template.title = title
    email_template.save()
    return email_template

def delete_email_template(id):
    email_template = EmailTemplate.objects.filter(id=id)
    return email_template.delete()

def get_all_template(organization):
    email_template = EmailTemplate.objects.filter(organization=organization)
    return email_template

def get_template_by_id(id):
    email_template = EmailTemplate.objects.get(id=id)
    return email_template