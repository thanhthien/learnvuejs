# -*- coding: utf-8 -*-
from django.core.exceptions import PermissionDenied
from django.utils.translation import ugettext
from jsonschema import Draft4Validator, Draft3Validator, validate

from authorization.rules import has_perm, KPI__EDIT_CHILDREN_DATA
from performance.models import KPI
from performance.services import track_change, get_kpi

children_data_schema = {
    "type": "object",

    "properties": {

        "parent_score_auto": {"type": "boolean"},

        "children_weights": {

            "type": "array",

            "items": {

                "type": "object",

                "properties": {
                    "kpi_id": {"type": "number"},
                    "weight": {"type": "number"},
                    "relation_type": {
                        "type": "array",
                        "items": {"type": "string"},
                        "maxItems": 1
                    },

                }

            }

        },
    },
}


def construct_child_weight(kpi):


    return {"kpi_id": kpi.id, "weight": kpi.get_cascaded_from_weight(), "relation_type": []}


def get_children_data(actor, kpi):
    # TODO: check perm or not?

    if not kpi:
        return {}

    refer_kpis = kpi.get_children_refer()

    # O construct data
    if kpi.children_data == None:
        # construct data if this attribute is not set
        default_auto_score_parent = False

        try:
            user = kpi.user
            org = user.profile.get_organization()
            default_auto_score_parent = org.default_auto_score_parent
        except AttributeError:  # catch  'NoneType' object has no attribute 'profile'
            pass

        children_data = {"parent_score_auto": default_auto_score_parent,
                         "children_weights": []
                         }

        children_weights = map(construct_child_weight,
                               refer_kpis)

        children_data["children_weights"] = children_weights

        kpi = set_children_data(kpi.user, kpi, children_data,force=True) # must force when constructing data for the first time

    # O2 make data consistent when add/delete related kpis
    else:
        # temporary variable
        old_children_weights = kpi.children_data["children_weights"]

        # O2.0 Prepare data: get kpi_ids list
        refer_kpi_ids = map(lambda x: x.id, refer_kpis)
        children_weights_ids = map(lambda x: x['kpi_id'], old_children_weights)

        # O2.0 Prepare data: get new_kpi_ids list
        intersection_ids = set(refer_kpi_ids).intersection(children_weights_ids)
        out_of_date_kpi_ids = set(children_weights_ids).difference(intersection_ids)
        new_kpi_ids = set(refer_kpi_ids).difference(intersection_ids)

        # O2.1 filter out removed refer_kpis:
        kpi.children_data["children_weights"] = filter(lambda x: x['kpi_id'] in intersection_ids,
                                                       old_children_weights)

        # O2.2 add new refer_kpis:
        if len(new_kpi_ids) > 0:
            new_kpis = KPI.objects.filter(id__in=new_kpi_ids)
            new_kpi_children_weights = map(construct_child_weight,
                                           new_kpis)
            kpi.children_data["children_weights"] = kpi.children_data["children_weights"] + new_kpi_children_weights

        # 02.3 if data changed ==> save it.
        if old_children_weights != kpi.children_data["children_weights"]:
            kpi.save()
            # kpi.save(update_fields=['children_data'])

    return kpi.children_data


def set_children_data(actor, kpi, children_data, force=False):
    # 1 check perm
    if not force: # force mean we will skip permission check, only use case was constructing data for the first time
        if actor and not has_perm(KPI__EDIT_CHILDREN_DATA, actor, kpi.user):
            raise PermissionDenied()

    # 2 validate children_data
    # this will raise error if note validated

    # If weight is None, set weight == 10.
    for children in children_data['children_weights']:
        if children['weight'] is None:
            children['weight'] = 10

    validate(children_data, children_data_schema)

    # 3 save data, add log,  & update score
    from performance.services import calculate_score_cascading, add_auto_comment
    try:
        # log change if there is changed

        if kpi.children_data['parent_score_auto'] != children_data['parent_score_auto']:
            if children_data['parent_score_auto'] is True:
                msg = ugettext('On')
            else:
                msg = ugettext('Off')

            comment = "{} {}: {}".format(ugettext("Update"),ugettext(" parent score auto"), msg)
            add_auto_comment(actor, kpi,comment )
    except:
        pass

    if kpi.children_data:
        for chil_weight in kpi.children_data['children_weights']:
            for chil_data in children_data['children_weights']:
                if chil_weight['kpi_id'] == chil_data['kpi_id'] and chil_weight['weight'] != chil_data['weight']:
                        name_kpi = get_kpi(chil_data['kpi_id']).get_name()
                        old_value = chil_weight['weight']
                        new_value = chil_data['weight']
                        comment = "{} \n {} KPI: {} {} → {} ".format(ugettext("Changed:"), ugettext("Weight"), name_kpi, old_value, new_value)
                        add_auto_comment(actor, kpi, comment)

    kpi.children_data = children_data

    kpi.save()

    calculate_score_cascading(kpi.id)

    # kpi.save(update_fields = ['children_data'])

    return kpi
