#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.core.cache import cache
# from performance.documents import FinalScoreHistory
import json

from performance.models import CascadeKPIQueue


from utils import brief_name, percent_to_score
from utils import notify_slack, postpone
from utils.threading_decorator import postpone_process
from user_profile.models import Profile


class PerformanceApi:
    @classmethod
    def get_user_organization(cls, user):
        try:
            user_org = cache.get('user_organization_' + str(user.id))
            if not user_org:
                user_org = Profile.objects.select_related('organization').get(user=user)
                cache.set('user_organization_' + str(user.id), user_org, 60 * 60 * 2)

            return user_org
        except:
            return None

    @classmethod
    def get_organization(cls, user):
        if not user:
            return None

        return user.profile.get_organization()

    # @classmethod
    # def create_core_value(cls, user, organization, quarter_period):
    #     from performance.models import Competency
    #     if not Competency.objects.filter(user=user,
    #                                      quarter_period=quarter_period,
    #                                      is_core_value=True).exists():
    #         core_value = organization.get_core_value()
    #         if core_value:
    #             core_value = eval(core_value)
    #         else:
    #             return
    #
    #         if core_value['apply_all']:
    #             comp = Competency.objects.create(name_vi=u"Giá trị cốt lõi",
    #                                              name_en="Core values",
    #                                              description=core_value['mission'],
    #                                              user=user,
    #                                              quarter_period=quarter_period,
    #                                              is_core_value=True)
    #             for item in core_value['core_values']:
    #                 Competency.objects.create(name_vi=item,
    #                                           name_en=item,
    #                                           description=core_value['mission'],
    #                                           user=user,
    #                                           quarter_period=quarter_period,
    #                                           is_core_value=True,
    #                                           parent=comp)

    # @classmethod
    # def get_performace_user_data(cls, user, quarter_period, organization):
    #    # from performance.models import Review
    #
    #     usr_org = cls.get_user_organization(user)
    #     profile = user.get_profile()
    #
    #     if not usr_org:
    #         return {}
    #
    #     job_title_name = ""
    #     if profile.current_job_title_id:
    #         job_title_name = profile.current_job_title.name
    #
    #     # self_review = Review.objects.get_or_create(organization_id=usr_org.organization_id,
    #     #                                            reviewee_id=user,
    #     #                                            reviewer_id=user,
    #     #                                            review_type='self',
    #     #                                            quarter_period=quarter_period)[0]
    #     share_to_manager = True
    #     share_to_self = True
    #
    #     max_min_kpi = profile.get_bound_score_kpi(quarter_period)
    #     max_min_competency = profile.get_bound_score_competency(quarter_period)
    #   #  PerformanceApi.create_core_value(user, organization, quarter_period)
    #
    #     try:
    #         scores = FinalScoreHistory.objects.filter(organization=organization.id,
    #                                                   quarter_period__lt=quarter_period.id,
    #                                                   user_id=profile.user_id) \
    #                                           .order_by('quarter_period') \
    #                                           .values_list('due_date', 'quarter_name',
    #                                                        'competency_score',
    #                                                        'kpi_score')[:10]
    #         history_scores = json.loads(scores.to_json())
    #     except:
    #         history_scores = []
    #
    #     kpis = profile.kpis_dict('self', quarter_period, share_to_manager, user.id)
    #     if organization.enable_competency:
    #         competencies = profile.competencies_dict('self', quarter_period, share_to_manager, user.id)
    #     else:
    #         competencies = []
    #
    #     user_data = {
    #         'user_id': profile.user_id,
    #         'profile': profile.id,
    #         # 'report_to': profile.report_to_id,
    #         'report_to': profile.parent.user_id if profile.parent else None,
    #         'name': profile.display_name,
    #         'email': user.email,
    #         'start_date': format(usr_org.start_date, 'd-m-Y') if usr_org.start_date else '',
    #         'role_start': format(usr_org.role_start, 'd-m-Y') if usr_org.role_start else '',
    #         'employee_code': usr_org.employee_code,
    #         'avatar': profile.get_avatar(),
    #         'job_title_name': job_title_name,
    #         'position': profile.title,
    #         'brief_name': brief_name(profile.display_name),
    #         'competencies': competencies,
    #         'kpis': kpis,
    #         'share_to_manager': share_to_manager,
    #         'short_term_career': profile.short_term_career,
    #         'short_term_development': profile.short_term_development,
    #         'long_term_career': profile.long_term_career,
    #         'long_term_development': profile.long_term_development,
    #         'competency_final_score': profile.score_full_competency(quarter_period),
    #         'kpi_final_score': profile.score_full_kpi_percent(quarter_period),
    #         'min_score_kpi': max_min_kpi[0],
    #         'max_score_kpi': max_min_kpi[1],
    #         'min_score_competency': max_min_competency[0],
    #         'max_score_competency': max_min_competency[1],
    #         'strength_kpi': profile.get_strength_kpi(quarter_period),
    #         'strength_competency': profile.get_strength_competency(quarter_period),
    #         'weak_kpi': profile.get_weak_kpi(quarter_period),
    #         'weak_competency': profile.get_weak_competency(quarter_period),
    #         'self_percent_finished': 0,
    #         'share_to_self': share_to_self,
    #         'reviews': profile.get_360_review_dict(quarter_period),
    #         'is_deputy_manager': profile.is_deputy_manager,
    #         'review_progress_html': profile.review_progress_html(),
    #         'previous_competency_final_score': profile.previous_competency_final_score,
    #         'previous_kpi_final_score': profile.previous_kpi_final_score,
    #         'history_scores': history_scores,
    #         'is_active': profile.is_active(),
    #         'status': 'active' if profile.is_active() else "inactive"
    #     }
    #     return user_data

@postpone
def calculate_now_inthread(q):
    calculate_now(q)


def calculate_now(q):
    from performance.services import calculate_score_cascading
    k = calculate_score_cascading(q.kpi.id)  # KPIServices().calculate_cascade_score(q.kpi)

    '''

    nếu chạy qua tới đây thì lưu lại.

    '''

    q.is_run = True
    q.save()

    CascadeKPIQueue.objects.filter(kpi_id=q.kpi_id, is_run=False).delete()

    return k
