# !/usr/bin/python
# -*- coding: utf-8 -*-
from performance.models import KpiComment
from performance.services import get_kpi
from django.utils.translation import ugettext
from performance.services.kpi import add_auto_comment
import os, sys
from elms.settings import ROOT_DIR, BASE_DIR
import urllib
from datetime import datetime, timedelta

def get_evidence(kpi_id, month):
    return KpiComment.objects.filter(kpi_id=kpi_id, month=month).order_by('-created_at')


def create_evidence(kpi_id, month, actor, content='', attachment=None):
    k = KpiComment()
    k.month = month
    k.file = attachment
    k.kpi_id = kpi_id
    k.content = content
    k.user = actor
    k.send_from_ui = 'web'
    k.save()
    kpi = get_kpi(kpi_id)

    evidence_cont = u"%s: %s" % (ugettext("Add evidence") + " KPI",
                                            " " +kpi.name + "\n")
    add_auto_comment(actor, kpi, evidence_cont, attachment = attachment)

    return k

def delete_evidence(id, kpi_id, actor):
    k = KpiComment.objects.get(id=id)

    now = datetime.now()
    time_can_delete = now - timedelta(hours=24)
    is_expired = actor.profile.get_current_quarter().is_expired()
    if time_can_delete > k.created_at or  is_expired == True:
        return False
    # use urllib to decode to unicode
    delete_url = os.path.join(BASE_DIR, 'static') + urllib.unquote(k.get_file_url()).decode('utf8')

    if os.path.exists(delete_url):
        os.remove(delete_url)

    kpi = get_kpi(kpi_id)

    comment = u"%s: %s" % (ugettext("Delete attachment") + " KPI" ,
                                         " " +kpi.name + "\n")
    add_auto_comment(actor, kpi, comment, attachment= k.file)
    k.delete()
    return True


