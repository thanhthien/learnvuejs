# !/usr/bin/python
# -*- coding: utf-8 -*-
import logging
from itertools import repeat

from celery.task import task
from elms.celery import app

from celery_once import QueueOnce
from django.core.mail import mail_admins, EmailMessage

from authorization.rules import has_perm, KPI__EDITING, SETTINGS__VIEW_ORG_CHART, DELETE_KPI, KPI__REVIEWING, \
    KPI__WEIGHT_EDIT, MANAGER_REVIEW, KPI__VIEWING
from company.models import Organization
from django.contrib.auth.models import User
from django.core.cache import cache
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.db import transaction
from django.db.models import Q, Max, Count
from django.utils.translation import ugettext
from elms import settings
from lru import lru_cache_function
from memoize import memoize
from performance.base.kpi_type import *
from performance.db_models.strategy_map import GroupKPI
from performance.logger import KPILogger
from performance.models import KPI, KpiComment, Ecal, MarkAsReadComment, update_targets_from_quarter_nothread, \
    update_group_name, update_kpi_same_unique_code, are_other_children_delayed, QuarterPeriod
from performance.services.CascadeKPIQueue import add_new_cascadeKPIQueue, remove_cascadeKPIQueue
# from performance.services.kpi_feedback import get__kpi_field_error_mappings
from performance.model_services import clone_attrs
from performance.services.notification import send_notification, DELETE_KPI_FAILED, USER_LOG_TYPE
# from performance.services import strategymap
from user_profile.services.permissions import can_view
from user_profile.services.profile import get_user_by_id, get_user
from user_profile.services.user import get_system_user, get_user_by_email
from utils import postpone, random_str, random_string
from utils.common import fix_input_float
from utils.kpi_utils import calculate_score
from user_profile.models import Profile
import traceback

logger = logging.getLogger('django')


def get_all_object_kpi_by_unique_key(kpi_unique_key):
    return KPI.all_objects.filter(unique_key=kpi_unique_key).first()


# Get KPIs in quarter
def get_kpis_in_quarter(actor, kpi_id, quarter):
    # Check if KPI with ID is existed
    target_kpi = KPI.objects.filter(pk=kpi_id).first()
    target_user = target_kpi.user
    if not has_perm(action=SETTINGS__VIEW_ORG_CHART, actor=actor, target=target_user):
        # 0903521972
        # 0236365003
        raise PermissionDenied
    # Return KPI with quarter
    uk = target_kpi.unique_key
    result_kpi = KPI.objects.filter(unique_key=uk, quarter=quarter).first()
    if result_kpi:
        return result_kpi
    return None


# Get user KPIs, just use for single or few users (<20 users)
def get_user_kpis_by_kwargs(actor, target, kwargs, quarter_id=None):
    if not has_perm(action=KPI__VIEWING, actor=actor, target=target):
        raise PermissionDenied
    try:
        quarter_period = QuarterPeriod.objects.filter(pk=quarter_id).first()
        if quarter_period is None:
            quarter_period = target.profile.get_organization().get_current_quarter()
        kpis_parent = list(target.profile.get_kpis_parent(quarter_period=quarter_period))

        # ## HOTFIX
        result = []
        for k in kpis_parent:
            tmp = k.to_json_excel(actor=actor)
            tmp['kpi_group_id'] = k.group_kpi_id
            result.append(tmp)
            # ## Make sure that KPI return kpi_refer_children
    except Exception as e:
        print traceback.format_exc()
        result = None

    return result


# Get organization KPIs, and actor must be admin of organization
def get_org_kpis_by_kwargs(actor, target, kwargs):
    pass


def update_targets_from_quarter(kpi):
    quarter = kpi.quarter_period
    # Khang note
    # target will be different from quarter_<n>_target
    # if user reviewed some months, not change target, target so will be different from quarter_<n>_target
    can_be_updated = True

    # move tu ham calculate_score_cascading
    if kpi.score_calculation_type == 'sum':
        kpi = update_kpi_target_from_month_targets(kpi)

    if kpi.score_calculation_type == 'sum':
        can_be_updated = (kpi.month_1_score is None) and (kpi.month_2_score is None) and (kpi.month_3_score is None)
    if quarter.quarter == 1:
        kpi.target = kpi.quarter_one_target

    elif quarter.quarter == 2:
        kpi.target = kpi.quarter_two_target

    elif quarter.quarter == 3:
        kpi.target = kpi.quarter_three_target

    elif quarter.quarter == 4:
        kpi.target = kpi.quarter_four_target

    if can_be_updated:
        kpi.save(update_fields=['target'])
    kpi = update_month_targets_onchange_calculation_type(kpi, save=True)
    return kpi


def update_kpi_target_from_month_targets(kpi):
    if kpi.score_calculation_type == 'sum':
        month_1_target = fix_input_float(kpi.month_1_target) if kpi.month_1_target else 0
        month_2_target = fix_input_float(kpi.month_2_target) if kpi.month_2_target else 0
        month_3_target = fix_input_float(kpi.month_3_target) if kpi.month_3_target else 0
        kpi.target = month_1_target + month_2_target + month_3_target
    else:
        '''
        for other `score_calculation_type` cases ( `most_recent` or `average`)
        for `average` case:
            - if kpi.month_3_target != None and/or kpi.month_3_target != kpi.month_2_target
                ==>  kpi.target = kpi.month_3_target

            - (if kpi.month_3_target == None and kpi.month_2_target != None) and/or kpi.month_2_target != kpi.month_1_target
               ==>  kpi.target = kpi.month_2_target

            - (if kpi.month_3_target == None and kpi.month_2_target == None and  kpi.month_1_target != None):
                ==>  kpi.target = kpi.month_1_target

            elif (if kpi.month_3_target == None and kpi.month_2_target == None and  kpi.month_1_target == None):
                ==> do nothing

        '''

        if kpi.month_3_target is not None:
            kpi.target = kpi.month_3_target
        elif kpi.month_2_target is not None:
            kpi.target = kpi.month_2_target
        elif kpi.month_1_target is not None:
            kpi.target = kpi.month_1_target

    kpi = kpi.update_quarter_targets()

    kpi.save(update_fields=['target'])

    return kpi


def update_month_targets_onchange_calculation_type(kpi, save=False):
    changed = False
    kpi.target = fix_input_float(kpi.target)

    if kpi.target is None:
        return kpi

    if kpi.score_calculation_type == 'average':
        if kpi.month_1_target != kpi.target:
            changed = True
            kpi.month_1_target = kpi.target

        if kpi.month_2_target != kpi.target:
            changed = True
            kpi.month_2_target = kpi.target

        if kpi.month_3_target != kpi.target:
            changed = True
            kpi.month_3_target = kpi.target

    elif kpi.score_calculation_type == 'sum':

        if kpi.month_1_target is None and kpi.month_2_target is None and kpi.month_3_target is None:
            changed = True
            kpi.month_1_target = round(float(kpi.target) / 3, 2)
            kpi.month_2_target = round(float(kpi.target) / 3, 2)
            kpi.month_3_target = round(float(kpi.target) / 3, 2)

    elif kpi.score_calculation_type == 'most_recent':

        if kpi.month_3_target is None:
            changed = True
            kpi.month_3_target = kpi.target

    if changed and save is True:
        # https://agile.cloudjet.solutions/project/fountainhead-cloudjet-kpi/us/1802?kanban-status=3
        # kpi.save()
        changed_fields = kpi.tracker.changed().keys()
        kpi.save(update_fields=list(changed_fields))

    return kpi


def remove_kpi_from_refer_to(kpi, actor=None):
    if kpi.refer_to_id > 0:
        kpi.refer_to_id = None

    try:
        cascaded_from = kpi.cascaded_from
    except ObjectDoesNotExist:  # pragma: no cover
        cascaded_from = None

    kpi.cascaded_from_id = None
    kpi.save()

    if cascaded_from:
        cascaded_from.delete()

    return kpi


# Add comment upload file
def add_comment_upload_file(user, kpi, file, unique_key=''):
    if (unique_key == ''):
        unique_key = kpi.unique_key
    kc = KpiComment(user=user,
                    kpi_unique_key=unique_key,
                    kpi=kpi, send_from_ui='web',
                    file=file,
                    is_update=True)
    kc.save()
    return kc


# Add auto comment
def add_auto_comment(user, kpi, content, unique_key='', attachment=None):
    if (unique_key == ''):
        unique_key = kpi.unique_key
    kc = KpiComment(user=user,
                    kpi_unique_key=unique_key,
                    kpi=kpi, send_from_ui='auto',
                    content=content,
                    is_update=True,
                    file=attachment)
    kc.save()

    if user != kpi.user:
        try:
            organization = kpi.user.profile.organization.id
        except:
            organization = None
        content = u"KPI: %s (%s)\n%s" % (kpi.name, kpi.id, content)
        send_notification(actor=user, user=kpi.user, recipient=kpi.user, description=content, type=USER_LOG_TYPE,
                          verb='', organization=organization)
    return kc


# Add user comment
def add_user_comment(user, kpi, content, unique_key=''):
    if (unique_key == ''):
        unique_key = kpi.unique_key
    kc = KpiComment(user=user,
                    kpi_unique_key=kpi.unique_key,
                    kpi=kpi, send_from_ui='web',
                    content=content,
                    is_update=True)
    kc.save()
    if user != kpi.user:
        try:
            organization = kpi.user.profile.organization.id
        except:
            organization = None
        content = u"KPI: %s (%s)\n%s" % (kpi.name, kpi.id, content)
        send_notification(actor=user, user=kpi.user, recipient=kpi.user, description=content, type=USER_LOG_TYPE,
                          verb='', organization=organization)
    return kc


def update_year_data_to_kpi_target(kpi, year_data, commit=False):
    '''
    Copy month target from year data to the months of the KPIs current quarter
    :param kpi: KPI need to update
    :param year_data: Year data
    :param commit: if True will be commited after update
    '''
    if year_data and isinstance(year_data, dict):
        quarter_period = kpi.quarter_period
        months_target = year_data.get('months_target') or {}
        quarter_data = months_target.get('quarter_' + str(quarter_period.quarter)) or {}

        for m in [1, 2, 3]:
            key = 'month_{}'.format(m)
            if quarter_data.get(key):
                try:
                    month = 'month_{}_target'.format(m)
                    setattr(kpi, month, float(quarter_data.get(key)))
                except ValueError:
                    pass

        if commit:
            kpi.save()

    return kpi


def update_kpi_target_to_year_data(kpi, commit=True):
    '''
    Copy months' target of current quarter to year_data
    :param kpi: KPI need to copy
    :param commit: if True will be commited after update
    '''
    quarter_period = kpi.quarter_period
    year_data = kpi.year_data or {}
    months_target = {}

    # Get exist data
    if year_data.get('months_target') and isinstance(year_data['months_target'], dict):
        months_target = year_data['months_target']
    else:
        year_data['months_target'] = {}

    # Update target KPI current quarter to year_data
    quarter_key = 'quarter_{}'.format(quarter_period.quarter)
    months_target[quarter_key] = {
        'month_1': kpi.month_1_target,
        'month_2': kpi.month_2_target,
        'month_3': kpi.month_3_target,
    }

    year_data['months_target'].update(months_target)
    kpi.year_data = year_data

    if commit:
        kpi.save()

    return kpi


class KPIServices:

    @classmethod
    def count_comment_by_kpiid(self, read_user_id, kpi_id):
        condictions = []
        condictions.append(~Q(user_id=read_user_id))
        condictions.append(Q(kpi_id=kpi_id))
        last_read_date = MarkAsReadComment.objects.filter(kpi_id=kpi_id, read_user_id=read_user_id).aggregate(
            last_read_date=Max('read_date')).get('last_read_date')
        if last_read_date is not None:
            condictions.append(Q(created_at__gt=last_read_date))
        kc = KpiComment.objects.filter(*condictions).aggregate(comment_counts=Count('id'))
        count = kc.get('comment_counts')
        return count

    @classmethod
    def count_comment(self, read_user_id, kpi_unique_key):
        condictions = []
        condictions.append(~Q(user_id=read_user_id))
        condictions.append(Q(kpi__unique_key=kpi_unique_key))
        last_read_date = MarkAsReadComment.objects.filter(kpi__unique_key=kpi_unique_key,
                                                          read_user_id=read_user_id).aggregate(
            last_read_date=Max('read_date')).get('last_read_date')
        if last_read_date is not None:
            condictions.append(Q(created_at__gt=last_read_date))

        kc = KpiComment.objects.filter(*condictions).aggregate(comment_counts=Count('id'))
        count = kc.get('comment_counts')
        return count

    def create_kpi(self, creator=None, name='', bsc_category="financial", ordering=None, unit='', quarter_period=None,
                   owner_email=None, user=None,
                   year=0, operator=">=",
                   year_target=None, quarter_one_target=None, quarter_two_target=None, quarter_three_target=None,
                   quarter_four_target=None, user_id=None,
                   target=None, current_goal='', parent=None, parent_id=None, refer_to=None, quarter=None,
                   future_goal='',
                   refer_to_id=None, description="", is_owner=True, end_date=None, archivable_score=8,
                   score_calculation_type='most_recent',
                   code='',
                   weight=10,
                   group_name='',
                   group_id=None,
                   year_data={}):

        if user_id:
            user = get_user(user_id)

        if creator and user:
            if not has_perm(KPI__EDITING, creator, user):
                raise PermissionDenied

        if name:
            name = name.replace('\n', '').replace('\r', '').strip()
        k = KPI()
        k.code = code

        k.name = name
        # k.name_vi = name
        # k.name_en = name
        k.description = description

        k.bsc_category = bsc_category
        # k.ordering = bsc_category
        # k.get_next_ordering()
        k.unit = unit
        # k.quarter_period = quarter_period

        k.owner_email = owner_email
        k.is_owner = is_owner
        k.end_date = end_date

        k.user = user

        if year:
            k.year = year
        if quarter:
            k.quarter = quarter

        if not quarter_period:
            if user:
                quarter_period = user.profile.get_current_quarter()

            # excluded from cover as following case cannot be reached
            # if user_id is not None, user is assigned value in first line of this method.
            # thus, code will always enter above if condition.
            elif k.user_id:  # pragma: no cover
                quarter_period = get_user_by_id(user_id).profile.get_current_quarter()

        k.quarter_period = quarter_period

        try:
            k.weight = fix_input_float(weight)
        except:  # pragma: no cover
            pass

        k.target = target
        k.current_goal = current_goal
        k.future_goal = future_goal

        if operator.strip() == "≥":
            operator = ">="
        elif operator.strip() == "≤":
            operator = "<="

        k.operator = operator
        k.year_target = fix_input_float(year_target)
        k.quarter_one_target = fix_input_float(quarter_one_target)
        k.quarter_two_target = fix_input_float(quarter_two_target)
        k.quarter_three_target = fix_input_float(quarter_three_target)
        k.quarter_four_target = fix_input_float(quarter_four_target)
        k.archivable_score = archivable_score
        k.score_calculation_type = score_calculation_type

        if parent_id:
            k.parent_id = parent_id
            parent = KPI.objects.get(id=parent_id)
            k.parent = parent
        elif parent:
            k.parent = parent

        if parent and not (refer_to or refer_to_id):
            k.refer_to = parent
        if parent_id and not (refer_to or refer_to_id):
            k.refer_to_id = parent_id

        if refer_to_id:

            k.refer_to_id = refer_to_id
        elif refer_to:
            k.refer_to = refer_to

        if k.refer_to_id and not k.owner_email and not k.user and not k.parent:

            try:
                if k.refer_to.user_id:
                    k.owner_email = k.refer_to.user.email
                    k.user = k.refer_to.user
                k.parent = k.refer_to
            except KPI.DoesNotExist:  # pragma: no cover
                pass

        if not bsc_category and parent:
            k.bsc_category = parent.bsc_category
        elif not bsc_category and k.refer_to:
            k.bsc_category = k.refer_to.bsc_category

        # if creator:

        #     if k.user_id == creator.id:
        #         k.self_confirmed = True

        #     if Profile.objects.filter(user_id=k.user_id, report_to=creator).exists():
        #         k.manager_confirmed = True

        '''
        KPI has to have Group first

        (1) if group_id provided, group_id != None --> assign kpi.group_kpi
        (2) if group_id provided, then create new group
                (2.1) if group_name is '' , then group_name = k.name.
        '''
        if group_id:

            k.group_kpi_id = group_id
        else:

            if k.name:
                group_name = k.name

        # elif group_name:
            group = GroupKPI()
            group.name = group_name
            group.category = k.bsc_category

            from performance.services.strategymap import get_user_current_map
            user_map = get_user_current_map(k.user)
            group.save()

            k.group_kpi = group

        k.year_data = year_data
        k = update_year_data_to_kpi_target(k, year_data)
        k.save(actor=creator)

        # k.get_group()
        # if creator:
        #     add_auto_comment(creator, k, "add kpi")

        '''
        SET DEFAULT VALUE FOR CHILD KPI
        '''

        # if quarter_one_target is None and (k.refer_to_id >0  or k.parent_id >0 ):
        #     quarter_one_target = 10
        # if quarter_two_target is None and (k.refer_to_id >0  or k.parent_id >0 ):
        #     quarter_two_target = 10
        # if quarter_three_target is None and (k.refer_to_id >0  or k.parent_id >0 ):
        #     quarter_three_target = 10
        # if quarter_four_target is None and (k.refer_to_id >0  or k.parent_id >0 ):
        #     quarter_four_target = 10

        '''
        END DEFAULT VALUE FOR CHILD KPI
        '''

        if not k.owner_email and not owner_email and k.user:
            k.owner_email = k.user.email

        if not ordering:
            if k.refer_to or k.parent:
                # k.ordering = None
                k.ordering = k.get_next_ordering()
                # re_order(k)
            else:
                k.ordering = k.get_next_ordering()
        else:
            k.ordering = fix_input_float(ordering)

        # Comment because don't update auto target => Changed logic
#         if not target:
#             k.target = self.get_target(k, quarter_period)
#
#         k = k.update_quarter_targets()
        k.save_calculate_score(just_created=True)
        KPILogger(user, k, 'created', description).start()
        return k

    def get_target(self, kpi, quarter_period):

        if not quarter_period:
            return None
        if quarter_period.quarter == 1:
            return kpi.quarter_one_target
        elif quarter_period.quarter == 2:
            return kpi.quarter_two_target
        elif quarter_period.quarter == 3:
            return kpi.quarter_three_target
        elif quarter_period.quarter >= 4:
            return kpi.quarter_four_target
        return None

    def update_kpi(self, actor, kpi, de_quy_xuong=False, de_quy_len=False, **d):

        kpi_type_v2 = kpi.kpi_type_v2()
        excluded_fields = ['weight', 'owner_email', 'user', 'user_id', 'parent', 'parent_id',
                           'id', 'cascaded_from', 'cascaded_from_id', 'assigned_to', 'assigned_to_id'
                                                                                     'temp_weight', 'copy_from',
                           'copy_from_id', 'refer_to', 'refer_to_id',
                           'reviewer', 'reviewer_id', 'hash', 'group', 'reviewer_email',
                           ]

        for attr, value in d.iteritems():

            if attr == 'reviewer_email':
                if value and not get_user_by_email(value):
                    return {'status': 'failed', "message": ugettext("Reminder email not existed")}

                    #  print attr
            if (de_quy_xuong or de_quy_len):

                if attr in excluded_fields:  # không set
                    continue

            if attr == 'weight' and (isinstance(value, str) or isinstance(value, unicode)):
                try:
                    float(value)
                except:
                    continue
            # # update refer_group_name
            # if attr == "refer_group_name":
            #     # print "group {}".format(value)
            #     update_group_name(self, None)

            if attr == 'group':
                pass

            if attr == 'refer_group_name':  # not update refer_group_name, let it be processed automatically
                continue

            if attr == "year_data":
                if isinstance(kpi.year_data, dict):
                    kpi.year_data.update(value)
                    update_year_data_to_kpi_target(kpi, value)
            else:
                setattr(kpi, attr, value)

        # kpi.name_vi = kpi.name
        # kpi.name_en = kpi.name

        if kpi.parent_id > 0:  # remove from BSC group (GroupKPI)
            pass

        '''
        nếu không phải là kpi_trung_gian thì cần set `refer_to_id`
        '''
        if kpi_type_v2 != kpi_trung_gian \
                and kpi.refer_to == None and kpi.parent_id > 0 and kpi.user_id > 0:
            kpi.refer_to_id = kpi.parent_id

        if kpi.cascaded_from and de_quy_xuong == False:
            self.update_kpi(actor, kpi.cascaded_from, de_quy_len=True, **d)
            # kpi.cascaded_from.name = kpi.name
            # kpi.cascaded_from.save()

        if kpi.assigned_to_id and de_quy_len == False:
            cascade = KPI.objects.filter(user_id=kpi.assigned_to_id,
                                         cascaded_from_id=kpi.id,
                                         quarter_period=kpi.quarter_period).first()
            if cascade:
                self.update_kpi(actor, cascade, de_quy_xuong=True, **d)
                # cascade.name = kpi.name
                # cascade.name_vi = kpi.name
                # cascade.name_en = kpi.namehttps://infinit.io/_/E3LGZGk
                # cascade.operator = kpi.name
                # cascade.target = form.cleaned_data['target']
                # cascade.unit = form.cleaned_data['unit']
                # cascade.end_date = form.cleaned_data['deadline']
                # cascade.save()

        if de_quy_xuong == False and de_quy_len == False:
            description = kpi.get_changed_history()
            # Push auto comment to db.
            track_change(actor, kpi)

            # if 'name' in changed or 'target' in changed or 'operator' in changed or 'real' in changed:
            #     if kpi.user_id == actor.id:
            #         kpi.manager_confirmed = False
            #     else:
            #         pass
            # kpi.self_confirmed = False
            if description:
                description = description  # why 'description' is not equal to 'description' itself????
                #   from performance.logger import KPILogger
                KPILogger(actor, kpi, 'update', description).start()

        kpi = kpi.update_quarter_targets()
        if not kpi.parent_id and kpi.user_id and kpi.owner_email != kpi.user.email:
            kpi.owner_email = kpi.user.email

        if not kpi.bsc_category and kpi.parent:
            kpi.bsc_category = kpi.parent.bsc_category
        elif not kpi.bsc_category and kpi.refer_to:
            kpi.bsc_category = kpi.refer_to.bsc_category

        if (kpi_type_v2 == kpi_trung_gian):
            # Kiểm tra dòng #334, sau khi KPI trung gian
            # nhận giá trị refer_to_id thì phải unset lại attribute này.
            # Hong update 27/2/2017: --> Đã check và thêm điều kiện:             kpi_type_v2 != kpi_trung_gian
            kpi.refer_to = None
        kpi.save()
        return kpi

    def get_parent_kpis_by_category(self, user_id, category):
        all_kpis = self.get_parent_kpis(user_id)
        kpis = []
        for k in all_kpis:
            if k.bsc_category == category:
                kpis.append(k)

        # kpis = KPI.objects.filter(user_id = user_id, bsc_category = category)

        return kpis

    def get_parent_kpis(self, user_id):
        user = User.objects.get(id=user_id)
        kpis = user.profile.get_kpis_parent()  # KPI.objects.filter(user_id = user_id)

        return kpis

        # def calculate_cascade_score(self, kpi):
        #     return calculate_score_cascading(kpi.id)

#
# @task(name= 'calculate_score_cascade_from')
# def calculate_score_cascade_from(kpi_id, score):
#
#     kpi = KPI.objects.filter(id = kpi_id).first()
#     if not kpi:
#         return False
#
#     cascaded_from = None
#     try:
#         cascaded_from = kpi.cascaded_from
#     except KPI.DoesNotExist:
#         pass
#
#     logger.info('line 509')
#
#     #add_new_cascadeKPIQueue(cascaded_from, calculate_now_thread=False)
#
#     if cascaded_from is not None:
#
#         if not cascaded_from.user_id:
#             cascaded_from.delete()
#             kpi.assigned_to = None
#             kpi.cascaded_from = None
#             kpi.save(add_queue=False)
#             logger.info('line 518')
#             return
#
#         kpi.set_cascaded_from_attrs()  # add queue here
#         logger.info('line 524')
#
#         if cascaded_from and cascaded_from.user_id == kpi.user_id:  # FUCK
#             return
#         if cascaded_from:
#             #    print "[cascaded_kpi1] score for kpi.id {0} is {1} - user_id {2}" \
#             #         "".format(cascaded_from.id, score, cascaded_from.user_id)
#
#             cascaded_from.set_score(score, cascaded_from.user_id)
#
#     logger.info('line 529')
#     try:
#         parent = kpi.refer_to
#     except KPI.DoesNotExist:
#         parent = None
#
#     if parent:
#         score = parent.calculate_avg_score(kpi.user_id)
#         parent.set_score(score, parent.user_id)
#
#         if parent.cascaded_from_id:
#             remove_cascadeKPIQueue(parent)
#             #        print "calculate parent kpi.id {0}".format(parent.id)
#             # if settings.ENABLE_CELERY:
#             #     calculate_score_cascading.delay(parent.id)
#             # else:
#
#             calculate_score_cascading(parent.id)


# @app.task(name='calculate_score_cascading1')
# @task(name='calculate_score_cascading')
def calculate_score_cascading(kpi_id):
    # ## exclude kpi with quarter_period is None
    kpi = KPI.objects.filter(id=kpi_id).first()

    if not kpi:
        return False
        #     return calculate_score_cascading_nodelay(kpi)
        #
        #
        # def calculate_score_cascading_nodelay(kpi):
        # return calculate_score_cascading_nodelay(kpi)
        #
        #
        # def calculate_score_cascading_nodelay(kpi):
    #     if kpi.score_calculation_type == 'sum':
    #         kpi = update_kpi_target_from_month_targets(kpi)
    # logger.info('line 469')

    if kpi.parent_id is None and not cache.get('fix_count_children_refer{}'.format(kpi.id)):
        kpi.fix_count_children_refer()
        cache.set('fix_count_children_refer{}'.format(kpi.id), True, 1 * 60 * 60)

    parent = None

    # logger.info('line 476')

    parent_score_auto = True
    try:
        parent_score_auto = kpi.children_data['parent_score_auto']
    except:
        pass

    # if kpi.get_children().count() == 0:  # KHONG CO KPI Con .

    '''
    - Have children kpis
    - parent_score_auto = True
    '''
    if parent_score_auto and kpi.get_children().count() > 0:
        score, m1, m2, m3 = kpi.calculate_avg_score(kpi.user_id)

        #  logger.info('line 499')
        kpi.set_score(score, month_1_score=m1, month_2_score=m2, month_3_score=m3)
    # logger.info('line 501')

    else:

        kpi_type_v2 = kpi.kpi_type_v2()

        if kpi_type_v2 == 'kpi_trung_gian':

            kpi_phan_cong = kpi.get_assigned_kpi()
            kpi_phan_cong.set_cascaded_from_attrs()
            score = kpi_phan_cong.get_score()
        else:
            # score = kpi_score_calculator(kpi.operator, kpi.target, kpi.real, kpi)  # k.get_score(k.user_id)
            kpi = calculate_score(kpi)
            score = kpi.latest_score

        kpi.set_score(score)
    # logger.info('line 492')

    # if settings.ENABLE_CELERY:
    #     calculate_score_cascade_from.delay(kpi.id,score)
    # else:

    refer_to = None
    try:
        refer_to = kpi.refer_to
    except KPI.DoesNotExist:  # pragma: no cover
        pass

    # continue calculate cascade_from , add into queue, if failed then will be retried.
    if refer_to and refer_to is not kpi:
        add_new_cascadeKPIQueue(refer_to, calculate_now_thread=False)

    return kpi

    # if self.sleep:
    # time.sleep(self.sleep)
    #


def update_default_real(actor, kpi_id, default_real):
    kpi = update_kpi(actor, kpi_id,
                     default_real=fix_input_float(default_real),
                     month_1=fix_input_float(default_real),
                     month_2=fix_input_float(default_real),
                     month_3=fix_input_float(default_real),
                     real=fix_input_float(default_real))

    return update_score(kpi, actor, kpi.month_1, kpi.month_2, kpi.month_3, kpi.real)


def update_year_kpis(actor, kpis):  # pragma: no cover
    for k in kpis:
        id = k[0]
        # print "year target = {}".format(k[12])
        # print "year_result = {}".format(k[13])
        # print "year_score = {}".format(k[14])
        # print "year_data = {} . len = {}".format(k[15:], len(k[15:]))

        data = k[15:15 + 24]  # 24 columne are : month 1 target, month 1 result.... month 12 target, month 12 result
        year_data = {}

        year_data['m1_target'] = fix_input_float(data[0])
        year_data['m1_result'] = fix_input_float(data[1])

        year_data['m2_target'] = fix_input_float(data[2])
        year_data['m2_result'] = fix_input_float(data[3])

        year_data['m3_target'] = fix_input_float(data[4])
        year_data['m3_result'] = fix_input_float(data[5])

        year_data['m4_target'] = fix_input_float(data[6])
        year_data['m4_result'] = fix_input_float(data[7])

        year_data['m5_target'] = fix_input_float(data[8])
        year_data['m5_result'] = fix_input_float(data[9])

        year_data['m6_target'] = fix_input_float(data[10])
        year_data['m6_result'] = fix_input_float(data[11])

        year_data['m7_target'] = fix_input_float(data[12])
        year_data['m7_resul t'] = fix_input_float(data[13])

        year_data['m8_target'] = fix_input_float(data[14])
        year_data['m8_result'] = fix_input_float(data[15])

        year_data['m9_target'] = fix_input_float(data[16])
        year_data['m9_result'] = fix_input_float(data[17])

        year_data['m10_target'] = fix_input_float(data[18])
        year_data['m10_result'] = fix_input_float(data[19])

        year_data['m11_target'] = fix_input_float(data[20])
        year_data['m11_result'] = fix_input_float(data[21])

        year_data['m12_target'] = fix_input_float(data[22])
        year_data['m12_result'] = fix_input_float(data[23])

        kpi = get_kpi(id, actor)

        update_kpi(actor, id,
                   year_target=fix_input_float(k[12]),
                   year_result=fix_input_float(k[13]),
                   year_score=fix_input_float(k[14]),
                   year_data=year_data,
                   )

        # To make sure we update all KPIs with the same signature
        kpi.update_unique_code_with_same_signature()
        update_kpi_same_unique_code(kpi)

    # TODO: add backup-result

    return kpis


def update_kpi(actor, kpi_id, **d):
    kpi = KPI.objects.filter(id=kpi_id).first()

    if not kpi:
        raise ObjectDoesNotExist
        # return KPI()

    res = {'status': 'successful', 'message': ugettext("Successful!")}

    # if allow_edit_kpi(kpi, actor):

    if has_perm(KPI__EDITING, actor, kpi.user):
        if d.get('weight') and kpi.weight != float(d['weight']):
            if not has_perm(KPI__WEIGHT_EDIT, actor, kpi.user):  # prgama: no cover # permission checked already
                raise PermissionDenied(ugettext("Only managers can edit."))

            # kpi weight is >= 0
            elif float(d['weight']) <= 0:
                res = {'status': 'failed', 'message': ugettext('Weight is not smaller zero')}
                return res

        if kpi.weight == 0 and d.get('weight') > 0:
            raise PermissionDenied(ugettext('This KPI was delayed can not be changed weight'))

        res = KPIServices().update_kpi(actor, kpi, de_quy_xuong=False, de_quy_len=False, **d)

        return res
    else:
        raise PermissionDenied("Permission denied")


@transaction.atomic
def clone_kpi_to_user(from_user, to_user, actor, delete_current_kpis=False, copy_refer_to=True, only_kpi_ids=[]):
    if not has_perm(KPI__EDITING, actor, to_user):
        raise PermissionDenied

    current_quarter = from_user.profile.get_current_quarter()

    logger.info('delete_current_kpis == True')

    if delete_current_kpis == True:
        remove_all_kpi(to_user, actor)

    if only_kpi_ids == []:
        kpis = from_user.profile.get_kpis_parent(ordered=False)
    else:
        kpis = KPI.objects.filter(id__in=only_kpi_ids)

    for kpi in kpis:
        temp = kpi.clone_to_user(to_user=to_user, new_quarter_period=current_quarter, copy_refer_to=copy_refer_to)

    return True


def clone_kpi_full(kpi, new_parent=None, new_refer_to=None):
    k = KPI.objects.get(id=kpi.id)
    # Khang add improve duplicate KPIs
    new_group_id = k.get_group().id if k.get_group() else None

    k.id = None
    k.pk = None
    k.unique_key = None
    k.hash = None

    if new_parent:
        k.parent = new_parent
    if new_refer_to and k.kpi_type_v2() != kpi_trung_gian:
        k.refer_to = new_refer_to

    k.save()

    # Khang add improve duplicate KPIs
    organization = k.user.profile.get_organization()
    old_group_id = k.get_group().id if k.get_group() else None
    from performance.services import strategymap
    strategymap.StrategyMapServices.update_group_kpi(organization.id, k.id, new_group_id, old_group_id,
                                                     get_system_user())
    k.save()

    return k

#
# def update_data_by_code(user, kpi_code, real, month):
#     '''
#     :param user:
#     :param kpi_code:
#     :param real:
#     :param month:
#     :return:
#     '''
#
#     '''
#     -1: user not
#     -2
#     1
#     '''
#     return {'status': 'success', 'message': 'dddd'}
#     return {'status': 'error', 'message': 'dddd'}
#     return {'status': 'error', 'message': 'dddd'}
#


def copy_attrs_to_children(kpi, actor):
    # map(clone_attrs, [ (kpi, child,actor, False  ) for child in  kpi.get_children() ])
    if not kpi:
        return False

    children_refer = kpi.get_children_refer()

    def clone_attrs_tuple(t):
        k, o, a, u = t
        clone_attrs(k, o, a, u)

    map(clone_attrs_tuple, [(kpi, child, actor, False) for child in children_refer])
    print "*" * 50
    print "clone_attrs_tuple called"
    print "*" * 50
    map(lambda child: copy_attrs_to_children(child, actor), children_refer)


def copy_to_new_user(kpi, to_user, new_parent_kpi, actor):
    # if not can_edit(to_user, actor):
    if not to_user:
        return False

    if not has_perm(KPI__EDITING, actor, to_user):
        return False  # raise PermissionDenied

    if not kpi:
        return False

    kpi_type = kpi.kpi_type_v2()

    quarter_period = to_user.profile.get_organization().get_current_quarter()

    if kpi_type == kpi_cha_normal or kpi_type == kpi_cha_duoc_phan_cong:
        new_parent = clone_kpi_full(kpi)
        new_parent.user = to_user

        new_parent.refer_to = new_parent_kpi
        new_parent.parent = new_parent_kpi
        new_parent.owner_email = to_user.email
        new_parent.latest_score = None
        new_parent.real = None

        new_parent.quarter_period = quarter_period

        new_parent.save()

        children = kpi.get_children()
        if children.count() > 0:
            new_parent.refer_to = None
            new_parent.parent = None
            new_parent.save()

            for k in children:
                c = copy_to_new_user(k, to_user, new_parent, actor)

        return new_parent

    if kpi_type == kpi_con_normal or kpi_type == kpi_trung_gian:
        new_kpi = clone_kpi_full(kpi, new_parent_kpi, new_parent_kpi)
        new_kpi.user = to_user
        if new_parent_kpi:
            new_kpi.refer_to = new_parent_kpi
            new_kpi.parent = new_parent_kpi
        else:
            new_kpi.refer_to = None
            new_kpi.parent = None

        new_kpi.owner_email = to_user.email
        new_kpi.latest_score = None
        new_kpi.real = None
        new_kpi.quarter_period = quarter_period

        new_kpi.save()

        return new_kpi

    return False  # pragma: no cover


def duplicate_kpi(kpi, actor, new_parent=None, new_refer_to=None):
    #  print "kpi.user {}".format(kpi.user)
    #  print "actor {}".format(actor)

    if not kpi:
        return False

    # if kpi and not can_edit(kpi.user, actor):
    if kpi and not has_perm(KPI__EDITING, actor, kpi.user):

        if kpi.refer_to and kpi.refer_to.user:
            # if not can_edit(kpi.refer_to.user, actor):  # kpi.user.profile.get_organization() != actor.profile.get_organization():
            if not has_perm(KPI__EDITING, actor, kpi.refer_to.user):
                return False
        else:
            return False

    kpi_type = kpi.kpi_type_v2()

    if kpi_type == kpi_con_normal:
        return clone_kpi_full(kpi, new_parent, new_parent)

    if kpi_type == kpi_cha_normal:
        new_parent = clone_kpi_full(kpi)

        for k in kpi.get_children_refer():
            c = duplicate_kpi(k, k.user, new_parent, new_parent)

        return new_parent

    if kpi_type == kpi_cha_duoc_phan_cong:
        new_parent = clone_kpi_full(kpi)

        new_parent.cascaded_from = None
        new_parent.save()

        if not new_refer_to: new_refer_to = kpi.refer_to
        new_parent.assign_up(new_refer_to,
                             force_run=True)  # we should force_run assign_up when duplicate kpi despite of parent kpi is delayed.

        for k in kpi.get_children_refer():
            c = duplicate_kpi(k, k.user, new_parent, new_parent)

        return new_parent

    if kpi_type == kpi_trung_gian:
        '''
        not allow to duplicate kpi_trung_gian
        '''
        raise PermissionDenied
        k = clone_kpi_full(kpi, new_parent, new_parent)

        # k.assign_to(kpi.user)

        # k.refer_to = k.parent
        k.save()

        return k

        #
        # def calculate_cascade_score(kpi, actor=None):
        #     return calculate_score_cascading(kpi.id)
        # return KPIServices().calculate_cascade_score(kpi)


@postpone
def remove_kpi_thread(kpi_id, actor=None):  # pragma: no cover (multithread)
    if remove_kpi(kpi_id, actor) == False:
        kpi = get_kpi(kpi_id, actor)
        send_notification(actor, actor, recipient=actor,
                          verb=DELETE_KPI_FAILED,
                          target=actor,
                          action_object=kpi,
                          description=u"Delete Failed for KPI: {}".format(kpi.name),
                          organization=actor.profile.organization.id)
        return False
    return True


def remove_all_kpi(user, actor):
    if has_perm(KPI__EDITING, actor, user):
        quarter = user.profile.get_current_quarter()
        KPI.objects.filter(user_id=user.id, quarter_period_id=quarter.id).delete()
    else:
        raise PermissionDenied


# @memoize(30) #memoize to avoid duplicate
def log_delete_kpi(actor, kpi):
    if not kpi or not actor:
        return False

    key = 'log_delete_kpi{}'.format(kpi.id)

    if cache.get(key, None):
        return  # memoize to avoid duplicate

    cache.set(key, True, 30)

    #    logger.info(key)

    try:
        KPILogger(actor, kpi, 'delete', kpi.get_name()).run()
    except Exception as e:
        #  print traceback.format_exc()
        pass

    add_auto_comment(actor, kpi, ugettext("Delete KPI") + " #{0} - {1}".format(kpi.id, kpi.name))


def remove_kpi(kpi_id, actor=None, within_recursive=False):
    '''
    if within_recursive is True: --> do not need to check for permission

    '''

    system_user = get_system_user()

    if not (type(kpi_id) == type(KPI())):

        kpi = KPI.objects.filter(id=kpi_id).first()  # , unique_key=unique_key)
    else:
        kpi = kpi_id

    if kpi:

        if actor is None:  #

            if hasattr(KPI, '_request_user'):
                # good, but we probably generalize this not just attach to KPI
                request_user = KPI._request_user  # KPI._request_user is set in performance.middleware.WhodidMiddleware

                actor = request_user

                # organization = actor.profile.get_organization()
        # Khang add this condition
        _has_perm = has_perm(DELETE_KPI, actor, kpi.user, kpi)
        print "_has_perm", _has_perm
        if within_recursive == False and actor.email != system_user.email and not _has_perm:
            print 'no permission: line 757'
            print 'actor: {}'.format(actor)
            return False

        log_delete_kpi(actor, kpi)

        # try:
        #     KPILogger(actor, kpi, 'delete', kpi.get_name_current_goal()).run()
        # except Exception as e:
        #     #  print traceback.format_exc()
        #     pass

        # children = list(kpi.get_children())
        # children.extend(list(kpi.get_children_refer()))
        children = list(kpi.get_children_refer())
        parent = None
        cascaded_from = None
        try:
            parent = kpi.parent
        except KPI.DoesNotExist:
            parent = None

        try:
            cascaded_from = kpi.cascaded_from
        except KPI.DoesNotExist:
            parent = None

        kpi.delete()

        for child in children:
            if child.assigned_to_id:
                cascaded_kpi = KPI.objects.filter(user_id=child.assigned_to_id,
                                                  cascaded_from=child).first()
                if cascaded_kpi:  # and cascaded_kpi.get_children().filter(assigned_to_id__isnull=True):
                    for c in cascaded_kpi.get_children():
                        log_delete_kpi(actor, c)
                        # KPILogger(actor, c, 'delete', c.get_name_current_goal()).run()

                    log_delete_kpi(actor, cascaded_kpi)

                    #   KPILogger(actor, cascaded_kpi, 'delete',
                    #            cascaded_kpi.get_name_current_goal()).run()

                    remove_kpi(cascaded_kpi.id, actor, within_recursive=True)

                    # cascaded_kpi.delete()
            log_delete_kpi(actor, child)
            # KPILogger(actor, child, 'delete', child.get_name_current_goal()).run()
            remove_kpi(child, actor, within_recursive=True)

        if cascaded_from:
            try:
                # cascaded_from = kpi.cascaded_from
                log_delete_kpi(actor, cascaded_from)

                #  KPILogger(actor, cascaded_from, 'delete',
                #           cascaded_from.get_name_current_goal()).run()
                if cascaded_from.parent_id:
                    #                        CascadeKPIQueue.objects.create(kpi=cascaded_from.parent)
                    add_new_cascadeKPIQueue(kpi=cascaded_from.parent)
                    # cascaded_from.delete()
                    remove_kpi(cascaded_from.id, actor, within_recursive=True)
            except Exception as e:
                # print traceback.format_exc()
                pass

        if parent:
            # parent = kpi.parent

            final_score, m1, m2, m3 = parent.calculate_avg_score(parent.user_id)
            if final_score:
                parent.set_score(final_score)  # , parent.user_id)
                # parent.set_score(final_score, parent.user.profile.report_to_id)
                # parent.set_score(final_score,
                #                                 parent.user.profile.parent.user_id if parent.user.profile.parent else None)
                # CascadeKPIQueue.objects.create(kpi=parent)
                add_new_cascadeKPIQueue(kpi=parent)

            if are_other_children_delayed(kpi):
                parent.delay_toggle(user=actor, recursive=False, force_to_state='delay', ignore_perm_check=True)
                # TODO: discuss later about the logic here.

        return True

        # print 'kpi deleted'

    return True


@postpone
def re_order_thread(kpi):
    return re_order(kpi)


def re_calculate(kpi):
    if not kpi:
        return False

    return calculate_score_cascading(kpi.id)
    # return KPIServices().calculate_cascade_score(kpi)

    #
    # refer_to = None
    # try:
    #     refer_to = kpi.refer_to
    # except:
    #     pass
    #
    # while refer_to != None:
    #
    #     KPIServices().calculate_cascade_score(refer_to)
    #
    #     refer_to = None
    #     try:
    #         refer_to = refer_to.refer_to
    #     except:
    #         refer_to = None


def re_order(kpi):
    if not kpi:
        return False
    if (kpi.parent or kpi.refer_to):

        try:
            parent = kpi.parent
        except KPI.DoesNotExist:  # pragma: no cover
            return None
            parent = None

        if not parent:
            parent = kpi.refer_to

        children = parent.get_children()

    else:
        children = KPI.objects.filter(user=kpi.user, parent__isnull=True, bsc_category=kpi.bsc_category,
                                      quarter_period=kpi.quarter_period)
    # children_no_id =[]
    for child in children:
        # notify_slack(SLACK_DEV_CHANNEL, 'update cascade_from prefix_id failed ID: {0} - Error: {1}'.format(kpi.id, str(e)))
        try:
            child.ordering = int(child.ordering)
        except:
            child.ordering = children.count()

    sorted_children = sorted(children, key=lambda x: x.get_kpi_id())

    i = 1
    for child in sorted_children:
        #  if not child.cascaded_from:
        child.ordering = i
        child.set_prefix_id()

        child.save()

        child.set_assigned_kpi_attrs()
        i += 1

        child_child = child.get_children_refer().first()
        if child_child: re_order_thread(child_child)


# @memoize(15)
def get_kpi_cache(id, actor=None):
    kpi = KPI.objects.filter(id=id).select_related('refer_to', 'user', 'cascaded_from').first()
    if kpi:
        kpi.actor = actor
    return kpi


def get_unique_code(id, actor=None):
    kpi = get_kpi(id, actor)

    if not kpi:
        return ''

    if kpi and not kpi.unique_code:
        kpi.unique_code = str(kpi.id) + "_" + random_string(l=5, alphabet="0123456789abcdefghijklmnopqrstuvwxy")
        kpi.save()

    return kpi.unique_code


def get_kpi(id, actor=None):
    kpi = KPI.objects.filter(id=id).select_related('refer_to', 'user').first()
    return kpi
    # if kpi and can_view(kpi.user, actor):
    #     return kpi
    # else:
    #     return None


def get_kpi_by_kwargs(**kwargs):
    kpi = KPI.objects.filter(**kwargs).select_related('refer_to', 'user').first()
    return kpi


def get_kpi_template(code, actor, user=None):
    organization = actor.profile.get_organization()
    quarter = organization.get_current_quarter()

    kpi = KPI.objects.filter(quarter_period=quarter, assigned_to__isnull=True, name__icontains=code)

    if user:
        kpi = kpi.filter(user=user)

    kpi = kpi.order_by(
        'id').first()

    return kpi


def get_deleted_kpi(id, actor):
    # kpi = KPI.objects.filter(id=id).first()
    kpi = KPI.deleted_objects.filter(id=id).first()

    # if kpi and can_view(kpi.user, actor):
    return kpi


def filter_kpi_by_quarter(quarter_period, actor):
    return KPI.objects.filter(quarter_period=quarter_period)


def get_kpi_by_hash(hash, actor):
    kpi = KPI.objects.filter(hash=hash).first()

    if kpi and can_view(kpi.user, actor):
        return kpi
    else:
        return None


def get_kpi_by_name_user(name, user, quarter_period, actor):
    name = name.replace('\n', '').replace('\r', '').strip()
    kpi = KPI.objects.filter(name__icontains=name, user=user, quarter_period=quarter_period).order_by(
        'id').first()  # Khang note: need to be order_by (id)
    # kpi = KPI.objects.filter(name__icontains=name, user=user, quarter_period=quarter_period).first() # Khang note: need to be order_by (id)

    if kpi and can_view(kpi.user, actor):
        return kpi
    else:
        return None


def get_kpi_by_code_user(code, user, quarter_period, actor):
    kpi = KPI.objects.filter(code__icontains=code, user=user, quarter_period=quarter_period).first()

    if kpi and can_view(kpi.user, actor):
        return kpi
    else:
        return None


def get_kpi_from_unique_key(unique_key):
    return KPI.objects.filter(unique_key=unique_key).first()


def get_ecal_link(unique_key):
    kpi = get_kpi_from_unique_key(unique_key)
    if not kpi:
        return ""

    try:
        cascaded_from = kpi.cascaded_from
        if cascaded_from is not None:
            kpi = cascaded_from
    except ObjectDoesNotExist:  # pragma: no cover
        pass

    unique_key = kpi.get_unique_key()

    ecal, created = Ecal.objects.get_or_create(kpi_unique_key=kpi.unique_key)

    return ecal.get_ethercalc()


def search_kpi(query, actor):  # pragma: no cover # not used -> confi
    org = actor.profile.get_organization()
    org_quarter = org.get_current_quarter()
    lib_org = Organization.objects.filter(id=settings.LIBRARY_ORGANIZATION_ID).first()
    lib_org_quarter = None
    if lib_org:
        lib_org_quarter = lib_org.get_current_quarter()

    no_lib = query.find('no_lib') >= 0
    only_lib = query.find('only_lib') >= 0

    query = query.replace('no_lib', '').replace('only_lib', '')

    kpis = KPI.objects

    if org.id != settings.LIBRARY_ORGANIZATION_ID:  # filtering

        if lib_org_quarter:

            if not no_lib and not only_lib:

                kpis = kpis.filter(Q(quarter_period=org_quarter) | Q(quarter_period=lib_org_quarter))
            elif no_lib:
                kpis = kpis.filter(quarter_period=org_quarter)
            else:
                kpis = kpis.filter(quarter_period=lib_org_quarter)

        else:
            kpis = kpis.filter(quarter_period=org_quarter)

    query = query.strip()

    if query.find('name:') >= 0:
        query = query.replace('name:', '')
        if settings.IS_POSTGRES:
            kpis = kpis.filter(user__profile__display_name__unaccent__icontains=query)

        else:
            kpis = kpis.filter(user__profile__display_name__icontains=query)
    elif query.find('title:') >= 0:
        query = query.replace('title:', '')
        if settings.IS_POSTGRES:
            kpis = kpis.filter(user__profile__title__unaccent__icontains=query)
        else:
            kpis = kpis.filter(user__profile__title__icontains=query)

    elif query.find('department:') >= 0:
        query = query.replace('department:', '')
        if settings.IS_POSTGRES:
            kpis = kpis.filter(user__profile__department__unaccent__icontains=query)
        else:
            kpis = kpis.filter(user__profile__department__icontains=query)

    elif len(query) >= 1:
        if settings.IS_POSTGRES:

            kpis = kpis.filter(Q(name__unaccent__icontains=query)
                               # | Q(name_vi__unaccent__icontains=query)
                               # | Q(name_en__unaccent__icontains=query)
                               | Q(unit__unaccent__icontains=query)
                               #       | Q(current_goal__unaccent__icontains=query)
                               #        | Q(future_goal__unaccent__icontains=query)
                               )
            # wait for Django 1.10
            # kpis = kpis.filter(Q(name__search=query)
            #                    | Q(name_vi__icontains=query)
            #                    | Q(name_en__icontains=query)
            #                    | Q(description__search=query)
            #                    | Q(current_goal__search=query)
            #                    | Q(future_goal__search=query)
            #                    )
        else:
            kpis = kpis.filter(Q(name__icontains=query)
                               # | Q(name_vi__icontains= query)
                               # | Q(name_en__icontains= query)
                               | Q(unit__icontains=query)
                               #          | Q(current_goal__icontains=query)
                               #        | Q(future_goal__icontains=query)
                               )
    else:
        kpis = kpis.all()

        # kpis = kpis.order_by('name')[:1000]
    if settings.IS_POSTGRES and len(query) >= 3:
        # ref: https://docs.djangoproject.com/en/dev/ref/models/querysets/#distinct
        # kpis = kpis.order_by('name_vi', 'name_en', 'unit').distinct('name_vi', 'name_en', 'unit')  # remove duplicate if same unit
        kpis = kpis.order_by('unit')  # .distinct('name_vi', 'name_en', 'unit')  # remove duplicate if same unit
    kpis = kpis[:150]

    return kpis


def create_child_kpi(parent, quarter, name=None, actor=None):
    if actor and parent.user:
        if not has_perm(KPI__EDITING, actor, parent.user) and actor.profile.parent.user != parent.user:
            raise PermissionDenied

    if not name:
        name = parent.name

    if parent.weight <= 0:
        raise PermissionDenied('You do not have permission to create new KPI')
    k = KPIServices().create_kpi(creator=actor,  # year=year,
                                 quarter_period=quarter,
                                 refer_to_id=parent.id,
                                 name=name,
                                 unit=parent.unit,
                                 operator=parent.operator,
                                 current_goal=parent.current_goal,
                                 future_goal=parent.future_goal,
                                 quarter_one_target=parent.quarter_one_target,
                                 quarter_two_target=parent.quarter_two_target,
                                 quarter_three_target=parent.quarter_three_target,
                                 quarter_four_target=parent.quarter_four_target,
                                 year_target=parent.year_target,
                                 archivable_score=parent.archivable_score,
                                 score_calculation_type=parent.score_calculation_type,
                                 bsc_category=parent.bsc_category,
                                 )  # add_other_kpi=add_other_kpi,
    return k


def create_kpi(creator=None, name='', bsc_category="financial", ordering=None, unit='', quarter_period=None,
               owner_email=None, user=None,
               year=0, operator=">=", year_data={},
               year_target=None, quarter_one_target=None, quarter_two_target=None, quarter_three_target=None,
               quarter_four_target=None, user_id=None,
               target=None, current_goal='', parent=None, parent_id=None, refer_to=None, quarter=None,
               future_goal='',
               refer_to_id=None, description="", is_owner=True, end_date=None, archivable_score=8,
               score_calculation_type='most_recent', code='', weight=10, group_id=None, **kwargs):
    if not creator:
        raise PermissionDenied
    if user and not has_perm(KPI__EDITING, creator, user):
        raise PermissionDenied

    return KPIServices().create_kpi(creator=creator, name=name,
                                    bsc_category=bsc_category, ordering=ordering, unit=unit,
                                    quarter_period=quarter_period,
                                    owner_email=owner_email, user=user,
                                    year=year, operator=operator,
                                    year_target=year_target,
                                    quarter_one_target=quarter_one_target,
                                    quarter_two_target=quarter_two_target,
                                    quarter_three_target=quarter_three_target,
                                    quarter_four_target=quarter_four_target, user_id=user_id,
                                    target=target, current_goal=current_goal, parent=parent,
                                    parent_id=parent_id, refer_to=refer_to,
                                    quarter=quarter,
                                    future_goal=future_goal,
                                    refer_to_id=refer_to_id, description=description, is_owner=is_owner,
                                    end_date=end_date, archivable_score=8,
                                    score_calculation_type=score_calculation_type,
                                    code=code,
                                    weight=weight,
                                    year_data=year_data,
                                    group_id=group_id)


# @lru_cache_function(max_size=1024, expiration=30)
# @memoize(20)
def calculate_weighted_weight_by_kpi(kpi):
    return calculate_weighted_weight(kpi.id)
    # return kpi.get_weighted_weight()


# @lru_cache_function(max_size=1024, expiration=30)
@memoize(20)
def calculate_weighted_weight(kpi_id):
    kpi = get_kpi(kpi_id, get_system_user())
    # print kpi.name
    # print 'not caching'
    return kpi.get_weighted_weight()


# @lru_cache_function(max_size=1024, expiration=30)
@memoize(20)
def calculate_weighted_weight_assigner_view(kpi_id):
    kpi = get_kpi(kpi_id, get_system_user())

    try:
        cascaded_from = kpi.cascaded_from
    except KPI.DoesNotExist:
        pass
    if cascaded_from is None:
        cascaded_from = kpi

    return cascaded_from.get_weighted_weight()


def export_kpicomment_to_excel(kpi, count=20):
    kcs = KpiComment.objects.filter(kpi=kpi, send_from_ui='web').order_by('-id')
    if kcs.count() > 0:
        return reduce(lambda x, y: x + y,
                      ["[{}] : {}\n\n".format(i.created_at.strftime('%d/%b/%Y'), i.content) for i in kcs])

    return ""


@lru_cache_function(max_size=1024, expiration=60)
def get_edge_kpi(kpi_id):
    kpi = get_kpi(kpi_id, get_system_user())

    assigned_kpi = kpi.get_assigned_kpi()
    if assigned_kpi:
        return get_edge_kpi(assigned_kpi.id)

    children = kpi.get_children()

    if children.count() == 1:
        child = children[0]
        return get_edge_kpi(child.id)
    else:

        return kpi


def delele_all_kpis(user_id, actor, permission=False):
    user = get_user_by_id(user_id)

    # if user and actor.profile.is_superuser() or permission:
    if has_perm(MANAGER_REVIEW, actor, user) or permission:
        kpis = user.profile.get_kpis_parent()
        map(lambda kpi: remove_kpi(kpi, actor), kpis)
        return True

    else:
        raise PermissionDenied


def recheck_score(user):
    kpis = user.profile.get_kpis_parent()
    map(recheck_score_kpi, kpis)


def recheck_score_kpi(kpi):
    map(recheck_score_kpi, kpi.get_children_refer())
    logger.info("recheck_score_kpi {}".format(kpi.id))

    kpi.month_1_score = kpi.set_month_1_score(save=False)
    kpi.month_2_score = kpi.set_month_2_score(save=False)
    kpi.month_3_score = kpi.set_month_3_score(save=False)

    kpi.save()


def update_score(kpi, user, month_1, month_2, month_3, real):
    '''
     Set review_type = "monthly" when there is month_1 or month_2 or month_3 result
    '''
    if kpi.weight == 0:
        raise PermissionDenied(ugettext("You can't review the postponed KPIs"))

    kpi = set_review_type(kpi, fix_input_float(month_1), fix_input_float(month_2), fix_input_float(month_3))

    real = fix_input_float(real)
    kpi.real = real

    track_change(user, kpi)

    # ## Hot fix for case missing score_calculation_type, performance over max limit score
    score_calculation_type = kpi.score_calculation_type
    if not score_calculation_type:
        kpi.score_calculation_type = "most_recent"
        kpi.save(update_fields=["score_calculation_type"])

    kpi = kpi.save_calculate_score(calculate_now_thread=False)

    if kpi:
        return kpi

    return None


def update_score_kpi_code(score, quarter_period, unique_code=None):
    if not unique_code:
        unique_code = score.get('unique_code', '')

    kpis = KPI.objects.filter(unique_code=unique_code, quarter_period=quarter_period).exclude(
        unique_code=None).exclude(unique_code='')
    month = int(score.get('month', 0)) % 3

    system_user = get_system_user()
    if month == 0:
        month = 3
    list_kpi = []

    for k in kpis:
        if k.weight == 0:
            raise PermissionDenied(ugettext("You can't review the postponed KPIs"))

        kpi = {
            "id": k.id,
            "name": k.name,
            "score": score.get('value', ''),
            "month": score.get('month', ''),
            "user": k.owner_email
        }
        if month == 1:
            k.month_1 = score.get('value', '')
            kpi['score'] = k.month_1
        elif month == 2:
            k.month_2 = score.get('value', '')
            kpi['score'] = k.month_2
        else:
            k.month_3 = score.get('value', '')
            kpi['score'] = k.month_3

        track_change(system_user, k)
        k.save(update_fields=['month_2', 'month_1', 'month_3'])

        # ## recalculate score
        k = k.save_calculate_score(calculate_now_thread=False)
        list_kpi.append(kpi)

    return list_kpi


def get_input_update_score(actor, kpi, data):
    try:
        if has_perm(KPI__REVIEWING, actor, kpi.user, kpi):
            operator = data['operator']

            real = data.get('real', None)

            unit = data.get('unit', '')

            month_1 = data.get('month_1')
            month_2 = data.get('month_2')
            month_3 = data.get('month_3')

            score_calculation_type = data.get('score_calculation_type', None)

            if score_calculation_type: kpi.score_calculation_type = score_calculation_type

            if real == '':
                kpi.review_type = "monthly"

            kpi.operator = operator

            if unit:
                kpi.unit = unit

            res = {
                'kpi': kpi,
                'month_1': month_1,
                'month_2': month_2,
                'month_3': month_3,
                'real': real
            }
            return res
        else:
            raise PermissionDenied
    except KeyError as e:
        return None


def create_new_kpi(actor, data):
    # ## get input
    user_id = data.get('user_id', None)
    if user_id:

        organization = actor.profile.get_organization()

        current_quarter = organization.get_current_quarter()

        uo = Profile.objects.get(user_id=user_id, organization=organization)
        if uo:
            parent_kpi = data.get('parent_kpi', None)

            year = current_quarter.year

            if parent_kpi:
                parent = KPI.objects.filter(id=parent_kpi).first()
            else:
                parent = None

            if parent_kpi and parent:

                kpi = create_child_kpi(parent, current_quarter, actor=actor)

            else:
                bsc_category = data['category']
                if not bsc_category:
                    return False
                kpi = KPIServices().create_kpi(name="", year=year,
                                               quarter_period=current_quarter,
                                               bsc_category=bsc_category,
                                               user_id=uo.user_id,
                                               owner_email=uo.user.email
                                               )

            return kpi
    return None


def get_kpi_parent_related(kpi_id):
    kpi = KPI.objects.select_related('parent').filter(pk=kpi_id).first()

    if kpi:
        return kpi

    return None


def get_kpi_parent_by_id(actor, parent_id):
    kpi = get_kpi(parent_id, actor)
    if kpi and can_view(kpi.user, actor):

        kpis = kpi.get_children_refer_order()
        kpis_json = []
        for k in kpis:
            kpis_json.append(k.to_json())
        return kpis_json


def get_kpi_by_id(actor, kpi_id):
    try:
        kpi_id = int(kpi_id)
    except:
        pass

    if isinstance(kpi_id, int):
        kpi = get_kpi(kpi_id, actor)

    else:
        kpi = get_kpi_by_hash(kpi_id, actor)

    if kpi and \
            can_view(kpi.user, actor):
        children = kpi.get_children()
        data = kpi.to_json(actor=actor)
        return data

    return None


def set_review_type(kpi, month_1, month_2, month_3):
    # Khang update condition to update month result
    # Must check lock month before update score
    try:

        # Khang note
        organization = kpi.user.profile.get_organization()
        if organization.enable_month_1_review():
            kpi.month_1 = month_1
        if organization.enable_month_2_review():
            kpi.month_2 = month_2
        if organization.enable_month_3_review():
            kpi.month_3 = month_3

        # Legacy code here
        if month_1 != '' and month_1 is not None:
            month = 1
            kpi.review_type = "monthly"
        if month_2 != '' and month_2 is not None:
            month = 2
            kpi.review_type = "monthly"
        if month_3 != '' and month_3 is not None:
            month = 3
            kpi.review_type = "monthly"
    except:
        return None
    return kpi


def get_one_kpicomment(key, kpi):
    changed = kpi.tracker.changed()
    keys = changed.keys()
    comment = ""
    if key in keys:
        title = kpi.get_custom_label(key) if kpi.get_custom_label(key) else kpi._meta.get_field(key).verbose_name
        blank = ugettext("Unknow") if key == "name" or key == "" else ugettext("None")
        old_value = changed[key] if changed[key] is not None else blank
        new_value = getattr(kpi, key) if getattr(kpi, key) is not None else ugettext("None")
        comment = u"%s: %s → %s" % (title, old_value, new_value)
    return comment


def track_change(actor, kpi, title=None):
    changed = kpi.tracker.changed()
    track = False
    changed_content = kpi.get_changed_history(False, u'\n')
    if changed and changed_content:
        track = True

    content = None
    if track:
        content = title if title else ugettext("Changed:") + ' \n' + changed_content
        '''
            Does not add comment about refer_group_name
        '''
        refer_group_name_comment = get_one_kpicomment('refer_group_name', kpi)
        if (u'- ' + refer_group_name_comment) in content:
            content = content.replace(u'- ' + refer_group_name_comment, '')
        add_auto_comment(actor, kpi, content)

        #  _changed = kpi.get_changed_history()
        # KPILogger(actor, kpi, 'update', _changed).start()


def convert_kpis_tojson(kpis, category=None):
    kpis_json = []
    for k in kpis:
        if category:
            if k.bsc_category == category:
                kpis_json.append(k.to_json(refer=True))
        else:
            kpis_json.append(k.to_json(refer=True))

    return kpis_json


def order_kpi(kpi_ids):
    count = 1
    for kpi_id in kpi_ids:
        try:
            kpi = KPI.objects.get(id=kpi_id)
            kpi.ordered = count
            kpi.save(update_fields=['ordered'])
            count += 1
        except:  # pragma: no cover
            pass


def get_kpi_of_organization(organization):
    if organization:
        return KPI.objects.filter(quarter_period=organization.get_current_quarter())
    else:
        []
