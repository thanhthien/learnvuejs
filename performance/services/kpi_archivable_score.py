# !/usr/bin/python
# -*- coding: utf-8 -*-
from performance.models import KPI
def update_archivable_score(kpi_id, archivable_score):
    k=KPI.objects.get(id=kpi_id)
    k.archivable_score=int(archivable_score)
    k.save()
