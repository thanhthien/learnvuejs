from django.core.exceptions import PermissionDenied, ObjectDoesNotExist

from authorization.rules import has_perm, SETTINGS__VIEW_ORG_CHART, REPORT_ADMIN
from performance.models import KPIBackup
from performance.services import convert_kpis_tojson
from performance.services.quarter import get_quarter
from user_profile.models import get_full_scores_v2


def get_kpibackups(actor, user, quarter_id, month=None):
    if (month):
        return KPIBackup.objects.filter(user=user, quarter_id=quarter_id, month=month)
    return KPIBackup.objects.filter(user=user, quarter_id=quarter_id).order_by('month')


def create_backup_kpis(actor, user, quarter_id, month):
    ''''
    Purpose of this function is to create a backup version for current KPIs and store into KPIBackup Table
    The information that needed to be stored is:
    - data: the current KPIs in json format
    - result: the current KPIs performance score for `month`
    '''

    if not has_perm(SETTINGS__VIEW_ORG_CHART, actor, user):
        raise PermissionDenied

    month = int(month)

    try:
        kbu = KPIBackup.objects.get(quarter_id=quarter_id, month=month, user=user)

        '''
        do not create new backup but return the current one
        '''

    except ObjectDoesNotExist:
        '''
        Get the backedup month performance result
        '''
        quarter = get_quarter(actor, quarter_id)
        kpi_percent, score_m1, score_m2, score_m3 = get_full_scores_v2(user.profile, quarter)

        '''
        create backup
        '''
        kbu = KPIBackup.objects.create(quarter_id=quarter_id, month=month, user=user)

        kbu.created_by = actor

        if month == 1:
            kbu.result = score_m1
        elif month == 2:
            kbu.result = score_m2
        elif month == 3:
            kbu.result = score_m3

        '''
        Get the KPIs json
        '''
        kpis = user.profile.get_kpis_parent()
        kpis_json = convert_kpis_tojson(kpis)
        kbu.data = kpis_json

        kbu.save()

    return kbu


def delete_backup_kpis(actor, user, quarter_id, month):
    if not has_perm(REPORT_ADMIN, actor, user):
        raise PermissionDenied

    try:
        kbu = KPIBackup.objects.get(quarter_id=quarter_id, month=month, user=user)
    except ObjectDoesNotExist:
        return None

    kbu.delete()

    return True
