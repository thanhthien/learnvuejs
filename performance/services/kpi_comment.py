import datetime
from django.core.paginator import Paginator, EmptyPage
from performance.models import KpiComment, KPI, MarkAsReadComment
from performance.services import get_all_object_kpi_by_unique_key


def group_comment_by_kpi(comments):
    kpi_comments = {}

    for c in comments:

        if not kpi_comments.get(c.kpi_id, None):
            kpi_comments[c.kpi_id] = ''

        kpi_comments[c.kpi_id] += '\r\n ' + c.content

    return kpi_comments


def group_comment_by_kpi_name(comments):
    kpi_comments = {}

    for c in comments:
        kpi = get_all_object_kpi_by_unique_key(c.kpi_unique_key)

        if not kpi:
            kpi_name = ''
        else:
            kpi_name = kpi.name

        if not kpi_comments.get(kpi_name, None):
            kpi_comments[kpi_name] = []

        kpi_comments[kpi_name].append(c.to_json())

    return kpi_comments


def get_kpi_comments_foruser(user, page=1, page_size=100):
    try:
        if not user:
            return []
        #
        # try:
        #     query = KpiComment.objects.all_objects(kpi__user = user)
        # except:

        #  query = KpiComment.objects.filter(kpi__user = user)
        query = KpiComment.objects.raw('SELECT performance_kpicomment.* FROM performance_kpicomment '
                                       'INNER JOIN performance_kpi ON performance_kpicomment.kpi_unique_key =performance_kpi.unique_key '
                                       'where performance_kpi.user_id = {} ORDER BY performance_kpicomment.id DESC'.format(
            user.id))

        # Use list to fix exception: object of type 'RawQuerySet' has no len()
        return Paginator(list(query), page_size).page(page)
    except Exception as e:
        print e
    return {}


def get_kpi_comments_forquarter(quarter, page=1, page_size=100):
    # try:
    #     query = KpiComment.objects.all_objects(kpi__quarter_period_id = quarter.id)
    # except:

    query = KpiComment.objects.raw('SELECT performance_kpicomment.* FROM performance_kpicomment '
                                   'INNER JOIN performance_kpi ON performance_kpicomment.kpi_unique_key =performance_kpi.unique_key '
                                   'where performance_kpi.quarter_period_id = {} ORDER BY performance_kpicomment.id DESC '.format(
        quarter.id))

    # query.order_by('-id')
    query = query[(page - 1) * page_size:  (page) * page_size]

    # try:
    #     return Paginator(query, page_size).page(page)
    # except EmptyPage:
    #     return []

    return query


def detect_data_kpi_comment(kpi_comment):
    arrays = []
    for note in kpi_comment:
        avatar = note.user.profile.get_avatar()
        note = note.to_json()
        note["avatar"] = avatar
        arrays.append(note)
    return arrays


def get_kpi_comments_by_kpi_id_and_unique_key(unique_key, kpi_id, user):
    if unique_key:
        if kpi_id:
            extra = {'id': kpi_id}
            kpi = KPI.objects.filter(unique_key=unique_key, **extra).order_by('-id').first()
        else:
            kpi = KPI.objects.filter(unique_key=unique_key).order_by('-id').first()

        if not kpi:
            kpi = KPI.deleted_objects.filter(unique_key=unique_key).order_by('-id').first()

    # MarkAsReadComment.objects.filter(kpi=kpi, read_user=request.user).update(read_date=datetime.datetime.now())
    MarkAsReadComment.objects.filter(kpi__unique_key=kpi.unique_key, read_user=user).update(
        read_date=datetime.datetime.now())

    return detect_data_kpi_comment(kpi.get_notes())
