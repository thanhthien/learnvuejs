# !/usr/bin/python
# -*- coding: utf-8 -*-
import json

from performance.db_models.kpi_library import KPILib, TagItem
from django.shortcuts import get_object_or_404
from django.db.models import Q
from tagging.models import Tag

from performance.services.notification import filter_contains


def get_kpilib(kpi_id):
    obj = get_object_or_404(KPILib, pk=kpi_id)
    obj.update_attribute()
    return obj


def get_list_kpilib(**kwargs):
    return KPILib.objects.filter(**kwargs)


def search_kpilib(query, tags=[]):
    kpis = KPILib.objects.none()
    if tags:
        kpis = KPILib.tagged.with_all(tags, KPILib.objects.all())

    if query:
        if tags:
            kpis = kpis.filter(Q(name__icontains=query) |
                               Q(objective__icontains=query) |
                               Q(description__icontains=query) |
                               Q(measurement_method__icontains=query))
        else:
            kpis = KPILib.objects.filter(Q(name__icontains=query) |
                                         Q(objective__icontains=query) |
                                         Q(description__icontains=query) |
                                         Q(measurement_method__icontains=query))

    return kpis


def get_kpilib_by_tags(tags=[]):
    return KPILib.tagged.with_all(tags)


def create_kpilib(objective, name, description, measurement_method, operator, category, tags=[], language='vi', **extra_data):
    '''

    :param category:
    :param language:
    :param objective:
    :param name:
    :param description:
    :param measurement_method:
    :param operator:
    :param tags:
        apple ball cat            [apple], [ball], [cat]         No commas, so space delimited
        apple, ball cat           [apple], [ball cat]            Comma present, so comma delimited
        “apple, ball” cat dog     [apple, ball], [cat], [dog]    All commas are quoted, so space delimited
        “apple, ball”, cat dog    [apple, ball], [cat dog]       Contains an unquoted comma, so comma delimited
        apple “ball cat” dog      [apple], [ball cat], [dog]     No commas, so space delimited
        “apple” “ball dog         [apple], [ball], [dog]         Unclosed double quote is ignored
    '''
    lib = KPILib(objective=objective,
                 name=name,
                 description=description,
                 measurement_method=measurement_method,
                 operator=operator,
                 category=category,
                 extra_data=extra_data,
                 language=language,
                 )

    lib.save()
    if tags:
        lib.tags = ",".join(tags) if len(tags) > 1 else '"{}"'.format(tags[0])
    return lib


def update_kpilib(kpi_id, tags=[], **data):
    '''

    :param kpi_id: ID of KPI Lib
    :param tags:
        apple ball cat            [apple], [ball], [cat]         No commas, so space delimited
        apple, ball cat           [apple], [ball cat]            Comma present, so comma delimited
        “apple, ball” cat dog     [apple, ball], [cat], [dog]    All commas are quoted, so space delimited
        “apple, ball”, cat dog    [apple, ball], [cat dog]       Contains an unquoted comma, so comma delimited
        apple “ball cat” dog      [apple], [ball cat], [dog]     No commas, so space delimited
        “apple” “ball dog         [apple], [ball], [dog]         Unclosed double quote is ignored
    '''
    lib = get_kpilib(kpi_id)
    extra_data = {}
    extra_fields = dict(KPILib.EXTRA_FIELDS)
    for (key, value) in data.iteritems():
        if key in KPILib._meta.get_all_field_names():
            setattr(lib, key, value)
        elif key in extra_fields and key != 'extra_data':
            extra_data[key] = value

    lib.extra_data.update(extra_data)
    if tags:
        lib.tags = ",".join(tags) if len(tags) > 1 else '"{}"'.format(tags[0])
    lib.save()
    return lib


def filter_contains(query):
    return Q(extra_data__icontains=json.dumps(query)[1:-1].replace(' ', '')) | Q(extra_data__icontains=query)


def search_kpi_library(data):
    kwargs = {}
    sort_values = ['name', '-created_at']
    field_extend = ['name', 'objective', 'measurement_method', 'all']
    kpi_libs = KPILib.objects.filter()

    if data.get('sub_category', ''):
        kpi_libs = kpi_libs.filter(
            filter_contains(
                {"sub_category_id": int(data['sub_category'])}))

    if data.get('extend_type', '') and data.get('extend_value', ''):
        if data['extend_type'] in field_extend:
            if data['extend_type'] == 'all':
                kpi_libs = kpi_libs.filter(
                    Q(name__icontains=data['extend_value']) | Q(objective__icontains=data['extend_value']) | Q(
                        measurement_method__icontains=data['extend_value']))
            else:
                kwargs[data['extend_type'] + '__icontains'] = data['extend_value']

    if data.get('language', ''):
        kwargs['language__icontains'] = data['language']

    if data.get('sort', '') in sort_values:
        return kpi_libs.filter(**kwargs).order_by(data['sort'])

    return kpi_libs.filter(**kwargs)


def get_function_tags():
    depts = list(TagItem.objects.filter(parent__isnull=True).values('name', 'id'))
    DEPARTMENTS = []
    for dept in depts:
        DEPARTMENT = {}
        # count = 0
        DEPARTMENT['childs'] = []
        DEPARTMENT['id'] = dept['id']
        DEPARTMENT['name'] = dept['name']
        childs = TagItem.objects.filter(parent_id=dept['id']).values('name', 'id', 'parent__name')
        for child in childs:
            DEPARTMENT_CHILD = {}
            DEPARTMENT_CHILD['id'] = child['id']
            DEPARTMENT_CHILD['name'] = child['name']
            DEPARTMENT_CHILD['count'] = KPILib.objects.filter(
                filter_contains({"sub_category_id": child['id']})).count()
            DEPARTMENT['childs'].append(DEPARTMENT_CHILD)
            # count += DEPARTMENT_CHILD['count']

        DEPARTMENT['count'] = KPILib.objects.filter(
            filter_contains({"function_category_id": dept['id']})).count()
        DEPARTMENTS.append(DEPARTMENT)
    return DEPARTMENTS


def get_function_category_name_by_id(id):
    functions = TagItem.objects.filter(id=id).first()
    if functions:
        return functions.name

    return None