# !/usr/bin/python
# -*- coding: utf-8 -*-
from performance.models import KVStore, ObjectDoesNotExist


def get_kvstore(key,user,  actor= None):

    #TODO: check perms

    return KVStore.objects.filter(key=key, user = user).last()


def create_kvstore(key, data, user,  actor= None):
    k = KVStore()
    k.key = key
    k.data = data
    k.user = user
    k.organization = user.profile.get_organization()
    k.created_by = actor

    k.save()

    return k


def update_kvstore(kv_store, data,  actor= None):
    k = get_kvstore(kv_store.key, user = kv_store.user, actor = actor)

    if not k:
        raise ObjectDoesNotExist

    k.data = data


    k.created_by = actor

    k.save()

    return k
