# -*- coding: utf-8 -*-
import json
import traceback

from auditlog.models import LogEntry
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.exceptions import PermissionDenied
from django.db import connection
from authorization.rules import has_perm, KPI__REVIEWING
from django.core.mail import EmailMessage
from django.core.urlresolvers import reverse
from django.template.loader import render_to_string
from django.utils.translation import ugettext

from elms.setting_modules.other_packages import LANGUAGES
from elms.settings import FROM_EMAIL
from notifications.models import Notification
from notifications.signals import notify
from elms import settings
from user_profile.services.profile import send_mail_with_perm
from user_profile.services.user import get_system_user
from utils import notify_slack
from utils.common import to_dict
from extrascore.services.exscore import get_extrascores_performance
from django.forms.models import model_to_dict
import logging
from django.db.models import Q

COMPLETE_REVIEW_VERB = "Complete KPI Review"
NEW_QUARTER_VERB = "New quarter is created"
NEW_COMMENT_VERB = "New comment"
DELETE_KPI_FAILED = "Delete KPI Failed"
USER_LOG_TYPE = "user_log"
SYSTEM_LOG_TYPE = "system_log"
CHANGE_LOG_TYPE = "change_log"

logger = logging.getLogger()


def notify_complete_review(user, actor):
    DOMAIN = settings.DOMAIN
    if DOMAIN.endswith("/"):
        DOMAIN = DOMAIN[:-1]

    if has_perm(KPI__REVIEWING, actor, user):
        context = {
            'kpis': user.profile.get_kpis_parent(),
            'profile': user.profile,
            'quarter_period': user.profile.get_organization().get_current_quarter(),
            'STATIC_URL': settings.STATIC_URL,
            'DOMAIN': DOMAIN,
            'KPI_EDITOR_EMP': reverse("kpi_editor_emp", kwargs={'user_id': user.id}),
            'organization': user.profile.get_organization(),
            'exscores_m1': round(get_extrascores_performance(actor, user.id)['1']['total'],2),
            'exscores_m2': round(get_extrascores_performance(actor, user.id)['2']['total'],2),
            'exscores_m3': round(get_extrascores_performance(actor, user.id)['3']['total'],2),
        }
        # Just send email when user has KPI
        # changed from count() to len() ==> len() works with both sql & postgres
        if len(user.profile.get_kpis_parent()) > 0:
            message = render_to_string('mail/performance/notification__complete_review.html', context)
            title = ugettext("Complete KPI Review") + ": {}".format(user.profile.display_name)
            to_list = [user.email]
            # if user.profile.report_to:
            if user.profile.parent and user.profile.parent.user:
                to_list.append(user.profile.parent.user.email)

            msg = send_mail_with_perm(subject=title,
                                      body=message,
                                      from_email=FROM_EMAIL,
                                      to=to_list
                                      )

            description = "{0} {1}. {2} {3}".format(user.profile.display_name,
                                                    ugettext("completed KPI Review"),
                                                    ugettext("Click this link to view result"),
                                                    "<a href='"+user.profile.get_url()+"' target='blank'>"+user.profile.get_url()+"</a>"
                                                    )
            # if user.profile.report_to_id > 0:
            if user.profile.parent:
                send_notification(actor, user,
                                  # recipient=user.profile.report_to,
                                  recipient=user.profile.parent.user if user.profile.parent else None,
                                  verb=COMPLETE_REVIEW_VERB,
                                  target=user,
                                  description=description, organization = actor.profile.organization.id)

        return True

    return False


def mark_read_all(actor):
    ns = get_all_unread_by_user(actor)
    for n in ns:
        n.mark_as_read()
    return True

    # return False


def mark_read(actor, id):
    n = Notification.objects.filter(id=id).first()
    if n and (
            n.recipient == actor or actor.is_staff or n.recipient.profile.get_organization() == actor.profile.get_organization()):
        n.mark_as_read()
        return True

    return False


def send_notification(actor, user, recipient, verb, description='', target=None, action_object=None, type='',
                      version='', release_date='', language='', organization=None):
    # TODO: check actor authorization
    if not actor:
        actor = get_system_user()
    if not recipient:
        recipient = actor
    if not user:
        user = actor
    return notify.send(actor, recipient=recipient,
                       verb=ugettext(verb),
                       target=target,
                       description=ugettext(description),
                       type=type,
                       actor_name=actor.profile.display_name,
                       version=version,
                       release_date=release_date,
                       language=language,
                       organization=organization
                       )


def get_all_unread():
    return Notification.objects.unread()


def get_all_unread_by_user(user):
    queryset = Notification.objects.filter(recipient=user, unread=True).order_by('-id')[
               :20]  # user.notifications.unread()
    # print 'query {}'.format(queryset.query)
    return queryset


def notification_to_list(notifications, organization):
    notification_list = []
    if notifications:

        for n in notifications:
            # if n.recipient.profile.get_organization() == organization:
            n_json = to_dict(n)
            n_json['to_email'] = n.recipient.email
            try:
                n_json['link'] = n.target.profile.get_url()
            except:
                pass
            notification_list.append(n_json)

    return notification_list


notifications_post_object_schema = {
    "type": "object",

    "properties": {

        "unread_list": {

            "type": ["array", "null"],

            "items": {
                "type": "integer"
            }

        }

    },
}


def get_unread_by_user_id(actor, user_id):
    # Todo: check permission
    notifications = get_notifications_by_user_id(actor=actor, user_id=user_id).filter(unread=True).values_list('id',
                                                                                                               flat=True)
    return notifications


def get_all_unread_by_user_id(actor, user_id):
    # Todo: check permission
    notifications = get_notifications_by_user_id(actor=actor, user_id=user_id).filter(unread=True)
    return notifications


def get_notifications_by_user_id(actor, user_id, language='en'):
    # Todo: check permission
    # check permission
    if actor.profile.is_superuser_org():

        if actor.profile.organization:

            notifications = Notification.objects.filter(
                Q(recipient__id=user_id) |
                (filter_contains({'type': 'change_log'}) & filter_contains({'language': language})) |
                (filter_contains({'type': 'system_log'}) & filter_contains({'organization': actor.profile.organization.id})))\
                .exclude(Q(data__isnull=True) |
                    (filter_contains({'type': 'system_log'}) & Q(recipient__id=user_id))).order_by('-unread', '-timestamp')
        else:
            return Notification.objects.filter(id =0) #return []
    else:
        notifications = Notification.objects.filter(
            Q(recipient__id=user_id) |
            filter_contains({'type': 'change_log'}) & filter_contains({'language': language}))\
            .exclude(Q(data__isnull=True) |
                     filter_contains({'type': 'system_log'}))\
            .order_by('-unread', '-timestamp')

    return notifications


def mark_notifications_as_read(actor, user, unread_id_list=None, language='en'):
    # Todo: check permission
    if actor != user:
        raise PermissionDenied(ugettext("You can not mark as read for others"))
    notifications = get_notifications_by_user_id(actor=actor, user_id=user.id, language=language)
    notifications_list = notifications
    if unread_id_list:
        notifications_list = notifications.filter(id__in=unread_id_list)

    notifications_list.update(unread=False)

    # if notifications:
    #     if update_field == 'unread' and update_value:
    #         fields = {
    #             'unread': update_value == 'true'
    #         }
    #         notifications = notifications.filter(unread=update_value != 'true')
    #     for n in notifications:
    #         n_json = model_to_dict(n)
    #         notification_list.append(n_json['id'])
    #
    #     notifications.update(**fields)

    return notifications


def filter_contains(query):
    return Q(data__contains=json.dumps(query)[1:-1].replace(' ', '')) | Q(data__contains=query)


def import_changelog_notifications():
    import markdown2
    import os.path
    from bs4 import BeautifulSoup
    import re
    # ## detect Notification changelog
    for language in LANGUAGES:
        lang = language[0]
        path = r'changelog/changelog.{}.md'.format(lang)
        if os.path.exists(path):
            f = open(r'changelog/changelog.{}.md'.format(lang), 'r')
            f.readline()
            information = f.readline()
            soup = BeautifulSoup(markdown2.markdown(information), 'html5lib')
            if soup.find('h1'):
                text = soup.find('h1').text
                match_date = re.search(r'(\d+-\d+-\d+)', text)
                match_version = re.search(r'(\d+.\d+.\d+)', text)
                content = ''
                if match_date and match_version:
                    date = match_date.group(1)
                    version = match_version.group(1)
                    changelog = Notification.objects.filter(
                        filter_contains({'version': version}) & filter_contains({'language': lang}))
                    line = f.readline()
                    if not changelog:
                        while line:

                            if re.search(r'(<a name="\d+.\d+.\d+"></a>)', line):
                                break
                            content += markdown2.markdown(line)
                            line = f.readline()
                    if content:
                        send_notification(user=None, actor=None, recipient=None, description=content,
                                          type=CHANGE_LOG_TYPE,
                                          verb='',
                                          version=version, release_date=date, language=language[0])
                    print content

            f.close()


def add_notifications(model, organization):
    ct = None
    try:
        ct = ContentType.objects.get(model=model)
    except Exception as e :
        notify_slack("notificatin.py:294")
        notify_slack(traceback.format_exc())
        notify_slack(str(e))

    try:

        get_model = ct.model_class()
        auditlog = LogEntry.objects.filter(
            content_type=ct).order_by(
            '-timestamp').first()
        result = auditlog.changes_dict
        description = ''
        if result:
            description = ugettext("Update") + ':'
        for name in result:
            name_verbose = get_model._meta.get_field(name).verbose_name
            # change name_verbose to modify clearly
            description += u"\n- %s: %s → %s" % (ugettext(name_verbose), ugettext(result[name][0]), ugettext(result[name][1]))
        actor = auditlog.actor

        if not actor:
            actor = get_system_user()
        last_notification = Notification.objects.filter(actor_object_id=actor.id).order_by(
            '-timestamp').first()

        if last_notification and last_notification.description != description:
            send_notification(actor=actor, user=actor, recipient=actor,
                              description=description, verb='',
                              type=SYSTEM_LOG_TYPE, organization=organization.id)
    except Exception as e:
        notify_slack("notificatin.py:321")
        notify_slack(traceback.format_exc())
        notify_slack(str(e))
       # logger.info(traceback.print_exc())
       # logger.info(str(e))
        # logger.info(str(e))

def add_notifications_todo(actor, task):
    import datetime
    from todo.services.comments import add_auto_comment_task

    try:
        auditlog = task.history.all().order_by('-timestamp').first()
        result = auditlog.changes_dict
        list_description = []
        organization = actor.profile.get_organization()
        recipient = task.assigned_to if task.assigned_to is not None else actor
        comment_description = ''
        for name in result:
            if name == 'title':
                description = u"%s %s %s %s" % (
                ugettext('changed Task Name'),"<b>"+ugettext(result[name][0])+"</b>", ugettext('to'),'<b><a href="/performance/todo" target="blank">'+ugettext(result[name][1])+"</a></b>")
                list_description.append(description)

            if name == 'assigned_to':
                # Gui cho nguoi dam nhan moi
                if recipient != task.created_by:
                    _description = u"%s %s" % (
                    ugettext('assigned you a new task'),'<b><a href="/performance/todo" target="blank">'+task.title+"</a></b>")
                    send_notification(actor=actor, user=actor, recipient=recipient,
                                      description= str(_description), verb='',
                                      type=USER_LOG_TYPE, organization=organization.id)
                if task.assigned_to != task.created_by and recipient != task:
                    # Gui cho nguoi dam nhan cu
                    _description = u"%s %s" % (ugettext('canceled you as a person in charged of task'), '<b><a href="/performance/todo" target="blank">' + task.title + "</a></b>")
                    send_notification(actor=actor, user=actor, recipient=task.assigned_to,
                                      description=str(_description), verb='',
                                      type=USER_LOG_TYPE, organization=organization.id)

                # Them auto comment voi content phan cong cong viec nay cho ai
                comment_description = u"%s %s" % (
                     ugettext('assigned this task to'), '<b>' + recipient.profile.display_name + '</b>')

            if name == 'due_date':
                description = u"%s %s %s %s" % (
                ugettext('changed the due dated of task'),'<b><a href="/performance/todo" target="blank">'+task.title+"</b></a>", ugettext('to'),"<b>"+datetime.datetime.strptime(result[name][1], '%Y-%m-%d').strftime('%d/%m/%y')+"</b>")
                list_description.append(description)

            if name == 'kpi':
                _name = result[name][1]
                if _name == 'None':
                    _name = 'Do not belong to any KPI'
                description = u"%s %s %s %s" % (
                ugettext('changed the KPI of task'),'<b><a href="/performance/todo" target="blank">'+task.title+"</a></b>", ugettext('to'),"<b>"+ugettext(_name)+"</b>")
                list_description.append(description)

            if name == 'permission':
                description = u"%s %s" % (
                ugettext('set' + ' <b>“'+result[name][1]+'“</b> ' + 'permission for task') ,'<b><a href="/performance/todo" target="blank">'+task.title+"</a></b>")
                list_description.append(description)

            if name == 'status':
                description = u"%s %s %s %s" % (
                ugettext('changed status of task'),'<b><a href="/performance/todo" target="blank">'+task.title+"</a></b>", ugettext('to') ,'<b>\"'+ugettext(result[name][1])+'\"</b>')
                list_description.append(description)

            if name == 'note':
                description = u"%s %s %s %s" % (
                ugettext('updated note for task'),'<b><a href="/performance/todo" target="blank">'+task.title+"</a></b>", ugettext('to') ,'<b>\"'+ugettext(result[name][1])+'\"</b>')
                list_description.append(description)

            if name == 'archive':
                description = u"%s %s" % (
                ugettext('archived task'),'<b><a href="/performance/todo" target="blank">'+task.title+"</a></b>")
                list_description.append(description)

        if not actor:
            actor = get_system_user()

        result_notification = '\n'.join(list_description)

        last_notification = Notification.objects.filter(actor_object_id=actor.id).order_by(
            '-timestamp').first()

        if actor == recipient:
            recipient = task.created_by

        if last_notification and last_notification.description != result_notification and len(list_description) > 0:
                if actor!=recipient:#  for notifications won't send if actor is recipient
                    send_notification(actor=actor, user=actor, recipient=recipient,
                                  description=result_notification, verb='',
                                  type=USER_LOG_TYPE, organization=organization.id)

                if comment_description != '': # for notifications
                    list_description.append(comment_description)

                for description in list_description:
                    add_auto_comment_task(actor, task, description)




    except Exception as e:
        logger.info(traceback.print_exc())
        logger.info(str(e))
        # logger.info(str(e))