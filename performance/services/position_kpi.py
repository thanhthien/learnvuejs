import datetime
import hashlib

from performance.db_models.position_kpi import PositionKPI


def create_position_kpi(position_id, backup_type, kpis=[], score=None, employee_name=None, employee_email=None):

    if position_id and backup_type:
        json_data = {
            'score': score,
            'employee_name': employee_name,
            'employee_email': employee_email,
            'kpis': kpis
        }
        kpip = PositionKPI(position_id=position_id, backup_type=backup_type, kpi_json_data=json_data)
        kpip.save()
    else:
        kpip = None

    return kpip


def get_position_kpi(position_kpi_id):      # get position kpi
    position_kpi = PositionKPI.objects.filter(id=position_kpi_id).first()
    return position_kpi


def get_postion_kpi(position_id):      # get position kpi backup_type= kpilib
    position_kpi = PositionKPI.objects.filter(position_id=position_id, backup_type='kpilib').order_by('id').last()
    return position_kpi


def get_position_kpi_typekpilib(position_id):       # get position kpi backup_type=kpilib if None => create new position kpi
    pos_kpi = None
    if position_id:
        pos_kpi = get_postion_kpi(position_id=position_id)
        if pos_kpi is None:
            pos_kpi = create_position_kpi(position_id=position_id, backup_type='kpilib')
    return pos_kpi


def add_kpi_to_position(position_kpi_id, kpi):       # add kpi vao kpis cua position kpi
    """
        kpi co dang:
            {
                'kpilib_unique_id': string
                'name':,
                'group_name':
                'target'
                'real'
                'result'
                'refer_relationship': [
                    {
                        'type': 'direct',
                        'position_id': Int,
                        'kpilib_unique_id': string
                    }
                ]
            }
    """
    position_kpi = get_position_kpi(position_kpi_id)
    if kpi and kpi['kpilib_unique_id'] == '':
        kpi['kpilib_unique_id'] = generate_kpilib_unique_id()
    if position_kpi:
        position_kpi.kpi_json_data['kpis'].append(kpi)
        position_kpi.save()

    return position_kpi


def delete_kpi_position(position_kpi_id, kpilib_unique_id):      # xoa kpi con trong position kpi
    position_kpi = get_position_kpi(position_kpi_id)
    if position_kpi:
        position_kpi.kpi_json_data['kpis'] = list(filter(lambda x: x['kpilib_unique_id'] != kpilib_unique_id, position_kpi.kpi_json_data['kpis']))

        position_kpi.save()

    return position_kpi


def update_kpi_position(position_kpi_id, kpis):
    """

    :param position_kpi_id:
    :param kpis: list kpis []
    :return:
    """
    position_kpi = get_position_kpi(position_kpi_id)
    if position_kpi and isinstance(kpis, list):
        position_kpi.kpi_json_data['kpis'] = kpis
        position_kpi.save()

    return position_kpi


def get_all_position_kpi(position_id, backup_type):      # get all position kpi of position
    kpis = []
    if backup_type == 'all':
        kpis = PositionKPI.objects.filter(position_id=position_id)
    if backup_type == 'kpilib':
        kpis = PositionKPI.objects.filter(position_id=position_id, backup_type='kpilib')
    if backup_type == 'performanceresult':
        kpis = PositionKPI.objects.filter(position_id=position_id, backup_type='performanceresult')
    return kpis


def generate_kpilib_unique_id():
    date = str(datetime.datetime.now())
    return hashlib.md5(date).hexdigest()
