from exceptions import Exception

from django.core.exceptions import PermissionDenied

from authorization.rules import has_perm, SETTINGS__VIEW_ORG_CHART
from memoize import memoize
from performance.base.views import PerformanceBaseView

from performance.models import QuarterPeriod


class QuarterServices(PerformanceBaseView):

    @classmethod
    @memoize(60)
    def get_quarter_start(cls, target_organization):
        quarter_start = 0

        current_quarter = target_organization.get_current_quarter()
        quarter_order = current_quarter.get_quarter_order()

        order_dict = {
            '1': quarter_order['quarter1_order'],
            '2': quarter_order['quarter2_order'],
            '3': quarter_order['quarter3_order'],
            '4': quarter_order['quarter4_order']
        }

        minValKey = min(order_dict, key=order_dict.get)
        quarter_start = minValKey
        return quarter_start


def get_quarter(actor, quarter_id):
    return QuarterPeriod.objects.filter(id=quarter_id).first()


def get_quarter_by_org(actor, quarter_id, organization):
    return QuarterPeriod.objects.filter(id=quarter_id, organization=organization).first()


def reactivate_quarter(actor, quarter, organization):
    if actor.profile.org_super_permission():

        '''
        Set 1: archive all quarters
        Set 2: set this quarter status into IN
        '''
        QuarterPeriod.objects.filter(organization=organization).exclude(status = "DELETED").update(status='AR')

        quarter.status = "IN"
        quarter.save()

        return quarter
    else:
        return None


def delete_quarter(actor, quarter, organization):
    if actor.profile.org_super_permission():

        quarter.status = "DELETED"
        quarter.save()

        return quarter
    else:
        return None


def restore_quarter(actor, quarter, organization):
    if actor.profile.org_super_permission():

        quarter.status = "AR"
        quarter.save()

        return quarter
    else:
        return None

@memoize(60)
def get_all_quarter_by_org(actor, organization):
    return QuarterPeriod.objects.filter(organization=organization, status__in=['IN', 'AR']).order_by('status',
                                                                                                     'due_date')

# Check permission with ceo of organization
@memoize(60)
def get_last_quarter(actor,organization):
    # if not has_perm(action=SETTINGS__VIEW_ORG_CHART,actor=actor,target=organization.ceo):
    #     raise PermissionDenied
    # TODO: Clear the permission
    #return QuarterPeriod.objects.filter(organization=organization).order_by('-id')[1]
    current_quarter = organization.get_current_quarter()
    try:
        last_quarter = QuarterPeriod.objects.filter(organization=organization,
                                            id=current_quarter.clone_from_quarter_period.id).first()
        if last_quarter.status == "DELETED": # return None when last quarter has been deleted
            last_quarter = None
    except:
        last_quarter = None

    return last_quarter
