from django.core.exceptions import PermissionDenied

from extrascore.services.exscore import get_extrascores, get_extrascores_performance
from performance.models import KPI
from user_profile.models import Profile, get_user_by_id, get_full_scores_from_log, get_full_scores_v2, \
    get_user_organization
from user_profile.services.profile import get_employee_code
from user_profile.services.user import get_user
from user_profile.services.user import get_user, get_system_user
from utils.common import to_json
from django.utils.dateparse import parse_datetime
from datetime import datetime, timedelta


def get_score_individual(quarter_period, p, organization, with_exscore=False):
    score = {}

    # manager = get_user_by_id(p.report_to_id)
    manager = get_user_by_id(p.parent.user_id) if p.parent else None
    user = get_user_by_id(p.user_id)
    # user_org = get_user_organization(p.user_id)
    score['user_id'] = p.user_id
    score['name'] = p.display_name
    score['id'] = p.user_id
    score['email'] = user.email
    score['title'] = p.title
    score['get_url'] = p.get_url()
    score['last_performance_review'] = p.last_performance_review.ctime() if p.last_performance_review else ''
    score['reason'] = p.reason
    score['department'] = p.department
    score['active'] = p.is_active()
    score['employee_code'] = get_employee_code(p)
    if manager:
        score['manager_email'] = manager.email

    # TODO: score['total_weighting_score'] = p.get_total_weighting_score()

    # quarter, m1, m2, m3 = get_full_scores(p, quarter_period)
    # quarter, m1, m2, m3 = get_full_scores_from_log(p, quarter_period)
    quarter, m1, m2, m3 = get_full_scores_v2(p, quarter_period)
    score['quarter_score'] = quarter
    score['m1_score'] = m1
    score['m2_score'] = m2
    score['m3_score'] = m3
    if organization:
        # organization == None => bug
        score['count_parent'], score['count_child'] = count_delay_and_not_evaluated_kpi_user(user, quarter_period)
    if with_exscore:
        system_user = get_system_user()
        exscores = to_json(get_extrascores_performance(system_user, p.user_id, quarter_period))
        score['m1'] = exscores['1']
        score['m2'] = exscores['2']
        score['m3'] = exscores['3']
        score['m1_total_performance'] = exscores['1']['total'] + score['m1_score']
        score['m2_total_performance'] = exscores['2']['total'] + score['m2_score']
        score['m3_total_performance'] = exscores['3']['total'] + score['m3_score']
    return score

    #
    # r.total_self_kpi = 0
    # r.total_assigned_kpi = 0
    #
    # kpis = p.get_kpis()
    # for k in kpis:
    #     if k.assigned_to_id > 0:
    #         r.total_assigned_kpi += 1
    #
    #     elif k.get_children().count() == 0:
    #         r.total_self_kpi += 1


def get_scores(actor, organization, quarter_period, user_ids, with_exscore=False):
    # map(get_score_individual, user_ids)
    scores = []

    if not isinstance(user_ids, list):
        user_ids = [user_ids]

    if organization and organization.id != quarter_period.organization_id:
        raise PermissionDenied

    profiles = Profile.objects.filter(user_id__in=user_ids)

    for profile in profiles:
        scores.append(get_score_individual(quarter_period, profile, organization, with_exscore=with_exscore))
    # for id in user_ids:
#     profile = Profile.objects.filter(user_id = user_ids).first()
#     scores.append(get_score_individual(quarter_period, profile, organization, with_exscore=with_exscore))
    return scores


def count_delay_and_not_evaluated_kpi_user(user, quarter_period=None):
    kpi_parent, kpi_child = user.profile.count_delay_and_not_evaluated_kpi(quarter_period)
    # from django.db.models import IntegerField, Sum, When, Case, Q, F
    # kpis = user.profile.get_kpis(quarter_period)
    # kpi_parent = kpis.filter(parent = None).aggregate(sum_delay_kpi_parent = Sum(Case(When(weight = 0, then=1), output_field = IntegerField())),\
    #                                                   month_1 = Sum(Case(When(month_1 = None, then=1), output_field = IntegerField())),\
    #                                                   month_2 = Sum(Case(When(month_2 = None, then=1), output_field = IntegerField())),\
    #                                                   month_3 = Sum(Case(When(month_3 = None, then=1), output_field = IntegerField())),)
    # kpi_child = kpis.all().exclude(parent = None).aggregate(sum_delay_kpi_child = Sum(Case(When(weight = 0, then=1), output_field = IntegerField())),\
    #                                                   month_1 = Sum(Case(When(month_1 = None, then=1), output_field = IntegerField())),\
    #                                                   month_2 = Sum(Case(When(month_2 = None, then=1), output_field = IntegerField())),\
    #                                                   month_3 = Sum(Case(When(month_3 = None, then=1), output_field = IntegerField())))
    # data = {
    #     'sum_delay_kpi_parent': 0,
    #     'sum_delay_kpi_child': 0,
    #     'sum_not_evaluated_kpi_parent': {
    #         'month_1': 0,
    #         'month_2': 0,
    #         'month_3': 0
    #     },
    #     'sum_not_evaluated_kpi_child': {
    #         'month_1': 0,
    #         'month_2': 0,
    #         'month_3': 0
    #     }
    # }
    # for kpi in kpis:
    #     if kpi.weight == 0:
    #
    #         if kpi.parent_id:
    #             data['sum_delay_kpi_child'] += 1
    #         else:
    #             data['sum_delay_kpi_parent'] += 1
    #     if kpi.month_1 is None or kpi.month_2 is None or kpi.month_3 is None:
    #         if kpi.parent_id:
    #             if kpi.month_1 is None:
    #                 data['sum_not_evaluated_kpi_child']['month_1'] += 1
    #             if kpi.month_2 is None:
    #                 data['sum_not_evaluated_kpi_child']['month_2'] += 1
    #             if kpi.month_3 is None:
    #                 data['sum_not_evaluated_kpi_child']['month_3'] += 1
    #         else:
    #             if kpi.month_1 is None:
    #                 data['sum_not_evaluated_kpi_parent']['month_1'] += 1
    #             if kpi.month_2 is None:
    #                 data['sum_not_evaluated_kpi_parent']['month_2'] += 1
    #             if kpi.month_3 is None:
    #                 data['sum_not_evaluated_kpi_parent']['month_3'] += 1

    # ## changed kpi.parent into kpi.parent_id
    # ## https://agile.cloudjet.solutions/project/fountainhead-cloudjet-kpi/us/1794?kanban-status=3
    return kpi_parent, kpi_child
