from performance.models import ReportQuery


def add_new_report_query(organization, user, name, create_at, query):
    report_query = ReportQuery.objects.filter(create_by=user, query=query).first()
    if report_query:
        return None
    rq = ReportQuery()
    rq.organization = organization
    rq.create_by = user
    rq.name = name
    if create_at:
        rq.created_at = create_at
    rq.query = query
    rq.save()

    return rq


def change_query(user, query, report_query_id):
    rq = report_query = ReportQuery.objects.get(id=report_query_id,query = query, create_by=user)
    rq.query = query
    rq.save()
    return rq


def get_report_query_by_id(user, report_query_id):
    try:
        if user and report_query_id:
            report_query = ReportQuery.objects.filter(id=report_query_id, create_by=user).first()
        if report_query:
            return report_query
        else:
            if report_query_id:
                report_query = ReportQuery.objects.filter(id=report_query_id).first()
            if report_query:
                return report_query

    except:
        return None


def get_report_query_by_user(user):
    try:
        data = ReportQuery.objects.filter(create_by=user)
        if data:
            return data
    except:
        return None


def delete_report_query_by_id(user, report_query_id):
    try:
        delete = get_report_query_by_id(user, report_query_id).delete()
        if delete:
            data = {'delete': True, 'id': report_query_id, 'user': user.id}
            return data
    except:
        return None


def delete_report_query_by_user(user):
    try:
        if get_report_query_by_user(user).delete():
            return []
    except:
        return None


def delete_report_query_all():
    try:
        if ReportQuery.objects.all().delete():
            return []
    except:
        return None
