from datetime import datetime

from django.utils.translation import gettext as _
from rest_framework.exceptions import PermissionDenied


# following code is excluded from coverage as it is used only in performance/controllers/settings.py
# and that file is omitted from coverage ->` .coveragerc`
def update_basic_settings(request, data):  # pragma: no cover
    user = request.user
    organization = user.profile.get_organization()
    if user.profile.is_admin and organization:
        if(data['self_review_date']):
            self_review_object = datetime.strptime(data['self_review_date'], '%d/%m/%Y')
            organization.self_review_date = self_review_object

        if(data['edit_to_date']):
            edit_to_date_object = datetime.strptime(data['edit_to_date'], '%d/%m/%Y')
            organization.edit_to_date = edit_to_date_object

        if data['max_score']:
            organization.max_score = data['max_score']

        if data['allow_delay_kpi']:
            organization.allow_delay_kpi = True if data['allow_delay_kpi'] == 'true' else False

        if data['allow_edit_monthly_target']:
            organization.allow_edit_monthly_target = True if data['allow_edit_monthly_target'] == 'true' else False

        if data['enable_weight_edit']:
            organization.enable_weight_editing = True if data['enable_weight_edit'] == 'true' else False

        if data['enable_email_notification']:
            organization.enable_email_notification = True if data['enable_email_notification'] == 'true' else False

        if data['default_auto_score_parent']:
            organization.default_auto_score_parent = True if data['default_auto_score_parent'] == 'true' else False

        if data['exscore']:
            organization.exscore = True if data['exscore'] == '1' else False

        if data['enable_abc_feature']:
            organization.enable_abc_feature = True if data['enable_abc_feature'] == '1' else False
        if data['name']:
            organization.name = data['name']
        if 'logo' in request.FILES:
            organization.logo = request.FILES['logo']
        if data['enable_require_target']:
            organization.enable_require_target = True if data['enable_require_target'] == '1' else False
        organization.save()
    else:
        raise PermissionDenied(_("Only super admin have permission to do this"))