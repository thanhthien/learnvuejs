# !/usr/bin/python
# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db.models import Max, F, FloatField

from performance.models import ScoreLog, Value
from user_profile.services.profile import Profile

def get_scorelog_by_quarter_and_subordinates(quarter_id, list_user_id):
    if quarter_id:
        #Lay danh sach profile tu Score log, theo quy va theo danh sach user id
        list_scorelog_profile = ScoreLog.objects.filter(id__in=ScoreLog.objects.filter(
            quarter_period_id=quarter_id, user_id__in=list_user_id) \
                                .values('user_id') \
                                .annotate(record_id=Max('id')) \
                                .values_list('record_id', flat=True)) \
            .annotate(display_name=F('user__profile__display_name')) \
            .annotate(report_to_name=F('user__profile__parent__display_name')) \
            .annotate(active=F('user__profile__active')) \
            .annotate(email=F('user__email')) \
            .annotate(department=F('user__profile__department')) \
            .annotate(unit_name=F('user__profile__organization__unitcode__name')) \
            .annotate(title=F('user__profile__title')) \
            .annotate(email_manager=F('user__profile__parent__user__email')) \
            .annotate(employee_code=F('user__profile__employee_code')) \
            .values('user_id', 'active', 'display_name', 'email', 'score', 'score_m1', 'score_m2', 'score_m3',
                    'report_to_name',
                    'department', 'unit_name', 'title', 'employee_code', 'email_manager')
        # tru ra nhung user da co danh gia, con lai nhung user chua danh gia
        list_user_id_no_scorelog = list(set(list_user_id) - set(list_scorelog_profile.values_list('user_id', flat=True)))

        # Lay danh sach cac profile chua co diem trong ScoreLog, mac dinh diem bang 0
        list_profile_empty_scorelog = Profile.objects.filter(user_id__in=list_user_id_no_scorelog) \
            .annotate(email_manager=F('parent__user__email')) \
            .annotate(score=Value('0', output_field=FloatField())) \
            .annotate(score_m1=Value('0', output_field=FloatField())) \
            .annotate(score_m2=Value('0', output_field=FloatField())) \
            .annotate(score_m3=Value('0', output_field=FloatField())) \
            .annotate(unit_name=F('organization__unitcode__name')) \
            .annotate(email=F('user__email')) \
            .annotate(report_to_name=F('parent__display_name')) \
            .values('user_id', 'active', 'display_name', 'email', 'score', 'score_m1', 'score_m2', 'score_m3',
                    'report_to_name',
                    'department', 'unit_name', 'title', 'employee_code', 'email_manager')
        return list(list_scorelog_profile) + list(list_profile_empty_scorelog)
    return None

def get_scorelog_by_organization(org):
    # hpthang: cho nay la code cu, can review va check lai vi /performance/report dang dung
    # theo nhu logic hien tai la ai cung co the thay duoc diem cua ca cong ty ????
    sls = ScoreLog.objects.filter(id__in=ScoreLog.objects.filter(
        user_id__in=User.objects.filter(profile__organization_id=org.id) \
            .values_list('id', flat=True).distinct()) \
                            .values('user_id') \
                            .annotate(record_id=Max('id')) \
                            .values_list('record_id', flat=True)) \
        .annotate(display_name=F('user__profile__display_name')) \
        .annotate(report_to=F('user__profile__parent__display_name')) \
        .annotate(active=F('user__profile__active')) \
        .annotate(email=F('user__email')) \
        .annotate(department=F('user__profile__department')) \
        .annotate(unit_name=F('user__profile__organization__unitcode__name')) \
        .annotate(title=F('user__profile__title')) \
        .annotate(email_manager=F('user__profile__parent__user__email')) \
        .annotate(employee_code=F('user__profile__employee_code')) \
        .values('user_id', 'active', 'display_name', 'email', 'score', 'report_to', 'department', 'unit_name', 'title',
                'employee_code', 'email_manager')
    return list(sls)

def get_scorelog_by_user(user, quarter_id):
    # lay diem quy cua user theo quarter_id hoac theo quy gan nhat neu khong co quarter_id
    if quarter_id:
        score_log = ScoreLog.objects.filter(user=user, quarter_period_id=quarter_id).order_by('id').last()
    else:
        score_log = ScoreLog.objects.filter(user=user).order_by('id').last()
    if score_log:
        return score_log.score
    return None
