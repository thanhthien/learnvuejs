# !/usr/bin/python
# -*- coding: utf-8 -*-
import traceback
from itertools import chain

from django.core.cache import cache
from django.db import IntegrityError, DatabaseError, DataError
from django.utils.translation import ugettext

from authorization.rules import KPI__EDITING
from django.contrib.auth.models import User
from django.http import Http404
from performance.db_models.strategy_map import StrategyMap, GroupKPI

from performance.models import  QuarterPeriod, KPI
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied, MultipleObjectsReturned

from company.models import Organization
from django.shortcuts import get_object_or_404
from performance.services import KPIServices, remove_kpi, has_perm, update_kpi
from utils import postpone
from utils.common import to_json
from django.utils.translation import ugettext


def get_kpigroup(group_id):
    return GroupKPI.objects.filter(id=group_id).first()


def get_kpis_by_group(group, actor, include_children=True):
    if group.map.organization != actor.profile.get_organization():
        raise PermissionDenied
    # kpi_list = KPI.objects.filter(groupkpi__id=group.id).order_by('ordering')
    kpi_list = KPI.objects.filter(group_kpi_id=group.id).order_by('ordering')
    result = kpi_list
    if include_children:
        result = [kpi.to_json_nochildren(actor=actor) for kpi in kpi_list]
    return result


def get_kpis_by_group_v2(group, actor, include_children=True):
    if group.map.organization != actor.profile.get_organization():
        raise PermissionDenied

    return group.to_json(include_kpi=True, actor=actor)['kpis']


def get_group_by_map_and_category(s_map, category, include_kpi=False, unsort=False):
    # ## Khong con duoc dung o duoi nua
    # def lowest_kpi_order_by_group(group):
    #     '''
    #     This is very slow, don't use it.
    #     '''
    #
    #     k = KPI.objects.filter(
    #         groupkpi__id=group.id).order_by('ordering').first()
    #     if k:
    #         return k.ordering
    #     else:
    #         return 0
    '''

    :param s_map: strategy_map object
    :param category:  string category
    :param include_kpi: boolean include kpi
    :param unsort: boolean unsort
    :return:
    '''

    '''
    HACK: filter only maps which have parent kpis and don't have any kpis
    '''
    map_director = s_map.get_director()

    '''
    Case 1: Group that have only parent kpis
    '''
    ids_group_contain_parent_kpis = map_director.profile.get_kpis_parent(quarter_period=s_map.quarter_period).values_list('group_kpi_id', flat=True)
    group_have_only_kpi_parents = GroupKPI.objects.filter(map=s_map, category=category, id__in=ids_group_contain_parent_kpis)

    '''
    Case 2: Group that don't have any KPIs
    '''
    ids_group_contain_kpis = map_director.profile.get_kpis(quarter_period=s_map.quarter_period).exclude(group_kpi_id__isnull=True).values_list('group_kpi_id', flat=True)
    # group_dont_have_any_kpis = GroupKPI.objects.filter(map=s_map, category=category,kpis__isnull=True)# legacy styles, not use anymore
    group_dont_have_any_kpis = GroupKPI.objects.filter(map=s_map, category=category).exclude(id__in=ids_group_contain_kpis)

    '''
    Group query must return case 1 and case 2
    '''
    unsorted_groupkpis = list(chain(group_have_only_kpi_parents, group_dont_have_any_kpis))

    '''
    END HACK
    '''
    if include_kpi:
        pass
    # .prefetch_related('kpis')#.values(, 'map', 'category')

    # for g in unsorted_groupkpis:
    #     g.kpi
    else:
        # pass
        # for g in unsorted_groupkpis:
        #     ks=g.kpis.only('id')
        #     print ks
        # # Step 1: Triggered query here
        unsorted_groupkpis = [group.to_json(include_kpi=False) for group in unsorted_groupkpis]

        # group_ids = GroupKPI.objects.filter(map=map, category=category).values_list('id', flat=True)
        # unsorted_groupkpis = GroupKPI.objects.filter(map=map, category=category).annotate(kpi_child=KPI.objects.filter(groupkpi__id__in=group_ids))
        # print (GroupKPI.objects.filter(map=map, category=category).values_list('id', flat=True))

        # print group_ids
        # unsorted_groupkpis = GroupKPI.objects.filter(map=map, category=category).annotate(kpis=KPI.objects.filter(groupkpi__id__=group_ids))

        # group_kpis_list = sorted(unsorted_groupkpis, key=lambda x: lowest_kpi_order_by_group(x))
    group_kpis_list = unsorted_groupkpis

    # if actor and child:
    #     for group in group_kpis_list:
    #         kpis = (get_kpis_by_group(group, actor))
    #         print kpis
    # group_kpis_list[group]['kpis'] = kpis
    return group_kpis_list
    # return group_ids


def get_kpis_group(strategy_map, include_kpi=False, bsc_category=None):
    if not strategy_map:
        return [], [], [], []

    if strategy_map.quarter_period is None:
        strategy_map.save()

    if not bsc_category:

        group_financial = get_group_by_map_and_category(strategy_map, 'financial', include_kpi=include_kpi)
        group_customer = get_group_by_map_and_category(strategy_map, 'customer', include_kpi=include_kpi)
        group_internal = get_group_by_map_and_category(strategy_map, 'internal', include_kpi=include_kpi)
        group_learning = get_group_by_map_and_category(strategy_map, 'learninggrowth', include_kpi=include_kpi)

        return group_financial, group_customer, group_internal, group_learning

    elif bsc_category == 'financial':

        group_financial = get_group_by_map_and_category(strategy_map, 'financial', include_kpi=include_kpi)
        return group_financial, [], [] , []

    elif bsc_category == 'customer':

        group_customer = get_group_by_map_and_category(strategy_map, 'customer', include_kpi=include_kpi)
        return [], group_customer, [] , []

    elif bsc_category == 'internal':

        group_internal = get_group_by_map_and_category(strategy_map, 'internal', include_kpi=include_kpi)
        return [], [], group_internal , []

    elif bsc_category == 'learninggrowth':

        group_learning = get_group_by_map_and_category(strategy_map, 'learninggrowth', include_kpi=include_kpi)
        return [], [], [], group_learning


def get_kpis_bsc_category(s_map, include_kpi=False, bsc_category=None):
    _group_financial, _group_customer, _group_internal, _group_learning = get_kpis_group(s_map, include_kpi=include_kpi, bsc_category=bsc_category)

    group_financial = {
        "group_kpis": to_json(_group_financial),
        "ungroup_kpis":[]
    }
    group_customer = {
        "group_kpis": to_json(_group_customer),
        "ungroup_kpis": []
    }
    group_internal = {
        "group_kpis": to_json(_group_internal),
        "ungroup_kpis":[]
    }
    group_learning = {
        "group_kpis": to_json(_group_learning),
        "ungroup_kpis": []
    }

    # ## Get ungroup strategy KPIs
    # ## Format must be the same as GROUP KPI
    if not bsc_category or bsc_category == 'financial':
        group_financial['ungroup_kpis'] = [{
            "category": "financial",
            "map": s_map.id,
            "name": "",
            "group": False,
            "kpis": to_json([kpi.to_json_nochildren() for kpi in s_map.get_upgroup_kpis("financial", s_map.quarter_period)])
        }]

    if not bsc_category or bsc_category == 'customer':

        group_customer['ungroup_kpis'] = [{
            "category": "customer",
            "map": s_map.id,
            "name": "",
            "group": False,
            "kpis": to_json([kpi.to_json_nochildren() for kpi in s_map.get_upgroup_kpis("customer", s_map.quarter_period)])
        }]

    if not bsc_category or bsc_category == 'internal':

        group_internal['ungroup_kpis'] = [{
            "category": "internal",
            "map": s_map.id,
            "name": "",
            "group": False,
            "kpis": to_json([kpi.to_json_nochildren() for kpi in s_map.get_upgroup_kpis("internal", s_map.quarter_period)])
        }]

    if not bsc_category or bsc_category == 'learninggrowth':

        group_learning['ungroup_kpis'] = [{
            "category": "learninggrowth",
            "map": s_map.id,
            "name": "",
            "group": False,
            "kpis": to_json([kpi.to_json_nochildren() for kpi in s_map.get_upgroup_kpis("learninggrowth", s_map.quarter_period)])
        }]
    return group_financial, group_customer, group_internal, group_learning


def get_user_current_map(user):
    org = user.profile.get_organization()
    if not org:
        return None

    quarter = org.get_current_quarter()

    if not quarter:  # pragma: no cover
        raise DataError("Quarter not existed")
        # return None

    return get_map(org, quarter, user_id=user.id)[0]


def clean_up_multiple_strategymap(maps):
    # clean up
    total_map_has_group = 0

    for sm in maps:
        groups = GroupKPI.objects.filter(map=sm)
        if groups.count() > 0:
            total_map_has_group += 1

    if total_map_has_group > 0:  # --> delete other strategy maps which do not have group
        for sm in maps:
            groups = GroupKPI.objects.filter(map=sm)
            if groups.count() == 0:  # no group --> delete
                sm.delete()
    else:  # delete other strategy maps
        for sm in maps[1:]:
            sm.delete()


def get_map(organization, current_quarter, hash=None, user_map_hash=None, user_id=None):
    use_hashurl_type = ''
    if hash:  # get smap by hash
        s_map = StrategyMap.objects.filter(organization=organization, hash=hash).first()
        if not s_map:
            raise Http404
    elif user_map_hash:
        s_map = StrategyMap.objects.filter(organization=organization, hash=user_map_hash).first()
        if not s_map:
            raise Http404
    else:  # get smap of current quarter
        if user_id:
            director = get_object_or_404(User, pk=user_id)

            try:
                old_map = StrategyMap.objects.filter(organization=organization,
                                                                  quarter_period=current_quarter,
                                                                  director__isnull=True).first()

                if old_map:
                    # errors = StrategyMap.objects.filter(organization=organization,
                    #                                   quarter_period=current_quarter,
                    #                                   director=director)
                    # # for er in errors:
                    #     er.delete()

                    old_map.director = director
                    old_map.save()

                if not old_map:

                    s_map, is_created = StrategyMap.objects.get_or_create(organization=organization,
                                                                  quarter_period=current_quarter,
                                                                  director=director)
                else:
                    s_map = old_map
                    is_created = False

            except MultipleObjectsReturned:

                is_created = False

                s_map_list = list(StrategyMap.objects.filter(organization=organization,
                                                             quarter_period=current_quarter,
                                                             director=director))

                clean_up_multiple_strategymap(s_map_list)

                s_map_list = list(StrategyMap.objects.filter(organization=organization,
                                                             quarter_period=current_quarter,
                                                             director=director))

                s_map = s_map_list[0]

            use_hashurl_type = 'user'

        else:
            try:
                s_map, is_created = StrategyMap.objects.get_or_create(organization=organization,
                                                                  quarter_period=current_quarter,
                                                                  director=organization.ceo)
                                                                  # director__isnull=True)

            except IntegrityError:
                s_map = StrategyMap.objects.filter(organization=organization,
                                                                      quarter_period=current_quarter,
                                                                      director=organization.ceo).first()
                is_created = False
            except MultipleObjectsReturned:

                is_created = False

                s_map_list = list(StrategyMap.objects.filter(organization=organization,
                                                            quarter_period=current_quarter,
                                                            director=organization.ceo))  # director__isnull=True))

                clean_up_multiple_strategymap(s_map_list)

                s_map_list = list(StrategyMap.objects.filter(organization=organization,
                                                             quarter_period=current_quarter,
                                                             director=organization.ceo))
                                                             # director__isnull=True))

                s_map = s_map_list[0]

            use_hashurl_type = 'user'  # 'organization' type is no longer valid

    return s_map, use_hashurl_type

    # HttpResponseRedirect(reverse('sub_strategy_map_by_hash', kwargs={'hash': s_map.get_hash()}))


class StrategyMapServices():

    # # Khang noted:
    # ## __init__ does:
    #### Check permission
    #### Check data consistency
    #### Make sure methods don't need to check permission anymore

    def __init__(self, map, action=None, actor=None):

        if map:
            target = map.director
            if not target:
                target = map.organization.ceo
            if not has_perm(action=action, actor=actor, target=target):
                raise PermissionDenied

    def set_group(self, parent_kpi, group, root_kpi_id=None):
        if parent_kpi.refer_group_name != group.name:
            parent_kpi.refer_group_name = group.name

            '''
            Update  group_kpi information
            '''
            parent_kpi.get_group().copy_group_kpi(group)
            try:
                parent_kpi.save(update_fields=['refer_group_name'])
            except DatabaseError:
                pass

            rdis_key = 'get_group_name{0}_{1}'.format(parent_kpi.id, root_kpi_id)
            cache.delete(rdis_key)

# Note: Hong tempo remove this    @postpone
    def set_group_threading(self, parent_kpi, group, root_kpi_id=None):  # pragma: no cover

        self.set_group(parent_kpi, group, root_kpi_id)
        # Only set refer_group_name for kpi refer
        for kpi in parent_kpi.get_children_refer():
            self.set_group(kpi, group, root_kpi_id)

    @classmethod
    def create(cls, organization_id, quarter_period_id):
        organization = Organization.objects.get(id=organization_id)
        quarter_period = QuarterPeriod.objects.get(id=quarter_period_id)
        if quarter_period.organization != organization:
            raise Exception("Organization not matched!")

        create = False
        s_map = None
        # Change to if condition check according to below query never cause an ObjectDoesNotExist exception,
        # try except block removed
        s_map = cls.get_map(organization.id, quarter_period.id)

        # Added code if None is returned:
        if not s_map:
            create = True
            s_map = StrategyMap.create(organization.id, quarter_period.id, organization.name + " - VISION")

        if s_map.description is None or (s_map.description == '' and create):
            previous_quarter = quarter_period.get_previous_quarter()
            if previous_quarter:  # pragma: no cover
                previous_strategy_map = previous_quarter.get_strategy_map()
                if previous_strategy_map:  # pragma
                    s_map.name = previous_quarter.name
                    s_map.description = previous_quarter.description
                    s_map.save()

        return s_map

    @classmethod
    def delete_kpi(cls, organization_id, kpi_id, actor):

        # TODO: lỗi authorization: cần phải truyền biến user (actor) vào để tránh việc user công ty nào xóa KPI của công ty khác

        # kpi=KPI.objects.get(id=kpi_id)
        if remove_kpi(kpi_id, actor):
            return True
        else:
            raise Exception('Cannot delete KPI')

            # return HttpResponse('ok')

    @classmethod
    def create_kpi(cls, organization_id, smap_id, category, actor):

        s_map = cls.get_map_by_id(smap_id, organization_id)
        if s_map.director:
            target_user = s_map.director
        else:
            target_user = s_map.organization.ceo
        kpi = KPIServices().create_kpi(creator=actor, name="...", bsc_category=category,
                                       quarter_period=s_map.quarter_period, user=target_user)
        return kpi

    @classmethod
    def delete_group_kpi(cls, organization_id, group_id, actor):

        has_kpi = False
        group = GroupKPI.objects.filter(pk=group_id).first()

        if group and group.get_kpis().count():

            raise PermissionDenied("Không thể xóa Mục tiêu trước khi xóa KPI")
            return False

            # kpis = group.get_kpis()
            #
            # has_kpi = True
            # kpis.update(refer_group_name=None, group_kpi=None)
            # for kpi in kpis:  # pragma: no cover
            #     cache.delete('get_group{}'.format(kpi.id))
            #     cache.delete("get_group_name{0}_{1}".format(kpi.id, None))

        check_group_perm(group, actor)

        if group:
            try:
                group.delete()
            except IntegrityError as e:
                kpi = KPI.all_objects.filter(group_kpi_id=group.id).update(group_kpi=None)
                group.delete()

        return has_kpi

    @classmethod
    def update_group_kpi(cls, organization_id, kpi_id, new_group_id=None, old_group_id=None, actor=None, new_bsc_category=None):
        kpi = get_object_or_404(KPI, pk=kpi_id)
        if new_bsc_category and new_bsc_category != kpi.bsc_category:
            kpi = update_kpi(actor, kpi.id, **{'bsc_category':new_bsc_category, 'ordering': None})
            kpi.ordering = kpi.get_next_ordering()
            kpi.save()

        if old_group_id:
            group = get_object_or_404(GroupKPI, pk=old_group_id)  # , map__organization_id=organization_id)

            # check_group_perm(group, actor)
            #
            # kpi.group_kpi_id = None
            # kpi.refer_group_name = ''
            # kpi.save()

           # kpi.groupkpi_set.remove(group)  # https://docs.djangoproject.com/en/1.9/topics/db/queries/#related-objects

        #     No need to update refer group name here

        if new_group_id:
            group = get_object_or_404(GroupKPI, pk=new_group_id)  # , map__organization_id=organization_id)

            check_group_perm(group, actor)

           # kpi.groupkpi_set.add(group)
            kpi.group_kpi = group
            kpi.save()

            StrategyMapServices(action=KPI__EDITING, actor=actor, map=group.map).set_group_threading(parent_kpi=kpi, group=group)

        cache.delete('get_group{}'.format(kpi.id))
        cache.delete("get_group_name{0}_{1}".format(kpi.id, None))
        return kpi

    @classmethod
    def create_group_kpi(cls, organization_id, smap_id, category, name='New Group...', actor=None):
        if not name:
            name = ugettext('New Group...')
        s_map = cls.get_map_by_id(smap_id, organization_id)
        group = GroupKPI.objects.create(map=s_map,
                                        name=name,
                                        category=category)
        return group

    @classmethod
    def update_group(cls, organization_id, id, smap_id, category, name, actor=None):
        s_map = cls.get_map_by_id(smap_id, organization_id)
        try:
            group = GroupKPI.objects.get(pk=id)
        except ObjectDoesNotExist:  # pragma: no cover
            return "Not Found"

        check_group_perm(group, actor)

        if s_map:
            group.map = s_map
        if name:
            group.name = name
        if category:
            group.category = category

        # print name;
        group.save()

        group.get_kpis().update(refer_group_name=group.name)

        for kpi in group.get_kpis():
            StrategyMapServices(action=KPI__EDITING, actor=actor, map=group.map).set_group_threading(parent_kpi=kpi,
                                                                                                     group=group)

            cache.delete('get_group{}'.format(kpi.id))
            cache.delete("get_group_name{0}_{1}".format(kpi.id, None))

        return group

    @classmethod
    def get_map(cls, organization_id, quarter_period_id):
        s_map = StrategyMap.get(organization_id, quarter_period_id)
        return s_map

    @classmethod
    def get_map_by_id(cls, smap_id, organization_id):
        s_map = StrategyMap.get_by_id(smap_id, organization_id)
        return s_map


def check_group_perm(group, actor):
    if group and group.map_id and group.map.director_id:
        if not has_perm(KPI__EDITING, actor, group.map.director):
            raise PermissionDenied(ugettext("You do not have permission for this action"))
    else:
        if actor and not actor.profile.is_superuser_org():
            raise PermissionDenied(ugettext("You do not have permission for this action"))


def create_group_kpi(organization_id, smap_id, category, name='New Group...'):
    if name:
        name = name.strip()

    return StrategyMapServices.create_group_kpi(organization_id, smap_id, category, name=name)


def get_similar_group(organization, user, group_name):
    if not organization or not user:
        return None

    current_quarter = organization.get_current_quarter()
    kpi = KPI.objects.select_related('group_kpi') \
        .filter(quarter_period=current_quarter, group_kpi__name__iexact=group_name).first()

    if kpi:
        return kpi.group_kpi

    return None
