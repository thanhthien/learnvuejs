import time
from datetime import timedelta, datetime

#from performance.data.fix_kpi import fix_children_no_refer_no_assign
from performance.services import add_new_cascadeKPIQueue
from performance.services.CascadeKPIQueue import cleaning_db
import pytest

__author__ = 'hongleviet'
#
# @pytest.mark.django_db(transaction=False)
# def test_add_new_cascadeKPIQueue(kpi):
#
#
#     assert add_new_cascadeKPIQueue(kpi) == True
#     time.sleep(6)
#     assert add_new_cascadeKPIQueue(kpi) == False #delay 5 seconds
#
#     time.sleep(6)
#     assert add_new_cascadeKPIQueue(kpi) == True #sleep second time


@pytest.mark.django_db(transaction=False)
def test_cleaning_db(kpi):


    time.sleep(6)

    add_new_cascadeKPIQueue(kpi)

    from performance.models import CascadeKPIQueue as ckq

    c = ckq.objects.filter(kpi=kpi).first()


    assert c is not None

    days8 = datetime.now() + timedelta(days = -8)
    days7 = datetime.now() + timedelta(days = -7)

    c.created_at = days8 # datetime.now() + timedelta(days = -8)
    c.is_run = True

    c.save()

    assert ckq.objects.filter(is_run = True, created_at__lt = days7 ).count() > 0

    cleaning_db()
    assert ckq.objects.filter(is_run = True, created_at__lt = days7 ).count() == 0


