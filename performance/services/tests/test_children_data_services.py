import logging

import pytest
from jsonschema import Draft4Validator, validate, Draft3Validator, ValidationError

from performance.models import KPI
from performance.services import remove_kpi
from performance.services.children_data_services import get_children_data, children_data_schema, set_children_data

logger = logging.getLogger('django')

def test_children_data_schema():
    Draft3Validator.check_schema(children_data_schema)

    try:
        validate({"parent_score_auto": "dd"}, children_data_schema)

        assert 1 == 0 #this should never reach here
    except Exception as e:

        assert type(e) == ValidationError
        assert 2 == 2 #this should reach here

@pytest.mark.django_db(transaction=False)
def test_get_children_data(userA, userB,userAdmin, organization):

    '''
    Prepare data
    '''
    quarter = organization.get_current_quarter()

    kpi = KPI(name = 'parent')
    kpi.user = userA
    kpi.quarter_period = quarter
    kpi.save()


    k1 = kpi.assign_to(userB)



    k_child1 = kpi.get_children()[0]
    assert k1.id != k_child1.id

    assert k1.cascaded_from == k_child1
    assert k1.refer_to == kpi

    k2 = KPI(name = "k2")
    k2.user = userA
    k2.parent = kpi
    k2.refer_to = kpi
    k2.weight = 15.678
    k2.quarter_period = quarter
    k2.save()

    refer_kpis = kpi.get_children_refer()
    assert len(refer_kpis) == 2

    '''
    #End Prepare data 
    '''

    # 1. check construct data from beginning
    children_data = get_children_data(userA, kpi)

    #logger.info("Yes, I'm fucking been here")

    validate( children_data,children_data_schema)
    for weight in children_data['children_weights']:
        assert weight['weight'] == 10 or weight['weight'] == 15.678

    # 2. check modify data

    # 2.1 add new child kpi
    k3 = KPI(name = "k3")
    k3.user = userA
    k3.parent = kpi
    k3.refer_to = kpi
    k3.weight = 11.23
    k3.quarter_period = quarter
    k3.save()

    children_data = get_children_data(userA, kpi)
    validate(children_data, children_data_schema)
    assert len(children_data['children_weights']) == 3
    for weight in children_data['children_weights']:
        assert weight['weight'] == 10 or weight['weight'] == 15.678 or weight['weight'] == 11.23

    # 2.2 delay 1 kpi
    k1.delay_toggle( user=userAdmin)
    for weight in KPI.objects.get(id = kpi.id).children_data['children_weights']:
        assert  weight['weight'] == 0 or weight['weight'] == 15.678 or weight['weight'] == 11.23


    # 2.3 delay another kpi
    k2.delay_toggle( user=userAdmin)
    for weight in KPI.objects.get(id = kpi.id).children_data['children_weights']:
        assert  weight['weight'] == 0 or weight['weight'] == 11.23

    # reactivate k2
    k2.delay_toggle( user=userAdmin)
    for weight in KPI.objects.get(id=kpi.id).children_data['children_weights']:
        assert weight['weight'] == 0 or weight['weight'] == 15.678 or weight['weight'] == 11.23

    # 2.4 delete 1 child kpi

    remove_kpi(k1.id, userAdmin)
    assert kpi.get_children_refer().count() == 2

    children_data = get_children_data(userA, kpi)
    validate(children_data, children_data_schema)
    assert len(children_data['children_weights']) == 2
    for weight in children_data['children_weights']:
        assert weight['weight'] == 15.678 or weight['weight'] == 11.23



    # 2.5 continute to remove more
    remove_kpi(k2.id, userAdmin)
    children_data = get_children_data(userA, kpi)
    validate(children_data, children_data_schema)
    assert len(children_data['children_weights']) == 1
    for weight in children_data['children_weights']:
        weight['weight'] == 11.23

    # check weight of number is None
    k4 = KPI(name = "k4")
    k4.user = userA
    k4.weight = None
    k4.quarter_period = quarter
    k4.save()

    _kpi = set_children_data(userA, kpi, {'children_weights': [{'kpi_id': k4.id, 'relation_type': [], 'weight': None}], 'parent_score_auto': True}, children_data_schema)
    assert _kpi.children_data['children_weights'][0]['weight'] == 10

