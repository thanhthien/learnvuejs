import pytest
from django.contrib.auth.models import User

from performance.services.common import PerformanceApi

#
# @pytest.mark.django_db(transaction=False)
# def test_get_user_organization(organization, userA):
#     user = PerformanceApi.get_user_organization(userA)
#     assert user.organization == organization
#
#     userA.profile.organization = None
#     user = PerformanceApi.get_user_organization(userA)
#     assert user.organization == organization
#
#     user = PerformanceApi.get_user_organization(None)
#     assert user == None
#
#     user = PerformanceApi.get_user_organization('TEST')
#     assert user == None
#
#     user = PerformanceApi.get_user_organization(None)
#     assert user == None
#
#     user = PerformanceApi.get_organization(None)
#     assert user == None
#
#     organization.enable_competency = True
#     user = PerformanceApi.get_performace_user_data(userA, organization.get_current_quarter(), organization)
#     assert user['email'] == userA.email
#
#     user_B = User()
#     user_B.save()
#     user = PerformanceApi.get_performace_user_data(user_B, organization.get_current_quarter(), organization)
#     assert user == {}
#
#     organization.enable_competency = False
#     user = PerformanceApi.get_performace_user_data(userA, organization.get_current_quarter(), organization)
#     assert user['email'] == userA.email
#
#
# remove this test due to legacy code
