# !/usr/bin/python
# -*- coding: utf-8 -*-
import json
import random

import math
import pytest
from django.core.files.uploadedfile import SimpleUploadedFile
from rest_framework.reverse import reverse

from performance.models import KPI
# from faker import Factory
from performance.services import *
from utils.random_str import random_string
import datetime

from performance.services.EmailTemplate import *


@pytest.mark.django_db(transaction=False)
def test_email_template(organization, ceo,client_ceo):
    ### test get all template
    url_template_email = reverse('auto_mail_view') + '?get_all=true'
    response = client_ceo.get(url_template_email)
    assert response.status_code == 200

    ### test get create by admin
    url_template_email = reverse('auto_mail_view') + '?create_template_by_admin=true'
    response = client_ceo.get(url_template_email)
    assert response.status_code == 200

    ### test create template
    url_template_email = reverse('auto_mail_view')
    data = {
        'title': 'Template 1',
        'content': 'Hello world'
    }

    response = client_ceo.put(url_template_email, json.dumps(data), content_type="application/json")
    assert response.status_code == 200
    data = json.loads(response.content)
    assert data['status'] == 'OK'
    assert data['data']['content'] == 'Hello world'
    assert data['data']['title'] == 'Template 1'
    assert add_new_email_template(organization, data['data']['title'], data['data']['content']) == None
    ### test edit template
    id = data['data']['id']
    assert get_template_by_id(id).title == data['data']['title']

    url_template_email = reverse('auto_mail_view') + '?id=' + str(id)
    data = {
        'title': 'Template 1 edited',
        'content': 'Hello world edited'
    }
    response = client_ceo.post(url_template_email, json.dumps(data), content_type="application/json")
    assert response.status_code == 200
    data = json.loads(response.content)
    assert data['status'] == 'OK'
    assert data['data']['title'] == 'Template 1 edited'
    assert data['data']['content'] == 'Hello world edited'

    ### test delete tempalte
    url_template_email = reverse('auto_mail_view') + '?id=' + str(id)
    response = client_ceo.delete(url_template_email, json.dumps({}), content_type="application/json")
    assert response.status_code == 200