# !/usr/bin/python
# -*- coding: utf-8 -*-
import logging

import pytest
from jsonschema import Draft4Validator, validate, Draft3Validator, ValidationError

from performance.models import KPI
from user_profile.models import Profile
from performance.services import remove_kpi
from performance.services.children_data_services import get_children_data, children_data_schema
from performance.management.commands.evn_thoi_gian_sua_dien import is_quarter_period_passed, update_kpi_by_unique_code
from utils.common import last_date_of_month, first_date_of_month
import datetime
logger = logging.getLogger('django')

@pytest.mark.django_db(transaction=False)
def test_is_quarter_period_passed( organization):
    current_quarter = organization.get_current_quarter()
    """
    Following logic added to check of current quarter passed if 1st quarter started from January.
    """
    current_month = datetime.date.today().month
    quarter = (current_month/3 + 1)
    current_quarter.quarter = quarter
    current_quarter.save()
    is_passed, message = is_quarter_period_passed(current_quarter)

    assert is_passed is False

    # set current_quarter is passed
    today=datetime.date.today()
    q = current_quarter.year
    current_quarter.year = today.year-1
    current_quarter.save()
    p = current_quarter.year
    is_passed, message = is_quarter_period_passed(current_quarter)
    assert is_passed is True


@pytest.mark.django_db(transaction=False)
def test_update_kpi_by_unique_code(userA, userB, kpi, organization):
    # employee_service_data, quarter_period, month
    # employee_service_data

    current_quarter = organization.get_current_quarter()
    month=1
    unit_code = 'PE0100'
    employee_code="A6144"
    unique_code='THOIGIANSUADIENONLINE_{}'.format(employee_code,).lower() # always lower unique_code
    employee_service_data={"maNhanVien":employee_code,"username":"BnhNT",
                           "tenNhanVien": u'Nguyễn Thanh Bình',"donVi":unit_code,
                           "tongThoiGianSuaDien":9825,"soTruongHop":377,"thoiGianTrungBinh":26}



    # setup data for user and kpi
    pA=userA.profile
    pA.unit_code=unit_code
    pA.employee_code=employee_code
    pA.save(update_fields=['unit_code', 'employee_code'])
    # refresh pA
    pA=Profile.objects.get(id=pA.id)
    assert pA.employee_code==employee_code
    assert pA.unit_code==unit_code


    kpi.unique_code = unique_code
    kpi.save(update_fields=['unique_code',])
    # refresh kpi
    kpi = KPI.objects.get(id=kpi.id)
    assert kpi.unique_code==unique_code
    # make sure kpi has quarter is current quarter
    assert kpi.quarter_period == current_quarter


    # do update kpi by unique_code
    update_kpi_by_unique_code(employee_service_data, current_quarter, month)
    # refresh kpi
    kpi=KPI.objects.get(id=kpi.id)

    # assert that the update is successful
    # month_2=kpi.month_2
    # month_3=kpi.month_3
    assert kpi.month_1==employee_service_data['thoiGianTrungBinh']
    pass