# !/usr/bin/python
# -*- coding: utf-8 -*-
import random
import json
import datetime
from django.core.files.uploadedfile import SimpleUploadedFile
import pytest
from django.core.urlresolvers import reverse

from performance.models import KpiComment
from performance.services import *
from performance.services.children_data_services import get_children_data, set_children_data
from performance.services.kpi_archivable_score import update_archivable_score
from utils import reload_model_obj
from utils.random_str import random_string
from django.utils.translation import ugettext as _
from random import randint


@pytest.mark.django_db(transaction=False)
def test_convert_kpis_to_json(userA_manager, organization, userA, kpi_userA_manager):
    current_quarter = organization.get_current_quarter()

    # ## CASE 1: Assign to that user, not will not have any child weight
    # userA_manager assign kpi_userA_manager into userA
    k = create_child_kpi(kpi_userA_manager, current_quarter, actor=userA_manager)
    k.assign_to(userA)

    # userA_manager assign kpis into userA from one of userA's kpis
    k2 = create_child_kpi(kpi_userA_manager, current_quarter, actor=userA_manager)
    k2.assign_to(userA)

    kpis = userA_manager.profile.get_kpis_parent()
    json_data = convert_kpis_tojson(kpis)
    kpi_userA_manager_json = kpi_userA_manager.to_json(refer=True)

    children_refer = kpi_userA_manager.get_children_refer().values_list('id', flat=True)
    children = kpi_userA_manager.get_children().values_list('id', flat=True)

    assert len(json_data) == 1
    assert kpi_userA_manager_json['id'] in [k['id'] for k in json_data]

    json_data = json_data[0]['children']

    assert len(json_data) == len(children_refer)
    assert len(json_data) == len(children)
    assert len(json_data) > 0

    for json_child in json_data:
        kpi = KPI.objects.get(id=json_child['id'])
        assert kpi.id in children_refer
        assert kpi.id not in children


@pytest.mark.django_db(transaction=False)
def test_recheck_score(userA, parent_kpi, parent_kpi2, kpi):
    recheck_score(userA)


@pytest.mark.django_db(transaction=False)
def test_remove_assigned_kpi_from_above(parent_kpi, userA, userA_manager, organization, kpi_userA_manager):
    current_quarter = organization.get_current_quarter()

    # userA_manager assign kpis into userA
    k = create_child_kpi(kpi_userA_manager, current_quarter, actor=userA_manager)
    k.assign_to(userA)

    # userA_manager assign kpis into userA from one of userA's kpis
    k2 = create_child_kpi(parent_kpi, current_quarter, actor=userA_manager)

    # userA try to delete kpis that assigned from userA_manager, assert False
    assert remove_kpi(k.id, actor=userA) == False

    # userA try to delete kpis that assigned from userA, assert True
    assert remove_kpi(k2.id, actor=userA) == True


@pytest.mark.django_db(transaction=False)
def test_remove_kpi(kpi, parent_kpi, userA, userAdmin, userB, organization):
    kpi_cha = KPI()
    kpi_cha.save()

    parent_kpi.parent = kpi_cha
    parent_kpi.final_score = 10
    parent_kpi.save()

    count = len(KPI.objects.filter(user=userA))

    kpi.user = userA
    kpi.final_score = 10
    kpi.parent = parent_kpi
    kpi.cascaded_from = parent_kpi
    kpi.save()

    """
    Remove KPI
    """
    remove_kpi(kpi.id, userAdmin)
    count1 = len(KPI.objects.filter(user=userA))

    """
    Adding assertion
    """
    assert count1 < count

    # need to check this case :
    # """
    # Case: no actor
    # """
    #
    # remove_kpi(parent_kpi.id, actor=None)
    # count2 = len(KPI.objects.filter(user=userA))
    # assert count2 < count1


@pytest.mark.django_db(transaction=False)
def test_remove_kpi_from_refer_to(userA, userB, organization):
    kpi = KPI()
    kpi.user = userA
    kpi.save()

    assert kpi.kpi_type_v2() == kpi_cha_normal

    kpiB = kpi.assign_to(userB)

    assert kpiB.kpi_type_v2() == kpi_cha_duoc_phan_cong

    cascaded_kpi = kpiB.cascaded_from

    assert cascaded_kpi.kpi_type_v2() == kpi_trung_gian
    assert cascaded_kpi.refer_to == None
    assert cascaded_kpi.parent == kpi

    remove_kpi_from_refer_to(kpiB, userA)

    assert kpi.get_children().count() == 0
    assert KPI.objects.filter(id=cascaded_kpi.id).first() == None
    assert KPI.objects.filter(id=kpiB.id).first().kpi_type_v2() == kpi_cha_normal
    assert KPI.objects.filter(id=kpiB.id).first().refer_to_id == None
    assert KPI.objects.filter(id=kpiB.id).first().cascaded_from_id == None

    kpi_1 = KPI()
    kpi_1.save()
    assert remove_kpi_from_refer_to(kpi_1).cascaded_from == None


@pytest.mark.django_db(transaction=False)
def test_copy_attrs_to_children(kpi, userA, userB, userC, organization):

    assert copy_attrs_to_children(None, userA) == False
    kpi = KPI()
    kpi.user = userA
    kpi.save()

    kpi1_1 = kpi.assign_to(userB)
    kpi1_2 = kpi.assign_to(userB)

    kpi1_1_1 = kpi1_1.assign_to(userC)
    kpi1_1_2 = kpi1_1.assign_to(userC)

    kpi1_2_1 = kpi1_2.assign_to(userC)
    kpi1_2_2 = kpi1_2.assign_to(userC)

    assert kpi1_1.id != kpi1_2.id
    assert kpi1_1_1.id != kpi1_1_2.id
    assert kpi1_2_1.id != kpi1_2_2.id

    kpi.name = random_string(50)
    kpi.unit = random_string(50)
    kpi.current_goal = random_string(50)
    kpi.future_goal = random_string(50)
    kpi.operator = random.choice(['<=', ">=", "="])

    kpi.target = random.randint(1, 100)
    kpi.quarter_one_target = random.randint(1, 100)
    kpi.quarter_two_target = random.randint(1, 100)
    kpi.quarter_three_target = random.randint(1, 100)
    kpi.quarter_four_target = random.randint(1, 100)

    kpi.month_1 = random.randint(1, 100)
    kpi.month_2 = random.randint(1, 100)
    kpi.month_3 = random.randint(1, 100)

    kpi.month_1_target = random.randint(1, 100)
    kpi.month_2_target = random.randint(1, 100)
    kpi.month_3_target = random.randint(1, 100)

    kpi.month_1_score = random.randint(1, 100)
    kpi.month_2_score = random.randint(1, 100)
    kpi.month_3_score = random.randint(1, 100)
    kpi.latest_score = random.randint(1, 100)

    kpi.score_calculation_type = random.choice(['sum', "average", "most_recent"])
    kpi.review_type = random.choice(['', "monthly"])

    kpi.real = random.randint(1, 100)
    kpi.save(add_queue=True)

    kpi = KPI.objects.get(id=kpi.id)

    copy_attrs_to_children(kpi, kpi.user)

    def check_dup(kpi1, kpi2_id):
        check_duplicated_data(kpi1, KPI.objects.get(id=kpi2_id), only_fields=['name',
                                                                              'unit',
                                                                              'current_goal',
                                                                              'future_goal',
                                                                              'operator',
                                                                              'target',
                                                                              'quarter_one_target',
                                                                              'quarter_two_target',
                                                                              'quarter_three_target',
                                                                              'quarter_four_target',
                                                                              'month_1',
                                                                              'month_2',
                                                                              'month_3',
                                                                              # 'month_1_target',
                                                                              #  'month_2_target',
                                                                              #  'month_3_target',
                                                                              #  'month_1_score',
                                                                              #  'month_2_score',
                                                                              #   'month_3_score',
                                                                              #   'latest_score',
                                                                              'score_calculation_type',
                                                                              'review_type',
                                                                              #  'real',
                                                                              ])

    check_dup(kpi, kpi1_1.id)
    check_dup(kpi, kpi1_2.id)
    check_dup(kpi, kpi1_1_1.id)
    check_dup(kpi, kpi1_1_2.id)
    check_dup(kpi, kpi1_2_1.id)
    check_dup(kpi, kpi1_2_2.id)


@pytest.mark.django_db(transaction=False)
def test_duplicate_kpi(kpi, userA, userB, organization):
    k = clone_kpi_full(kpi)

    k_1 = KPI()
    k_1.user = userA
    k_1.save()

    assert duplicate_kpi(None, userA) == False
    assert duplicate_kpi(k_1, userB) == False

    excluded_field = ['created_at', 'id', 'unique_key', 'hash', 'log_trace', 'group_kpi']
    check_duplicated_data(k, kpi, excluded_field)

    # **** test: kpi_cha_normal
    k1 = KPI()
    k1.user = userA
    k1.save()

    k1x = duplicate_kpi(k1, userA)

    check_duplicated_data(k1x, k1, excluded_field)

    # **** test: kpi_con_normal
    k1_c = KPI()
    k1_c.user = userA
    k1_c.parent = k1
    k1_c.refer_to = k1
    k1_c.save()

    '''
    k1 --> k1_c (con)
    '''

    assert k1.get_children().count() == 1
    assert k1.get_children_refer().count() == 1

    k1_cx = duplicate_kpi(k1_c, userA)

    excluded_field.append('refer_to')
    check_duplicated_data(k1_cx, k1_c, excluded_field)
    # print 'k1_cxsx'

    '''
    k1 --> k1_c (con)
       --> k1_cx (con)
    '''

    assert k1.get_children().count() == 2
    assert k1.get_children_refer().count() == 2

    # **** test: kpi_cha_duoc_phan_cong

    k1_assigned = k1.assign_to(userB)

    '''
    k1 --> k1_c (con)
       --> k1_cx (con)
       --> k1_assigned_trung_gian <-- k1_assigned.cascaded_from
    '''

    assert k1.get_children().count() == 3
    assert k1.get_children_refer().count() == 3

    # print k1_assigned.kpi_type_v2()
    k1_assignedx = duplicate_kpi(k1_assigned, userA)

    '''
       k1 --> k1_c (con)
          --> k1_cx (con)
          --> k1_assignremove_kpied_trung_gian <-- k1_assigned.cascaded_from
          --> k1_assignedx_trung_gian <-- k1_assignedx.cascaded_from
    '''

    assert k1.get_children_refer().count() == 4
    assert k1.get_children().count() == 4

    # print 'k1_cx2'
    assert k1_assigned.cascaded_from != k1_assignedx.cascaded_from
    excluded_field.extend(['cascaded_from', 'ordering'])

    check_duplicated_data(k1_assignedx, k1_assigned, excluded_field)

    #
    # # **** test: kpi_trung_gian
    # cascaded_fromx = duplicate_kpi(k1_assigned.cascaded_from, userA)
    # excluded_field.remove('ordering')
    # excluded_field.remove('cascaded_from')
    # excluded_field.append('refer_to')
    # excluded_field.append('month_1_score')
    # excluded_field.append('month_2_score')
    # excluded_field.append('month_3_score')
    #
    # # assert cascaded_fromx.month_1_score == k1_assigned.cascaded_from.month_1_score
    # check_duplicated_data(cascaded_fromx, k1_assigned.cascaded_from, excluded_field)

    # **** test: kpi_trung_gian

    # **** test: kpi_cha_normal

    #    print k1.kpi_type_v2()
    k1x2 = duplicate_kpi(k1, userA)

    assert k1x2.get_children().count() == k1.get_children().count()


@pytest.mark.django_db(transaction=False)
def test_add_user_comment(userA):
    kpi_test = KPI(user=userA, name='KPI Test')
    kpi_test.save()
    content = kpi_test.name
    unique_key = kpi_test.unique_key
    kc = add_user_comment(userA, kpi_test, content, unique_key)
    assert kc.kpi_unique_key == unique_key and \
           kc.kpi == kpi_test and \
           kc.content == content and \
           kc.user == userA and \
           kc.send_from_ui == 'web'
    kc.delete()
    kpi_test.delete()

    kc = add_user_comment(userA, kpi_test, content, '')
    assert kc.kpi_unique_key == kpi_test.unique_key


@pytest.mark.django_db(transaction=False)
def test_add_auto_comment(userA):
    # auto comment will be in action when updating a KPI, so create a new comment then delete it.
    kpi_test = KPI(user=userA, name='KPI Test')
    kpi_test.save()
    unique_key = kpi_test.unique_key  # get unique key
    KPIServices().update_kpi(actor=userA, kpi=kpi_test, de_quy_len=False, de_quy_xuong=False,
                             weight=9)  # try to update KPI

    # now=datetime.datetime.now()
    kc = KpiComment.objects.filter(kpi_id=kpi_test.id).order_by('id').last()  # get last comment

    assert kc.kpi_unique_key == unique_key and \
           kc.kpi == kpi_test and \
           kc.content != 'Update: ' and len(kc.content) > 0 and \
           kc.user == userA and \
           kc.send_from_ui == 'auto'
    kc.delete()  # delte when finished test
    kpi_test.delete()


@pytest.mark.django_db(transaction=False)
# TODO: temporary disable until fixed https://gitlab.com/cjs/cloudjet/issues/1
def _test_upload_comment_file(userA):
    kpi_test = KPI(user=userA, name='KPI Test')
    kpi_test.save()
    unique_key = kpi_test.unique_key  # get unique key
    file = SimpleUploadedFile('test.txt', 'test kpi comment ahihi')
    kc = add_comment_upload_file(userA, kpi_test, file)
    assert kc.kpi_unique_key == unique_key and \
           kc.kpi == kpi_test and \
           file.name.replace('.txt', '') in kc.get_file_name() and \
           kc.user == userA and \
           kc.send_from_ui == 'web'
    kc.delete()
    kpi_test.delete()

######################
# # Vui lòng không xoá test nếu không hiểu rõ mình đang làm gì
######################


@pytest.mark.django_db(transaction=False)
def test_calculate_avg_score(organization, userA):
    k = KPI()
    k.user = userA
    k.name = 'parent k'
    k.review_type = 'monthly'
    k.quarter_period = organization.get_current_quarter()
    k.save()

    k1 = KPI()
    k1.user = userA
    k1.name = 'child k1'
    k1.quarter_period = organization.get_current_quarter()
    k1.parent = k
    k1.refer_to = k
    k1.review_type = 'monthly'
    k1.month_1 = 10
    k1.month_1_target = 10
    k1.operator = ">="
    k1.save_calculate_score(calculate_now_thread=False)

    k2 = KPI()
    k2.user = userA
    k2.review_type = 'monthly'
    k2.name = 'child k2'
    k2.quarter_period = organization.get_current_quarter()
    k2.parent = k
    k2.refer_to = k
    k2.month_1 = 5
    k2.month_1_target = 10
    k2.operator = ">="
    k2.save_calculate_score(calculate_now_thread=False)

    assert k1.weight == 10
    assert k1.get_cascaded_from_weight() == 10
    assert k2.weight == 10
    assert k2.get_cascaded_from_weight() == 10

    k1 = calculate_score_cascading(k1.id)
    k2 = calculate_score_cascading(k2.id)

    assert k1.month_1_score == 100
    assert k2.month_1_score == 50

    children_data = get_children_data(k.user, k)
    assert children_data['parent_score_auto'] == True
    assert children_data['children_weights'][0]['weight'] == 10
    assert children_data['children_weights'][1]['weight'] == 10

    assert k.calculate_avg_score()[0] == 75

    '''
    These part of test is not used for v5
    '''
    #
    # k1.weight = 30
    # k1.save()
    #
    #
    # k2.weight = 15
    # k2.save()
    #
    # assert k1.get_cascaded_from_weight() == 30
    # assert k2.weight == 15
    # assert k2.get_cascaded_from_weight() == 15
    #
    # assert k1.get_score() == 100
    # assert k2.get_score() == 50
    #
    # children_data = get_children_data(k.user, k)
    # assert children_data['parent_score_auto'] == True
    # assert children_data['children_weights'][0]['weight'] == 15 or children_data['children_weights'][0]['weight'] == 30
    # assert children_data['children_weights'][1]['weight'] == 15 or children_data['children_weights'][1]['weight'] == 30
    #
    # assert k.calculate_avg_score()[0] == float(3500) / 45
    #
    # k1.weight = 0
    # k1.save()
    # k2.weight = 0
    # k2.save()
    #
    # assert k.calculate_avg_score()[0] == 0

    '''
    test parent_score_auto
    '''
    organization.default_auto_score_parent = True
    organization.save()

    children_data = get_children_data(k.user, k)
    children_data['parent_score_auto'] = False
    set_children_data(k.user, k, children_data)

    assert k.calculate_avg_score()[0] == None

    k.month_1 = 50
    k.month_1_target = 100

    k.score_calculation_type = "most_recent"
    k.review_type = "monthly"
    k.save()
    assert k.calculate_avg_score()[0] == 50  # quarter
    assert k.calculate_avg_score()[1] == 50  # month_1

    k.month_2 = 80
    k.month_2_target = 100
    k.save()
    assert k.calculate_avg_score()[0] == 80  # quarter
    assert k.calculate_avg_score()[1] == 50  # month_1
    assert k.calculate_avg_score()[2] == 80  # month_2

    k.month_3 = 90
    k.month_3_target = 100

    k.save()
    assert k.calculate_avg_score()[0] == 90  # quarter
    assert k.calculate_avg_score()[1] == 50  # month_1
    assert k.calculate_avg_score()[2] == 80  # month_2
    assert k.calculate_avg_score()[3] == 90  # month_3


def check_duplicated_data(object_1, object_2,
                          excluded_field=None,
                          only_fields=None):
    if only_fields:
        for attr in only_fields:
            print "attr: {}".format(attr)

            if isinstance(getattr(object_1, attr), (int, long, float)):
                assert round(getattr(object_1, attr), 2) == round(getattr(object_2, attr), 2)
            else:
                assert getattr(object_1, attr) == getattr(object_2, attr)
    else:

        opts = object_1._meta

        for f in opts.concrete_fields:  # + opts.many_to_many:
            # print "f.name: {}".format(f.name)
            if f.name in excluded_field:

                continue
                # assert  f.value_from_object(object_1) != f.value_from_object(object_2)
            else:
                assert f.value_from_object(object_1) == f.value_from_object(object_2)


@pytest.mark.django_db(transaction=False)
def test_copy_to_new_user(kpi, userA, userB, userC, organization, organization2):
    # test: kpi_cha_duoc_phan_cong
    kpi_c = copy_to_new_user(kpi, userB, None, None)
    assert kpi_c == False

    kpi_c = copy_to_new_user(kpi, None, None, userB)
    assert kpi_c == False

    kpi_c = copy_to_new_user(None, userB, None, userB)
    assert kpi_c == False

    kpi_cha_duoc_phan_cong_uA = KPI()
    kpi_cha_duoc_phan_cong_uA.user = userA
    kpi_cha_duoc_phan_cong_uA.save()

    kpi_uB = copy_to_new_user(kpi_cha_duoc_phan_cong_uA, userB, None, userB)
    assert kpi_uB.user == userB
    assert kpi_uB.id != kpi_cha_duoc_phan_cong_uA.id

    kpi_uC = copy_to_new_user(kpi_cha_duoc_phan_cong_uA, userC, None, userC)
    assert kpi_uC.quarter_period == userC.profile.get_organization().get_current_quarter()

    kpi_con_normal_uA = KPI()
    kpi_con_normal_uA.parent = kpi_cha_duoc_phan_cong_uA
    kpi_con_normal_uA.user = userA
    kpi_con_normal_uA.save()

    kpi_uB2 = copy_to_new_user(kpi_cha_duoc_phan_cong_uA, userB, kpi_uB, userB)

    assert kpi_uB2.user == userB
    assert kpi_uB2.parent == None
    assert kpi_uB2.refer_to == None
    assert kpi_uB2.get_children().count() > 0
    for c in kpi_uB2.get_children():
        assert c.user == userB
        assert c.owner_email == userB.email

    kpi_con_normal_uB = copy_to_new_user(kpi_con_normal_uA, userB, kpi_uB, userB)
    assert kpi_con_normal_uB.user == userB
    assert kpi_con_normal_uB.parent == kpi_uB
    assert kpi_con_normal_uB.refer_to == kpi_uB

    kpi_con_normal_uB = copy_to_new_user(kpi_con_normal_uA, userB, None, userB)
    assert kpi_con_normal_uB.user == userB
    assert kpi_con_normal_uB.parent == None
    assert kpi_con_normal_uB.refer_to == None

    kpi_con_normal_uC = copy_to_new_user(kpi_con_normal_uB, userC, kpi_uC, userC)
    assert kpi_con_normal_uC.quarter_period == userC.profile.get_organization().get_current_quarter()


@pytest.mark.django_db(transaction=False)
def test_calculate_cascade_score(userA, userB, organization):
    k = KPI()
    k.quarter_period = organization.get_current_quarter()
    k.name = "Ten kpi"
    k.month_1 = 50
    k.month_1_target = 200
    k.operator = ">="

    k.save()

    assert calculate_score_cascading(None) == False

    kpi = calculate_score_cascading(k.id)

    assert kpi.month_1_score == 25

    k.operator = "="
    k.save()
    kpi = calculate_score_cascading(k.id)
    assert round(kpi.month_1_score, 1) == 0

    k.operator = "<="
    k.save()

    kpi = calculate_score_cascading(k.id)
    assert kpi.month_1_score == 120

    kpi_1 = KPI()
    kpi_1.quarter_period = organization.get_current_quarter()
    kpi_1.name = 'kpi_1'
    kpi_1.month_1 = 0
    kpi_1.month_1_target = 100
    kpi_1.operator = ">="
    kpi_1.user = userA
    kpi_1.save()

    kpi_2 = KPI()
    kpi_2.quarter_period = organization.get_current_quarter()
    kpi_2.name = 'kpi_2'
    kpi_2.month_1 = 0
    kpi_2.month_1_target = 100
    kpi_2.operator = ">="
    kpi_2.user = userB

    kpi_2.save()

    kpi_2.set_score(123)

    kpi_1.assign_up(kpi_2)

    kpi_trunggian = kpi_2.get_children()[0]
    kpi_phancong = kpi_2.get_children_refer_order()[0]

    kpi_1 = calculate_score_cascading(kpi_1.id)

    assert kpi_1.month_1_score == 0
    assert kpi_2.month_1_score == 0  # 123

    kpi_trunggian.latest_score = 35
    kpi_trunggian.save()
    # kpi_2.latest_score = 25
    # kpi_2.save()

    assert kpi_phancong.id == kpi_1.id

    assert kpi_1.cascaded_from == kpi_trunggian
    assert kpi_trunggian.parent == kpi_2
    assert kpi_phancong.refer_to == kpi_2

    calculate_score_cascading(kpi_1.id)

    assert round(kpi_1.get_score(), 0) == 0
    assert round(kpi_2.get_score(), 0) == 0
    assert round(kpi_phancong.get_score(), 0) == 0
    # print kpi_trunggian.latest_score

    kpi_trunggian = kpi_2.get_children()[0]

    assert round(kpi_trunggian.get_score(), 0) == 0

    # test case: when KPI_trung_gian have different attribute compare to kpi_phan_cong
    kpi_trunggian.real = 9999
    calculate_score_cascading(kpi_1.id)

    calculate_score_cascading(kpi_trunggian.id)

    assert round(kpi_trunggian.get_score(), 0) == 0
    # TODO:

#
#
# @pytest.mark.django_db(transaction=False)
# def test_calculate_cascade_score_multi_level( userA_manager, userA_manager_manager, userA, userB):
#     '''
#     testing different level
#     '''
#     k = KPI()
#     k.name = "Ten kpi"
#     k.real = 50
#     k.target = 200
#     k.operator = ">="
#     k.user = userA_manager_manager
#
#     k.save()
#
#     assert  k.latest_score == 25
#
#     k1 = k.assign_to(userA_manager)
#
#     assert k1.user == userA_manager


@pytest.mark.django_db(transaction=False)
def test_get_ecal_link():
    k = KPI()
    k.save()

    assert get_ecal_link('TEST') == ""
    el = get_ecal_link(k.get_unique_key())

    assert el.find('https://ethercalc.org/') >= 0

    x = KPI()
    x.cascaded_from = k
    x.save()

    assert el == get_ecal_link(x.get_unique_key())


@pytest.mark.django_db(transaction=False)
def test_search_kpi(kpi, userA, organization):
    kpi.quarter_period = organization.get_current_quarter()
    kpi.save()

    kpis = search_kpi(kpi.name, kpi.user)
    assert kpi in kpis

    p = kpi.user.profile
    p.display_name = 'some name'
    p.title = 'some title'
    p.department = 'some department'
    p.save()

    assert kpi in search_kpi("name:{}".format(p.display_name), kpi.user)
    assert kpi in search_kpi("title:{}".format(p.title), kpi.user)
    assert kpi in search_kpi("department:{}".format(p.department), kpi.user)
    assert kpi in search_kpi("{} no_lib".format(kpi.name), kpi.user)


#    assert kpi not in search_kpi("{} only_lib".format(kpi.name), kpi.user)
@pytest.mark.django_db(transaction=False)
def test_update_kpi(kpi, userA, ceo, userB, userA_manager, organization):
    flag = 0
    try:
        update_kpi(userB, kpi.id, name="XXX")
    except PermissionDenied:
        flag = 1
    assert flag == 1

    update_kpi(userA, kpi.id, name="XXX")
    assert get_kpi(kpi.id, actor=userA).name == "XXX"

    k = kpi.assign_to(userB)

    assert k.name == kpi.name
    assert k.id != kpi.id

    '''
    GIVEN: k.cascaded_from = kpi
    THEN: change kpi.name --> k.name also changes
    '''

    assert update_kpi(userA, kpi.id, name="123asd") != False
    assert get_kpi(k.id, actor=userA).name.lower() == "123asd"

    res = {'status': 'failed', 'message': _('Weight is not smaller zero')}
    response = update_kpi(userA, kpi.id, weight=random.randint(-110000, 0))
    assert response == res

    assert update_kpi(ceo, k.id, name="123asd123123") != False
    assert get_kpi(kpi.id, actor=userA).name.lower() == "123asd123123"

    '''
        Test update weight when enable_weight_editing is False
    '''
    # Not allow edit weight of kpi owner
    assert has_perm(KPI__WEIGHT_EDIT, userA, kpi.user) is True
    # Allow edit weight of kpis of subordinates
    assert has_perm(KPI__WEIGHT_EDIT, userA_manager, kpi.user) is True

    organization.enable_weight_editing = False
    organization.save()
    # Not allow edit weight of kpi owner
    assert has_perm(KPI__WEIGHT_EDIT, userA, kpi.user) is False
    # Allow edit weight of kpis of subordinates
    assert has_perm(KPI__WEIGHT_EDIT, userA_manager, kpi.user) is True

    assert KPIServices().update_kpi(actor=userA, kpi=kpi, de_quy_len=False, de_quy_xuong=False,
                             reviewer_email='TEST@gmail.com')['status'] == 'failed'
    KPIServices().update_kpi(actor=userA, kpi=kpi, de_quy_len=False, de_quy_xuong=False,
                             weight='10')

    KPIServices().update_kpi(actor=userA, kpi=kpi, de_quy_len=False, de_quy_xuong=False,
                             group='TEST')

    kpi.weight = 0
    kpi.save()
    with pytest.raises(PermissionDenied) as excinfo:
        update_kpi(userA_manager, kpi.id, name='XXX', weight=1)

    KPIServices().update_kpi(actor=userA, kpi=kpi, de_quy_len=False, de_quy_xuong=False,
                             group='TEST', refer_group_name="Billionare at 30")

    assert kpi.refer_group_name != "Billionare at 30"

    assert kpi.group == "TEST"


@pytest.mark.django_db(transaction=False)
def test_update_year_kpis(userA, kpi, parent_kpi):
    pass


@pytest.mark.django_db(transaction=False)
def test_re_order(kpi, userA):
   assert  re_order(kpi) == None

   assert re_order(None) == False


@pytest.mark.django_db(transaction=False)
def test_get_all_object_kpi_by_unique_key(kpi, organization):
    get_all_object_kpi_by_unique_key(kpi.unique_key)


@pytest.mark.django_db(transaction=False)
def test_get_kpis_in_quarter(userA, ceo, userB, organization, kpi):
    kpis = get_kpis_in_quarter(ceo, kpi.id, organization.get_current_quarter().id)
    assert kpis == None

    kpi.quarter = organization.get_current_quarter().id
    kpi.save()
    kpis = get_kpis_in_quarter(ceo, kpi.id, organization.get_current_quarter().id)
    assert kpis == kpi

    flag = 0
    try:
        result = get_kpis_in_quarter(userB, kpi.id, organization.get_current_quarter().id)
    except PermissionDenied:
        flag = 1

    assert flag == 1


@pytest.mark.django_db(transaction=False)
def test_get_user_kpis_by_kwargs(userA, ceo, userB, organization, kpi):
    quarter_period = organization.get_current_quarter()
    kpis = get_user_kpis_by_kwargs(ceo, userA, userA, quarter_period.id)
    assert len(kpis) > 0

    kpis = get_user_kpis_by_kwargs(ceo, userB, userA, quarter_period.id)
    assert len(kpis) == 0

    """
    Case for Permission Denied.
    3rd argument passed as None as it is not used in the method
    """
    flag = 0
    try:
        result = get_user_kpis_by_kwargs(userB, ceo, None, quarter_period.id)
    except PermissionDenied:
        flag = 1

    assert flag == 1


@pytest.mark.django_db(transaction=False)
def test_get_org_kpis_by_kwargs(userA, ceo, organization):
    get_org_kpis_by_kwargs(ceo, userA, userA)


@pytest.mark.django_db(transaction=False)
def test_update_targets_from_quarter(userA, ceo, organization, kpi):
    kpi.quarter_period = organization.get_current_quarter()
    kpi.score_calculation_type = 'sum'
    kpi.save()
    kpis = update_targets_from_quarter(kpi)
    assert kpis.id == kpi.id

    kpi.quarter_period.quarter = 2
    kpi.save()
    kpis = update_targets_from_quarter(kpi)
    assert kpis.id == kpi.id

    kpi.quarter_period.quarter = 3
    kpi.save()
    kpis = update_targets_from_quarter(kpi)
    assert kpis.id == kpi.id

    kpi.quarter_period.quarter = 4
    kpi.save()
    kpis = update_targets_from_quarter(kpi)
    assert kpis.id == kpi.id


@pytest.mark.django_db(transaction=False)
def test_update_kpi_target_from_month_targets(userA, ceo, organization, kpi):
    kpi.score_calculation_type == ''
    kpi.month_1_target = 10
    kpi.month_2_target = 10
    kpi.month_3_target = 10
    kpi.save()

    kpis = update_kpi_target_from_month_targets(kpi)
    assert kpis.month_1_target == 10


@pytest.mark.django_db(transaction=False)
def test_update_month_targets_onchange_calculation_type(userA, ceo, organization, kpi):
    kpi.score_calculation_type = 'average'
    kpi.target = 20
    kpi.month_1_target = 10
    kpi.month_2_target = 10
    kpi.month_3_target = 10
    kpi.save()

    kpis = update_month_targets_onchange_calculation_type(kpi, save=True)
    assert kpis.id == kpi.id

    kpis = update_month_targets_onchange_calculation_type(kpi, save=True)
    assert kpis.id == kpi.id

    kpi.score_calculation_type = 'sum'
    kpi.month_1_target = None
    kpi.month_2_target = None
    kpi.month_3_target = None
    kpi.save()

    kpis = update_month_targets_onchange_calculation_type(kpi, save=True)
    assert kpis.id == kpi.id

    kpi2 = KPI()
    kpi2.target = 10
    kpi2.score_calculation_type = 'most_recent'
    kpi2.month_3_target == None
    kpi2.save()

    kpis = update_month_targets_onchange_calculation_type(kpi2, save=True)
    assert kpis.month_3_target == 10

    # ## test for save=False
    def create_kpi(score_calculation_type, target, month_1_target, month_2_target, month_3_target):
        k = KPI()
        k.save()
        # do update directly to database
        KPI.objects.filter(id=k.id).update(score_calculation_type=score_calculation_type, target=target, month_1_target=month_1_target, month_2_target=month_2_target, month_3_target=month_3_target)
        return KPI.objects.get(id=k.id)

    k3 = create_kpi('most_recent', 60, None, None, None)
    k3.score_calculation_type = 'sum'

    # k, k3 is kpi with runtime data, not actually real db data
    k = update_month_targets_onchange_calculation_type(k3, save=False)
    # get real kpi data from kpi
    k_db = KPI.objects.get(id=k3.id)

    # assert for runtime data
    assert k.month_1_target == round(float(k.target) / 3, 2)
    assert k.month_2_target == round(float(k.target) / 3, 2)
    assert k.month_3_target == round(float(k.target) / 3, 2)
    assert k.score_calculation_type == 'sum'
    # assert for real data when save=False
    assert k_db.score_calculation_type != k.score_calculation_type
    assert k_db.month_1_target is None and k_db.month_2_target is None and k_db.month_3_target is None

    k4 = create_kpi('most_recent', 60, None, None, None)
    k4.score_calculation_type = 'average'
    # k is kpi with runtime data, not actually real db data
    k = update_month_targets_onchange_calculation_type(k4, save=False)
    # get real kpi data from kpi
    k_db = KPI.objects.get(id=k4.id)

    # assert for runtime data
    assert k.month_1_target == k.target
    assert k.month_2_target == k.target
    assert k.month_3_target == k.target
    assert k.score_calculation_type == 'average'
    # assert for real data when save=False, nothing save to database
    assert k_db.score_calculation_type != k.score_calculation_type
    assert k_db.month_1_target is None and k_db.month_2_target is None and k_db.month_3_target is None

    k5 = create_kpi('average', 60, 60, 60, None)
    k5.score_calculation_type = 'most_recent'
    # k is kpi with runtime data, not actually real db data
    k = update_month_targets_onchange_calculation_type(k5, save=False)
    # get real kpi data from kpi
    k_db = KPI.objects.get(id=k5.id)

    # assert for runtime data
    assert k.month_3_target == k.target
    assert k.score_calculation_type == 'most_recent'
    # assert for real data when save=False, nothing save to database
    assert k_db.score_calculation_type != k.score_calculation_type
    assert k_db.month_3_target is None


@pytest.mark.django_db(transaction=False)
def test_add_comment_upload_file(userA, ceo, organization, kpi):
    comment = add_comment_upload_file(userA, kpi, '', unique_key='')
    assert comment.user == userA

    comment = add_comment_upload_file(userA, kpi, '')
    assert comment.user == userA


@pytest.mark.django_db(transaction=False)
def test_convert_kpis_tojson(userA, ceo, organization, kpi):
    kpi.bsc_category = 'financial'
    kpi.quarter_period = organization.get_current_quarter()
    kpi.user = userA
    kpi.save()

    kpis = userA.profile.get_kpis(organization.get_current_quarter())
    json_k = convert_kpis_tojson(kpis, category='financial')
    assert len(json_k) > 0


@pytest.mark.django_db(transaction=False)
def test_track_change(userA, kpi):
    old_refer_group_name = u'TÊN GROUP CŨ'
    kpi.refer_group_name = old_refer_group_name
    kpi.save()
    reload_model_obj(kpi)
    new_name = u"TÊN CỦA KPI"
    new_refer_group_name = u"TÊN CỦA KPI"
    setattr(kpi, 'name', new_name)
    setattr(kpi, 'refer_group_name', new_refer_group_name)
    track_change(userA, kpi)
    changed_content = kpi.get_changed_history()
    key = 'refer_group_name'
    title = kpi.get_custom_label(key) if kpi.get_custom_label(key) else kpi._meta.get_field(key).verbose_name
    comment = u"- %s: %s → %s" % (title, old_refer_group_name, new_refer_group_name)

    kpi.save()
    reload_model_obj(kpi)

    def get_latest_comment(kpi):
        return KpiComment.objects.filter(kpi=kpi).latest('created_at')

    assert ugettext(kpi.name) == u"TÊN CỦA KPI"
    assert comment in changed_content
    assert comment not in get_latest_comment(kpi).content


@pytest.mark.django_db(transaction=False)
def test_get_one_kpicomment(kpi, client_authed, organization):
    pass


@pytest.mark.django_db(transaction=False)
def test_set_review_type(userA, ceo, organization, kpi):
    kpis = set_review_type(kpi, 10, 10, 10)
    assert kpis.month_1 == 10

    kpis = set_review_type(None, 10, 10, 10)
    assert kpis == None


@pytest.mark.django_db(transaction=False)
def test_get_kpi_by_id(userA, ceo, organization, kpi):
    kpi_id = get_kpi_by_id(userA, kpi.id)
    assert kpi_id['id'] == kpi.id

    kpi_id = get_kpi_by_id(userA, "TEST")
    assert kpi_id == None


@pytest.mark.django_db(transaction=False)
def test_get_kpi_parent_by_id(userA, ceo, organization, parent_kpi, kpi):
    kpi.parent = parent_kpi
    kpi.refer_to = parent_kpi
    kpi.save()
    assert len(parent_kpi.get_children()) == 1
    kpi_parent = get_kpi_parent_by_id(userA, parent_kpi.id)

    assert len(kpi_parent) > 0


@pytest.mark.django_db(transaction=False)
def test_get_kpi_parent_related(userA, ceo, organization, kpi):
    kpi_id = get_kpi_parent_related(kpi.id)
    assert kpi_id.id == kpi.id

    kpi_id = get_kpi_parent_related(0)
    assert kpi_id == None


@pytest.mark.django_db(transaction=False)
def test_create_new_kpi(userA, ceo, organization, parent_kpi, kpi):
    kpi_create = create_new_kpi(ceo, {'user_id':userA.id, 'parent_kpi':parent_kpi.id, 'category':'financial'})
    assert kpi_create.bsc_category == None

    kpi_create = create_new_kpi(ceo, {'user_id': userA.id, 'category': 'financial'})
    assert kpi_create.bsc_category == 'financial'

    kpi_create = create_new_kpi(ceo, {'user_id': userA.id, 'category':None})
    assert kpi_create == False


@pytest.mark.django_db(transaction=False)
def test_get_input_update_score(userA, ceo, organization, parent_kpi, kpi):
    kpi_score = get_input_update_score(ceo, kpi, {'operator': '>=', 'score_calculation_type':'sum', 'real':'', 'month_1':1, 'month_2':2, 'month_3':3, 'unit':'score'})
    assert kpi_score['month_1'] == 1
    assert kpi_score['real'] == ''


@pytest.mark.django_db(transaction=False)
def test_update_score(userA, ceo, organization, parent_kpi, kpi):
    kpi.weight = 10
    kpi.score_calculation_type = None
    kpi.save()

    kpi_score = update_score(kpi, userA, 1, 2, 3, '')
    assert kpi_score.id == kpi.id
    assert kpi_score.score_calculation_type == "most_recent"


@pytest.mark.django_db(transaction=False)
def test_delele_all_kpis(userA, userAdmin, organization, parent_kpi, kpi):
    delete_kpis = delele_all_kpis(userA.id, userAdmin, permission=True)
    assert delete_kpis == True


@pytest.mark.django_db(transaction=False)
def test_get_edge_kpi(userA, ceo, organization, parent_kpi, kpi):
    kpi.assign_to(userA)
    kpi.refer_to = parent_kpi
    kpi.save()

    edge_kpi = get_edge_kpi(parent_kpi.id)
    assert edge_kpi == kpi

    kpi.cascaded_from = parent_kpi
    kpi.save()
    edge_kpi = get_edge_kpi(kpi.id)
    assert edge_kpi == kpi


@pytest.mark.django_db(transaction=False)
def test_export_kpicomment_to_excel(userA, ceo, organization, parent_kpi, kpi):
    kcs = export_kpicomment_to_excel(kpi)
    assert kcs == ''
    kpi_comment = KpiComment()
    kpi_comment.user = userA
    kpi_comment.kpi = kpi
    kpi_comment.kpi_unique_key = kpi.unique_key
    kpi_comment.send_from_ui = 'web'
    kpi_comment.save()

    kcs = export_kpicomment_to_excel(kpi)
    assert len(kcs) > 0


@pytest.mark.django_db(transaction=False)
def test_calculate_weighted_weight_assigner_view(userA, ceo, organization, kpi):
    kpi.cascaded_from = None
    kpi.save()

    kpi_weight = calculate_weighted_weight_assigner_view(kpi.id)
    assert kpi_weight == 100


@pytest.mark.django_db(transaction=False)
def test_calculate_weighted_weight(userA, ceo, organization, parent_kpi, kpi):
    kpi_weight = calculate_weighted_weight(kpi.id)
    assert kpi_weight == kpi.get_weighted_weight()


@pytest.mark.django_db(transaction=False)
def test_calculate_weighted_weight_by_kpi(userA, ceo, organization, parent_kpi, kpi):
    calculate_weighted_weight_by_kpi(kpi)


@pytest.mark.django_db(transaction=False)
def test_create_kpi(userA, userA_manager, ceo, organization, parent_kpi):

    kpi = create_kpi(creator=ceo, user_id=userA.id)
    assert kpi.weight == 10

    kpi2 = KPIServices().create_kpi(creator=ceo, name="New KPI...", year=2017, refer_to=kpi,
                                 quarter_period=organization.get_current_quarter(),
                                 bsc_category='financial', quarter=1, operator='≥', parent_id=parent_kpi.id, user_id=userA.id)
    assert kpi2.id > 0

    kpi2 = KPIServices().create_kpi(creator=ceo, name="New KPI...", year=2017, user=userA,
                                 bsc_category='financial', quarter=1, operator='≤', parent=parent_kpi, weight=None, user_id=userA.id)
    assert kpi2.id > 0

    kpi2 = KPIServices().create_kpi(creator=ceo, name="New KPI...", year=2017, user=userA
                                 , quarter=1, operator='≤', parent_id=parent_kpi.id, weight=None, ordering=10, user_id=userA.id)
    assert kpi2.id > 0

    """
    Case: Permission Denied
    """
    flag = 0
    try:
        kpi2 = KPIServices().create_kpi(creator=userA, name="New KPI...", year=2017, user=ceo
                                    , quarter=1, operator='≤', parent_id=parent_kpi.id, weight=None, ordering=10)
    except PermissionDenied:
        flag = 1

    assert flag == 1

    """
    Case: bsc_category = None
    """
    kpi3 = KPIServices().create_kpi(creator=ceo, name="New KPI...", year=2017, refer_to=kpi,
                                    quarter_period=organization.get_current_quarter(), bsc_category=None,
                                    quarter=1, operator='≥', parent_id=parent_kpi.id, user_id=userA.id)
    assert kpi3.id > 0


@pytest.mark.django_db(transaction=False)
def test_get_kpi_by_code_user(userA, userB, ceo, organization, kpi):
    kpi.code = 'F'
    kpi.save()
    kpi_code = get_kpi_by_code_user('F', userA, organization.get_current_quarter(), ceo)
    assert kpi_code.id == kpi.id

    kpi_code = get_kpi_by_code_user('F', userA, organization.get_current_quarter(), None)
    assert kpi_code == None


@pytest.mark.django_db(transaction=False)
def test_get_kpi_by_name_user(userA, userB, ceo, organization, kpi):

    kpi_name = get_kpi_by_name_user('CHILD KPI', userA, organization.get_current_quarter(), ceo)
    assert kpi_name.name == kpi.name

    kpi_name = get_kpi_by_name_user('CHILD KPI', userA, organization.get_current_quarter(), None)
    assert kpi_name == None


@pytest.mark.django_db(transaction=False)
def test_get_kpi_by_hash(userA, ceo, organization, kpi):
    kpi_hash = get_kpi_by_hash(kpi.hash, ceo)
    assert kpi_hash == kpi


@pytest.mark.django_db(transaction=False)
def test_filter_kpi_by_quarter(userA, ceo, organization, kpi):
    kpi.quarter_period = organization.get_current_quarter()
    kpi.save()

    kpi_filter = filter_kpi_by_quarter(organization.get_current_quarter(), ceo)
    assert len(kpi_filter) > 0


@pytest.mark.django_db(transaction=False)
def test_get_deleted_kpi(userA, ceo, organization, kpi):
    kpi.delete()

    kpi_deleted = get_deleted_kpi(kpi.id, ceo)
    assert kpi_deleted == kpi


@pytest.mark.django_db(transaction=False)
def test_get_kpi_template(userA, ceo, organization, kpi):
    kpi.quarter_period = organization.get_current_quarter()
    kpi.save()

    kpi_template = get_kpi_template('CHILD KPI', ceo, userA)
    assert kpi_template.id == kpi.id
    assert kpi_template.name == kpi.name


@pytest.mark.django_db(transaction=False)
def test_get_kpi_cache(userA, organization, kpi):
    kpi_cache = get_kpi_cache(kpi.id, userA)
    assert kpi_cache.user == userA


@pytest.mark.django_db(transaction=False)
def test_re_order(userA, organization, kpi, parent_kpi):
    kpi.parent = parent_kpi
    kpi.save()
    assert re_order(kpi) == None

    kpi_reorder = re_order(None)
    assert kpi_reorder == False

    assert re_order_thread(kpi) == None
    assert re_calculate(kpi).weight == kpi.weight
    assert re_calculate(None) == False


@pytest.mark.django_db(transaction=False)
def test_log_delete_kpi(userA, ceo, organization, kpi, parent_kpi):
    kpi_log = log_delete_kpi(None, kpi)
    assert kpi_log == False

    kpi_log = log_delete_kpi(ceo, kpi)
    assert kpi_log == None


@pytest.mark.django_db(transaction=False)
def test_remove_all_kpi(userA, ceo, organization):
    remove = remove_all_kpi(userA, ceo)
    assert remove == None


@pytest.mark.django_db(transaction=False)
def test_remove_kpi_thread(userA, ceo, organization, kpi, parent_kpi):
    remove_kpi_thread(kpi.id, ceo)


@pytest.mark.django_db(transaction=False)
def test_clone_kpi_to_user(userA, userB, ceo, organization, kpi, parent_kpi):
    kpi.user = userA
    kpi.save()
    flag = 0
    try:
        kpi_clone = clone_kpi_to_user(userA, ceo, userB, True)
    except PermissionDenied:
        flag = 1
    assert flag == 1
    assert len(userB.profile.get_kpis()) == 0

    kpi1 = KPI()
    kpi1.user = userA
    kpi1.quarter_period = organization.get_current_quarter()
    kpi1.organization = organization
    kpi1.name = "Second kpi"
    kpi1.save()
    kpi_clone = clone_kpi_to_user(userA, userB, ceo, True, only_kpi_ids=[kpi1.id])
    assert kpi_clone is True
    assert len(userB.profile.get_kpis()) > 0
    assert KPI.objects.filter(user=userB).count() == 1

    kpi_clone = clone_kpi_to_user(userA, userB, ceo, True)
    assert kpi_clone is True


@pytest.mark.django_db(transaction=False)
def test_update_default_real(userA, ceo, organization, kpi):
    kpi_u = update_default_real(userA, kpi.id, 10)
    assert kpi_u.month_1 == 10


@pytest.mark.django_db(transaction=False)
def test_count_comment_by_kpiid(userA, ceo, organization, kpi):
    count = KPIServices.count_comment_by_kpiid(userA, kpi.id)
    assert count > 0

    mread = MarkAsReadComment()
    mread.kpi = kpi
    mread.read_user = userA
    mread.save()
    mread.read_date = datetime.date.today()
    mread.save()

    count = KPIServices.count_comment_by_kpiid(userA, kpi.id)
    assert count > 0
    check = MarkAsReadComment.objects.filter(kpi_id=kpi.id, read_user_id=userA.id).aggregate(
        last_read_date=Max('read_date')).get('last_read_date')
    assert check is not None

    count = KPIServices.count_comment(userA, kpi.unique_key)
    assert count > 0


@pytest.mark.django_db(transaction=False)
def test_get_target(userA, ceo, organization, kpi):

    kpi_q = KPIServices().get_target(kpi, None)
    assert kpi_q == None

    kpi.quarter_one_target = random.randint(1, 100)
    kpi.quarter_two_target = random.randint(1, 100)
    kpi.quarter_three_target = random.randint(1, 100)
    kpi.quarter_four_target = random.randint(1, 100)
    kpi.save()

    quarter = organization.get_current_quarter()
    quarter.quarter = 2
    kpi.quarter_period = quarter
    kpi.save()

    kpi_q = KPIServices().get_target(kpi, quarter)
    assert kpi_q is not None
    assert kpi_q == kpi.quarter_two_target

    quarter = organization.get_current_quarter()
    quarter.quarter = 3

    kpi_q = KPIServices().get_target(kpi, quarter)
    assert kpi_q is not None
    assert kpi.quarter_three_target == kpi_q

    quarter = organization.get_current_quarter()
    quarter.quarter = 4

    kpi_q = KPIServices().get_target(kpi, quarter)
    assert kpi_q is not None
    assert kpi.quarter_four_target == kpi_q

    quarter.quarter = random.randint(5, 100)
    quarter.save()

    kpi_q = KPIServices().get_target(kpi, quarter)
    assert kpi_q is not None
    assert kpi.quarter_four_target == kpi_q

    """
    To test none case
    """
    quarter.quarter = 0
    quarter.save()

    kpi_q = KPIServices().get_target(kpi, quarter)
    assert kpi_q is None


@pytest.mark.django_db(transaction=False)
def test_get_parent_kpis(userA, ceo, organization, kpi):
    kpi.user = userA
    kpi.save()
    kpis = KPIServices().get_parent_kpis(userA.id)
    assert len(kpis) > 0


@pytest.mark.django_db(transaction=False)
def test_get_parent_kpis_by_category(userA, ceo, organization, parent_kpi, kpi):
    kpi.user = userA
    kpi.bsc_category = 'financial'
    kpi.refer_to = parent_kpi
    kpi.save()

    parent_kpi.bsc_category = 'financial'
    parent_kpi.save()

    kpis = KPIServices().get_parent_kpis_by_category(userA.id, 'financial')
    assert len(kpis) > 0


@pytest.mark.django_db(transaction=False)
def test_update_archivable_score(userA, ceo, organization, parent_kpi, kpi):
    kpi_score = update_archivable_score(kpi.id, 10)


@pytest.mark.django_db(transaction=False)
def test_get_unique_code(kpi, organization):
    result = get_unique_code(kpi.id)
    assert str(kpi.id) in result
    fail_test_id = randint(1, 10000)
    result = get_unique_code(fail_test_id)
    assert result == ''


@pytest.mark.django_db(transaction=False)
def test_create_child_kpi(kpi, userA, userB, organization):
    """
    Negative Test - Permission Denied
    """
    quarter = organization.get_current_quarter()
    kpi1 = KPI()
    kpi1.user = userB

    kpi1.name = "Not kpi parent"

    kpi1.save()
    flag = True
    try:
        result = create_child_kpi(kpi1, quarter, kpi.name, userA)
    except PermissionDenied:
        flag = False

    assert flag is False


@pytest.mark.django_db(transaction=False)
def test_create_kpi_1(kpi, userA, userB, organization, parent_kpi):
    """
    Test for services/kpi.py::create_kpi
    Case: Permission Denied
    creator = None
    """
    flag = 0
    try:
        kpi2 = create_kpi(creator=None, name="New KPI...", year=2017, refer_to=kpi,
                                 quarter_period=organization.get_current_quarter(),
                                 bsc_category='financial', quarter=1, operator='≥', parent_id=parent_kpi.id)
    except PermissionDenied:
        flag = 1

    assert flag == 1

    """
    Test for services/kpi.py::create_kpi
    Case: Permission Denied
    has_perm denied
    """

    flag = 0
    try:
        kpi2 = create_kpi(creator=userB, name="New KPI...", year=2017, refer_to=kpi,
                          quarter_period=organization.get_current_quarter(), user=userA,
                          bsc_category='financial', quarter=1, operator='≥', parent_id=parent_kpi.id)
    except PermissionDenied:
        flag = 1

    assert flag == 1


@pytest.mark.django_db(transaction=False)
def test_update_year_data_to_kpi_target(kpi):
    year_data = {
        'months_target': {
            'quarter_1': {
                'month_1': 100,
                'month_2': 200,
                'month_3': 300
            },
            'quarter_2': {
                'month_1': 400,
                'month_2': 500,
                'month_3': 600
            },
            'quarter_3': {
                'month_1': 700,
                'month_2': 800,
                'month_3': 900
            },
            'quarter_4': {
                'month_1': 1000,
                'month_2': 1100,
                'month_3': 1200
            }
        }
    }

    kpi = update_year_data_to_kpi_target(kpi, year_data, True)
    key = 'quarter_{}'.format(kpi.quarter_period.quarter)
    assert kpi.month_1_target == year_data['months_target'][key]['month_1']
    assert kpi.month_2_target == year_data['months_target'][key]['month_2']
    assert kpi.month_3_target == year_data['months_target'][key]['month_3']


@pytest.mark.django_db(transaction=False)
def test_update_kpi_target_to_year_data(kpi):
    kpi.month_1_target = 100
    kpi.month_2_target = 200
    kpi.month_3_target = 300
    kpi.save()

    kpi = update_kpi_target_to_year_data(kpi, True)
    key = 'quarter_{}'.format(kpi.quarter_period.quarter)
    assert kpi.month_1_target == kpi.year_data['months_target'][key]['month_1']
    assert kpi.month_2_target == kpi.year_data['months_target'][key]['month_2']
    assert kpi.month_3_target == kpi.year_data['months_target'][key]['month_3']
