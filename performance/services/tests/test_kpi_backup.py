import pytest

from performance.models import KPI, KPIBackup
from performance.services import convert_kpis_tojson
from performance.services.kpi_backup import create_backup_kpis, get_kpibackups, delete_backup_kpis
from performance.services.report import get_scores
from utils.random_str import random_string


@pytest.mark.django_db()
def test_backup_kpis(organization, userA, userA_manager, userAdmin):
    quarter_period = organization.get_current_quarter()
    quarter_period_id = quarter_period.id

    kpi1 = KPI()
    kpi1.name = random_string()
    kpi1.user = userA
    kpi1.save()

    kpi2 = KPI()
    kpi2.name = random_string()
    kpi2.user = userA
    kpi2.save()

    kpis = userA.profile.get_kpis_parent()
    kpis_json = convert_kpis_tojson(kpis)

    assert len(kpis_json) == 0

    kbu = create_backup_kpis(userA_manager, userA, quarter_period_id, 1)
    assert kbu is not None
    assert kbu.data == kpis_json

    assert get_kpibackups(userA_manager, userA, quarter_period_id)[0] == kbu

    '''
    Create duplicated backup
    '''

    kbu = create_backup_kpis(userA_manager, userA, quarter_period_id, 1)
    assert len(get_kpibackups(userA_manager, userA, quarter_period_id)) == 1

    '''
    Create new backup
    '''
    kbu = create_backup_kpis(userA_manager, userA, quarter_period_id, 2)
    assert len(get_kpibackups(userA_manager, userA, quarter_period_id)) == 2

    kbu = create_backup_kpis(userA_manager, userA, quarter_period_id, 3)
    assert len(get_kpibackups(userA_manager, userA, quarter_period_id)) == 3

    '''
    Delete backup
    '''
    delete_backup_kpis(userAdmin, userA, quarter_period, 2)
    assert len(get_kpibackups(userA_manager, userA, quarter_period_id)) == 2
    assert KPIBackup.deleted_objects.count() == 1

    kpi_bu = get_kpibackups(userA_manager, userA, quarter_period_id,month=1)
    assert len(kpi_bu) > 0