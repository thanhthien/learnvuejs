import pytest

from performance.models import KpiComment

from performance.services.kpi_comment import *


@pytest.mark.django_db(transaction=False)
def test_group_comment_by_kpi(kpi, userA):
    kpi.user = userA
    kpi.save()
    kpi_comment = KpiComment()
    kpi_comment.kpi_unique_key = kpi.unique_key
    kpi_comment.user = userA
    kpi_comment.kpi = kpi
    kpi_comment.content = 'TEST'
    kpi_comment.save()

    assert group_comment_by_kpi({kpi_comment})[kpi.id] == '\r\n TEST'
    kpi_comment.kpi = None
    kpi_comment.content = 'TEST'
    kpi_comment.save()
    assert group_comment_by_kpi({kpi_comment})[None] == '\r\n TEST'


@pytest.mark.django_db(transaction=False)
def test_group_comment_by_kpi_name(kpi, userA):
    kpi.user = userA
    kpi.save()
    kpi_comment = KpiComment()
    kpi_comment.kpi_unique_key = kpi.unique_key
    kpi_comment.user = userA
    kpi_comment.kpi = kpi
    kpi_comment.content = 'TEST'
    kpi_comment.save()

    assert group_comment_by_kpi_name({kpi_comment})[kpi.name][0]['content'] == 'TEST'

    kpi_comment.kpi_unique_key = 'TEST'
    kpi_comment.save()
    assert group_comment_by_kpi_name({kpi_comment})[''][0]['content'] == 'TEST'

@pytest.mark.django_db(transaction=False)
def test_get_kpi_comments_foruser(kpi, userA, organization):
    user = get_kpi_comments_foruser(None)
    assert user == []

    query = get_kpi_comments_forquarter(organization.get_current_quarter())
    assert len(query) > 0

