import pytest

from performance.services.kvstore import *
from utils import random_string


@pytest.mark.django_db(transaction=False)
def test_kvstore(organization, userA):
    key = random_string()
    value = random_string()
    k = create_kvstore(   key,  {'test': value}, userA, userA )

    assert k.id == get_kvstore(key, userA).id
    assert get_kvstore(key, userA).data['test'] == value

    value2 = random_string()

    k = update_kvstore(k, {'test': value2})

    assert get_kvstore(key, userA).data['test'] == value2