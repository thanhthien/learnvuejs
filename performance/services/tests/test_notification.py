import time

import pytest
from datetime import datetime, timedelta
from django.contrib.contenttypes.models import ContentType
from monthdelta import monthdelta

from performance.services.notification import *

from performance.services.notification import mark_read_all
from notifications.models import Notification

from utils import random_string


@pytest.mark.django_db(transaction=False)
def test_get_all_unread():
    get_all_unread()

@pytest.mark.django_db(transaction=False)
def test_mark_read_all(ceo, userB, userA, organization):
    assert mark_read_all(ceo) == True
    # ntf = Notification()
    # ntf.LEVELS = 'success'
    # ntf.unread = True
    # ntf.timestamp__gt = datetime.now() + timedelta(days=-1)
    # ntf.actor = userA
    # ntf.recipient = ceo
    # ntf.save()
    ntf = send_notification(actor=userA, user=ceo, recipient=ceo, description='test1', verb='', type=SYSTEM_LOG_TYPE)
    assert mark_read_all(ceo) == True
    all_unread_by_user = Notification.objects.filter(recipient=ceo, unread=True).order_by('-id')[
               :20]

    assert all_unread_by_user is not None

@pytest.mark.django_db(transaction=True)
def test_send_notification(userA_manager, userA, organization):
    sent = send_notification(actor=userA_manager, user=userA, recipient=userA,
                      description='test1', verb='',
                      type=USER_LOG_TYPE),
    assert sent is not None
    assert len(sent) == 1

    """
    Case: actor is None
    """
    sent = send_notification(actor=None, user=userA, recipient=userA,
                             description='test1', verb='',
                             type=USER_LOG_TYPE),
    assert sent is not None
    assert len(sent) == 1


@pytest.mark.django_db(transaction=False)
def test_get_all_unread_by_user(userA):
    assert len(get_all_unread_by_user(userA)) == 0

@pytest.mark.django_db(transaction=False)
def test_get_notifications_by_user_id(userA, ceo):
    ntf = notify.send(ceo, recipient=userA,
                      verb=ugettext(''),
                      target=None,
                      description=random_string(100),
                      type=CHANGE_LOG_TYPE,
                      actor_name=ceo.profile.display_name,
                      version='',
                      release_date='',
                      language='en'
                      )

    assert len(get_all_unread_by_user_id(ceo, userA.id)) == 1


@pytest.mark.django_db(transaction=False)
def test_mark_read(userA, organization):
    content = ContentType()
    content.save()

    ntf = Notification()
    ntf.recipient = userA
    ntf.unread = True
    ntf.actor_content_type = content
    ntf.save()

    assert mark_read(userA, ntf.id) == True
    assert mark_read(userA, None) == False


@pytest.mark.django_db(transaction=False)
def test_notify_complete_review(userA, organization):
    assert notify_complete_review(userA, userA) == True
    assert notify_complete_review(userA, None) ==  False


@pytest.mark.django_db(transaction=False)
def test_notification_to_list(userA, ceo, userB, organization):
    ntf = notify.send(ceo, recipient=userA,
                       verb=ugettext(''),
                       target=None,
                       description=random_string(100),
                       type=SYSTEM_LOG_TYPE,
                       actor_name=ceo.profile.display_name,
                       version='',
                       release_date='',
                       language=''
                       )

    ns = Notification.objects.filter(recipient=userA).order_by('-id')[:20]
    result = notification_to_list(ns, organization)
    assert result != []


@pytest.mark.django_db(transaction=False)
def test_mark_notifications_as_read(userA, ceo, organization):
    """
    Case: Permission Denied
    """
    ntf = notify.send(ceo, recipient=userA,
                      verb=ugettext(''),
                      target=None,
                      description=random_string(100),
                      type=CHANGE_LOG_TYPE,
                      actor_name=ceo.profile.display_name,
                      version='',
                      release_date='',
                      language='en'
                      )
    flag = 0
    try:
        mark_notifications_as_read(ceo, userA)
    except:
        flag = 1

    assert flag == 1
