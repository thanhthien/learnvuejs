import pytest

from company.models import Position
from performance.services.position_kpi import create_position_kpi,  \
     get_all_position_kpi, update_kpi_position, delete_kpi_position, add_kpi_to_position


@pytest.mark.django_db(transaction=False)
def test_create_position_kpi(organization):
    position = Position(organization=organization, name='CEO')
    position.save()

    # not param position_id and backup_type
    pos_kpi = create_position_kpi(None, None)
    assert pos_kpi is None

    #
    pos_kpi = create_position_kpi(position_id=position.id, backup_type='kpilib')
    assert pos_kpi.position == position
    assert pos_kpi.backup_type == 'kpilib'
    assert pos_kpi.kpi_json_data['kpis'] == []

    pos_kpi = create_position_kpi(position_id=position.id, backup_type='performanceresult')
    assert pos_kpi.position == position
    assert pos_kpi.backup_type == 'performanceresult'

    pos_kpi = create_position_kpi(position_id=position.id, backup_type=None)
    assert pos_kpi is None

    pos_kpi = create_position_kpi(position_id=None, backup_type='kpilib')
    assert pos_kpi is None


@pytest.mark.django_db(transaction=False)
def test_add_kpi_to_postion(organization):
    position = Position(organization=organization, name='CEO')
    position.save()

    pos_kpi = create_position_kpi(position_id=position.id, backup_type='kpilib')
    assert pos_kpi.position == position
    assert pos_kpi.backup_type == 'kpilib'

    # 1. add kpi

    kpi1 = {
        'kpilib_unique_id': 'kpilib1',
        'name': 'kpi 1',
        'group_name': 'group 1',
        'target': 10,
        'real': 10,
        'result': 50,
        'refer_relationship': [
            {
                'type': 'direct',
                'position_id': position.id,
                'kpilib_unique_id': 'kpilib1'
            }
        ]
    }
    kpi2 = {
        'kpilib_unique_id': 'kpilib2',
        'name': 'kpi 2',
        'group_name': 'group 2',
        'target': 10,
        'real': 10,
        'result': 50,
        'refer_relationship': [
            {
                'type': 'direct',
                'position_id': position.id,
                'kpilib_unique_id': 'kpilib2'
            }
        ]
    }

    kpi_pos = add_kpi_to_position(position_kpi_id=None, kpi=kpi1)
    assert kpi_pos is None

    kpi_pos = add_kpi_to_position(pos_kpi.id, kpi1)
    assert len(kpi_pos.kpi_json_data['kpis']) == 1
    _kpi_pos = kpi_pos.kpi_json_data['kpis'][0]
    assert _kpi_pos['kpilib_unique_id'] == 'kpilib1'
    assert _kpi_pos['name'] == 'kpi 1'

    kpi_pos = add_kpi_to_position(pos_kpi.id, kpi2)
    assert len(kpi_pos.kpi_json_data['kpis']) == 2

    # 2 update kpi in kpis

    list_kpis = [
        {
            'kpilib_unique_id': 'kpilib1',
            'name': 'kpi 1',
            'group_name': 'group 1',
            'target': 20,
            'real': 20,
            'result': 80,
            'refer_relationship': [
                {
                    'type': 'direct',
                    'position_id': position.id,
                    'kpilib_unique_id': 'kpilib1'
                }
            ]
        },
        {
            'kpilib_unique_id': 'kpilib2',
            'name': 'kpi 2',
            'group_name': 'group 2',
            'target': 30,
            'real': 15,
            'result': 70,
            'refer_relationship': [
                {
                    'type': 'direct',
                    'position_id': position.id,
                    'kpilib_unique_id': 'kpilib2'
                }
            ]
        }
    ]
    # 2.1 position_kpi_id is None

    kpi_pos = update_kpi_position(position_kpi_id=None, kpis=list_kpis)
    assert kpi_pos is None

    # 2.2 update target, real, result

    kpi_pos = update_kpi_position(position_kpi_id=pos_kpi.id, kpis=list_kpis)
    assert len(kpi_pos.kpi_json_data['kpis']) == 2
    _kpi_pos = kpi_pos.kpi_json_data['kpis'][0]
    assert _kpi_pos['kpilib_unique_id'] == 'kpilib1'
    assert _kpi_pos['name'] == 'kpi 1'
    assert _kpi_pos['target'] == list_kpis[0]['target']
    assert _kpi_pos['real'] == list_kpis[0]['real']
    assert _kpi_pos['result'] == list_kpis[0]['result']

    # 3 delete kpi in kpis

    # 3.1 position_kpi_id is None

    kpi_pos = delete_kpi_position(position_kpi_id=None, kpilib_unique_id='kpilib1')
    assert kpi_pos is None

    # 3.2 delete kpi 'kpi 1'

    kpi_pos = delete_kpi_position(position_kpi_id=pos_kpi.id, kpilib_unique_id='kpilib1')
    assert len(kpi_pos.kpi_json_data['kpis']) == 1
    _kpi_pos = kpi_pos.kpi_json_data['kpis'][0]
    assert _kpi_pos['kpilib_unique_id'] == 'kpilib2'
    assert _kpi_pos['name'] == 'kpi 2'


@pytest.mark.django_db(transaction=False)
def test_get_all_position_kpi(organization):
    position = Position(organization=organization, name='CEO')
    position.save()

    pos_kpi = create_position_kpi(position_id=position.id, backup_type='kpilib')
    assert pos_kpi.position == position
    assert pos_kpi.backup_type == 'kpilib'

    pos_kpi = create_position_kpi(position_id=position.id, backup_type='performanceresult')
    assert pos_kpi.position == position
    assert pos_kpi.backup_type == 'performanceresult'

    kpis = get_all_position_kpi(position_id=None, backup_type=None)
    assert len(kpis) == 0

    kpis = get_all_position_kpi(position_id=position.id, backup_type='all')

    assert len(kpis) == 2
    assert kpis[0].backup_type == 'kpilib'
    assert kpis[1].backup_type == 'performanceresult'

    kpis = get_all_position_kpi(position_id=position.id, backup_type='kpilib')
    assert len(kpis) == 1
    assert kpis[0].backup_type == 'kpilib'

    kpis = get_all_position_kpi(position_id=position.id, backup_type='performanceresult')
    assert len(kpis) == 1
    assert kpis[0].backup_type == 'performanceresult'

    kpis = get_all_position_kpi(position_id=position.id, backup_type='')
    assert len(kpis) == 0