from performance.models import QuarterPeriod
from performance.services.quarter import *
from performance.services.timeline import *
import pytest

from performance.services.quarter import QuarterServices


@pytest.mark.django_db(transaction=False)
def test_get_last_quarter(organization, userA, ceo):

    '''
    Only one quarter --> none
    '''
    assert get_last_quarter(userA, organization) == None

    '''
    Create new quarter
    '''
    first_quarter = organization.get_current_quarter()

    second_quarter = QuarterPeriod.objects.create(organization=organization,
                                     due_date=datetime.now().date(),
                                     clone_from_quarter_period=first_quarter)

    second_quarter.status = 'IN'
    second_quarter.save()

    assert get_last_quarter(userA, organization) == first_quarter

    '''
    Create one more quarter
    '''
    third_quarter = QuarterPeriod.objects.create(organization=organization,
                                     due_date=datetime.now().date(),
                                     clone_from_quarter_period=second_quarter)

    quarter = delete_quarter(userA,second_quarter,organization)
    assert quarter == None
    delete_quarter(ceo,second_quarter,organization)
    # print second_quarter.status
    assert get_last_quarter(userA, organization) == None # Last quarter has been deleted



@pytest.mark.django_db(transaction=False)
def test_quarter_services(organization, userA, ceo):
    quarter_start =  QuarterServices.get_quarter_start(organization)
    assert quarter_start[0] == '1'


@pytest.mark.django_db(transaction=False)
def test_get_quarter_by_org(organization, userA, ceo):
    get_quarter_by_org(userA, organization.get_current_quarter().id, organization)

@pytest.mark.django_db(transaction=False)
def test_get_all_quarter_by_org(organization, userA, ceo):
    get_all_quarter_by_org(userA, organization)

@pytest.mark.django_db(transaction=False)
def test_restore_quarter(organization, userA, userAdmin):
    quarter = restore_quarter(userA, organization.get_current_quarter(), organization)
    assert quarter == None

    quarter = restore_quarter(userAdmin, organization.get_current_quarter(), organization)
    assert quarter.status == 'AR'

@pytest.mark.django_db(transaction=False)
def test_reactivate_quarter(organization, userA, userAdmin):
    quarter = reactivate_quarter(userA, organization.get_current_quarter(), organization)
    assert quarter == None

    quarter = reactivate_quarter(userAdmin, organization.get_current_quarter(), organization)
    assert quarter.status == 'IN'






