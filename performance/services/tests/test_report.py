import pytest
from django.contrib.auth.models import User

from performance.services.report import *
from datetime import datetime

from performance.services.report_query import *

from performance.models import KPI


@pytest.mark.django_db(transaction=False)
def test_get_scores(organization, userA, userB, userA_manager):
    quarter_period = organization.get_current_quarter()
    scores = get_scores(userA, organization, quarter_period, userA.id)

    assert len(scores) == 1
    assert scores[0]['name'] in userA.profile.display_name


@pytest.mark.django_db(transaction=False)
def test_get_score_individual(organization, userA, userB, userA_manager):
    quarter_period = organization.get_current_quarter()

    # test user with organization == None
    user_test = User()
    user_test.save()
    # Khong co organization nen bo qua count_delay_and_not_evaluated_kpi_user(user)
    assert len(get_score_individual(quarter_period, user_test.profile,organization, with_exscore=True)) == 23
    assert get_score_individual(quarter_period, user_test.profile,organization, with_exscore=True)['quarter_score'] == 0

    scores = get_score_individual(quarter_period, userA.profile, organization, with_exscore=True)
    assert scores['name'] == userA.profile.display_name
    assert scores['email'] == userA.email


@pytest.mark.django_db(transaction=False)
def test_count_delay_and_not_evaluated_kpi_user(organization, userA, userB, userA_manager):

    k1 = KPI()
    k1.user = userA
    k1.quarter_period = organization.get_current_quarter()
    k1.month_2_target = 10
    k1.month_2 = 10
    k1.save()

    k2 = KPI()
    k2.user = userA
    k2.quarter_period = organization.get_current_quarter()
    k2.parent = k1
    k2.weight = 0
    k2.save()

    k3 = KPI()
    k3.user = userA
    k3.quarter_period = organization.get_current_quarter()
    k3.weight = 0
    k3.save()
    count_parent, count_child = count_delay_and_not_evaluated_kpi_user(userA)

    assert count_parent['sum_delay_kpi_parent'] == 1
    assert count_child['sum_delay_kpi_child'] == 1
    assert count_child['month_2'] == 1


@pytest.mark.django_db(transaction=False)
def test_report_query(organization, userA, userB, userA_manager):
    date = datetime.today()
    # add new query
    rq = add_new_report_query(organization, userA, 'TEST QR', date, 'TEST')
    assert rq.id > 0

    rq_check = add_new_report_query(organization, userA, 'TEST QR', date, 'TEST')
    assert rq_check == None

    # test change_query
    rq2 = change_query(userA, 'TEST', rq.id)
    assert rq2.id > 0

    # get query by id
    rq3 = get_report_query_by_id(userA, rq2.id)
    assert rq3.id == rq2.id

    rq3 = get_report_query_by_id(userB, rq2.id)
    assert rq3.id == rq2.id

    rq3 = get_report_query_by_id(None, None)
    assert rq3 == None

    # get query by user
    rq_user = get_report_query_by_user(userA)
    assert len(rq_user) > 0

    rq_user = get_report_query_by_user(None)
    assert rq_user == None

    # delete query by id
    rq_delete = delete_report_query_by_id(userA, rq.id)
    assert rq_delete['delete'] == True

    rq_delete = delete_report_query_by_id(userA, None)
    assert rq_delete == None

    # delete query by user
    rq_delete = delete_report_query_by_user(userA)
    assert rq_delete == None

    rq = add_new_report_query(organization, userA, 'TEST QR', date, 'TEST')
    assert rq.id > 0

    rq_delete = delete_report_query_by_user(userA)
    assert rq_delete == []

    # delete all query
    rq_delete_all = delete_report_query_all()
    assert rq_delete_all == []
