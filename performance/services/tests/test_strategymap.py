import pytest
from django.forms import DateTimeField
from django.http import Http404
from django.utils.datetime_safe import datetime

from performance.services.strategymap import *
from django.core.cache import cache
from performance.models import get_group_name
from utils import reload_model_obj, random_string

from performance.services import *

from performance.db_models.strategy_map import StrategyMap

__author__ = 'hongleviet'


@pytest.mark.django_db(transaction=False)
def test_get_kpigroup(organization):
    category = 'financial'
    current_quarter = organization.get_current_quarter()
    smap, use_hashurl_type = get_map(organization, current_quarter)

    group = create_group_kpi(organization.id, smap.id, category, 'Hello World')
    assert get_kpigroup(group.id) == group


@pytest.mark.django_db(transaction=False)
def test_get_kpis_by_group(userA, kpi, ceo, userB, organization):

    current_quarter = organization.get_current_quarter()

    strategymap = StrategyMap(
        organization=organization,
        year=2017,
        director=ceo,
        name=random_string(),
        description=random_string(),
        quarter_period=ceo.profile.get_current_quarter(),
        created_at=DateTimeField()
    )
    strategymap.save()
    group_kpi = GroupKPI(
        map=strategymap,
        name=random_string()
    )
    group_kpi.save()

    # kpi.groupkpi_set.add(group_kpi)
    kpi.group_kpi = group_kpi
    kpi.save()
    kpis = get_kpis_by_group(group_kpi, userA, include_children=True)
    assert len(kpis) == 1
    flag = 0
    pB = userB.profile
    pB.organization = None
    pB.save()
    try:
        kpis = get_kpis_by_group(group_kpi, userB, include_children=True)
    except PermissionDenied:
        flag = 1
    assert flag == 1


@pytest.mark.django_db(transaction=False)
def test_get_kpis_by_group_v2(userA, userB, organization):
    category = 'financial'
    current_quarter = organization.get_current_quarter()
    smap, use_hashurl_type = get_map(organization, current_quarter)

    group = create_group_kpi(organization.id, smap.id, category, 'Hello World')

    kpis = get_kpis_by_group_v2(group, userA, include_children=True)
    assert len(kpis) == 0
    flag = 0
    pB = userB.profile
    pB.organization = None
    pB.save()
    try:
        kpis = get_kpis_by_group_v2(group, userB, include_children=True)
    except PermissionDenied:
        flag = 1
    assert flag == 1


@pytest.mark.django_db(transaction=False)
def test_get_kpis_bsc_category(userA, organization):
    current_quarter = organization.get_current_quarter()
    smap, use_hashurl_type = get_map(organization, current_quarter)

    kpis = get_kpis_bsc_category(smap, include_kpi=True)
    assert len(kpis) == 4

    kpis = get_kpis_bsc_category(smap, include_kpi=True, bsc_category='customer')
    assert len(kpis) == 4

    kpis = get_kpis_bsc_category(smap, include_kpi=True, bsc_category='internal')
    assert len(kpis) == 4

    kpis = get_kpis_bsc_category(smap, include_kpi=True, bsc_category='learninggrowth')
    assert len(kpis) == 4


@pytest.mark.django_db(transaction=False)
def test_set_group(organization, ceo, userA, parent_kpi, kpi):
    current_quarter = organization.get_current_quarter()
    smap, use_hashurl_type = get_map(organization, current_quarter)
    assert smap.id > 0

    assert get_map(organization, current_quarter, hash=smap.hash)[0].id == smap.id

    smp2, use_hashurl_type = get_map(organization, current_quarter, user_id=userA.id)
    assert smp2.id > 0
    assert smp2.hash != smap.hash

    assert get_map(organization, current_quarter)[0].id == smap.id
    rdis_key = 'get_group_name{0}_{1}'.format(parent_kpi.id, parent_kpi.id)

    '''
    Create group KPI
    '''
    category = 'financial'

    group = create_group_kpi(organization.id, smap.id, category, 'Hello World')
    assert group.id > 0

    StrategyMapServices(action=KPI__EDITING, actor=ceo, map=smp2).set_group(parent_kpi=parent_kpi, group=group, root_kpi_id=parent_kpi.id)
    # time.sleep(5)

    assert parent_kpi.refer_group_name == group.name

    assert cache.get(rdis_key) == None


@pytest.mark.django_db(transaction=False)
def test_strategymapservices_create(organization, ceo, userA, parent_kpi, userC, kpi):
    """
    Test for services/strategymap.py::StrategyMapServices().create()
    """

    current_quarter = organization.get_current_quarter()

    smap, use_hashurl_type = get_map(organization, current_quarter)
    map1 = StrategyMapServices(action=KPI__EDITING, actor=ceo, map=smap).create(organization.id, current_quarter.id)
    assert map1.id > 0
    assert map1.organization == organization

    """
    Case: Permission Denied for actor
    """
    org = Organization()
    org.user = userC
    org.save()
    flag = 0
    try:
        group = StrategyMapServices(actor=userC, map=smap).create(organization.id, current_quarter.id)
    except PermissionDenied:
        flag = 1
    assert flag == 1

    qp = org.get_current_quarter()
    assert qp is not None

    """
    Case: Permission Denied as organization and quarter period don't match
    """
    flag = 0
    try:
        group = StrategyMapServices.create(org.id, current_quarter.id)
    except:
        flag = 1
    assert flag == 1

    """
    Case: smap doesn't exist, so it will be created in create()
    """
    map2 = StrategyMapServices.create(org.id, qp.id)
    assert map2.id > 0
    assert map2 in StrategyMap.objects.filter(organization=org.id)
    assert map2.organization == org


@pytest.mark.django_db(transaction=False)
def test_delete_kpi(organization, ceo, userA, userB, kpi):
    current_quarter = organization.get_current_quarter()
    smap, use_hashurl_type = get_map(organization, current_quarter)

    try:
        kpi_check = StrategyMapServices(action=KPI__EDITING, actor=ceo, map=smap).delete_kpi(organization.id, kpi.id, userB)
    except Exception:
        kpi_check = False
    assert kpi_check == False

    kpi_check = StrategyMapServices(action=KPI__EDITING, actor=ceo, map=smap).delete_kpi(organization.id, kpi.id, ceo)
    assert kpi_check is True


@pytest.mark.django_db(transaction=False)
def test_create_kpi(organization, ceo, userA, kpi):
    current_quarter = organization.get_current_quarter()
    smap, use_hashurl_type = get_map(organization, current_quarter)
    kpi_check = StrategyMapServices(action=KPI__EDITING, actor=ceo, map=smap).create_kpi(organization.id, smap.id, 'financial', ceo)
    assert kpi_check == get_kpi(kpi_check.id, ceo)

   # smap.director = ceo
    smap.save()
    kpi_check = StrategyMapServices(action=KPI__EDITING, actor=ceo, map=smap).create_kpi(organization.id, smap.id,
                                                                                         'financial', ceo)
    assert kpi_check == get_kpi(kpi_check.id, ceo)


@pytest.mark.django_db(transaction=False)
def test_delete_group_kpi(organization, ceo, parent_kpi, userA, kpi):
    current_quarter = organization.get_current_quarter()
    smap, use_hashurl_type = get_map(organization, current_quarter)

    category = 'financial'

    group = create_group_kpi(organization.id, smap.id, category, 'Hello World')

    kpi_check = StrategyMapServices(action=KPI__EDITING, actor=ceo, map=smap).delete_group_kpi(organization.id, group.id, ceo)
    assert kpi_check == False

    group = create_group_kpi(organization.id, smap.id, category, 'Hello World')
    StrategyMapServices.update_group_kpi(organization.id, parent_kpi.id, group.id, None,
                                         ceo)
    '''
    test case: can not delete group when there is KPI attached into it
    '''
    kpi.group_kpi = group
    kpi.save()

    try:
        kpi_check = StrategyMapServices(action=KPI__EDITING, actor=ceo, map=smap).delete_group_kpi(organization.id, group.id, ceo)

        assert False#if reach here --> something wrong
    except PermissionDenied:
        assert True

    '''
        test case: can  delete group when there is no KPI attached into it
    '''

    kpi.delete()
    kpi_check = StrategyMapServices(action=KPI__EDITING, actor=ceo, map=smap).delete_group_kpi(organization.id,
                                                                                               group.id, ceo)
    assert GroupKPI.objects.filter(id = group.id).count() == 0
   # assert kpi_check == True



    kpi_check = StrategyMapServices(action=KPI__EDITING, actor=ceo, map=smap).delete_group_kpi(organization.id, 0, ceo)
    assert kpi_check == False


@pytest.mark.django_db(transaction=False)
def test_groupkpi_to_json(organization, ceo, userA, parent_kpi, kpi):
    current_quarter = organization.get_current_quarter()
    smap, use_hashurl_type = get_map(organization, current_quarter)
    assert smap.id > 0

    assert get_map(organization, current_quarter, hash=smap.hash)[0].id == smap.id

    smp2, use_hashurl_type = get_map(organization, current_quarter, user_id=userA.id)
    assert smp2.id > 0
    assert smp2.hash != smap.hash

    assert get_map(organization, current_quarter)[0].id == smap.id
    rdis_key = 'get_group_name{0}_{1}'.format(parent_kpi.id, parent_kpi.id)

    '''
    Create group KPI
    '''
    category = 'financial'

    group = create_group_kpi(organization.id, smap.id, category, 'Hello World')
    assert group.id > 0

    StrategyMapServices.update_group_kpi(organization.id, parent_kpi.id, group.id, None,
                                         ceo)
    # time.sleep(5)

    assert group.get_kpis().count() > 0

    json_data = group.to_json(include_kpi=False)['kpis']

    assert len(json_data) == 0

    json_data_2 = group.to_json(include_kpi=True)['kpis']

    assert len(json_data_2) == 1

    # assert parent_kpi.refer_group_name == group.name

    '''
    Create group KPI
    '''
    category = 'customer'

    group_2 = create_group_kpi(organization.id, smap.id, category, 'Hello World 2')
    assert group_2.id > 0

    StrategyMapServices.update_group_kpi(organization.id, parent_kpi.id, group_2.id, None,
                                         ceo, new_bsc_category=category)

    assert group_2.get_kpis().count() > 0

    assert cache.get('get_group{}'.format(parent_kpi.id)) is None
    assert cache.get("get_group_name{0}_{1}".format(parent_kpi.id, None)) is None

    new_group_name = "Update group"
    StrategyMapServices.update_group(organization.id, group_2.id, smap.id, category, new_group_name, ceo)

    group_2 = reload_model_obj(group_2)
    parent_kpi = reload_model_obj(parent_kpi)
    assert group_2.name == new_group_name
    assert get_group_name(parent_kpi) == new_group_name

    json_data = group.to_json(include_kpi=False)['kpis']

    assert len(json_data) == 0

    json_data_2 = group_2.to_json(include_kpi=True)['kpis']

    assert len(json_data_2) == 1
    # assert parent_kpi.refer_group_name == group_2.name

    StrategyMapServices.update_group_kpi(organization.id, parent_kpi.id, group_2.id, group.id,
                                         ceo)
    assert group_2.get_kpis().count() > 0


@pytest.mark.django_db(transaction=False)
def test_get_user_current_map(userA, ceo, organization, userB):
    """
    Case 1: map is not None
    """
    current_quarter = organization.get_current_quarter()
    smap3 = StrategyMap()
    smap3.organization = organization
    smap3.director = userA
    smap3.name = "TEST"
    smap3.quarter_period = current_quarter
    smap3.save()
    result = get_user_current_map(userA)
    assert result == smap3

    """
    Case 2: orgnanization is None
    """
    pA = userA.profile
    pA.organization = None
    pA.save()
    result = get_user_current_map(userA)
    assert result is None


@pytest.mark.django_db(transaction=False)
def test_check_group_perm(userA, kpi, organization, userC, ceo):
    qp = organization.get_current_quarter()
    map1 = StrategyMapServices.create(organization.id, qp.id)
    assert map1 in StrategyMap.objects.filter(organization=organization)
    group_kpi = GroupKPI(
        map=map1,
        name=random_string(),
        category='financial'
    )
    flag = 0
    try:
        result = check_group_perm(group_kpi, userC)
    except PermissionDenied:
        flag = 1

    assert flag == 1

    map1.director = ceo
    map1.save()
    flag = 0
    try:
        result = check_group_perm(group_kpi, userA)
    except PermissionDenied:
        flag = 1

    assert flag == 1


@pytest.mark.django_db(transaction=False)
def test_get_kpis_group(organization, userA):
    current_quarter = organization.get_current_quarter()
    smap, use_hashurl_type = get_map(organization, current_quarter)

    assert len(get_kpis_group(smap)) == 4

    kpis = get_kpis_group(smap, include_kpi=True, bsc_category='financial')
    assert len(kpis) == 4

    kpis = get_kpis_group(smap, include_kpi=True, bsc_category='customer')
    assert len(kpis) == 4

    kpis = get_kpis_group(smap, include_kpi=True, bsc_category='internal')
    assert len(kpis) == 4

    kpis = get_kpis_group(smap, include_kpi=True, bsc_category='learninggrowth')
    assert len(kpis) == 4

    smap.quarter_period = None
    kpis = get_kpis_group(smap)
    assert len(kpis) == 4

    smap = None
    kpis = get_kpis_group(smap, include_kpi=True, bsc_category='financial')
    assert len(kpis) == 4
    for index in kpis:
        assert index == []


@pytest.mark.django_db(transaction=False)
def test_get_map(organization, userA):
    current_quarter = organization.get_current_quarter()
    smap, use_hashurl_type = get_map(organization, current_quarter)
    assert smap.id > 0

    assert get_map(organization, current_quarter, hash=smap.hash)[0].id == smap.id

    assert get_map(organization, current_quarter)[0].id == smap.id

    assert get_map(organization, current_quarter, user_map_hash=smap.hash, user_id=userA.id)[0] == smap



    smp2, use_hashurl_type = get_map(organization, current_quarter, user_id=userA.id)
    assert smp2.id is not None
    assert smp2.hash != smap.hash

    org = Organization()
    org.user = userA
    org.save()
    flag = 0
    try:
        result = get_map(org, org.get_current_quarter(), hash=org.hash)
    except:
        flag = 1
    assert flag == 1

    flag = 0
    try:
        result = get_map(org, org.get_current_quarter(), user_map_hash=userA.profile.get_hash())
    except:
        flag = 1
    assert flag == 1

    # #### create duplicate and try get_map
    #
    # duplicated_map = StrategyMap(organization=organization,
    #                              quarter_period=current_quarter,
    #                              )
    # duplicated_map.save()
    #
    #
    # duplicated_map2 = StrategyMap(organization=organization,
    #                              quarter_period=current_quarter,
    #                              )
    # duplicated_map2.save()
    #
    # assert get_map(organization, current_quarter)[0].id == duplicated_map.id
 #   assert StrategyMap.objects.filter(organization=organization,quarter_period=current_quarter).count() == 1




@pytest.mark.django_db(transaction=False)
def test_create_query_group(organization, userA):
    current_quarter = organization.get_current_quarter()
    smap, use_hashurl_type = get_map(organization, current_quarter)
    category = 'financial'

    '''
    Test if create group successful
    '''

    group = create_group_kpi(organization.id, smap.id, category)
    assert group.id > 0

    '''
    Test if we can query back the group have just created
    '''
    groups = get_group_by_map_and_category(smap, category)

    indexes = [i for i, x in enumerate(groups) if x == group]
    assert indexes > 0

