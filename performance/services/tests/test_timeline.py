from performance.services.timeline import *
import pytest


#from faker import Factory


#faker = Factory.create()


@pytest.mark.django_db(transaction=False)
def test_get_timeline(organization):
    get_timeline(organization)

@pytest.mark.django_db(transaction=False)
def test_get_TaskEveryStage(organization):
    get_TaskEveryStage(organization)

@pytest.mark.django_db(transaction=False)
def test_notify_late_timeline(organization):
    notify_late_timeline()

@pytest.mark.django_db(transaction=False)
def test_get_last_task(organization):
    get_last_task(organization)

@pytest.mark.django_db(transaction=False)
def test_get_last_stage(organization):
    get_last_stage(organization)