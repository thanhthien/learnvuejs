from random import randint

import pytest
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied

from performance.services.user_organization import toggle_active

from performance.models import KPI

from    performance.services.user_change_log import log_change_user_email


@pytest.mark.django_db(transaction=False)
def test_toggle_active(ceo, userA, organization):
    active_user = toggle_active(True, userA.id, ceo)
    assert active_user != None

    kpi = KPI()
    kpi.user= userA
    kpi.quarter_period = organization.get_current_quarter()
    kpi.save()
    """
    Case 1: user_id given, but user doesn't exist
    """
    fake_id = randint(1,1000)
    checklist = User.objects.filter().all()
    for index in checklist:
        if fake_id != index.id:
            flag = 0
            try:
                active_user = toggle_active(False, user_id=fake_id, actor=ceo)
            except:
                flag = 1
            assert flag == 1
            break
    """
    Case 2: user_id is None
    """
    flag = 0
    try:
        active_user = toggle_active(False, user_id=None, actor=ceo)
    except:
        flag = 1
    assert flag == 1

    profile = userA.profile
    profile.active = False
    profile.save()
    assert userA.profile.active is False

    """
    Case 3: Success
    """
    toggle_active(True, userA.id, ceo)
    profile = userA.profile
    assert profile.active is True


@pytest.mark.django_db(transaction=False)
def test_log_change_user_email(userA, ceo, organization):

    uc = log_change_user_email(userA, userA, '', '', organization.get_current_quarter())
    assert uc.user == userA
    assert uc.actor == userA