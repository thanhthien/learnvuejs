# -*- coding: utf-8 -*-
from datetime import datetime

from company.models import Organization
from django.conf import settings
from performance.db_models.timeline import TaskEveryStage, Timeline

from utils import notify_slack

__author__ = 'hongleviet'
def notify_late_timeline():

    date = datetime.today()

    if (date.day % 31 == 0):
        message = u"@channel: Khác với việc bán sản phẩm, tính năng chạy được hay không chạy được, ra kết quả đúng hay không đúng, mọi thứ rất rõ ràng. " \
                   u"\r\nKhi bán dịch vụ & tư vấn, chúng ta rất dễ phải nhận thêm requests của khách hàng, bên cạnh đó, dù cho chúng ta có hỗ trợ " \
                   u"dành thời gian như thế nào đi nữa, bao nhiêu meeting đi nữa, thì khách hàng vẫn luôn muốn thêm giờ training, thêm thời gian, thêm dịch vụ..." \
                   u"\r\n Nguy hiểm hơn nữa, sẽ rất dễ rơi vào trường hợp chúng ta support rất nhiều, tốn rất nhiều thời gian nhưng không ghi nhận lại rõ ràng, đến lúc nghiệm thu khách hàng" \
                   u"sẽ tiếp tục demand hoặc complaint là chúng ta chưa làm hoặc làm không đủ mà nếu chúng ta không ghi nhận vào timeline thì chúng ta không có cách hiệu quả để đưa ra evident" \
                   u"\r\n Thế nên việc cập nhật và bám sát timeline phải cực kỳ cẩn thận, tỉ mĩ, tránh sai sót , thiếu , cẩu thả qua loa hay gửi email không kèm timeline, meeting minutes không ghi vào timeline" \
                   u"\r\n Chúng ta không cẩn thận ở việc bám sát và ghi nhận timeline thì dù chúng ta có làm tốt hay không tốt, đến lúc nghiệm thu khách hàng sẽ luôn complain và demand thêm , lúc đó sẽ vất vả hơn rất nhiều " \
                   u"\r\n Tóm lại, việc cập nhật timeline là vô cùng thiết yếu, mọi email cho khách hàng đều đính kèm timeline và tiến độ, chúng ta làm càng kỹ thì càng khỏe, càng sơ sài cẩu thả thì đến lúc nghiệm thu càng vất vả và mệt mõi." \
                   u"" \
                   u"\r\n\r\n Thế nên tuyệt đối cẩu thả ở điểm này, không bao giờ làm thêm việc mà không ghi nhận vào timeline, không bao giờ gửi minutes mà không chèn minutes vào timeline, không bao giờ làm thêm cho khách hàng mà không ghi vào timeline.  "

        notify_slack(settings.SLACK_TIMELINE_CHANNEL, message)

    tasks = TaskEveryStage.objects.filter(organization__demo_account = False)

    #message = u"@channel: những công ty bị trễ deadline  \r\n\r\n"
    #notify_slack(settings.SLACK_DEPLOY_CHANNEL, message)
    orgs = {}
    for t in tasks:
        if t.finished_percentage() < 100 and t.remain_days() == 0 \
                and (
                        (t.end_date is None and t.due_date <= date.date() )
                        or
                        (t.end_date is not None and t.end_date <= date.date())
                ):

            org = t.organization


            orgs[org.id] = t.name

   # orgs = list(set(orgs))

    i = 0
    for id, task in orgs.items():
        i += 1

        org = Organization.objects.get(id = id)


        message = u"Công ty `{0}` - trễ deadline step `{1}` : {2}".format(org.name, task, org.get_timeline_url() )

     #   notify_slack(settings.SLACK_TIMELINE_CHANNEL, message)

    message = u"@channel @thaopham @thanhtruc : có {0} đang trễ timeline, mọi người follow up điều chỉnh nhé . ".format( i)

  #  notify_slack(settings.SLACK_TIMELINE_CHANNEL, message)

def get_timeline(org):
    return Timeline.objects.filter(organization=org).order_by('deadline')

def get_TaskEveryStage(org):
    #return TaskEveryStage.objects.filter(organization=org).order_by('stage_id', 'due_date',
    return TaskEveryStage.objects.filter(organization=org).order_by('stage__deadline', 'due_date',
                                                                                  'due_time_start', 'end_date')
def get_last_stage(org):
    return Timeline.objects.filter(organization=org).order_by('-id')

def get_last_task(org):
    return TaskEveryStage.objects.filter(organization=org).order_by('-id')