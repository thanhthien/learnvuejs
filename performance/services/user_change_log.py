from performance.db_models.log import UserChangeLog


def log_change_user_email(actor, user, pre_email, post_email, quarter):

    uc = UserChangeLog()
    uc.actor = actor
    uc.user = user
    uc.pre_email = pre_email
    uc.post_email = post_email
    uc.quarter = quarter
    uc.save()

    return uc
