from authorization.rules import has_perm, KPI__EDITING, MANAGER_REVIEW
from django.core.exceptions import PermissionDenied
from django.http import Http404

from user_profile.models import Profile, get_full_scores_v2
from user_profile.services.profile import get_profile
from user_profile.services.user import get_user
from performance.services.kpi_backup import create_backup_kpis
from performance.models import KPIBackup, KPI
from django.contrib.auth.models import User
from utils import random_string
from user_profile.common import fix_invalid_user_email


def toggle_active(active, user_id, actor, ignore_perm_check=False, reason=''):
    if user_id:
        user = get_user(actor, user_id)
    else:
        raise Http404

    if not user:
        raise Http404

    # will not enter this case as permission is being checked in first if condition --> in get_user()
    if not has_perm(KPI__EDITING, actor, user) or not has_perm(MANAGER_REVIEW, actor, user):  # pragma: no cover
        # Neu user tu active/deactive ban than, se roi vao Case if nay va return ve user_id cua chinh user nay.
        raise PermissionDenied
    # ## remove this
    # user_org.active = active
    # user_org.save()

    profile = get_profile(user_id=user_id, actor=actor)
    profile.active = active

    # Khi active user phai luu reason = trong.
    profile.reason = reason

    profile.save(update_fields=['active', 'reason'])

    quarter_period = user.profile.get_organization().get_current_quarter()

    kpis = user.profile.get_kpis_parent(ordered=False, quarter_period=quarter_period)
    #
    # kpis = KPI.objects.filter(quarter_period=quarter_period,
    #                           parent=None,
    #                           user_id=org.user_id,
    #                           cascaded_from__isnull=False)

    to_state = 'active'
    if not active:
        to_state = 'delay'

    for kpi in kpis:
        # CascadeKPIQueue.objects.create(kpi=kpi)
        # set down_recursive=true to delay assigned kpi
        # kpi.delay_toggle(user=actor, force_to_state=to_state, down_recursive=True, ignore_perm_check=ignore_perm_check)
        kpi.delay_toggle(user=actor, force_to_state=to_state, down_recursive=True, ignore_perm_check=True)

    return profile


def migrate_employee_to_exist_position(current_profile, target_profile, keep_kpi_history, actor):

    def backup_kpi_history(actor, profile, organization):
        '''
        Backup KPI history
        '''
        quarter_periods = organization.get_archived_quarter() \
            .filter(kpi__user_id=profile.user_id).distinct()

        for quarter in quarter_periods:
            for month in [1, 2, 3]:
                create_backup_kpis(actor=actor, user=profile.user,
                                   quarter_id=quarter.id, month=month)

        current_quarter = organization.get_current_quarter()
        kpi_percent, score_m1, score_m2, score_m3 = get_full_scores_v2(profile, current_quarter)
        month = 1
        for score_m in [score_m1, score_m2, score_m3]:
            if score_m:
                create_backup_kpis(actor=actor, user=profile.user,
                                   quarter_id=current_quarter.id, month=month)
                month += 1

    def copy_profile(from_profile, target_profile):
        target_profile.organization = from_profile.organization
        target_profile.display_name = from_profile.display_name
        target_profile.phone = from_profile.phone
        target_profile.skype = from_profile.skype
        target_profile.avatar = from_profile.avatar
        target_profile.title = from_profile.title
        target_profile.department = from_profile.department
        target_profile.unit_code = from_profile.unit_code
        target_profile.employee_code = from_profile.employee_code
        target_profile.unit_code = from_profile.unit_code
        target_profile.save()

    def store_data_target_user(target_profile):
        '''
        Store target user
        '''
        store_user = User.objects.get(pk=target_profile.user_id)
        # Fix follow request Mr.Tung, when move user to exist position email, username  not change
        # store_user.email = u"[bk{}]{}".format(store_user.id, store_user.email)
        store_user.email = store_user.email + '__bk{}'.format(random_string(2))
        store_user.username = store_user.username + '__bk{}'.format(random_string(2))
        store_user.id = None
        store_user.save()
        new_profile = store_user.profile
        copy_profile(target_profile, new_profile)

        # Move backup to store user
        KPIBackup.objects.filter(user=target_profile.user).update(user=store_user)

    def move_data_current_user_to_target_user(current_profile, target_profile,
                                              organization, keep_kpi_history):
        target_user = target_profile.user
        user = current_profile.user

        # Copy email, password and profile
        target_user_email = target_user.email
        target_user.email = user.email
        target_user.username = user.username
        target_user.password = user.password

        # Backup current user
        user_email = user.email
        user.email = u"bk_{}_{}__{}".format(random_string(2), user.id, user_email)
        user.username = u"bk__{}_{}".format(random_string(2), user.id, user_email)
        user.save()
        target_user.save()

        copy_profile(current_profile, target_profile)
        current_profile.display_name = '[tp]'
        current_profile.avatar = ''
        current_profile.save()

        if keep_kpi_history:
            # Move backup from current user to target user
            KPIBackup.objects.filter(user=current_profile.user).update(user=target_user)
        # Update owner email KPIs
        KPI.objects.filter(quarter_period=organization.get_current_quarter(),
                           user=target_user,
                           owner_email=target_user_email).update(owner_email=target_user.email)

    # Backup KPI current user
    backup_kpi_history(actor, current_profile, current_profile.organization)

    # Backup KPI target user
    backup_kpi_history(actor, target_profile, current_profile.organization)

    store_data_target_user(target_profile)

    move_data_current_user_to_target_user(current_profile,
                                          target_profile, current_profile.organization,
                                          keep_kpi_history=keep_kpi_history)


def get_archived_employee(actor, user_id, per):

    # Get all user backup
    if not user_id:
        org = actor.profile.get_organization()
        try:
            list_user_backup_org = Profile.objects.filter(organization=org, parent=None, is_admin=False,
                                                          is_superuser=False).exclude(user=org.ceo).order_by("-updated_at")
            users = User.objects.filter(email__icontains='[', id__in=list_user_backup_org.values_list('user_id', flat=True))
            for u in users:
                fix_invalid_user_email(u)

            return list_user_backup_org
        except:  # pragma: no cover
            pass

    # Get detail user backup
    elif isinstance(user_id, int):
        if per:  # With request.user can't not see user_id profile
            try:
                profile = Profile.objects.get(user_id=user_id)
            except Profile.MultipleObjectsReturned:  # pragma: no cover
                profile = Profile.objects.filter(user_id=user_id).first()
                Profile.objects.filter(user_id=user_id).exclude(id=profile.id).delete()
        else:  # With request.user can see
            profile = get_profile(user_id, actor=actor)

        if profile:
            user_backup = profile.get_user_data()
            return user_backup

    return None
