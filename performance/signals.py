#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.core.cache import cache
from utils.caching_name import ORGANIZATION__GET_CURRENT_QUARTER


def set_user_organization_cache(sender, instance, **kwargs):
    cache.set('user_organization_' + str(instance.user_id), instance, 60 * 60 * 2)


def set_current_quarter_cache(sender, instance, **kwargs):
    key = ORGANIZATION__GET_CURRENT_QUARTER.format(instance.id)
    if instance.status == 'IN':
        cache.set(key, instance, 60)
    else:
        cache.delete(key)
