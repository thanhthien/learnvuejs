import traceback

from django.core.cache import cache
from django.core.mail import mail_admins

from elms.celery import app
from performance.cloner import start_cloner
from django.db import transaction
from company.services.position_chart import generate_position_chart


from utils.common import set_redis
# start_cloner() already covered in tests

from performance.services import calculate_score_cascading
from utils.threading_decorator import threading_or_worker

from performance.services.user_organization import migrate_employee_to_exist_position


@threading_or_worker
@app.task(name="cloner", queue="cloner")
def start_cloner_in_celery(organization, quarter_period):  # pragma: no cover
    start_cloner(organization=organization, quarter_period=quarter_period, prev_quarter_period=quarter_period.clone_from_quarter_period)


@threading_or_worker
@app.task(name="generate_position_chart", queue="cloner")
def start_generate_position_chart(organization):
    generate_position_chart(organization)


# https://stackoverflow.com/questions/46530784/make-django-test-case-database-visible-to-celery
# this task is neccessary for test work correctly
@app.task(name='celery.ping')
def ping():
    # type: () -> str
    """Simple task that just returns 'pong'."""
    return 'pong'


@threading_or_worker
@app.task(name='calculate_score_cascading_celery')
def calculate_score_cascading_celery(kpi_id):
    return calculate_score_cascading(kpi_id)



@threading_or_worker
@app.task(name="migrate_employee_to_exist_position_use_celery_or_thread", queue="cloner")
def migrate_employee_to_exist_position_use_celery_or_thread(profile, target_profile, keep_kpi_history, user):  # Quoc Duan wrote
    with transaction.atomic():
        try:
            migrate_employee_to_exist_position(profile, target_profile, keep_kpi_history, user)
        except Exception as e:
            errors = traceback.format_exc()
            mail_admins('error when migrate_employee_to_exist_position', errors)
            cache.set('start_thread' + str(profile.user_id), 'fail')
            raise e
        else:
            cache.set('start_thread' + str(profile.user_id), 'moved')


@threading_or_worker
@app.task(name="check_celery_working_at_django_startup", queue="cloner")
def check_celery_working_at_django_startup(celery_check_working_key):  # pragma: no cover
    set_redis(celery_check_working_key, 1, 3600)
