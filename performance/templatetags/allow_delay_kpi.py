from authorization.rules import *
from django import template
from performance.models import KPI, User, Organization
import datetime
from authorization.rules import has_perm, KPI__DELAY

register = template.Library()

@register.simple_tag(name="delay_kpi")
def delay_kpi(kpi, user):
    user_kpi = kpi.user
    if user_kpi.profile.active:
       return has_perm(KPI__DELAY, user, user_kpi)
    return False