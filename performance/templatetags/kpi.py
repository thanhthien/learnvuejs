# coding: utf-8
from random import randint

from authorization.rules import has_perm, REPORT_ADMIN
from company.services.organization import is_expired_v2
from django import template
from django.core.cache import cache
from elms import settings
from performance.base.kpi_type import *
from performance.db_models.kpi_library import JobTitle
from performance.models import KPI
from user_profile.models import Profile

# from dbtemplates.utils.cache import cache
from django.core.urlresolvers import reverse
from datetime import date
from performance.services.common import PerformanceApi

register = template.Library()


@register.filter
def get_item(dictionary, key):
    try:
        if type(dictionary) is dict:
            return dictionary.get(key)
        elif type(dictionary) is list or type(dictionary) is tuple:
            return dictionary[int(key)]
    except:
        return "Error"
#
#
# @register.filter
# def get_jobs_title(category):
#     return JobTitle.objects.filter(category=category)


@register.filter
def statistic(obj):  # pragma: no cover
    if obj:
        return "%d NV" % (obj,)
    else:
        return ""


@register.simple_tag
def settings_value(name):#pragma: no cover
    return getattr(settings, name, "")


@register.filter
def show_zero(value):#pragma: no cover
    if value == 0:
        return "0"
    elif value is None:
        return "---"
    else:
        return value


@register.simple_tag
def debug_on():#pragma: no cover
    """
    When DEBUG = True in settings, returns True.
    Returns False otherwise.
    """
    return settings.DEBUG


@register.filter
def job_title(user):#pragma: no cover
    p = Profile.objects.filter(user_id=user).first()
    if not p:
        return ''
    elif p.title:
        return p.title
    else:
        if p.current_job_title:
            return p.current_job_title.name
        else:
            return ''


@register.filter
def is_admin(user):
    # this will always return false as target argument is None
    # if has_perm(REPORT_ADMIN, user):  # pragma: no cover
    #     return True

    if user.profile.is_superuser_org():
        return True

    # try:
    #     uo = UserOrganization.objects.get(user=user)
    #     return uo.is_admin
    # except:
    #     pass

    return False


@register.filter
def to_percent(value, total):
    try:
        return value / float(total) * 100
    except:
        pass
    return 0


#TODO:
@register.filter
def get_list_kpi(group):  # pragma:no cover
    kpi_ids = []
    #     for g in group:
    #         kpi_ids += list(g.get_kpis().values_list('id', flat=True))
    return KPI.objects.filter(groupkpi__in=group).select_related('user').order_by('ordering')

#
# @register.filter
# def kpi_not_sync(kpi):
#     if not kpi.user_id or not kpi.quarter_period_id or kpi.user.email != kpi.owner_email:
#         return True
#     return False


@register.filter
def can_remove_kpi(user, kpi):
    if kpi.user_id == user.id or kpi.user_id is None:
        return True

    my_team = cache.get('my_team_' + str(user.id))
    if not isinstance(my_team, list):
        my_team = list(user.get_profile().get_subordinate().values_list('user_id', flat=True))
        cache.set('my_team_' + str(user.id), my_team, 60 * 20)

    if kpi.user_id in my_team:
        return True

    return False


@register.filter
def get_user(code):
    uo = Profile.objects.select_related('user').filter(employee_code=code).first()
    if uo:
        return uo.user
    return None

#
# @register.filter
# def is_consultant(user):
#     return False  # Profile.objects.filter(user=user, is_consultant=True).exists()


@register.filter
def is_ready(user):#pragma: no cover
    uo = user.profile
    if uo:
        organization = uo.organization
        quarter_period = organization.get_current_quarter()
        return quarter_period.is_ready
    return True


@register.filter
def url_worksheet(user):#pragma: no cover
    uo = user.profile
    if user.id == uo.organization.ceo_id: # or is_consultant(user):
        user_id = user.id
    else:
        # user_id = user.get_profile().report_to_id
        user_id = user.profile.parent.user_id if user.profile.parent else None
        if not user_id:
            user_id = user.id

    return reverse('kpi_worksheet_employee_new', kwargs={
        'hash': uo.organization.hash,
        'year': date.today().year,
        'user_id': user_id
    })

#
# @register.filter
# def old_url_worksheet(user):
#     uo = user.profile
#     if user.id == uo.organization.ceo_id : # or is_consultant(user):
#         user_id = user.id
#     else:
#         # user_id = user.get_profile().report_to_id
#         user_id = user.profile.parent.user_id if user.profile.parent else None
#         if not user_id:
#             user_id = user.id
#
#     return reverse('kpi_worksheet_employee', kwargs={
#         'hash': uo.organization.hash,
#         'year': date.today().year,
#         'user_id': user_id
#     })
#

@register.filter
def get_organization(user):
    return PerformanceApi.get_organization(user)


@register.tag(name='captureas')
def do_captureas(parser, token):#pragma: no cover
    try:
        tag_name, args = token.contents.split(None, 1)
    except ValueError:
        raise template.TemplateSyntaxError("'captureas' node requires a variable name.")
    nodelist = parser.parse(('endcaptureas',))
    parser.delete_first_token()
    return CaptureasNode(nodelist, args)


class CaptureasNode(template.Node):#pragma: no cover

    def __init__(self, nodelist, varname):
        self.nodelist = nodelist
        self.varname = varname

    def render(self, context):
        output = self.nodelist.render(context)
        context[self.varname] = output
        return ''

#
# from random import randint
#
#
# @register.assignment_tag
# def get_addressee():
#     return "World"


@register.assignment_tag()
def random_number(length=3):#pragma: no cover
    """
    Create a random integer with given length.
    For a length of 3 it will be between 100 and 999.
    For a length of 4 it will be between 1000 and 9999.
    """
    return randint(10 ** (length - 1), (10 ** (length) - 1))
#
#
# @register.filter
# def is_kpi_acc_expired(user):
#     # tartget_end_date = datetime.date.today() + datetime.timedelta(days=30)
#     # get Organization ID
#     # always returns true
#     # if not user.is_authenticated():  # pragma: no cover
#     #     return False
#
#     return is_expired_v2(user)
#         # today = date.today()
#         #
#         # if organization.kpi_end_date and organization.kpi_end_date != '':
#         #     if organization.kpi_end_date <= today:
#         #         return True
#


@register.filter
def kpi_type(k):#pragma: no cover
    kpitype = k.kpi_type()
    if kpitype == 'trunggian':
        return u'1: Trung gian'
    elif kpitype == 'chatructiep':
        return u'2.1: cha trực tiếp'
    elif kpitype == 'chacongiantiep':
        return u'2.2 or 3.2: cha gián tiếp or con gián tiếp'
    elif kpitype == 'contructiep':
        return '3.1: con trực tiếp'
    else:
        return 'UNKNOWN'


@register.filter
def bsc_type(type):
    if type == 'f':
        return 'financial'
    if type == 'c':
        return 'customer'
    if type == 'p':
        return 'procedure'
    if type == 'l':
        return 'learn'


@register.assignment_tag
def createNameForm(type_order, is_hide):
    subName = ''
    if is_hide == 'true':
        subName = 'hide'
    return type_order + subName


@register.filter
def assignablelist_from_user_id(k):
    '''
    check test_assignablelist_from_user_id for document of this function

    '''

    from_user_id = None
    if k.kpi_type_v2() == kpi_cha_normal:  # k.kpi_type() == 'chatructiep':
        from_user_id = k.user_id
    else:
        '''
        danh sách phân quyền là danh sách những nhân viên từ k.refer_to.user
        '''

        #  print 'here1'

        if k.refer_to_id > 0:
            from_user_id = k.refer_to.user.id
        elif k.parent_id > 0:
            #    print 'here'
            from_user_id = k.parent.user.id
        else:
            from_user_id = k.user_id

            # else:

            # if k.user.profile.is_subordinate_of(k.refer_to.user.profile):  # k.refer_to.user is quan ly truc tiep cua k.user
            #    from_user_id = k.refer_to.user.id
            # from_user_id=k.user.profile.report_to.id
            # else:
            #  #THIS IS CRAZY   from_user_id = k.user.profile.report_to.profile.report_to_id

    return from_user_id


# https://docs.djangoproject.com/en/dev/howto/custom-template-tags/#writing-custom-template-tags
@register.simple_tag(takes_context=True)
def comment_count(context, *args, **kwargs):#pragma: no cover

    return 0

    '''
    Tạm thời remove tính năng này để tăng tốc độ
    '''

    from performance.services.kpi import KPIServices
    request = context['request']
    read_user_id = request.user.id

    kpi = kwargs['kpi']
    kpi_id = kpi.id
    kpi_unique_key = kpi.unique_key
    # if read_user is None:
    #     read_user=k.user
    count = KPIServices.count_comment_by_kpiid(read_user_id, kpi_id)
    # count = KPIServices.count_comment(read_user_id, kpi_unique_key)
    return count if count > 0 else ''

@register.filter
def addf(exscore, performance_score):
    # use this to add float number
    # add filter default just for int
    return exscore + performance_score