# -*- coding: utf-8 -*-
import datetime
import pytest
import json
from random import random, randint

from performance.models import KPI, kpi_con_normal, kpi_cha_duoc_phan_cong, kpi_trung_gian
from performance.templatetags.kpi import get_item, is_admin, to_percent, can_remove_kpi, get_user, get_organization, \
    bsc_type, assignablelist_from_user_id, kpi_cha_normal
from utils import random_string


@pytest.mark.django_db(transaction=False)
def test_get_item(userA, userA_manager):
    list = [userA.id, userA_manager.id]
    result = get_item(list, 0)
    assert result == userA.id
    dictionary = {"id": userA_manager.id, "subordinate": userA.id}
    dictionary = dict(dictionary)
    key = "id"
    result = get_item(dictionary, key)
    assert result == userA_manager.id
    wrong_list = [1, 2, 3, 4]
    result = get_item(wrong_list, 6)
    assert result == "Error"


@pytest.mark.django_db(transaction=False)
def test_is_admin(userA, userA_manager, userAdmin, organization):
    result = is_admin(userA)
    assert result is False

    result = is_admin(userAdmin)
    assert result is True

    result = is_admin(userA_manager)
    assert result is False


@pytest.mark.django_db(transaction=False)
def test_to_percent():
    value1 = randint(1, 1000)
    total1 = randint(1, 1000)
    result = to_percent(value1, total1)
    assert result == (value1/float(total1))*100
    total1 = 0
    result = to_percent(value1, total1)
    assert result == 0


@pytest.mark.django_db(transaction=False)
def test_can_remove_kpi(userA, kpi, userA_manager, organization):
    kpi.user = userA
    kpi.save()
    result = can_remove_kpi(userA, kpi)
    assert result is True
    result = can_remove_kpi(userA_manager, kpi)
    assert result is True
    kpi.user = userA_manager
    kpi.save()
    result = can_remove_kpi(userA, kpi)
    assert result is False


@pytest.mark.django_db(transaction=False)
def test_get_user(userA, organization):
    code = randint(1, 1000)

    p = userA.profile
    p.employee_code = code
    p.save()
    result = get_user(code)
    assert result == userA

    code1 = randint(1, 1000)
    result = get_user(code1)
    if code1 != p.employee_code:
        assert result is None


@pytest.mark.django_db(transaction=False)
def test_get_organization(userA, organization):
    p = userA.profile
    p.save()
    result = get_organization(userA)
    assert result == p.organization


# @pytest.mark.django_db(transaction=False)
# def test_is_kpi_acc_expired(userA, organization):
#     """
#     Case 1: organization exists, expiry date = today
#     """
#     organization.kpi_end_date = datetime.date.today()
#     organization.save()
#     result = is_kpi_acc_expired(userA)
#     assert result is True
#
#     """
#     Case 2: no organization
#     """
#     p = userA.profile
#     p.organization = None
#     p.save()
#     result = is_kpi_acc_expired(userA)
#     assert result is False


@pytest.mark.django_db(transaction=False)
def test_assignablelist_from_user_id(userA, userB, organization):
    parent = KPI()
    parent.user = userA
    parent.save()

    """
    GIVEN: đây là KPI cha bình thường (kpi_cha_normal)
    THEN: kết quả là k.user_id
    """
    assert parent.kpi_type_v2() == kpi_cha_normal
    assert assignablelist_from_user_id(parent) == userA.id

    """
    GIVEN: đây là KPI con bình thường (kpi_con_normal)
    THEN: kết quả là k.user_id
    """

    child = KPI()
    child.user = userA
    child.parent = parent
    child.save()

    assert child.kpi_type_v2() == kpi_con_normal
    assert assignablelist_from_user_id(child) == userA.id

    """
    GIVEN: đây là KPI con cha được phân công  (kpi_cha_duoc_phan_cong)
    THEN: kết quả là k.refer_to.user_id
    """

    kpi_phan_cong = child.assign_to(userB)

    assert kpi_phan_cong.kpi_type_v2() == kpi_cha_duoc_phan_cong
    assert assignablelist_from_user_id(kpi_phan_cong) == userA.id

    """
    GIVEN: đây là KPI con trung gian (kpi_trung_gian)
    THEN: kết quả là k.refer_to.user_id
    """
    assert child.kpi_type_v2() == kpi_trung_gian
    assert assignablelist_from_user_id(child) == userA.id


    """
    GIVEN: đây là KPI con của KPI được phân công (kpi_trung_gian)
    THEN: kết quả là k.refer_to.user_id
    """
    child2 = KPI()
    child2.parent = kpi_phan_cong
    child2.save()
    assert child2.kpi_type_v2() == kpi_con_normal
    assert assignablelist_from_user_id(child2) == userB.id

    k1 = KPI()
    k1.user = userA
    k1.save()
    assert assignablelist_from_user_id(k1) == k1.user_id



@pytest.mark.django_db(transaction=False)
def test_bsc_type():
    x = random_string(1)
    typek = ['f', 'c', 'p', 'l']
    result = bsc_type(typek[0])
    assert result == "financial"
    result = bsc_type(typek[1])
    assert result == "customer"
    result = bsc_type(typek[2])
    assert result == "procedure"
    result = bsc_type(typek[3])
    assert result == "learn"
    if x not in typek:
        result = bsc_type(x)
        assert result is None



