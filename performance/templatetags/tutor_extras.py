# coding: utf-8
from django import template
from elms import settings

register = template.Library()


@register.inclusion_tag('tracking/code.html', takes_context=True)
def show_tracking(context):
    request = context['request']
    show = True
    is_authenticated = request.user.is_authenticated()
    if request.user.is_authenticated() and request.user.is_staff:
            show = False

    if settings.DEBUG or settings.TESTING or settings.DEV_SERVER:
        show = False

    # Hong remove this to track right  for KPIs
    # if settings.DISABLE_ANALYTICS:
    #     show = False

    return {
                'show': show,
                'is_authenticated': is_authenticated,
                'user': request.user,
              #  'saleup': request.path.find('saleup') >= 0
            }


@register.filter(name='deadline')
def deadline(cf, user):  # pragma: no cover
    return cf.deadline(user)

@register.inclusion_tag('tracking/facebook.html', takes_context=True)
def show_facebook_tracking(context):
    # request = context['request']
    show = True

    # is_authenticated = request.user.is_authenticated()
    if settings.DISABLE_FACEBOOK_TRACKING:
        show = False

    return {
        'show': show,
        # 'is_authenticated': is_authenticated,
        # 'user': request.user,
        # 'saleup': request.path.find('saleup') >= 0
    }

@register.inclusion_tag('common/remarketing-google.html', takes_context=True)
def show_google_remarketing(context):
    # request = context['request']
    show = True
    # is_authenticated = request.user.is_authenticated()
    if settings.DISABLE_GOOGLE_REMARKETING:
        show = False

    return {
        'show': show,
        # 'is_authenticated': is_authenticated,
        # 'user': request.user,
        # 'saleup': request.path.find('saleup') >= 0
    }

@register.inclusion_tag('modules/remarketing_pixel.html', takes_context=True)
def show_facebook_remarketing(context):
    # request = context['request']
    show = True
    # is_authenticated = request.user.is_authenticated()
    if settings.DISABLE_FACEBOOK_REMARKETING:
        show = False

    return {
        'show': show,
        # 'is_authenticated': is_authenticated,
        # 'user': request.user,
        # 'saleup': request.path.find('saleup') >= 0
    }

@register.inclusion_tag('common/googleads-doubleclick-emailbased.html', takes_context=True)
def show_googleads_doubleclick_emailbased(context):
    # request = context['request']
    show = True
    # is_authenticated = request.user.is_authenticated()
    if settings.DISABLE_GOOGLEADS_DOUBLECLICK:
        show = False

    return {
        'show': show,
        # 'is_authenticated': is_authenticated,
        # 'user': request.user,
        # 'saleup': request.path.find('saleup') >= 0
    }


