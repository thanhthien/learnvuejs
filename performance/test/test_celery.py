from __future__ import absolute_import, unicode_literals
from celery import shared_task
# from pyvirtualdisplay import Display
#
# display = Display(visible=0, size=(800, 600))
# display.start()
import logging
import time
import pytest
from django.contrib.auth.models import User
from utils import reload_model_obj

# from elms import settingscelery
# from django.conf import settings
# from elms.celery import app
from utils.threading_decorator import threading_or_worker
from utils.random_str import random_string
logger = logging.getLogger(__name__)


userA_new_username = random_string()
userA_new_username_1 = random_string()
userA_new_username_2 = random_string()
userA_new_username_3 = random_string()
userA_new_username_4 = random_string()

@threading_or_worker(auto_select=True)
@shared_task(name="threading_or_worker__auto_select")
def threading_or_worker__auto_select(userA):
    userA.username = userA_new_username_1
    userA.save()

@threading_or_worker(auto_select=False, use_celery=True)
@shared_task(name="threading_or_worker__use_celery")
def threading_or_worker__use_celery(userA):
    userA.username = userA_new_username_2
    userA.save()

@threading_or_worker(auto_select=False, use_celery=False, use_threading=True)
@shared_task(name="threading_or_worker__use_threading")
def threading_or_worker__use_threading(userA):
    userA.username = userA_new_username_3
    userA.save()

@threading_or_worker(auto_select=False, use_celery=False, use_threading=False)
@shared_task(name="threading_or_worker__inprocess")
def threading_or_worker__inprocess(userA):
    userA.username = userA_new_username_4
    userA.save()


def wait_for_threading_or_worker_done():
    time.sleep(10)


# from conftest import userA_new_username_1, threading_or_worker__auto_select
@pytest.mark.django_db(transaction=True)
def test_threading_or_worker__auto_select(userA,  celery_app):
    # depend on settings.ENABLE_CELERY, the function will run in either Celery or Threading
    logger.info('call threading_or_worker__auto_select(userA)')
    threading_or_worker__auto_select(userA)

    logger.info('call wait_for_threading_or_worker_done()')
    wait_for_threading_or_worker_done()
    userA=reload_model_obj(userA)
    assert userA.username == userA_new_username_1

@pytest.mark.django_db(transaction=True)
def test_threading_or_worker__use_celery(userA,  celery_app):
    threading_or_worker__use_celery(userA)

    wait_for_threading_or_worker_done()
    userA = reload_model_obj(userA)
    assert userA.username == userA_new_username_2


@pytest.mark.django_db(transaction=True)
def test_threading_or_worker__use_threading(userA,  celery_app):
    threading_or_worker__use_threading(userA)

    wait_for_threading_or_worker_done()
    userA = reload_model_obj(userA)
    assert userA.username == userA_new_username_3


@pytest.mark.django_db(transaction=True)
def test_threading_or_worker__inprocess(userA):
    threading_or_worker__inprocess(userA)

    # do not need to wait, because we for use in-process mode
    # wait_for_threading_or_worker_done()
    userA = reload_model_obj(userA)
    assert userA.username == userA_new_username_4





# @pytest.mark.django_db(transaction=True)
# def test_run_celery( celery_app):
#     run_celery()
#

@threading_or_worker(auto_select=True)
@shared_task(name='celery_1')
def celery_1():
    logger.info('In celery_1 function')

@pytest.mark.django_db(transaction=True)
def test_run_celery_shared_task( celery_app):
    celery_1()



@threading_or_worker(auto_select=False, use_celery=True)
@shared_task(name="test_celery", queue='default')
def temp_celery(userA):

    logger.debug('Change userA username')
    userA.username = userA_new_username
    userA.save()



@pytest.mark.django_db(transaction=True)
def test_celery(userA,  celery_app):
    temp_celery(userA)

    logger.debug('test_celery:23')


    is_done = False
    while is_done == False:

        if User.objects.get(id = userA.id).username == userA_new_username:
            is_done = True

    #if celery run success then we will be here, and test pass
    assert 1 == 1

@pytest.mark.django_db(transaction=True)
def test_threading_or_worker__access_original_function(userA,  celery_app):
    threading_or_worker__auto_select._original_func(userA)
    # do not need to wait
    reload_model_obj(userA)
    assert userA.username == userA_new_username_1

    threading_or_worker__use_celery._original_func(userA)
    # do not need to wait
    reload_model_obj(userA)
    assert userA.username == userA_new_username_2

    threading_or_worker__use_threading._original_func(userA)
    # do not need to wait
    reload_model_obj(userA)
    assert userA.username == userA_new_username_3

    threading_or_worker__inprocess._original_func(userA)
    # do not need to wait
    reload_model_obj(userA)
    assert userA.username == userA_new_username_4

