from __future__ import absolute_import, unicode_literals
import re
from datetime import datetime
from random import randint

import pytest
import time
import json
from django.core.urlresolvers import reverse
from django.forms import DateTimeField
# from performance.cloner import PerformanceCloner
from performance.cloner import start_cloner, start_clone_strategy_map, clone_user, CompanyCloner, \
    clone_group, align_kpi_to_new_quarter, align_kpis_single_user, Cloner
# from performance.conftest import run_celery
from performance.db_models.competency import Competency
from performance.db_models.log import KpiLog
from performance.models import KPI, QuarterPeriod
from performance.tasks import start_cloner_in_celery
from user_profile.models import Profile
from company.models import Organization, UserOrganization
from user_profile.services.user import set_new_password
from utils.random_str import random_string
from performance.db_models.strategy_map import StrategyMap, GroupKPI
from utils import unsigned_vi
from django.contrib.auth.models import User
import logging
from utils import reload_model_obj

from celery import shared_task

from utils.threading_decorator import threading_or_worker

logger=logging.getLogger('django')

def create_competency(user, q):
    comp = Competency()
    comp.user = user
    comp.quarter_period = q
    comp.name = random_string(5)
    comp.name_vi = random_string(8)
    comp.name_en = random_string(8)
    comp.description = random_string()
    comp.category = "BSC"
    comp.code = 'F'
    comp.level = 1
    comp.save()
    return comp

def get_quarter_target(kpi):
    if kpi.quarter_period.quarter == 1:
        return kpi.quarter_one_target
    elif kpi.quarter_period.quarter == 2:
        return kpi.quarter_two_target
    elif kpi.quarter_period.quarter == 3:
        return kpi.quarter_three_target
    elif kpi.quarter_period.quarter >= 4:
        return kpi.quarter_four_target
    return None


# @pytest.mark.celery(result_backend=CELERY_RESULT_BACKEND)
@pytest.mark.django_db(transaction=True)
def test_create_new_quarter(organization, userA, userA_manager_manager, userA_manager,  celery_app):
    '''
       GIVEN: PerformanceCloner is runnning
       THEN: fix_self should not working
       <fix>
       '''

    old_quarter = organization.get_current_quarter()

    # score_calculation_type = 'most_recent'
    k = KPI()
    k.name = "k.name | " + random_string()
    k.user = userA_manager_manager
    k.score_calculation_type = 'most_recent'
    k.quarter_one_target = 33
    k.quarter_two_target = 66
    k.quarter_three_target = 99
    k.quarter_four_target = 111
    k.quarter_period = old_quarter
    k.save()

    k1 = k.assign_to(userA_manager)
    k1.name = "k1.name | " + random_string()
    k1.save()

    assert k.get_children().count() == 1

    k12 = k1.assign_to(userA)

    assert k1.get_children().count() == 1

    k12.name = "k2.name | " + random_string()
    k12.save()

    # score_calculation_type = 'average'
    avg_k = KPI()
    avg_k.name = "k.name | " + random_string()
    avg_k.user = userA_manager_manager
    avg_k.score_calculation_type = 'average'
    avg_k.quarter_one_target = 33
    avg_k.quarter_two_target = 66
    avg_k.quarter_three_target = 99
    avg_k.quarter_four_target = 111
    avg_k.quarter_period = old_quarter
    avg_k.save()

    # score_calculation_type = 'sum'
    sum_k = KPI()
    sum_k.name = "k.name | " + random_string()
    sum_k.user = userA_manager_manager
    sum_k.score_calculation_type = 'sum'
    sum_k.quarter_one_target = 33
    sum_k.quarter_two_target = 66
    sum_k.quarter_three_target = 99
    sum_k.quarter_four_target = 111
    sum_k.quarter_period = old_quarter
    sum_k.save()

    q = QuarterPeriod.objects.create(organization=organization,
                                     due_date=datetime.now().date(),
                                     clone_from_quarter_period=old_quarter)

    q.status = 'IN'
    q.save()

    # cloner = PerformanceCloner(organization, q)
    # cloner._run()
    logger.info('Cloner.mark_system_as_in_cloning_process(organization): {}'.format(organization.name))
    Cloner.mark_system_as_in_cloning_process(organization)
    start_cloner_in_celery(organization, q)
    # start_cloner_in_celery.delay(organization, q)

    '''
    Wait until finish
    '''
    while organization.is_ready() is False:
        print 'waiting CREATING_ASSESSMENT_ORG'
        time.sleep(1)

    new_quarter = organization.get_current_quarter()
    assert old_quarter.id != organization.get_current_quarter()

    k_new = KPI.objects.filter(quarter_period=new_quarter,
                               user=userA_manager_manager,
                               parent=None,
                               score_calculation_type='most_recent').first()
    avg_k_new = KPI.objects.filter(quarter_period=new_quarter,
                                   user=userA_manager_manager,
                                   parent=None,
                                   score_calculation_type='average').first()
    sum_k_new = KPI.objects.filter(quarter_period=new_quarter,
                                   user=userA_manager_manager,
                                   parent=None,
                                   score_calculation_type='sum').first()
    # assert k_new.user == userA_manager_manager#

    assert k_new.name == k.name

    k_new_children = k_new.get_children()
    assert k_new_children.count() == 1
    assert k_new_children[0].copy_from == k1.cascaded_from
    # assert k_new_children[0].name == k1.cascaded_from.name => no need to assert for children kpi's name

    k1_new = k_new_children[0].get_assigned_kpi()
    assert k1_new is not None
    assert k1_new.name == k1.name

    k1_new_children = k1_new.get_children()
    assert k1_new_children.count() == 1
    # assert k1_new_children[0].name == k12.name

    k12_new = k1_new_children[0].get_assigned_kpi()
    assert k12_new.user == userA

    # Check quarter target not change
    assert k_new.quarter_one_target == k.quarter_one_target
    assert k_new.quarter_two_target == k.quarter_two_target
    assert k_new.quarter_three_target == k.quarter_three_target
    assert k_new.quarter_four_target == k.quarter_four_target

    # score_calculation_type = 'most_recent' -> 3 thang target bang target quy
    target = get_quarter_target(k_new)
    assert k_new.month_1_target == target
    assert k_new.month_2_target == target
    assert k_new.month_3_target == target

    # Check quarter target not change
    assert avg_k_new.quarter_one_target == avg_k.quarter_one_target
    assert avg_k_new.quarter_two_target == avg_k.quarter_two_target
    assert avg_k_new.quarter_three_target == avg_k.quarter_three_target
    assert avg_k_new.quarter_four_target == avg_k.quarter_four_target

    # score_calculation_type = 'average' -> 3 thang target bang target quy
    target = get_quarter_target(avg_k_new)
    assert avg_k_new.month_1_target == target
    assert avg_k_new.month_2_target == target
    assert avg_k_new.month_3_target == target

    # Check quarter target not change
    assert sum_k_new.quarter_one_target == sum_k.quarter_one_target
    assert sum_k_new.quarter_two_target == sum_k.quarter_two_target
    assert sum_k_new.quarter_three_target == sum_k.quarter_three_target
    assert sum_k_new.quarter_four_target == sum_k.quarter_four_target

    # score_calculation_type = 'sum' -> 3 thang target bang 1/3 target quy
    target = get_quarter_target(sum_k_new)
    assert sum_k_new.month_1_target == target / 3.0
    assert sum_k_new.month_2_target == target / 3.0
    assert sum_k_new.month_3_target == target / 3.0


@pytest.mark.django_db(transaction=False)
def test_start_cloner(organization, userA):
    organization.enable_competency = True
    organization.save()

    prev_quarter_period = QuarterPeriod()
    prev_quarter_period.due_date = datetime.now()
    prev_quarter_period.quarter = 2
    prev_quarter_period.organization = organization
    prev_quarter_period.save()

    sm = StrategyMap(
        organization=organization,
        year=datetime.now().year,
        director=userA,
        name=random_string(),
        description=random_string(),
        quarter_period=prev_quarter_period,
    )
    sm.save()

    group_kpi = GroupKPI(
        map=sm,
        name=random_string(),
        category='financial'
    )

    group_kpi.save()

    count_group_kpi_before = GroupKPI.objects.all().count()
    count_sm_before = StrategyMap.objects.all().count()
    count_kpi_before = KPI.objects.all().count()
    count_kpi_quarter = KPI.objects.filter(quarter_period=organization.get_current_quarter()).count()

    quarter_period = organization.get_current_quarter()
    quarter_period.clone_from_quarter_period = prev_quarter_period
    quarter_period.save()

    assert quarter_period.clone_from_quarter_period == prev_quarter_period

    start_cloner(organization=organization, quarter_period=quarter_period,
                 prev_quarter_period=quarter_period.clone_from_quarter_period)

    count_group_kpi_after = GroupKPI.objects.all().count()
    assert count_group_kpi_after == count_group_kpi_before * 2

    count_kpi_after = KPI.objects.all().count()
    assert count_kpi_after == count_kpi_before + count_kpi_quarter

    count_sm_after = StrategyMap.objects.all().count()
    assert count_sm_after == count_sm_before * 2


@pytest.mark.django_db(transaction=False)
def test_clone_user(organization, userA, userA_manager, kpi, ceo):

    slug = unsigned_vi(organization.name).lower().replace(' ', '')
    domain = slug + ".cloudjetsolutions.com"
    q = organization.get_current_quarter()

    """
    Case 1: Username length > 30
    """
    username_length = randint(31,200)
    manager_clone = userA_manager
    p_userA = userA.profile
    p_userA.display_name = random_string(username_length)
    p_userA.save()

    _username = unsigned_vi(p_userA.display_name).lower().replace(' ', '')
    username = re.sub(r'[^A-Za-z0-9\.\+_-]+', "", _username)

    username = username[:30]

    clone_user(organization,userA_manager, domain, q, manager_clone, clone_org=None, clone_quarter_period=q)

    assert len(User.objects.filter(username=username)) == 1


    """
    Case 2: Username length <= 30
    """
    username_length = randint(1, 30)
    p_userA = userA.profile
    p_userA.display_name = random_string(username_length)
    p_userA.save()

    _username = unsigned_vi(p_userA.display_name).lower().replace(' ', '')
    username = re.sub(r'[^A-Za-z0-9\.\+_-]+', "", _username)

    clone_user(organization, userA_manager, domain, q, manager_clone, clone_org=None, clone_quarter_period=q)

    assert len(User.objects.filter(username=username)) == 1
    count = len(User.objects.filter().all())

    """
    Case 3: Username Exists & competency is not None for userA
    """
    comp = Competency()
    comp.user = userA
    comp.quarter_period = q
    comp.name = random_string(5)
    comp.name_vi = random_string(8)
    comp.name_en = random_string(8)
    comp.description = random_string()
    comp.category = "BSC"
    comp.code = 'F'
    comp.level = 1
    comp.save()
    clone_user(organization, userA_manager, domain, q, manager_clone, clone_org=None, clone_quarter_period=q)

    assert len(User.objects.filter(username=username)) == 1
    assert len(User.objects.filter().all()) > count

# from_organization = organization
    # from_quarter = from_organization.get_current_quarter()
    # pceo = ceo.profile
    # pceo.display_name = random_string(40)
    # pceo.save()
    # from_ceo = ceo
    # from_p_ceo = from_ceo.profile
    #
    # count_company_before = Organization.objects.all().count()
    #
    # count_user_before = Profile.objects.all().count()
    # count_user_company = from_p_ceo.get_descendant_count()
    #
    # new_ceo_email = random_string() + '@cloudjetsolution.com'
    # new_ceo_username = random_string()
    # new_ceo = User.objects.create(username=new_ceo_username, email=new_ceo_email)
    # password = random_string()
    # new_ceo, p = set_new_password(new_ceo.id, password)
    # new_ceo.save()
    #
    # company_name = organization.name
    #
    # slug = unsigned_vi(company_name).lower().replace(' ', '')
    # domain = slug + ".cloudjetsolutions.com"
    #
    # new_organization = Organization.objects.create(user=new_ceo,
    #                                                slug=slug,
    #                                                domain=domain,
    #                                                name=company_name,
    #                                                enable_performance=True,
    #                                                ceo=new_ceo,
    #                                                industry_id=organization.industry_id,
    #                                                trial=True)
    #
    # new_quarter_period = organization.get_current_quarter()
    # new_quarter_period.is_ready = True
    # new_quarter_period.save()
    #
    # clone_user(new_organization, new_ceo, domain, new_quarter_period, from_ceo, from_organization,
    #            from_quarter)
    #
    # count_company_after = Organization.objects.all().count()
    # count_user_after = Profile.objects.all().count()
    #
    # assert count_company_after == count_company_before + 1
    # assert count_user_after == count_user_before + count_user_company + 1  # +1 new_ceo


@pytest.mark.django_db(transaction=False)
def test_company_cloner(organization, ceo, kpi, userA_manager, userA):
    count_company_before = Organization.objects.all().count()
    count_user_before = Profile.objects.all().count()
    count_user_company = organization.ceo.profile.get_descendant_count()

    company_name = random_string()
    ceo_email = random_string() + '@cloudjetsolutions.com'
    password = random_string()

    """
    To test Username exists loop
    """
    username = unsigned_vi(ceo.get_profile().display_name).lower().replace(' ', '')
    userA.username = username
    userA.save()
    count = 1
    temp = username + str(count)
    userA_manager.username = temp
    userA_manager.save()

    comp = create_competency(ceo, organization.get_current_quarter())
    kpi.user = ceo
    kpi.parent = None
    kpi.save()

    CompanyCloner(ceo, organization, company_name, ceo_email, password).run()
    count_company_after = Organization.objects.all().count()
    count_user_after = Profile.objects.all().count()

    assert count_company_after == count_company_before + 1
    assert count_user_after == count_user_before + count_user_company + 1


# confirm copy_from logic --> Done
@pytest.mark.django_db(transaction=False)
def test_clone_group(userA, kpi, organization):
    strategymap = StrategyMap(
        organization=organization,
        year=2017,
        director=userA,
        name=random_string(),
        description=random_string(),
        quarter_period=userA.profile.get_current_quarter(),
        created_at=DateTimeField()
    )
    strategymap.save()

    group_kpi = GroupKPI(
        map=strategymap,
        name=random_string()
    )

    group_kpi.save()

    kpi.group_kpi = group_kpi
    kpi.quarter_period = organization.get_current_quarter()
    kpi.save()

    k = KPI()
    k.user = userA
    k.quarter_period = organization.get_current_quarter()
    k.name = "Copied kpi"
    k.group_kpi = group_kpi
    k.copy_from = kpi
    k.save()

    groups = [group_kpi]
    count = GroupKPI.objects.count()
    clone_group(groups, strategymap, organization.get_current_quarter())

    assert (count+1) == GroupKPI.objects.count()


@pytest.mark.django_db(transaction=False)
def test_align_kpi_to_new_quarter(userA, userA_manager, organization, kpi, parent_kpi):
    old_quarter = organization.get_current_quarter()
    dt = organization.last_copy_time

    k1 = KPI()
    k1.name = "First KPI"
    k1.user = userA
    k1.quarter_period = old_quarter
    k1.cascaded_from = kpi
    k1.parent = None
    k1.save()

    kpi.cascaded_from = parent_kpi
    kpi.save()


    """
    Cascaded new
    """
    k2 = KPI()
    k2.name = "Second KPI"
    k2.user = userA
    k2.copy_from = kpi
    k2.refer_to = kpi
    k2.quarter_period = old_quarter
    k2.cascaded_from = k1
    k2.parent = None
    k2.save()

    new_quarter = QuarterPeriod.objects.create(organization=organization,
                                               due_date=datetime.now().date(),
                                               clone_from_quarter_period=old_quarter)
    """
    New Quarter KPI
    """
    k3 = KPI()
    k3.name = "Third KPI"
    k3.user = userA
    k3.copy_from = k1
    k3.quarter_period = new_quarter
    k3.cascaded_from = kpi
    k3.parent = None
    k3.save()

    result = align_kpi_to_new_quarter(userA, old_quarter, new_quarter, organization)
    assert dt != organization.last_copy_time
    checkpi = KPI.objects.get(id=k2.id)
    assert checkpi.refer_to is None

@pytest.mark.django_db(transaction=False)
def test_align_kpis_single_user(organization, userA, userA_manager, parent_kpi, kpi_userA_manager):
    # do assign up parent_kpi to kpi_userA_manager for later assertion
    parent_kpi.assign_up(kpi_userA_manager)
    quarter_period=organization.get_current_quarter()

    today=datetime.now().date()
    # year = 2018
    year=today.year

    q = QuarterPeriod.objects.create(organization=organization,
                                     due_date=today,
                                     year=year,
                                     quarter=1,
                                     #   clone_from_quarter=current_quarter,
                                     clone_from_quarter_period=quarter_period,
                                     quarter_name='test quarter_name',
                                     quarter_1_year=year,
                                     quarter_2_year=year,
                                     quarter_3_year=year,
                                     quarter_4_year=year,
                                     )



    thecloner=Cloner(organization, quarter_period, q)
    # manualy call clone phase,
    # this action will clone all kpis from old quarter to new quarter
    thecloner._clone_kpis_phase()
    # userA had kpi `parent_kpi` in old quarter
    assert parent_kpi.quarter_period == quarter_period
    # userA_manager had kpi `kpi_userA_manager` in old quarter
    assert kpi_userA_manager.quarter_period == quarter_period
    # assert that `parent_kpi` have been cloned to new quarter
    new_parent_kpi=KPI.objects.filter(copy_from=parent_kpi, quarter_period=q).first()
    assert new_parent_kpi is not None
    assert new_parent_kpi.quarter_period == q
    # assert that `kpi_userA_manager` have been cloned to new quarter
    new_kpi_userA_manager = KPI.objects.filter(copy_from=kpi_userA_manager, quarter_period=q).first()
    assert new_kpi_userA_manager is not None
    assert new_kpi_userA_manager.quarter_period == q


    # since userA's kpis already cloned to new quarter, now we align userA's kpis
    align_kpis_single_user(userA, quarter_period, q, organization)
    # assertions here
    # refresh kpi data
    new_parent_kpi=reload_model_obj(new_parent_kpi)
    new_kpi_userA_manager=reload_model_obj(new_kpi_userA_manager)
    # assert that `new_parent_kpi` which cloned from `parent_kpi` has aligned
    assert new_parent_kpi.refer_to == new_kpi_userA_manager
    assert new_parent_kpi.cascaded_from.copy_from == parent_kpi.cascaded_from


# @pytest.mark.django_db(transaction=True)
# def test_run_celery( celery_app):
#     run_celery()
#


##  THE FOLLOWING TEST WORKED
# @threading_or_worker
# @shared_task
# def celery_1():
#     logger.info('In celery_1 function')
#
# @pytest.mark.django_db(transaction=True)
# def test_run_celery_shared_task( celery_app):
#     celery_1()
