# !/usr/bin/python
# -*- coding: utf-8 -*-
import pytest
from performance.models import KPI
from django.core.urlresolvers import reverse


@pytest.mark.django_db(transaction=False)
def test_comment_note(organization, userA, userA_manager, client_authed):
    k = KPI()
    k.quarter_period = organization.get_current_quarter()
    k.user = userA_manager
    k.save()

    k1 = k.assign_to(userA)

    url = '{}?id={}'.format(reverse('kpicomment', kwargs={'unique_key': k1.unique_key}), k1.id)
    data = {
        'comment': 'Test comment'
    }
    response = client_authed.post(url, data, content_type='application/json')
    assert response.status_code == 200
    #assert len(k1.get_notes()) == 1

