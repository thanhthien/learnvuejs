import pytest
from actstream import action
from actstream.actions import follow
from actstream.models import user_stream, any_stream, target_stream


@pytest.mark.django_db(transaction=False)
def test_create_activity(userA, userB, userC,  kpi):
    action.send(userA, verb='reached level 10')

    us = user_stream(userA, with_user_activity=True)

    #print us
    assert len(us) == 1

    follow(userA, userB,send_action = False, actor_only=False)
    follow(userA, userB,send_action = False, actor_only=False)

    action.send(userB, verb='reached level 11')
    us = user_stream(userA, with_user_activity=False)

    #print us
    assert len(us) == 1

    action.send(userC, verb='fighting with', target=userB)
    us = user_stream(userA)

    print us
    assert len(us) == 2

