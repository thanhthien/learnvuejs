# !/usr/bin/python
# -*- coding: utf-8 -*-
import json

from django.core.urlresolvers import reverse
import pytest

from user_profile.models import Profile
from utils.random_str import random_string
import random


class ConfigReview:
    MAX_LEVEL = 3
    AMOUNT_KPI_PER_PERSON = 8
    MAX_SUBORDINATE = 3


_index = 0


def create_organization_chart(ceo, organization_standard, client_ceo):
    max_level = ConfigReview.MAX_LEVEL
    kpis = []

    def create_new_subordinate(organization, manager_profile_id, level):
        global _index
        data = {'name': 'user test level {}'.format(level),
                'email': 'user_{}_level_{}@gmail.com'.format(_index, level),
                'manager': 'u{}'.format(manager_profile_id),
                'send_pass': 'true'
                }
        url = reverse('add_people')
        response = client_ceo.post(url, data)

        data = json.loads(response.content)
        assert response.status_code == 200
        assert data['id'] > 0
        _index = _index + 1
        return data['data']

    def create_new_sub_recursive(organization, manager_profile_id, max_level, kpis=[], level=1):
        if level > max_level:
            return

        for i in range(0, ConfigReview.MAX_SUBORDINATE):
            # ## Step 1: create new user
            create_new_subordinate(organization=organization,
                                   manager_profile_id=manager_profile_id,
                                   level=level)

        manager_profile = Profile.objects.get(id=manager_profile_id)
        subs = manager_profile.get_subordinate()
        for sub in subs:
            # ## Step 4: create new user's subordinate
            create_new_sub_recursive(organization=organization,
                                     manager_profile_id=sub.id,
                                     max_level=max_level, kpis=[],
                                     level=level + 1)

    create_new_sub_recursive(organization=organization_standard,
                             manager_profile_id=ceo.profile.id,
                             max_level=max_level,
                             kpis=kpis)

    # assert _index == 2 ** (max_level + 1) - 1
    assert ceo.profile.get_all_subordinate().count() == _index


def update_levels_data(ceo):
    data_levels = {}
    _get_data_levels(ceo.profile, data_levels)
    return data_levels


def _get_data_levels(profile, data_levels, level=1):
    subs = profile.get_subordinate()

    if subs.exists():
        if not data_levels.get(level):
            data_levels[level] = []

        data_levels[level].extend(list(subs.values_list('id', flat=True)))
        for sub in subs:
            _get_data_levels(sub, data_levels, level=level + 1)


def get_random_manager(data_levels):
    data_keys = data_levels.keys()
    index = random.randint(0, len(data_keys) - 1)
    return data_levels[data_keys[index]][0]


@pytest.mark.django_db(transaction=False)
def test_get_all_subordinate(ceo, organization_standard, client_ceo):
    create_organization_chart(ceo, organization_standard, client_ceo)
    total = _index

    data_levels = update_levels_data(ceo)
    link = reverse('add_people')

    # Test create new 10 employees
    emails = []
    for i in range(1, 11):
        name = random_string()
        position = random_string(5)
        phone = random.randint(1, 10000)
        manager = get_random_manager(data_levels)
        email = u'{}{}@gmail.com'.format(name.lower(), i)
        data = {
            'name': name,
            'email': email,
            'manager': 'u{}'.format(manager),
            'phone': str(phone),
            'position': position,
            'employee_code': None,
            'skype': None,
            'unit_code': None,
            'send_pass': False,

        }
        response = client_ceo.post(link, data)
        emails.append(email)
        assert response.status_code == 200

    assert ceo.profile.get_all_subordinate().count() == (total + 10)

    data_levels = update_levels_data(ceo)

    link = reverse('manager_change')

    # Test change manager of 8 employees
    all_profiles = Profile.objects.filter(user__email__in=emails)
    profiles = all_profiles[:8]
    remove_profiles = all_profiles[8:]
    manager_p = ceo.profile.get_subordinate().last()
    for p in profiles:
        data = {
             "user_id": p.user_id,
             "new_manager_email": manager_p.user.email,
             "move_everything": True
        }
        response = client_ceo.post(link, json.dumps(data), content_type="application/json")
        assert response.status_code == 200

    assert ceo.profile.get_all_subordinate().count() == (total + 10)

    # Test delete 2 employees
    link = reverse('delete_people')
    for profile in remove_profiles:
        assert profile.get_subordinate().count() == 0
        data = {
            'user_id': profile.user_id,
            'delete_kpis': True
        }
        response = client_ceo.post(link, json.dumps(data), content_type="application/json")
        assert response.status_code == 200

    assert ceo.profile.get_all_subordinate().count() == (total + 8)
