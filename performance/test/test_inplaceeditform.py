# # !/usr/bin/python
# # -*- coding: utf-8 -*-
# import json
#
# import pytest
# from django.core.urlresolvers import reverse
# from django.http import JsonResponse
# from performance.models import KPI
# from utils.random_str import random_string
#
#
# @pytest.mark.django_db(transaction=False)
# def test_inplace(kpi_userA_manager, userA, userB, userA_manager, client_userA_manager):
#     url_inplace = reverse('inplace_save')
#     ### test manager userA assignment for userA --> ok
#     kpi_id = kpi_userA_manager.id
#     email = userA.email
#     data = {
#         "filters_to_show": "default:'---'",
#         "obj_id": kpi_id,
#         "adaptor": "kpi_assign_user",
#         "edit_empty_value": "---",
#         "autosave": 1,
#         "can_auto_save": 1,
#         "auto_width": 1,
#         "app_label": "performance",
#         "module_name": "kpi",
#         "auto_height": 1,
#         "field_name": "owner_email",
#         "__widget_font_size": "13px",
#         "value": email
#     }
#     kwargs = {'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
#     response = client_userA_manager.post(url_inplace, data, **kwargs)
#     assert response.status_code == 200
#     data = json.loads(response.content)
#     assert data['errors'] == False
#     assert data['value'] != ''
#
#     ### test manager userA assignment for random email --> ok
#
#     kpi_id = kpi_userA_manager.id
#     email = userB.email
#     data = {
#         "filters_to_show": "default:'---'",
#         "obj_id": kpi_id,
#         "adaptor": "kpi_assign_user",
#         "edit_empty_value": "---",
#         "autosave": 1,
#         "can_auto_save": 1,
#         "auto_width": 1,
#         "app_label": "performance",
#         "module_name": "kpi",
#         "auto_height": 1,
#         "field_name": "owner_email",
#         "__widget_font_size": "13px",
#         "value": random_string()
#     }
#     kwargs = {'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
#     response = client_userA_manager.post(url_inplace, data, **kwargs)
#     assert response.status_code == 404
#
#     ### test manager userA assignment for userB --> ok
#     kpi_id = kpi_userA_manager.id
#     email = userB.email
#     data = {
#         "filters_to_show": "default:'---'",
#         "obj_id": kpi_id,
#         "adaptor": "kpi_assign_user",
#         "edit_empty_value": "---",
#         "autosave": 1,
#         "can_auto_save": 1,
#         "auto_width": 1,
#         "app_label": "performance",
#         "module_name": "kpi",
#         "auto_height": 1,
#         "field_name": "owner_email",
#         "__widget_font_size": "13px",
#         "value": email
#     }
#     kwargs = {'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
#     response = client_userA_manager.post(url_inplace, data, **kwargs)
#     assert response.status_code == 403