# coding: utf-8
import datetime
import pytest

from authorization.rules import has_perm, DELETE_KPI
from performance.base.kpi_type import *
from performance.models import KPI, KpiComment
from performance.templatetags.kpi import assignablelist_from_user_id
from performance.services.kpi import update_score, remove_kpi, \
    calculate_score_cascading, get_kpi_by_kwargs
from django.core.exceptions import PermissionDenied
from performance.services.kpi import create_child_kpi
from django.utils.translation import ugettext
from django.core.urlresolvers import reverse
from utils.common import to_json
import json
from utils import reload_model_obj
from django.contrib.auth.models import User


@pytest.mark.django_db(transaction=False)
def test_create_child_kpi_when_other_are_delayed(userAdmin, userA_manager, userA, userB, organization):
    '''
    test admin/manager can delete kpi of user/subordinate
    :param userAdmin:
    :param userA_manager:
    :param userA:
    :param userB:
    :param organization:
    :return:
    '''

    k1 = create_kpi(userA, organization)
    k2 = create_child_kpi(k1, organization.get_current_quarter(), 'New KPI Child 1', userA_manager)
    k3 = create_child_kpi(k1, organization.get_current_quarter(), 'New KPI Child 2', userA_manager)
    k4 = create_child_kpi(k1, organization.get_current_quarter(), 'New KPI Child 3', userA_manager)

    '''
    delay k1 and try to create new kpi => raise PermissionDenied and no KPI was created
    '''
    k1.delay_toggle(recursive=False, force_to_state='delay')

    k5 = None
    try:
        k5 = create_child_kpi(k1, organization.get_current_quarter(), 'New KPI Child 4', userA_manager)
    except:
        pass

    assert k5 == None
    assert k1.weight == 0


@pytest.mark.django_db(transaction=False)
def test_delete_kpi_when_other_are_delayed(userAdmin, userA_manager, userA, userB, organization):
    k1 = create_kpi(userA, organization)
    k2 = create_child_kpi(k1, organization.get_current_quarter(), 'New KPI Child 1', userA_manager)
    k3 = create_child_kpi(k1, organization.get_current_quarter(), 'New KPI Child 2', userA_manager)
    k4 = create_child_kpi(k1, organization.get_current_quarter(), 'New KPI Child 3', userA_manager)
    organization.edit_to_date = datetime.datetime.now()
    organization.save()
    assert k1 == k2.refer_to
    '''
    active parent, 2 children were delayed, if delete one child, delay parent
    '''
    '''
    Step 1: Delay 2 of 3 children
    '''
    k2.delay_toggle(user=userA_manager, recursive=False, force_to_state='delay')

    assert k2.weight == 0

    k3.delay_toggle(user=userA_manager, recursive=False, force_to_state='delay')

    assert k3.weight == 0
    '''
    Step 2: delete one left kpi, check status of parent whether active or delayed
    '''
    assert remove_kpi(actor=userA_manager, kpi_id=k4.id, within_recursive=True) == True
    # k4.delete()
    kpi = KPI.objects.get(id=k1.id)

    assert kpi.weight == 0

    assert k2.refer_to_id == kpi.id

    '''
    step 3: we have 1 delayed parent, 2 delayed children. remove one of children, then assert parent kpi should be still delayed
    '''
    assert remove_kpi(actor=userA_manager, kpi_id=k3.id, within_recursive=True) == True
    # k4.delete()
    kpi = KPI.objects.get(id=k1.id)

    assert kpi.weight == 0

    '''
    step 4: reactive parent KPIs, 2 delayed children. remove one of children, then assert parent kpi should be still delayed
    '''
    # k4.delete()
    kpi = KPI.objects.get(id=k1.id)
    kpi.delay_toggle(user=userA_manager, force_to_state='active')
    assert kpi.weight > 0

    kpi2 = KPI.objects.get(id=k2.id)
    assert kpi2.weight > 0


def create_kpi(user, organization, save=True):
    current_quarter = organization.get_current_quarter()
    k = KPI()
    k.user = user
    k.quarter_period = current_quarter
    if save is True:
        k.save()
    return k


@pytest.mark.django_db(transaction=False)
def test_autocomment_when_add_new_kpi(userA, organization):
    k = create_kpi(userA, organization)
    kc = KpiComment.objects.filter(kpi_id=k.id).first()
    assert ugettext("Created KPI") in kc.content


@pytest.mark.django_db(transaction=False)
def test_admin_delete_kpi(userAdmin, userA_manager, userA, userB, organization):
    '''
    test admin/manager can delete kpi of user/subordinate
    :param userAdmin:
    :param userA_manager:
    :param userA:
    :param userB:
    :param organization:
    :return:
    '''

    k1 = create_kpi(userA, organization)
    k2 = create_kpi(userA, organization)
    k3 = create_kpi(userB, organization)
    k4 = create_kpi(userB, organization)

    '''
    admin can delete kpi of any user
    '''
    assert has_perm(DELETE_KPI, userAdmin, userA, k1) is True
    assert has_perm(DELETE_KPI, userAdmin, userB, k3) is True

    '''
    manager can delete kpi of subordinate, but not other.
    '''
    assert has_perm(DELETE_KPI, userA_manager, userA, k1) is True
    assert has_perm(DELETE_KPI, userA_manager, userB, k3) is False


@pytest.mark.django_db(transaction=False)
def test_change_kpi_user(userAdmin, userA_manager_manager, userA_manager, userA, userB, organization):
    '''
    https://cloudjet.atlassian.net/wiki/spaces/CKD/blog/2017/09/14/1634354/Psuedo+workflow+when+assign+kpi+to+user
    :param userAdmin:
    :param userA_manager:
    :param userA:
    :param userB:
    :param organization:
    :return:
    '''
    # # store arguments to local variables to adapt to docs later
    _userAdmin = userAdmin
    _userA_manager_manager = userA_manager_manager
    _userA_manager = userA_manager
    _userA = userA
    _userB = userB

    ##########################################################################################################

    # case1: kpi la con truc tiep
    userA = _userA_manager_manager
    userB = _userA_manager
    kp = create_kpi(userA, organization)
    kpi = create_kpi(userA, organization)
    kpi.parent = kp
    kpi.refer_to = kp
    kpi.save()
    assert kpi.kpi_type_v2() == kpi_con_normal
    assert kp.kpi_type_v2() == kpi_cha_normal

    kpi = kpi.change_kpi_user(userB)

    assert kpi.user == userB
    assert kpi.kpi_type_v2() == kpi_cha_duoc_phan_cong
    assert kpi.cascaded_from.kpi_type_v2() == kpi_trung_gian
    assert kp.kpi_type_v2() == kpi_cha_normal
    #########################################################################################

    # case 2: kpi la con duoc phan cong
    # # case 2.1: kpi.refer_to.user==userB:
    # ## case 2.1.1: kpi has child kc:
    #### case 2.1.1.1: kc.user==userB:
    # this case for case of invalid data that a subordinate assign back kpi to manager
    userB = _userA_manager_manager
    userA = _userA_manager
    kp = create_kpi(userB, organization)
    kpi = kp.assign_to(userA)
    __kp = kpi.cascaded_from

    kc = kpi.assign_to(userB)
    __kpi = kc.cascaded_from
    assert kp.kpi_type_v2() == kpi_cha_normal
    assert __kp.kpi_type_v2() == kpi_trung_gian
    assert kpi.kpi_type_v2() == kpi_cha_duoc_phan_cong
    assert kc.kpi_type_v2() == kpi_cha_duoc_phan_cong and kc.user == userB and kc.cascaded_from.kpi_type_v2() == kpi_trung_gian

    kpi.change_kpi_user(userB)
    # refresh kc data by get from database
    kc = KPI.objects.get(id=kc.id)
    assert __kp.id is not None  # because of cache in variable
    assert __kpi.id is not None  # # because of cache in variable
    assert KPI.objects.filter(id=__kp.id).exists() is False
    assert KPI.objects.filter(id=__kpi.id).exists() is False
    assert kpi.user == userB
    assert kc.kpi_type_v2() == kpi_con_normal
    assert kpi.kpi_type_v2() == kpi_con_normal

    # case 2: kpi la con duoc phan cong
    # # case 2.1: kpi.refer_to.user==userB:
    # ## case 2.1.1: kpi has child kc:
    #### case 2.1.1.2: kc.user==userA ( kc.user!=userB)
    userB = _userA_manager_manager
    userA = _userA_manager
    kp = create_kpi(userB, organization)
    kpi = kp.assign_to(userA)
    __kp = kpi.cascaded_from
    kc = create_kpi(userA, organization)
    kc.parent = kpi
    kc.refer_to = kpi
    kc.save()

    # the following lines we already assert above
    # assert kp.kpi_type_v2() == kpi_cha_normal
    # assert __kp.kpi_type_v2() == kpi_trung_gian
    # assert kpi.kpi_type_v2() == kpi_cha_duoc_phan_cong
    assert kpi.kpi_type_v2() == kpi_cha_duoc_phan_cong
    assert kc.kpi_type_v2() == kpi_con_normal

    kpi.change_kpi_user(userB)
    # __kpi=kc.cascaded_from
    # need get from database
    __kpi = KPI.objects.filter(_cascaded_from__id=kc.id).first()

    # refresh kc by get from database
    kc = KPI.objects.get(id=kc.id)

    assert __kp.id is not None
    assert KPI.objects.filter(id=__kp.id).exists() is False
    assert kpi.user == userB
    assert kpi.kpi_type_v2() == kpi_con_normal
    assert __kpi.kpi_type_v2() == kpi_trung_gian and __kpi.user == userB
    assert kc.kpi_type_v2() == kpi_cha_duoc_phan_cong

    # case 2: kpi la con duoc phan cong
    # # case 2.1: kpi.refer_to.user==userB:
    # ## case 2.1.1: kpi has child kc:
    #### case 2.1.1.3: kc.user=userC
    userB = _userA_manager_manager
    userA = _userA_manager
    userC = _userA
    kp = create_kpi(userB, organization)
    kpi = kp.assign_to(userA)
    __kp = kpi.cascaded_from
    kc = kpi.assign_to(userC)
    __kpi = kc.cascaded_from

    assert kc.kpi_type_v2() == kpi_cha_duoc_phan_cong
    assert kpi.kpi_type_v2() == kpi_cha_duoc_phan_cong
    assert kp.kpi_type_v2() == kpi_cha_normal
    assert __kp.kpi_type_v2() == kpi_trung_gian
    assert __kpi.kpi_type_v2() == kpi_trung_gian

    kpi.change_kpi_user(userB)

    assert KPI.objects.filter(id=__kp.id).exists() is False
    assert kpi.kpi_type_v2() == kpi_con_normal
    assert kc.kpi_type_v2() == kpi_cha_duoc_phan_cong
    assert __kpi.kpi_type_v2() == kpi_trung_gian

    # case 2: kpi la con duoc phan cong
    # # case 2.1: kpi.refer_to.user==userB:
    # ## case 2.1.2: kpi has no child:
    userB = _userA_manager_manager
    userA = _userA_manager
    kp = create_kpi(userB, organization)
    kpi = kp.assign_to(userA)
    __kp = kpi.cascaded_from

    assert kpi.kpi_type_v2() == kpi_cha_duoc_phan_cong
    assert kp.kpi_type_v2() == kpi_cha_normal
    assert __kp.kpi_type_v2() == kpi_trung_gian

    kpi.change_kpi_user(userB)

    assert kpi.kpi_type_v2() == kpi_con_normal
    assert KPI.objects.filter(id=__kp.id).exists() is False

    # case 2: kpi la con duoc phan cong
    # # case 2.2: kpi.refer_to.user!=userB:
    # ## case 2.2.1: kpi has child kc:
    #### case 2.2.1.1: kc.user==userB:

    userC = _userA_manager_manager
    userA = _userA_manager
    userB = _userA
    kp = create_kpi(userC, organization)
    kpi = kp.assign_to(userA)
    __kp = kpi.cascaded_from
    kc = kpi.assign_to(userB)
    __kpi = kc.cascaded_from

    kpi.change_kpi_user(userB)

    # refresh kc by get form db
    kc = KPI.objects.get(id=kc.id)

    assert kc.kpi_type_v2() == kpi_con_normal
    assert kpi.kpi_type_v2() == kpi_cha_duoc_phan_cong
    assert KPI.objects.filter(id=__kpi.id).exists() is False

    # case 2: kpi la con duoc phan cong
    # # case 2.2: kpi.refer_to.user!=userB:
    # ## case 2.2.1: kpi has child kc:
    #### case 2.2.1.2: kc.user==userA ( kc.user!=userB)

    userC = _userA_manager_manager
    userA = _userA_manager
    userB = _userA
    kp = create_kpi(userC, organization)
    kpi = kp.assign_to(userA)
    __kp = kpi.cascaded_from
    kc = create_kpi(userA, organization)
    kc.parent = kpi
    kc.refer_to = kpi
    kc.save()

    assert kc.kpi_type_v2() == kpi_con_normal
    assert kpi.kpi_type_v2() == kpi_cha_duoc_phan_cong

    kpi.change_kpi_user(userB)

    # refresh kc data from db
    kc = KPI.objects.get(id=kc.id)
    __kpi = kc.cascaded_from
    assert kc.kpi_type_v2() == kpi_cha_duoc_phan_cong
    assert kpi.kpi_type_v2() == kpi_cha_duoc_phan_cong and kpi.user == userB
    assert __kpi.kpi_type_v2() == kpi_trung_gian

    # case 2: kpi la con duoc phan cong
    # # case 2.2: kpi.refer_to.user!=userB:
    # ## case 2.2.1: kpi has child kc:
    #### case 2.2.1.3: kc.user=userD
    userC = _userA_manager_manager
    userA = _userA_manager
    userB = _userA
    userD = _userB
    kp = create_kpi(userC, organization)
    kpi = kp.assign_to(userA)
    __kp = kpi.cascaded_from
    kc = kpi.assign_to(userD)
    __kpi = kc.cascaded_from

    assert kc.kpi_type_v2() == kpi_cha_duoc_phan_cong

    kpi.change_kpi_user(userB)

    # refresh __kpi data by get from database
    __kpi = KPI.objects.filter(_cascaded_from__id=kc.id).first()

    assert kpi.kpi_type_v2() == kpi_cha_duoc_phan_cong and kpi.user == userB
    assert kc.kpi_type_v2() == kpi_cha_duoc_phan_cong
    assert __kpi.kpi_type_v2() == kpi_trung_gian
    assert __kpi.user == userB

    # case 2: kpi la con duoc phan cong
    # # case 2.2: kpi.refer_to.user!=userB:
    # ## case 2.2.2: kpi has no child:
    userC = _userA_manager_manager
    userA = _userA_manager
    userB = _userA
    kp = create_kpi(userC, organization)
    kpi = kp.assign_to(userA)
    __kp = kpi.cascaded_from

    kpi.change_kpi_user(userB)

    assert kpi.kpi_type_v2() == kpi_cha_duoc_phan_cong  and kpi.user == userB

    # case 3: kpi is parent-kpi, that not a child of other #kpi la kpi cha (khong la con cua kpi khac)
    # # case 3.1: kpi has child: kc
    # ## case 3.1.1: kc.user == userA:
    userA = _userA_manager_manager
    userB = _userA_manager
    kpi = create_kpi(userA, organization)
    kc = create_kpi(userA, organization)
    kc.parent = kpi
    kc.refer_to = kpi
    kc.save()
    assert kc.kpi_type_v2() == kpi_con_normal
    assert kpi.kpi_type_v2() == kpi_cha_normal

    kpi.change_kpi_user(userB)
    # refresh kc data by get from db
    kc = KPI.objects.get(id=kc.id)
    __kpi = kc.cascaded_from
    assert kc.kpi_type_v2() == kpi_cha_duoc_phan_cong and __kpi.user == userB
    assert kpi.kpi_type_v2() == kpi_cha_normal and kpi.user == userB

    # case 3: kpi is parent-kpi, that not a child of other #kpi la kpi cha (khong la con cua kpi khac)
    # # case 3.1: kpi has child: kc
    # ## case 3.1.2: kc.user == userB
    userA = _userA_manager_manager
    userB = _userA_manager
    kpi = create_kpi(userA, organization)
    kc = kpi.assign_to(userB)
    __kpi = kc.cascaded_from

    kpi.change_kpi_user(userB)
    # refresh kc data by get from db
    kc = KPI.objects.get(id=kc.id)
    assert kc.kpi_type_v2() == kpi_con_normal
    assert KPI.objects.filter(id=__kpi.id).exists() is False

    # case 3: kpi is parent-kpi, that not a child of other #kpi la kpi cha (khong la con cua kpi khac)
    # # case 3.1: kpi has child: kc
    # ## case 3.1.3: kc.user == userC
    userA = _userA_manager_manager
    userB = _userA_manager
    userC = _userA
    kpi = create_kpi(userA, organization)
    kc = kpi.assign_to(userC)
    __kpi = kc.cascaded_from

    kpi.change_kpi_user(userB)

    # refresh __kpi data by get from db
    __kpi = KPI.objects.filter(_cascaded_from__id=kc.id).first()

    assert kpi.user == userB and __kpi.user == userB

    # case 3: kpi is parent-kpi, that not a child of other #kpi la kpi cha (khong la con cua kpi khac)
    # # case 3.2: kpi has no child:
    userA = _userA_manager_manager
    userB = _userA_manager
    kpi = create_kpi(userA, organization)
    kpi.change_kpi_user(userB)
    assert kpi.user == userB
    pass


@pytest.mark.django_db(transaction=False)
def test_review_month_3(userA_manager_manager, userA_manager, organization):
    '''
    test when update month_3 of kpi, values of month_1_score & month_2_score still reserved
    '''

    def init_kpi_data(k):
        kpi_data = {'is_started': True, 'code': u'', 'hash': u'294b49ba5920c60af5fed2cc880c96e5e3c28339', 'score_calculation_type': u'most_recent', 'weight': 7.0, 'unique_key': u'697e157e-3fb1-11e7-8bb1-72c38066bec9', 'unique_code': None, 'last_email_sent': None, 'year': 0, 'prefix_id': u'01.02.', 'owner_email': u'tailv@demo.cjs.vn',
                  # 'refer_to': 129086,
                  # 'cascaded_from': 129089,
                  'self_confirmed': False, 'current_goal': u'B\xc1O C\xc1O THANH QUY\u1ebeT TO\xc1N H\xc0NG QU\xdd', 'operator': u'>=', 'reviewer': None, 'data_input_approved': False,
                  # u'id': 129088,

                  'unit': u'L\u1ea7n',
                  'month_1_target': 0.0, 'month_2_target': 0.0,

                  'in_library': False, 'group': u'',
                  # 'quarter_period': 7,

                  'temp_weight': 0.0, 'ordering': 1.0, 'quarter_two_target': 0.0, 'outcome_notes': u'', 'default_real': None,
                  'month_1_score': 100.0,
                  'quarter_one_target': None, 'score_calculation_automation': False, 'month_3': 0.0, 'review_type': u'monthly', 'year_target': None,

                  'start_date': None, 'month_3_target': 0.0, 'month_1': 0.0, 'real': 0.0, 'map': None, 'current_result': None, 'end_date': None, 'description': u'', 'parent': None, 'last_qc_check': None, 'approval_status': u'', 'quarter_four_target': 1.0,

                  # 'user': 167,

                  'quarter_three_target': None, 'manager_confirmed': False, 'reviewer_email': None, 'is_private': False, 'name': u'\u0110\u1ea2M B\u1ea2O HO\u1ea0T \u0110\u1ed8NG KINH DOANH DVSC C\xd3 L\u1ee2I NHU\u1eacN >= 10%', 'archivable_score': None, 'is_owner': True, 'target': 0.0, 'future_goal': u'', 'month_3_score': 100.0, 'outcome_notes_self': u'',
                  # 'copy_from': None,
                  'real_end_date': None, 'bsc_category': u'financial', 'status': u'not_started', 'assigned_to': None, 'refer_group_name': u'L\u1ee2I NHU\u1eacN', 'month_2': 0.0, 'quarter': 1, 'month_2_score': 100.0,
    'log_trace': u'',
    'latest_score': 100.0}

        update_keys = kpi_data.keys()
        for key, value in kpi_data.items():
            if hasattr(k, key):
                setattr(k, key, value)

        k.save()
        return k

    k1 = create_kpi(userA_manager, organization)

    init_kpi_data(k1)

    assert k1.month_1_score > 0
    assert k1.month_2_score > 0
    assert k1.month_3 == 0

    # store some orignial values
    old_month_1_score = k1.month_1_score
    old_month_2_score = k1.month_2_score
    # update month_3
    k1.month_3 = 2
    update_score(k1, userA_manager_manager, k1.month_1, k1.month_2, k1.month_3,
                       k1.real)

    # refresh k1 data by get directly from     database
    k1 = KPI.objects.get(id=k1.id)

    assert k1.month_3 == 2
    assert k1.month_1_score > 0 and k1.month_1_score == old_month_1_score
    assert k1.month_2_score > 0 and k1.month_2_score == old_month_2_score


@pytest.mark.django_db(transaction=False)
def test_create_interchange_kpi_v2(userA, userB, organization):
    k1 = create_kpi(userA, organization)
    k2 = create_kpi(userB, organization)
    interchange_k = KPI.create_interchange_kpi_v2(k1, k2, to_user=userB)
    assert interchange_k.kpi_type_v2() == kpi_trung_gian
    assert k1.kpi_type_v2() == kpi_cha_normal
    assert k2.kpi_type_v2() == kpi_cha_duoc_phan_cong
    assert interchange_k.refer_to is None


@pytest.mark.django_db(transaction=False)
def test_get_children_visible(userA_manager_manager, userA_manager, userA, organization):
    k1 = create_kpi(userA_manager_manager, organization)
    k2 = create_kpi(userA_manager, organization)

    k11 = create_kpi(userA_manager_manager, organization)
    k11.parent = k1
    k11.refer_to = k1
    k11.save()
    k12 = k1.assign_to(userA_manager)
    k13 = k1.assign_to(userA)

    assert k11.kpi_type_v2() == kpi_con_normal
    assert k12.kpi_type_v2() == kpi_cha_duoc_phan_cong
    assert k13.kpi_type_v2() == kpi_cha_duoc_phan_cong

    k21 = k2.assign_to(userA)

    k22 = create_kpi(userA_manager, organization)
    k22.parent = k2
    k22.refer_to = k2
    k22.save()
    assert k22.kpi_type_v2() == kpi_con_normal
    assert k21.kpi_type_v2() == kpi_cha_duoc_phan_cong
    assert k21.kpi_type() == 'chacongiantiep'

    # check the number of childs that should be visible on frontend
    assert k1.get_children_visible().count() == 3
    # check the number of childs that has parent = k1
    assert k1.get_children().count() == 3

    # check the number of childs that should be visible on frontend
    assert k2.get_children_visible().count() == 2
    # check the number of childs that has parent = k2
    assert k2.get_children().count() == 2

    assert (k21.cascaded_from in k2.get_children()) and (k21 not in k2.get_children()) and (k21 in k2.get_children_visible())
    assert k22 in k2.get_children() and k22 in k2.get_children_visible()

    pass


@pytest.mark.django_db(transaction=False)
def test_postponed_kpi(userA, organization):
    kpi = create_kpi(userA, organization)
    assert update_score(kpi, userA, 10, 10, 10, 9) is not None

    kpi.weight = 0
    kpi.save()
    with pytest.raises(PermissionDenied) as excinfo:
        update_score(kpi, userA, 10, 10, 10, 9)


@pytest.mark.django_db(transaction=False)
def test_review_kpi_for_each_month(userA, organization, client_authed):
    # Test KPI no any children
    k = KPI()
    k.quarter_period = organization.get_current_quarter()
    k.quarter_one_target = 10
    k.quarter_two_target = 10
    k.quarter_three_target = 10
    k.quarter_four_target = 10
    k.user = userA
    k.month_1_target = 10
    k.month_2_target = 10
    k.month_3_target = 10
    k.save()

    url = reverse('kpi_update_score')
    data = to_json(k)
    # Review month 1
    data['month_1'] = 10
    res = client_authed.post(url, data)
    assert res.status_code == 200
    k = reload_model_obj(k)
    assert k.month_1_score is not None
    assert k.month_2_score is None
    assert k.month_3_score is None

    # Review month 2
    data = to_json(k)
    data['month_2'] = 0
    res = client_authed.post(url, data)
    assert res.status_code == 200
    k = reload_model_obj(k)
    assert k.month_1_score is not None
    assert k.month_2_score is not None
    assert k.month_3_score is None

    # Review month 3
    data = to_json(k)
    data['month_3'] = 10
    res = client_authed.post(url, data)
    assert res.status_code == 200
    k = reload_model_obj(k)
    assert k.month_1_score is not None
    assert k.month_2_score is not None
    assert k.month_3_score is not None

    # Test KPI has children
    p = KPI()
    p.quarter_period = organization.get_current_quarter()
    p.quarter_one_target = 10
    p.quarter_two_target = 10
    p.quarter_three_target = 10
    p.quarter_four_target = 10
    p.user = userA
    p.save()

    k = KPI()
    k.quarter_period = organization.get_current_quarter()
    k.quarter_one_target = 10
    k.quarter_two_target = 10
    k.quarter_three_target = 10
    k.quarter_four_target = 10
    k.user = userA
    k.parent = p
    k.month_1_target = 10
    k.month_2_target = 10
    k.month_3_target = 10
    k.save()

    data = to_json(k)
    data['month_1'] = 10
    res = client_authed.post(url, data)
    assert res.status_code == 200
    calculate_score_cascading(p.id)
    p = reload_model_obj(p)
    assert p.month_1_score is not None
    assert p.month_2_score is None
    assert p.month_3_score is None

    k = reload_model_obj(k)
    data = to_json(k)
    data['month_2'] = 0
    res = client_authed.post(url, data)
    assert res.status_code == 200
    calculate_score_cascading(p.id)
    p = reload_model_obj(p)
    assert p.month_1_score is not None
    assert p.month_2_score is not None
    assert p.month_3_score is None

    k = reload_model_obj(k)
    data = to_json(k)
    data['month_3'] = 10
    res = client_authed.post(url, data)
    assert res.status_code == 200
    calculate_score_cascading(p.id)
    p = reload_model_obj(p)
    assert p.month_1_score is not None
    assert p.month_2_score is not None
    assert p.month_3_score is not None


@pytest.mark.django_db(transaction=False)
def test_add_kpi_disabled_user(userB, organization, client_admin):
    url = reverse('inactive_user')
    data = {
        'reason': 'Nghi viec',
        'user_id': userB.id
    }
    res = client_admin.post(url, data)
    assert res.status_code == 200
    assert userB.profile.active is False

    url = reverse('kpi_editor_emp', kwargs={"user_id": userB.id})
    data = {
        'level': 1,
        'new_template': True,
        'category': 'financial'
    }
    res = client_admin.post(url, data)
    assert res.status_code == 403


@pytest.mark.django_db(transaction=False)
def test_assign_kpi_disabled_user(userA, organization, client_admin):
    k = KPI(name="KPI name")
    k.user = userA
    k.quarter_period = organization.get_current_quarter()
    k.save()

    k1 = KPI(name="KPI name")
    k1.user = userA
    k1.parent = k
    k1.quarter_period = organization.get_current_quarter()
    k1.save()

    usertmp = User(username="usertmp", email="tempuser@gmail.com")
    usertmp.save()

    profile = usertmp.profile
    profile.parent = userA.profile
    profile.organization = organization
    profile.save()

    url = reverse('inactive_user')
    data = {
        'reason': 'Nghi viec',
        'user_id': usertmp.id
    }
    res = client_admin.post(url, data)
    assert res.status_code == 200
    assert usertmp.profile.active is False

    data = {
        'obj_id': k1.id,
        'adaptor': 'kpi_assign_user',
        'value': u'"{}"'.format(usertmp.email),
        'app_label': 'performance',
        'module_name': 'kpi',
        'field_name': 'owner_email',
        'edit_empty_value': '',
        'autosave': 1
    }
    res = client_admin.post('/inplaceeditform/save/', data)
    assert res.status_code == 403


@pytest.mark.django_db(transaction=False)
def test_prefix_id_when_save_kpi(userA_manager, userA, parent_kpi, kpi, organization):

    kp = create_kpi(userA_manager, organization)
    k1 = kp.assign_to(userA)

    kp.ordering = 1000
    # this save() will update prefix_id of all children ==> k1.prefix_id changed
    kp.save()
    fresh_k1 = KPI.objects.get(id=k1.id)
    assert KPI.objects.get(id=kp.id).ordering == 1000
    assert fresh_k1.prefix_id == '1000.'

    # change k1 ordering to 999
    fresh_k1.ordering = 999
    fresh_k1.save()

    # create new kpi ==> prefix_id must be set
    k11 = create_kpi(userA, organization, save=False)
    k11.parent = fresh_k1
    k11.refer_to = fresh_k1
    k11.save()
    fresh_k11 = KPI.objects.get(id=k11.id)

    assert fresh_k11.prefix_id == '1000.999.'


@pytest.mark.django_db(transaction=False)
def test_MoveToExistPosition_404(organization, userB, userA, client_admin):
    url = reverse('move-to-exist-position')
    data = {
        'user_id': userB.id,
        'to_user': userA.id
    }
    p = userA.profile
    p.delete()

    res = client_admin.post(url, json.dumps(data), content_type="application/json")
    assert res.status_code == 404


@pytest.mark.django_db(transaction=False)
def test_MoveToExistPosition_403(organization, userB, userA, client_admin):
    url = reverse('move-to-exist-position')
    data = {
        'user_id': userB.id,
        'to_user': userA.id
    }
    p = userB.profile
    p.delete()

    res = client_admin.post(url, json.dumps(data), content_type="application/json")
    assert res.status_code == 403


@pytest.mark.django_db(transaction=False)
def test_get_kpi_by_kwargs_id(kpi):
    k = get_kpi_by_kwargs(id=kpi.id)
    assert k.id == kpi.id


@pytest.mark.django_db(transaction=False)
def test_get_kpi_by_kwargs_unique_key(organization, kpi):
    quarter_period = organization.get_current_quarter()
    k = get_kpi_by_kwargs(unique_key=kpi.unique_key,
                          quarter_period=quarter_period)
    assert k.id == kpi.id
