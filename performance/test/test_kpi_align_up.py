# !/usr/bin/python
# -*- coding: utf-8 -*-
# def test_kpi_toggle(kpi):
#     pass
from datetime import datetime
import time
# from faker import Factory
from django.contrib.auth.models import User
from django.core.cache import cache
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.utils.translation import ugettext

from performance.base.kpi_type import *
# from performance.cloner import PerformanceCloner
from performance.services import calculate_weighted_weight, calculate_weighted_weight_by_kpi
from performance.services.CascadeKPIQueue import is_queued
import pytest

# faker = Factory.create()
from performance.models import KPI, QuarterPeriod, CascadeKPIQueue

# import os
#
# def include(filename):
#     if os.path.exists(filename):
#         execfile(filename)
#
#
# include('../../conftest_init.py')
from types import FunctionType
from user_profile.services.user import create_user
from utils.caching_name import CREATING_ASSESSMENT, CREATING_ASSESSMENT_ORG
from utils.random_str import random_string
from conftest import userA_manager, userA_manager_manager, userA
from django.core.urlresolvers import reverse
import json
from django.test.client import Client
from performance.test.test_kpi import create_kpi
from utils import reload_model_obj
import datetime

@pytest.mark.django_db(transaction=False)
def test_alignup(userA, userB, organization):
    k1 = KPI()
    k1.user = userB
    k1.quarter_period = organization.get_current_quarter()
    k1.quarter_one_target = 10
    k1.target = 10

    k1.save()

    k2 = KPI()
    k2.user = userA
    k2.quarter_one_target = 10
    k2.target = 10
    k2.save()

    assert k1.get_children().count() == 0

    k2.assign_up(k1)

    assert k1.get_children().count() == 1
    assert k2.kpi_type_v2() == kpi_cha_duoc_phan_cong
    assert k2.cascaded_from.kpi_type_v2() == kpi_trung_gian
    assert k1.kpi_type_v2() == kpi_cha_normal

    assert k1.fix_self(use_cache=False) == False
    assert k2.cascaded_from.fix_self(use_cache=False) == False
    assert k2.fix_self(use_cache=False) == False

    assert k1.get_children().count() == 1


@pytest.mark.django_db(transaction=False)
def test_alignup_postponed_kpi(userA, userA_manager, client_authed, organization):
    current_quarter = organization.get_current_quarter()
    k = KPI(name="KPI K")
    k.user = userA_manager
    k.quarter_period = current_quarter
    k.bsc_category = 'financial'
    k.weight = 10
    k.quarter_one_target = 10
    k.target = 10
    k.save()

    k1 = KPI(name="KPI K1")
    k1.user = userA_manager
    k1.bsc_category = 'financial'
    k1.quarter_period = current_quarter
    k1.weight = 0
    k1.quarter_one_target = 10
    k1.target = 10

    k1.save()

    k2 = KPI(name="KPI K2")
    k2.user = userA
    k.weight = 10
    k2.bsc_category = 'financial'
    k2.quarter_period = current_quarter
    k2.quarter_one_target = 10
    k2.target = 10
    k2.save()

    url = "{0}?user_id={1}&bsc_category={2}".format(reverse('align-up-kpi'), userA_manager.id, 'financial')
    response = client_authed.get(url)
    assert response.status_code == 200
    # print response.content
    assert len(json.loads(response.content)) == KPI.objects.filter(user=userA_manager,
                                                                   quarter_period=current_quarter,
                                                                   weight__gt=0).count()
    with pytest.raises(PermissionDenied) as e:
        k2.assign_up(k1)


@pytest.mark.django_db(transaction=False)
def test_align_and_unlink_kpi(organization, userA, userA_manager, userA_manager_manager,
                              client_authed, client_userA_manager, client_userA_manager_manager, client_ceo):

    k1 = create_kpi(userA_manager_manager, organization)
    k1.bsc_category = 'financial'
    k1.save()

    k2 = create_kpi(userA_manager, organization)
    k2.bsc_category = 'financial'
    k2.save()

    # Test align up
    data = {
        'user_id': userA_manager.id,
        'kpi_id': k1.id,
        'aligned_kpi_id': k2.id
    }
    res = client_userA_manager.post(reverse("align-up-kpi"), data)
    assert res.status_code == 200
    content = json.loads(res.content)

    k2 = reload_model_obj(k2)
    assert k2.cascaded_from_id == content['cascaded_from']

    # Test unlink kpi 403
    data = {
        'user_id': userA_manager.id,
        'aligned_kpi_id': k2.id
    }
    res = client_authed.post(reverse("align-up-kpi"), data)
    assert res.status_code == 403

    # test unlink 200
    data = {
        'user_id': userA_manager.id,
        'aligned_kpi_id': k2.id
    }
    res = client_userA_manager.post(reverse("align-up-kpi"), data)
    assert res.status_code == 200


@pytest.mark.django_db(transaction=False)
def test_alignup_kpi_1(organization, userA, userA_manager, userA_manager_manager,
                              client_authed, client_userA_manager, client_userA_manager_manager):

    k2 = create_kpi(userA_manager, organization)
    k2.bsc_category = 'financial'
    k2.save()

    '''
        Case 403 when manager edit with enable_edit = False
    '''

    organization.enable_edit = False
    organization.save()
    link = reverse('align-up-kpi')
    data = {
        'user_id': userA_manager.id,
        'aligned_kpi_id': k2.id
    }
    response = client_userA_manager.post(link, data)
    assert response.status_code == 403

@pytest.mark.django_db(transaction=False)
def test_alignup_kpi_2(organization, userA_manager, client_userA_manager):

    new_kpi = KPI(name="New KPI",
                  quarter_period=organization.get_current_quarter(),
                  quarter_one_target=10,
                  quarter_two_target=2,
                  quarter_three_target=5,
                  quarter_four_target=10,
                  score_calculation_type='sum'
                  )
    new_kpi.save()

    k3 = create_kpi(userA_manager, organization)
    k3.bsc_category = 'financial'
    k3.cascaded_from = new_kpi
    k3.save()
    link = reverse('align-up-kpi')
    data = {
        'user_id': userA_manager.id,
        'aligned_kpi_id': k3.id,
    }
    response = client_userA_manager.post(link, data)
    assert response.status_code == 200
    res = json.loads(response.content)
    assert res['cascaded_from'] is None
    assert res['category'] == 'financial'


@pytest.mark.django_db(transaction=False)
def test_alignup_kpi_3(organization, userA_manager, client_userA_manager, userA):
    kpi_0 = KPI(user=userA_manager,
                name="New KPI 0",
                quarter_period=organization.get_current_quarter(),
                score_calculation_type='sum',
                target=10,
                )
    kpi_0.save()
    kpi_1 = KPI(user=userA_manager,
                name="New KPI 1",
                quarter_period=organization.get_current_quarter(),
                score_calculation_type='sum',
                target=10,
                )
    kpi_1.save()
    kpi_2 = KPI(user=userA_manager,
                name="New KPI 2",
                quarter_period=organization.get_current_quarter(),
                score_calculation_type='sum',
                target=10,
                )
    kpi_2.save()
    kpi_3 = KPI(user=userA_manager,
                name="New KPI 3",
                quarter_period=organization.get_current_quarter(),
                score_calculation_type='sum',
                target=10,
                bsc_category='financial'
                )
    kpi_3.save()
    kpi_4 = KPI(user=userA_manager,
                name="New KPI 4",
                quarter_period=organization.get_current_quarter(),
                score_calculation_type='sum',
                target=10,
                )
    kpi_4.save()

    kpi_1.refer_to = kpi_0
    kpi_1.weight = 0
    kpi_1.save()

    kpi_2.refer_to = kpi_0
    kpi_2.weight = 0
    kpi_2.save()

    kpi_0.weight = 0
    kpi_0.save()

    kpi_3.cascaded_from = kpi_4
    kpi_3.refer_to = kpi_0
    kpi_3.save()

    link = reverse('align-up-kpi')
    data = {
        'user_id': userA_manager.id,
        'aligned_kpi_id': kpi_3.id
    }
    response = client_userA_manager.post(link, data)
    assert response.status_code == 200
    res = json.loads(response.content)
    assert res['cascaded_from'] is None
    assert res['category'] == 'financial'


@pytest.mark.django_db(transaction=False)
def test_alignup_kpi_4(organization, userA_manager, client_userA_manager, userA):
    kpi_0 = KPI(user=userA_manager,
                name="New KPI 0",
                quarter_period=organization.get_current_quarter(),
                score_calculation_type='sum',
                target=10,
                )
    kpi_0.save()
    kpi_1 = KPI(user=userA_manager,
                name="New KPI 1",
                quarter_period=organization.get_current_quarter(),
                score_calculation_type='sum',
                target=10,
                )
    kpi_1.save()

    kpi_0.cascaded_from = kpi_1
    kpi_0.weight = 0
    kpi_0.save()
    link = reverse('align-up-kpi')
    data = {
        'kpi_id': kpi_1.id,
        'user_id': userA_manager.id,
        'aligned_kpi_id': kpi_0.id
    }
    response = client_userA_manager.post(link, data)
    res = json.loads(response.content)
    assert response.status_code == 403
    assert res['message'] == ugettext("KPI was delayed.")


@pytest.mark.django_db(transaction=False)
def test_alignup_kpi_5(organization, userA_manager, client_userA_manager, userA):
    kpi_0 = KPI(user=userA_manager,
                name="New KPI 0",
                quarter_period=organization.get_current_quarter(),
                score_calculation_type='sum',
                target=10
                )
    kpi_0.save()
    kpi_1 = KPI(user=userA_manager,
                name="New KPI 1",
                quarter_period=organization.get_current_quarter(),
                score_calculation_type='sum',
                target=10,
                )
    kpi_1.save()

    kpi_2 = KPI(user=userA_manager,
                name="New KPI 1",
                quarter_period=organization.get_current_quarter(),
                score_calculation_type='sum',
                target=10,
                bsc_category='financial'
                )
    kpi_2.save()

    kpi_0.cascaded_from = kpi_1
    kpi_0.save()
    link = reverse('align-up-kpi')
    data = {
        'kpi_id': kpi_2.id,
        'user_id': userA_manager.id,
        'aligned_kpi_id': kpi_0.id
    }
    response = client_userA_manager.post(link, data)
    res = json.loads(response.content)
    assert response.status_code == 200
    assert res['category'] == 'financial'

    '''
        Case 403 when assign_up
    '''
    kpi_2.weight = 0
    kpi_2.save()
    link = reverse('align-up-kpi')
    data = {
        'kpi_id': kpi_2.id,
        'user_id': userA_manager.id,
        'aligned_kpi_id': kpi_0.id
    }
    response = client_userA_manager.post(link, data)
    res = json.loads(response.content)
    assert response.status_code == 403
    assert res['message'] == ugettext("You can't link to the postponed KPI of your manager")
