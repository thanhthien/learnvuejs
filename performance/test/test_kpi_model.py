# !/usr/bin/python
# -*- coding: utf-8 -*-
# def test_kpi_toggle(kpi):
#     pass
import datetime
import time

# from faker import Factory
from random import randint
from django.core.cache import cache
from django.test.utils import override_settings
from django.contrib.auth.models import User
from django.core.cache import cache
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from performance.base.kpi_type import *
# from performance.cloner import PerformanceCloner
from performance.services import calculate_weighted_weight, calculate_weighted_weight_by_kpi, create_child_kpi, \
    calculate_score
from performance.services.CascadeKPIQueue import is_queued
import pytest
# system_user = get_system_user()
# faker = Factory.create()
from performance.models import KPI, QuarterPeriod, CascadeKPIQueue, get_children_refer_order, log_user_score_all, \
    ScoreLog
from django.utils.dateformat import format
# import os
#
# def include(filename):
#     if os.path.exists(filename):
#         execfile(filename)
#
#
# include('../../conftest_init.py')
from types import FunctionType

from performance.tasks import start_cloner_in_celery
from user_profile.models import get_kpis_parent_by_user
from user_profile.services.user import create_user
from utils.caching_name import CREATING_ASSESSMENT, CREATING_ASSESSMENT_ORG
from utils.common import remove_redis
from utils.random_str import random_string
from django.core.exceptions import PermissionDenied


@pytest.mark.django_db(transaction=False)
def test_log_user_score_all(organization, userA, kpi):
    result = ScoreLog.objects.filter(user=userA)
    assert len(result) == 0

    quarter = organization.get_current_quarter()
    log_user_score_all(userA.id, quarter.id)
    result = ScoreLog.objects.filter(user=userA)
    assert len(result) == 1

    log_user_score_all(userA.id, quarter_period_id=None)
    result = ScoreLog.objects.filter(user=userA)
    assert len(result) == 2

    result = log_user_score_all(user_id=None, quarter_period_id=quarter.id)
    assert result is False

    random_id = randint(1, 10000)
    if userA.id != random_id:
        result = log_user_score_all(user_id=random_id, quarter_period_id=quarter.id)
        assert result is False


@pytest.mark.django_db(transaction=False)
def test_calculate_weighted_weight_by_kpi():
    # print user.username
    user = User()
    user.email = 'userX' + '@gmail.com'
    user.username = 'userX' + 'username'

    user.save()

    pA = user.profile
    pA.save()
    kpi1 = KPI()
    kpi1.user = user
    kpi1.weight = 5
    kpi1.save()
    # assert calculate_weighted_weight_by_kpi(kpi1) == 100
    kpi1.get_weighted_weight() == 100

    kpi2 = KPI()
    kpi2.user = user
    kpi2.weight = 5
    kpi2.save()
    # assert calculate_weighted_weight_by_kpi(kpi2) == 50
    kpi2.get_weighted_weight() == 50
    # assert calculate_weighted_weight_by_kpi(kpi1) == 50

#
# @pytest.mark.django_db(transaction=False)
# def test_delay_toggle_advance(userAdmin, ceo, userA_manager_manager, userA_manager, userA, organization):
#     '''
#     test document: https://cloudjet.atlassian.net/wiki/spaces/CKD/pages/2589435/Delay+active+kpi+v3+--+test+cases
#     related docs:
#         https://cloudjet.atlassian.net/wiki/spaces/CKD/pages/2490734/Delay+active+kpi+v3
#         https://cloudjet.atlassian.net/wiki/spaces/CKD/pages/2622059/delay+active+kpi+v3+Advance
#         https://cloudjet.atlassian.net/wiki/spaces/CKD/pages/2425406/Delay+active+kpi+v3+--+Usage
#     :param kpi:
#     :return:
#     '''
#     _ceo = ceo
#     _userA_manager_manager = userA_manager_manager
#     _userA_manager = userA_manager
#     _userA = userA
#
#     userA = _ceo
#     userB = _userA_manager_manager
#     userC = _userA_manager
#     userD = _userA
#     admin = userAdmin
#
#     organization = organization
#
#     def create_kpi(user, organization):
#         current_quarter = organization.get_current_quarter()
#         k = KPI()
#         k.user = user
#         k.quarter_period = current_quarter
#         k.save()
#         return k
#
#     def set_parent(kc, kp):
#         kc.parent = kp
#         kc.refer_to = kp
#         kc.save()
#         return kc
#
#     def create_directly_child(kp):
#         kc = create_kpi(kp.user, organization)
#         kc = set_parent(kc, kp)
#         return kc
#
#     def set_to_state(k, to_state):
#         if to_state == 'active':
#             k.weight = k.temp_weight if k.temp_weight > 0 else 10
#         else:  # to_state == 'delay':
#             k.temp_weight = k.weight if k.weight > 0 else 10
#             k.weight = 0
#         k.save()
#         return k
#
#     def refresh_kpi(k):
#         return KPI.objects.get(id=k.id)
#
#     def refresh_kpis(ks):
#         r = []
#         for k in ks:
#             r.append(refresh_kpi(k))
#         return tuple(r)
#
#     def create_kpi_tree():
#         '''
#         Sample kpi structure:
#
#                         + userA > userB > userC > userD
#                         + delay/active kx
#                                                               kp(userA)
#                                                            ,-'     -   `-.._
#                                                        _.-'         `.      `-.._
#                                                 [__kx(userA)]  [__ky(userA)]     `-.._
#                                                 ,-'     `.               `.           `-.._
#                                              _,'          `.               -_              `--
#                                           kx(userB)         `.          ky(userC)          kz(userA)
#                                         ,-    \               `.
#                                       ,'       \                `_
#                                     ,'    [__k1(userB)]    [__k2(userB)]
#                                   ,'             \                   .
#                                _,'                \                   `.
#                             k3(userB)          k1(userC)             k2(userC)
#                                                 ,'    `..
#                                               ,'         `-.
#                                      [__k11(userC)]    [__k1'(userD)]
#                                         ,'                     .
#                                       _'                        `-.
#                                k11(userD)                      k12(`userD)
#
#         '''
#         kp = create_kpi(userA, organization)
#         kx = kp.assign_to(userB)
#         ky = kp.assign_to(userC)
#         kz = create_directly_child(kp)
#         k3 = create_directly_child(kx)
#         k1 = kx.assign_to(userC)
#         k2 = kx.assign_to(userC)
#         k11 = k1.assign_to(userD)
#         k12 = k1.assign_to(userD)
#
#         return kp, kx, ky, kz, k1, k2, k3, k11, k12
#
#     # kpi = kpi.delay_toggle(kpi.user)
#     # assert kpi.weight == 0
#     #
#     # kpi = kpi.delay_toggle(kpi.user)
#     # assert kpi.weight > 0
#     #
#     # kpi = kpi.delay_toggle(kpi.user)
#     # assert kpi.weight == 0
#
#     # create kpi tree with all kpis are active
#     kp, kx, ky, kz, k1, k2, k3, k11, k12 = create_kpi_tree()
#
#     # 1. force_to_state
#     ## 1.1: force_to_state = None:
#     assert kx.weight > 0
#     kx.delay_toggle_advance(admin, force_to_state=None)
#
#     assert kx.weight == 0
#
#     # 1. force_to_state
#     ## 1.2: force_to_state = 'delay':
#     ### 1.2.1: kx.state=='delay':
#
#     # create kpi tree with all kpis are active
#     kp, kx, ky, kz, k1, k2, k3, k11, k12 = create_kpi_tree()
#
#     kx = set_to_state(kx, 'delay')
#     assert k3.weight > 0 and kx.weight == 0
#     kx.delay_toggle_advance(admin, force_to_state='delay')
#
#     k3 = refresh_kpi(k3)
#     assert k3.weight == 0
#
#     # 1. force_to_state
#     ## 1.3: force_to_state = 'active':
#     ### 1.3.2: kx.state=='active':
#     kp, kx, ky, kz, k1, k2, k3, k11, k12 = create_kpi_tree()
#     k1 = set_to_state(k1, 'delay')
#     assert kx.weight > 0 and k1.weight == 0
#
#     kx.delay_toggle_advance(admin, force_to_state='active')
#     k1 = refresh_kpi(k1)
#     assert k1.weight > 0
#
#     # 2. recursive:
#     ## 2.1.: recursive == False:
#
#     kp, kx, ky, kz, k1, k2, k3, k11, k12 = create_kpi_tree()
#     # given kx, k3 is active; k1 is delay
#     k1 = set_to_state(k1, 'delay')
#     assert k1.weight == 0
#
#     kx.delay_toggle_advance(admin, recursive=False)
#     k3 = refresh_kpi(k3)
#     assert kx.weight == 0 and k3.weight > 0
#
#     # 2. recursive:
#     ## 2.1.: recursive == False:
#     ### 2.2.1: down_recursive==True, up_recursive == True, force_toggle_all_childs=Fasle:
#     '''
#     given: kx, k3, k11, k12, kp,  is active;
#             k1, ky, kz is delay
#     '''
#     kp, kx, ky, kz, k1, k2, k3, k11, k12 = create_kpi_tree()
#     k1 = set_to_state(k1, 'delay')
#     ky = set_to_state(ky, 'delay')
#     kz = set_to_state(kz, 'delay')
#     assert k1.weight == 0 and ky.weight == 0 and kz.weight == 0
#
#     kx.delay_toggle_advance(admin, recursive=True, up_recursive=True, down_recursive=True,
#                             force_toggle_all_childs=False)
#     kp, kx, k1, k11, k12 = refresh_kpis([kp, kx, k1, k11, k12])
#
#     assert kx.weight == 0
#     assert k1.weight == 0 and k11.weight > 0 and k12.weight > 0
#     '''
#     assert kp.state == 'delay' (because now all childs of kp is delay)
#     '''
#
#     assert kp.weight == 0
#     # 2. recursive:
#     ## 2.1.: recursive == False:
#     ### 2.2.2: down_recursive==True, up_recursive == True, force_toggle_all_childs=True:
#     '''
#     given: kx, k3, k11, k12, kp,  is active;
#             k1, ky, kz is delay
#     '''
#     kp, kx, ky, kz, k1, k2, k3, k11, k12 = create_kpi_tree()
#     k1 = set_to_state(k1, 'delay')
#     ky = set_to_state(ky, 'delay')
#     kz = set_to_state(kz, 'delay')
#     assert k1.weight == 0 and ky.weight == 0 and kz.weight == 0
#
#     kx.delay_toggle_advance(admin, recursive=True, up_recursive=True, down_recursive=True, force_toggle_all_childs=True)
#     kx, k1, k11, k12 = refresh_kpis([kx, k1, k11, k12])
#
#     assert kx.weight == 0
#     assert k1.weight == 0 and k11.weight == 0 and k12.weight == 0


@pytest.mark.django_db(transaction=False)
def test_delay_toggle(userAdmin, ceo, userA_manager_manager, userA_manager, userA, organization):
    '''
    test document: https://cloudjet.atlassian.net/wiki/spaces/CKD/pages/2589435/Delay+active+kpi+v3+--+test+cases
    related docs:
        https://cloudjet.atlassian.net/wiki/spaces/CKD/pages/2490734/Delay+active+kpi+v3
        https://cloudjet.atlassian.net/wiki/spaces/CKD/pages/2622059/delay+active+kpi+v3+Advance
        https://cloudjet.atlassian.net/wiki/spaces/CKD/pages/2425406/Delay+active+kpi+v3+--+Usage
    :param kpi:
    :return:
    '''
    _ceo = ceo
    _userA_manager_manager = userA_manager_manager
    _userA_manager = userA_manager
    _userA = userA

    userA = _ceo
    userB = _userA_manager_manager
    userC = _userA_manager
    userD = _userA
    admin = userAdmin

    organization = organization

    def create_kpi(user, organization):
        current_quarter = organization.get_current_quarter()
        k = KPI()
        k.user = user
        k.quarter_period = current_quarter
        k.save()
        return k

    def set_parent(kc, kp):
        kc.parent = kp
        kc.refer_to = kp
        kc.save()
        return kc

    def create_directly_child(kp):
        kc = create_kpi(kp.user, organization)
        kc = set_parent(kc, kp)
        return kc

    def set_to_state(k, to_state):
        if to_state == 'active':
            k.weight = k.temp_weight if k.temp_weight > 0 else 10
        else:  # to_state == 'delay':
            k.temp_weight = k.weight if k.weight > 0 else 10
            k.weight = 0
        k.save()
        return k

    def refresh_kpi(k):
        return KPI.objects.get(id=k.id)

    def refresh_kpis(ks):
        r = []
        for k in ks:
            r.append(refresh_kpi(k))
        return tuple(r)

    def create_kpi_tree():
        '''
        Sample kpi structure:

                        + userA > userB > userC > userD
                        + delay/active kx
                                                              kp(userA)
                                                           ,-'     -   `-.._
                                                       _.-'         `.      `-.._
                                                [__kx(userA)]  [__ky(userA)]     `-.._
                                                ,-'     `.               `.           `-.._
                                             _,'          `.               -_              `--
                                          kx(userB)         `.          ky(userC)          kz(userA)
                                        ,-    \               `.
                                      ,'       \                `_
                                    ,'    [__k1(userB)]    [__k2(userB)]
                                  ,'             \                   .
                               _,'                \                   `.
                            k3(userB)          k1(userC)             k2(userC)
                                                ,'    `..
                                              ,'         `-.
                                     [__k11(userC)]    [__k1'(userD)]
                                        ,'                     .
                                      _'                        `-.
                               k11(userD)                      k12(`userD)

        '''
        kp = create_kpi(userA, organization)
        kx = kp.assign_to(userB)
        ky = kp.assign_to(userC)
        kz = create_directly_child(kp)
        k3 = create_directly_child(kx)
        k1 = kx.assign_to(userC)
        k2 = kx.assign_to(userC)
        k11 = k1.assign_to(userD)
        k12 = k1.assign_to(userD)

        return kp, kx, ky, kz, k1, k2, k3, k11, k12

    # kpi = kpi.delay_toggle(kpi.user)
    # assert kpi.weight == 0
    #
    # kpi = kpi.delay_toggle(kpi.user)
    # assert kpi.weight > 0
    #
    # kpi = kpi.delay_toggle(kpi.user)
    # assert kpi.weight == 0

    # create kpi tree with all kpis are active
    kp, kx, ky, kz, k1, k2, k3, k11, k12 = create_kpi_tree()

    # 1. force_to_state
    # # 1.1: force_to_state = None:
    assert kx.weight > 0
    kx.delay_toggle(admin, force_to_state=None)

    assert kx.weight == 0

    # 1. force_to_state
    # # 1.2: force_to_state = 'delay':
    # ## 1.2.1: kx.state=='delay':

    # create kpi tree with all kpis are active
    kp, kx, ky, kz, k1, k2, k3, k11, k12 = create_kpi_tree()

    kx = set_to_state(kx, 'delay')
    assert k3.weight > 0 and kx.weight == 0

    k3.delay_toggle(admin, force_to_state='delay')
    assert k3.weight == 0

    try:
        k3.delay_toggle(admin, force_to_state='active')
        assert k3.weight == 0
    except PermissionDenied:
        pass

    kx.delay_toggle(admin, force_to_state='delay')

    k3 = refresh_kpi(k3)
    assert k3.weight == 0

    # 1. force_to_state
    # # 1.3: force_to_state = 'active':
    # ## 1.3.2: kx.state=='active':
    kp, kx, ky, kz, k1, k2, k3, k11, k12 = create_kpi_tree()
    k1 = set_to_state(k1, 'delay')
    assert kx.weight > 0 and k1.weight == 0

    kx.delay_toggle(admin, force_to_state='active')
    k1 = refresh_kpi(k1)
    assert k1.weight > 0

    # 2. recursive:
    # # 2.1.: recursive == False:

    kp, kx, ky, kz, k1, k2, k3, k11, k12 = create_kpi_tree()
    # given kx, k3 is active; k1 is delay
    k1 = set_to_state(k1, 'delay')
    assert k1.weight == 0

    kx.delay_toggle(admin, recursive=False)
    k3 = refresh_kpi(k3)
    assert kx.weight == 0 and k3.weight > 0

    # # 2. recursive:
    # ## 2.1.: recursive == False:
    # ### 2.2.1: down_recursive==True, up_recursive == True, force_toggle_all_childs=Fasle:
    # '''
    # given: kx, k3, k11, k12, kp,  is active;
    #         k1, ky, kz is delay
    # '''
    # kp, kx, ky, kz, k1, k2, k3, k11, k12 = create_kpi_tree()
    # k1=set_to_state(k1,'delay')
    # ky=set_to_state(ky,'delay')
    # kz=set_to_state(kz,'delay')
    # assert k1.weight == 0 and ky.weight == 0 and kz.weight == 0
    #
    # kx.delay_toggle(admin, recursive=True, up_recursive=True, down_recursive=True, force_toggle_all_childs=False)
    # kp, kx, k1, k11, k12 = refresh_kpis([kp, kx, k1, k11, k12])
    #
    # assert kx.weight == 0
    # assert k1.weight == 0 and k11.weight > 0 and k12.weight > 0
    # '''
    # assert kp.state == 'delay' (because now all childs of kp is delay)
    # '''
    #
    # assert kp.weight == 0

    # 2. recursive:
    # # 2.1.: recursive == False:
    # ## 2.2.2: down_recursive==True, up_recursive == True, force_toggle_all_childs=True:
    '''
    given: kx, k3, k11, k12, kp,  is active;
            k1, ky, kz is delay
    '''
    kp, kx, ky, kz, k1, k2, k3, k11, k12 = create_kpi_tree()
    k1 = set_to_state(k1, 'delay')
    ky = set_to_state(ky, 'delay')
    kz = set_to_state(kz, 'delay')
    assert k1.weight == 0 and ky.weight == 0 and kz.weight == 0

    kx.delay_toggle(admin, recursive=True, up_recursive=True, down_recursive=True)  # , force_toggle_all_childs=True)
    kp, kx, k1, k11, k12 = refresh_kpis([kp, kx, k1, k11, k12])

    assert kx.weight == 0
    assert k1.weight == 0 and k11.weight == 0 and k12.weight == 0
    '''
    assert kp.state == 'delay' (because now all childs of kp is delay)
    '''
    assert kp.weight == 0

    # sepcial testcase:
    '''
        given: kx, k1, k11, kp,  is active;
                ky, kz, k2, k3, k12 is delay
        '''
    kp, kx, ky, kz, k1, k2, k3, k11, k12 = create_kpi_tree()
    ky = set_to_state(ky, 'delay')
    kz = set_to_state(kz, 'delay')
    k2 = set_to_state(k2, 'delay')
    k3 = set_to_state(k3, 'delay')
    k12 = set_to_state(k12, 'delay')

    assert ky.weight == 0 and kz.weight == 0 and k3.weight == 0 and k12.weight == 0

   # k11.delay_toggle_advance(admin, recursive=True, up_recursive=True, down_recursive=True)
   # kp, kx, k1, k11, k12 = refresh_kpis([kp, kx, k1, k11, k12])
    # assert kp.weight == 0
    u = User()
    u.save()
    kp1 = create_kpi(ceo, organization)
    flag = 0
    try:
        result = kp1.delay_toggle(u)
    except:
        flag = 1
    assert flag == 1


@pytest.mark.django_db(transaction=False)
def test_fix_self(userA, userB, userC, organization, userA_manager):
    # Test kpi_trung_gian has multiple assigned_kpi
    k = KPI()
    k.quarter_period = organization.get_current_quarter()
    k.user = userA
    k.save()

    assigned_kpi = k.assign_to(userB)
    assert assigned_kpi is not None
    cascaded_from = assigned_kpi.cascaded_from  # cascaded_from is kpi_trung_gian
    assert cascaded_from is not None

    cache.clear()
    assert cascaded_from.kpi_type_v2() == kpi_trung_gian
    # TODO: assert cascaded_from.fix_self(use_cache=False) == False

    '''
     fix điểm số kpi_trung_gian và kpi_duoc_phan_cong
    '''
    assigned_kpi.month_1_target = 10
    assigned_kpi.month_1 = 5

    assigned_kpi.target = 10
    assigned_kpi.real = 5
    # assigned_kpi.latest_score =
    assigned_kpi.save_calculate_score(calculate_now_thread=False)
    #    assert cascaded_from.fix_self(use_cache=False) == True
    #    assert cascaded_from.fix_self(use_cache=False) == False
    assert KPI.objects.get(id=cascaded_from.id).get_score() == 50  # lt_score #assigned_kpi.latest_score

    ############ test kpi_trung_gian.assigned_to

    cascaded_from.assigned_to = userC
    cascaded_from.save()
    assert cascaded_from.fix_self(use_cache=False) == True
    assert cascaded_from.fix_self(use_cache=False) == False
    assert KPI.objects.filter(parent=cascaded_from.parent_id).count() == 1

    '''
    Test kpi_trung_gian co nhieu kpi  phan cong
    '''
    k2 = KPI()
    k2.quarter_period = organization.get_current_quarter()
    k2.user = userB
    k2.cascaded_from = cascaded_from
    k2.save()
    assert cascaded_from.fix_self(use_cache=False) == True
    assert cascaded_from.fix_self(use_cache=False) == False
    assert KPI.objects.filter(parent=cascaded_from.parent_id).count() == 1

    ############ Test kpi_cha_duoc_phan_cong có refer_to != cascaded.parent
    kx = KPI()
    kx.quarter_period = organization.get_current_quarter()
    kx.save()
    assigned_kpi.refer_to = kx
    assigned_kpi.save()
    assert assigned_kpi.fix_self(use_cache=False) == True
    assert assigned_kpi.fix_self(use_cache=False) == False
    assert KPI.objects.filter(parent=cascaded_from.parent_id).count() == 1

    # case: KPI trung gian co kpi con

    '''
    GIVEN: cascaded_from la kpi_trung_gian
    GIVEN: kpi trung gian nay lai co them kpi con la : kpi_con1 va kpi_con2
    THEN: delete kpi_con1 va kpi_con2
    '''
    cascaded_from1 = KPI()
    cascaded_from1.parent = k
    cascaded_from1.user = userA
    cascaded_from1.save()
    cascaded_from1.assign_to(userB)

    cache.clear()
    assert cascaded_from.kpi_type_v2() == kpi_trung_gian

    kpi_con1 = KPI()
    kpi_con1.quarter_period = organization.get_current_quarter()
    kpi_con1.parent = cascaded_from1
    kpi_con1.save()
    kpi_con2 = KPI()
    kpi_con2.quarter_period = organization.get_current_quarter()
    kpi_con2.parent = cascaded_from1
    kpi_con2.save()

    assert cascaded_from1.fix_self(use_cache=False) == True
    assert cascaded_from1.fix_self(use_cache=False) == False

    '''
    Khi assigned_kpi có --> hoãn -->  cascaded_from cũng hoãn theo
    '''

    cascaded_from.assign_to(userB)
    assigned_kpi = cascaded_from.get_assigned_kpi()
    assigned_kpi.weight = 0
    assigned_kpi.save()

    assert cascaded_from.fix_self(use_cache=False) == True
    assert cascaded_from.fix_self(use_cache=False) == False

    '''
    GIVEN:  kpi_trung_gian co assigned_kpi.refer_to != self.parent
    THEN: assigned_kpi.refer_to == self.parent
    '''
    kpi_random = KPI()
    kpi_random.quarter_period = organization.get_current_quarter()
    kpi_random.save()
    assigned_kpi.refer_to = kpi_random
    assigned_kpi.save()

    assert cascaded_from.fix_self(use_cache=False) == True
    assert cascaded_from.fix_self(use_cache=False) == False

    '''
    GIVEN:  KPI cha A có KPI con A1
            KPI con A1 phân công thành X1
            KPI X1 bị xoá
    THEN       --> KPI A1 cần được điều chỉnh refer_to = A
    '''
    A = KPI()
    A.quarter_period = organization.get_current_quarter()
    A.user = userA
    A.save()
    A1 = KPI()
    A1.quarter_period = organization.get_current_quarter()
    A1.user = userA
    A1.parent = A
    A1.save()

    assert A1.kpi_type_v2() == kpi_con_normal
    A1.assign_to(userB)
    X1 = A1.get_assigned_kpi()

    assert A1.kpi_type_v2() == kpi_trung_gian
    assert X1.kpi_type_v2() == kpi_cha_duoc_phan_cong

    X1.delete()

    A.fix_self(use_cache=False)
    assert KPI.objects.get(id=A1.id).refer_to == A

    '''
    kpi_con_normal: fix_children_assigned
    GIVEN:
        A2 là kpi_con_normal
        Nếu A2 có A2.assigned_to is not None --> error
    THEN:
        Convert A2 thành  into kpi_trung_gian
    '''

    A2 = KPI()
    A2.quarter_period = organization.get_current_quarter()
    A2.user = userA
    A2.parent = A
    A2.save()
    assert A2.kpi_type_v2() == kpi_con_normal

    A2.assigned_to = userB
    A2.save()

    assert A2.fix_self(use_cache=False) == True
    assert A2.fix_self(use_cache=False) == False

    '''
    GIVEN: PerformanceCloner is runnning
    THEN: fix_self should not working
    <fix>
    '''
    q = QuarterPeriod.objects.create(organization=organization,
                                     due_date=datetime.date.today(),
                                     clone_from_quarter_period=organization.get_current_quarter())

    q.status = 'IN'
    q.save()


    # cloner = PerformanceCloner(organization, q)
    # cloner.start()
    start_cloner_in_celery(organization, q)

    time.sleep(2)


    k5 = KPI()
    k5.quarter_period = organization.get_current_quarter()
    k5.user = userA
    k5.save()
    # cache_key = CREATING_ASSESSMENT_ORG.format(organization.id)
    # TEMPO Disable: assert k5.fix_self() == cache_key
    # TEMPO Disable: assert k5.fix_self() == organization.id
    #
    # '''
    # Delete cache to avoid other test fail
    # '''
    #
    # cache.delete(cache_key)
    # remove_redis(cache_key)
    '''
    </fix>
    '''

    '''
    GIVEN:  kpi_trung_gian co assigned_kpi.refer_to != self.parent
    THEN: assigned_kpi.refer_to == self.parent
    <fix>
    '''
    kd = KPI()
    kd.quarter_period = organization.get_current_quarter()
    kd.name = "DUPLICATED KPI 123"
    kd.user = userA
    kd.parent = k
    kd.quarter_period = organization.get_current_quarter()
    kd.save()
    assert kd.kpi_type_v2() == kpi_con_normal

    kd.assign_to(userB)

    assert kd.kpi_type_v2() == kpi_trung_gian

    kd_assign = kd.get_assigned_kpi()
    assert kd_assign.kpi_type_v2() == kpi_cha_duoc_phan_cong

    kdB = KPI()
    kdB.quarter_period = organization.get_current_quarter()
    kdB.name = kd.name
    kdB.user = userB
    kdB.quarter_period = organization.get_current_quarter()
    kdB.save()
    assert kdB.kpi_type_v2() == kpi_cha_normal

    kdB1 = KPI()
    kdB1.quarter_period = organization.get_current_quarter()
    kdB1.name = "duplicated kpi child"
    kdB1.user = userB
    kdB1.parent = kdB
    kdB1.save()

    assert kdB1.kpi_type_v2() == kpi_con_normal

    assert bool(kdB.fix_self()) == True
    assert kdB.fix_self(use_cache=False) == False
    assert kdB.kpi_type_v2() == kpi_cha_duoc_phan_cong
    assert kdB.refer_to == k
    assert KPI.objects.filter(id=kd.id).first() == None

    '''
    </fix>
    '''

    '''
        GIVEN:  kpi_trung_gian.weight == 0 and assigned_kpi.weight > 0
        THEN: toggle_weight(kpi_trung_gian.weight)
    '''
    kd = KPI()
    kd.quarter_period = organization.get_current_quarter()
    kd.name = "DUPLICATED KPI 123"
    kd.user = userA
    kd.parent = k
    kd.quarter_period = organization.get_current_quarter()
    kd.save()

    kd.assign_to(userB)

    assert kd.kpi_type_v2() == kpi_trung_gian
    kd.weight = 0
    kd.save(update_fields=['weight'])
    assert bool(kdB.fix_self()) == True
    assert kdB.fix_self(use_cache=False) == False

    '''
    test kpi recursive bugs
    for case: k.refer_to==k
    '''
    k1 = KPI()
    k1.quarter_period = organization.get_current_quarter()
    k1.user = userA_manager
    k1.save()

    k2 = k1.assign_to(userA)
    cascaded_from = k2.cascaded_from

    assert k2.refer_to == k1
    assert cascaded_from.parent == k1

    k2.refer_to = k2
    k2.save(update_fields=['refer_to', ])

    k2.fix_self()
    assert k2.refer_to.id == k1.id

    '''
    GIVEN: Test case kpi khong co kpi con ma co kpi trung gian
    THEN: Chuyen kpi trung gian thanh kpi con normal va tinh lai diem
    '''
    k = KPI()
    k.quarter_period = organization.get_current_quarter()
    k.user = userA_manager
    k.month_1_target = 10
    k.score_calculation_type = 'sum'
    k.review_type = 'monthly'
    k.save()

    k_con = create_child_kpi(k, k.quarter_period)
    k_assigned = k_con.assign_to(userA)

    k_tg = k_assigned.cascaded_from

    assert k_tg.kpi_type_v2() == kpi_trung_gian

    k_assigned.delete()

    k.month_1 = 9
    k.save()

    k.fix_self()
    calculate_score(k)

    assert k.month_1_score == 90

    b = KPI.objects.filter(id=k_tg.id).first()
    # kpi trung gian sau khi fix_self se tro thanh kpi con normal
    assert b.kpi_type_v2() == kpi_con_normal

    # check calculation == sum and month_target is None
    _k = KPI()
    _k.score_calculation_type = 'sum'
    _k.target = 12
    _k.quarter_period = organization.get_current_quarter()
    _k.user = userA
    _k.save()

    _k.fix_self()

    assert _k.month_1_target == 4
    assert _k.month_2_target == 4
    assert _k.month_3_target == 4

    # check calculation == 'most_recent', month_target == quarter_target
    _k = KPI()
    _k.score_calculation_type = 'most_recent'
    _k.target = 12
    _k.quarter_period = organization.get_current_quarter()
    _k.user = userA
    _k.save()

    _k.fix_self()

    assert _k.month_1_target == 12
    assert _k.month_2_target == 12
    assert _k.month_3_target == 12


@pytest.mark.django_db(transaction=False)
def test_get_weighted_weight(userB, organization):
    k1 = KPI()
    k1.user = userB
    k1.quarter_period = organization.get_current_quarter()
    k1.save()
    k2 = KPI()
    k2.user = userB
    k2.save()

    assert k1.get_weighted_weight() == 100

    k2.quarter_period = organization.get_current_quarter()
    k2.save()
    # k1.weight =10 and k2.weight =10
    assert k1.get_weighted_weight() == 50

    k11 = KPI()
    k11.parent = k1
    k11.save()
    assert k11.get_weighted_weight() == 50

    k12 = KPI()
    k12.parent = k1
    k12.save()
    assert k12.get_weighted_weight() == 25


@pytest.mark.django_db(transaction=False)
def test_all_methods():
    k = KPI()
    k.name = 'name'
    k.save()

    # print methods(k)

    for m in methods(k):
        # if m != 'delete':
        # print m
        if not m.startswith('_') and m != 'delete':

            try:
                getattr(k, m)()
            except KeyError:
                pass
            except TypeError:
                pass
            except AttributeError:
                pass
            except ValidationError:
                pass
            except ObjectDoesNotExist:
                pass

    getattr(k, 'delete')()

    # to_json_dump


@pytest.fixture
@pytest.mark.django_db(transaction=False)
def assigned_kpi(kpi, userB):
    return kpi.assign_to(userB)


@pytest.mark.django_db(transaction=False)
def test_assign_up(parent_kpi, userA, userB, kpi):
    k = KPI()
    k.user = userB
    k.save()

    assert k.assign_up(None) is None
    assert k.assign_up(parent_kpi).parent_id == parent_kpi.id
    assert k.assign_up(parent_kpi).refer_to_id is None
    assert k.refer_to_id == parent_kpi.id

    k.user = userA
    k.save()
    kpi.bsc_category = "financial"
    kpi.cascaded_from = parent_kpi
    kpi.save()
    c = kpi.cascaded_from
    c.assigned_to_id = userA
    c.save()

    assert kpi.assign_up(k) is kpi.cascaded_from

    # test for assign-up kpi, case of parent-kpi is delayed
    k1 = KPI()
    k1.user = userB
    k1.save()
    # current parent kpi is active
    assert parent_kpi.weight > 0
    # change parent kpi to delay
    parent_kpi.weight = 0
    parent_kpi.save(update_fields=['weight', ])
    assert parent_kpi.weight == 0
    k1.assign_up(parent_kpi, force_run=True)
    # assert that new_kpi still link to parent_kpi regardless of whether parent_kpi is delayed or not
    assert k1.refer_to_id == parent_kpi.id


@pytest.mark.django_db(transaction=False)
def test_assign_to_no_duplicate(parent_kpi, organization, userA, userB, kpi):
    result = kpi.assign_to_no_duplicate(userA)
    assert result is False
    k1 = KPI()
    k1.name = "Child KPI"
    k1.quarter_period = organization.get_current_quarter()
    k1.user = userB
    k1.target = 100
    k1.operator = ">="

    k1.save()
    dkpi = KPI.objects.filter(name=kpi.name, user=userB).first()
    assert dkpi.refer_to is None
    kpi.assign_to_no_duplicate(userB)

    assert KPI.objects.filter(name=kpi.name, user=userB).first().refer_to == kpi.parent


@pytest.mark.django_db(transaction=False)
def test_to_percent(kpi, parent_kpi):
    assert kpi.to_percent() == "{0}%".format(kpi.get_score())
    kpi.latest_score = randint(1, 100)
    kpi.save()
    assert kpi.get_score() == kpi.latest_score
    assert kpi.to_percent() == "{0}%".format(kpi.get_score())
    assert kpi.to_percent(num=True) == kpi.get_score()


@pytest.mark.django_db(transaction=False)
def test_get_unique_key(kpi, parent_kpi):
    """
    case 1 : kpi not saved
    """
    k = KPI()
    result = k.get_unique_key()
    assert k.unique_key == result
    """
    case 2 : kpi saved
    """
    assert kpi.get_unique_key() == kpi.get_unique_key()


@pytest.mark.django_db(transaction=False)
def test_get_name_current_goal(kpi, parent_kpi):
    """
    Case 1: no current_goal
    """
    assert kpi.get_name() == kpi.get_name_current_goal()

    """
    Case 2: With current goal
    """
    kpi.current_goal = random_string(50)
    kpi.save()
    assert kpi.current_goal in kpi.get_name_current_goal()


@pytest.mark.django_db(transaction=False)
def test_move_kpi(kpi, assigned_kpi, userB, parent_kpi, parent_kpi2):
    # kpi được phân công cho userB

    assert assigned_kpi is not None
    assert assigned_kpi.user == userB
    assert assigned_kpi.cascaded_from_id == kpi.id
    assert assigned_kpi.refer_to_id == kpi.parent_id

    assert assigned_kpi.id == kpi.get_assigned_kpi().id

    assert kpi.assigned_to_id > 0

    kpi.move_kpi(parent_kpi2.id)

    # khi move kpi qua parent_kpi2 , lúc này

    assigned_kpi = KPI.objects.get(id=assigned_kpi.id)

    # kpi = KPI.objects.filter(id = kpi.id).first()

    # assert kpi is None
    assert assigned_kpi.refer_to_id == parent_kpi2.id
    # print assigned_kpi.cascaded_from_id
    assert assigned_kpi.cascaded_from_id > 0

    p_id = random_string()
    result = parent_kpi2.move_kpi(parent_id=p_id)
    assert result.parent_id is None

    assert parent_kpi.assigned_to_id is None
    result = parent_kpi2.move_kpi(parent_kpi.id)
    assert result.parent_id == parent_kpi.id


@pytest.mark.django_db(transaction=False)
def test_set_target_from_quarter_targets(kpi, organization):
    q = QuarterPeriod()
    q.due_date = datetime.date.today()  # .date
    q.quarter = 2
    q.organization = organization
    q.save()

    kpi.quarter_period = q

    kpi.quarter_one_target = 10
    kpi.quarter_two_target = 20
    kpi.quarter_three_target = 30
    kpi.quarter_four_target = 40
    kpi.save()

    kpi.set_target_from_quarter_targets()

    assert kpi.target == kpi.quarter_two_target

    q.quarter = 1
    q.save()
    kpi.set_target_from_quarter_targets()
    assert kpi.target == kpi.quarter_one_target

    q.quarter = 3
    q.save()
    kpi.set_target_from_quarter_targets()
    assert kpi.target == kpi.quarter_three_target

    q.quarter = 4
    q.save()
    kpi.set_target_from_quarter_targets()
    assert kpi.target == kpi.quarter_four_target


@pytest.mark.django_db(transaction=False)
def test_set_prefix_id(kpi):
    prefix_id = kpi.set_prefix_id()

    assert prefix_id != '' and prefix_id != None


@pytest.mark.django_db(transaction=False)
def test_name_mindmap(kpi, userC):
    result = kpi.name_mindmap()
    assert result == kpi.name
    userC.username = random_string(5)
    userC.save()
    kpi.assigned_to = userC
    kpi.save()
    result = kpi.name_mindmap()
    assert userC.username in result


@pytest.mark.django_db(transaction=False)
def test_clone_to_user(userA, userB, organization, kpi, parent_kpi):

    # test for clone kpi, case of parent-kpi is delayed
    k = kpi
    # current parent kpi is active
    assert k.refer_to_id == parent_kpi.id
    assert parent_kpi.weight > 0
    # change parent kpi to delay
    parent_kpi.weight = 0
    parent_kpi.save(update_fields=['weight', ])
    assert parent_kpi.weight == 0
    new_kpi = k.clone_to_user(userA)
    # assert that new_kpi still link to parent_kpi regardless of whether parent_kpi is delayed or not
    assert new_kpi.refer_to_id == parent_kpi.id

    kpi = KPI()
    kpi.name = 'kpi_cha'
    kpi.user = userA
    kpi.save()

    kpi_child_1 = KPI()
    kpi_child_1.name = 'kpi_child_1'
    kpi_child_1.user = userA
    kpi_child_1.parent = kpi

    kpi_child_1.save()

    kpi_child_2 = KPI()
    kpi_child_2.name = 'kpi_child_2'
    kpi_child_2.user = userA
    kpi_child_2.parent = kpi

    kpi_child_2.save()

    current_quarter = kpi.user.profile.get_current_quarter()
    new_kpi = kpi.clone_to_user(userA, current_quarter)

    assert new_kpi.user == userA
    assert new_kpi.unique_key == kpi.unique_key

    children = new_kpi.get_children()
    assert children.count() == 2
    assert children[0].name == kpi_child_1.name or children[0].name == kpi_child_2.name
    assert children[1].name == kpi_child_1.name or children[1].name == kpi_child_2.name

    for c in children:
        c.delete()

    assert new_kpi.get_children().count() == 0
    new_kpi.fix_clone_to_user_missing_child()

    children = new_kpi.get_children()
    assert children.count() == 2
    assert children[0].name == kpi_child_1.name or children[0].name == kpi_child_2.name
    assert children[1].name == kpi_child_1.name or children[1].name == kpi_child_2.name

    assert KPI.objects.get(id=kpi_child_1.id).refer_to_id is None
    assert kpi.fix_count_children_refer() == True
    # TODO assert KPI.objects.get(id =  kpi_child_1.id).refer_to_id is not None
    kpi.default_real = randint(1, 100)
    kpi.save()
    new_kpi2 = kpi.clone_to_user(userA, current_quarter)

    assert new_kpi2.user == userA
    assert new_kpi2.unique_key == kpi.unique_key
    assert new_kpi2.month_1 == kpi.default_real


@pytest.mark.django_db(transaction=False)
def test_clone_to_new_kpi(kpi, userB, organization):
    current_quarter = kpi.user.profile.get_current_quarter()

    new_kpi = kpi.clone_to_new_kpi(userB, current_quarter, kpi.unique_key)

    pass


@pytest.mark.django_db(transaction=False)
def test_get_children_refer_order(parent_kpi, kpi, organization):
    kpi.fix_self()
    # assert parent_kpi.get_children_refer_order()[0].id == kpi.id
    # assert parent_kpi2.get_children_refer_order()[0].id == kpi.id

    # Testing caching

    kpis = get_children_refer_order(kpi)
    for k in kpis:
        print k


@pytest.mark.django_db(transaction=False)
def test_get_monthly_target(kpi):
    k = KPI()
    k.target = 100
    k.save()
    assert k.get_month_1_target() == 100
    assert k.get_month_2_target() == 100
    assert k.get_month_3_target() == 100


@pytest.mark.django_db(transaction=False)
def test_kpi_save_recalculate_score_after_save(kpi):
    if is_queued(kpi):
        CascadeKPIQueue.objects.filter(kpi=kpi).delete()

    kpi.name += ' - edit '

    time.sleep(6)
    kpi.save()

    is_queued_ = is_queued(kpi)
    assert is_queued_ is not None


@pytest.mark.django_db(transaction=False)
def set_score(kpi):
    kpi.latest_score = 100
    kpi.save()

    assert KPI.objects.get(id=kpi.id).latest_score == 100

    kpi.set_score(0)  # this is an important test
    assert KPI.objects.get(id=kpi.id).latest_score == 0

    kpi.set_score(25)  # this is an important test
    assert KPI.objects.get(id=kpi.id).latest_score == 25

    kpi.set_score(35)  # this is an important test
    assert KPI.objects.get(id=kpi.id).latest_score == 35


@pytest.mark.django_db(transaction=False)
def test_restore_kpi(kpi):
    kpi_id = kpi.id
    kpi.delete()
    assert KPI.objects.filter(id=kpi_id).first() is None

    # assert kpi.parent_id > 0

    kpi.restore()
    assert KPI.objects.filter(id=kpi_id).first() is not None


@pytest.mark.django_db(transaction=False)
def test_kpi_type_v2(parent_kpi):
    # THIS test is not completed yet
    kpi = KPI()

    assert kpi.kpi_type_v2() == kpi_cha_normal

    kpi.parent_id = parent_kpi.id
    assert kpi.kpi_type_v2() == kpi_con_normal

    kpi.parent_id = None
    kpi.refer_to_id = parent_kpi.id
    assert kpi.kpi_type_v2() == kpi_cha_duoc_phan_cong

    kpi.refer_to_id = None
    kpi.cascaded_from_id = parent_kpi.id
    assert kpi.kpi_type_v2() == kpi_cha_duoc_phan_cong


@pytest.mark.django_db(transaction=False)
def test_save_kpi_invalid_data(userB):
    k = KPI()
    k.name = 'test'
    k.weight = None
    k.user = userB
    k.save()

    assert k.weight == 10

    prefix_id = random_string(1000)

    k.prefix_id = prefix_id
    k.save()

    assert prefix_id.startswith(k.prefix_id) == True

    k.prefix_id = None
    k.save()


@pytest.mark.django_db(transaction=False)
def test_kpi_to_json_excel(parent_kpi, userA_manager, organization, userA, kpi_userA_manager):
    current_quarter = organization.get_current_quarter()

    # ## CASE 1: Assign to that user, not will not have any child weight
    # userA_manager assign kpis into userA
    k = create_child_kpi(parent_kpi, current_quarter, actor=userA_manager)
    k.assign_to(userA)

    # userA_manager assign kpis into userA from one of userA's kpis
    k2 = create_child_kpi(parent_kpi, current_quarter, actor=userA_manager)
    k2.assign_to(userA)

    json_data = parent_kpi.to_json_excel()['children']
    children_refer = parent_kpi.get_children_refer().values_list('id', flat=True)
    children = parent_kpi.get_children().values_list('id', flat=True)

    # Do not modify without knowing what you're doing
    assert len(json_data) == len(children_refer)
    assert len(json_data) == len(children)
    assert len(json_data) > 0

    # Do not modify without knowing what you're doing
    for json_child in json_data:
        kpi_c1 = KPI.objects.get(id=json_child['id'])
        assert kpi_c1.id in children_refer
        assert kpi_c1.id in children

    # ## Case 2: assign kpi from userA_manager to userA
    # userA_manager assign kpis into userA
    k = create_child_kpi(kpi_userA_manager, current_quarter, actor=userA_manager)
    k.assign_to(userA)

    # userA_manager assign kpis into userA from one of userA's kpis
    k2 = create_child_kpi(kpi_userA_manager, current_quarter, actor=userA_manager)
    k2.assign_to(userA)

    json_data = kpi_userA_manager.to_json_excel()['children']
    children_refer = kpi_userA_manager.get_children_refer().values_list('id', flat=True)
    children = kpi_userA_manager.get_children().values_list('id', flat=True)

    # # Do not modified without knowing what you're doing
    assert len(json_data) == len(children_refer)
    assert len(json_data) == len(children)
    assert len(json_data) > 0

    # # Do not modified without knowing what you're doing
    for json_child in json_data:
        kpi = KPI.objects.get(id=json_child['id'])
        assert kpi.id in children_refer
        assert kpi.id not in children


@pytest.mark.django_db(transaction=False)
def test_kpi_to_json(parent_kpi, userA_manager, organization, userA, kpi_userA_manager):
    current_quarter = organization.get_current_quarter()

    # ## CASE 1: Assign to that user, not will not have any child weight
    # userA_manager assign kpis into userA
    k = create_child_kpi(parent_kpi, current_quarter, actor=userA_manager)
    k.assign_to(userA)

    # userA_manager assign kpis into userA from one of userA's kpis
    k2 = create_child_kpi(parent_kpi, current_quarter, actor=userA_manager)
    k2.assign_to(userA)

    json_data = parent_kpi.to_json()['children']
    children_refer = parent_kpi.get_children_refer().values_list('id', flat=True)
    children = parent_kpi.get_children().values_list('id', flat=True)

    assert len(json_data) == len(children_refer)
    assert len(json_data) == len(children)
    assert len(json_data) > 0

    # # Do not modified without knowing what you're doing
    for json_child in json_data:
        kpi_c1 = KPI.objects.get(id=json_child['id'])
        # khi KPI duoc phan cong thi kpi con cung la kpi con co refer
        assert kpi_c1.id in children_refer
        assert kpi_c1.id in children

    # ## Case 2: assign kpi from userA_manager to userA

    # userA_manager assign kpis into userA
    k = create_child_kpi(kpi_userA_manager, current_quarter, actor=userA_manager)
    k.assign_to(userA)

    # userA_manager assign kpis into userA from one of userA's kpis
    k2 = create_child_kpi(kpi_userA_manager, current_quarter, actor=userA_manager)
    k2.assign_to(userA)

    json_data = kpi_userA_manager.to_json()['children']
    children_refer = kpi_userA_manager.get_children_refer().values_list('id', flat=True)
    children = kpi_userA_manager.get_children().values_list('id', flat=True)

    # # Do not modified without knowing what you're doing
    assert len(json_data) == len(children_refer)
    assert len(json_data) == len(children)
    assert len(json_data) > 0

    for json_child in json_data:  # # Do not modified without knowing what you're doing
        kpi = KPI.objects.get(id=json_child['id'])
        assert kpi.id not in children_refer
        assert kpi.id in children

    # ## Case 3: test to_json(refer=True) will act like to_json_excel
    json_data = kpi_userA_manager.to_json(refer=True)['children']
    children_refer = kpi_userA_manager.get_children_refer().values_list('id', flat=True)
    children = kpi_userA_manager.get_children().values_list('id', flat=True)

    # # Do not modified without knowing what you're doing
    assert len(json_data) == len(children_refer)
    assert len(json_data) == len(children)
    assert len(json_data) > 0

    for json_child in json_data:
        kpi = KPI.objects.get(id=json_child['id'])
        assert kpi.id in children_refer
        assert kpi.id not in children


@pytest.mark.django_db(transaction=False)
def test_get_kpi_lock(kpi):
    kl = kpi.get_kpi_lock()
    assert kl.get_permission() == 0


def methods(object):
    # return [x for x, y in cls.__dict__.items() if type(y) == FunctionType]

    ms = []  # [method for method in dir(object) if callable(getattr(object, method))]
    for m in [method for method in dir(object)]:
        try:
            if callable(getattr(object, m)):
                ms.append(m)
        except:
            pass

    return ms


@pytest.mark.django_db(transaction=False)
def test_assign_to(userA_manager, userA, userB, kpi, organization):
    current_quarter = organization.get_current_quarter()
    k1 = KPI()
    k1.user = userA_manager
    k1.quarter_period = current_quarter

    k1.save()

    k2 = k1.assign_to(userA)
    assert k2 is not None
    assert k2.user.id == userA.id
    kpis = get_kpis_parent_by_user(userA.id, current_quarter)

    assert k2 in kpis

    k1.cascaded_from = kpi
    k1.save()
    k2 = kpi.assign_to(userB)
    assert k2.owner_email == userB.email


@pytest.mark.django_db(transaction=False)
def test_get_score(userA, kpi):
    """
    Test for month parameter
    """
    kpi.month_1_score = randint(1, 100)
    kpi.month_2_score = randint(1, 100)
    kpi.month_3_score = randint(1, 100)
    kpi.latest_score = randint(1, 100)
    kpi.save()

    result = kpi.get_score(month=1)
    assert result == kpi.month_1_score
    result = kpi.get_score(month=2)
    assert result == kpi.month_2_score
    result = kpi.get_score(month=3)
    assert result == kpi.month_3_score


@pytest.mark.django_db(transaction=False)
def test_reset_child_score(userA, kpi, parent_kpi):
    kpi.month_1_score = randint(1, 100)
    kpi.month_2_score = randint(1, 100)
    kpi.month_3_score = randint(1, 100)
    kpi.latest_score = randint(1, 100)
    kpi.save()
    owner = parent_kpi.user
    assert len(parent_kpi.get_children()) == 1
    parent_kpi.reset_child_score(reviewer_id=owner.email)
    assert KPI.objects.get(id=kpi.id).latest_score is None


@pytest.mark.django_db(transaction=False)
def test_create_interchange_kpi(userA, kpi, parent_kpi, userB):
    kpi.cascaded_from = parent_kpi
    kpi.save()

    kpi.create_interchange_kpi(userA)
    assert KPI.objects.get(id=kpi.id).cascaded_from is None

    parent_kpi.owner_email = userA.email
    parent_kpi.save()

    k1 = KPI()
    k1.parent = parent_kpi
    k1.user = userB
    k1.refer_to = parent_kpi
    k1.owner_email = userB.email
    k1.save()
    num = len(parent_kpi.get_children())

    k1.create_interchange_kpi()
    assert len(parent_kpi.get_children()) == num + 1


@pytest.mark.django_db(transaction=False)
def test_get_next_ordering(userA, parent_kpi, kpi, organization):
    k1 = KPI()
    k1.refer_to = parent_kpi
    k1.save()
    result = k1.get_next_ordering()
    assert result > 1


@pytest.mark.django_db(transaction=False)
def test_has_child(parent_kpi, kpi):

    @override_settings(
        CACHES={
            'default': {
                'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
                'LOCATION': 'unique-snowflake',
            }
        }
    )
    def test_runner():
        result = parent_kpi.has_child()
        assert result is True

        """
        Set cache
        """
        key = "has_child{}".format(parent_kpi.id)
        cache.set(key, result, 60)
        result = parent_kpi.has_child()
        assert result is True

    test_runner()


@pytest.mark.django_db(transaction=False)
def test_get_children_dict(kpi, parent_kpi, userA):
    from user_profile.templatetags.user_tags import display_name
    k1 = KPI()
    k1.end_date = datetime.date.today()
    k1.parent = parent_kpi
    k1.refer_to = parent_kpi
    k1.user = userA
    k1.assigned_to_id = userA.id
    k1.latest_score = randint(1, 100)
    k1.month_1_score = randint(1, 100)
    k1.save()
    result = parent_kpi.get_children_dict(person=None, reviewer=True)
    for index in result:
        if index['id'] == k1.id:
            assert index['deadline'] == format(k1.end_date, "d-m-Y")
            assert index['assigned_to'] == {'user_id': k1.assigned_to_id,
                                       'full_name': display_name(k1.assigned_to_id)}
            assert index['histories'] == []
            assert index['score'] == k1.get_score()


@pytest.mark.django_db(transaction=False)
def test_to_json_nochildren(kpi, parent_kpi, userA):
    from user_profile.templatetags.user_tags import display_name
    k1 = KPI()
    k1.end_date = datetime.date.today()
    k1.parent = parent_kpi
    k1.refer_to = parent_kpi
    k1.user = userA
    k1.assigned_to_id = userA.id
    k1.latest_score = randint(1, 100)
    k1.month_1_score = randint(1, 100)
    k1.save()
    result = k1.to_json_nochildren(userA)
    assert result['assigned_to_email'] == userA.email
    assert result['enable_edit'] is True
