# !/usr/bin/python
# -*- coding: utf-8 -*-
import pytest
from performance.db_models.kpi_library import KPILib
from performance.services.kpilib import get_list_kpilib, search_kpilib, \
    create_kpilib, update_kpilib
from utils import reload_model_obj
from django.core.urlresolvers import reverse
import json
from utils.common import to_json
from performance.models import KPI
from django.forms.models import model_to_dict
from api_v2.serializers.kpilib import KPILibSerializer


@pytest.mark.django_db(transaction=False)
def test_kpilib_no_organization():
    name = "Lib"
    objective = "Dev"
    k = KPILib(name=name, objective=objective)
    k.save()

    assert k.name == name
    assert k.objective == objective
    assert k.organization is None
    assert k.extra_data == {}


@pytest.mark.django_db(transaction=False)
def test_kpilib_has_organization(organization):
    name = "Lib"
    objective = "Dev"
    k = KPILib(name=name, objective=objective, organization=organization)
    k.save()

    assert k.organization_id == organization.id
    assert k.name == name
    assert k.objective == objective
    assert k.organization is not None
    assert k.extra_data == {}


@pytest.mark.django_db(transaction=False)
def test_kpilib_has_tags(organization):
    name = "Lib"
    objective = "Dev"
    k = KPILib(name=name, objective=objective, organization=organization)
    k.save()

    k.tags = "dev, coder, tech"
    k.save()

    assert k.organization_id == organization.id
    assert k.name == name
    assert k.objective == objective
    assert k.organization is not None
    assert k.extra_data == {}
    assert KPILib.tagged.with_all(["dev", "coder", "tech"]) is not None


@pytest.mark.django_db(transaction=False)
def test_kpilib_extra_data():
    name = "Lib"
    objective = "Dev"
    data = {
        'data1': 1,
        'data2': 2
    }
    k = KPILib(name=name, objective=objective, extra_data=data)
    k.save()

    assert k.name == name
    assert k.__unicode__() == name
    assert k.objective == objective
    assert k.organization is None
    assert k.extra_data == data


@pytest.mark.django_db(transaction=False)
def test_kpilib_delete(organization):
    name = "Lib"
    objective = "Dev"
    k = KPILib(name=name, objective=objective, organization=organization)
    k.save()

    kpi_id = k.id

    k.delete()

    assert KPILib.all_objects.filter(id=kpi_id).exists()


@pytest.mark.django_db(transaction=False)
def test_get_list_kpilib(organization):
    name = "Lib"
    objective = "Dev"
    data = {
        'data1': 1,
        'data2': 2
    }
    k = KPILib(name=name, objective=objective, extra_data=data)
    k.save()

    assert get_list_kpilib().count() == 1


@pytest.mark.django_db(transaction=False)
def test_search_kpilib(organization):
    name = "Lib"
    objective = "Dev"
    data = {
        'data1': 1,
        'data2': 2
    }
    k = KPILib(name=name, objective=objective, extra_data=data)
    k.save()

    assert search_kpilib('dev').count() == 1


@pytest.mark.django_db(transaction=False)
def test_create_kpilib(organization):
    extra_data = {
        'unit': 'unit1',
        'data_source': 'frequency1',
        'example': 'example1',
        'tips': "tips1",
        'kpi_source': 'kpi_source1',
        'category': 'financial'
    }
    data = {
        'objective': 'objective1',
        'name': 'name1',
        'description': 'description1',
        'measurement_method': 'measurement_method1',
        'operator': "<=",
    }
    data.update(extra_data)

    lib = create_kpilib(**data)

    assert lib.name == data['name']
    assert lib.objective == data['objective']
    assert lib.description == data['description']
    assert lib.measurement_method == data['measurement_method']
    assert lib.operator == data['operator']

    assert lib.extra_data['unit'] == data['unit']
    assert lib.extra_data['data_source'] == data['data_source']
    assert lib.extra_data['example'] == data['example']
    assert lib.extra_data['tips'] == data['tips']
    assert lib.extra_data['kpi_source'] == data['kpi_source']


@pytest.mark.django_db(transaction=False)
def test_update_kpilib(organization):
    data = {
        'objective': 'objective1',
        'name': 'name1',
        'description': 'description1',
        'measurement_method': 'measurement_method1',
        'operator': "<=",
        'extra_data': {
            'unit': 'unit1',
            'data_source': 'frequency1',
            'example': 'example1',
            'tips': "tips1",
            'kpi_source': 'kpi_source1'
        }
    }

    k = KPILib(**data)
    k.save()

    data = {
        'objective': 'objective updated',
        'name': 'name1 updated',
        'description': 'description1 updated',
        'measurement_method': 'measurement_method1 updated',
        'operator': "=",

        'unit': 'unit1 updated',
        'data_source': 'frequency1 updated',
        'example': 'example1 updated',
        'tips': "tips1 updated",
        'kpi_source': 'kpi_source1 updated'
    }

    update_kpilib(k.id, **data)
    k = reload_model_obj(k)

    assert k.name == data['name']
    assert k.objective == data['objective']
    assert k.description == data['description']
    assert k.measurement_method == data['measurement_method']
    assert k.operator == data['operator']

    assert k.extra_data['unit'] == data['unit']
    assert k.extra_data['data_source'] == data['data_source']
    assert k.extra_data['example'] == data['example']
    assert k.extra_data['tips'] == data['tips']
    assert k.extra_data['kpi_source'] == data['kpi_source']


@pytest.mark.django_db(transaction=False)
def test_add_kpilib(organization, client_admin, userAdmin):
    k = KPILib(name="KPI LIB", objective="Objective")
    k.save()

    data = {
        'level': 1,
        'new_template': True,
        'from_lib': True,
        'kpilib': json.dumps(KPILibSerializer(instance=k).data),
        'category': k.category
    }

    url = reverse("kpi_dashboard")
    res = client_admin.post(url, data)
    assert res.status_code == 200
    content = json.loads(res.content)
    kpi = KPI.objects.get(id=content['kpi_id'])
    assert k.name == kpi.name


@pytest.mark.django_db(transaction=False)
def test_search_kpilib_with_tags(organization):
    name = "Lib"
    objective = "Dev"
    data = {
        'data1': 1,
        'data2': 2
    }
    k = KPILib(name=name, objective=objective, extra_data=data)
    k.save()
    k.tags = "dev, it, coder"

    assert search_kpilib('dev', tags=['dev', 'it', 'coder']).count() == 1


@pytest.mark.django_db(transaction=False)
def test_KPILibManagement(organization, client_authed):
    url = reverse("kpi-lib-management")
    res = client_authed.get(url)
    assert res.status_code == 200


@pytest.mark.django_db(transaction=False)
def test_TagItem():
    pass