import glob
import hashlib
from checksumdir import dirhash

def test_files_not_changed():

    '''
    This test to make sure we don't make any changes in performance anymore since april 13 2017
     
    All changes and improvements should be in new api_v2 and using Vuejs instead of django templates 
    '''

    # assert  dirhash('performance/controllers', ignore_hidden = True) == '37774b644f0cae1df0bc508f5ee46703'
    # assert md5_file('performance/views.py') == '6be86328e340297f8c274037cbf8cca9'

def md5_file(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()
