# !/usr/bin/python
# -*- coding: utf-8 -*-
import pytest
from performance.models import KpiComment, KPI
from django.core.urlresolvers import reverse
from elms.settings import data
import json
from utils import unsigned_vi
from django.conf.locale import km
from utils.common import to_json


@pytest.mark.django_db(transaction=False)
def test_log_kpi(organization, userA_manager, userA,
                 userAdmin, client_userA_manager):

    count = 0
    url = reverse('kpi_dashboard')
    data = {
        'add_other_kpi': ''
    }
    res = client_userA_manager.post(url, json.dumps(data), content_type="application/json")
    assert res.status_code == 200
    data = json.loads(res.content)
    count += 1

    k = KPI.objects.get(id=data['kpi_id'])
    json_data = to_json(k)
    assert KpiComment.objects.filter(kpi=k).count() == count

    url = reverse('kpi_services')
    data = json_data
    data.update({
        'month_1_target': 15,
        'month_2_target': 15,
        'month_3_target': 20,
        'id': k.id,
        'command': 'update_month_target'
    })
    res = client_userA_manager.post(url, json.dumps(data), content_type="application/json")
    assert res.status_code == 200
    count += 1
    assert KpiComment.objects.filter(kpi=k).count() == count

    data = {
        'id': k.id,
        'command': 'reorder'
    }
    res = client_userA_manager.post(url, data)
    assert res.status_code == 200
    count += 1
    assert KpiComment.objects.filter(kpi=k).count() == count

    data = json_data
    data['quarter_one_target'] = 10
    data['quarter_two_target'] = 10
    data['quarter_three_target'] = 10
    data['quarter_four_target'] = 10
    data['command'] = 'update_quarter_target'

    res = client_userA_manager.post(url, json.dumps(data), content_type="application/json")
    assert res.status_code == 200
    count += 1
    assert KpiComment.objects.filter(kpi=k).count() == count

    data = to_json(k)
    data.update({
        'month_1': 10,
        'month_2': 10,
        'month_3': 10,
        'real': 10,
        'operator': '>='
    })
    url = reverse("kpi_update_score")
    res = client_userA_manager.post(url, data)
    assert res.status_code == 200
    count += 1
    assert KpiComment.objects.filter(kpi=k).count() == count

    url = reverse('kpi_services')
    data = {
        'id': k.id,
        'command': 'delay_toggle',
        'reason': 'delay'
    }
    res = client_userA_manager.post(url, data)
    assert res.status_code == 200
    count += 1
    assert KpiComment.objects.filter(kpi=k).count() == count
