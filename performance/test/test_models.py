import hashlib
from random import randint

import datetime
from time import sleep

import pytest
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.cache import cache
from django.utils.dateformat import format
from django.utils.text import slugify
from performance.db_models.strategy_map import StrategyMap, GroupKPI
from performance.models import QuarterPeriod, ugettext, Review, ReviewProgress, KPI, KpiComment, update_group_name, \
    update_kpi_same_unique_code, update_targets_from_quarter_nothread, are_other_children_delayed, KPILock, \
    relog_user_score_all, KPIData, get_parent_kpis_by_category, get_kpi_id
from performance.services import create_child_kpi
from utils import random_string, reload_model_obj

from utils.kpi_utils import kpi_score_calculator
from django.forms import DateTimeField
from django.test.utils import override_settings

# from utils.kpi_utils import kpi_score_calculator


@pytest.mark.django_db(transaction=False)
def test_month_1_name(organization):
    '''
        quarter = 1 return month_1_name = January
    '''

    quarter = 1
    period = QuarterPeriod(organization_id=organization.id, quarter_name='test', quarter=quarter, due_date=datetime.datetime.now())
    period.save()
    p = QuarterPeriod.objects.filter(organization_id=organization.id).first()
    assert p.month_1_name() == ugettext('January')

    '''
        quarter = 2 return month_1_name = April
    '''

    quarter = 2
    p = QuarterPeriod.objects.filter(organization_id=organization.id).first()
    p.quarter = quarter
    p.save()
    assert p.month_1_name() == ugettext('April')

    '''
        quarter = 3 return month_1_name = July
    '''
    quarter = 3
    p = QuarterPeriod.objects.filter(organization_id=organization.id).first()
    p.quarter = quarter
    p.save()
    assert p.month_1_name() == ugettext('July')

    '''
            quarter different 1,2,3 return month_1_name = October
    '''

    quarter = randint(4, 10)
    p = QuarterPeriod.objects.filter(organization_id=organization.id).first()
    p.quarter = quarter
    p.save()
    assert p.month_1_name() == ugettext('October')


@pytest.mark.django_db(transaction=False)
def test_month_2_name(organization):
    '''
        quarter = 1 return month_1_name = February
    '''

    quarter = 1
    period = QuarterPeriod(organization_id=organization.id, quarter_name='test', quarter=quarter, due_date=datetime.datetime.now())
    period.save()
    p = QuarterPeriod.objects.filter(organization_id=organization.id).first()
    assert p.month_2_name() == ugettext('February')

    '''
        quarter = 2 return month_1_name = May
    '''

    quarter = 2
    p = QuarterPeriod.objects.filter(organization_id=organization.id).first()
    p.quarter = quarter
    p.save()
    assert p.month_2_name() == ugettext('May')

    '''
        quarter = 3 return month_1_name = August
    '''
    quarter = 3
    p = QuarterPeriod.objects.filter(organization_id=organization.id).first()
    p.quarter = quarter
    p.save()
    assert p.month_2_name() == ugettext('August')

    '''
            quarter different 1,2,3 return month_1_name = November
    '''

    quarter = randint(4, 10)
    p = QuarterPeriod.objects.filter(organization_id=organization.id).first()
    p.quarter = quarter
    p.save()
    assert p.month_2_name() == ugettext('November')


@pytest.mark.django_db(transaction=False)
def test_month_3_name(organization):
    '''
        quarter = 1 return month_1_name = March
    '''

    quarter = 1
    period = organization.get_current_quarter()
    period.quarter = quarter
    period.save()
    assert period.month_3_name() == ugettext('March')

    '''
        quarter = 2 return month_1_name = June
    '''

    quarter = 2
    period = organization.get_current_quarter()
    period.quarter = quarter
    period.save()
    assert period.month_3_name() == ugettext('June')

    '''
        quarter = 3 return month_1_name = September
    '''
    quarter = 3
    period = organization.get_current_quarter()
    period.quarter = quarter
    period.save()
    assert period.month_3_name() == ugettext('September')

    '''
            quarter different 1,2,3 return month_1_name = December
    '''

    quarter = randint(4, 10)
    period = organization.get_current_quarter()
    period.quarter = quarter
    period.save()
    assert period.month_3_name() == ugettext('December')


@pytest.mark.django_db(transaction=False)
def test_get_previous_quarter(organization):
    quarter = 1
    period = QuarterPeriod(organization_id=organization.id, quarter_name='test', quarter=quarter,
                           due_date=datetime.datetime.now())
    period.save()
    previous_quarter = period.get_previous_quarter()
    assert previous_quarter is not None


@pytest.mark.django_db(transaction=False)
def test_to_json_period(organization):
    '''
       quarter = 1
    '''
    quarter = 1
    period = organization.get_current_quarter()
    period.quarter = quarter
    period.save()
    js = period.to_json()
    assert js['month_1_name'] == ugettext('January')
    assert js['month_2_name'] == ugettext('February')
    assert js['month_3_name'] == ugettext('March')

    '''
       quarter = 2
    '''
    quarter = 2
    period = organization.get_current_quarter()
    period.quarter = quarter
    period.save()
    js = period.to_json()
    assert js['month_1_name'] == ugettext('April')
    assert js['month_2_name'] == ugettext('May')
    assert js['month_3_name'] == ugettext('June')

    '''
           quarter = 3
        '''
    quarter = 3
    period = organization.get_current_quarter()
    period.quarter = quarter
    period.save()
    js = period.to_json()
    assert js['month_1_name'] == ugettext('July')
    assert js['month_2_name'] == ugettext('August')
    assert js['month_3_name'] == ugettext('September')


@pytest.mark.django_db(transaction=False)
def test_get_strategy_map(organization):

    period = QuarterPeriod(organization_id=organization.id, quarter_name='test', quarter=1,
                           due_date=datetime.date.today())
    period.save()
    stra = StrategyMap(quarter_period=period, name='test', organization=organization)
    stra.save()
    assert period.get_strategy_map() == stra
    assert period.get_strategy_map().name == 'test'


@pytest.mark.django_db(transaction=False)
def test_is_expired(organization):
    '''
        is_expired is True
    '''
    due_date = datetime.date.today() + datetime.timedelta(days=-5)
    period = QuarterPeriod(organization_id=organization.id, quarter_name='test', quarter=1,
                           due_date=due_date)
    period.save()
    assert period.is_expired() is True

    '''
        is_expired is False
    '''
    due_date1 = datetime.date.today() + datetime.timedelta(days=+5)
    period = QuarterPeriod(organization_id=organization.id, quarter_name='test', quarter=1,
                           due_date=due_date1)
    period.save()
    assert period.is_expired() is False


@pytest.mark.django_db(transaction=False)
def test_update_quarter(organization):
    period = QuarterPeriod(organization_id=organization.id, quarter_name='test', quarter=1,
                           due_date=datetime.date.today())
    period.save()
    assert period.update_quarter() == 1


@pytest.mark.django_db(transaction=False)
def test_to_json_review(organization, userA):
    period = QuarterPeriod(organization_id=organization.id, quarter_name='test', quarter=1,
                           due_date=datetime.date.today())
    period.save()
    review = Review(organization=organization, reviewer=userA, quarter_period=period)
    review.save()
    js = review.to_json()
    assert js['is_complete'] is False
    assert js['quarter'] == 1


@pytest.mark.django_db(transaction=False)
def test_unicode_review(organization, userA):
    period = organization.get_current_quarter()
    review = Review(organization=organization, reviewer=userA, quarter_period=period)
    review.save()
    assert review.__unicode__() == str(userA.username) + ' - '

#
# @pytest.mark.django_db(transaction=False)
# def test_unicode_review(organization, userA):
#     period = QuarterPeriod(organization_id=organization.id, quarter_name='test', quarter=1,
#                            due_date=datetime.date.today())
#     period.save()
#     review = Review(organization=organization, reviewer=userA, quarter_period=period)
#     review.save()
#     key = userA.username + ' - ' +


@pytest.mark.django_db(transaction=False)
def test_get_category_order(kpi):
    '''
         financial
    '''
    kpi.bsc_category = 'financial'
    kpi.save()
    assert kpi.get_category_order() == 1

    '''
        customer
    '''
    kpi.bsc_category = 'customer'
    kpi.save()
    assert kpi.get_category_order() == 2

    '''
        internal
    '''
    kpi.bsc_category = 'internal'
    kpi.save()
    assert kpi.get_category_order() == 3

    '''
            learninggrowth
    '''
    kpi.bsc_category = 'learninggrowth'
    kpi.save()
    assert kpi.get_category_order() == 4

    '''
        other
    '''

    kpi.bsc_category = 'test'
    kpi.save()
    assert kpi.get_category_order() == 5


@pytest.mark.django_db(transaction=False)
def test_get_prefix_category(kpi):
    '''
             financial
        '''
    kpi.bsc_category = 'financial'
    kpi.save()
    assert kpi.get_prefix_category() == 'F'

    '''
        customer
    '''
    kpi.bsc_category = 'customer'
    kpi.save()
    assert kpi.get_prefix_category() == 'C'

    '''
        internal
    '''
    kpi.bsc_category = 'internal'
    kpi.save()
    assert kpi.get_prefix_category() == 'P'

    '''
            learninggrowth
    '''
    kpi.bsc_category = 'learninggrowth'
    kpi.save()
    assert kpi.get_prefix_category() == 'L'

    '''
        other
    '''

    kpi.bsc_category = 'test'
    kpi.save()
    assert kpi.get_prefix_category() == 'O'


@pytest.mark.django_db(transaction=False)
def test_get_current_month_target(kpi):
    '''
        * month = 1,2,3
    '''
    '''
        month_1
    '''
    kpi.score_calculation_type = 'most_recent'
    kpi.month_1_target = randint(1, 100)
    kpi.save()
    assert kpi.get_current_month_target(month=1) == kpi.get_month_1_target()

    '''
        month_2
    '''
    kpi.score_calculation_type = 'most_recent'
    kpi.month_2_target = randint(1, 100)
    kpi.save()
    assert kpi.get_current_month_target(month=2) == kpi.get_month_2_target()

    '''
        month_3
    '''
    kpi.score_calculation_type = 'most_recent'
    kpi.month_3_target = randint(1, 100)
    kpi.save()
    assert kpi.get_current_month_target(month=3) == kpi.get_month_3_target()

    '''
        * month is none
    '''
    '''
        month_1
    '''
    kpi.score_calculation_type = 'most_recent'
    kpi.month_1 = randint(1, 100)
    kpi.save()
    assert kpi.get_current_month_target() == kpi.get_month_1_target()

    '''
        month_2
    '''
    kpi.score_calculation_type = 'most_recent'
    kpi.month_2 = randint(1, 100)
    kpi.save()
    assert kpi.get_current_month_target() == kpi.get_month_2_target()

    '''
        month_3
    '''
    kpi.score_calculation_type = 'most_recent'
    kpi.month_3 = randint(1, 100)
    kpi.save()
    assert kpi.get_current_month_target() == kpi.get_month_3_target()

    '''
        score_calculation_type != 'most_recent'
    '''
    kpi.score_calculation_type = None
    kpi.target = randint(1, 100)
    kpi.save()
    assert kpi.get_current_month_target() == kpi.target


@pytest.mark.django_db(transaction=False)
def test_get_current_month_real(kpi):
    '''
            * month is none
    '''
    '''
        month_1
    '''

    kpi.score_calculation_type = 'most_recent'
    kpi.month_1 = randint(1, 100)
    kpi.save()
    assert kpi.get_current_month_real() == kpi.month_1

    '''
        month_2
    '''
    kpi.score_calculation_type = 'most_recent'
    kpi.month_2 = randint(1, 100)
    kpi.save()
    assert kpi.get_current_month_real() == kpi.month_2

    '''
        month_3
    '''
    kpi.score_calculation_type = 'most_recent'
    kpi.month_3 = randint(1, 100)
    kpi.save()
    assert kpi.get_current_month_real() == kpi.month_3

    '''
    month in not None
    '''
    '''
        month_1
    '''
    kpi.score_calculation_type = 'most_recent'
    kpi.month_1 = randint(1, 100)
    kpi.save()
    assert kpi.get_current_month_real(month=1) == kpi.month_1

    '''
        month_2
    '''
    kpi.score_calculation_type = 'most_recent'
    kpi.month_2 = randint(1, 100)
    kpi.save()
    assert kpi.get_current_month_real(month=2) == kpi.month_2

    '''
        month_3
    '''
    kpi.score_calculation_type = 'most_recent'
    kpi.month_3 = randint(1, 100)
    kpi.save()
    assert kpi.get_current_month_real(month=3) == kpi.month_3

    '''
        other
    '''
    kpi1 = KPI()
    kpi1.score_calculation_type = 'most_recent'
    kpi1.real = randint(1, 100)
    kpi1.save()
    assert kpi1.get_current_month_real() == kpi1.real

    '''
        score_calculation_type != 'most_recent'
    '''
    kpi.score_calculation_type = None
    kpi.target = randint(1, 100)
    kpi.save()
    assert kpi.get_current_month_real() == kpi.target


@pytest.mark.django_db(transaction=False)
def test_cascaded_from_deleted(organization, userA, userA_manager):
    '''
        cascaded_from is none
    '''
    k1 = KPI()
    k1.quarter_period = organization.get_current_quarter()
    k1.user = userA_manager
    k1.save()

    k2 = k1.assign_to(userA)
    assert k2.cascaded_from_deleted() is False

    cascaded_id = k2.cascaded_from.id
    KPI.objects.get(id=cascaded_id).delete()
    assert k2.cascaded_from_deleted() is True

    '''
        cascaded_from is not None
    '''
    k = KPI()
    k.user = userA_manager
    k.save()
    k1 = k.assign_to(userA)
    assert k1.cascaded_from_deleted() is False


@pytest.mark.django_db(transaction=False)
def test_newbie_to_json(parent_kpi, userA, userA_manager):

    k1 = KPI()
    k1.parent_id = parent_kpi.id
    k1.user = userA_manager
    k1.save()
    '''
        kpi have parent_id
        assigned_to is None
    '''
    assert k1.newbie_to_json()['assigned_to'] is None

    '''
        kpi have parent_id
        assigned_to not None
    '''
    k2 = k1.assign_to(userA)
    k3 = k2.cascaded_from
    js = k3.newbie_to_json()
    assert js['assigned_to'] is not None
    assert k1.newbie_to_json()['assigned_to']['user_id'] == userA.profile.user_id

    '''
        kpi not have parent_id
    '''
    k = KPI()
    k.save()
    js1 = k.newbie_to_json()
    assert 'assigned_to' not in js1


@pytest.mark.django_db(transaction=False)
def test_get_month_1_target():
    kpi = KPI()
    kpi.target = 10
    kpi.score_calculation_type = 'sum'
    kpi.review_type = 'test'
    result = kpi.get_month_1_target()
    month_1_target = kpi.target * (1.0 / 3.0)
    assert result == month_1_target

    kpi.score_calculation_type = 'average'
    kpi.month_1_target = None
    kpi.save()
    kpi.get_month_1_target()
    k1 = KPI.objects.get(id=kpi.id)
    assert k1.month_1_target == kpi.target


@pytest.mark.django_db(transaction=False)
def test_get_month_2_target():
    kpi = KPI()
    kpi.target = 10
    kpi.score_calculation_type = 'sum'
    kpi.review_type = 'test'
    result = kpi.get_month_2_target()
    month_2_target = kpi.target * (1.0 / 3.0)
    assert result == month_2_target

    kpi.score_calculation_type = 'average'
    kpi.month_2_target = None
    kpi.save()
    kpi.get_month_2_target()
    k1 = KPI.objects.get(id=kpi.id)
    assert k1.month_2_target == kpi.target


@pytest.mark.django_db(transaction=False)
def test_get_month_3_target():
    kpi = KPI()
    kpi.target = 10
    kpi.score_calculation_type = 'sum'
    kpi.review_type = 'test'
    result = kpi.get_month_3_target()
    month_3_target = kpi.target * (1.0 / 3.0)
    assert result == month_3_target

    kpi.score_calculation_type = 'average'
    kpi.month_3_target = None
    kpi.save()
    kpi.get_month_3_target()
    k1 = KPI.objects.get(id=kpi.id)
    assert k1.month_3_target == kpi.target


@pytest.mark.django_db(transaction=False)
def test_get_line_up(kpi, parent_kpi, userA, organization):
    kpi_list = [kpi, parent_kpi]
    result = kpi._get_line_up(kpi, kpi_list)
    assert result['break_point'] == parent_kpi

    k1 = KPI()
    k1.name = "Root kpi"
    k1.user = userA
    k1.quarter_period = organization.get_current_quarter()
    k1.save()

    parent_kpi.parent = k1
    parent_kpi.refer_to = k1
    parent_kpi.save()
    kpi_list = [kpi, parent_kpi, k1]
    result = kpi._get_line_up(parent_kpi, kpi_list)
    assert result['break_point'] == k1


@pytest.mark.django_db(transaction=False)
def test_kpi_debug_info(kpi):
    kpi_id = randint(1, 10)
    data = kpi.kpi_debug_info(kpi_id=kpi_id)
    assert data['msg'] == 'NOT FOUND!'

    data = kpi.kpi_debug_info(kpi.id)
    assert data['id'] == kpi.id
    assert data['name'] == kpi.name


@pytest.mark.django_db(transaction=False)
def test_update_kpi_same_unique_code(kpi, organization):
    kpi.unique_code = 'TEST'
    kpi.quarter_period = organization.get_current_quarter()
    kpi.month_1 = 1
    kpi.month_2 = 2
    kpi.month_3 = 3
    kpi.month_1_score = 10
    kpi.month_2_score = 20
    kpi.month_3_score = 30
    kpi.save()

    k1 = KPI()
    k1.unique_code = 'TEST'
    k1.quarter_period = organization.get_current_quarter()
    k1.save()

    k2 = KPI()
    k2.quarter_period = organization.get_current_quarter()
    k2.unique_code = 'TEST'
    k2.save()

    update_kpi_same_unique_code(kpi=kpi)
    sleep(2)
    kpi1 = reload_model_obj(k1)
    kpi2 = reload_model_obj(k2)
    assert kpi1.month_1 == 1
    assert kpi2.month_1 == 1
    assert kpi1.month_2 == 2
    assert kpi2.month_2 == 2


@pytest.mark.django_db(transaction=False)
def test_update_targets_from_quarter_nothread(kpi, organization, userA):
    kpi1 = KPI()
    kpi1.user = userA
    kpi1.quarter_period = organization.get_current_quarter()
    kpi1.score_calculation_type = 'average'
    kpi1.quarter_period.quarter = 1
    kpi1.save()
    kpi1.quarter_one_target = 10
    update_targets_from_quarter_nothread(kpi1)
    assert kpi1.target == 10

    kpi2 = KPI()
    kpi2.user = userA
    kpi2.quarter_period = organization.get_current_quarter()
    kpi2.score_calculation_type = 'average'
    kpi2.quarter_period.quarter = 2
    kpi2.save()
    kpi2.quarter_two_target = 5
    update_targets_from_quarter_nothread(kpi2)
    assert kpi2.target == 5

    kpi3 = KPI()
    kpi3.user = userA
    kpi3.quarter_period = organization.get_current_quarter()
    kpi3.score_calculation_type = 'average'
    kpi3.quarter_period.quarter = 3
    kpi3.save()
    kpi3.quarter_three_target = 15
    update_targets_from_quarter_nothread(kpi3)
    assert kpi3.target == 15

    kpi4 = KPI()
    kpi4.user = userA
    kpi4.quarter_period = organization.get_current_quarter()
    kpi4.score_calculation_type = 'average'
    kpi4.quarter_period.quarter = 4
    kpi4.save()
    kpi4.quarter_four_target = 20
    update_targets_from_quarter_nothread(kpi4)
    assert kpi4.target == 20


@pytest.mark.django_db(transaction=False)
def test_update_review_mode():
    '''
        have most_recent and monthly
    '''
    k2 = KPI()
    k2.month_1 = 100
    k2.month_1_target = 100

    k2.month_2_target = 100
    k2.month_2 = 80
    k2.month_3_target = 30
    k2.month_3 = 15
    k2.save()
    k2 = k2.update_review_mode('most_recent', 'monthly')

    assert k2.get_month_1_score() == 100
    assert k2.get_month_2_score() == 80
    assert k2.get_month_3_score() == 50


@pytest.mark.django_db(transaction=False)
def test_kpi_type_error(userA, userA_manager):
    '''
        KPI trung gian
    '''
    k2 = KPI()
    k2.save()
    k = KPI()
    k.user = userA_manager
    k.save()
    k1 = k.assign_to(userA)
    cascaded_kpi = k1.cascaded_from
    cascaded_kpi.refer_to = None
    result = cascaded_kpi.kpi_type_error()
    assert result['is_error'] is False

    cascaded_kpi.refer_to = k
    result = cascaded_kpi.kpi_type_error()
    assert result['suggest'] == 'refer_to should be None'

    cascaded_kpi.cascaded_from_id = 123
    result = cascaded_kpi.kpi_type_error()
    assert result['suggest'] == 'cascaded_from should be None'

    '''
        KPI cha truc tiep,
    '''
    # cha truc tiep
    result = k.kpi_type_error()
    assert result['is_error'] is False

    k1.cascaded_from.parent = k2
    result = k1.kpi_type_error()
    key = "KPI shoulded be: {}".format("self.cascaded_from.parent == self.refer_to",)
    assert result['best_matched'] == 'chacongiantiep'
    assert result['suggest'] == key

    #
    k1.cascaded_from.parent = k
    k1.refer_to = k
    result = k1.kpi_type_error()
    assert result['is_error'] is False

    #
    k1.refer_to = None
    result = k1.kpi_type_error()
    assert result['best_matched'] == 'chacongiantiep'
    assert result['suggest'] == 'refer_to should be Not None'
    #

    k1.cascaded_from_id = 0
    k1.refer_to = None
    result = k1.kpi_type_error()
    assert result['best_matched'] == 'chatructiep'
    assert result['suggest'] == 'refer_to should be None'

    '''
        con truc tiep, con gian tiep
    '''
    #
    k1.cascaded_from = None
    k1.parent = k
    k1.refer_to = k
    result = k1.kpi_type_error()
    assert result['is_error'] is False

    #
    k1.parent = k2
    result = k1.kpi_type_error()
    key = 'KPI should be {}'.format('self.refer_to==self.parent',)
    assert result['best_matched'] == 'contructiep'
    assert result['suggest'] == key

    #
    #
    k1.refer_to = None
    k1.parent_id = None
    result = k1.kpi_type_error()
    assert result['best_matched'] == 'contructiep'
    assert result['suggest'] == 'parent should be Not None'

    k1.parent_id = k.id
    result = k1.kpi_type_error()
    assert result['best_matched'] == 'contructiep'
    assert result['suggest'] == 'refer_to should be Not None'


@pytest.mark.django_db(transaction=False)
def test_update_score(userA, userA_manager, organization):
    '''
        kpi not have children
    '''

    quarter = organization.get_current_quarter()

    parent_kpi = KPI()
    parent_kpi.user = userA_manager
    parent_kpi.real = 10
    parent_kpi.target = 10
    parent_kpi.month_1 = 10
    parent_kpi.month_1_target = 10
    parent_kpi.operator = '>='
    parent_kpi.month_1_score = 10
    parent_kpi.month_2_score = 10
    parent_kpi.month_3_score = 10
    parent_kpi.quarter_period = quarter
    parent_kpi.save()

    k1 = KPI()
    k1.user = userA_manager
    k1.real = 10
    k1.target = 10
    k1.operator = '>='
    k1.month_1 = 10
    k1.month_1_target = 10
    k1.month_1_score = 10
    k1.month_2_score = 10
    k1.month_3_score = 10
    k1.quarter_period = quarter
    k1.save()
    score = kpi_score_calculator(k1.operator, k1.target, k1.real, k1)
    k1.update_score()
    k1 = reload_model_obj(k1)
    assert k1.latest_score == score

    '''
        kpi have children
    '''
    k1.assign_to(userA)
    score1, m1, m2, m3 = k1.calculate_avg_score(k1.user_id)
    k1.update_score()
    assert k1.latest_score == score1

    '''
        kpi have parent
    '''
    k1.parent = parent_kpi
    k1.refer = parent_kpi
    k1.save()
    #
    # k1.cascaded_from_id = parent_kpi.id
    # k1.save()
    parent_kpi.month_1_score = 10
    parent_kpi.month_2_score = 10
    parent_kpi.month_3_score = 10
    parent_kpi.save()

    # Note: Hong remove since it's not clear why test this for what user case:
    score3, m1, m2, m3 = parent_kpi.calculate_avg_score(k1.user_id)
    k1.update_score()
    assert k1.latest_score == score3

    ckpi = KPI()
    ckpi.user = userA
    ckpi.latest_score = randint(0, 100)
    ckpi.cascaded_from_id = parent_kpi.id
    ckpi.month_1_score = 10
    ckpi.month_2_score = 10
    ckpi.month_3_score = 10
    ckpi.save()
    score3, m1, m2, m3 = k1.calculate_avg_score(ckpi.user_id)
    ckpi.update_score()
    assert k1.latest_score == score3

# QUOCDUAN: comment out this test, because the method update_score already commented out
# @pytest.mark.django_db(transaction=False)
# def test_update_score(userA, userA_manager, parent_kpi):
#     '''
#         kpi not have children
#     '''
#     kpi = KPI()
#     kpi.user = userA_manager
#     kpi.real = 10
#     kpi.target = 10
#     kpi.operator = '>='
#     kpi.month_1_score = 10
#     kpi.month_2_score = 10
#     kpi.month_3_score = 10
#     kpi.save()
#     score = kpi_score_calculator(kpi.operator, kpi.target, kpi.real, kpi)
#     kpi.update_score()
#     assert kpi.latest_score == score
#
#     '''
#         kpi have children
#     '''
#     kpi.assign_to(userA)
#     score1, m1, m2, m3 = kpi.calculate_avg_score(kpi.user_id)
#     kpi.update_score()
#     assert kpi.latest_score == score1
#
#     '''
#         kpi have parent
#     '''
#     parent_kpi.month_1_score = 10
#     parent_kpi.month_2_score = 10
#     parent_kpi.month_3_score = 10
#     parent_kpi.save()
#     score3, m1, m2, m3 = parent_kpi.calculate_avg_score(kpi.user_id)
#     kpi.update_score()
#     assert kpi.latest_score == score3


@pytest.mark.django_db(transaction=False)
def test_update_quarter_targets(userA, userA_manager, parent_kpi, kpi):

    kpi.quarter_one_target = 5
    kpi.quarter_two_target = 10
    kpi.quarter_three_target = 15
    kpi.quarter_four_target = 20
    kpi.target = 25
    kpi.save()
    '''
        quarter = 1
    '''
    kpi.quarter_period.quarter = 1
    kpi.target = 0
    new_kpi = kpi.update_quarter_targets()
    assert new_kpi.quarter_one_target == 0

    '''
        quarter = 2
    '''
    kpi.quarter_period.quarter = 2
    kpi.target = 5
    new_kpi = kpi.update_quarter_targets()
    assert new_kpi.quarter_two_target == 5

    '''
        quarter = 3
    '''
    kpi.quarter_period.quarter = 3
    kpi.target = 10
    new_kpi = kpi.update_quarter_targets()
    assert new_kpi.quarter_three_target == 10

    '''
        quarter =4
    '''
    kpi.quarter_period.quarter = 4
    kpi.target = 15
    new_kpi = kpi.update_quarter_targets()
    assert new_kpi.quarter_four_target == 15

    '''
        quarter other
    '''
    kpi.quarter_period.quarter = 5
    kpi.target = 20
    new_kpi = kpi.update_quarter_targets()
    assert new_kpi.quarter_four_target == 20
    assert new_kpi.quarter_period.quarter == 1


@pytest.mark.django_db(transaction=False)
def test_update_cascade_target(userA, userA_manager, parent_kpi, kpi):
    k = KPI()
    k.user = userA_manager
    k.save()
    k1 = k.assign_to(userA)
    kpi = k1.cascaded_from

    k1.quarter_one_target = 5
    k1.quarter_two_target = 10
    k1.quarter_three_target = 15
    k1.quarter_four_target = 20
    k1.save()
    '''
        quarter = 1
    '''
    quarter = k1.quarter_period
    quarter.quarter = 1
    quarter.save()
    kpi.update_cascade_target(0)
    k1 = reload_model_obj(k1)
    assert k1.quarter_one_target == 0

    '''
        quarter = 2
    '''
    quarter = k1.quarter_period
    quarter.quarter = 2
    quarter.save()
    kpi.update_cascade_target(5)
    k1 = reload_model_obj(k1)
    assert k1.quarter_two_target == 5

    '''
        quarter = 3
    '''
    quarter = k1.quarter_period
    quarter.quarter = 3
    quarter.save()
    kpi.update_cascade_target(10)
    k1 = reload_model_obj(k1)
    assert k1.quarter_three_target == 10

    '''
        quarter >=4
    '''
    quarter = k1.quarter_period
    quarter.quarter = 4
    quarter.save()
    kpi.update_cascade_target(15)
    k1 = reload_model_obj(k1)
    assert k1.quarter_four_target == 15


@pytest.mark.django_db(transaction=False)
def test_get_root_kpi_by_search_terms(userA, organization, parent_kpi, kpi):
    '''
        not search_terms
    '''
    current_quarter = organization.get_current_quarter()
    category = 'financial'
    result = kpi.get_root_kpi_by_search_terms(search_terms=None, user_id=userA.profile.user_id, quarter_period_id=current_quarter, category=category)
    assert result == {}
    '''
        category == 'other'
    '''
    category = 'other'
    kpi.bsc_category = 'other'
    search_terms = 'Child KPI'
    kpi.save()
    result = kpi.get_root_kpi_by_search_terms(search_terms=search_terms, user_id=userA.profile.user_id, quarter_period_id=current_quarter, category=category)
    assert len(result) > 0

    '''
        category = 'financial'
    '''
    category = 'financial'
    kpi.bsc_category = 'financial'
    search_terms = 'Child KPI'
    kpi.save()
    result = kpi.get_root_kpi_by_search_terms(search_terms=search_terms, user_id=userA.profile.user_id,
                                              quarter_period_id=current_quarter, category=category)
    assert len(result) > 0

    '''
        reviewer_email
    '''
    category = 'financial'
    kpi.bsc_category = 'financial'
    kpi.reviewer_email = userA.email
    search_terms = 'reviewer_email:' + userA.email
    kpi.save()
    result = kpi.get_root_kpi_by_search_terms(search_terms=search_terms, user_id=userA.profile.user_id,
                                              quarter_period_id=current_quarter, category=category)
    assert len(result) > 0

    '''
           id
    '''
    category = 'financial'
    kpi.bsc_category = 'financial'
    search_terms = 'id:' + str(kpi.id)
    kpi.save()
    result = kpi.get_root_kpi_by_search_terms(search_terms=search_terms, user_id=userA.profile.user_id,
                                              quarter_period_id=current_quarter, category=category)
    assert len(result) > 0

    '''
           email
    '''
    category = 'financial'
    kpi.bsc_category = 'financial'
    search_terms = 'email:' + userA.email
    kpi.save()
    result = kpi.get_root_kpi_by_search_terms(search_terms=search_terms, user_id=userA.profile.user_id,
                                              quarter_period_id=current_quarter, category=category)
    assert len(result) > 0

    category = 'other'
    kpi.bsc_category = 'other'
    search_terms = 'KPI'
    kpi.save()
    result = kpi.get_root_kpi_by_search_terms(search_terms=search_terms, user_id=userA.profile.user_id,
                                              quarter_period_id=current_quarter, category=category)
    assert parent_kpi in result
    kpi.refer_to_id = None
    kpi.refer_to = parent_kpi
    kpi.save()
    parent_kpi.refer_to_id = None
    parent_kpi.save()
    result = kpi.get_root_kpi_by_search_terms(search_terms=search_terms, user_id=userA.profile.user_id,
                                              quarter_period_id=current_quarter, category=category)
    assert parent_kpi in result


@pytest.mark.django_db(transaction=False)
def test_kpi_type(userA, userA_manager, parent_kpi, kpi):

    '''
        kpi trung gian
    '''

    k1 = KPI()
    k1.name = 'kpi test'
    k1.parent = parent_kpi
    k1.user = userA_manager
    k1.save()
    k1.assign_to(user=userA)
    assert k1.kpi_type() == 'trunggian'

    '''
        kpi cha truc tiep
    '''
    k2 = KPI()
    k2.name = 'kpi test'
    k2.user = userA_manager
    k2.save()
    k2.assign_to(user=userA_manager)
    assert k2.kpi_type() == 'chatructiep'

    '''
        con truc tiep
    '''
    k3 = KPI()
    k3.name = 'kpi test'
    k3.parent = parent_kpi
    k3.user = userA_manager
    k3.refer_to_id = k3.parent_id
    k3.save()
    assert k3.kpi_type() == 'contructiep'

    '''
        cha con gian tiep
    '''
    k4 = KPI()
    k4.name = 'kpi test'
    k4.user = userA_manager
    k4.cascaded_from = kpi
    k4.refer_to = parent_kpi
    k4.save()

    assert k4.kpi_type() == 'chacongiantiep'

    '''
        unknown
    '''
    k5 = KPI()
    k5.name = 'kpi test'
    k5.refer_to_id = parent_kpi.id
    k5.save()
    assert k5.kpi_type() == 'unknown'


@pytest.mark.django_db(transaction=False)
def test_month_score_rounded(kpi):
    '''
        month_1_score

    '''

    kpi.month_1_score = None
    assert kpi.month_1_score_rounded() == 0
    kpi.month_1_score = 10.567
    assert kpi.month_1_score_rounded() == 10.57

    '''
        month_2_score
    '''
    kpi.month_2_score = None
    assert kpi.month_2_score_rounded() == 0
    kpi.month_2_score = 9.123
    assert kpi.month_2_score_rounded() == 9.12

    '''
        month_3_score
    '''
    kpi.month_3_score = None
    assert kpi.month_3_score_rounded() == 0
    kpi.month_3_score = 7.567
    assert kpi.month_3_score_rounded() == 7.57


@pytest.mark.django_db(transaction=False)
def test_get_quarter(organization, userA):
    '''
        kpi have quarter_period
    '''
    k1 = KPI()
    current_quarter = organization.get_current_quarter()
    k1.quarter_period = current_quarter
    k1.save()
    assert k1.get_quarter() == current_quarter

    '''
        kpi not have quarter_period
    '''
    k2 = KPI()
    k2.user = userA
    k2.save()
    current_quarter1 = k2.user.profile.get_organization().get_current_quarter()
    assert k2.get_quarter() == current_quarter1


@pytest.mark.django_db(transaction=False)
def test_clone_kpi_to_user(organization, userB, kpi):
    quarter_period = organization.get_current_quarter()
    new_kpi = kpi.clone_kpi_to_user(userB, quarter_period=quarter_period)
    assert new_kpi.user == userB


#
# @pytest.mark.django_db(transaction=False)
# def test_clone_kpi_to_user(organization, userB, kpi):
@pytest.mark.django_db(transaction=False)
def test_group_name(organization, kpi, parent_kpi, userA, userA_manager):

    @override_settings(
        CACHES={
            'default': {
                'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
                'LOCATION': 'unique-snowflake',
            }
        }
    )
    def test_runner():
        kpi.refer_to_id = kpi.parent_id
        kpi.save()
        assert kpi.refer_group_name == kpi.name

        update1 = update_group_name(kpi, _root_id=kpi.parent_id)
        assert update1 == kpi.refer_group_name
        kpi.refer_to_id = kpi.parent_id
        kpi.save()

        kpi2 = KPI()
        kpi2.name = "test"
        kpi2.refer_to = kpi
        kpi2.quarter_period = organization.get_current_quarter()
        kpi2.user = userA_manager
        kpi2.save()

        pkpi = kpi.parent
        pkpi.refer_to_id = kpi2.id
        pkpi.save()
        update2 = update_group_name(kpi)
        assert update2 == pkpi.name

        key = "get_group_name{0}_{1}".format(kpi.id, kpi.parent_id)
        cache.set(key, kpi.name, 60 * 5)
        assert update_group_name(kpi, kpi.parent_id) == cache.get(key, '')

    test_runner()


@pytest.mark.django_db(transaction=False)
def test_get_group(kpi, userA, parent_kpi, organization, userC, organization2):

    @override_settings(
        CACHES={
            'default': {
                'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
                'LOCATION': 'unique-snowflake',
            }
        }
    )
    def test_runner():
        strategymap = StrategyMap(
            organization=organization,
            year=2017,
            director=userA,
            name=random_string(),
            description=random_string(),
            quarter_period=userA.profile.get_current_quarter(),
            created_at=DateTimeField()
        )
        strategymap.save()

        group_kpi = GroupKPI(
            map=strategymap,
            name=random_string()
        )

        group_kpi.save()

        kpi.group_kpi = None
        kpi.save()
        result = kpi.get_group()
        assert result == kpi.group_kpi

        k1 = KPI()
        k1.save()
        key = 'get_group{}'.format(k1.id)
        cache.set(key, group_kpi, 60 * 5)
        result = k1.get_group()
        assert result == group_kpi

        kpi.group_kpi = group_kpi
        kpi.save()
        result = kpi.get_group()
        assert result == kpi.group_kpi

        # ## test case: user have no organization, user have kpi k2 and k2.group_kpi is None
        # ## ==> test that when call k2.get_group() does not create new group for the kpi k2

        # create User who not belong to any organization
        # also, create kpi for the user
        k2 = KPI()
        k2.user = userC
        k2.save()
        # assert that get_group() does not create a group for the kpi since user have not belong to any ogranization
        assert k2.get_group() is None

    test_runner()


@pytest.mark.django_db(transaction=False)
def test_latest_score_rounded(kpi):
    result = kpi.latest_score_rounded()
    assert result == 0
    kpi.latest_score = randint(1, 300) / 3
    result = kpi.latest_score_rounded()
    assert result == round(result, 2)


@pytest.mark.django_db(transaction=False)
def test_set_month_1_score(kpi, parent_kpi):
    kpi.latest_score = randint(30, 100)
    kpi.operator = "<="
    kpi.target = 0
    kpi.real = 0
    kpi.save()
    result = kpi.set_month_1_score()
    assert result == kpi.month_1_score
    result = parent_kpi.set_month_1_score()
    assert result == parent_kpi.month_1_score


@pytest.mark.django_db(transaction=False)
def test_are_other_children_delayed(userA, kpi, parent_kpi, organization):
    kpi.refer_to = parent_kpi
    kpi.save()

    result = are_other_children_delayed(kpi)
    assert result is True

    k1 = KPI()
    k1.parent = parent_kpi
    k1.refer_to = None
    k1.weight = 10
    k1.save()

    result = are_other_children_delayed(k1)
    assert result is False

    k1.refer_to = parent_kpi
    k1.weight = 10
    k1.save()

    result = are_other_children_delayed(kpi)
    assert result is False


@pytest.mark.django_db(transaction=False)
def test_update_group_name(userA, kpi, organization, userA_manager):
    kpi.refer_to_id = kpi.parent_id
    kpi.save()
    assert kpi.refer_group_name == kpi.name

    update1 = update_group_name(kpi, _root_id=kpi.parent_id)
    assert update1 == kpi.refer_group_name
    kpi.refer_to_id = kpi.parent_id
    kpi.save()

    kpi2 = KPI()
    kpi2.name = "test"
    kpi2.quarter_period = organization.get_current_quarter()
    kpi2.user = userA_manager
    kpi2.save()

    pkpi = kpi.parent
    pkpi.refer_to_id = kpi2.id
    pkpi.save()
    update2 = update_group_name(kpi)
    kpi2 = reload_model_obj(kpi2)
    assert update2 == pkpi.name
#
#
# @pytest.mark.django_db(transaction=False)
# def test_get_kpi_refer_group_old(userA, kpi, organization, userA_manager):
#     '''
#         have refer_to_id
#     '''
#     k = create_child_kpi(kpi, organization.get_current_quarter(), 'New KPI Child 1', userA_manager)
#     assert k.get_kpi_refer_group_old() == k.refer_to_id
#
#     '''
#         kpi not have refer_to_id
#     '''
#     assert kpi.get_kpi_refer_group_old() == kpi.id

'''
    model KpiComment
'''


@pytest.mark.django_db(transaction=False)
def test_unicode_kpi_comment(userA, kpi):
    content = random_string()
    cmt = KpiComment(user=userA, content=content, kpi=kpi)
    cmt.save()
    key = str(userA) + " " + content
    assert cmt.__unicode__() == key


@pytest.mark.django_db(transaction=False)
def test_get_file_name(userA, kpi):
    '''
        not file comment
    '''
    content = random_string()
    cmt = KpiComment(user=userA, content=content, kpi=kpi)
    cmt.save()
    assert cmt.get_file_name() is None

    '''
        have file comment
    '''
    file_name = random_string()
    file_cmt = SimpleUploadedFile(file_name + '.txt', 'test kpi comment')
    content = random_string()
    cmt = KpiComment(user=userA, content=content, kpi=kpi, file=file_cmt)
    cmt.save()
    assert cmt.get_file_name() == file_name + '.txt'


@pytest.mark.django_db(transaction=False)
def test_get_file_url(userA, kpi):
    '''
            not file comment
    '''

    content = random_string()
    cmt = KpiComment(user=userA, content=content, kpi=kpi)
    cmt.save()
    assert cmt.get_file_url() is None

    '''
        have file comment
    '''
    file_name = random_string()
    file_cmt = SimpleUploadedFile(file_name + '.txt', 'test kpi comment')
    content = random_string()
    cmt = KpiComment(user=userA, content=content, kpi=kpi, file=file_cmt)
    cmt.save()
    assert cmt.get_file_url() is not None


@pytest.mark.django_db(transaction=False)
def test_to_dict(userA, kpi):
    content = random_string()
    cmt = KpiComment(user=userA, content=content, kpi=kpi)
    cmt.save()
    js = cmt.to_dict()
    assert js is not None
    assert js['content'] == content
    assert js['file_name'] is None


@pytest.mark.django_db(transaction=False)
def test_fix_no_owner_email(kpi, userA):
    kpi.fix_no_owner_email()
    assert kpi.owner_email == userA.email

# @pytest.mark.django_db(transaction=False)
# def test_kpi_type_error(kpi, userA, userA_manager):
#     k1 = KPI()
#     k1.assigned_to = userA
#     k1.parent = kpi


@pytest.mark.django_db(transaction=False)
def test_get_hash(kpi, userA):
    '''
        not hash
    '''
    k1 = KPI()
    assert k1.hash is None
    assert k1.get_hash() is not None

    '''
        have hash
    '''
    hash_kpi = hashlib.sha1(str(kpi.id) + random_string(3)).hexdigest()
    kpi.hash = hash_kpi
    kpi.save()
    assert kpi.get_hash() == hash_kpi


@pytest.mark.django_db(transaction=False)
def test_set_owner_from_email(userA, userA_manager):
    kpi = KPI()
    kpi.owner_email = userA_manager.email
    kpi.user = userA
    kpi.save()
    kpi.set_owner_from_email()
    k = KPI.objects.get(id=kpi.id)
    assert k.user == userA_manager


@pytest.mark.django_db(transaction=False)
def test_assign_to_email(userA, userA_manager):
    '''
        email is incorrect
    '''
    kpi_parent = KPI()
    kpi_parent.save()
    kpi = KPI()
    kpi.parent = kpi_parent
    kpi.name = 'kpi test'
    kpi.user = userA_manager

    kpi.save()
    result = kpi.assign_to_email('test@gmail.com')
    assert result is False

    '''
        email is correct
    '''
    kpi.assign_to_email(userA.email)
    dup_kpi = KPI.objects.filter(name=kpi.name, user=userA).first()
    assert dup_kpi.refer_to == kpi.parent

    '''
        assign user = user
    '''
    result = kpi.assign_to_email(userA_manager.email)
    assert result is False


@pytest.mark.django_db(transaction=False)
def test_calculate_score_ladder(organization, userA, parent_kpi):
    '''
         userA not have kpi
    '''
    quarter = organization.get_current_quarter()
    reviewpro = ReviewProgress(user=userA, quarter=quarter)
    reviewpro.save()
    assert reviewpro.calculate_score_ladder() == 100

    '''
        userA have 2 kpi
    '''
    kpi = KPI()
    kpi.name = "Child KPI"
    kpi.quarter_period = organization.get_current_quarter()
    kpi.user = userA

    kpi.parent = parent_kpi

    kpi.target = 100
    kpi.operator = ">="

    kpi.save()

    kpi1 = KPI()
    kpi1.name = "Child test"
    kpi1.quarter_period = organization.get_current_quarter()
    kpi1.user = userA

    kpi1.parent = parent_kpi

    kpi1.target = 100
    kpi1.operator = ">="

    kpi1.save()

    quarter = organization.get_current_quarter()
    reviewpro = ReviewProgress(user=userA, quarter=quarter)
    reviewpro.save()
    assert reviewpro.calculate_score_ladder() == 50


@pytest.mark.django_db(transaction=False)
def test_reviewprogress_save(userA, organization):
    reviewpro1 = ReviewProgress(user=userA)
    reviewpro1.save()
    assert reviewpro1.quarter == userA.get_profile().get_organization().get_current_quarter()


@pytest.mark.django_db(transaction=False)
def test_get_parent(kpi):
    """
    Negative test for get_parent
    """
    parent = KPI()
    parent.save()
    result = parent.get_parent()
    assert result is None

    """
    Positive Test
    """
    result = kpi.get_parent()
    assert result == kpi.parent


@pytest.mark.django_db(transaction=False)
def test_get_refer_group_name(userA, organization):
    kpi = KPI()
    kpi.user = userA
    kpi.save()

    '''
    Test init data
    '''
    assert kpi.get_group() is not None  # Kpi has its own group
    assert kpi.group_name() == kpi.name
    assert kpi.group_name() is None  # to make sure logic not break, todo: change this test
    assert kpi.group_kpi.name == ""  # to make sure logic not break, todo: change the test
    assert kpi.group_name() != kpi.group_kpi.name  # to make sure logic not break, todo: change the test
    assert kpi.refer_group_name == kpi.name

    '''
    Change kpi.name => kpi.refer_group_name should be changed, group name not changed (follow the legacy logic)
    '''
    kpi.name = "BILLIONARE WHEN 30"
    kpi.save()

    assert kpi.refer_group_name == kpi.name
    assert kpi.group_name() == kpi.refer_group_name
    assert kpi.get_group().name == kpi.group_kpi.name
    assert kpi.get_group().name == ""


@pytest.mark.django_db(transaction=False)
def test_get_refer_to(kpi):
    """
    Negative Test for get_refer_to
    """
    parent = KPI()
    parent.save()
    result = parent.get_refer_to()
    assert result is None

    """
    Positive Test for get_refer_to
    """
    result = kpi.get_refer_to()
    assert result == kpi.refer_to


@pytest.mark.django_db(transaction=False)
def test_get_kpi_refer_group(kpi):
    """
    Test for get_kpi_refer_group
    """
    result = kpi.get_kpi_refer_group()
    assert result == slugify(kpi.refer_group_name)


@pytest.mark.django_db(transaction=False)
def test_relog_user_score_all(userA, organization):
    result = relog_user_score_all(organization=None)
    assert result == None


@pytest.mark.django_db(transaction=False)
def test_kpidata_save(userA, kpi, organization):
    kdata = KPIData()
    kdata.kpi = kpi
    kdata.date = datetime.date.today()
    kdata.value = randint(1, 10000)
    kdata.input_by = userA
    kdata.save()

    assert kdata.created_at.year == datetime.date.today().year


@pytest.mark.django_db(transaction=False)
def test_get_parent_kpis_by_category(userA, kpi, organization):

    @override_settings(
        CACHES={
            'default': {
                'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
                'LOCATION': 'unique-snowflake',
            }
        }
    )
    def test_runner():

        category = "financial"
        current_quarter = organization.get_current_quarter()

        k1 = KPI()
        k1.bsc_category = category
        k1.ordering = 1
        k1.user = userA
        k1.quarter_period = current_quarter
        k1.refer_group_name = "B"
        k1.save()

        k2 = KPI()
        k2.bsc_category = category
        k2.ordering = 2
        k2.user = userA
        k2.quarter_period = current_quarter
        k2.refer_group_name = "A"
        k2.save()

        kpis = get_parent_kpis_by_category(userA.id, current_quarter, category, only_id=False, use_cache=False)

        assert kpis[0].id == k2.id
        assert kpis[1].id == k1.id

        user_id = userA.id
        quarter_period_id = organization.get_current_quarter().id
        category = "financial"
        kpi.bsc_category = category
        kpi.save()
        only_id = True
        key = "get_parent_kpis_by_category{0}{1}{2}{3}".format(user_id, quarter_period_id, category, only_id)
        kpis = [kpi.id]
        cache.set(key, kpis, 60)
        result = get_parent_kpis_by_category(user_id, quarter_period_id, category)
        assert result == cache.get(key, None)

    test_runner()


@pytest.mark.django_db(transaction=False)
def test_get_kpi_id(userA, kpi, organization):
    kpi.ordering = None
    kpi.save()
    result = get_kpi_id(kpi)
    assert result == kpi.get_prefix_id() + ""

    kpi.ordering = randint(1, 10000)
    kpi.save()
    result = get_kpi_id(kpi)
    assert result == kpi.get_prefix_id() + str(int(kpi.ordering))


@pytest.mark.django_db(transaction=False)
def test_mark_quarter_ready(organization):

    def refresh_quarter_data():
        # refesh Quarter Period data
        q = QuarterPeriod.objects.get(id=current_quarter.id)
        return q

    current_quarter = organization.get_current_quarter()
    current_quarter.is_ready = False
    current_quarter.save(update_fields=['is_ready'])
    q = refresh_quarter_data()
    assert q.is_ready is False

    # mark quarter as ready
    q.mark_quarter_ready()
    q = refresh_quarter_data()
    assert q.is_ready is True

