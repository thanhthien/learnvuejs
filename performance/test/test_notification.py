# !/usr/bin/python
# -*- coding: utf-8 -*-
# def test_kpi_toggle(kpi):
#     pass
import json

import pytest
from django.core.urlresolvers import reverse
from notifications.models import Notification
from notifications.signals import notify


@pytest.mark.django_db(transaction=False)
def test_push_notification(userA, userB, userA_manager , client_authed):
    notify.send(userA, recipient=userA, verb='you reached level 10')
    ns = Notification.objects.unread()

    assert ns.count() == 1

    ns = userA.notifications.unread()
    assert ns.count() == 1

    link = reverse('notificationAPI')

    response = client_authed.post(link, json.dumps({ 'id': ns[0].id,  'command': 'mark_read'}), content_type="application/json" )
    assert response.status_code == 201 or response.status_code == 302 # in case of redirect to /performance/creating/
    assert ns.count() == 0
