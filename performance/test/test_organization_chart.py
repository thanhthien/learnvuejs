# !/usr/bin/python
# -*- coding: utf-8 -*-
import pytest
from performance.models import KPI, KPIBackup
from performance.services.kpi import calculate_score_cascading
from utils import reload_model_obj
from django.core.urlresolvers import reverse
from utils.common import to_json
from performance.services.user_organization import migrate_employee_to_exist_position


@pytest.mark.django_db(transaction=False)
def test_move_employee_to_exist_position(userA, userB, userAdmin, organization, client_admin):
    k = KPI()
    k.quarter_period = organization.get_current_quarter()
    k.quarter_one_target = 10
    k.quarter_two_target = 10
    k.quarter_three_target = 10
    k.quarter_four_target = 10
    k.user = userA
    k.month_1_target = 10
    k.month_2_target = 10
    k.month_3_target = 10
    k.save()

    url = reverse('kpi_update_score')
    data = to_json(k)
    data['month_1'] = 10
    data['month_2'] = 10
    data['month_3'] = 10
    res = client_admin.post(url, data)
    assert res.status_code == 200
    k = reload_model_obj(k)
    data = to_json(k)

    assert k.month_1_score == 100
    assert k.month_2_score == 100
    assert k.month_3_score == 100

    userA_name = userA.profile.display_name
    profile = userA.profile
    profile.phone = "00000000000000"
    profile.employee_code = "99999"
    userA_email = userA.email
    userA_password = userA.password
    userA_phone = profile.phone
    userA_code = profile.employee_code

    migrate_employee_to_exist_position(profile, userB.profile, True, userAdmin)

    userB = reload_model_obj(userB)
    assert userB.profile.display_name == userA_name
    assert userB.email == userA_email
    assert userB.password == userA_password
    assert userB.profile.phone == userA_phone
    assert userB.profile.employee_code == userA_code

    assert KPIBackup.objects.filter(user=userB).exists()
    bk = KPIBackup.objects.filter(user=userB).first()
    for k in bk.data:
        assert k['name'] == data['name']


import json
from django.test.client import Client
@pytest.mark.django_db(transaction=False)
def test_get_backup_users(userA, userB, userAdmin, organization, client_authed):
    url = reverse('move-to-exist-position')
    response = client_authed.get(url)
    assert response.status_code == 200
    assert json.loads(response.content) is not None

    response = client_authed.get(url, {'user_id':userA.id,'req':True})
    assert response.status_code == 200

    response = client_authed.get(url, {'user_id': userA.id})
    assert response.status_code == 200


