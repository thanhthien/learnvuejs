# !/usr/bin/python
# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
import pytest


@pytest.mark.django_db(transaction=False)
def test_kpi_editor_emp_200(organization, client_authed, userB, organization2, userC):
    url = reverse("kpi_editor_emp", kwargs={'user_id': userB.id})
    res = client_authed.get(url)
    assert res.status_code == 200


@pytest.mark.django_db(transaction=False)
def test_kpi_editor_emp_404(organization, client_authed, userB, organization2, userC):
    url = reverse("kpi_editor_emp", kwargs={'user_id': userC.id})
    res = client_authed.get(url)
    assert res.status_code == 404


@pytest.mark.django_db(transaction=False)
def test_people_200(organization, client_admin, userB, organization2, userC):
    url = reverse("people") + "?user_id={}".format(userB.id)
    res = client_admin.get(url)
    assert res.status_code == 200


@pytest.mark.django_db(transaction=False)
def test_people_404(organization, client_admin, userB, organization2, userC):
    url = reverse("people") + "?user_id={}".format(userC.id)
    res = client_admin.get(url)
    assert res.status_code == 404
