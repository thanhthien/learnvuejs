# import pytest
# from performance.report import KPIReport
# from user_profile.models import Profile
#
#
# @pytest.mark.django_db(transaction=False)
# def test_get_superuser_email(organization, userA, userAdmin, ceo):
#     kpi_report = KPIReport()
#     report = kpi_report.get_superuser_email(userA)
#     assert len(report) == 2
#
#
#     report1 = kpi_report.get_superuser_email(userAdmin)
#     assert len(report1) >= 1
import pytest
from django.test import Client
from performance.controllers.report import *


@pytest.mark.django_db(transaction=False)
def test_report_score(client_authed, client_userA_manager, userA, userA_manager, organization, client_ceo, client_admin):
    q = organization.get_current_quarter()
    olink = reverse('report-score')

    '''
        Report of mamager
    '''
    link = olink + '?quarter_id=' + str(q.id) +'&report_type=score&user_id='+str(userA_manager.id)
    response = client_userA_manager.get(link)
    assert response.status_code == 200

    '''
        Employee does not have permission to view manager report
    '''
    response = client_authed.get(link)
    assert response.status_code == 403

    '''
        Report of employee
    '''
    link = olink + '?quarter_id=' + str(q.id) +'&report_type=score&user_id='+str(userA.id)
    response = client_authed.get(link)
    response.status_code == 200

    '''
        Manager can view employee report
    '''
    response = client_userA_manager.get(link)
    assert response.status_code == 200

    '''
        Admin view report of company
    '''
    link = olink + '?quarter_id=' + str(q.id) + '&report_type=score'
    response = client_admin.get(link)
    assert response.status_code == 200

    '''
        Employee view report of company
    '''
    response = client_authed.get(link)
    assert response.status_code == 403

    '''
        CEO view report of company
    '''
    response = client_ceo.get(link)
    assert response.status_code == 200


    '''
        CASE: request with invalid params 
    '''
    link = olink +"?quarter_id="+str(q.id)+"&report_type=score&user_id="+"something"
    response = client_admin.get(link)
    # assert response.status_code == 404
    assert response.status_code == 200

    '''
        CASE: request with invalid params: user_id empty
    '''
    link = olink +"?quarter_id="+str(q.id)+"&report_type=score&user_id="+""
    response = client_admin.get(link)
    # assert response.status_code == 404
    assert response.status_code == 200