# !/usr/bin/python
# -*- coding: utf-8 -*-
# def test_kpi_toggle(kpi):
#     pass
import random
from datetime import datetime
import time


from django.contrib.auth.models import User
import pytest

from django.core.cache import cache
from performance.models import KPI, ScoreLog
from utils.common import get_redis


@pytest.mark.django_db(transaction=False)
def test_log_user_score(organization, userA):

    return False
    '''
    Hong removed this test on July 2017
    :param organization:
    :param userA:
    :return:
    '''

    userX = User()
    userX.save()

#     userOrg = UserOrganization(user=userX, organization=organization)
#     userOrg.save()

    k = KPI()
    k.latest_score = random.randint(1, 100)
    k.user = userX
    k.quarter_period = organization.get_current_quarter()
    k.weight = 10

    k.save()

    '''
    Trigger score log call
    '''
    k.name = 'new name'
    k.save()

    k1 = KPI()
    k1.latest_score = random.randint(1, 100)
    k1.user = userX
    k1.parent = k
    k1.quarter_period = organization.get_current_quarter()
    k1.save()

    # sl = ScoreLog.objects.filter(user=userX).last()
    # assert sl is None

    k1.name = "New random name"
    k1.save()

    '''
    Sleep wait for thread to be done
    '''
    time.sleep(10)

   # sl = ScoreLog.objects.filter(user=userX).last()
    #
    # if not sl: #wait for another 60 seconds
    #     time.sleep(60)
    #     sl = ScoreLog.objects.filter(user=userX).last()

    # assert cache.get('score_log_is_called_{}'.format(userX.id), False   ) == True
    assert bool(get_redis('score_log_is_called_{}'.format(userX.id))) == True
