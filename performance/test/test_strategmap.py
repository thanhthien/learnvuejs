# !/usr/bin/python
# -*- coding: utf-8 -*-
# def test_kpi_toggle(kpi):
#     pass
import random
# from faker import Factory
from performance.db_models.strategy_map import StrategyMap
from performance.services import strategymap, update_kpi, remove_kpi
import pytest

# faker = Factory.create()

# import os
#
# def include(filename):
#     if os.path.exists(filename):
#         execfile(filename)
#
#
# include('../../conftest_init.py')
from performance.services.strategymap import StrategyMapServices
from utils import random_string

"""
Commented the code below as test in 
performance/db_models/test/test_strategy_map.py::test_strategy_map()
"""
#
# @pytest.mark.django_db(transaction=False)
# def test_get_name_description(organization):
#     sm = StrategyMap()
#     sm.organization = organization
#     sm.description = random_string()
#     sm.save()
#     sm.get_name()
#     result = sm.get_description()
#     assert result == sm.description


@pytest.mark.django_db(transaction=False)
def test_group_and_kpi_strategymap(organization, userAdmin):
    current_quarter = organization.get_current_quarter()
    category = 'financial'
    s_map, is_created = StrategyMap.objects.get_or_create(organization=organization,
                                                          quarter_period=current_quarter,
                                                          director=None)
    userAdmin.profile.get_create_user_organization(organization)
    name = ''
    group = strategymap.StrategyMapServices.create_group_kpi(organization.id, s_map.id, category, name,
                                                                                 userAdmin)
    assert group.id != None

    # GROUP - update group
    # # name is null
    name = ''
    afterGroup = strategymap.StrategyMapServices.update_group(organization.id, group.id, group.map.id, group.category, name,
                                                                             userAdmin)
    assert afterGroup.name == group.name

    # #name is random
    name = random_string()
    afterGroup = strategymap.StrategyMapServices.update_group(organization.id, group.id, group.map.id, group.category, name,
                                                              userAdmin)
    assert afterGroup.name == name

    # delete group
    ceo = organization.ceo
    has_kpi = strategymap.StrategyMapServices.delete_group_kpi(organization.id, afterGroup.id, ceo)
    assert has_kpi == False

    #=================== GROUP KPI
    # # create KPI
    group = strategymap.StrategyMapServices.create_group_kpi(organization.id, s_map.id, category, name,
                                                             userAdmin)
    newKPI = StrategyMapServices.create_kpi(organization.id, s_map.id, category, ceo)

    assert newKPI != None

    # # update kpi in group
    name = random_string()
    weight = random.randint(0, 1000)
    description = random_string()
    ordering = random.randint(1, 10)
    newKPI = update_kpi(ceo, newKPI.id, name=name, weight=weight, description=description,
               ordering=ordering)
    assert newKPI.name == name.upper() and newKPI.weight == weight and newKPI.description == description \
           and newKPI.ordering == ordering

    # # update kpi group
    name = random_string()
    newGroup = strategymap.StrategyMapServices.create_group_kpi(organization.id, s_map.id, category, name,
                                                             userAdmin)

    old_group = group.id
    new_group_id = newGroup.id
    kpi = strategymap.StrategyMapServices.update_group_kpi(organization.id, newKPI.id, new_group_id=new_group_id, old_group_id=old_group,
                                                           actor=ceo, new_bsc_category=category)

    assert kpi.get_group().id == newGroup.id

    # # delete kpi
    newKPI = StrategyMapServices.create_kpi(organization.id, s_map.id, category, ceo)
    is_removed = remove_kpi(newKPI.id, ceo)
    assert is_removed == True

