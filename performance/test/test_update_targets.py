#!/usr/bin/python
# -*- coding: utf-8 -*-
import pytest
from performance.services.kpi import update_targets_from_quarter, \
    update_kpi_target_from_month_targets
from performance.models import KPI


@pytest.mark.django_db(transaction=False)
def test_update_targets_from_quarter(userA, organization):
    kpi = KPI()
    kpi.user = userA
    kpi.quarter_period = organization.get_current_quarter()
    kpi.quarter_one_target = 10
    kpi.target = 10
    kpi.score_calculation_type = 'sum'
    kpi.quarter_one_target = 1
    kpi.quarter_two_target = 2
    kpi.quarter_three_target = 3
    kpi.quarter_four_target = 4
    kpi.save()

    quarter_period = kpi.quarter_period
    quarter_period.quarter = 1
    quarter_period.save()
    kpi = KPI.objects.get(id=kpi.id)
    update_targets_from_quarter(kpi)
    assert kpi.target == kpi.quarter_one_target
    assert kpi.month_1_target == round(float(kpi.target) / 3, 2)
    assert kpi.month_2_target == round(float(kpi.target) / 3, 2)
    assert kpi.month_3_target == round(float(kpi.target) / 3, 2)

    quarter_period = kpi.quarter_period
    quarter_period.quarter = 2
    quarter_period.save()
    kpi = KPI.objects.get(id=kpi.id)
    update_targets_from_quarter(kpi)
    assert kpi.target == kpi.quarter_two_target

    quarter_period = kpi.quarter_period
    quarter_period.quarter = 3
    quarter_period.save()
    kpi = KPI.objects.get(id=kpi.id)
    update_targets_from_quarter(kpi)
    assert kpi.target == kpi.quarter_three_target

    quarter_period = kpi.quarter_period
    quarter_period.quarter = 4
    quarter_period.save()
    kpi = KPI.objects.get(id=kpi.id)
    update_targets_from_quarter(kpi)
    assert kpi.target == kpi.quarter_four_target

    kpi.score_calculation_type = 'average'
    kpi.save()
    update_targets_from_quarter(kpi)
    assert kpi.month_1_target == kpi.target
    assert kpi.month_2_target == kpi.target
    assert kpi.month_3_target == kpi.target

    kpi.score_calculation_type = 'most_recent'
    kpi.target = 10
    kpi.save()
    update_targets_from_quarter(kpi)
    assert kpi.month_3_target == kpi.target


@pytest.mark.django_db(transaction=False)
def test_update_quarter_target_from_month_targets(userA, userB, organization):
    parent = KPI()
    parent.user = userA
    parent.quarter_period = organization.get_current_quarter()
    parent.target = 15
    parent.score_calculation_type = 'sum'
    parent.save()

    kpi = KPI()
    kpi.user = userA
    kpi.quarter_period = organization.get_current_quarter()
    kpi.parent = parent
    kpi.quarter_one_target = 10
    kpi.target = 10
    kpi.month_1_target = 1
    kpi.month_2_target = 2
    kpi.month_3_target = 3
    kpi.score_calculation_type = 'sum'
    kpi.save()

    quarter_period = kpi.quarter_period
    quarter_period.quarter = 1
    update_kpi_target_from_month_targets(kpi)
    assert kpi.target == kpi.month_1_target + kpi.month_2_target + kpi.month_3_target

    kpi.score_calculation_type = 'average'
    kpi.month_1_target = 1
    kpi.month_2_target = None
    kpi.month_3_target = None
    kpi.save()

    update_kpi_target_from_month_targets(kpi)
    assert kpi.target == kpi.month_1_target

    kpi.month_1_target = 1
    kpi.month_2_target = 2
    kpi.month_3_target = None
    kpi.save()
    update_kpi_target_from_month_targets(kpi)
    assert kpi.target == kpi.month_2_target

    kpi.month_1_target = 10
    kpi.month_2_target = 20
    kpi.month_3_target = 30
    kpi.save()
    update_kpi_target_from_month_targets(kpi)
    assert kpi.target == kpi.month_3_target

    kpi_assign = kpi.assign_to(userB)
    assert kpi_assign.id == KPI.objects.filter(user_id=kpi.assigned_to_id, cascaded_from=kpi).first().id

    quarter_period.quarter = 1
    quarter_period.save()
    update_kpi_target_from_month_targets(kpi)
    assert kpi.quarter_one_target == kpi.target
    assert kpi_assign.quarter_one_target == kpi.target

    quarter_period.quarter = 2
    quarter_period.save()
    kpi.month_1_target = 5
    kpi.month_2_target = 6
    kpi.month_3_target = 7
    kpi.save()

    update_kpi_target_from_month_targets(kpi)
    assert kpi.quarter_period.quarter == 2
    assert kpi_assign.quarter_period.quarter == 2
    assert kpi.quarter_two_target == kpi.target
    kpi_assign = KPI.objects.filter(user_id=kpi.assigned_to_id, cascaded_from=kpi).first()
    assert kpi_assign.quarter_two_target == kpi.target

    quarter_period.quarter = 3
    quarter_period.save()

    kpi.month_1_target = 8
    kpi.month_2_target = 9
    kpi.month_3_target = 10
    kpi.save()

    update_kpi_target_from_month_targets(kpi)
    assert kpi.quarter_three_target == kpi.target
    kpi_assign = KPI.objects.filter(user_id=kpi.assigned_to_id, cascaded_from=kpi).first()
    assert kpi_assign.quarter_three_target == kpi.target

    quarter_period.quarter = 4
    quarter_period.save()
    kpi.month_1_target = 11
    kpi.month_2_target = 12
    kpi.month_3_target = 13
    kpi.save()
    update_kpi_target_from_month_targets(kpi)
    assert kpi.quarter_four_target == kpi.target
    kpi_assign = KPI.objects.filter(user_id=kpi.assigned_to_id, cascaded_from=kpi).first()
    assert kpi_assign.quarter_four_target == kpi.target

