# !/usr/bin/python
# -*- coding: utf-8 -*-
import time

import pytest
from django.core import mail

from django.core.urlresolvers import reverse
# from performance.conftest import ts_javascript_error
from performance.urls import urlpatterns
from django.test import Client
#
# def ts_javascript_error(selenium_userA_client, live_server, link):
#     selenium_userA_client.get(live_server + link)
#     try:
#         body = selenium_userA_client.find_elements_by_tag_name('body')[0]
#     except:
#         body = None
#
#     if body:
#         JSError = body.get_attribute("JSError")
#        # print link
#         #print body
#         # if JSError != "":
#         #
#         #     print "ERRORR"
#         #     print link
#
#         assert JSError == ""


@pytest.mark.django_db(transaction=False)
def test_admin_urls(live_server, userAdmin, organization):

    link = reverse( 'schedule')
    #TODO: re-add this ts_javascript_error(selenium_userAdmin_client,live_server, link )
    time.sleep(.5)

    link = reverse( 'basic_view')
   #TODO: add this  ts_javascript_error(selenium_userAdmin_client,live_server, link )
    #time.sleep(.5)


#
@pytest.mark.django_db(transaction=False)
def test_all_simple_urlpatterns(client_authed, userA, organization):

    for url in urlpatterns:

        link = None
        try:
            link = reverse(url.name)
        except:
            pass

        if link:
            # Lam commented this, because it's useless
            # if url.name == "report_goal" \
            #         or url.name == "report_dev_plan" \
            #         or url.name == "kpi-new-pricing" \
            #         or url.name == "perform_reward"\
            #         or url.name == "logs" \
            #         or url.name == "activity_report": #THE FUCK?
            #     continue #TODO: remove this

            response = client_authed.get(link)
            assert response.status_code == 200 \
                   or response.status_code == 302 \
                   or response.status_code == 403 \
                   or response.status_code == 405 or response.status_code == 404

            '''
                If user access to these page, server will return 302 code
            '''
            if url.name == "report-home" \
                    or url.name == "report-score" \
                    or url.name == "report-strategymap" \
                    or url.name == "report-personal":
                unauth_client = Client()
                res = unauth_client.get(link)
                assert res.status_code == 302

            str_response = str(response.status_code)
            assert str_response[0] != "5"

                #TODO: check this



            if url.name == "people_node" \
                    or url.name == "delete_people" \
                    or url.name == "goal-new" \
                    or url.name == "create-sm-branch" \
                    or url.name == "history_deleted_kpi" \
                    or url.name == "import_kpi":
                continue

            content_type = response.get('Content-Type', None)

            if response.status_code == 200 \
                    and  content_type \
                    and content_type.find('text/html') >= 0:

                   # from conftest import ts_javascript_error
                # temporary disable
                #ts_javascript_error(selenium_userA_client,live_server, link )
               # print link
                pass
                #time.sleep(.5)


    #assert 2==3

