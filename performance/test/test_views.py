import json

import pytest
from random import randint, random
import datetime
from time import sleep

from django.core.exceptions import PermissionDenied
from rest_framework.reverse import reverse

from performance.base.views import PerformanceBaseView
from performance.models import KPI
from user_profile.models import Profile
from user_profile.services.profile import remove_user_from_organization
from utils import random_string


@pytest.mark.django_db(transaction=False)
def test_all_kpi(userA, kpi, client_authed, organization):
    link = reverse('all_kpi') + "?team_id=" + format(userA.id)
    response = client_authed.get(link)
    assert response.status_code == 302
    assert '/performance/team-performance/?' in response['Location']


# @pytest.mark.django_db(transaction=False) --> not used
# def test_overall_performance_overview(userA, client_authed, organization):
#     link = reverse('overall_performance_overview')
#     response = client_authed.post(link)
#     assert response.status_code == 200


@pytest.mark.django_db(transaction=False)
def test_update_weighting(userA, kpi, userA_manager, client_authed, organization):


    kpi1 = KPI()
    kpi1.user = userA_manager
    kpi1.weight = 80
    kpi1.month_1_score = 75
    kpi1.month_2_score = 85
    kpi1.month_3_score = 65
    kpi1.latest_score = 90
    kpi1.quarter_period = organization.get_current_quarter()
    kpi1.save()

    kpi.weight = 87
    kpi.latest_score = 79
    kpi.user = userA
    kpi.parent = kpi1
    kpi.month_1_score = 65
    kpi.month_2_score = 95
    kpi.month_3_score = 85
    kpi.quarter_period = organization.get_current_quarter()
    kpi.save()
    data = {'value': kpi.weight, 'pk': kpi.id}
    link = reverse('update_weighting')
    response = client_authed.post(link, data)
    js = json.loads(response.content)
    assert response.status_code == 200
    assert js['id'] == kpi.id
    # assert js['final_score'] == kpi.get_final_score()
    # /ssert js['final_score'] == kpi.get_score()
    # assert js['final_score'] == 0


@pytest.mark.django_db(transaction=False)
def test_subordinate_list(userA, userA_manager, client_userA_manager, client_authed, organization):
    link = reverse('subordinate-users')
    response = client_userA_manager.post(link)
    assert response.status_code == 200
    js = json.loads(response.content)
    assert len(js) == len(Profile.objects.filter(organization=organization, active=True))


# tests for /performance/base/views.py
@pytest.mark.django_db(transaction=False)
def test_get_organization(userA, organization):
    org = PerformanceBaseView()
    """
    Case 1: Success
    """
    result = org.get_organization(userA)
    assert result == userA.profile.organization
    """
    Case 2: Permission denied
    """
    pA = userA.profile
    pA.organization = None
    pA.save()

    flag = 0
    try:
        result = org.get_organization(userA)
    except PermissionDenied:
        flag = 1

    assert flag == 1


@pytest.mark.django_db(transaction=False)
def test_get_current_quarter(userA, organization):
    org = PerformanceBaseView()
    result = org.get_current_quarter(userA)
    assert userA.profile.get_current_quarter() == result


# Wrongly named method in performance/base/views.py::get_user_organization
# as method returns profile object even if organization is none. Need to check
@pytest.mark.django_db(transaction=False)
def test_get_user_organization(userA):
    org = PerformanceBaseView()
    """
    Case 1: Success
    """
    result = org.get_user_organization(userA)
    assert result == userA.profile

    """
    Case 2: Permission denied
    """
    pA = userA.profile
    pA.organization = None
    pA.delete()
    flag = 0

    try:
        result = org.get_user_organization(userA)
    except PermissionDenied:
        flag = 1

    assert flag == 1
