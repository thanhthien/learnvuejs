# !/usr/bin/python
# -*- coding: utf-8 -*-
from model_utils.tracker import FieldInstanceTracker, FieldTracker
from django.core.exceptions import FieldError


class CJSFieldInstanceTracker(FieldInstanceTracker):

    def has_changed(self, field):
        if field in self.fields:
            field_type = self.instance._meta.get_field(field).get_internal_type()
            pre_val = self.previous(field)
            new_val = self.get_field_value(field)

            if field_type in ['FloatField', 'IntegerField', 'DecimalField', 'BigIntegerField']:
                if isinstance(new_val, basestring):
                    try:
                        new_val = float(new_val)
                    except:
                        pass

            return pre_val != new_val
        else:
            raise FieldError('field "%s" not tracked' % field)


class CJSFieldTracker(FieldTracker):
    tracker_class = CJSFieldInstanceTracker
