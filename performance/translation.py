__author__ = 'hongleviet'
from modeltranslation.translator import translator, TranslationOptions


class JobTranslationOptions(TranslationOptions):
    fields = ('name', 'description', 'factors')


class CompetencyLibTranslationOptions(TranslationOptions):
    fields = ('name', 'description',)


class CompetencyTranslationOptions(TranslationOptions):
    fields = ('name', 'description',)


class KPITranslationOptions(TranslationOptions):
    fields = ('name', 'description',)


class HistoricalCompetencyTranslationOptions(TranslationOptions):
    fields = ('name', 'description',)
#
#
# class HistoricalKPITranslationOptions(TranslationOptions):
#     fields = ('name', 'description',)


class DescriptionCellTranslationOptions(TranslationOptions):
    fields = ('title', 'description',)


class BscDepartmentTranslationOptions(TranslationOptions):
    fields = ('name',)


class BscJobTitleTranslationOptions(TranslationOptions):
    fields = ('name',)


class BscKpiTranslationOptions(TranslationOptions):
    fields = ('name', 'measurement')


