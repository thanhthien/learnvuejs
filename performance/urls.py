from django.conf.urls import url, include

from performance import views, api
from performance.controllers import report
from performance.controllers.competency import *
from performance.controllers.kpi import *
from performance.controllers.kpi_editor import *
from performance.controllers.misc import *
from performance.controllers.notifications import Notifications
from performance.controllers.onboading import *
from performance.controllers.people import *
from performance.controllers.performance_review import *
from performance.controllers.report import *
from performance.controllers.settings import *
from performance.controllers.strategy_map_bsc import *
from performance.controllers.import_data import *
from performance.controllers.kpilib import KPILibManagement
from performance.controllers.kpi_position import *

# from performance.customers import vinhphat as vp

# from performance.views import import_bsc
from performance.decorators import validate_license
from performance.views import ListKPI_View

urlpatterns = [
    url(r'^$', views.home_view, name='home_view'),
    url(r'^home/$', views.home_view, name='home_view'),
    #   url(r'^pricing/$', views.Pricing.as_view(), name='kpi-new-pricing'),
    #  url(r'^swot/$', SWOT.as_view(), name='swot'),
    url(r'^team/$', TEAM.as_view(), name='team_view'),
    url(r'^ksf/$', BSC_KSF.as_view(), name='ksf'),
    url(r'^bhag/$', BHAG.as_view(), name='bhag'),
    #  url(r'^assess/$', views.ASSESS.as_view(), name='assess'),
    url(r'^mobile/review/chat/$', views.MOBILE_REVIEW.as_view(), name='mobile'),

    url(r'^onboarding/$', ONBOARDING.as_view(), name='onboarding'),
    url(r'^onboarding/step-1/$', ONBOARDING1.as_view(), name='onboarding-step-1'),
    url(r'^onboarding/step-2/$', ONBOARDING2.as_view(), name='onboarding-step-2'),
    url(r'^onboarding/step-3/$', ONBOARDING3.as_view(), name='onboarding-step-3'),
    url(r'^onboarding/step-3-2/$', ONBOARDING32.as_view(), name='onboarding-step-3-2'),
    url(r'^onboarding/step-4/$', ONBOARDING4.as_view(), name='onboarding-step-4'),
    url(r'^onboarding/step-5/$', ONBOARDING5.as_view(), name='onboarding-step-5'),
    url(r'^onboarding/step-6/$', ONBOARDING6.as_view(), name='onboarding-step-6'),
    url(r'^onboarding/step-7/$', ONBOARDING7.as_view(), name='onboarding-step-7'),

    #   url(r'^timeline/$', TimelineView.as_view(), name='bsc-timeline'),

    url(r'^strategy-map/public/(?P<org_hash>.+)/year/(?P<year>\d{4})/$', PublicStrategyMap.as_view(),
        name='strategy_map_public_by_year'),
    url(r'^strategy-map/public/(?P<org_hash>.+)/(?P<smap_hash>\w+-\w+)/$', PublicStrategyMap.as_view(),
        name='strategy_map_public_by_hash'),

    # url(r'^kpi-worksheet/(?P<hash>[a-z0-9]+)/(?P<year>\d{4})/$', KPIWorkSheetMap.as_view(), name='kpi_worksheet'),
    # url(r'^kpi-worksheet/(?P<hash>[a-z0-9]+)/(?P<year>\d{4})/(?P<user_id>\d+)/$', KPIWorkSheetEmployee.as_view(),
    #     name='kpi_worksheet_employee'),

    url(r'^kpi-editor/(?P<year>\d{4})/(?P<user_id>.+)/$', KPIWorkSheetEmployeeNew.as_view(), name='kpi_dashboard_emp'),

    url(r'^kpi-editor/year/$', kpi_editor_year, name='kpi_editor_year'),

    url(r'^kpi-editor/emp/(?P<user_id>.+)/$', KPIWorkSheetEmployeeNew.as_view(), name='kpi_editor_emp'),
    url(r'^kpi-mindmap/p/(?P<user_id>.+)/$', KPIMindMapPub.as_view(), name='kpi_mindmappub'),
    url(r'^kpi-mindmap/(?P<user_id>.+)/$', KPIMindMap.as_view(), name='kpi_mindmap'),
    url(r'^kpi-editor/kpi/(?P<kpi_id>\d+)/$', KPIWorkSheetDetail.as_view(), name='KPIWorkSheetDetail'),
    url(r'^kpi-editor/$', KPIDaskboard.as_view(), name='kpi_dashboard'),
    url(r'^composer/$', KPIComposer.as_view(), name='kpi_composer'),

    #Ankita commented after discussing wih Mr.Khang:
    #url(r'^get-public-kpi-editor/(?P<user_id>.+)/$', public_kpi_editor, name='public_kpi_editor'),

    url(r'^search/$', KPIDaskboard.as_view(), name='kpi_editor_search'),
    #
    # url(r'^kpi-quality-control/(?P<hash>.+)/$', KPIQualityControl.as_view(),
    #     name='organzation-kpi-quality-control-public'),
    # url(r'^import-bsc/$', csrf_exempt(import_bsc)),
    url(r'^bsc.json$', bsc_json),  # TO BE REMOVED
    url(r'^basic/$', BasicSettings.as_view(), name='basic_view'),
    url(r'^auto-mail/$', AutoMail.as_view(), name='auto_mail_view'),
    #   url(r'^broadcast/$', broadcast, name='broadcast'),
    # url(r'^core-value/$', core_value_view, name='core_value'),

    url(r'^people/$', people_view, name='people'),
    url(r'^position-chart/$', PositionChartView.as_view(), name='position-chart'),
    url(r'^position-chart/node/$', NodePositionView.as_view(), name='position-chart-node'),
    url(r'^creating/$', creating_assessment, name='creating_assessment'),
    url(r'^people/node/$', PeopleNode.as_view(), name='people_node'),
    url(r'^position/node/$', PositionNode.as_view(), name='position_node'),
    url(r'^people/reset-password/$', UserResetPassword.as_view(), name='p-reset_password'),
    url(r'^people/reset-password-group/$', GroupUserResetPassword.as_view(), name='group-reset_password'),
    url(r'^peope/move-to-exist-position/$', MoveToExistPosition.as_view(), name='move-to-exist-position'),
    url(r'^people/new/$', AddEmployee.as_view(), name='add_people'),
    url(r'^people/delete/$', delete_people, name='delete_people_old'),
    url(r'^people/json/$', people_json, name='people_json'),
    # url(r'^schedule/old/$', schedule_view, name='schedule_old'),
    url(r'^schedule/$', schedule_view_v2, name='schedule'),
    url(r'^schedule/delete/$', schedule_delete, name='schedule_delete'),
    #  url(r'^helicopter/$', helicopter, name='helicopter'),

    url(r'^kpi_deleted/$', KPIDeleted.as_view(), name='kpi_deleted'),

    url(r'^kpicomment/(?P<unique_key>.+)/$', kpicomment, name='kpicomment'),
    url(r'^file-upload/(?P<unique_key>.+)/$', views.UploadFileComment.as_view(), name='upload-file-kpicomment'),

    #    url(r'^kpi_mail_action/(?P<kpi_id>\d+)/(?P<unique_key>.+)/(?P<value>.+)/(?P<reviewer_id>\d+)/$', kpi_mail_action,
    #       name='kpi_mail_action'),

    # url(r'^kpi_approval/reminder/$', kpi_approval_reminder_redirect, name='kpi_approval_reminder'),
    # url(r'^team-performance/$', teamperformance_view, name='team_performance'),
    # it's no use
    # url(r'^m/$', KPIMobileReview.as_view(), name='self_review_mobile'),
    # url(r'^team-performance/update/$', teamperformance_update, name='teamperformance_update'),
    # url(r'^self-performance/update/$', selfperformance_update, name='selfperformance_update'),
    # url(r'^team-performance/export/$', PerformanceExporter.as_view(), name='teamperformance_export'),
    url(r'^kpi/update-score/$', UpdateScoreKPI.as_view(), name='kpi_update_score'),
    url(r'^kpi/delete/$', KpiDelete.as_view(), name='delete_kpi'),
  #  url(r'^kpi/import/$', ImportKPI.as_view(), name='import_kpi'),
    url(r'^import/users/$', import_users, name='import_users'),
    url(r'^import/general-kpis/$', import_general_kpis, name='import_general_kpis'),
    url(r'^import/kpis/$', import_kpis, name='import_kpis'),
    url(r'^import/kpis/copy/$$', import_kpis_copy, name='import_kpis_copy'),
    url(r'^email_redirect/$', email_redirect, name='email_redirect'),
    # url(r'^kpi/import/(?P<user_id>\d+)/$', ImportKPIByUser.as_view(), name='import_kpi_user'),
    # url(r'^competency/delete/$', delete_competency, name='delete_competency'),
    # url(r'^update-plan/$', views.performance_update_plan, name='performance_update_plan'),
    # url(r'^own-performance/$', views.ownperformance_view, name='ownperformance_view'),
    # url(r'^perform-reward/$', views.PerformReward.as_view(), name='perform_reward'),

    # url(r'^report-overall-performance/$', views.overall_performance_overview, name='overall_performance_overview'),
    url(r'^report-individual-performance/$', views.individual_performance_overview,
        name='individual_performance_overview'),

    #   url(r'^report-self-assessment-review-status/$', report_self_assessment_review_status,
    #       name='self_assessment_review_status'),
    # url(r'^xls/$', xls_export, name='xls'),
    # url(r'^report_exporter/$', report_exporter, name='report_exporter'),

    # url(r'^invitation-data/$', views.invitation_data, name="invitation-data"),

    # url(r'^search_user/$', views.search_user_kpi),

    # url(r'^competency_explain/(?P<id>\d+)/$', competency_explain_request),
    # url(r'^competency_lib_copy/$', competency_lib_copy),

    url(r'^update-weighting/$', views.WeightingUpdateView.as_view(), name="update_weighting"),
    url(r'^update-ordering/$', views.OrderingUpdateView.as_view(), name="update_ordering"),

    #  url(r'^review/reminder/$', views.ReviewReminder.as_view(), name="review_reminder"),
    # url(r'^kpis-competencies/duplicate/$', DuplicateKPICompetency.as_view(), name="duplicate_kpi_competency"),
    url(r'^kpi_log/(?P<user_id>\d+)/$', kpi_logs, name="kpi_logs"),  # TODO: remove this
    # url(r'^kpi_log/(?P<loguser_id>\d+)/$', kpi_logs, name="kpi_logs"),  # TODO: remove this
    url(r'^log/$', logs, name="logs"),
    url(r'^system-log/$', system_logs, name="system_logs"),

    #  url(r'^update-role/$', views.UpdateRole.as_view(), name="update-role"),

    url(r'^user/all-kpis/$', views.all_kpi, name='all_kpi'),
    url(r'^subordinate/users/$', views.SubordinateList.as_view(), name="subordinate-users"),  # unuse
    url(r'^assign/kpi/$', AssignKPI.as_view(), name="assign-kpi"),
    # url(r'^competencies/autocomplete/$', CompetencyComplete.as_view(), name="competency-autocomplete"),
    url(r'^member/new/$', AddMember.as_view(), name='add_member'),
    url(r'^kpi/align-up/$', AlignUpKPI.as_view(), name='align-up-kpi'),
    url(r'^cascade-kpi/(?P<kpi_id>\d+)/$', CascadeKPIName.as_view(), name='cascade-kpi-name'),
    url(r'^kpi/update-category/$', CategoryUpdateView.as_view(), name="update_category"),
    # url(r'^kpi/confirm/$', views.ConfirmKPI.as_view(), name="confirm_kpi"),
    # url(r'^kpi/remove-assigned-to/$', RemoveAssignedTo.as_view(), name="remove-assigned-to"),
    url(r'^kpi/comments/load/$', LoadComments.as_view(), name="load-comments"),
    url(r'^kpi/ecal/(?P<kpi_unique_key>.+)/$', Ecal.as_view(), name="ecal"),

    # url(r'^kpi-tree/(?P<year>\d{4})/$', views.KPITreeView.as_view(), name='kpi-tree'),
    url(r'^login/ceo/$', validate_license(LoginCEO.as_view()), name='login-ceo'),
    url(r'^login/employee/$', validate_license(LoginEmployee.as_view()), name='login-as-employee'),
    #  url(r'^bsc/report/$', BSCReportView.as_view(), name='bsc-report'),

    # url(r'^assign-kpi-cfor-team-member/$', AssignKPITeamMember.as_view(), name='assign-kpi-team-member'),

    #url(r'^strategy-map/$', OrganizationStrategyMap.as_view(), name='company_strategy_map_detail'),
    url(r'^strategy-map-v2/$', OrganizationStrategyMapV2.as_view(), name='company_strategy_map_detail_v2'),
    #url(r'^strategy-map/(?P<hash>\w+-\w+)/$', OrganizationStrategyMap.as_view(), name='strategy_map_by_hash'),
    url(r'^strategy-map-v2/(?P<hash>\w+-\w+)/$', OrganizationStrategyMapV2.as_view(), name='strategy_map_by_hash_v2'),

    # url(r'^strategy-map/sub/(?P<user_id>\d+)/$', OrganizationStrategyMap.as_view(),
    #     name='sub_company_strategy_map_detail'),

    url(r'^strategy-map/sub-v2/(?P<user_id>\d+)/$', OrganizationStrategyMapV2.as_view(),
        name='sub_company_strategy_map_detail_v2'),
    # url(r'^strategy-map/branch/(?P<director_id>\d+)/$', SubOrganizationStrategyMap.as_view(), name='sub_company_strategy_map_detail'),
    # url(r'^strategy-map/sub/(?P<user_map_hash>\w+-\w+)/$', OrganizationStrategyMap.as_view(),
    #     name='sub_strategy_map_by_hash'),
    url(r'^strategy-map/sub-v2/(?P<user_map_hash>\w+-\w+)/$', OrganizationStrategyMapV2.as_view(),
        name='sub_strategy_map_by_hash_v2'),
    # url(r'^strategy-map/branch/(?P<hash>\w+-\w+)/$', SubOrganizationStrategyMap.as_view(), name='sub_strategy_map_by_hash'),

    url(r'^strategy-map/quarter/(?P<quarter_period>\d+)/$', OrganizationStrategyMap.as_view(),
        name='strategy_map_by_quarter'),
    url(r'^strategy-map/year/(?P<year>\d{4})/$', OrganizationStrategyMap.as_view(), name='strategy_map_by_year'),

    # url(r'^import-report/$', ImportReviewReport.as_view(), name='import-file-check'),
    # url(r'^import-report/(?P<quarter_id>\d+)/$', ImportReviewReport.as_view(), name='quarter-import-file-check'),
    # url(r'^move-kpi/$', MoveKPI.as_view(), name='move-kpi'),
    # url(r'^data-kpi/(?P<hash>.+)/$', InputDataKPI.as_view(), name='data-input-link'),
    url(r'^enable-edit-kpi/$', EnableEditKPI.as_view(), name='enable-edit-kpi'),

    url(r'^update-review-type/$', UpdateReviewTypeKPI.as_view(), name='update-review-type'),

    # url(r'^api/perform-reward/$', api.PerformRewardData.as_view(), name='api-perform-reward'),
    url(r'^kpi/comment/(?P<unique_key>.+)/$', KPIComment.as_view(), name='kpi-comment'),
    # url(r'^kpi-learning/$', views.KPILearning.as_view(), name='kpi-learning'),

    #  url(r'^export-assigned-kpis/$', ExportAssignedKPIs.as_view(), name='export-assigned-kpi'),

    url(r'^create-sm-branch/$', CreateStrategyMapForBranch.as_view(), name='create-sm-branch'),

    # url(r'^excel_full/(?P<user_id>\d+)/$', ExportFull.as_view(), name='export-full'),

    url(r'^integration/$', integration, name='integration'),
    url(r'^report/$', report_home, name='report-home'),
    url(r'^report/strategymap/$', report_strategymap, name='report-strategymap'),
    url(r'^deleted_kpi/$', history_deleted_kpi, name='history_deleted_kpi'),
    url(r'^score/$', report_score, name='report-score'),
    url(r'^report/ket-qua-ca-nhan/$', report_personal, name='report-personal'),

    url(r'^kpilib/$', KPILibManagement.as_view(), name='kpi-lib-management'),

    url(r'^notifications/$', Notifications.as_view(), name='perf-notification'),

    url(r'^position/(?P<position_id>\d+)/kpis/$', KPIPostition.as_view(), name='kpi-position'),
    url('^todo/', include('todo.urls')),

    url(r'^kpi-target/$', KPITarget.as_view(), name='kpi-target'),

    url(r'^kpi_list/$', ListKPI_View.as_view(), name='performance-kpi-list'),
]
# url(r'^import/kpis/copy$', import_kpis_copy, name='import_kpis_copy'),
