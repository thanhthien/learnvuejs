# !/usr/bin/python
# -*- coding: utf-8 -*-

import datetime
import json

from django.views import generic

from company.models import Organization
from dateutil.parser import parse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.cache import cache
from django.core.exceptions import PermissionDenied
from django.db.models.aggregates import Sum
from django.http import HttpResponseRedirect
from django.http.response import HttpResponse, Http404
from django.shortcuts import get_object_or_404
from django.shortcuts import render_to_response, render
from django.template import RequestContext
from django.template.defaultfilters import linebreaksbr
from django.utils.dateformat import format
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import View
from performance.base.views import PerformanceBaseView
from performance.logger import KPILogger
from performance.controllers.competency import update_competency

# from performance.decorators import consultant_permission
from performance.db_models.timeline import TaskEveryStage
from performance.db_models.timeline import Timeline
from performance.forms import PerformanceForm, InvitationReviewForm
from performance.forms import SmartKPIForm
from performance.models import KPI,  DescriptionCell, REVIEW_TYPE,  QuarterPeriod
from performance.services import add_new_cascadeKPIQueue
#from performance.services.Review import create_core_value
from performance.services.kpi import KPIServices, add_user_comment, add_comment_upload_file
from performance.services.quarter import get_all_quarter_by_org
from user_profile.models import Profile
from user_profile.services.profile import get_id_name_email_dict
from utils import brief_name
from utils.common import HttpResponseJson


@login_required
# #@consultant_permission
def home_view(request):
    return HttpResponseRedirect("/")

    #
    # if not organization.enable_performance:
    #     return HttpResponseRedirect("/")
    #
    # # quarters = QuarterPeriod.objects.filter(organization=organization, status__in=['IN', 'AR']).order_by('status','due_date')
    # quarters = get_all_quarter_by_org(request.user, organization)
    #
    # review_count = Review.objects.filter(reviewer=request.user,
    #                                      quarter_period=organization.get_current_quarter(),
    #                                      is_complete=False).exclude(review_type__in=['self', 'manager']).count()
    #
    # core_value = organization.get_core_value()
    # mission = None
    # if core_value:
    #     core_value = eval(core_value)
    #     mission = core_value.get('mission', None)
    #
    # ceo = organization.ceo
    # is_not_ceo_group_senso = True
    # profile = request.user.get_profile()
    # if organization.id == 205 and ceo == request.user:
    #     is_not_ceo_group_senso = False
    # # elif organization.id == 205 and profile.report_to_id == ceo.id:
    # elif organization.id == 205 and profile.parent == ceo.profile:
    #     is_not_ceo_group_senso = False
    #
    # if organization.show_timeline:
    #     timeline = Timeline.objects.filter(organization=organization).order_by('id')
    #     tasks = TaskEveryStage.objects.filter(organization=organization).order_by('stage_id', 'due_date',
    #                                                                               'due_time_start', 'end_date')
    # else:
    #     timeline = []
    #     tasks = []
    #
    # manager = ceo
    # p = Profile.objects.get(user=request.user)
    # # if p.report_to:
    # if p.parent:
    #     manager = p.parent
    # count_kpi = len(manager.get_profile().get_kpis_parent()) + 1
    # height = 250 * count_kpi + 300
    #
    # variables = {
    #     'organization': organization,
    #     'user_organization': user_organization,
    #     'quarters': quarters,
    #     'review_count': review_count,
    #     'mission': mission,
    #     'ceo': ceo,
    #     'is_not_ceo_group_senso': is_not_ceo_group_senso,
    #     'timeline': timeline,
    #     'tasks': tasks,
    #     'today': datetime.date.today(),
    #     'height': height
    # }
    #
    # return render(request, 'performance/index.html', variables)


@login_required()
def all_kpi(request):
    user_id = request.GET.get('team_id')
    if user_id:
        profile = get_object_or_404(Profile, user_id=user_id)

        return HttpResponseRedirect(profile.get_kpi_url())

        # team_id = user_id
        # if profile.report_to_id:
        #     team_id = profile.report_to_id
        #
        # return HttpResponseRedirect(reverse("team_performance") + "?team=" + str(team_id))
    else:
        raise Http404

#
# class CurrentGoals(PerformanceBaseView):
#     def get(self, request, *args, **kwargs):
#         # try:
#         user_organization = request.user.profile
#         organization = user_organization.organization
#
#         manager = organization.ceo
#         profile = request.user.get_profile()
#         # if profile.report_to_id:
#         if profile.parent:
#             # manager = profile.report_to
#             manager = profile.parent.user
#
#         extra_select = '''CASE WHEN performance_kpi.bsc_category = 'financial' THEN 1
#               WHEN performance_kpi.bsc_category = 'customer' THEN 2
#               WHEN performance_kpi.bsc_category = 'internal' THEN 3
#               WHEN performance_kpi.bsc_category = 'learninggrowth' THEN 4 ELSE 5 END '''
#
#         kpis = manager.get_profile().get_kpis_parent(False)
#         kpis = kpis.extra(select={'order_category': extra_select}).order_by('order_category', 'ordering', 'id')
#         variables = {
#             'organization': organization,
#             'user_organization': user_organization,
#             'manager': manager,
#             'kpis': kpis,
#             'categories': BSC_CATEGORY,
#             'year': datetime.date.today().year
#         }
#
#         return render(request, 'performance/bsc/current_goals.html', variables)

#
# class YearGoals(PerformanceBaseView):
#     def get(self, request, *args, **kwargs):
#         year = int(kwargs['year'])
#         user_organization = request.user.profile
#         organization = user_organization.organization
#
#         years = []
#         for y in range(2014, datetime.date.today().year + 2):
#             years.append(y)
#
#         manager = organization.ceo
#         extra_select = '''CASE WHEN performance_kpi.bsc_category = 'financial' THEN 1
#               WHEN performance_kpi.bsc_category = 'customer' THEN 2
#               WHEN performance_kpi.bsc_category = 'internal' THEN 3
#               WHEN performance_kpi.bsc_category = 'learninggrowth' THEN 4 ELSE 5 END '''
#
#         kpis = KPI.objects.filter(user_id=manager.id,
#                                   parent=None,
#                                   quarter_period=None,
#                                   year=year)
#
#         kpis = kpis.extra(select={'order_category': extra_select}).order_by('order_category', 'id')
#
#         variables = {
#             'organization': organization,
#             'user_organization': user_organization,
#             'manager': manager,
#             'kpis': kpis,
#             'categories': BSC_CATEGORY,
#             'year': year,
#             'years': years
#         }
#
#         return render(request, 'performance/bsc/year_goals.html', variables)


def basic_view(request): #pragma: no cover #not used
    organization = request.user.get_profile().get_organization()
    if request.method == "POST":
        if 'enable_edit' in request.POST:
            enable_edit = request.POST['enable_edit']
            enable_to_date = request.POST['date']
            try:
                enable_to_date = parse(enable_to_date, dayfirst=True)
            except:
                enable_to_date = None

            organization.enable_edit = True if enable_edit == 'true' else False
            organization.edit_to_date = enable_to_date
            organization.save()
            return HttpResponse('ok')

        if 'enable_competency' in request.POST:
            enable_competency = request.POST['enable_competency']
            organization.enable_competency = True if enable_competency == 'true' else False
            if not organization.enable_competency:
                organization.review_360 = False

            organization.save()
            return HttpResponse('ok')

        if 'company_name' in request.POST:
            organization.name = request.POST['company_name']
            organization.save()
            return HttpResponse('ok')

        if 'workflow' in request.POST:
            if request.POST.get('workflow') == 'self-assessment':
                organization.self_assessment = True if request.POST.get('value') == '1' else False
            elif request.POST.get('workflow') == '360-preview':
                organization.review_360 = True if request.POST.get('value') == '1' else False
                if not organization.enable_competency and organization.review_360:
                    organization.review_360 = False

            organization.save()
            return HttpResponse('ok')

        if 'logo' in request.FILES:
            organization.logo = request.FILES['logo']
            organization.save()

    if not organization.enable_competency and organization.review_360:
        organization.review_360 = False
        organization.save()

    variables = RequestContext(request, {
        'organization': organization
    })

    return render_to_response('performance/basic.html', variables)


#not used
@login_required
@csrf_exempt
def update_kpi(request, quarter_period, is_bsc): #pragma: no cover
    result = "ok"
    is_bsc = True
    form = SmartKPIForm(is_bsc, request.POST)
    if form.is_valid():
        kpi_id = request.POST.get('obj_id', '')

        if kpi_id.isdigit():
            try:
                kpi = KPI.objects.get(pk=kpi_id, quarter_period=quarter_period)
            except KPI.DoesNotExist:
                return HttpResponse(u"KPI không tồn tại.")

                # kpi.description = form.cleaned_data['description']

            # changed = kpi.tracker.changed()
            # content = ugettext("Update: ") + kpi.get_changed_history(False, u', ')
            # description = kpi.get_changed_history()
            # kc = KpiComment(user=request.user,
            # kpi_unique_key=kpi.unique_key,
            # kpi=kpi, send_from_ui='auto',
            #                 content=content,
            #                 is_update=True)


            kpi = KPIServices().update_kpi(request.user, kpi, operator=form.cleaned_data['operator'],
                                           bsc_category=form.cleaned_data['category'],
                                           target=form.cleaned_data['target'],
                                           unit=form.cleaned_data['unit'],
                                           end_date=form.cleaned_data['deadline'],
                                           name=form.cleaned_data['name']
                                           )

            # TODO: move this part into services
            person_id = request.POST.get('person_id', None)
            parent_id = request.POST.get('parent', None)
            kpi = kpi.move_kpi(parent_id, request.user)

            # kpi.save()
            # if is_bsc:
            # kpi.operator = form.cleaned_data['operator']
            # kpi.bsc_category = form.cleaned_data['category']
            # kpi.target = form.cleaned_data['target']
            # kpi.unit = form.cleaned_data['unit']
            # kpi.end_date = form.cleaned_data['deadline']

            # if kpi.name != form.cleaned_data['name']:
            #     kpi.name = form.cleaned_data['name']
            #     kpi.name_vi = form.cleaned_data['name']
            #     kpi.name_en = form.cleaned_data['name']
            #     if kpi.cascaded_from:
            #         kpi.cascaded_from.name = form.cleaned_data['name']
            #         kpi.cascaded_from.save()

            # if kpi.assigned_to_id:
            #     cascade = KPI.objects.filter(user_id=kpi.assigned_to_id,
            #                                  cascaded_from_id=kpi.id,
            #                                  quarter_period=quarter_period).first()
            #     if cascade:
            #         cascade.name = form.cleaned_data['name']
            #         cascade.name_vi = form.cleaned_data['name']
            #         cascade.name_en = form.cleaned_data['name']
            #         cascade.operator = form.cleaned_data['operator']
            #         cascade.target = form.cleaned_data['target']
            #         cascade.unit = form.cleaned_data['unit']
            #         cascade.end_date = form.cleaned_data['deadline']
            #         cascade.save()





            #            kc.save()
            #
            #             if 'name' in changed or 'target' in changed or 'operator' in changed or 'real' in changed:
            #                 if kpi.user_id == request.user.id:
            #                     kpi.manager_confirmed = False
            #                 else:
            #                     pass
            #                     # kpi.self_confirmed = False
            #             kpi.update_targets()
            #             if not kpi.parent_id and kpi.user_id and kpi.owner_email != kpi.user.email:
            #                 kpi.owner_email = kpi.user.email

            #            kpi.save()

            # if description:
            #     description = description
            #     KPILogger(request.user, kpi, 'update', description).start()

            return HttpResponse(result)
        else:

            person_id = request.POST.get('person_id', None)
            parent_id = request.POST.get('parent', None)
            if not person_id:
                return HttpResponse("Data was not loaded.")
            kpi = KPIServices().create_kpi(creator=request.user, operator=form.cleaned_data['operator'],
                                           bsc_category=form.cleaned_data['category'],
                                           target=form.cleaned_data['target'],
                                           unit=form.cleaned_data['unit'],
                                           end_date=form.cleaned_data['deadline'],
                                           name=form.cleaned_data['name'],
                                           description=form.cleaned_data['description'],
                                           quarter_period=quarter_period,
                                           is_owner=True,
                                           user_id=person_id,
                                           parent_id=parent_id,

                                           #  owner_email=request.user.email
                                           )
            #  kpi = KPI()
            # kpi.name = form.cleaned_data['name']
            # kpi.name_en = form.cleaned_data['name']
            # kpi.name_vi = form.cleaned_data['name']
            #   kpi.description = form.cleaned_data['description']


            # kpi.
            # if is_bsc:
            #     kpi.operator = form.cleaned_data['operator']
            #     kpi.bsc_category = form.cleaned_data['category']
            #     kpi.target = form.cleaned_data['target']
            #     kpi.unit = form.cleaned_data['unit']
            #     kpi.end_date = form.cleaned_data['deadline']

            #  kpi.quarter_period = quarter_period
            # kpi.is_owner = True

            # TODO: move this into services: KPIServices().create_kpi
            if parent_id.isdigit() and person_id.isdigit() and KPI.objects.filter(user_id=person_id,
                                                                                  id=parent_id).exists():
                kpi.parent_id = parent_id
                kpi.refer_to_id = parent_id
                parent = KPI.objects.get(id=parent_id)
                parent.owner_email = parent.user.email
                #  kpi.owner_email = kpi.user.email
                parent.save()
            else:
                kpi.parent_id = None
                # kpi.owner_email = kpi.user.email
            # ordering = kpi.get_next_ordering()
            # kpi.ordering = ordering


            # kpi.update_targets()
            kpi.save()

            result = {
                'status': 'ok',
                'type': 'new',
                "parent": kpi.parent_id,
                'obj': kpi.newbie_to_json()
            }

            # kc = KpiComment(user=request.user,
            #                 kpi_unique_key=kpi.unique_key,
            #                 kpi=kpi, send_from_ui='web',
            #                 content=ugettext("added KPI: ") + kpi.name,
            #                 is_update=True)
            # kc.save()

            # Khang
            # Changed into service
            add_user_comment(request.user, kpi, ugettext("added KPI: ") + kpi.name)

            KPILogger(request.user, kpi, 'new', kpi.get_name()).start()
            return HttpResponse(json.dumps(result), content_type="application/json")
        return HttpResponse('failed')
    errors = json.dumps(form.errors)
    return HttpResponse(errors, content_type="application/json")


# except:
# return HttpResponseRedirect(reverse('home_view'))

#
# @login_required
# @csrf_exempt
# def performance_update_plan(request):
#     if request.is_ajax() and request.method == "POST":
#         profile_id = request.POST.get('id', None)
#         try:
#             profile = Profile.objects.get(pk=profile_id)
#         except:
#             raise Http404
#
#         changed = None
#         if 'short_term_career' in request.POST:
#             profile.short_term_career = request.POST.get('short_term_career')
#             changed = ugettext('Short term career objective')
#         elif 'short_term_development' in request.POST:
#             profile.short_term_development = request.POST.get('short_term_development')
#             changed = ugettext('Short term development activities')
#         elif 'long_term_career' in request.POST:
#             profile.long_term_career = request.POST.get('long_term_career')
#             changed = ugettext('Long term career objective')
#         elif 'long_term_development' in request.POST:
#             profile.long_term_development = request.POST.get('long_term_development')
#             changed = ugettext('Long term development activities')
#         else:
#             return HttpResponse("Request invalid")
#
#         profile.save()
#         if changed:
#             KPILogger(request.user, profile, 'update', description=changed).start()
#         return HttpResponse('ok')
#     else:
#         return HttpResponseJson({'status': 'Bad Request'})
#         # return HttpResponse("Bad Request")
#

#
# def my_handler(obj):
#     print "my handler"
#     return "mmmmmm"

#
# @login_required
# @csrf_exempt
# # @consultant_permission
# def ownperformance_view(request):
#     profile = request.user.get_profile()
#     organization = profile.get_organization()
#     if not organization:
#         raise PermissionDenied
#
#     current_quarter_period = organization.get_current_quarter()
#     quarter_period = None
#
#     if 'quarter' in request.GET:
#         try:
#             due_date = parse(request.GET['quarter'], dayfirst=True)
#             quarter_period = QuarterPeriod.objects.get(due_date=due_date,
#                                                        organization=organization)
#         except:
#             quarter_period = None
#
#     if not quarter_period:
#         quarter_period = current_quarter_period
#
#     usr_org = profile.get_user_organization()
#
#     if request.method == "POST" and 'type' in request.POST:
#         if 'type' in request.POST and request.POST['type'] == 'kpi':
#             return update_kpi(request, quarter_period, organization.enable_bsc)
#         elif 'type' in request.POST and request.POST['type'] == 'comp':
#             return update_competency(request, quarter_period)
#     #
#     # if request.method == "POST" and request.is_ajax():
#     #     review = Review.objects.get_or_create(organization=organization,
#     #                                           reviewee=request.user,
#     #                                           reviewer=request.user,
#     #                                           review_type='self',
#     #                                           quarter_period=quarter_period)[0]
#     #
#     #     review.share_to_manager = True if request.POST.get('share_to_manager', '') == "true" else False
#     #     review.save()
#     #     return HttpResponse('ok')
#
#     if request.GET.get('json_data', None):
#         data = []
#         job_title_name = ""
#         if profile.current_job_title:
#             job_title_name = profile.current_job_title.name
#
#         reviewer_id = None
#         # if profile.report_to:
#         if profile.parent:
#             # reviewer_id = profile.report_to_id
#             reviewer_id = profile.parent.user_id
#
#         share_to_manager = None
#         # self_review = Review.objects.get_or_create(organization=organization,
#         #                                            reviewee=request.user,
#         #                                            reviewer=request.user,
#         #                                            review_type='self',
#         #                                            quarter_period=quarter_period)[0]
#         # share_to_manager = self_review.share_to_manager
#
#         shared_manager = None
#         percent_completed = 0
#         if reviewer_id:
#             # r = Review.objects.filter(organization=organization,
#             #                           reviewee=request.user,
#             #                           reviewer_id=reviewer_id,
#             #                           review_type='manager',
#             #                           quarter_period=quarter_period)
#             # if r.count() > 1:
#             #     temp = r.first()
#             #     r.exclude(id=temp.id).delete()
#             #
#             # review = Review.objects.get_or_create(organization=organization,
#             #                                       reviewee=request.user,
#             #                                       reviewer_id=reviewer_id,
#             #                                       review_type='manager',
#             #                                       quarter_period=quarter_period)[0]
#             # shared_manager = review.share_to_all
#             # percent_completed = review.completion
#             pass
#
#         #create_core_value(profile.user, organization, quarter_period)
#         if organization.enable_competency:
#             competencies = profile.competencies_dict('manager', quarter_period,
#                                                      shared_manager, reviewer_id)
#         else:
#             competencies = []
#         kpis = profile.kpis_dict('manager', quarter_period,
#                                  shared_manager, reviewer_id)
#         current_q = quarter_period.id == current_quarter_period.id
#         current = current_q
#         if current:
#             current = quarter_period.due_date >= datetime.date.today()
#
#         max_min_kpi = profile.get_bound_score_kpi(quarter_period)
#         max_min_competency = profile.get_bound_score_competency(quarter_period)
#
#         expired_review = current_quarter_period.due_date < datetime.date.today()
#         ignore_zero = True
#         if organization.id == 47:
#             ignore_zero = False
#
#         user_data = {
#             'user_id': profile.user_id,
#             # 'report_to': profile.report_to_id,
#             'report_to': profile.parent.user_id if profile.parent else None,
#             'profile': profile.id,
#             'name': profile.display_name,
#             'email': profile.user.email,
#             'phone': profile.phone,
#             'skype': profile.skype,
#             'start_date': format(usr_org.start_date, 'd-m-Y') if usr_org.start_date else '',
#             'role_start': format(usr_org.role_start, 'd-m-Y') if usr_org.role_start else '',
#             'employee_code': profile.employee_code,  # usr_org.employee_code,
#             'avatar': profile.get_avatar(),
#             'job_title_name': job_title_name,
#             'position': profile.title,
#             'brief_name': brief_name(profile.display_name),
#             'competencies': competencies,
#             'kpis': kpis,
#             'share_to_manager': share_to_manager,
#             'shared_manager': shared_manager,
#             'short_term_career': profile.short_term_career,
#             'short_term_development': profile.short_term_development,
#             'long_term_career': profile.long_term_career,
#             'long_term_development': profile.long_term_development,
#             'percent_completed': percent_completed,
#             'reviews': profile.get_360_review_dict(quarter_period),
#             'current': current,
#             'current_q': current_q,
#
#             'competency_final_score': profile.score_full_competency(quarter_period, ignore_zero),
#             'kpi_final_score': profile.score_full_kpi_percent(quarter_period, ignore_zero),
#             'min_score_kpi': max_min_kpi[0],
#             'max_score_kpi': max_min_kpi[1],
#             'min_score_competency': max_min_competency[0],
#             'max_score_competency': max_min_competency[1],
#             'strength_kpi': profile.get_strength_kpi(quarter_period),
#             'strength_competency': profile.get_strength_competency(quarter_period),
#             'weak_kpi': profile.get_weak_kpi(quarter_period),
#             'weak_competency': profile.get_weak_competency(quarter_period),
#             'previous_competency_final_score': profile.previous_competency_final_score,
#             'previous_kpi_final_score': profile.previous_kpi_final_score,
#             'enable_competency': organization.enable_competency,
#             'enable_bsc': organization.enable_bsc,
#             'history_scores': [],
#             'is_active': profile.is_active(),
#             'status': 'active' if profile.is_active() else "inactive",
#             'quarter_period': quarter_period.id,
#             'can_edit': organization.enable_to_edit(),
#             'expired_review': expired_review
#         }
#
#         data.append(user_data)
#
#         items = {}
#         for i in DescriptionCell.objects.all():
#             temp = i.slug.replace('-', '_')
#             items[temp] = linebreaksbr(i.description)
#
#         return HttpResponseJson([data, items])
#
#         # return HttpResponse(json.dumps([data, items], indent=4, default=json_default_handler),
#         #                     content_type="application/json")
#         #
#         # result={"a": datetime.datetime.now() }
#         # # return HttpResponse(json.dumps(result, default=json_default_handler), content_type="application/json")
#         # # print "dump json data"
#         # testtest={"a": float('nan')}
#         # return HttpResponse(json.dumps(testtest, default=json_default_handler, allow_nan=False), content_type="application/json")
#
#
#         # redirect
#     user_id = request.user.id
#     return HttpResponseRedirect('/performance/team-performance/?team={0}&user_id={0}'.format(user_id))
#
#     jobs = JobTitle.objects.filter(category__industry_id=organization.industry_id)
#     remain = (quarter_period.due_date - datetime.date.today()).days
#     remain = remain if remain > 0 else 0
#
#     form = PerformanceForm()
#     kpi_form = SmartKPIForm(True)
#     invitation_form = InvitationReviewForm()
#     quarter_periods = organization.get_all_quarter_period()
#
#     if organization.ceo is None:
#         organization.ceo = request.user
#         organization.save()
#
#     variables = RequestContext(request, {
#         'organization': organization,
#         'jobs': jobs,
#         'quarter_period': quarter_period,
#         'remain': remain,
#         'form': form,
#         'kpi_form': kpi_form,
#         'review_types': REVIEW_TYPE[2:],
#         'invitation_form': invitation_form,
#         'employee': True,
#         'usr_org': usr_org,
#         'quarter_periods': quarter_periods,
#         'is_ceo': organization.ceo.id == request.user.id
#     })
#
#     return render_to_response('performance/own-performance.html', variables)

#
# @login_required
# @csrf_exempt
# # @consultant_permission
# def invite_view(request):
#     profile = request.user.get_profile()
#     organization = profile.get_organization()
#     quarter_period = organization.get_current_quarter()
#
#     if request.method == "POST" and request.is_ajax():
#         review_id = request.POST.get('review_id', None)
#         # try:
#         #     review = Review.objects.get(pk=review_id, reviewer=request.user)
#         #     review.is_complete = not review.is_complete
#         #     review.save()
#         #     return HttpResponse('ok')
#         # except Review.DoesNotExist:
#         #     return HttpResponse('Error')
#
#     # reviews = Review.objects.filter(review_type__in=['peer', 'senior_manager', 'direct_report'],
#     #                                 reviewer=request.user, organization=organization,
#     #                                 quarter_period=quarter_period)
#     variables = RequestContext(request, {
#         'reviews': [],
#         'quarter_period': quarter_period
#     })
#     return render_to_response('performance/360_invite.html', variables)
#
#
# @login_required       --> not used
# @csrf_exempt
# def overall_performance_overview(request):
#
#     return render(request, 'report/overall_performance_overview.html')


@login_required
@csrf_exempt
def individual_performance_overview(request): #pragma: no cover
    profile = request.user.get_profile()
    organization = profile.get_organization()

    user_id = request.GET.get('user_id', 0)

    if user_id == 0:
        user = organization.ceo
    else:
        user = User.objects.get(id=user_id)

    # users = UserOrganization.objects.filter(organization=organization, active=True)



    quarter_period = organization.get_current_quarter()

    profiles = []

    group1 = []
    group2 = []
    group3 = []
    group4 = []
    group5 = []

    group1_finance = []
    group2_finance = []
    group3_finance = []
    group4_finance = []
    group5_finance = []

    group1_customer = []
    group2_customer = []
    group3_customer = []
    group4_customer = []
    group5_customer = []

    group1_process = []
    group2_process = []
    group3_process = []
    group4_process = []
    group5_process = []

    group1_learnngrowth = []
    group2_learnngrowth = []
    group3_learnngrowth = []
    group4_learnngrowth = []
    group5_learnngrowth = []

    all_parents = KPIServices().get_parent_kpis(user.id)

    delay_kpis = []

    for k in all_parents:
        if k.weight == 0:
            delay_kpis.append(k)
        else:
            score = k.get_score()
            # notify_slack( SLACK_DEV_CHANNEL,u"KPI Name " + k.name + u" " + str(score))
            if score <= 30:
                group1.append(k)

                if k.bsc_category == 'financial':
                    group1_finance.append(k)

                elif k.bsc_category == 'customer':
                    group1_customer.append(k)

                elif k.bsc_category == 'internal':
                    group1_process.append(k)

                elif k.bsc_category == 'learninggrowth':
                    group1_learnngrowth.append(k)


            elif score <= 50:
                group2.append(k)

                if k.bsc_category == 'financial':
                    group2_finance.append(k)

                elif k.bsc_category == 'customer':
                    group2_customer.append(k)

                elif k.bsc_category == 'internal':
                    group2_process.append(k)

                elif k.bsc_category == 'learninggrowth':
                    group2_learnngrowth.append(k)

            elif score <= 80:
                group3.append(k)

                if k.bsc_category == 'financial':
                    group3_finance.append(k)

                elif k.bsc_category == 'customer':
                    group3_customer.append(k)

                elif k.bsc_category == 'internal':
                    group3_process.append(k)

                elif k.bsc_category == 'learninggrowth':
                    group3_learnngrowth.append(k)


            elif score <= 100:
                group4.append(k)

                if k.bsc_category == 'financial':
                    group4_finance.append(k)

                elif k.bsc_category == 'customer':
                    group4_customer.append(k)

                elif k.bsc_category == 'internal':
                    group4_process.append(k)

                elif k.bsc_category == 'learninggrowth':
                    group4_learnngrowth.append(k)


            else:
                group5.append(k)

                if k.bsc_category == 'financial':
                    group5_finance.append(k)

                elif k.bsc_category == 'customer':
                    group5_customer.append(k)

                elif k.bsc_category == 'internal':
                    group5_process.append(k)

                elif k.bsc_category == 'learninggrowth':
                    group5_learnngrowth.append(k)

    finance = KPIServices().get_parent_kpis_by_category(user.id, 'financial')
    customer = KPIServices().get_parent_kpis_by_category(user.id, 'customer')
    process = KPIServices().get_parent_kpis_by_category(user.id, 'internal')
    learnngrowth = KPIServices().get_parent_kpis_by_category(user.id, 'learninggrowth')

    total = len(all_parents)
    if total == 0:
        total = 1
    # group1_percentage = len(group1) * 100/total
    # group2_percentage = len(group2) * 100/total
    # group3_percentage = len(group3) * 100/total
    # group4_percentage = len(group4) * 100/total
    # group5_percentage = len(group5) * 100/total

    ceo = organization.ceo.profile
    ceo.kpi_score = ceo.score_full_kpi_percent(quarter_period)

    sorted_profiles = sorted(profiles, key=lambda profile: profile.kpi_score)

    # paginator = Paginator(report, 50)
    # page = request.GET.get('page')



    return render(request, 'report/individual_performance_overview.html', {
        'ceo': ceo,
        'profiles': sorted_profiles,
        'group1': group1,
        'group2': group2,
        'group3': group3,
        'group4': group4,
        'group5': group5,

        'group1_finance': group1_finance,
        'group2_finance': group2_finance,
        'group3_finance': group3_finance,
        'group4_finance': group4_finance,
        'group5_finance': group5_finance,

        'group1_customer': group1_customer,
        'group2_customer': group2_customer,
        'group3_customer': group3_customer,
        'group4_customer': group4_customer,
        'group5_customer': group5_customer,

        'group1_process': group1_process,
        'group2_process': group2_process,
        'group3_process': group3_process,
        'group4_process': group4_process,
        'group5_process': group5_process,

        'group1_learnngrowth': group1_learnngrowth,
        'group2_learnngrowth': group2_learnngrowth,
        'group3_learnngrowth': group3_learnngrowth,
        'group4_learnngrowth': group4_learnngrowth,
        'group5_learnngrowth': group5_learnngrowth,

        'finance': finance,
        'customer': customer,
        'process': process,
        'learnngrowth': learnngrowth,
        'delay_kpis': delay_kpis,
        'delay_kpis_counter': len(delay_kpis),
    })

#
# @csrf_exempt
# @login_required()
# def invite_review(request):
#     profile = request.user.get_profile()
#     organization = profile.get_organization()
#
#     if request.method == "POST":
#         form = InvitationReviewForm(request.POST)
#         if form.is_valid():
#             quarter_period = organization.get_current_quarter()
#             user = User.objects.get(email=form.cleaned_data['email'])
#             review, created = Review.objects.get_or_create(organization=organization,
#                                                            reviewer=user,
#                                                            reviewee_id=form.cleaned_data['reviewee'],
#                                                            review_type=form.cleaned_data['review_type'],
#                                                            quarter_period=quarter_period)
#
#             site = settings.DOMAIN  # Site.objects.get_current()
#             hash_key = review.get_hash_key()
#             link = '%s%s?hash_key=%s' % (site, reverse('review_360'), hash_key)
#             if created:
#                 message = render_to_string('mail/360-review.html', {
#                     'full_name': profile.display_name,
#                     'company_name': organization.name,
#                     'reviewee': review.reviewee.profile.display_name,
#                     'due_date': quarter_period.due_date,
#                     'message': form.cleaned_data['content'],
#                     'link': link
#                 },
#                                            context_instance=RequestContext(request))
#                 if not settings.LOCAL:
#                     msg = send_mail_with_perm(u'360 review', message,
#                                        'Cloudjet <giang@cloudjetsolutions.com>', [form.cleaned_data['email'], ])
#
#                 item = {
#                     'id': review.id,
#                     'reviewer': review.reviewer.profile.display_name,
#                     'review_type': review.get_review_type_display(),
#                     'is_complete': review.is_complete,
#                     'completion': review.completion,
#                     'invited_at': format(review.created_at, 'd-M-Y'),
#                     'due_on': format(quarter_period.due_date, 'd M Y'),
#                     'updated_at': format(review.updated_at, 'd M Y')
#                 }
#                 return HttpResponse(json.dumps(item), content_type='application/json')
#         else:
#             return HttpResponse(form.as_ul())
#
#     return HttpResponse("ok")
#
#
# @login_required
# @login_required()
# def invitation_data(request):
#     # profile = request.user.get_profile()
#     # organization = profile.get_organization()
#     # uo = UserOrganization.objects.filter(organization=organization)
#     # results = []
#     # for u in uo:
#     #     item = {
#     #         'name': u.user.profile.display_name,
#     #         'email': u.user.email
#     #     }
#     #     results.append(item)
#
#     results = []
#
#     return HttpResponse(json.dumps(results, indent=4), content_type="application/json")

#
# @csrf_exempt
# def review_360(request):
#     hash_key = request.GET.get('hash_key', None)
#     valid = False
#     try:
#         map_key = signing.loads(hash_key)
#         valid = True
#         user = User.objects.get(pk=map_key['reviewee'])
#         organization = user.profile.get_organization()
#         quarter_period = organization.get_current_quarter()
#     except BadSignature:
#         valid = False
#         user = None
#         organization = None
#         quarter_period = None
#         review = None
#
#     if request.method == "POST":
#         if 'comp' in request.GET:
#             comp_id = request.POST.get('comp_id', None)
#             value = request.POST.get('value', None)
#             user_id = request.POST.get('person', None)
#             try:
#                 comp = Competency.objects.get(pk=comp_id, user_id=user_id)
#                 comp.set_score(value, '360' + str(map_key['reviewer']))
#             except Exception as e:
#                 return HttpResponse(str(e))
#             return HttpResponse(json.dumps({'score': value}), content_type='application/json')
#         elif 'review' in request.GET:
#             percent = request.POST.get('percent', 0)
#             user_id = request.POST.get('user_id', 0)
#             # try:
#             #     review = Review.objects.get(organization=organization,
#             #                                 reviewee_id=map_key['reviewee'],
#             #                                 reviewer=map_key['reviewer'],
#             #                                 review_type=map_key['review_type'],
#             #                                 quarter_period=quarter_period)
#             #
#             #     review.completion = float(percent)
#             #     review.save()
#             # except:
#             #     pass
#             return HttpResponse(percent)
#         elif 'comment' in request.GET:
#             comp_id = request.POST.get('id', None)
#             user_id = request.POST.get('user_id', None)
#             comment = request.POST.get('comment', None)
#             try:
#                 comp = Competency.objects.get(pk=comp_id, user_id=map_key['reviewee'])
#                 comp.set_comment_360(comment, map_key['reviewer'])
#             except Exception as e:
#                 return HttpResponse(str(e))
#             return HttpResponse('ok')
#         elif 'status' in request.GET:
#             # try:
#             #     review = Review.objects.get(organization=organization,
#             #                                 reviewee_id=map_key['reviewee'],
#             #                                 reviewer=map_key['reviewer'],
#             #                                 review_type=map_key['review_type'],
#             #                                 quarter_period=quarter_period)
#             #     review.is_complete = not review.is_complete
#             #     review.save()
#             # except Review.DoesNotExist:
#             #     return HttpResponse('error')
#             return HttpResponse('ok')
#
#     if valid and 'json_data' in request.GET:
#         competencies = Competency.objects.filter(user_id=map_key['reviewee'],
#                                                  parent=None,
#                                                  quarter_period=quarter_period)
#         list_competencies = []
#         for competency in competencies:
#             comp = competency.competency_dict('360', False, map_key['reviewer'])
#             list_competencies.append(comp)
#
#         data = {
#             'user_id': user.id,
#             'competencies': list_competencies
#         }
#         return HttpResponse(json.dumps([data], indent=4), content_type='application/json')
#
#     review = None
#     # if valid:
#         # try:
#         #     review = Review.objects.get(organization=organization,
#         #                                 reviewee_id=map_key['reviewee'],
#         #                                 reviewer=map_key['reviewer'],
#         #                                 review_type=map_key['review_type'],
#         #                                 quarter_period=quarter_period)
#         # except Review.DoesNotExist:
#         #     valid = False
#
#     return render(request, 'performance/360_review.html', {
#         'reviewee': user,
#         'organization': organization,
#         'quarter_period': quarter_period,
#         'review': review,
#         'valid': valid
#     })

#
# @login_required()
# def planning_view(request):
#     organization = request.user.get_profile().get_organization()
#     core_value = organization.get_core_value()
#     if core_value:
#         core_value = eval(core_value)
#     return render(request, 'performance/planning.html', {
#         'core_value': core_value
#     })
#
#
# @staff_member_required
# def search_user_kpi(request):
#     q = request.GET.get('q', '')
#     users = []
#     if q != '':
#         users = User.objects.filter(Q(username__contains=q) | Q(email__contains=q))
#
#     return render(request, 'performance/management/planning.html', {
#         'users': users,
#
#     })

#
# class CurrentKPIView(CurrentPerformanceBaseView):
#     model = KPI


class WeightingUpdateView(View):
    @method_decorator(login_required)
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(WeightingUpdateView, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        organization = request.user.get_profile().get_organization()
        quarter_period = organization.get_current_quarter()

        value = request.POST.get('value', 0)
        obj_id = request.POST.get('pk')
        kpi = KPI.objects.get(pk=obj_id, quarter_period=quarter_period)
        kpi.weight = float(value)
        kpi.save()

        final_score = None
        if kpi.parent:
            total_weight = kpi.parent.get_children().aggregate(Sum('weight'))['weight__sum']
            final_score , m1, m2, m3 = kpi.parent.calculate_avg_score(kpi.user_id)
            if final_score:
                kpi.parent.set_score(final_score, kpi.user_id)
                # kpi.parent.set_score(final_score, kpi.user.profile.report_to_id)
                kpi.parent.set_score(final_score, kpi.user.profile.parent.user_id if kpi.user.profile.parent else None)
                # CascadeKPIQueue.objects.create(kpi=kpi.parent)
                add_new_cascadeKPIQueue(kpi.parent)
        else:
            total_weight = 0
            final_score = kpi.get_score()  # kpi.user_id)

        kpi_final_score = kpi.user.profile.score_full_kpi_percent(quarter_period)
        return HttpResponse(json.dumps({
            'id': kpi.id,
            'parent_id': kpi.parent_id,
            'weight': kpi.weight,
            'final_score': final_score,
            'kpi_final_score': kpi_final_score,
            'total_weight': total_weight
        }), content_type='application/json')


class OrderingUpdateView(View): #pragma: no cover
    @method_decorator(login_required)
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(OrderingUpdateView, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        organization = request.user.get_profile().get_organization()
        quarter_period = organization.get_current_quarter()

        value = request.POST.get('value', 0)
        obj_id = request.POST.get('pk')
        try:
            kpi = KPI.objects.get(pk=obj_id, quarter_period=quarter_period)
        except:
            return HttpResponse("Kpi doesn't exist.")
        kpi.ordering = value
        kpi.save()

        ordering = float(kpi.ordering)
        if ordering == int(ordering):
            ordering = int(ordering)
        return HttpResponse(json.dumps({
            'id': kpi.id,
            'parent_id': kpi.parent_id,
            'ordering': ordering
        }), content_type='application/json')

class PerformReward(PerformanceBaseView):#pragma: no cover
    def get(self, request, *args, **kwargs):
        organization = self.get_organization(request.user)
        if not organization.enable_perform_reward:
            raise PermissionDenied

        user_id_list = Profile.objects.filter(organization=organization).values_list('user_id', flat=True)
        users = User.objects.filter(id__in=user_id_list)
        # return HttpResponse('OK')
        return render(request, 'report/perform_reward_report.html', {'users': users})

class UploadFileComment(PerformanceBaseView):#pragma: no cover
    def post(self, request, *args, **kwargs):
        organization = self.get_organization(request.user)
        quarter_period = organization.get_current_quarter()
        kpi = KPI.objects.filter(unique_key=kwargs['unique_key'],
                                 quarter_period=quarter_period).order_by('-id')

        if kpi.count() > 0 and request.FILES:
            kpi = kpi[0]
            content_file = request.FILES['file']
            # kc = KpiComment(user=request.user, content='',
            #                 file=content_file,
            #                 kpi_unique_key=kwargs['unique_key'],
            #                 kpi=kpi, send_from_ui='web')
            # kc.save()

            # Khang
            # Change into service.
            kc = add_comment_upload_file(user=request.user, kpi=kpi, file=content_file, unique_key=kwargs['unique_key'])

            variables = {
                'obj_id': kc.id,
                'unique_key': kc.kpi_unique_key,
                'file_name': kc.get_file_name(),
                'file_url': kc.file.url
            }

            return HttpResponse(json.dumps(variables), content_type="application/json")
        else:
            return HttpResponse('')


# use in team-review.js & self-review.js
class SubordinateList(PerformanceBaseView):
    def post(self, request, *args, **kwargs):
        organization = self.get_organization(request.user)

        searchable_data = cache.get('searchable_data' + str(organization.id))
        if not searchable_data:
            searchable_data = []
            searchable_users = Profile.objects.filter(organization=organization,
                                                      active=True)  # .values('user_id', 'user__email',
            #        'user__profile__display_name')

            for u in searchable_users:
                searchable_data.append(

                    get_id_name_email_dict(u.user.profile))
                #   {'id': str(u['user_id']), 'name': u['user__profile__display_name'] + u' - ' + u['user__email']})

            cache.set('searchable_data' + str(organization.id), searchable_data, 30 * 60)

        searchable_list = json.dumps(searchable_data)

        return HttpResponse(searchable_list, content_type="application/json")


class PublicTimeline(View):#pragma: no cover
    def get(self, request, *args, **kwargs):
        organization = None
        hash = kwargs['hash']
        try:
            organization = Organization.objects.get(hash=hash)
        except:
            raise Http404

        if not organization.enable_performance:
            return HttpResponseRedirect("/")

        core_value = organization.get_core_value()
        mission = None
        if core_value:
            core_value = eval(core_value)
            mission = core_value.get('mission', None)

        ceo = organization.ceo

        step = request.GET.get('step', 0)

        if organization.show_timeline:

            timeline = Timeline.objects.filter(organization=organization).order_by('id')

            if step > 0:
                tasks = TaskEveryStage.objects.filter(organization=organization, id=step).order_by('stage_id',
                                                                                                   'due_date')
            else:
                tasks = TaskEveryStage.objects.filter(organization=organization).order_by('stage_id', 'due_date')
        else:
            timeline = []
            tasks = []

        lang = 'vi'
        if 'lang' in request.GET and request.GET.get('lang') in ['en', 'vi']:
            lang = request.GET.get('lang')

        variables = {
            'organization': organization,
            'mission': mission,
            'ceo': ceo,
            'timeline': timeline,
            'tasks': tasks,
            'today': datetime.date.today(),
            'lang': lang,
            'hash': hash,
            'step': step
        }

        return render(request, 'performance/timeline/timeline_public.html', variables)
#
#
# class Pricing(View):
#     def get(self, request, *args, **kwargs):
#         variables = {}
#
#         return render(request, 'performance/pricing/price-kpi.html', variables)

#
# class ASSESS(PerformanceBaseView):
#     def get(self, request, *args, **kwargs):
#         variables = {}
#
#         return render(request, 'performance/assess/index.html', variables)


class MOBILE_REVIEW(PerformanceBaseView):#pragma: no cover
    def get(self, request, *args, **kwargs):
        variables = {}

        return render(request, 'mobile/review/chat.html', variables)

#
# class KPILearning(PerformanceBaseView):
#     def get(self, request, *args, **kwargs):
#         organization = self.get_organization(request.user)
#
#         return render(request, 'performance/learning.html', {
#             'organization': organization
#         })



class ListKPI_View(PerformanceBaseView):
    def get(self, request, *args, **kwargs):
        variables = {}

        return render(request, 'performance/kpi_list.html', variables)
